<?php

return [
    'schema' => [
        \App\Presentation\Services\Filters\Scenario\ModelViewLeftSideBar::CONTEXT =>
            \App\Presentation\Services\Filters\Generators\ModelViewLeftSideBarGenerator::class,
        \App\Presentation\Services\Filters\Scenario\ModelViewPhones::CONTEXT =>
            \App\Presentation\Services\Filters\Generators\ModelViewPhonesGenerator::class,
        \App\Presentation\Services\Filters\Scenario\BrandViewLeftSideBar::CONTEXT =>
            \App\Presentation\Services\Filters\Generators\BrandViewLeftSideBarGenerator::class,
    ],

    'map' => [
        'radio' => \App\Utils\Template\Filters\Filters\Radio::class,
        'slider' => \App\Utils\Template\Filters\Filters\Slider::class,
        'checkbox' => \App\Utils\Template\Filters\Filters\Checkbox::class,
    ],

    'filters' => [
        \App\Presentation\Services\Filters\Scenario\ModelViewLeftSideBar::CONTEXT => [
            'monthly_cost' => [
                'type' => 'radio',
                'content' => [20, 30, 40],
            ],
            'upfront_cost' => [
                'type' => 'radio',
                'content' =>  [0, 50, 100],
            ],
        ],
    ],

    'items' => [
        \App\Presentation\Services\Filters\Scenario\ModelViewLeftSideBar::CONTEXT => [
            'base' => \App\Utils\Template\Filters\Items\CompositeItem::class,
            'items' => [
                \App\Presentation\Services\Filters\Generators\Items\MonthlyCostSliderGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\MonthlyCostRadioGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\TotalCostSliderGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\TotalCostRadioGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\NetworkCheckboxGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\ConditionCheckboxGenerator::class,
            ]
        ],
        \App\Presentation\Services\Filters\Scenario\ModelViewPhones::CONTEXT => [
            'base' => \App\Utils\Template\Filters\Items\CompositeItem::class,
            'items' => [
                \App\Presentation\Services\Filters\Generators\Items\PhonesCapacityRadioGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\PhoneColorRadioGenerator::class,
            ]
        ],
        \App\Presentation\Services\Filters\Scenario\BrandViewLeftSideBar::CONTEXT => [
            'base' => \App\Utils\Template\Filters\Items\CompositeItem::class,
            'items' => [
                \App\Presentation\Services\Filters\Generators\Items\MonthlyCostSliderGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\MonthlyCostRadioGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\TotalCostSliderGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\TotalCostRadioGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\PhoneModelRadioGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\NetworkCheckboxGenerator::class,
                \App\Presentation\Services\Filters\Generators\Items\ConditionCheckboxGenerator::class,
            ]
        ]
    ]
];
