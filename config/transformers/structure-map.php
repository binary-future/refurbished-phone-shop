<?php

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\AwinDealsImport;
use App\Application\Services\Importer\Scenarios\AwinPhonesImport;
use App\Application\Services\Importer\Scenarios\BrandImageUploadImport;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\AwinDealsImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\AwinPhonesImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\BrandImageUploadImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\StoreRatingSeedFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\NetworkImageUploadImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\PhoneImageUploadImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\SpecsDumpImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\SpecsSeedFields;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\StoreImportFields;
use App\Application\Services\Importer\Scenarios\EcrawlerDealsImport;
use App\Application\Services\Importer\Scenarios\StoreRatingSeed;
use App\Application\Services\Importer\Scenarios\NetworkImageUploadImport;
use App\Application\Services\Importer\Scenarios\PhoneImageUploadImport;
use App\Application\Services\Importer\Scenarios\SpecsDumpImport;
use App\Application\Services\Importer\Scenarios\SpecsSeed;
use App\Application\Services\Importer\Scenarios\StoreImport;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Specs\Autonomy;
use App\Domain\Phone\Specs\Camera;
use App\Domain\Phone\Specs\Connection;
use App\Domain\Phone\Specs\Display;
use App\Domain\Phone\Specs\OperatingSystem;
use App\Domain\Phone\Specs\Performance;
use App\Domain\Phone\Specs\PhoneCase;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Store;

return [
    StoreImport::CONTEXT => [
        StoreImportFields::RG_STORE_ID => [
            'group' => 'datafeed',
            'alias' => DatafeedInfo::FIELD_EXTERNAL_ID,
        ],
        StoreImportFields::API_ID => [
            'group' => 'datafeed',
            'alias' => DatafeedInfo::FIELD_API_ID,
        ],
        StoreImportFields::STORE_TITLE => [
            'group' => 'store',
            'alias' => Store::FIELD_NAME,
        ],
        StoreImportFields::URL_KEY => [
            'group' => 'store',
            'alias' => Store::FIELD_SLUG,
        ],
        StoreImportFields::TERM_IN_MONTHS => [
            'group' => 'store',
            'alias' => Store::FIELD_TERMS,
        ],
        StoreImportFields::GUARANTEE_IN_MONTHS => [
            'group' => 'store',
            'alias' => Store::FIELD_GUARANTEE,
        ],
        StoreImportFields::STORE_DESCRIPTION => [
            'group' => 'description',
            'alias' => Description::FIELD_CONTENT,
        ],
        StoreImportFields::IMAGE_URL => [
            'group' => 'image',
            'alias' => Image::FIELD_PATH,
        ],
        StoreImportFields::URL => [
            'group' => 'link',
            'alias' => Link::FIELD_LINK,
        ],
    ],
    /* PHONES */

//    \App\Application\Services\Importer\Scenarios\WebgainsDealsImport::CONTEXT => [
//        'brand' => [
//            'group' => 'brand',
//            'alias' => \App\Domain\Phone\Brand\Brand::FIELD_NAME,
//        ],
//        'product_name' => [
//            'group' => 'product',
//            'alias' => 'name',
//        ],
//        'Colour' => [
//            'group' => 'color',
//            'alias' => \App\Domain\Phone\Phone\Color::FIELD_NAME,
//        ],
//        'product_id' => [
//            'group' => 'deal',
//            'alias' => 'external_id',
//        ],
//        'contract_type' => [
//            'group' => 'deal',
//            'alias' => 'type',
//        ],
//        'contract_length' => [
//            'group' => 'deal-payment',
//            'alias' => 'payment_term',
//        ],
//        'handset_price' => [
//            'group' => 'deal-payment',
//            'alias' => 'initial_cost',
//        ],
//        'price' => [
//            'group' => 'deal-payment',
//            'alias' => 'monthly_cost'
//        ],
//        'deeplink' => [
//            'group' => 'link',
//            'alias' => 'link',
//        ],
//        'network_promotion' => [
//            'group' => 'operator',
//            'alias' => 'name',
//        ],
//        "inclusive_minutes" => [
//            'group' => 'contract',
//            'alias' => 'minutes',
//        ],
//        'inclusive_texts' => [
//            'group' => 'contract',
//            'alias' => 'sms'
//        ],
//        'data_allowance' => [
//            'group' => 'contract',
//            'alias' => 'data'
//        ],
//        'stock_code' => [
//            'group' => 'deal',
//            'alias' => 'in_stock',
//        ]
//    ],
//    \App\Application\Services\Importer\Scenarios\WebgainsAffordableDealsImport::CONTEXT => [
//        'brand' => [
//            'group' => 'brand',
//            'alias' => \App\Domain\Phone\Brand\Brand::FIELD_NAME,
//        ],
//        'product_name' => [
//            'group' => 'product',
//            'alias' => 'name',
//        ],
//        'Colour' => [
//            'group' => 'color',
//            'alias' => \App\Domain\Phone\Phone\Color::FIELD_NAME,
//        ],
//        'product_id' => [
//            'group' => 'deal',
//            'alias' => 'external_id',
//        ],
//        'contract_type' => [
//            'group' => 'deal',
//            'alias' => 'type',
//        ],
//        'contract_length' => [
//            'group' => 'deal-payment',
//            'alias' => 'payment_term',
//        ],
//        'handset_price' => [
//            'group' => 'deal-payment',
//            'alias' => 'initial_cost',
//        ],
//        'price' => [
//            'group' => 'deal-payment',
//            'alias' => 'monthly_cost'
//        ],
//        'deeplink' => [
//            'group' => 'link',
//            'alias' => 'link',
//        ],
//        'mobile_network' => [
//            'group' => 'operator',
//            'alias' => 'name',
//        ],
//        "inclusive_minutes" => [
//            'group' => 'contract',
//            'alias' => 'minutes',
//        ],
//        'inclusive_texts' => [
//            'group' => 'contract',
//            'alias' => 'sms'
//        ],
//        'data_allowance' => [
//            'group' => 'contract',
//            'alias' => 'data'
//        ],
//        'stock_code' => [
//            'group' => 'deal',
//            'alias' => 'in_stock',
//        ]
//    ],
    /* PHONES SPECS */
    SpecsSeed::CONTEXT => [
        SpecsSeedFields::BRAND => [
            'group' => 'brand',
            'alias' => Brand::FIELD_NAME,
        ],
        SpecsSeedFields::MODEL => [
            'group' => 'phone_model',
            'alias' => PhoneModel::FIELD_NAME,
        ],
        SpecsSeedFields::MEMORY_CARD_SLOT => [
            'group' => 'performance',
            'alias' => Performance::FIELD_MEMORY_CARD_SLOT,
        ],
        SpecsSeedFields::CHIPSET => [
            'group' => 'performance',
            'alias' => Performance::FIELD_CHIPSET,
        ],
        SpecsSeedFields::BLUETOOTH => [
            'group' => 'network',
            'alias' => \App\Domain\Phone\Specs\Network::FIELD_BLUETOOTH_TYPE,
        ],
        SpecsSeedFields::WIFI => [
            'group' => 'network',
            'alias' => \App\Domain\Phone\Specs\Network::FIELD_WIFI_TYPE,
        ],
        SpecsSeedFields::GPS => [
            'group' => 'connection_spec',
            'alias' => Connection::FIELD_GPS_TYPE
        ],
        SpecsSeedFields::BATTERY_TYPE => [
            'group' => 'autonomy',
            'alias' => Autonomy::FIELD_BATTERY_TYPE
        ],
        SpecsSeedFields::DISPLAY_SIZE => [
            'group' => 'display',
            'alias' => Display::FIELD_DISPLAY_SIZE
        ],
        SpecsSeedFields::DISPLAY_TYPE => [
            'group' => 'display',
            'alias' => Display::FIELD_DISPLAY_TYPE
        ],
    ],

    /* IMAGES */
    PhoneImageUploadImport::CONTEXT => [
        PhoneImageUploadImportFields::NAME => [
            'group' => 'phone',
            'alias' => Phone::FIELD_CHECK_HASH,
        ],
        PhoneImageUploadImportFields::PATH => [
            'group' => 'image',
            'alias' => Image::FIELD_PATH,
        ]
    ],
    BrandImageUploadImport::CONTEXT => [
        BrandImageUploadImportFields::NAME => [
            'group' => 'brand',
            'alias' => Brand::FIELD_SLUG,
        ],
        BrandImageUploadImportFields::PATH => [
            'group' => 'image',
            'alias' => Image::FIELD_PATH,
        ]
    ],
    NetworkImageUploadImport::CONTEXT => [
        NetworkImageUploadImportFields::NAME => [
            'group' => 'network',
            'alias' => Network::FIELD_SLUG,
        ],
        NetworkImageUploadImportFields::PATH => [
            'group' => 'image',
            'alias' => Image::FIELD_PATH,
        ]
    ],

    /* RATING */
    StoreRatingSeed::CONTEXT => [
        StoreRatingSeedFields::STORE_SLUG => [
            'group' => 'store',
            'alias' => Store::FIELD_SLUG,
        ],
        StoreRatingSeedFields::RATING_VALUE => [
            'group' => 'rating',
            'alias' => Rating::FIELD_RATING,
        ],
        StoreRatingSeedFields::URL => [
            'group' => 'link',
            'alias' => Link::FIELD_LINK
        ]
    ]
];
