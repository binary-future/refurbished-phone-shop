<?php

return [
    /*------------------------------------------------------------------------------------------------------------------
     * Here declared transformers schema.
     *------------------------------------------------------------------------------------------------------------------
     *
     * Variant 1
     *
     * 'alias' => [
     *     'base' => Transformers class name
     * ]
     *
     * Variant 2
     * 'alias' => [
     *      'base' => Transformers composite class name,
     *      'items' => ['transformer 1 class name', 'transformer 2 class name']
     * ]
     *__________________________________________________________________________________________________________________
     */
    'default' => [
        'base' => \App\Utils\Transformers\Transformer::class
    ],
    \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Utils\Transformers\Transformers\StoreImportTransformer::class,
            \App\Utils\Transformers\Transformers\ImageImportTransformer::class,
            \App\Utils\Transformers\Transformers\DescriptionImportTransformer::class,
            \App\Utils\Transformers\Transformers\LinkTransformer::class,
            \App\Utils\Transformers\Transformers\DatafeedInfoTransformer::class,
        ]
    ],
//    \App\Application\Services\Importer\Scenarios\PhonesImport::CONTEXT => [
//        'base' => \App\Utils\Transformers\Transformer::class,
//        'items' => [
//            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
//            \App\Utils\Transformers\Transformers\PhoneModelImportTransformer::class,
//            \App\Utils\Transformers\Transformers\Specs\RevglueSpecsImportTransformer::class,
//            \App\Utils\Transformers\Transformers\ColorImportTransformer::class,
//            \App\Utils\Transformers\Transformers\PhoneImportTransformer::class,
//            \App\Utils\Transformers\Transformers\ImageImportTransformer::class,
//        ]
//    ],
    \App\Application\Services\Importer\Scenarios\AwinPhonesImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Application\Services\Importer\Transformers\Preprocessors\AwinPreprocessor::class,
            \App\Utils\Transformers\Transformers\NetworkTransformer::class,
            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
            \App\Utils\Transformers\Transformers\PhoneModelImportTransformer::class,
            \App\Utils\Transformers\Transformers\ColorImportTransformer::class,
            \App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer::class,
            \App\Utils\Transformers\Transformers\ImageImportTransformer::class,
        ]
    ],
    \App\Application\Services\Importer\Scenarios\SpecsSeed::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
            \App\Utils\Transformers\Transformers\PhoneModelImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\RevglueSpecsImportTransformer::class,
        ]
    ],
    \App\Application\Services\Importer\Scenarios\SpecsDumpImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
            \App\Utils\Transformers\Transformers\PhoneModelImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\RevglueSpecsImportTransformer::class,
        ]
    ],
//    \App\Application\Services\Importer\Scenarios\RevglueDealsImport::CONTEXT => [
//        'base' => \App\Utils\Transformers\Transformer::class,
//        'items' => [
//            \App\Utils\Transformers\Transformers\OperatorTransformer::class,
//            \App\Utils\Transformers\Transformers\ContractTransformer::class,
//            \App\Utils\Transformers\Transformers\RevgluePhoneTransformer::class,
//            \App\Utils\Transformers\Transformers\RevglueDealsTransformer::class,
//            \App\Utils\Transformers\Transformers\LinkWithNoAffiliationTransformer::class,
//        ]
//    ],
    \App\Application\Services\Importer\Scenarios\AwinDealsImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Application\Services\Importer\Transformers\Preprocessors\AwinPreprocessor::class,
            \App\Utils\Transformers\Transformers\NetworkTransformer::class,
            \App\Utils\Transformers\Transformers\Awin\AwinContractTransformer::class,
            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
            \App\Utils\Transformers\Transformers\PhoneModelImportTransformer::class,
            \App\Utils\Transformers\Transformers\ColorImportTransformer::class,
            \App\Utils\Transformers\Transformers\DescriptionImportTransformer::class,
            \App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer::class,
            \App\Utils\Transformers\Transformers\DealTransformer::class,
            \App\Utils\Transformers\Transformers\LinkTransformer::class,
        ]
    ],
    \App\Application\Services\Importer\Scenarios\EcrawlerDealsImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => []
    ],
//    \App\Application\Services\Importer\Scenarios\WebgainsDealsImport::CONTEXT => [
//        'base' => \App\Utils\Transformers\Transformer::class,
//        'items' => [
//            \App\Utils\Transformers\Transformers\Webgains\WebgainsDealsTransformer::class,
//            \App\Utils\Transformers\Transformers\OperatorTransformer::class,
//            \App\Utils\Transformers\Transformers\ContractTransformer::class,
//            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
//            \App\Utils\Transformers\Transformers\ColorImportTransformer::class,
//            \App\Utils\Transformers\Transformers\WebgainsPhoneModelTransformer::class,
//            \App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer::class,
//            \App\Utils\Transformers\Transformers\LinkTransformer::class,
//        ]
//    ],
//    \App\Application\Services\Importer\Scenarios\WebgainsAffordableDealsImport::CONTEXT => [
//        'base' => \App\Utils\Transformers\Transformer::class,
//        'items' => [
//            \App\Utils\Transformers\Transformers\OperatorTransformer::class,
//            \App\Utils\Transformers\Transformers\ContractTransformer::class,
//            \App\Utils\Transformers\Transformers\PhoneBrandImportTransformer::class,
//            \App\Utils\Transformers\Transformers\WebgainsAffordablePhoneModelTransformer::class,
//            \App\Utils\Transformers\Transformers\ColorImportTransformer::class,
//            \App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer::class,
//            \App\Utils\Transformers\Transformers\RevglueDealsTransformer::class,
//            \App\Utils\Transformers\Transformers\LinkTransformer::class,
//        ]
//    ],
    \App\Utils\Transformers\Transformers\Specs\RevglueSpecsImportTransformer::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Utils\Transformers\Transformers\Specs\AutonomyImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\CameraImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\ConnectionImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\DisplayImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\NetworkImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\OperatingSystemImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\PerformanceImportTransformer::class,
            \App\Utils\Transformers\Transformers\Specs\PhoneCaseImportTransformer::class,
        ]
    ],
    \App\Application\Services\Importer\Scenarios\PhoneImageUploadImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformers\PhonesImageImportTransformer::class,
    ],
    \App\Application\Services\Importer\Scenarios\BrandImageUploadImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformers\BrandImageImportTransformer::class,
    ],
    \App\Application\Services\Importer\Scenarios\NetworkImageUploadImport::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformers\NetworkImageImportTransformer::class,
    ],
    \App\Application\Services\Importer\Scenarios\StoreRatingSeed::CONTEXT => [
        'base' => \App\Utils\Transformers\Transformer::class,
        'items' => [
            \App\Utils\Transformers\Transformers\Store\StoreAliasTransformer::class,
            \App\Utils\Transformers\Transformers\LinkTransformer::class,
            \App\Utils\Transformers\Transformers\RatingImportTransformer::class,
        ]
    ]
];
