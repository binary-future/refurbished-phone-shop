<?php

return [

    /*------------------------------------------------------------------------------------------------------------------
     * Here declared specifications schema.
     *------------------------------------------------------------------------------------------------------------------
     *
     * Variant 1
     *
     * 'alias' => [
     *     'base' => Specifications class name
     * ]
     *
     * Variant 2
     * 'alias' => [
     *      'base' => Specification composite class name,
     *      'items' => ['specification 1 class name', 'specification 2 class name']
     * ]
     *__________________________________________________________________________________________________________________
     */
    'default' => [
        'base' => \App\Utils\Specification\Specification::class
    ],
    \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT => [
        'base' => \App\Application\Services\Importer\Specifications\StoreImportSpecification::class
    ],
//    \App\Application\Services\Importer\Scenarios\PhonesImport::CONTEXT => [
//        'base' => \App\Application\Services\Importer\Specifications\PhonesImportSpecification::class
//    ],
    \App\Application\Services\Importer\Scenarios\AwinPhonesImport::CONTEXT => [
        'base' => \App\Application\Services\Importer\Specifications\Awin\AwinPhonesImportSpecification::class
    ],
//    \App\Application\Services\Importer\Scenarios\RevglueDealsImport::CONTEXT => [
//        'base' => \App\Application\Services\Importer\Specifications\RevglueDealsImportSpecification::class
//    ],
    \App\Application\Services\Importer\Scenarios\AwinDealsImport::CONTEXT => [
        'base' => \App\Application\Services\Importer\Specifications\Awin\AwinDealsImportSpecification::class
    ],
//    \App\Application\Services\Importer\Scenarios\WebgainsDealsImport::CONTEXT => [
//        'base' => \App\Application\Services\Importer\Specifications\WebgainsDealsImportSpecification::class
//    ],
//    \App\Application\Services\Importer\Scenarios\WebgainsAffordableDealsImport::CONTEXT => [
//        'base' => \App\Application\Services\Importer\Specifications\WebgainsDealsImportSpecification::class
//    ],
    \App\Application\Services\Importer\Scenarios\SpecsSeed::CONTEXT => [
        'base' => \App\Application\Services\Importer\Specifications\SpecsSeedSpecification::class,
    ],
    \App\Application\Services\Importer\Scenarios\SpecsDumpImport::CONTEXT => [
        'base' => \App\Application\Services\Importer\Specifications\SpecsSeedSpecification::class,
    ],
    \App\Application\Services\Specifications\UserManageSpecification::SPECIFICATION_ALIAS => [
        'base' => \App\Utils\Specification\Specification::class,
        'items' => [
            \App\Domain\User\Specifications\IsNotHimself::class,
            \App\Domain\User\Specifications\IsNotSuperAdmin::class
        ],
    ],
];
