<?php

return [
    'path' => sprintf('%s/wp-content/uploads', env('BLOG_DOMAIN')),
    'image_path' => sprintf('%s/wp-content/uploads', env('BLOG_CDN_DOMAIN') ?? env('BLOG_DOMAIN')),
];
