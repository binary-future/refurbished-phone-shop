<?php

/*______________________________________________________________________________________________________________________
 *
 * Criteria list divided by the contexts
 *
 * 'context' => [
 *     'criteria alias' => 'Criteria class'
 * ]
 *----------------------------------------------------------------------------------------------------------------------
 */

return [
    /*------------------------------------------------------------------------------------------------------------------
     *                                              COMMON
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\CriteriaFactory::DEFAULT_CONTEXT => [
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::WITH =>
            \App\Utils\Repository\Criteria\Common\With::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::ALL =>
            \App\Utils\Repository\Criteria\Common\All::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::ID =>
            \App\Utils\Repository\Criteria\Common\Id::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::IDS =>
            \App\Utils\Repository\Criteria\Common\Ids::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::ORDER_BY_DESC =>
            \App\Utils\Repository\Criteria\Common\OrderByDesc::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::ORDER_BY =>
            \App\Utils\Repository\Criteria\Common\OrderBy::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::GROUP_BY =>
            \App\Utils\Repository\Criteria\Common\GroupBy::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::LIMIT =>
            \App\Utils\Repository\Criteria\Common\Limit::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::OFFSET =>
            \App\Utils\Repository\Criteria\Common\Offset::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::WITH_COUNT =>
            \App\Utils\Repository\Criteria\Common\WithCount::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::COUNT_COMPARE_WITH_ZERO =>
            \App\Utils\Repository\Criteria\Common\CountCompareWithZero::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::SELECT =>
            \App\Utils\Repository\Criteria\Common\Select::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::UNIQUE =>
            \App\Utils\Repository\Criteria\Common\Unique::class,
        \App\Domain\Common\Contracts\Repository\CriteriaDictionary::SELECT_MAX =>
            \App\Utils\Repository\Criteria\Common\SelectMax::class,


    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              STORES
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\StoreCriteriaFactory::CONTEXT => [
        \App\Domain\Store\Repository\StoresCriteriaDictionary::CRITERIA_ALIAS =>
            \App\Utils\Repository\Criteria\Store\ByAlias::class,
        \App\Domain\Store\Repository\StoresCriteriaDictionary::CRITERIA_ACTIVE =>
            \App\Utils\Repository\Criteria\Store\IsActive::class,
        \App\Domain\Store\Repository\StoresCriteriaDictionary::CRITERIA_SLUG =>
            \App\Utils\Repository\Criteria\Store\BySlug::class,
        \App\Domain\Store\Repository\StoresCriteriaDictionary::HAS_LOCAL_IMAGE =>
            \App\Utils\Repository\Criteria\Store\ByLocalImage::class,
    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              PAYMENT METHODS
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\PaymentMethodCriteriaFactory::CONTEXT => [
        \App\Domain\Store\PaymentMethod\Repository\PaymentMethodsCriteriaDictionary::CRITERIA_SLUG =>
            \App\Utils\Repository\Criteria\PaymentMethod\BySlug::class,
    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              BRANDS
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\BrandCriteriaFactory::CONTEXT => [
        \App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary::CRITERIA_ALIAS =>
            \App\Utils\Repository\Criteria\Brand\ByAlias::class,
        \App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary::CRITERIA_BY_SLUG =>
            \App\Utils\Repository\Criteria\Brand\BySlug::class,
        \App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary::CRITERIA_BY_ID =>
            \App\Utils\Repository\Criteria\Brand\ById::class,
        \App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary::CRITERIA_BY_ACTIVE_MODELS =>
            \App\Utils\Repository\Criteria\Brand\ByActiveModels::class,
    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              PHONE MODELS
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\PhoneModelCriteriaFactory::CONTEXT => [
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_ALIAS =>
            \App\Utils\Repository\Criteria\PhoneModel\ByAlias::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_ALIAS_OR_SLUG =>
            \App\Utils\Repository\Criteria\PhoneModel\ByAliasOrSlug::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_BRAND_ID =>
            \App\Utils\Repository\Criteria\PhoneModel\ByBrandId::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_BRAND_IDS =>
            \App\Utils\Repository\Criteria\PhoneModel\ByBrandIds::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_OR_BRAND_IDS =>
            \App\Utils\Repository\Criteria\PhoneModel\OrBrandIds::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_ACTIVE_OR_BRAND_IDS =>
            \App\Utils\Repository\Criteria\PhoneModel\ActiveOrBrandIds::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_SLUG =>
            \App\Utils\Repository\Criteria\PhoneModel\BySlug::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_NAME_LIKE =>
            \App\Utils\Repository\Criteria\PhoneModel\NameLike::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_EXCEPT_MODEL =>
            \App\Utils\Repository\Criteria\PhoneModel\ExceptModel::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_EXCEPT_MODELS =>
            \App\Utils\Repository\Criteria\PhoneModel\ExceptModels::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_EXCEPT_MODELS_BY_ID =>
            \App\Utils\Repository\Criteria\PhoneModel\ExceptModelsById::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_HAS_DEALS =>
            \App\Utils\Repository\Criteria\PhoneModel\HasDeals::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_SORT_BY_COSTLY_DEAL =>
            \App\Utils\Repository\Criteria\PhoneModel\SortByCoslyDeal::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_IS_ACTIVE =>
            \App\Utils\Repository\Criteria\PhoneModel\IsActive::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_IDS =>
            \App\Utils\Repository\Criteria\PhoneModel\ByIds::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_BY_BASE_MODEL_ID =>
            \App\Utils\Repository\Criteria\PhoneModel\ByBaseModelId::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_HAS_SYNONYMS =>
            \App\Utils\Repository\Criteria\PhoneModel\HasSynonyms::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_NOT_MODEL_SYNONYM =>
            \App\Utils\Repository\Criteria\PhoneModel\NotModelSynonym::class,
        \App\Domain\Phone\Model\Repository\ModelCriteriaDictionary::CRITERIA_CREATED_BEFORE =>
            \App\Utils\Repository\Criteria\PhoneModel\CreatedBefore::class,
    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              PHONE TOP MODELS
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\PhoneTopModelCriteriaFactory::CONTEXT => [
        \App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary::CRITERIA_BY_ORDER =>
            \App\Utils\Repository\Criteria\TopModel\ByOrder::class,
        \App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary::CRITERIA_BY_MODEL_ID =>
            \App\Utils\Repository\Criteria\TopModel\ByModelId::class,
        \App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary::CRITERIA_BY_SYNONYMS =>
            \App\Utils\Repository\Criteria\TopModel\BySynonyms::class,
    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              PHONE LATEST MODELS
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\PhoneLatestModelCriteriaFactory::CONTEXT => [
        App\Application\Services\Model\LatestModel\Repository\LatestModelCriteriaDictionary::CRITERIA_BY_MODEL_ID =>
            \App\Utils\Repository\Criteria\LatestModel\ByModelId::class,
        App\Application\Services\Model\LatestModel\Repository\LatestModelCriteriaDictionary::CRITERIA_BY_IS_LATEST =>
            \App\Utils\Repository\Criteria\LatestModel\ByLatest::class,
        App\Application\Services\Model\LatestModel\Repository\LatestModelCriteriaDictionary::CRITERIA_BY_BRAND_ID =>
            \App\Utils\Repository\Criteria\LatestModel\ByBrandId::class,
    ],

    /*------------------------------------------------------------------------------------------------------------------
     *                                              PHONE COLORS
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\ColorCriteriaFactory::CONTEXT => [
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_NAME =>
            \App\Utils\Repository\Criteria\Color\ByName::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_ALIAS =>
            \App\Utils\Repository\Criteria\Color\ByAlias::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_SLUG =>
            \App\Utils\Repository\Criteria\Color\BySlug::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_INACTIVE_MODELS_ONLY =>
            \App\Utils\Repository\Criteria\Color\InactiveModelsOnly::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_IDS =>
            \App\Utils\Repository\Criteria\Color\ByIds::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_PARENT_ID =>
            \App\Utils\Repository\Criteria\Color\ByParentId::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_HAS_SYNONYMS =>
            \App\Utils\Repository\Criteria\Color\HasSynonyms::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_LIKE_NAME =>
            \App\Utils\Repository\Criteria\Color\LikeName::class,
        \App\Domain\Phone\Phone\Repository\ColorCriteriaDictionary::CRITERIA_EXCEPT_ID =>
            \App\Utils\Repository\Criteria\Color\ExceptId::class
    ],
    /*------------------------------------------------------------------------------------------------------------------
     *                                              FAQ
     *__________________________________________________________________________________________________________________
     */
    \App\Utils\Repository\Criteria\FaqCriteriaFactory::CONTEXT => [
        \App\Domain\Phone\Faq\Repository\FaqsCriteriaDictionary::CRITERIA_BY_IDS =>
            \App\Utils\Repository\Criteria\Faq\ByIds::class,
        \App\Domain\Phone\Faq\Repository\FaqsCriteriaDictionary::CRITERIA_BY_MODEL_ID =>
            \App\Utils\Repository\Criteria\Faq\ByModelId::class,
    ],
];
