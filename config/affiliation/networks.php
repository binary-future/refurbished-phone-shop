<?php

return [
    \App\Utils\Affiliation\Factory::DEFAULT_GENERATOR => \App\Utils\Affiliation\Generators\SimpleGenerator::class,


    'networks' => [
        \App\Domain\Store\Affiliation\AffiliationDictionary::NONE
            => \App\Utils\Affiliation\Generators\SimpleGenerator::class,
        \App\Domain\Store\Affiliation\AffiliationDictionary::SKIMLINK
            => \App\Utils\Affiliation\Generators\SkimlinkGenerator::class,
    ]
];
