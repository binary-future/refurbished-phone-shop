<?php

return [
    \App\Utils\Affiliation\Generators\SkimlinkGenerator::class => [
        /*
        |--------------------------------------------------------------------------
        | Skimlinks Api Key
        |--------------------------------------------------------------------------
        |
        | This value is the api key for skimlinks.com api. This value is used when the
        | outgoing links need to be transformed for Skimlinks service usage.
        |
        */
        'key' => env('CUSTOM_SKIMLINKS_SECRET'),

        /*
        |--------------------------------------------------------------------------
        | Skimlinks Domain
        |--------------------------------------------------------------------------
        |
        | This value is the domain, that registered for Skimlinks service.
        |
        */
        'domain' => env('CUSTOM_SKIMLINKS_SUBDOMAIN'),

        /*
        |--------------------------------------------------------------------------
        | Skimlinks ID
        |--------------------------------------------------------------------------
        |
        | This value is used for link transformation as part of it.
        |
        */
        'id' => env('CUSTOM_SKIMLINKS_ID', 1),
    ],
];
