<?php

use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily', 'sentry'],
            'ignore_exceptions' => false,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'deals-delete' => [
            'driver' => 'stack',
            'channels' => ['deals-delete-daily', 'sentry'],
            'ignore_exceptions' => false,
        ],

        'deals-delete-daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/deals/delete.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'deals-import' => [
            'driver' => 'stack',
            'channels' => ['deals-import-daily', 'sentry'],
            'ignore_exceptions' => false,
        ],

        'deals-import-daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/deals/import.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'planned-import' => [
            'driver' => 'daily',
            'path' => storage_path('logs/planned/import.log'),
            'level' => 'debug',
            'days' => 14,
        ],
        'download-images' => [
            'driver' => 'daily',
            'path' => storage_path('logs/images/download.log'),
            'level' => 'debug',
            'days' => 14,
        ],
        'description-images' => [
            'driver' => 'daily',
            'path' => storage_path('logs/images/description-images.log'),
            'level' => 'debug',
            'days' => 14,
        ],
        'duplication-images' => [
            'driver' => 'daily',
            'path' => storage_path('logs/images/duplication-images.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'papertrail' => [
            'driver' => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'sentry' => [
            'driver' => 'sentry',
            'level'  => null, // The minimum monolog logging level at which this handler will be triggered
            // For example: `\Monolog\Logger::ERROR`
            'bubble' => true, // Whether the messages that are handled can bubble up the stack or not
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],
    ],

];
