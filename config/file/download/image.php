<?php

return [
    'path' => [
        \App\Domain\Phone\Phone\Phone::class => 'public/images/logos/mobiles',
        \App\Domain\Store\Store::class => 'public/images/logos/stores',
    ]
];
