<?php

return [

    'owners' => [
        \App\Domain\Store\Store::class => [
            'path' => 'public/images/logos/stores',
        ],
        \App\Domain\Store\PaymentMethod\PaymentMethod::class => [
            'path' => 'public/images/logos/payment-methods',
        ],
        \App\Domain\Deals\Contract\Network::class => [
            'path' => 'public/images/logos/networks',
        ],
        \App\Domain\Phone\Brand\Brand::class => [
            'path' => 'public/images/logos/brands',
        ],
        \App\Domain\Phone\Phone\Phone::class => [
            'path' => 'public/images/logos/mobiles',
            'remove_previous' => false,
            'original_name' => false,
        ]
    ],

    'description-image' => [
        'base_extension' => 'jpg',
        'storage_suffix' => 'storage/upload',
        'base_upload_path' => 'public/upload/descriptions',
    ]
];
