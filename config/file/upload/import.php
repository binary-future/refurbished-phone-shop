<?php

return [

    'scenarios' => [
        'phones' => [
            'images' => [
                'application/zip' => \App\Application\Services\Importer\Scenarios\PhoneImageUploadImport::class,
            ],
        ],
        'brands' => [
            'images' => [
                'application/zip' => \App\Application\Services\Importer\Scenarios\BrandImageUploadImport::class,
            ],
        ],
        'networks' => [
            'images' => [
                'application/zip' => \App\Application\Services\Importer\Scenarios\NetworkImageUploadImport::class,
            ],
        ],
    ],

    'configs' => [
        \App\Utils\Importer\Source\Upload\ZipUploadSource::class => [
            'upload_path' => 'temp/upload/zip',
            'export_path' => storage_path('app/temp/upload/zip'),
            'extensions' => ['jpg', 'jpeg', 'png', 'svg']
        ],
    ],
];
