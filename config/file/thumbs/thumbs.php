<?php

return [
    'sizes' => [
        [
            'height' => 125,
            'width' => 125
        ],
        [
            'height' => 300,
            'width' => 300
        ],
        [
            'height' => 500,
            'width' => 500
        ],
    ]
];
