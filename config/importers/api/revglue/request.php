<?php

return [
    'schema' => [
        \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT =>
            \App\Utils\Importer\Source\Providers\Revglue\Request\ContentIndependentGenerator::class,
//        \App\Application\Services\Importer\Scenarios\PhonesImport::CONTEXT =>
//            \App\Utils\Importer\Source\Providers\Revglue\Request\ContentIndependentGenerator::class,
        \App\Application\Services\Importer\Scenarios\RevglueDealsImport::CONTEXT =>
            \App\Utils\Importer\Source\Providers\Revglue\Request\ContentDependentGenerator::class,
    ],

];
