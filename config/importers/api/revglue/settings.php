<?php

return [
    /*
     * Revglue api secret key
     */
    'key' => env('REVGLUE_KEY', 'IeThOdlQKCpzMiA6kZyIaHYUPMCGv8qWwvJsRkRIDE34VQVNey'),

    /*
     * Revglue api domain
     */
    'domain' => env('REVGLUE_DOMAIN', 'https://www.revglue.com/api'),

    /*
     * Content type
     */
    'type' => 'json',

    /*
     * Product type
     */
    'product' => [
        \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT => 'mobile_stores',
//        \App\Application\Services\Importer\Scenarios\PhonesImport::CONTEXT => 'mobiles',
        \App\Application\Services\Importer\Scenarios\RevglueDealsImport::CONTEXT => 'mobile_deals',
    ]
];
