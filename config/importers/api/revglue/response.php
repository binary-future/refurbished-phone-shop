<?php

return [
    'indexes' => [
        \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT => 'stores',
//        \App\Application\Services\Importer\Scenarios\PhonesImport::CONTEXT => 'mobiles',
        \App\Application\Services\Importer\Scenarios\RevglueDealsImport::CONTEXT => 'mobile_deals',
    ]
];
