<?php

return [
    'content' => [
        'base_dir' => 'datafeeds',
        'prefix' => 'app',
        'default_storage_context' => 'default',
        'zip_file' => 'datafeeds/%s/zip/datafeed.zip',
    ],

    'request' => [
        /*
         * AWIN api secret key
         */
        'key' => env('AWIN_DATAFEED_KEY'),

        /*
         * AWIN api domain
         */
        'domain' => env('AWIN_DATAFEED_DOMAIN', 'https://productdata.awin.com/datafeed/download'),

        /*
         * Content type
         */
        'type' => 'csv/delimiter/%2C',

        'compression' => 'zip',
    ]
];
