<?php

return [
    'content' => [
        'base_dir' => 'datafeeds',
        'prefix' => 'app',
        'default_storage_context' => 'default',
        'zip_file' => 'datafeeds/%s/zip/datafeed.zip',
    ],

    'request' => [
        /*
         * AWIN api secret key
         */
        'key' => env('WEBGAINS_DATAFEED_KEY'),

        /*
         * AWIN api domain
         */
        'domain' => env('WEBGAINS_DATAFEED_DOMAIN', 'https://www.webgains.com/affiliates/datafeed.html'),

        /*
         * Content type
         */
        'format' => 'csv',
        'separator' => 'comma',
        'compression' => 'zip',
    ]
];
