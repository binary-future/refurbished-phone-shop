<?php

return [
    'list' => [
        \App\Application\Services\Importer\Scenarios\SpecsSeed::CONTEXT =>
            database_path('dumps/specs.json'),
        \App\Application\Services\Importer\Scenarios\SpecsDumpImport::CONTEXT =>
            database_path('dumps/mobile-specs.csv'),
        \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT =>
            database_path('dumps/stores.csv'),
        \App\Application\Services\Importer\Scenarios\StoreRatingSeed::CONTEXT =>
            database_path('dumps/rating.json'),
    ]
];
