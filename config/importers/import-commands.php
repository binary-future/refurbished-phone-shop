<?php

return [
    \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\StoreImportCommand::class,

//    \App\Application\Services\Importer\Scenarios\PhonesImport::CONTEXT =>
//        \App\Application\Services\Importer\Importer\Commands\PhonesImportCommand::class,

    \App\Application\Services\Importer\Scenarios\AwinPhonesImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\PhonesImportCommand::class,

    \App\Application\Services\Importer\Scenarios\AwinDealsImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\DealsImportCommand::class,

    \App\Application\Services\Importer\Scenarios\EcrawlerDealsImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\DealsImportCommand::class,

    \App\Application\Services\Importer\Scenarios\SpecsSeed::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\SpecsSeedCommand::class,

    \App\Application\Services\Importer\Scenarios\SpecsDumpImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\SpecsSeedCommand::class,

    \App\Application\Services\Importer\Scenarios\PhoneImageUploadImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\ImageImportCommand::class,

    \App\Application\Services\Importer\Scenarios\BrandImageUploadImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\ImageImportCommand::class,

    \App\Application\Services\Importer\Scenarios\NetworkImageUploadImport::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\ImageImportCommand::class,

    \App\Application\Services\Importer\Scenarios\StoreRatingSeed::CONTEXT =>
        \App\Application\Services\Importer\Importer\Commands\StoreRatingsImportCommand::class,
];
