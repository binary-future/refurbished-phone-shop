<?php

return [
    'schema' => [
        \App\Utils\Importer\Handlers\Factory::DEFAULT_SCENARIO => [
            'source' => \App\Utils\Importer\Source\Providers\Revglue\RevglueApiSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\StoreImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],

        /* STORES  */
        \App\Application\Services\Importer\Scenarios\StoreImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Dump\CSVDumpSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\StoreImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],

        /* PHONES  */
        \App\Application\Services\Importer\Scenarios\AwinPhonesImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Providers\Awin\AwinApiSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\PhonesImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],

        /* DEALS  */
//        \App\Application\Services\Importer\Scenarios\RevglueDealsImport::CONTEXT => [
//            'source' => \App\Utils\Importer\Source\Providers\Revglue\RevglueApiSource::class,
//            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
//            'importer' => \App\Application\Services\Importer\Importer\DealImporter::class,
//            'handler' => \App\Utils\Importer\Handlers\Handler::class
//        ],
        \App\Application\Services\Importer\Scenarios\AwinDealsImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Providers\Awin\AwinApiSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\DealImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],
        \App\Application\Services\Importer\Scenarios\EcrawlerDealsImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Providers\Ecrawler\EcrawlerApiSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\DealImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],
//        \App\Application\Services\Importer\Scenarios\WebgainsDealsImport::CONTEXT => [
//            'source' => \App\Utils\Importer\Source\Providers\Webgains\WebgainsApiSource::class,
//            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
//            'importer' => \App\Application\Services\Importer\Importer\DealImporter::class,
//            'handler' => \App\Utils\Importer\Handlers\Handler::class
//        ],
//        \App\Application\Services\Importer\Scenarios\WebgainsAffordableDealsImport::CONTEXT => [
//            'source' => \App\Utils\Importer\Source\Providers\Webgains\WebgainsApiSource::class,
//            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
//            'importer' => \App\Application\Services\Importer\Importer\DealImporter::class,
//            'handler' => \App\Utils\Importer\Handlers\Handler::class
//        ],

        /* PHONES SHARED */
        \App\Application\Services\Importer\Scenarios\SpecsSeed::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Dump\JsonDumpSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\SpecsSeedImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],
        \App\Application\Services\Importer\Scenarios\SpecsDumpImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Dump\CSVDumpSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\SpecsSeedImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],
        \App\Application\Services\Importer\Scenarios\StoreRatingSeed::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Dump\JsonDumpSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\StoreRatingImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],
        \App\Application\Services\Importer\Scenarios\PhoneImageUploadImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Upload\ZipUploadSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\PhoneImageImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],

        /* BRANDS SHARED  */
        \App\Application\Services\Importer\Scenarios\BrandImageUploadImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Upload\ZipUploadSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\BrandImageImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],

        /* NETWORKS SHARED  */
        \App\Application\Services\Importer\Scenarios\NetworkImageUploadImport::CONTEXT => [
            'source' => \App\Utils\Importer\Source\Upload\ZipUploadSource::class,
            'transformer' => \App\Application\Services\Importer\Transformers\StructureTransformer::class,
            'importer' => \App\Application\Services\Importer\Importer\NetworkImageImporter::class,
            'handler' => \App\Utils\Importer\Handlers\Handler::class
        ],
    ]
];
