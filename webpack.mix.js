const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/admin/admin.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css/main.css')
    .sass('resources/sass/admin/admin.scss', 'public/css')
    .minify('public/css/custom.css');

mix.copyDirectory('node_modules/tinymce/skins', 'public/js/skins');
mix.copy('node_modules/cookie-bar/cookiebar-latest.min.js', 'public/js/cookiebar-latest.min.js');

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
