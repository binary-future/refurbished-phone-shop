<?php

Route::prefix('/brands')->middleware(['auth'])->name('admin.brands.')->namespace('Phones\Brand')->group(function () {
    Route::get('/', 'BrandMain')->name('main');
    Route::get('/{brand}/edit', 'BrandEdit')->where('brand', '[0-9a-zA-Z-_]+')->name('edit');
    Route::post('{brand}/update', 'BrandUpdate')->where('brand', '[0-9a-zA-Z-_]+')->name('update');
});
