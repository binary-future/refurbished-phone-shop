<?php

use App\Presentation\Http\Controllers\Backend\Networks\NetworkEdit;
use App\Presentation\Http\Controllers\Backend\Networks\NetworkMain;
use App\Presentation\Http\Controllers\Backend\Networks\NetworkUpdate;
use Illuminate\Support\Facades\Route;

Route::prefix('/networks')->middleware(['auth', 'is.admin'])->name('admin.networks.')->namespace('')
    ->group(function () {
        Route::get('/', NetworkMain::class)->name('main');
        Route::get('/{network}/edit', NetworkEdit::class)
            ->where('network', '[0-9a-zA-Z-_]+')
            ->name('edit');
        Route::post('{network}/update', NetworkUpdate::class)
            ->where('network', '[0-9a-zA-Z-_]+')
            ->name('update');
    });
