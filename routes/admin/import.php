<?php

Route::prefix('/import')->middleware(['auth', 'is.admin'])->name('admin.import.')->namespace('Import')->group(function () {
    Route::prefix('/model')->name('model.')->namespace('Model')->group(function () {
        Route::get('/', 'Main')->name('main');
        Route::get('/inactive', 'Inactive')->name('inactive');
        Route::get('/without-deals', 'WithoutDeals')->name('without.deals');
    });

    Route::prefix('/planned')->name('planned.')->namespace('PlannedImport')->group(function () {
        Route::get('/', 'PlanList')->name('main');
        Route::post('/save', 'PlanSave')->name('save');
        Route::post('/{plan}/update', 'PlanUpdate')->where('plan', '[0-9]+')->name('update');
        Route::post('/{plan}/delete', 'PlanDelete')
            ->where('plan', '[0-9]+')
            ->name('delete');
    });
});
