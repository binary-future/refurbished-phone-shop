<?php

Route::prefix('/color')->middleware(['auth', 'is.admin'])->name('admin.colors.')->namespace('Phones\Color')->group(function () {
    Route::get('/', 'ColorMain')->name('main');
    Route::get('/{color}/edit', 'ColorEdit')->where('color', '[0-9a-zA-Z-_]+')->name('edit');
    Route::get('/{color}/synonyms', 'ColorSynonyms')->where('color', '[0-9a-zA-Z-_]+')->name('synonyms');
    Route::post('/{color}/synonyms/add', 'ColorAddSynonym')->where('color', '[0-9a-zA-Z-_]+')->name('synonyms.add');
    Route::post('/{color}/synonyms/remove', 'ColorRemoveSynonym')
        ->where('color', '[0-9a-zA-Z-_]+')
        ->name('synonyms.remove');
    Route::post('{color}/update', 'ColorUpdate')->where('color', '[0-9a-zA-Z-_]+')->name('update');
});
