<?php

use App\Presentation\Http\Controllers\Backend\PaymentMethods\PaymentMethodCreate;
use App\Presentation\Http\Controllers\Backend\PaymentMethods\PaymentMethodDelete;
use App\Presentation\Http\Controllers\Backend\PaymentMethods\PaymentMethodEdit;
use App\Presentation\Http\Controllers\Backend\PaymentMethods\PaymentMethodSave;
use App\Presentation\Http\Controllers\Backend\PaymentMethods\PaymentMethodsMain;
use App\Presentation\Http\Controllers\Backend\PaymentMethods\PaymentMethodUpdate;
use Illuminate\Support\Facades\Route;

Route::prefix('/payment-methods')->middleware(['auth'])->name('admin.payment-methods.')
    ->group(static function () {
        Route::get('/', PaymentMethodsMain::class)->name('main');
        Route::get('/create', PaymentMethodCreate::class)->name('create')->middleware(['is.admin']);
        Route::post('/save', PaymentMethodSave::class)->name('save')->middleware(['is.admin']);
        Route::get('/{payment_method}/edit', PaymentMethodEdit::class)->where('payment_method', '[0-9a-zA-Z-_]+')
            ->name('edit');
        Route::post('/{payment_method}/update', PaymentMethodUpdate::class)->where('payment_method', '[0-9a-zA-Z-_]+')
            ->name('update');
        Route::post('/{payment_method}/delete', PaymentMethodDelete::class)
        ->where('store', '[0-9a-zA-Z-_]+')
        ->name('delete')
        ->middleware(['is.admin']);
    });
