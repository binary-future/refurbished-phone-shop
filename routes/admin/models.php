<?php

Route::prefix('/models')->middleware(['auth'])->name('admin.models.')->namespace('Phones\Model')->group(function () {
    Route::get('/', 'ModelIndex')
        ->name('main');
    Route::get('/search', 'ModelSearch')
        ->name('search');
    Route::get('/autocomplete', 'Autocomplete')->name('autocomplete');
    Route::get('/old', 'OldModelsList')
        ->middleware(['is.admin'])
        ->name('old.list');
    Route::get('/{brand}', 'ModelList')
        ->where('brand', '[0-9a-zA-Z-_]+')
        ->name('list');
    Route::get('/{brand}/{model}', 'ModelView')
        ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
        ->name('view');
    Route::get('/{brand}/{model}/synonyms', 'ModelViewSynonyms')
        ->middleware(['is.admin'])
        ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
        ->name('view.synonyms');
    Route::post('/{brand}/{model}/synonyms/add', 'ModelAddSynonym')
        ->middleware(['is.admin'])
        ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
        ->name('synonyms.add');
    Route::post('/{brand}/{model}/synonyms/remove', 'ModelRemoveSynonym')
        ->middleware(['is.admin'])
        ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
        ->name('synonyms.remove');
    Route::get('/{brand}/{model}/edit', 'ModelEdit')
        ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
        ->name('edit');
    Route::post('/{brand}/{model}/update', 'ModelUpdate')
        ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
        ->name('update');
});
