<?php

Route::prefix('/users')->middleware(['auth', 'is.admin'])->name('admin.users.')->namespace('User\User')->group(function () {
    Route::get('/', 'UserIndex')->name('main');
    Route::get('create', 'UserCreate')->name('create');
    Route::post('save', 'UserSave')->name('save');
    Route::get('/{user}/edit', 'UserEdit')->where('user', '[0-9]+')->name('edit');
    Route::post('{user}/update', 'UserUpdate')->where('user', '[0-9]+')->name('update');
    Route::post('{user}/delete', 'UserDelete')->where('user', '[0-9]+')->name('delete');
});