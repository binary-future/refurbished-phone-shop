<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* GENERAL */

use App\Presentation\Services\Routes\Dictionaries\DealsRouteDict as DealsRD;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomePage')->name('home');
Route::get('/about', function () {
    return view('frontend.general.about');
})->name('about');

Route::get('/help', function () {
    return view('frontend.general.help');
})->name('help');
Route::get('/accessibility', function () {
    return view('frontend.general.accessibility');
})->name('accessibility');
Route::get('/terms', function () {
    return view('frontend.general.terms');
})->name('terms');
Route::get('/privacy', function () {
    return view('frontend.general.privacy');
})->name('privacy');
Route::get('/contact-us', function () {
    return view('frontend.general.contact');
})->name('contact.us');
Route::post('contact-us/send', 'General\ContactSend')->name('contact.us.send');

/* SITEMAP */
Route::get('/sitemap.xml', 'General\SitemapsRender')->name('sitemaps');

/* DEALS */
Route::get('/mobiles/deals/outlink/{' . DealsRD::DEAL_OUTLINK_PARAM_DEAL . '}', 'Deal\DealOutlink')
    ->where(DealsRD::DEAL_OUTLINK_PARAM_DEAL, '[0-9]+')
    ->name(DealsRD::DEAL_OUTLINK);
Route::get('/mobiles/deals/outlink/{' . DealsRD::DEAL_OUTLINK_GO_PARAM_DEAL . '}/go', 'Deal\DealOutlinkGo')
    ->where(DealsRD::DEAL_OUTLINK_GO_PARAM_DEAL, '[0-9]+')
    ->name(DealsRD::DEAL_OUTLINK_GO);
Route::get('/mobiles/deals/{deal}-popup', 'Deal\PhoneDealPopup')->where('deal', '[0-9]+')
    ->name('mobile.deal.popup');

/* STORES */
Route::prefix('/stores')->name('stores.')->namespace('Store')->group(function () {
    Route::get('/', 'StoreIndex')
        ->name('main');
    Route::get('/{store}', 'StoreView')
        ->where('store', '[0-9a-zA-Z-_]+')
        ->name('view');
});

/* SEARCH */
Route::prefix('/search')->name('search.')->namespace('Search')->group(function () {
    Route::get('/models', 'SearchModel')->name('model');
    Route::get('/autocomplete', 'AutocompleteModel')->name('autocomplete');
});

/* BRAND */
Route::get('/manufacturers', 'Brand\BrandIndex')
    ->name('manufacturers.main');
Route::get('/{brand}', 'Brand\BrandView')->where('brand', '[0-9a-zA-Z-_]+')
    ->name('manufacturers.view');
Route::get('/{brand}/deals-filter', 'Brand\BrandViewFilter')
    ->where(['brand' => '[0-9a-zA-Z-_]+'])
    ->name('manufacturers.view.filter');

/* MODEL */
Route::get('/{brand}/{model}-refurbished', 'Model\ModelView')
    ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
    ->name('models.view');
Route::get('/{brand}/{model}-refurbished-filter', 'Model\ModelDealsFilter')
    ->where(['brand' => '[0-9a-zA-Z-_]+', 'model' => '[0-9a-zA-Z-_]+'])
    ->name('models.view.filter');
