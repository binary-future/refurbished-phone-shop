<?php

use Illuminate\Database\Seeder;

class DatafeedApiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('datafeeds_api')->insert($this->getApi());
    }

    private function getApi()
    {
        $names = \App\Application\Services\Importer\Datafeeds\ApiDictionary::NAMES;
        $api = [];
        foreach ($names as $name) {
            $api[] = [
                \App\Application\Services\Importer\Datafeeds\DatafeedApi::FIELD_NAME => $name
            ];
        }

        return $api;
    }
}
