<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->insert($this->getUser());
    }

    private function getUser()
    {
        $user = [
            'email' => 'admin@example.com',
            'name' => 'Admin',
            'email_verified_at' => now(),
            'remember_token' => \Illuminate\Support\Str::random(10),
            \App\Domain\User\User::FIELD_ROLE_ID => 1
        ];

        $user['password'] = config('app.env') === 'production'
            ? bcrypt('U?J6S#n&@9LRbB*s')
            : bcrypt('secret');

        return [$user];
    }
}
