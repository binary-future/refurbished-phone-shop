<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('roles')->insert($this->getRoles());
    }

    private function getRoles()
    {
        $roles = [];
        foreach (\App\Domain\User\Role::ROLES as $title) {
            $roles[] = ['title' => $title];
        }

        return $roles;
    }
}
