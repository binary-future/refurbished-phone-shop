<?php


namespace Tests\Feature\UseCases\Backend\Phones\Models;


use App\Application\UseCases\Backend\Phones\Model\OldPhonesListCase;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Repository\Models;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class OldPhonesListCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var
     */
    private $models;

    /**
     * @var OldPhonesListCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->models = $this->getBaseMock(Models::class);
        $this->case = $this->app->make(OldPhonesListCase::class);
        $this->app->instance(Models::class, $this->models);
    }

    /**
     * @param Collection $models
     * @dataProvider validDataProvider
     */
    public function testExecution(Collection $models)
    {
        $this->models->method('findBy')->willReturn($models);
        $response = $this->case->execute();
        $this->assertEquals($models, $response->getModels());
    }

    public function validDataProvider()
    {
        return [
            [collect([])],
            [collect([new PhoneModel()])]
        ];
    }
}
