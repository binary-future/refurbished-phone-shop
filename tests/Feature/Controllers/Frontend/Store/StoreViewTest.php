<?php

namespace Tests\Feature\Controllers\Frontend\Store;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Repository\Phones;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use App\Presentation\Http\Controllers\Frontend\Store\StoreView;
use App\Presentation\Services\Translators\StorePhones\Translator;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreViewTest extends TestCase
{
    use BaseMock;

    /**
     * @var StoreView
     */
    private $controller;
    private $stores;
    private $phones;
    /**
     * @var Translator
     */
    private $translator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->stores = $this->getBaseMock(Stores::class);
        $this->phones = $this->getBaseMock(Phones::class);
        $this->app->instance(Stores::class, $this->stores);
        $this->app->instance(Phones::class, $this->phones);
        $this->controller = $this->app->make(StoreView::class);
        $this->translator = $this->app->make(Translator::class);
    }

    /**
     * @param Collection $stores
     * @param Collection $phones
     * @dataProvider validDataProvider
     */
    public function testExecution(Collection $stores, Collection $phones): void
    {
        $this->stores->method('findBy')->willReturn($stores);
        $this->phones->method('findBy')->willReturn($phones);

        $response = $this->controller->__invoke('store-slug')->getData();

        $this->assertEquals($stores->first(), $response['store']);
        $this->assertEquals($this->translator->translate($phones), $response['phones']);
    }

    /**
     * @param Collection $stores
     * @param Collection $phones
     * @dataProvider validDataProvider
     */
    public function testRender(Collection $stores, Collection $phones): void
    {
        $this->stores->method('findBy')->willReturn($stores);
        $this->phones->method('findBy')->willReturn($phones);

        $response = $this->controller->__invoke('store-slug')->render();

        $this->assertIsString($response);
    }

    public function validDataProvider(): array
    {
        $store = new Store([
            Store::FIELD_NAME => 'store',
            Store::FIELD_SLUG => 'store-slug',
        ]);
        $brand = new Brand([
            Brand::FIELD_NAME => 'brand',
            Brand::FIELD_SLUG => 'brand-slug',
        ]);
        $phoneModel = new PhoneModel([
            PhoneModel::FIELD_NAME => 'phoneModel',
            PhoneModel::FIELD_SLUG => 'phone-model-slug',
        ]);
        $phoneModel->id = 2;
        $phoneModel->setRelations([
            PhoneModel::RELATION_BRAND => $brand,
        ]);
        $color = new Color([
            Color::FIELD_SLUG => 'color',
            Color::FIELD_NAME => 'colorName',
        ]);
        $phone = new Phone([
            Phone::FIELD_MODEL_ID => $phoneModel->getKey()
        ]);
        $phone->id = 1;
        $phone->setRelations([
            Phone::RELATION_IMAGES => new Collection(),
            Phone::RELATION_MODEL => $phoneModel,
            Phone::RELATION_COLOR => $color,
        ]);
        return [
            [new Collection([$store]), new Collection([$phone])],
        ];
    }
}
