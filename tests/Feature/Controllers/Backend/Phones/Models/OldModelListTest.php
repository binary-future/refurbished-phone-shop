<?php

namespace Tests\Feature\Controllers\Backend\Phones\Models;

use App\Application\UseCases\Backend\Phones\Model\OldPhonesListCase;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Repository\Models;
use App\Presentation\Http\Controllers\Backend\Phones\Model\OldModelsList;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class OldModelListTest extends TestCase
{
    use BaseMock;

    /**
     * @var
     */
    private $models;

    /**
     * @var OldModelsList
     */
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();
        $this->models = $this->getBaseMock(Models::class);
        $this->controller = $this->app->make(OldModelsList::class);
        $this->app->instance(Models::class, $this->models);
    }

    /**
     * @param Collection $models
     * @dataProvider validDataProvider
     */
    public function testExecution(Collection $models)
    {
        $this->models->method('findBy')->willReturn($models);
        $response = $this->controller->__invoke()->getData();
        $this->assertEquals($models, $response['models']);
    }

    public function validDataProvider()
    {
        return [
            [collect([])],
            [collect([new PhoneModel()])]
        ];
    }
}
