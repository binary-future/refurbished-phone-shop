<?php


namespace Tests\Unit\Presentation\Services\Specification;


use App\Presentation\Services\Specification\ModelsGroupSpecification;
use Tests\Unit\Utils\Specification\Specifications\BaseSpecificationTest;

class ModelsGroupSpecificationTest extends BaseSpecificationTest
{
    protected function getSpecification()
    {
        return new ModelsGroupSpecification();
    }

    public function validValuesDataProvider()
    {
        return [
            [['brand' => 'apple', 'model' => 'iphone']],
            [['brand' => 'samsung', 'model' => 'galaxy']],
        ];
    }

    public function inValidValuesDataProvider()
    {
        return [
            [['brand' => 'apple', 'model' => 'iphone-6s']],
            [['brand' => 'apple', 'model' => 'ipad']],
            [[]],
        ];
    }

}