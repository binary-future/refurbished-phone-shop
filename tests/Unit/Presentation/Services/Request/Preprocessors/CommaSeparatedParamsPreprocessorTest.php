<?php

namespace Tests\Unit\Presentation\Services\Request\Preprocessors;

use App\Presentation\Services\Request\Preprocessors\CommaSeparatedParamsPreprocessor;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class CommaSeparatedParamsPreprocessorTest extends TestCase
{
    use BaseMock;

    private const PARAM = 'param';

    public function getPreprocessor(): CommaSeparatedParamsPreprocessor
    {
        return new CommaSeparatedParamsPreprocessor();
    }

    /**
     * @param string $data
     * @param array $expectedResult
     * @dataProvider validDataProvider
     */
    public function testValidProcessNetworks(string $data, array $expectedResult): void
    {
        $preprocessor = $this->getPreprocessor();

        $result = $preprocessor->process($data);

        $this->assertEquals($expectedResult, $result);
    }

    public function validDataProvider(): array
    {
        $data1 = 'param1,param2';
        $expectedResult1 = ['param1', 'param2'];

        $data2 = 'data';
        $expectedResult2 = [$data2];

        return [
            [$data1, $expectedResult1],
            [$data2, $expectedResult2],
        ];
    }
}
