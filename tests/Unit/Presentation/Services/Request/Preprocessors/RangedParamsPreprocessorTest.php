<?php


namespace Tests\Unit\Presentation\Services\Request\Preprocessors;


use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Presentation\Services\Request\Preprocessors\RangedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Request\RangedParamsRequest;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class RangedParamsPreprocessorTest extends TestCase
{
    use BaseMock;

    /**
     * @var RangedParamsPreprocessor
     */
    private $preprocessor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->preprocessor = new RangedParamsPreprocessor();
    }

    /**
     * @param array $params
     * @param array $rangeMap
     * @param RangedParamsRequest $result
     * @throws RequestPreprocessorException
     * @dataProvider processingDataProvider
     */
    public function testPreprocessing(
        array $params,
        array $rangeMap,
        RangedParamsRequest $result
    ) {
        $this->assertEquals(
            $result->getParams(),
            $this->preprocessor->process($params, $rangeMap)->getParams()
        );
    }

    public function processingDataProvider()
    {
        $paramsFirst = [
            'sort' => 'sort',
            'monthly_cost' => '0,55',
            'upfront_cost' => '20,55',
            'data' => '20,55',
        ];
        $result = new RangedParamsRequest([
            'sort' => 'sort',
            'monthly_cost_range' => [0,55],
            'upfront_cost_range' => [20,55],
            'data_range' => [20,55],
        ]);
        $rangeMap = [
            'monthly_cost' => 'monthly_cost_range',
            'upfront_cost' => 'upfront_cost_range',
            'data' => 'data_range',
        ];

        return [
            [$paramsFirst, $rangeMap, $result],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->preprocessor);
        parent::tearDown();
    }
}
