<?php


namespace Tests\Unit\Presentation\Services\Request\Preprocessors;


use App\Application\Services\Merge\Model\DTO\ParentModelId;
use App\Application\Services\Merge\Model\DTO\SynonymsIds;
use App\Application\Services\Merge\Model\MergeGroup;
use App\Application\UseCases\Backend\Merge\Tool\Models\Requests\MergeSynonymsRequest;
use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Presentation\Services\Request\Preprocessors\MergeSynonymsRequestPreprocessor;
use Tests\TestCase;

final class MergeSynonymsRequestPreprocessorTest extends TestCase
{
    /**
     * @var MergeSynonymsRequestPreprocessor
     */
    private $preprocessor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->preprocessor = new MergeSynonymsRequestPreprocessor();
    }

    protected function tearDown(): void
    {
        unset($this->preprocessor);
        parent::tearDown();
    }

    /**
     * @param array $data
     * @param MergeSynonymsRequest $request
     * @dataProvider validDataProvider
     */
    public function testPreprocessing(array $data, MergeSynonymsRequest $request)
    {
        $this->assertEquals($request, $this->preprocessor->process($data));
    }

    public function validDataProvider()
    {
        $data = [
            "models" => [
                [
                    "synonyms" => "[3,5,6,9,10,85,86,87,137,143,235,236,237,335,336,360]",
                    "parent" => "1"
                ],
            ],
        ];
        $parentId = new ParentModelId(1);
        $synonymsIds = new SynonymsIds([3,5,6,9,10,85,86,87,137,143,235,236,237,335,336,360]);
        $mergeGroup = new MergeGroup($parentId, $synonymsIds);

        return [
            [['models' => []], new MergeSynonymsRequest([])],
            [$data, new MergeSynonymsRequest([$mergeGroup])],
        ];
    }

    /**
     * @param array $data
     * @dataProvider invalidDataProvider
     */
    public function testThrowsExceptionIfInvalidData(array $data)
    {
        $this->expectException(RequestPreprocessorException::class);
        $this->preprocessor->process($data);
    }

    public function invalidDataProvider()
    {
        return [
            [[[]]],
            [['aasdsdd']],
        ];
    }
}
