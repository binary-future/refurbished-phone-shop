<?php

namespace Tests\Unit\Presentation\Services\Request\Preprocessors;

use App\Application\UseCases\Phones\Requests\ModelDealsFilterRequest;
use App\Presentation\Services\Request\Preprocessors\Contracts\RangedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Presentation\Services\Request\Preprocessors\ModelDealFilterPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Contracts\CommaSeparatedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Request\RangedParamsRequest;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelDealFilterPreprocessorTest extends TestCase
{
    use BaseMock;

    /**
     * @var ModelDealFilterPreprocessor
     */
    private $preprocessor;

    private $commaSeparatedParamsPreprocessor;

    private $serializer;
    private $rangedParamsPreprocessor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->commaSeparatedParamsPreprocessor = $this->getBaseMock(CommaSeparatedParamsPreprocessor::class);
        $this->rangedParamsPreprocessor = $this->getBaseMock(RangedParamsPreprocessor::class);
        $this->preprocessor = new ModelDealFilterPreprocessor(
            $this->serializer,
            $this->commaSeparatedParamsPreprocessor,
            $this->rangedParamsPreprocessor
        );
    }

    protected function tearDown(): void
    {
        unset(
            $this->serializer,
            $this->commaSeparatedParamsPreprocessor,
            $this->rangedPreprocessor,
            $this->preprocessor
        );
        parent::tearDown();
    }

    /**
     * @param array $params
     * @param string $brand
     * @param string $model
     * @param RangedParamsRequest $rangedParamsRequest
     * @param array $processedParams
     * @param ModelDealsFilterRequest $result
     * @throws RequestPreprocessorException
     * @dataProvider processingDataProvider
     */
    public function testPreprocessing(
        array $params,
        string $brand,
        string $model,
        RangedParamsRequest $rangedParamsRequest,
        array $processedParams,
        ModelDealsFilterRequest $result
    ) {
        $this->rangedParamsPreprocessor->method('process')->with($params)
            ->willReturn($rangedParamsRequest);
        $this->commaSeparatedParamsPreprocessor->expects($this->never())->method('process');
        $this->serializer
            ->expects($this->once())
            ->method('fromArray')
            ->with(ModelDealsFilterRequest::class, $processedParams)
            ->willReturn($result);

        $this->assertEquals(
            $result->getAdjustedFilters(),
            $this->preprocessor->process($params, $brand, $model)->getAdjustedFilters()
        );
    }

    /**
     * @param array $params
     * @param RangedParamsRequest $request
     * @param string $brand
     * @param string $model
     * @throws RequestPreprocessorException
     * @dataProvider serializerExceptionDataProvider
     */
    public function testIfSerializerThrowsException(
        array $params,
        RangedParamsRequest $request,
        string $brand,
        string $model
    ) {
        $this->serializer->method('fromArray')->will($this->throwException(new \Exception()));
        $this->rangedParamsPreprocessor->method('process')->willReturn($request);
        $this->commaSeparatedParamsPreprocessor->expects($this->never())->method('process');

        $this->expectException(RequestPreprocessorException::class);

        $this->preprocessor->process($params, $brand, $model);
    }

    public function serializerExceptionDataProvider()
    {
        $params = ['sort' => 'sort'];
        $request = new RangedParamsRequest($params);
        $model = 'model';
        $brand = 'brand';

        return [
            [$params, $request, $brand, $model]
        ];
    }

    public function processingDataProvider(): array
    {
        $model = 'model';
        $brand = 'brand';
        $request = new ModelDealsFilterRequest();
        $request->setModel($model);
        $request->setBrand($brand);
        $paramsFirst = [
            'sort' => 'sort',
            'monthly_cost' => '20,55',
            'total_cost' => '20,55',
        ];
        $processedRangedParamsFirst = [
            'sort' => 'sort',
            'monthly_cost_range' => [20,55],
            'total_cost_range' => [20,55],
        ];
        $rangedParamsRequestFirst = new RangedParamsRequest($processedRangedParamsFirst);
        $processedParamsFirst = array_merge($processedRangedParamsFirst, [
            'sort' => 'sort',
            'brand' => 'brand',
            'model' => 'model',
            'monthly_cost_range' => [20,55],
            'total_cost_range' => [20,55],
            'networks' => [],
            'conditions' => [],
        ]);
        $requestFirst = clone $request;
        $requestFirst->setSort('sort');
        $requestFirst->setMonthlyCostRange([20, 55]);
        $requestFirst->setTotalCostRange([20, 55]);
        $paramsSecond = [
            'sort' => 'sort',
            'monthly_cost' => '20,20',
            'total_cost' => '20,20',
        ];
        $processedRangedParamsSecond = [
            'sort' => 'sort',
            'monthly_cost_range' => [20,20],
            'total_cost_range' => [20,20],
        ];
        $rangedParamsRequestSecond = new RangedParamsRequest($processedRangedParamsSecond);
        $processedParamsSecond = array_merge($processedRangedParamsSecond, [
            'sort' => 'sort',
            'brand' => 'brand',
            'model' => 'model',
            'monthly_cost_range' => [20,20],
            'total_cost_range' => [20,20],
            'networks' => [],
            'conditions' => [],
        ]);
        $requestSecond = clone $request;
        $requestSecond->setSort('sort');
        $requestSecond->setMonthlyCostRange([20,20]);
        $requestSecond->setTotalCostRange([20,20]);
        $paramsThird = [
            'sort' => 'sort',
            'monthly_cost' => '55',
            'total_cost' => '55',
        ];
        $processedRangedParamsThird = [
            'sort' => 'sort',
            'monthly_cost_range' => [55,55],
            'total_cost_range' => [55,55],
        ];
        $rangedParamsRequestThird = new RangedParamsRequest($processedRangedParamsThird);
        $processedParamsThird = array_merge($processedRangedParamsThird, [
            'sort' => 'sort',
            'brand' => 'brand',
            'model' => 'model',
            'monthly_cost_range' => [55,55],
            'total_cost_range' => [55,55],
            'networks' => [],
            'conditions' => [],
        ]);
        $requestThird = clone($request);
        $requestThird->setMonthlyCostRange([55, 55]);
        $requestThird->setTotalCostRange([55, 55]);
        $requestThird->setSort('sort');

        return [
            [$paramsFirst, $brand, $model, $rangedParamsRequestFirst, $processedParamsFirst, $requestFirst],
            [$paramsSecond, $brand, $model, $rangedParamsRequestSecond, $processedParamsSecond, $requestSecond],
            [$paramsThird, $brand, $model, $rangedParamsRequestThird, $processedParamsThird, $requestThird],
        ];
    }
}
