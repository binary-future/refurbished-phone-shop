<?php

namespace Tests\Unit\Presentation\Services\Request\Preprocessors;

use App\Application\UseCases\Phones\Requests\BrandDealsFilterRequest;
use App\Presentation\Services\Request\Preprocessors\BrandDealFilterPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Contracts\CommaSeparatedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Contracts\RangedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Presentation\Services\Request\Preprocessors\Request\RangedParamsRequest;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class BrandDealFilterPreprocessorTest extends TestCase
{
    use BaseMock;

    /**
     * @var BrandDealFilterPreprocessor
     */
    private $preprocessor;

    private $commaSeparatedParamsPreprocessor;

    private $serializer;
    private $rangedPreprocessor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->commaSeparatedParamsPreprocessor = $this->getBaseMock(CommaSeparatedParamsPreprocessor::class);
        $this->rangedPreprocessor = $this->getBaseMock(RangedParamsPreprocessor::class);
        $this->preprocessor = new BrandDealFilterPreprocessor(
            $this->serializer,
            $this->commaSeparatedParamsPreprocessor,
            $this->rangedPreprocessor
        );
    }

    protected function tearDown(): void
    {
        unset(
            $this->serializer,
            $this->commaSeparatedParamsPreprocessor,
            $this->rangedPreprocessor,
            $this->preprocessor
        );
        parent::tearDown();
    }

    /**
     * @param array $params
     * @param RangedParamsRequest $request
     * @param string $brand
     * @throws RequestPreprocessorException
     * @dataProvider serializerExceptionDataProvider
     */
    public function testIfSerializerThrowsException(
        array $params,
        RangedParamsRequest $request,
        string $brand
    ) {
        $this->serializer->method('fromArray')->will($this->throwException(new \Exception()));
        $this->commaSeparatedParamsPreprocessor->method('process')->willReturn($params);
        $this->rangedPreprocessor->method('process')->willReturn($request);

        $this->expectException(RequestPreprocessorException::class);

        $this->preprocessor->process($params, $brand);
    }

    public function serializerExceptionDataProvider()
    {
        $params = ['sort' => 'sort'];
        $request = new RangedParamsRequest($params);
        $brand = 'brand';

        return [
            [$params, $request, $brand]
        ];
    }

    /**
     * @param array $params
     * @param string $brand
     * @param RangedParamsRequest $rangedParamsRequest
     * @param array $processedParams
     * @param BrandDealsFilterRequest $result
     * @throws RequestPreprocessorException
     * @dataProvider processingDataProvider
     */
    public function testPreprocessing(
        array $params,
        string $brand,
        RangedParamsRequest $rangedParamsRequest,
        array $processedParams,
        BrandDealsFilterRequest $result
    ) {
        $this->rangedPreprocessor
            ->expects($this->once())
            ->method('process')->with($params)
            ->willReturn($rangedParamsRequest);
        if (isset($params['networks'])) {
            $this->commaSeparatedParamsPreprocessor
                ->expects($this->once())
                ->method('process')->with($params['networks'])
                ->willReturn($result->getNetworks());
        }
        $this->serializer
            ->expects($this->once())
            ->method('fromArray')
            ->with(BrandDealsFilterRequest::class, $processedParams)
            ->willReturn($result);

        $this->assertEquals($result, $this->preprocessor->process($params, $brand));
    }

    public function processingDataProvider()
    {
        $brand = 'brand';
        $request = new BrandDealsFilterRequest();
        $request->setBrand($brand);
        $paramsFirst = [
            'sort' => 'sort',
            'monthly_cost' => '0,55',
            'total_cost' => '20,55',
            'networks' => '1,2',
        ];
        $processedRangedParams = [
            'sort' => 'sort',
            'monthly_cost_range' => [0,55],
            'total_cost_range' => [20,55],
            'networks' => '1,2',
        ];
        $rangedParamsRequest = new RangedParamsRequest($processedRangedParams);
        $processedParamsFirst = array_merge($processedRangedParams, [
            'brand' => 'brand',
            'networks' => ['1','2'],
            'conditions' => [],
        ]);
        $requestFirst = clone $request;
        $requestFirst->setSort('sort');
        $requestFirst->setMonthlyCostRange([0, 55]);
        $requestFirst->setTotalCostRange([20,55]);
        $requestFirst->setNetworks(['1','2']);

        return [
            [$paramsFirst, $brand, $rangedParamsRequest, $processedParamsFirst, $requestFirst],
        ];
    }
}
