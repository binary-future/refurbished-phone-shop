<?php

namespace Tests\Unit\Presentation\Services\Generators;

use App\Presentation\Services\Generators\LastCheckedPhraseGenerator;
use Carbon\Carbon;
use Tests\TestCase;

final class LastCheckedPhraseGeneratorTest extends TestCase
{
    private function getGenerator()
    {
        return new LastCheckedPhraseGenerator();
    }

    /**
     * @dataProvider datesDataProvider
     * @param Carbon $date
     * @param string $phrase
     */
    public function testPhraseGenerator(Carbon $date, string $phrase)
    {
        $generator = $this->getGenerator();
        $result = $generator->generatePhrase($date);
        $this->assertEquals($phrase, $result);
    }

    /**
     *
     */
    public function datesDataProvider()
    {
        $subWeekDate = $this->getCurrentDate()->subWeeks(2);
        return [
            [$this->getCurrentDate()->subMinutes(30), 'a few minutes ago'],
            [$this->getCurrentDate()->subHour(), 'an hour ago'],
            [$this->getCurrentDate()->subHours(2), '2 hours ago'],
            [$this->getCurrentDate()->subDays(2), '3 days ago'],
            [$subWeekDate, $subWeekDate->toFormattedDateString()]
        ];
    }

    /**
     * @dataProvider datesDataProvider
     * @dataProvider datesWithFakesDataProvider
     * @param Carbon $date
     * @param string $phrase
     * @param int $moreThan
     */
    public function testFakePhraseGenerator(Carbon $date, string $phrase, int $moreThan = 30): void
    {
        $generator = $this->getGenerator();
        $result = $generator->generateFakePhrase($date, $moreThan);
        $this->assertEquals($phrase, $result);
    }

    public function datesWithFakesDataProvider(): array
    {
        return [
            [$this->getCurrentDate()->subDays(2), '3 days ago', 3],
            [$this->getCurrentDate()->subDays(2), 'an hour ago', 2]
        ];
    }

    private function getCurrentDate(): Carbon
    {
        return Carbon::now();
    }
}
