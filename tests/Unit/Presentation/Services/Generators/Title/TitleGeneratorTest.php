<?php

namespace Tests\Unit\Presentation\Services\Generators\Title;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\Contracts\AssignmentScenario;
use App\Presentation\Services\Generators\Title\Exceptions\TitleGeneratorException;
use App\Presentation\Services\Generators\Title\TitleConstructors\BrandAndModelNamesTitleConstructor;
use App\Presentation\Services\Generators\Title\TitleGenerator;
use App\Utils\Adapters\Config\Contracts\Config;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

class TitleGeneratorTest extends TestCase
{
    use BaseMock;

    private $config;
    private $assignmentScenario;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->assignmentScenario = $this->getBaseMock(AssignmentScenario::class);
    }

    /**
     * @dataProvider validDataProvider
     *
     * @param $brand
     * @param $model
     * @param $config
     * @param $key
     * @param $title
     */
    public function testPositiveGenerate($brand, $model, $config, $key, $title)
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($config);
        $this->assignmentScenario->expects($this->once())
            ->method('getKey')
            ->willReturn($key);

        $titleGenerator = new TitleGenerator($this->config, $this->assignmentScenario);
        $actualTitle = $titleGenerator->generate($brand, $model);

        $this->assertEquals($title, $actualTitle);
    }

    public function validDataProvider()
    {
        $brand = new Brand();
        $brand->setName('brand');
        $model = new PhoneModel();
        $model->setName('model');

        $config = [
            'key1' => [
                'template' => 'title1 %s %s',
                'constructor' => BrandAndModelNamesTitleConstructor::class
            ]
        ];

        return [
            [$brand, $model, $config, 'key1', 'title1 brand model']
        ];
    }

    /**
     * @dataProvider invalidDataProvider
     *
     * @param $brand
     * @param $model
     * @param $config
     * @param $key
     * @param $title
     */
    public function testNegativeGenerate($brand, $model, $config, $key)
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($config);
        $this->assignmentScenario->expects($this->once())
            ->method('getKey')
            ->willReturn($key);

        $this->expectException(TitleGeneratorException::class);

        $titleGenerator = new TitleGenerator($this->config, $this->assignmentScenario);
        $titleGenerator->generate($brand, $model);
    }

    public function invalidDataProvider(): array
    {
        $brand = new Brand();
        $brand->setName('brand');
        $model = new PhoneModel();
        $model->setName('model');

        $config = [
            'key1' => [
                'template' => null,
                'constructor' => BrandAndModelNamesTitleConstructor::class
            ],
            'key2' => [
                'template' => 'template %s %s %s',
                'constructor' => BrandAndModelNamesTitleConstructor::class
            ],
            'key3' => [
                'template' => 'template %s %s %s',
                'constructor' => null
            ],
            'key4' => null
        ];

        return [
            [$brand, $model, $config, 'key1'],
            [$brand, $model, $config, 'key2'],
            [$brand, $model, $config, 'key3'],
            [$brand, $model, $config, 'key4'],
            [$brand, $model, $config, 'null'],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->titleFactory);
        parent::tearDown();
    }


}
