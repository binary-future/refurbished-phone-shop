<?php


namespace Tests\Unit\Presentation\Services\Generators\Title\AssignmentScenarios;


use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\AssignmentScenarios\ByPhoneModelIdAssignmentScenario;
use PHPUnit\Framework\TestCase;

class ByPhoneModelIdAssignmentScenarioTest extends TestCase
{
    /**
     * @dataProvider validDataProvider
     *
     * @param array $titles
     * @param int $modelId
     * @param int $expectedTemplateKey
     */
    public function testGetKey(array $titles, int $modelId, int $expectedTemplateKey)
    {
        $model = new PhoneModel;
        $model->setAttribute('id', $modelId);

        $key = (new ByPhoneModelIdAssignmentScenario())
            ->getKey($titles, $model);

        $this->assertEquals($expectedTemplateKey, $key);
    }

    public function validDataProvider()
    {
        $titles = [
            [], [], [], [],
        ];
        return [
            [$titles, 1, 1],
            [$titles, 4, 0],
            [$titles, 5, 1],
        ];
    }
}
