<?php


namespace Tests\Unit\Presentation\Services\Generators\Title\TitleConstructors;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\TitleConstructors\BrandAndModelNamesTitleConstructor;
use PHPUnit\Framework\TestCase;

class BrandAndModelNamesTitleConstructorTest extends TestCase
{
    public function testExecute()
    {
        $brand = new Brand();
        $brand->setName('brand');
        $model = new PhoneModel();
        $model->setName('model');
        $template = 'title %s %s';
        $expectedTitle = sprintf($template, $brand->getName(), $model->getName());

        $title = (new BrandAndModelNamesTitleConstructor())
            ->execute($template, $brand, $model);

        $this->assertEquals($expectedTitle, $title);
    }
}
