<?php


namespace Tests\Unit\Presentation\Services\Generators\Title\TitleConstructors;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\TitleConstructors\ModelNameTitleConstructor;
use PHPUnit\Framework\TestCase;

class ModelNameTitleConstructorTest extends TestCase
{
    public function testExecute()
    {
        $brand = new Brand();
        $model = new PhoneModel();
        $model->setName('model');
        $template = 'title %s';
        $expectedTitle = sprintf($template, $model->getName());

        $title = (new ModelNameTitleConstructor())
            ->execute($template, $brand, $model);

        $this->assertEquals($expectedTitle, $title);
    }
}
