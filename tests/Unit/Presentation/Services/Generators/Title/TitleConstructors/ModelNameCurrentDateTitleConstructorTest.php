<?php


namespace Tests\Unit\Presentation\Services\Generators\Title\TitleConstructors;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\TitleConstructors\ModelNameCurrentDateTitleConstructor;
use PHPUnit\Framework\TestCase;

class ModelNameCurrentDateTitleConstructorTest extends TestCase
{
    public function testExecute()
    {
        $brand = new Brand([Brand::FIELD_NAME => 'brandname']);
        $model = new PhoneModel();
        $model->setName('model');
        $date = (new \DateTime())->format('M Y');
        $template = 'title %s %s';
        $expectedTitle = sprintf($template, $brand->getName(), $model->getName());

        $title = (new ModelNameCurrentDateTitleConstructor())
            ->execute($template, $brand, $model);

        $this->assertEquals($expectedTitle, $title);
    }
}
