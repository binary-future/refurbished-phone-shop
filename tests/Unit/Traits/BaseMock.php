<?php

namespace Tests\Unit\Traits;

trait BaseMock
{
    protected function getBaseMock(string $className)
    {
        $mock = $this->getMockBuilder($className)->disableOriginalConstructor()->getMock();

        return $mock;
    }
}
