<?php

namespace Tests\Unit\Utils\Serializer;

use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Encoders\Contracts\JsonEncoder;
use App\Utils\Serializer\Serializer;
use Illuminate\Contracts\Support\Arrayable;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class SerializerTest extends TestCase
{
    use BaseMock;

    private $container;

    private $jsonEncoder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->container = $this->getBaseMock(Container::class);
        $this->jsonEncoder = $this->getBaseMock(JsonEncoder::class);
    }

    private function getSerializer()
    {
        return new Serializer($this->container, $this->jsonEncoder);
    }

    public function testToArraySerialization()
    {
        $arrayable = $this->getBaseMock(Arrayable::class);
        $arrayable->expects($this->once())
            ->method('toArray')
            ->willReturn([]);

        $serializer = $this->getSerializer();
        $data = $serializer->toArray($arrayable);
        $this->assertEquals([], $data);
    }
}
