<?php

namespace Tests\Unit\Utils\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Factory;
use App\Utils\Transformers\Transformers\StructureByMapTransformer;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class FactoryTest extends TestCase
{
    use BaseMock;

    /**
     * @var Container|MockObject
     */
    private $container;

    /**
     * @var Config|MockObject
     */
    private $config;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->container = $this->getBaseMock(Container::class);
        $this->config = $this->getBaseMock(Config::class);
    }

    /**
     * @return Factory
     */
    private function getFactory()
    {
        return new Factory($this->container, $this->config);
    }

    /**
     * @param array $config
     * @param string $transformer
     * @dataProvider transformerListDataProvider
     */
    public function testFactoryBuildsTransformer(array $config, string $transformer)
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($config);
        $this->container->expects($this->once())
            ->method('resolve')
            ->willReturn($this->getBaseMock($transformer));
        $factory = $this->getFactory();
        $result = $factory->buildTransformer('alias');
        $this->assertInstanceOf($transformer, $result);
    }

    /**
     * @return array
     */
    public function transformerListDataProvider()
    {
        return [
            [
                ['alias' => ['base' => StructureByMapTransformer::class]], Transformer::class
            ],
            [
                ['default' => ['base' => StructureByMapTransformer::class]], Transformer::class
            ]
        ];
    }

    public function testFactoryThrowsExceptionIfCannotBuild()
    {
        $this->config->method('get')
            ->willReturn([]);

        $factory = $this->getFactory();
        $this->expectException(TransformerException::class);
        $factory->buildTransformer('alias');
    }
}
