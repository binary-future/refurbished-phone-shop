<?php


namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Transformers\BrandImageImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BrandImageImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    private function getTransformer()
    {
        return new BrandImageImportTransformer($this->serializer);
    }

    public function testTransforming()
    {
        $this->serializer->expects($this->any())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new Brand(), new Image()));
        $transformer = $this->getTransformer();
        $result = $transformer->transform(['brand' => ['slug' => 'hash'], 'image' => ['path' => 'path']]);
        $this->assertInstanceOf(Brand::class, $result['owner']);
        $this->assertInstanceOf(Image::class, $result['image']);
    }

    protected function tearDown(): void
    {
        unset($this->translator, $this->serializer);
        parent::tearDown();
    }
}
