<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Autonomy;
use App\Utils\Transformers\Transformers\Specs\AutonomyImportTransformer;

final class AutonomyImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return AutonomyImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return Autonomy::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['autonomy' => []],
                'autonomy'
            ],
            [
                [],
                'autonomy'
            ],
        ];
    }
}
