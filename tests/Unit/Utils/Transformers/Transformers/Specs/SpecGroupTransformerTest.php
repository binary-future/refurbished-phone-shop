<?php

namespace Tests\Unit\Utils\Transformers\Transformers\Specs;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Common\Contracts\Translators\Translator;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

abstract class SpecGroupTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $translator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->translator = $this->getBaseMock(Translator::class);
    }

    abstract protected function getTransformerName(): string;

    private function getTransformer(): Transformer
    {
        $transformer = $this->getTransformerName();

        return new $transformer($this->serializer, $this->translator);
    }

    abstract protected function getSpecClass(): string;

    /**
     * @param array $data
     * @param string $resultField
     * @dataProvider validDataProvider
     */
    public function testTransforming(array $data, string $resultField)
    {
        $spec = $this->getSpecClass();
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new $spec());

        $this->translator->expects($this->once())
            ->method('translate')
            ->willReturn([]);
        $transformer = $this->getTransformer();
        $result = $transformer->transform($data);
        $this->assertInstanceOf($this->getSpecClass(), $result[$resultField]);
    }

    abstract public function validDataProvider(): array;
}
