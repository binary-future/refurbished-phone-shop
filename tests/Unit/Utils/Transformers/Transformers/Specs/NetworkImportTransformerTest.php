<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Network;
use App\Utils\Transformers\Transformers\Specs\NetworkImportTransformer;

class NetworkImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return NetworkImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return Network::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['network' => []],
                'network'
            ],
            [
                [],
                'network'
            ],
        ];
    }

}