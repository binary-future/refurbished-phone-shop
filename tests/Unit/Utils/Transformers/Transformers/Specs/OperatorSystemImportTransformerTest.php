<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\OperatingSystem;
use App\Utils\Transformers\Transformers\Specs\OperatingSystemImportTransformer;

final class OperatorSystemImportTransformerTest  extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return OperatingSystemImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return OperatingSystem::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['operating' => []],
                'operating'
            ],
            [
                [],
                'operating'
            ],
        ];
    }
}