<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\PhoneCase;
use App\Utils\Transformers\Transformers\Specs\PhoneCaseImportTransformer;

class PhoneCaseImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return PhoneCaseImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return PhoneCase::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['phone_case' => []],
                'phone_case'
            ],
            [
                [],
                'phone_case'
            ],
        ];
    }
}
