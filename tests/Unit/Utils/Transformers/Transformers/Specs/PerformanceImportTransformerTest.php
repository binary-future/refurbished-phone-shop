<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;

use App\Domain\Phone\Specs\Performance;
use App\Utils\Transformers\Transformers\Specs\PerformanceImportTransformer;

class PerformanceImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return PerformanceImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return Performance::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['performance' => []],
                'performance'
            ],
            [
                [],
                'performance'
            ],
        ];
    }
}