<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Connection;
use App\Utils\Transformers\Transformers\Specs\ConnectionImportTransformer;

final class ConnectionImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return ConnectionImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return Connection::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['connection_spec' => []],
                'connection_spec'
            ],
            [
                [],
                'connection_spec'
            ],
        ];
    }
}