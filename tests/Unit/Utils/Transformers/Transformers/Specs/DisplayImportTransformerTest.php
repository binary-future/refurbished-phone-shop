<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Display;
use App\Utils\Transformers\Transformers\Specs\DisplayImportTransformer;

final class DisplayImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return DisplayImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return Display::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['display' => []],
                'display'
            ],
            [
                [],
                'display'
            ],
        ];
    }

}