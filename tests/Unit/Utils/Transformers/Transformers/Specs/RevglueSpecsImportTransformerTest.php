<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;


use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Specs\PhoneSpecs;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Factory;
use App\Utils\Transformers\Transformers\Specs\RevglueSpecsImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class RevglueSpecsImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $factory;

    private $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->factory = $this->getBaseMock(Factory::class);
        $this->transformer = $this->getBaseMock(Transformer::class);
    }

    private function getTransformer()
    {
        $this->factory->method('buildTransformer')
            ->willReturn($this->transformer);

        return new RevglueSpecsImportTransformer($this->serializer, $this->factory);
    }

    public function testTransforming()
    {
        $this->transformer->expects($this->once())->method('transform')->willReturn([]);
        $this->serializer->expects($this->once())->method('fromArray')->willReturn(new PhoneSpecs());
        $transformer = $this->getTransformer();
        $result = $transformer->transform([]);
        $this->assertInstanceOf(PhoneSpecs::class, $result['specs']);
    }

    public function testIfTransformerThrowsException()
    {
        $this->transformer->method('transform')
            ->will($this->throwException(new \Exception()));
        $transformer = $this->getTransformer();
        $result = $transformer->transform([]);
        $this->assertNull($result['specs']);
    }

    public function testIfSerializerThrowsException()
    {
        $this->transformer->expects($this->once())->method('transform')->willReturn([]);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->will($this->throwException(new \Exception()));
        $transformer = $this->getTransformer();
        $result = $transformer->transform([]);
        $this->assertNull($result['specs']);
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->factory, $this->transformer);
        parent::tearDown();
    }
}
