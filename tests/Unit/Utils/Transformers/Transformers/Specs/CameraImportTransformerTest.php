<?php


namespace Tests\Unit\Utils\Transformers\Transformers\Specs;

use App\Domain\Phone\Specs\Camera;
use App\Utils\Transformers\Transformers\Specs\CameraImportTransformer;

class CameraImportTransformerTest extends SpecGroupTransformerTest
{
    protected function getTransformerName(): string
    {
        return CameraImportTransformer::class;
    }

    protected function getSpecClass(): string
    {
        return Camera::class;
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['camera' => []],
                'camera'
            ],
            [
                [],
                'camera'
            ],
        ];
    }
}