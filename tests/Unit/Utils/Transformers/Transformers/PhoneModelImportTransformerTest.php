<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\PhoneModelImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PhoneModelImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $slugGenerator;

    private $aliasGenerator;

    private $specification;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->aliasGenerator = $this->getBaseMock(CheckAliasGenerator::class);
        $this->specification = $this->getBaseMock(Specification::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->serializer,
            $this->slugGenerator,
            $this->aliasGenerator,
            $this->specification,
        );
        parent::tearDown();
    }

    private function getTransformer(): PhoneModelImportTransformer
    {
        return new PhoneModelImportTransformer(
            $this->serializer,
            $this->slugGenerator,
            $this->aliasGenerator,
            $this->specification
        );
    }

    /**
     * @param array $data
     * @dataProvider transformDataProvider
     */
    public function testTransformingValidData(array $data): void
    {
        $this->slugGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('slug');
        $this->aliasGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('alias');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(PhoneModel::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(PhoneModel::class, $result['phone_model']);
    }

    public function testTransformerThrowsExceptionIfInvalidData(): void
    {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform([]);
    }

    /**
     * @param array $data
     * @dataProvider transformDataProvider
     */
    public function testTransformerThrowsExceptionIfFailedSpecification(array $data): void
    {
        $model = $this->getBaseMock(PhoneModel::class);
        $model->method('getName')
            ->willReturn('name');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($model);
        $this->slugGenerator->method('generate')
            ->willReturn('name');
        $this->aliasGenerator->method('generate')
            ->willReturn('alias');
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    public function transformDataProvider(): array
    {
        $brand = new Brand();
        $brand->setName('brand-name');
        $data = [
            'phone_model' => ['name' => 'name'],
            'brand' => $brand
        ];

        return [
            [$data]
        ];
    }
}
