<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Shared\Link\Link;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\LinkWithNoAffiliationTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class LinkWithNoAffiliationTransformerTest extends TestCase
{
    use BaseMock;

    private $transformer;

    /**
     * @var LinkWithNoAffiliationTransformer
     */
    private $testTransformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->getBaseMock(Transformer::class);
        $this->testTransformer = new LinkWithNoAffiliationTransformer($this->transformer);
    }

    protected function tearDown(): void
    {
        unset(
            $this->transformer,
            $this->testTransformer
        );
        parent::tearDown();
    }

    /**
     * @param array $data
     * @param array $transformedData
     * @param array $response
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, array $transformedData, array $response): void
    {
        $this->transformer->method('transform')->with($transformedData)->willReturn($response);

        $result = $this->testTransformer->transform($data);

        $this->assertEquals($response, $result);
    }

    public function validDataProvider(): array
    {
        $data = [
            'link' => [
                'link' =>
                    'http://www.awin1.com/cread.php?awinmid=5719&awinaffid=243515&clickref={subid-value}' .
                    '&p=http://giffgaff.com/huawei/huawei-p20/phone-plans?color=black&memory=128000&gb=BD030&condition='
            ]
        ];

        $dataSecond = [
            'link' => [
                'link' =>
                    'https://www.mobiles.co.uk/?&model=IP6SP128RO&tariff=JC1ZMMAR19&gift=NA&hs=O2MIP6SP128RO' .
                    '&affiliate=!!!refer!!!'
            ]
        ];
        $transformedData = [
            'link' => [
                'link' =>
                    'http://giffgaff.com/huawei/huawei-p20/phone-plans?color=black&memory=128000&gb=BD030&condition='
            ]
        ];

        $transformedDataSecond = [
            'link' => [
                'link' =>
                    'https://www.mobiles.co.uk/?&model=IP6SP128RO&tariff=JC1ZMMAR19&gift=NA&hs=O2MIP6SP128RO'
            ]
        ];

        return [
            [$data, $transformedData, ['link' => new Link()]],
            [$dataSecond, $transformedDataSecond, ['link' => new Link()]],
        ];
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = ['links' => ['data']];

        $this->expectException(TransformerException::class);

        $this->testTransformer->transform($data);
    }

    public function testTransformerThrowsExceptionIfIncludedTransformerFailed()
    {
        $data = [
            'link' => [
                'link' =>
                    'http://www.awin1.com/cread.php?awinmid=5719&awinaffid=243515&clickref={subid-value}' .
                    '&p=http://giffgaff.com/huawei/huawei-p20/phone-plans?color=black&memory=128000&gb=BD030&condition='
            ]
        ];
        $this->transformer->method('transform')->will($this->throwException(new \Exception()));

        $this->expectException(TransformerException::class);

        $this->testTransformer->transform($data);
    }
}
