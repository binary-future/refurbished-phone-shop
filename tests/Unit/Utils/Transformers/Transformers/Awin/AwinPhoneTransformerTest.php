<?php

namespace Tests\Unit\Utils\Transformers\Transformers\Awin;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class AwinPhoneTransformerTest extends TestCase
{
    use BaseMock;

    private $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->getBaseMock(Transformer::class);
    }

    protected function tearDown(): void
    {
        unset($this->transformer);
        parent::tearDown();
    }

    private function getTransformer(): AwinPhoneTransformer
    {
        return new AwinPhoneTransformer($this->transformer);
    }

    /**
     * @param array $data
     * @param array $transformedData
     * @param array $response
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, array $transformedData, array $response): void
    {
        $this->transformer->expects($this->once())
            ->method('transform')
            ->with($transformedData)
            ->willReturn($response);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertEquals($response['phone'], $result['product']);
    }

    public function validDataProvider(): array
    {
        $data = ['product' => ['any' => 'values']];
        $changedData = $data;
        $changedData['phone'] = $changedData['product'];
        $response = ['phone' => []];

        return [
            [ $data, $changedData, $response ]
        ];
    }
}
