<?php

namespace Tests\Unit\Utils\Transformers\Transformers\Awin;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Contract;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\Awin\AwinContractTransformer;
use App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class AwinContractTransformerTest extends TestCase
{
    use BaseMock;

    private $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->getBaseMock(Transformer::class);
    }

    protected function tearDown(): void
    {
        unset($this->transformer);
        parent::tearDown();
    }

    private function getTransformer(): AwinContractTransformer
    {
        return new AwinContractTransformer($this->transformer);
    }

    /**
     * @param array $data
     * @param array $transformedData
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, array $transformedData): void
    {
        $this->transformer->expects($this->once())
            ->method('transform')
            ->with($transformedData)
            ->willReturn(['contract' => new Contract()]);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Contract::class, $result['contract']);
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['contract' => ['condition' => 'Good Condition']],
                ['contract' => ['condition' => Condition::GOOD()]],
            ],
            [
                ['contract' => ['condition' => 'very good   condition']],
                ['contract' => ['condition' => Condition::VERY_GOOD()]],
            ],
            [
                ['contract' => ['condition' => 'PRISTINE condition']],
                ['contract' => ['condition' => Condition::PRISTINE()]],
            ],
        ];
    }

    /**
     * @param array $data
     * @dataProvider invalidDataProvider
     */
    public function testTransformInvalidData(array $data): void
    {
        $this->transformer->expects($this->never())
            ->method('transform');
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    public function invalidDataProvider(): array
    {
        return [
            [ ['nocontract'] ],
            [ ['contract' => ['nocondition']] ],
            [ ['contract' => ['condition' => 'best condition']] ],
        ];
    }
}
