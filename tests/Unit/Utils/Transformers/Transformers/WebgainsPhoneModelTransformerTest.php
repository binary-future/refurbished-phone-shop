<?php


namespace Tests\Unit\Utils\Transformers\Transformers;


use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Phone\Color;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\WebgainsPhoneModelTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class WebgainsPhoneModelTransformerTest extends TestCase
{
    use BaseMock;

    private $transformer;

    /**
     * @var WebgainsPhoneModelTransformer
     */
    private $testTransformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->getBaseMock(Transformer::class);
        $this->testTransformer = new WebgainsPhoneModelTransformer($this->transformer);
    }

    private function getTransformer()
    {
        return $this->testTransformer;
    }

    /**
     * @param array $data
     * @param array $transformedData
     * @param array $response
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, array $transformedData, array $response)
    {
        $this->transformer->expects($this->once())
            ->method('transform')
            ->with($transformedData)
            ->willReturn($response);
        $transformer = $this->getTransformer();
        $result = $transformer->transform($data);
        $this->assertEquals($response, $result);
    }

    public function validDataProvider()
    {
        $brand = new Brand();
        $brand->setName('Apple');
        $color = new Color();
        $color->setName('Red');
        $data = ['product' => ['name' => ' Apple iPhone 6s Plus 128GB']];
        $data['brand'] = $brand;
        $data['color'] = $color;
        $changedData = $data;
        $changedData['phone_model']['name'] = 'iPhone 6s Plus';
        $changedData['product'] = ['capacity' => '128GB'];

        return [
            [$data, $changedData, []]
        ];
    }

    public function testTransformerTrowsException()
    {
        $transformer = $this->getTransformer();
        $this->expectException(TransformerException::class);
        $transformer->transform([]);
    }

    protected function tearDown(): void
    {
        unset($this->transformer, $this->testTransformer);
        parent::tearDown();
    }
}
