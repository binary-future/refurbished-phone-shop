<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\ImageImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class ImageImportTransformerTest extends TestCase
{
    use BaseMock;

    private $specification;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->specification = $this->getBaseMock(Specification::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->specification,
            $this->serializer
        );
        parent::tearDown();
    }

    private function getTransformer(): ImageImportTransformer
    {
        return new ImageImportTransformer($this->serializer, $this->specification);
    }

    public function testTransformValidData(): void
    {
        $data = ['image' => ['data']];
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Image::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Image::class, $result['image']);
    }

    public function testTransformInvalidData(): void
    {
        $data = ['image' => ['data']];
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Image::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertNull($result['image']);
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = ['images' => ['data']];
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }
}
