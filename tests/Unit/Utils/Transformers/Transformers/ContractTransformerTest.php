<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Contract;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\ContractTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class ContractTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    protected function tearDown(): void
    {
        unset($this->serializer);
        parent::tearDown();
    }

    private function getTransformer(): ContractTransformer
    {
        return new ContractTransformer($this->serializer);
    }

    /**
     * @param array $data
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data): void
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new Contract());

        $transformer = $this->getTransformer();
        $result = $transformer->transform($data);

        $this->assertInstanceOf(Contract::class, $result['contract']);
    }

    public function validDataProvider(): array
    {
        return [
            [
                [
                    'contract' => [
                        'condition' => Condition::VERY_GOOD()
                    ],
                ],
            ],
            [
                [
                    'contract' => [
                        'condition' => Condition::GOOD()
                    ],
                ],
            ],
            [
                [
                    'contract' => [
                        'condition' => Condition::PRISTINE()
                    ],
                ],
            ],
        ];
    }

    /**
     * @param array $data
     * @dataProvider invalidDataProvider
     */
    public function testTransformerThrowsExceptionIfThereIsNoData(array $data): void
    {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }


    public function invalidDataProvider(): array
    {
        return [
            [
                [],
            ],
            [
                [
                    'contract' => []
                ],
            ],
            [
                [
                    'contract' => [
                        'condition' => 'bad value'
                    ],
                ],
            ],
        ];
    }
}
