<?php


namespace Tests\Unit\Utils\Transformers\Transformers;


use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Transformers\PhonesImageImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhonesImageImportTransformerTest extends TestCase
{
    use BaseMock;

    private $translator;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->translator = $this->getBaseMock(Translator::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    private function getTransformer()
    {
        return new PhonesImageImportTransformer($this->translator, $this->serializer);
    }

    public function testTransforming()
    {
        $this->translator->expects($this->once())
            ->method('translate')
            ->willReturn(['check_hash' => 'hash']);
        $this->serializer->expects($this->any())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new Phone(), new Image()));
        $transformer = $this->getTransformer();
        $result = $transformer->transform(['phone' => ['check_hash' => 'hash'], 'image' => ['path' => 'path']]);
        $this->assertInstanceOf(Phone::class, $result['owner']);
        $this->assertInstanceOf(Image::class, $result['image']);
    }

    protected function tearDown(): void
    {
        unset($this->translator, $this->serializer);
        parent::tearDown();
    }
}
