<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\StructureByMapTransformer;
use Tests\TestCase;

final class StructureByMapTransformerTest extends TestCase
{
    /**
     * @var StructureByMapTransformer
     */
    private $transformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = new StructureByMapTransformer();
    }

    /**
     * @param array $data
     * @param array $map
     * @param array $response
     * @dataProvider validDataProvider
     */
    public function testTransformingByMap(array $data, array $map, array $response)
    {
        $result = $this->transformer->transform($data, $map);

        $this->assertEquals($response, $result);
    }

    public function validDataProvider()
    {
        return [
            [
                ['brand_name' => 'aaa', 'model_name' => 'bbb', 'product_name' => 'ccc'],
                [
                    'brand_name' => [
                        'group' => 'brand',
                        'alias' => 'name'
                    ],
                    'model_name' => [
                        'group' => 'model',
                        'alias' => 'name'
                    ],
                    'model_image' => [
                        'group' => 'model',
                        'alias' => 'image'
                    ],
                ],
                ['brand' => ['name' => 'aaa'], 'model' => ['name' => 'bbb', 'image' => '']]
            ]
        ];
    }

    public function testTransformerNotChangeStructureInNoMapGiven()
    {
        $data = ['name' => 'test'];
        $result = $this->transformer->transform($data);

        $this->assertEquals($data, $result);
    }

    public function testTransformerThrowsExceptionIfInvalidData()
    {
        $data = 'hello';
        $this->expectException(TransformerException::class);
        $this->transformer->transform($data, ['test']);
    }
}
