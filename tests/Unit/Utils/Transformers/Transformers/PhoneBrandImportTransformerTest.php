<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Phone\Brand\Brand;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\PhoneBrandImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhoneBrandImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $slugGenerator;

    private $specification;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->specification = $this->getBaseMock(Specification::class);
    }

    private function getTransformer(): PhoneBrandImportTransformer
    {
        return new PhoneBrandImportTransformer($this->serializer, $this->slugGenerator, $this->specification);
    }

    public function testTransformingValidData(): void
    {
        $this->slugGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('slug');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Brand::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $transformer = $this->getTransformer();

        $result = $transformer->transform(['brand' => ['name' => 'name']]);

        $this->assertInstanceOf(Brand::class, $result['brand']);
    }

    public function testTransformerThrowsExceptionIfInvalidData(): void
    {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform([]);
    }

    public function testTransformerThrowsExceptionIfFailedSpecification(): void
    {
        $this->slugGenerator->method('generate')
            ->willReturn('name');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new Brand());
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform(['brand' => ['name' => 'name']]);
    }
}
