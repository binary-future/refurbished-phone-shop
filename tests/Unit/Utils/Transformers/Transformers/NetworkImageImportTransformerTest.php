<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Network;
use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Transformers\NetworkImageImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class NetworkImageImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    private function getTransformer()
    {
        return new NetworkImageImportTransformer($this->serializer);
    }

    public function testTransforming()
    {
        $this->serializer->expects($this->any())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new Network(), new Image()));
        $transformer = $this->getTransformer();
        $result = $transformer->transform(['network' => ['slug' => 'hash'], 'image' => ['path' => 'path']]);
        $this->assertInstanceOf(Network::class, $result['owner']);
        $this->assertInstanceOf(Image::class, $result['image']);
    }

    protected function tearDown(): void
    {
        unset($this->serializer);
        parent::tearDown();
    }
}
