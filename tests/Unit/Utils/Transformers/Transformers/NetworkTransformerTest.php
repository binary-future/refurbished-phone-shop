<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Network;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\NetworkTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class NetworkTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $slugGenerator;

    private $aliasGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->aliasGenerator = $this->getBaseMock(CheckAliasGenerator::class);
    }

    private function getTransformer(): NetworkTransformer
    {
        return new NetworkTransformer($this->serializer, $this->slugGenerator, $this->aliasGenerator);
    }

    public function testTransformValidData(): void
    {
        $data = ['network' => ['name' => 'name']];
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Network::class));
        $this->slugGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('slug');
        $this->aliasGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('alias');

        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Network::class, $result['network']);
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = ['network' => ['data']];
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }
}
