<?php


namespace Tests\Unit\Utils\Transformers\Transformers;


use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Brand\Brand;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\WebgainsAffordablePhoneModelTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class WebgainsAffordablePhoneModelTransformerTest extends TestCase
{
    use BaseMock;

    private $transformer;

    /**
     * @var WebgainsAffordablePhoneModelTransformer
     */
    private $testTransformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transformer = $this->getBaseMock(Transformer::class);
        $this->testTransformer = new WebgainsAffordablePhoneModelTransformer($this->transformer);
    }

    private function getTransformer()
    {
        return $this->testTransformer;
    }

    /**
     * @param array $data
     * @param array $transformedData
     * @param array $response
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, array $transformedData, array $response)
    {
        $this->transformer->expects($this->once())
            ->method('transform')
            ->with($transformedData)
            ->willReturn($response);
        $transformer = $this->getTransformer();
        $result = $transformer->transform($data);
        $this->assertEquals($response, $result);
    }

    public function validDataProvider()
    {
        $brand = new Brand();
        $brand->setName('Apple');
        $data = ['product' => ['name' => ' iPhone 6s 128GB Grey']];
        $data['brand'] = $brand;
        $changedData = $data;
        $changedData['phone_model']['name'] = 'iPhone 6s';
        $changedData['color']['name'] = 'Grey';
        $changedData['product'] = ['capacity' => '128GB'];

        return [
            [$data, $changedData, []]
        ];
    }

    /**
     * @param array $data
     * @dataProvider invalidDataProvider
     */
    public function testTransformerTrowsException(array $data)
    {
        $transformer = $this->getTransformer();
        $this->expectException(TransformerException::class);
        $transformer->transform($data);
    }

    public function invalidDataProvider()
    {
        $brand = new Brand();
        $brand->setName('Apple');
        $data = ['product' => ['name' => 'iPhone 6s Grey']];
        $data['brand'] = $brand;
        $dataWithNoColor = ['product' => ['name' => 'iPhone 6s 128GB']];
        $dataWithNoColor['brand'] = $brand;

        return [
            [[]],
            [$data],
            [$dataWithNoColor],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->transformer, $this->testTransformer);
        parent::tearDown();
    }
}
