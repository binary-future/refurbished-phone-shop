<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Store\Store;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\StoreImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreImportTransformerTest extends TestCase
{
    use BaseMock;

    public const GROUP_STORE = 'store';

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    protected function tearDown(): void
    {
        unset($this->serializer);
        parent::tearDown();
    }

    private function getTransformer(): StoreImportTransformer
    {
        return new StoreImportTransformer($this->serializer);
    }

    /**
     * @param array $data
     * @dataProvider validDataProvider
     */
    public function testSerialization(array $data): void
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new Store());
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Store::class, $result[self::GROUP_STORE]);
    }

    public function testTransformerThrowsExceptionIfInvalidData(): void
    {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform([]);
    }

    public function validDataProvider(): array
    {
        return [
            [
                [self::GROUP_STORE => [
                    Store::FIELD_NAME => 'Store',
                    Store::FIELD_SLUG => 'slug',
                    Store::FIELD_TERMS => '3'
                ]],
            ],
        ];
    }
}
