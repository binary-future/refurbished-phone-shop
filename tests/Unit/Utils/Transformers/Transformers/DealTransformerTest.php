<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\AwinDealsImport;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Payment;
use App\Domain\Store\Store;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\DealTransformer;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class DealTransformerTest extends TestCase
{
    use BaseMock;

    private const GROUP_DEAL = 'deal';
    private const GROUP_DEAL_PAYMENT = 'deal-payment';
    private const OPTION_SCENARIO_CONTENT = 'scenario_content';

    /**
     * @var Serializer|MockObject
     */
    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    protected function tearDown(): void
    {
        unset($this->serializer);
        parent::tearDown();
    }

    private function getTransformer(): DealTransformer
    {
        return new DealTransformer($this->serializer);
    }

    /**
     * @param array $data
     * @param array $options
     * @param array $expectedDealData
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, array $options, array $expectedDealData): void
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->with(Deal::class, $expectedDealData)
            ->willReturn(new Deal());
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data, $options);

        $this->assertInstanceOf(Deal::class, $result[self::GROUP_DEAL]);
    }

    public function validDataProvider(): array
    {
        $data = [
            self::GROUP_DEAL => [
                Deal::FIELD_EXTERNAL_ID => '7832765',
            ],
            self::GROUP_DEAL_PAYMENT => [
                Deal::FIELD_TOTAL_COST => '120.30',
            ]
        ];
        $store = new Store();
        $store->id = 1;
        $store->setTerms(3);
        $datafeed = new DatafeedInfo();
        $datafeed->setStoreId($store->getKey());
        $datafeed->setRelation(DatafeedInfo::RELATION_STORE, $store);
        $options = [self::OPTION_SCENARIO_CONTENT => $datafeed];

        $expectedDealData = [
            Deal::FIELD_EXTERNAL_ID => '7832765',
            Deal::OBJECT_PAYMENT => new Payment(0, 40.10, 120),
            Deal::FIELD_STORE_ID => $store->getKey()
        ];
        return [
            [$data, $options, $expectedDealData]
        ];
    }

    /**
     * @param array $invalidData
     * @param array $options
     * @dataProvider invalidDataProvider
     */
    public function testTransformerThrowsExceptionIfThereIsInvalidData(
        array $invalidData,
        array $options
    ): void {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($invalidData);
    }

    public function invalidDataProvider(): array
    {
        $store = new Store();
        $store->id = 1;
        $store->setTerms(3);
        $datafeed = new DatafeedInfo();
        $datafeed->setStoreId($store->getKey());
        $datafeed->setRelation(DatafeedInfo::RELATION_STORE, $store);
        $defaultOptions = [self::OPTION_SCENARIO_CONTENT => $datafeed];

        $datafeedWithoutStore = new DatafeedInfo();

        $emptyStore = new Store();
        $datafeedWithEmptyStore = new DatafeedInfo();
        $datafeedWithEmptyStore->setRelation(DatafeedInfo::RELATION_STORE, $emptyStore);

        $storeWithoutTerms = new Store();
        $storeWithoutTerms->id = 1;
        $datafeedWithStoreWithoutTerms = new DatafeedInfo();
        $datafeedWithStoreWithoutTerms->setStoreId($storeWithoutTerms->getKey());
        $datafeedWithStoreWithoutTerms->setRelation(DatafeedInfo::RELATION_STORE, $storeWithoutTerms);


        return [
            [
                [self::GROUP_DEAL => ['data']],
                $defaultOptions
            ],
            [
                [self::GROUP_DEAL_PAYMENT => []],
                $defaultOptions
            ],
            [
                [
                    self::GROUP_DEAL => [Deal::FIELD_EXTERNAL_ID => 'abc'],
                    self::GROUP_DEAL_PAYMENT => []
                ],
                $defaultOptions
            ],
            [
                [
                    self::GROUP_DEAL => [Deal::FIELD_EXTERNAL_ID => '125'],
                    self::GROUP_DEAL_PAYMENT => [Deal::FIELD_TOTAL_COST => '']
                ],
                $defaultOptions
            ],
            [
                [null],
                $defaultOptions
            ],
            [
                [
                    self::GROUP_DEAL => [Deal::FIELD_EXTERNAL_ID => '7832765'],
                    self::GROUP_DEAL_PAYMENT => [Deal::FIELD_TOTAL_COST => '120.30']
                ],
                [self::OPTION_SCENARIO_CONTENT => $datafeedWithoutStore],
            ],
            [
                [
                    self::GROUP_DEAL => [Deal::FIELD_EXTERNAL_ID => '7832765'],
                    self::GROUP_DEAL_PAYMENT => [Deal::FIELD_TOTAL_COST => '120.30']
                ],
                [self::OPTION_SCENARIO_CONTENT => $datafeedWithEmptyStore],
            ],
            [
                [
                    self::GROUP_DEAL => [Deal::FIELD_EXTERNAL_ID => '7832765'],
                    self::GROUP_DEAL_PAYMENT => [Deal::FIELD_TOTAL_COST => '120.30']
                ],
                [self::OPTION_SCENARIO_CONTENT => $datafeedWithStoreWithoutTerms],
            ]
        ];
    }
}
