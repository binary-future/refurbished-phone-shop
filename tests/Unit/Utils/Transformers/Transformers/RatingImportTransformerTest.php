<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Shared\Rating\Rating;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\RatingImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class RatingImportTransformerTest extends TestCase
{
    use BaseMock;

    private const GROUP_RATING = 'rating';
    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    private function getTransformer(): RatingImportTransformer
    {
        return new RatingImportTransformer($this->serializer);
    }

    /**
     * @param array $data
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data): void
    {
        $rating = new Rating();
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($rating);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertEquals($rating, $result[self::GROUP_RATING]);
    }

    public function validDataProvider(): array
    {
        return [
            [
                [self::GROUP_RATING => [Rating::FIELD_RATING => 50]]
            ]
        ];
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = [];
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    protected function tearDown(): void
    {
        unset($this->serializer);
        parent::tearDown();
    }
}
