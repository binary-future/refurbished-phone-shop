<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Network;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\ColorImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ColorImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $slugGenerator;

    private $aliasGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->aliasGenerator = $this->getBaseMock(CheckAliasGenerator::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->serializer,
            $this->slugGenerator,
            $this->aliasGenerator
        );
        parent::tearDown();
    }

    private function getTransformer(): ColorImportTransformer
    {
        return new ColorImportTransformer($this->serializer, $this->slugGenerator, $this->aliasGenerator);
    }

    /**
     * @param array $data
     * @param string $colorName
     * @dataProvider validDataProvider
     */
    public function testSerialization(array $data, string $colorName): void
    {
        $slug = 'slug';
        $alias = 'alias';

        $this->slugGenerator->expects($this->once())
            ->method('generate')
            ->willReturn($slug);
        $this->aliasGenerator->expects($this->once())
            ->method('generate')
            ->willReturn($alias);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->with(Color::class, [
                Color::FIELD_NAME => $colorName,
                Color::FIELD_SLUG => $slug,
                Color::FIELD_ALIAS => $alias
            ])
            ->willReturn(new Color());
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Color::class, $result['color']);
    }

    /**
     * @param array $data
     * @dataProvider invalidDataProvider
     */
    public function testSerializationThrowExceptionIfColorNotFound(array $data): void
    {
        $this->slugGenerator->expects($this->never())
            ->method('generate');
        $this->aliasGenerator->expects($this->never())
            ->method('generate');
        $this->serializer->expects($this->never())
            ->method('fromArray');
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    public function testTransformerThrowsExceptionIfInvalidData(): void
    {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform([]);
    }

    public function validDataProvider(): array
    {
        $brand = new Brand();
        $brand->setName('Brand');
        $phoneModel = new PhoneModel();
        $phoneModel->setName('Model S');
        $network = new Network();
        $network->setName('NETWORK');

        return [
            [
                [
                    'image' => [Image::FIELD_PATH => 'https://store.co.uk/.../product/AI8__Midnight_Black__1.jpg'],
                    'product' => ['name' => 'any'],
                    'brand' => $brand,
                    'phone_model' => $phoneModel,
                    'network' => $network,
                ],
                'Midnight Black'
            ],
            [
                [
                    'image' => [Image::FIELD_PATH => 'https://store.co.uk/.../product/AI8__Black__1.jpg'],
                    'product' => ['name' => 'any'],
                    'brand' => $brand,
                    'phone_model' => $phoneModel,
                    'network' => $network,
                ],
                'Black'
            ],
            [
                [
                    'image' => [Image::FIELD_PATH => 'https://store.co.uk/.../product/Main.jpg'],
                    'product' => ['name' => 'Brand Model S 128GB (Product) Red NETWORK - Refurbished / Used'],
                    'brand' => $brand,
                    'phone_model' => $phoneModel,
                    'network' => $network,
                ],
                '(Product) Red'
            ],
            [
                [
                    'image' => [Image::FIELD_PATH => 'https://store.co.uk/.../product/Main.jpg'],
                    'product' => ['name' => 'Brand Model S 128GB Black NETWORK - Refurbished / Used'],
                    'brand' => $brand,
                    'phone_model' => $phoneModel,
                    'network' => $network,
                ],
                'Black'
            ],
        ];
    }

    public function invalidDataProvider(): array
    {
        $brand = new Brand();
        $phoneModel = new PhoneModel();
        $network = new Network();

        return [
            [
                [
                    'image' => [Image::FIELD_PATH => 'https://store.co.uk/.../product/Main.jpg'],
                    'product' => ['name' => 'any'],
                    'brand' => $brand,
                    'phone_model' => $phoneModel,
                    'network' => $network,
                ]
            ]
        ];
    }
}
