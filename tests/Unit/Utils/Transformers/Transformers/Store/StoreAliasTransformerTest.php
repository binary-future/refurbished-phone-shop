<?php

namespace Tests\Unit\Utils\Transformers\Transformers\Store;

use App\Domain\Store\Store;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\Store\StoreAliasTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreAliasTransformerTest extends TestCase
{
    use BaseMock;

    private const GROUP_STORE = 'store';
    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    private function getTransformer(): StoreAliasTransformer
    {
        return new StoreAliasTransformer($this->serializer);
    }

    /**
     * @param array $data
     * @param Store $store
     * @dataProvider validDataProvider
     */
    public function testTransformValidData(array $data, Store $store): void
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($store);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertEquals($store, $result[self::GROUP_STORE]);
    }

    public function validDataProvider(): array
    {
        return [
            [
                [self::GROUP_STORE => [Store::FIELD_SLUG => 'store-slug']],
                new Store([Store::FIELD_SLUG => 'store-slug', Store::FIELD_ALIAS => 'store-slug']),
            ]
        ];
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = [];
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    protected function tearDown(): void
    {
        unset($this->serializer);
        parent::tearDown();
    }
}
