<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Shared\Description\Description;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\DescriptionImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class DescriptionImportTransformerTest extends TestCase
{
    use BaseMock;

    private $specification;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->specification = $this->getBaseMock(Specification::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->specification,
            $this->serializer
        );
        parent::tearDown();
    }

    private function getTransformer(): ?DescriptionImportTransformer
    {
        return new DescriptionImportTransformer($this->serializer, $this->specification);
        ;
    }

    public function testTransformValidData(): void
    {
        $data = ['description' => [Description::FIELD_CONTENT => 'sometext']];

        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Description::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Description::class, $result['description']);
    }

    public function testTransformInvalidData(): void
    {
        $data = ['description' => [Description::FIELD_CONTENT => '']];

        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Description::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertNull($result['description']);
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = ['invalid' => ['data']];
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }
}
