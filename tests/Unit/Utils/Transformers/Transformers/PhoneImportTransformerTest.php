<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\PhoneImportTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PhoneImportTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $specification;

    private $generator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->specification = $this->getBaseMock(Specification::class);
        $this->generator = $this->getBaseMock(CheckAliasGenerator::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->serializer,
            $this->specification,
            $this->generator,
        );
        parent::tearDown();
    }


    private function getTransformer(): PhoneImportTransformer
    {
        return new PhoneImportTransformer(
            $this->serializer,
            $this->specification,
            $this->generator
        );
    }

    /**
     * @param array $data
     * @param Phone $phone
     * @dataProvider transformDataProvider
     */
    public function testTransformingValidData(array $data, Phone $phone): void
    {
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($phone);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Phone::class, $result['phone']);
    }

    public function testTransformerThrowsExceptionIfInvalidData(): void
    {
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform([]);
    }

    /**
     * @param array $data
     * @param Phone $phone
     * @dataProvider transformDataProvider
     */
    public function testTransformerThrowsExceptionIfFailedSpecification(array $data, Phone $phone): void
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($phone);
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    /**
     * @param array $data
     * @param Phone $phone
     * @dataProvider transformDataProvider
     */
    public function testTransformerThrowsExceptionIfCapacityNotFound(array $data, Phone $phone): void
    {
        $data['phone'] = ['name' => 'Xiaomi Mi 5 White UNLOCKED - Refurbished / Used'];

        $this->serializer->expects($this->never())
            ->method('fromArray');
        $this->specification->expects($this->never())
            ->method('isSatisfy');
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    public function transformDataProvider(): array
    {
        $model = new PhoneModel();
        $model->setAlias('alias');
        $color = new Color();
        $color->setName('name');
        $data = [
            'phone' => ['name' => 'Xiaomi Mi 5 64GB White UNLOCKED - Refurbished / Used'],
            'phone_model' => $model,
            'color' => $color
        ];
        $phone = new Phone();
        $phone->setCapacity(64);

        return [
            [$data, $phone]
        ];
    }
}
