<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\RevgluePhoneTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class RevgluePhoneTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
    }

    private function getTransformer()
    {
        return new RevgluePhoneTransformer($this->serializer);
    }

    public function testTransformValidData()
    {
        $data = ['product' => ['external_Id' => 746]];
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Phone::class));
        $transformer = $this->getTransformer();
        $result = $transformer->transform($data);
        $this->assertInstanceOf(Phone::class, $result['product']);
    }

    public function testTransformerThrowsExceptionIfThereIsNoData()
    {
        $data = ['phones' => ['data']];
        $transformer = $this->getTransformer();
        $this->expectException(TransformerException::class);
        $transformer->transform($data);
    }
}
