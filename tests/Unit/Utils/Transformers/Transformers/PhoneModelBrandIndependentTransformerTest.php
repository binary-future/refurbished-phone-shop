<?php


namespace Tests\Unit\Utils\Transformers\Transformers;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\PhoneModelBrandIndependentTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhoneModelBrandIndependentTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $aliasGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->aliasGenerator = $this->getBaseMock(CheckAliasGenerator::class);
    }

    private function getTransformer()
    {
        return new PhoneModelBrandIndependentTransformer(
            $this->serializer,
            $this->aliasGenerator
        );
    }

    /**
     * @param array $data
     * @param string $alias
     * @dataProvider transformDataProvider
     */
    public function testTransformingValidData(array $data, string $alias)
    {
        $phoneModel = new PhoneModel();
        $phoneModel->setName('addasd');
        $this->aliasGenerator->expects($this->once())
            ->method('generate')
            ->willReturn($alias);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($phoneModel);
        $transformer = $this->getTransformer();
        $result = $transformer->transform($data);
        $this->assertInstanceOf(PhoneModel::class, $result['phone_model']);
        $this->assertEquals($alias, $result['phone_model']->getAlias());
    }

    public function testTransformerThrowsExceptionIfInvalidData()
    {
        $transformer = $this->getTransformer();
        $this->expectException(TransformerException::class);
        $transformer->transform([]);
    }

    public function transformDataProvider()
    {
        $data = ['phone_model' => ['name' => 'name']];

        return [
            [$data, 'alias']
        ];
    }
}
