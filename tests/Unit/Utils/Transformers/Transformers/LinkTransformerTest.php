<?php

namespace Tests\Unit\Utils\Transformers\Transformers;

use App\Domain\Shared\Link\Link;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;
use App\Utils\Transformers\Transformers\LinkTransformer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class LinkTransformerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $specification;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->specification = $this->getBaseMock(Specification::class);
    }

    private function getTransformer(): LinkTransformer
    {
        return new LinkTransformer($this->serializer, $this->specification);
    }

    public function testTransformValidData(): void
    {
        $data = [
            'link' => [
                Link::FIELD_LINK => 'https://www.awin1.com/pclick.php?p=21887729347&a=243515&m=5792'
            ]
        ];

        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($this->getBaseMock(Link::class));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $transformer = $this->getTransformer();

        $result = $transformer->transform($data);

        $this->assertInstanceOf(Link::class, $result['link']);
    }

    public function testTransformerThrowsExceptionIfThereIsNoData(): void
    {
        $data = ['link' => ['data']];
        $transformer = $this->getTransformer();

        $this->expectException(TransformerException::class);

        $transformer->transform($data);
    }

    public function testTransformerThrowsExceptionIfSpecificationFailed(): void
    {
        $data = [
            'link' => [
                Link::FIELD_LINK => 'https://www.awin1.com/pclick.php?p=21887729347&a=243515&m=5792'
            ]
        ];
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $transformer = $this->getTransformer();
        $this->expectException(TransformerException::class);
        $transformer->transform($data);
    }
}
