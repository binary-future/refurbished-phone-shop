<?php

namespace Tests\Unit\Utils\Encoders;

use App\Utils\Encoders\JsonEncoder;
use Tests\TestCase;

class JsonEncoderTest extends TestCase
{
    /**
     * @var JsonEncoder
     */
    private $encoder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->encoder = new JsonEncoder();
    }

    /**
     * @param array $data
     * @param string $encoded
     * @dataProvider encodeDataProvider
     */
    public function testEncode(array $data, string $encoded)
    {
        $result = $this->encoder->encode($data);
        $this->assertEquals($encoded, $result);
    }

    public function encodeDataProvider()
    {
        return [
            [
                [], '[]'
            ],
            [
                ['name' => 'test'], '{"name":"test"}'
            ],
        ];
    }

    /**
     * @param string $data
     * @param array $decoded
     * @dataProvider decodeDataProvider
     */
    public function testDecode(string $data, array $decoded)
    {
        $result = $this->encoder->decode($data);
        $this->assertEquals($decoded, $result);
    }

    public function decodeDataProvider()
    {
        return [
            [
                '[]', [],
            ],
            [
                '{"name":"test"}', ['name' => 'test'],
            ],
        ];
    }
}
