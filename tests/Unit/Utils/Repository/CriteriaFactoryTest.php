<?php

namespace Tests\Unit\Utils\Repository;

use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\With;
use App\Utils\Repository\Exceptions\CriteriaBuildException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

abstract class CriteriaFactoryTest extends TestCase
{
    use BaseMock;

    protected $config;
    protected $container;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->container = $this->getBaseMock(Container::class);
    }

    abstract protected function getFactory();

    /**
     * @param array $list
     * @param string $alias
     * @dataProvider criteriaDataProvider
     */
    public function testFactoryCreatesCriteria(array $list, string $alias)
    {
        $criteria = $this->getBaseMock(Criteria::class);
        $this->config->method('get')
            ->willReturn($list);
        $this->container->expects($this->once())->method('resolve')
            ->willReturn($criteria);
        $factory = $this->getFactory();
        $criteria = $factory->buildCriteria($alias, []);
        $this->assertInstanceOf(Criteria::class, $criteria);
    }

    public function criteriaDataProvider()
    {
        return [
            [
                ['default' => ['alias' => With::class]], 'alias'
            ],
        ];
    }

    public function testFactoryThrowsExceptionIfConfigHasNotCriteria()
    {
        $this->config->method('get')->willReturn([]);
        $factory = $this->getFactory();
        $this->expectException(CriteriaBuildException::class);
        $factory->buildCriteria('criteria', []);
    }

    /**
     *
     * @dataProvider criteriaDataProvider
     * @param array $list
     * @param string $alias
     */
    public function testFactoryThrowsExceptionIfContainerHasNotCriteria(array $list, string $alias)
    {
        $this->config->expects($this->once())->method('get')
            ->willReturn($list);
        $this->container->expects($this->once())->method('resolve')
            ->willReturn(null);
        $factory = $this->getFactory();

        $this->expectException(CriteriaBuildException::class);

        $factory->buildCriteria($alias, []);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->config, $this->container);
    }
}
