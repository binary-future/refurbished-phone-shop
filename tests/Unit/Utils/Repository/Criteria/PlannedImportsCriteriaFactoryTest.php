<?php


namespace Tests\Unit\Utils\Repository\Criteria;


use App\Utils\Repository\Criteria\PlannedImportsCriteriaFactory;
use Tests\Unit\Utils\Repository\CriteriaFactoryTest;

/**
 * Class PlannedImportsCriteriaFactoryTest
 * @package Tests\Unit\Utils\Repository\Criteria
 */
class PlannedImportsCriteriaFactoryTest extends CriteriaFactoryTest
{
    /**
     * @return PlannedImportsCriteriaFactory
     */
    protected function getFactory()
    {
        return new PlannedImportsCriteriaFactory($this->config, $this->container);
    }

}
