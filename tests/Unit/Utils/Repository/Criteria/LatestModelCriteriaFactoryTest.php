<?php

namespace Tests\Unit\Utils\Repository\Criteria;

use App\Utils\Repository\Criteria\PhoneLatestModelCriteriaFactory;
use Tests\Unit\Utils\Repository\CriteriaFactoryTest;

class LatestModelCriteriaFactoryTest extends CriteriaFactoryTest
{
    protected function getFactory()
    {
        return new PhoneLatestModelCriteriaFactory($this->config, $this->container);
    }
}
