<?php

namespace Tests\Unit\Utils\Repository\Criteria;

use App\Utils\Repository\Criteria\StoreCriteriaFactory;
use Tests\Unit\Utils\Repository\CriteriaFactoryTest;

class StoreCriteriaFactoryTest extends CriteriaFactoryTest
{
    protected function getFactory()
    {
        return new StoreCriteriaFactory($this->config, $this->container);
    }
}
