<?php

namespace Tests\Unit\Utils\Repository\Criteria;

use App\Utils\Repository\Criteria\LinkCriteriaFactory;
use Tests\Unit\Utils\Repository\CriteriaFactoryTest;

class LinkCriteriaFactoryTest extends CriteriaFactoryTest
{
    protected function getFactory(): LinkCriteriaFactory
    {
        return new LinkCriteriaFactory($this->config, $this->container);
    }
}
