<?php

namespace Tests\Unit\Utils\Repository\Builder;

use App\Utils\Repository\Builder\JoinTableChecker;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Query\JoinClause;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class JoinTableCheckerTest extends TestCase
{
    use BaseMock;

    public function getJoinTableChecker(): JoinTableChecker
    {
        return new JoinTableChecker();
    }

    private function getBuilder(?JoinClause $joinClause): Builder
    {
        $queryBuilder = $this->getBaseMock(QueryBuilder::class);
        $queryBuilder->joins = $joinClause ? [$joinClause] : null;
        $builder = $this->getBaseMock(Builder::class);
        $builder->expects($this->once())->method('getQuery')->willReturn($queryBuilder);

        return $builder;
    }

    public function testIsTableJoined()
    {
        $joinClause = $this->getBaseMock(JoinClause::class);
        $joinClause->table = 'table';
        $builder = $this->getBuilder($joinClause);
        $checker = $this->getJoinTableChecker();

        $isJoined = $checker->isTableJoined($builder, 'table');

        $this->assertTrue($isJoined);
    }

    public function testReturnFalseIfTableWrong()
    {
        $joinClause = $this->getBaseMock(JoinClause::class);
        $joinClause->table = 'wrong_table';
        $builder = $this->getBuilder($joinClause);
        $checker = $this->getJoinTableChecker();

        $isJoined = $checker->isTableJoined($builder, 'table');

        $this->assertFalse($isJoined);
    }

    public function testReturnFalseJoinsNull()
    {
        $builder = $this->getBuilder(null);
        $checker = $this->getJoinTableChecker();

        $isJoined = $checker->isTableJoined($builder, 'table');

        $this->assertFalse($isJoined);
    }
}
