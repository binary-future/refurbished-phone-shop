<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Importer\Datafeeds\DatafeedApi;
use App\Utils\Repository\Repositories\DatafeedsApiRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class DatafeedsApiRepositoryTest extends RepositoryTest
{
    protected function getModelClass()
    {
        return DatafeedApi::class;
    }

    protected function getRepository()
    {
        return new DatafeedsApiRepository(
            $this->criteriaFactory,
            $this->model
        );
    }
}