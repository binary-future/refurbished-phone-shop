<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Utils\Repository\Repositories\PaymentMethodRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class PaymentMethodRepositoryTest extends RepositoryTest
{
    /**
     * @return PaymentMethodRepository
     * @throws \App\Utils\Repository\Exceptions\RepositoryException
     */
    protected function getRepository(): PaymentMethodRepository
    {
        return new PaymentMethodRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass(): string
    {
        return PaymentMethod::class;
    }
}
