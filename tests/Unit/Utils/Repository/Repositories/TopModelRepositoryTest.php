<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Model\TopModel\TopModel;
use App\Utils\Repository\Repositories\TopModelsRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

/**
 * Class TopModelRepositoryTest
 * @package Tests\Unit\Utils\Repository\Repositories
 */
final class TopModelRepositoryTest extends RepositoryTest
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return TopModel::class;
    }

    /**
     * @return TopModelsRepository
     * @throws \App\Utils\Repository\Exceptions\RepositoryException
     */
    protected function getRepository(): TopModelsRepository
    {
        return new TopModelsRepository($this->criteriaFactory, $this->model);
    }
}
