<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Repositories\PhoneModelsRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class PhoneModelRepositoryTest extends RepositoryTest
{
    protected function getRepository()
    {
        return new PhoneModelsRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass()
    {
        return PhoneModel::class;
    }
}
