<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Importer\Reports\NegativeReport;
use App\Utils\Repository\Repositories\NegativeReportRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class NegativeReportRepositoryTest extends RepositoryTest
{
    protected function getModelClass()
    {
        return NegativeReport::class;
    }

    protected function getRepository()
    {
        return new NegativeReportRepository($this->criteriaFactory, $this->model);
    }

}