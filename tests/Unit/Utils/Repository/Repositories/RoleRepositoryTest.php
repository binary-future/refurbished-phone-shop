<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\User\Role;
use App\Utils\Repository\Repositories\RoleRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class RoleRepositoryTest extends RepositoryTest
{
    protected function getRepository()
    {
        return new RoleRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass()
    {
        return Role::class;
    }
}