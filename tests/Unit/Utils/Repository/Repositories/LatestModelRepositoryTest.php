<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Model\LatestModel\LatestModel;
use App\Utils\Repository\Repositories\LatestModelsRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

/**
 * Class LatestModelRepositoryTest
 * @package Tests\Unit\Utils\Repository\Repositories
 */
final class LatestModelRepositoryTest extends RepositoryTest
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return LatestModel::class;
    }

    /**
     * @return LatestModelsRepository
     * @throws \App\Utils\Repository\Exceptions\RepositoryException
     */
    protected function getRepository(): LatestModelsRepository
    {
        return new LatestModelsRepository($this->criteriaFactory, $this->model);
    }
}
