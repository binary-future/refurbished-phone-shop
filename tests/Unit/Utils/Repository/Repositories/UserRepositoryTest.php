<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\User\User;
use App\Utils\Repository\Exceptions\RepositoryException;
use App\Utils\Repository\Repositories\UserRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

/**
 * Class UserRepositoryTest
 * @package Tests\Unit\Utils\Repository\Repositories
 */
final class UserRepositoryTest extends RepositoryTest
{
    /**
     * @return UserRepository
     * @throws RepositoryException
     */
    protected function getRepository()
    {
        return new UserRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass(): string
    {
        return User::class;
    }
}
