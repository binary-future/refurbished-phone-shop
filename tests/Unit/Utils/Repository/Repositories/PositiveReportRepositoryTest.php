<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Importer\Reports\PositiveReport;
use App\Utils\Repository\Repositories\PositiveReportRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class PositiveReportRepositoryTest extends RepositoryTest
{
    protected function getModelClass()
    {
        return PositiveReport::class;
    }

    protected function getRepository()
    {
        return new PositiveReportRepository($this->criteriaFactory, $this->model);
    }

}