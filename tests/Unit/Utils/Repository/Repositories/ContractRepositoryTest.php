<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Deals\Contract\Contract;
use App\Utils\Repository\Repositories\ContractRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class ContractRepositoryTest extends RepositoryTest
{
    protected function getModelClass()
    {
        return Contract::class;
    }

    protected function getRepository()
    {
        return new ContractRepository(
            $this->criteriaFactory,
            $this->model
        );
    }
}
