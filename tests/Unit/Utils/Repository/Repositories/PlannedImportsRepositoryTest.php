<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Utils\Repository\Repositories\PlannedImportsRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class PlannedImportsRepositoryTest extends RepositoryTest
{
    protected function getRepository()
    {
        return new PlannedImportsRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass()
    {
        return PlannedImport::class;
    }
}