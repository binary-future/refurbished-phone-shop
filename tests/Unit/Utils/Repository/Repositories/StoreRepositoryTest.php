<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Store\Store;
use App\Utils\Repository\Repositories\StoreRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class StoreRepositoryTest extends RepositoryTest
{
    protected function getRepository()
    {
        return new StoreRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass()
    {
        return Store::class;
    }
}
