<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Repositories\PhoneItemRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class PhoneItemRepositoryTest extends RepositoryTest
{
    protected function getRepository()
    {
        return new PhoneItemRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass()
    {
        return Phone::class;
    }
}
