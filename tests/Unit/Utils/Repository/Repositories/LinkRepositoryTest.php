<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Shared\Link\Link;
use App\Utils\Repository\Repositories\LinkRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class LinkRepositoryTest extends RepositoryTest
{
    /**
     * @return LinkRepository
     * @throws \App\Utils\Repository\Exceptions\RepositoryException
     */
    protected function getRepository(): LinkRepository
    {
        return new LinkRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass(): string
    {
        return Link::class;
    }
}
