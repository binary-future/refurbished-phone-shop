<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\DescriptionImage;
use App\Utils\Repository\DataStorage\DescriptionImageStorage;
use App\Utils\Repository\Repositories\DescriptionImageRepository;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

/**
 * Class DescriptionImageRepositoryTest
 * @package Tests\Unit\Utils\Repository\Repositories
 */
class DescriptionImageRepositoryTest extends TestCase
{
    use BaseMock;

    private $storage;

    /**
     * @var DescriptionImageRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storage = $this->getBaseMock(DescriptionImageStorage::class);
        $this->repository = new DescriptionImageRepository($this->storage);
    }

    /**
     * @param Description $description
     * @param Collection $images
     * @param array $storedData
     * @param array $updatedData
     * @dataProvider syncDataProvider
     */
    public function testSyncFunction(
        Description $description,
        Collection $images,
        array $storedData,
        array $updatedData
    ) {
        $this->storage->method('getContent')->willReturn($storedData);
        $this->storage->method('putContent')->with($updatedData)->willReturn(true);

        $this->assertTrue($this->repository->sync($description, $images));
    }

    public function syncDataProvider()
    {
        $description = new Description();
        $description->id = 1;
        $images = collect([
            new DescriptionImage('path', 1),
            $description,
            new DescriptionImage('path2', 1)
        ]);
        $storedData = [
            1 => ['path3', 'path4'],
            2 => ['path5']
        ];
        $updatedData = [
            1 => ['path', 'path2'],
            2 => ['path5']
        ];

        return [
            [$description, $images, $storedData, $updatedData]
        ];
    }

    /**
     * @param Description $description
     * @param array $storedData
     * @param array $updatedData
     * @dataProvider removeDataProvider
     */
    public function testRemoveFunction(Description $description, array $storedData, array $updatedData)
    {
        $this->storage->method('getContent')->willReturn($storedData);
        $this->storage->method('putContent')->with($updatedData)->willReturn(true);

        $this->assertTrue($this->repository->remove($description));
    }

    public function removeDataProvider()
    {
        $description = new Description();
        $description->id = 1;
        $storedData = [
            1 => ['path3', 'path4'],
            2 => ['path5']
        ];
        $updatedData = [
            2 => ['path5']
        ];

        return [
            [$description, $storedData, $updatedData]
        ];
    }

    /**
     * @param array $storedData
     * @param Collection $response
     * @dataProvider allMethodDataProvider
     */
    public function testAllFunction(array $storedData, Collection $response)
    {
        $this->storage->method('getContent')->willReturn($storedData);

        $this->assertEquals($response, $this->repository->all());
    }

    public function allMethodDataProvider()
    {
        $storedData = [
            1 => ['path3', 'path4'],
            2 => ['path5']
        ];
        $response = collect([
            new DescriptionImage('path3', 1),
            new DescriptionImage('path4', 1),
            new DescriptionImage('path5', 2),
        ]);

        return [
            [$storedData, $response]
        ];

    }


}
