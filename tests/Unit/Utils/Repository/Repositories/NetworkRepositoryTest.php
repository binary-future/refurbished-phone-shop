<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Deals\Contract\Network;
use App\Utils\Repository\Repositories\NetworkRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class NetworkRepositoryTest extends RepositoryTest
{
    protected function getModelClass()
    {
        return Network::class;
    }

    protected function getRepository()
    {
        return new NetworkRepository(
            $this->criteriaFactory,
            $this->model
        );
    }
}
