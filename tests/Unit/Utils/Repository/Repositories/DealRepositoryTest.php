<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Repositories\DealRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class DealRepositoryTest extends RepositoryTest
{
    protected function getModelClass()
    {
        return Deal::class;
    }

    protected function getRepository()
    {
        return new DealRepository($this->criteriaFactory, $this->model);
    }

}