<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Phone\Phone\Color;
use App\Utils\Repository\Exceptions\RepositoryException;
use App\Utils\Repository\Repositories\PhoneColorRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class PhoneColorRepositoryTest extends RepositoryTest
{
    /**
     * @return PhoneColorRepository
     * @throws RepositoryException
     */
    protected function getRepository(): PhoneColorRepository
    {
        return new PhoneColorRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass(): string
    {
        return Color::class;
    }
}
