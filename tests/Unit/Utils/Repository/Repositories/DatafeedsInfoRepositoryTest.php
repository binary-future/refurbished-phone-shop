<?php


namespace Tests\Unit\Utils\Repository\Repositories;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Utils\Repository\Repositories\DatafeedsInfoRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

/**
 * Class DatafeedsInfoRepositoryTest
 * @package Tests\Unit\Utils\Repository\Repositories
 */
final class DatafeedsInfoRepositoryTest extends RepositoryTest
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return DatafeedInfo::class;
    }

    protected function getRepository()
    {
        return new DatafeedsInfoRepository($this->criteriaFactory, $this->model);
    }
}
