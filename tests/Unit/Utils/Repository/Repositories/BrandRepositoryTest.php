<?php

namespace Tests\Unit\Utils\Repository\Repositories;

use App\Domain\Phone\Brand\Brand;
use App\Utils\Repository\Repositories\BrandRepository;
use Tests\Unit\Utils\Repository\RepositoryTest;

final class BrandRepositoryTest extends RepositoryTest
{
    protected function getRepository()
    {
        return new BrandRepository($this->criteriaFactory, $this->model);
    }

    protected function getModelClass()
    {
        return Brand::class;
    }
}
