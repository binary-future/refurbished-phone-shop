<?php

namespace Tests\Unit\Utils\Repository\Conductor;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Utils\Bus\Exceptions\BusDispatcherException;
use App\Utils\Repository\Conductor\Exceptions\ConductorException;
use App\Utils\Repository\Conductor\Generators\MethodGenerator;
use App\Utils\Repository\Conductor\WhereConductor;
use App\Utils\Repository\Contracts\CriteriaValue;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class WhereConductorTest extends TestCase implements OperatorDictionary
{
    use BaseMock;

    private $methodGenerator;

    private $availableBuilderMethods = [
        'where', 'orWhere',
        'whereIn', 'whereNotIn', 'orWhereIn', 'orWhereNotIn',
        'whereBetween', 'whereNotBetween', 'orWhereBetween', 'orWhereNotBetween',
        'whereNull', 'whereNotNull', 'orWhereNull', 'orWhereNotNull'
    ];

    private $noWarningBuilderMethods = [
        'whereNot', 'orWhereNot'
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->methodGenerator = $this->getBaseMock(MethodGenerator::class);
    }

    private function getConductor()
    {
        return new WhereConductor($this->methodGenerator);
    }

    private function getBuilder()
    {
        return $this->getMockBuilder(Builder::class)
            ->disableOriginalConstructor()
            ->setMethods(array_merge($this->availableBuilderMethods, $this->noWarningBuilderMethods))
            ->getMock();
    }

    /**
     * @param string $expectedMethod
     * @param CriteriaValue $value
     * @param bool $or
     * @param bool $not
     * @dataProvider positiveDataProvider
     */
    public function testPositiveApply(string $expectedMethod, CriteriaValue $value, bool $or, bool $not)
    {
        $column = 'column';
        $this->methodGenerator->expects($this->once())
            ->method('generate')
            ->willReturn($expectedMethod);
        $conductor = $this->getConductor();
        $builder = $this->getBuilder();
        $builder->expects($this->once())
            ->method($expectedMethod)
            ->willReturn($builder);

        $result = $conductor->apply($builder, $column, $value, $or, $not);

        $this->assertInstanceOf(Builder::class, $result);
    }

    public function positiveDataProvider()
    {
        $value1 = new CompareValue('value');
        $value2 = new CompareValue('value', self::OD_IN);
        $value3 = new BetweenValues('value', 'value');
        $value4 = new CompareValue(null, self::OD_IS);

        return [
            ['where', $value1, false, false],
            ['orWhere', $value1, true, false],
            ['whereIn', $value2, false, false],
            ['whereNotIn', $value2, false, true],
            ['orWhereIn', $value2, true, false],
            ['orWhereNotIn', $value2, true, true],
            ['whereBetween', $value3, false, false],
            ['whereNotBetween', $value3, false, true],
            ['orWhereBetween', $value3, true, false],
            ['orWhereNotBetween', $value3, true, true],
            ['whereNull', $value4, false, false],
            ['whereNotNull', $value4, false, true],
            ['orWhereNull', $value4, true, false],
            ['orWhereNotNull', $value4, true, true],
        ];
    }

    /**
     * @param string $method
     * @param CriteriaValue $value
     * @param bool $or
     * @param bool $not
     * @dataProvider negativeDataProvider
     */
    public function testNegativeApply(string $method, CriteriaValue $value, bool $or, bool $not)
    {
        $column = 'column';
        $this->methodGenerator->expects($this->once())
            ->method('generate')
            ->willReturn($method);
        $conductor = $this->getConductor();
        $builder = $this->getBuilder();
        $builder->expects($this->once())
            ->method($method)
            ->willThrowException(new BusDispatcherException());

        $this->expectException(ConductorException::class);

        $conductor->apply($builder, $column, $value, $or, $not);
    }

    public function negativeDataProvider()
    {
        $value1 = new CompareValue('value');
        $value2 = new BetweenValues('value', 'value');

        return [
            ['whereNot', $value1, false, true],
            ['orWhereNot', $value1, true, true],
            ['where', $value2, false, false],
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->methodGenerator);
    }
}
