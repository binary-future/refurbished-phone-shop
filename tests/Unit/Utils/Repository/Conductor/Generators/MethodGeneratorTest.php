<?php

namespace Tests\Unit\Utils\Repository\Conductor\Generators;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Utils\Repository\Conductor\Generators\MethodGenerator;
use PHPUnit\Framework\TestCase;

class MethodGeneratorTest extends TestCase implements OperatorDictionary
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    private function getGenerator()
    {
        return new MethodGenerator();
    }

    /**
     * @param string $expected
     * @param string $command
     * @param string $operator
     * @param bool $or
     * @param bool $not
     * @param bool $isNull
     * @dataProvider generateProvider
     */
    public function testGenerate(
        string $expected,
        string $command,
        string $operator,
        bool $or,
        bool $not,
        bool $isNull
    ) {
        $generator = $this->getGenerator();

        $result = $generator->generate($command, $operator, $or, $not, $isNull);

        $this->assertEquals($expected, $result);
    }

    public function generateProvider()
    {
        return [
            ['where', 'where', self::OD_EQUAL, false, false, false],
            ['orWhere', 'where', self::OD_EQUAL, true, false, false],
            ['orWhereNot', 'where', self::OD_EQUAL, true, true, false],
            ['whereBetween', 'where', self::OD_BETWEEN, false, false, false],
            ['whereNotBetween', 'where', self::OD_BETWEEN, false, true, false],
            ['orWhereBetween', 'where', self::OD_BETWEEN, true, false, false],
            ['orWhereNotBetween', 'where', self::OD_BETWEEN, true, true, false],
            ['whereIn', 'where', self::OD_IN, false, false, false],
            ['whereNotIn', 'where', self::OD_IN, false, true, false],
            ['orWhereIn', 'where', self::OD_IN, true, false, false],
            ['orWhereNotIn', 'where', self::OD_IN, true, true, false],
            ['whereNull', 'where', self::OD_IS, false, false, true],
            ['whereNotNull', 'where', self::OD_IS, false, true, true],
            ['orWhereNull', 'where', self::OD_IS, true, false, true],
            ['orWhereNotNull', 'where', self::OD_IS, true, true, true],
        ];
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
}
