<?php

namespace Tests\Unit\Utils\Repository;

use App\Domain\Common\Contracts\Repository\Query;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Contracts\CriteriaFactory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

abstract class RepositoryTest extends TestCase
{
    use BaseMock;

    protected $criteriaFactory;
    protected $model;

    protected function setUp(): void
    {
        parent::setUp();
        $this->criteriaFactory = $this->getBaseMock(CriteriaFactory::class);
        $this->model = $this->getModel();
    }

    private function getBaseMock(string $className, array $methods = []): MockObject
    {
        $mockBuilder = $this->getMockBuilder($className)
            ->disableOriginalConstructor();
        if (! empty($methods)) {
            $mockBuilder->onlyMethods(get_class_methods($className));
            $mockBuilder->addMethods($methods);
        }
        return $mockBuilder->getMock();
    }

    private function getModel(array $mockMethods = [])
    {
        $model = $this->getBaseMock($this->getModelClass(), $mockMethods);
        $model->method('newQuery')
            ->willReturn($this->getPreparedBuilder());

        return $model;
    }

    private function getQuery()
    {
        return $this->getBaseMock(Query::class);
    }

    abstract protected function getModelClass();


    private function getPreparedBuilder()
    {
        $builder = $this->getBuilder();
        $builder->method('get')
            ->willReturn(new Collection());
        $builder->method('paginate')
            ->willReturn($this->getBaseMock(LengthAwarePaginator::class));
        $builder->method('create')
            ->willReturn($this->getBaseMock(Model::class));
        $builder->method('delete')
            ->willReturn(1);

        return $builder;
    }

    abstract protected function getRepository();

    public function testSave()
    {
        $this->model->expects($this->once())
            ->method('save')
            ->willReturn(true);
        $repository = $this->getRepository();

        $result = $repository->save($this->model);

        $this->assertInstanceOf(Model::class, $result);
    }

    public function testDelete()
    {
        $this->model->expects($this->once())
            ->method('delete')
            ->willReturn(true);
        $repository = $this->getRepository();

        $result = $repository->delete($this->model);

        $this->assertTrue($result);
    }

    public function testDeleteBy()
    {
        $builder = $this->getBuilder();
        $this->criteriaFactory->method('buildCriteria')
            ->willReturn($this->getCriteria($builder));
        $query = $this->getQuery();
        $repository = $this->getRepository();

        $result = $repository->deleteBy($query);

        $this->assertEquals(1, $result);
    }

    public function testPushWithoutRelations(): void
    {
        $this->model->expects($this->once())
            ->method('getRelations')
            ->willReturn([]);
        $this->model->expects($this->once())
            ->method('push')
            ->willReturn(true);
        $repository = $this->getRepository();

        $result = $repository->push($this->model);

        $this->assertInstanceOf(Model::class, $result);
    }

    public function testPushWithoutBelongsToManyRelation(): void
    {
        $this->model = $this->getModel(['relation']);
        $this->model->expects($this->once())
            ->method('getRelations')
            ->willReturn(['relation' => $this->getBaseMock(Model::class)]);
        $this->model->expects($this->once())
            ->method('relation')
            ->willReturn($this->getBaseMock(MorphTo::class));
        $this->model->expects($this->once())
            ->method('push')
            ->willReturn(true);
        $repository = $this->getRepository();

        $result = $repository->push($this->model);

        $this->assertInstanceOf(Model::class, $result);
    }

    public function testPushWithBelongsToManyRelation(): void
    {
        $this->model = $this->getModel(['relation']);
        $belongsToManyRelation = $this->getBaseMock(BelongsToMany::class);
        $belongsToManyRelation->expects($this->once())
            ->method('sync');
        $this->model->expects($this->once())
            ->method('getRelations')
            ->willReturn(['relation' => Collection::make(['id' => 1])]);
        $this->model->expects($this->exactly(2))
            ->method('relation')
            ->willReturn($belongsToManyRelation);
        $this->model->expects($this->once())
            ->method('unsetRelation')
            ->with('relation');
        $this->model->expects($this->once())
            ->method('push')
            ->willReturn(true);
        $repository = $this->getRepository();

        $result = $repository->push($this->model);

        $this->assertInstanceOf(Model::class, $result);
    }

    public function testUpdate()
    {
        $this->model->expects($this->once())
            ->method('update')
            ->with([])
            ->willReturn(true);
        $repository = $this->getRepository();

        $result = $repository->update($this->model, []);

        $this->assertInstanceOf(Model::class, $result);
    }

    public function testFindBy()
    {
        $this->criteriaFactory->method('buildCriteria')
            ->willReturn($this->getCriteria($this->getBuilder()));
        $repository = $this->getRepository();

        $result = $repository->findBy($this->getBaseMock(Query::class));

        $this->assertInstanceOf(Collection::class, $result);
    }

    public function testFindPaginated()
    {
        $this->criteriaFactory->method('buildCriteria')
            ->willReturn($this->getCriteria($this->getBuilder()));
        $repository = $this->getRepository();
        $result = $repository->findPaginated($this->getBaseMock(Query::class));
        $this->assertInstanceOf(LengthAwarePaginator::class, $result);
    }

    public function testCreate()
    {
        $repository = $this->getRepository();
        $result = $repository->create([]);
        $this->assertInstanceOf(Model::class, $result);
    }

    public function testFindSingleThrowsModelNotFoundException()
    {
        $this->criteriaFactory->method('buildCriteria')
            ->willReturn($this->getCriteria($this->getBuilder()));
        $repository = $this->getRepository();
        $this->expectException(ModelNotFoundException::class);
        $result = $repository->findBySingle($this->getBaseMock(Query::class));
    }

    private function getCriteria(Builder $builder)
    {
        $criteria = $this->getBaseMock(Criteria::class);
        $criteria->method('apply')
            ->willReturn($builder);

        return $criteria;
    }

    /**
     * @return Builder|MockObject
     */
    private function getBuilder()
    {
        return $this->getBaseMock(Builder::class);
    }

    protected function tearDown(): void
    {
        unset($this->criteriaFactory, $this->model);
        parent::tearDown();
    }
}
