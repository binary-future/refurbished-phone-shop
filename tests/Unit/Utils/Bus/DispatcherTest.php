<?php

namespace Tests\Unit\Utils\Bus;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Bus\Dispatcher;
use App\Utils\Bus\Exceptions\BusDispatcherException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class DispatcherTest extends TestCase
{
    use BaseMock;

    /**
     * @var
     */
    private $container;

    protected function setUp(): void
    {
        parent::setUp();
        $this->container = $this->getBaseMock(Container::class);
    }

    private function getDispatcher()
    {
        return new Dispatcher($this->container);
    }

    private function getHandler(object $response)
    {
        $handler = $this->getBaseMock(Handler::class);
        $handler->method('handle')
            ->willReturn($response);

        return $handler;
    }

    /**
     * @param $handler
     * @param $response
     * @dataProvider classNamesDataProvider
     */
    public function testDispatcherFindHandler($handler, $response)
    {
        $this->container->expects($this->once())
            ->method('resolve')
            ->with($handler)
            ->willReturn($this->getHandler($response));
        $dispatcher = $this->getDispatcher();
        $result = $dispatcher->dispatch($dispatcher);
        $this->assertInstanceOf(\stdClass::class, $result);
    }

    public function classNamesDataProvider()
    {
        return [
            [sprintf('%sHandler', Dispatcher::class), new \stdClass()]
        ];
    }

    public function testDispatcherThrowsException()
    {
        $this->container->expects($this->once())
            ->method('resolve')
            ->willReturn($this->getDispatcher());
        $dispatcher = $this->getDispatcher();
        $this->expectException(BusDispatcherException::class);
        $dispatcher->dispatch($dispatcher);
    }
}
