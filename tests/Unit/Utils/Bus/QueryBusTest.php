<?php

namespace Tests\Unit\Utils\Bus;

use App\Utils\Bus\Dispatcher;
use App\Utils\Bus\Exceptions\BusDispatcherException;
use App\Utils\Bus\QueryBus;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class QueryBusTest extends TestCase
{
    use BaseMock;

    private $dispatcher;

    protected function setUp(): void
    {
        parent::setUp();
        $this->dispatcher = $this->getBaseMock(Dispatcher::class);
    }

    private function getBus()
    {
        return new QueryBus($this->dispatcher);
    }

    public function testBusUseDispatcher()
    {
        $query = new \StdClass();
        $response = new \StdClass();
        $this->dispatcher->expects($this->once())
            ->method('dispatch')
            ->with($query)
            ->willReturn($response);
        $bus = $this->getBus();
        $result = $bus->dispatch($query);
        $this->assertEquals($response, $result);
    }

    public function testBusThrowsExceptionIfNoResponse()
    {
        $query = new \StdClass();
        $response = null;
        $this->dispatcher->expects($this->once())
            ->method('dispatch')
            ->with($query)
            ->willReturn($response);
        $bus = $this->getBus();
        $this->expectException(BusDispatcherException::class);
        $bus->dispatch($query);
    }
}
