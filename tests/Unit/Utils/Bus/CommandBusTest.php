<?php

namespace Tests\Unit\Utils\Bus;

use App\Utils\Bus\CommandBus;
use App\Utils\Bus\Dispatcher;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class CommandBusTest extends TestCase
{
    use BaseMock;

    private $dispatcher;

    protected function setUp(): void
    {
        parent::setUp();
        $this->dispatcher = $this->getBaseMock(Dispatcher::class);
    }

    private function getBus()
    {
        return new CommandBus($this->dispatcher);
    }

    public function testBusUseDispatcher()
    {
        $command = new \StdClass();
        $response = new \StdClass();
        $this->dispatcher->expects($this->once())
            ->method('dispatch')
            ->with($command)
            ->willReturn($response);
        $bus = $this->getBus();
        $result = $bus->dispatch($command);
        $this->assertEquals($response, $result);
    }

    public function testCommandBusCouldNotReturnResponse()
    {
        $command = new \StdClass();
        $response = null;
        $this->dispatcher->expects($this->once())
            ->method('dispatch')
            ->with($command)
            ->willReturn($response);
        $bus = $this->getBus();
        $result = $bus->dispatch($command);
        $this->assertEquals($response, $result);
    }
}
