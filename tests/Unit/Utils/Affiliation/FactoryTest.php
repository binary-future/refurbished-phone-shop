<?php

namespace Tests\Unit\Utils\Affiliation;

use App\Domain\Store\Affiliation\Affiliation;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Affiliation\Contracts\AffiliationGenerator;
use App\Utils\Affiliation\Exceptions\AffiliationException;
use App\Utils\Affiliation\Factory;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class FactoryTest extends TestCase
{
    use BaseMock;

    private $container;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->container = $this->getBaseMock(Container::class);
    }

    private function getFactory()
    {
        return new Factory($this->container, $this->config);
    }

    /**
     * @param array $configs
     * @param Affiliation $affiliation
     * @dataProvider validConfigsDataProvider
     */
    public function testBuildGenerator(array $configs, Affiliation $affiliation)
    {
        $this->config->method('get')
            ->willReturn($configs);
        $this->container->method('resolve')
            ->willReturn($this->getBaseMock(AffiliationGenerator::class));
        $factory = $this->getFactory();
        $result = $factory->buildGenerator($affiliation);
        $this->assertInstanceOf(AffiliationGenerator::class, $result);
    }

    public function testThrowsExceptionIfNoMap()
    {
        $this->config->method('get')
            ->willReturn([]);
        $this->container->expects($this->never())
            ->method('resolve');
        $factory = $this->getFactory();
        $this->expectException(AffiliationException::class);
        $factory->buildGenerator(new Affiliation('test'));
    }

    /**
     * @param array $configs
     * @param Affiliation $affiliation
     * @dataProvider validConfigsDataProvider
     */
    public function testThrowsExceptionIfNoGeneratorBuilds(array $configs, Affiliation $affiliation)
    {
        $this->config->method('get')
            ->willReturn($configs);
        $this->container->method('resolve')
            ->willReturn($this->getBaseMock(Factory::class));
        $factory = $this->getFactory();
        $this->expectException(AffiliationException::class);
        $factory->buildGenerator($affiliation);

    }

    public function validConfigsDataProvider()
    {
        return [
            [['networks' => ['network' => 'generator']], new Affiliation('network')]
        ];
    }
}
