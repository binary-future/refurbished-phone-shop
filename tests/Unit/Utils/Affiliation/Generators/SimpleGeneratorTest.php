<?php


namespace Tests\Unit\Utils\Affiliation\Generators;


use App\Utils\Affiliation\Generators\SimpleGenerator;
use Tests\TestCase;

/**
 * Class SimpleGeneratorTest
 * @package Tests\Unit\Utils\Affiliation\Generators
 */
final class SimpleGeneratorTest extends TestCase
{
    public function testGenerating()
    {
        $sample = 'sample';
        $generator = new SimpleGenerator();
        $response = $generator->generate($sample);
        $this->assertEquals($response, $sample);
    }
}
