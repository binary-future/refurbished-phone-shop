<?php

namespace Tests\Unit\Utils\Affiliation;

use App\Domain\Shared\Contracts\HasLinkWithAffiliation;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\Affiliation\Affiliation;
use App\Utils\Affiliation\Contracts\AffiliationGenerator;
use App\Utils\Affiliation\Factory;
use App\Utils\Affiliation\Generator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class GeneratorTest extends TestCase
{
    use BaseMock;

    private $factory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->factory = $this->getBaseMock(Factory::class);
    }

    private function getGenerator()
    {
        return new Generator($this->factory);
    }

    public function testGenerating()
    {
        $response = 'response';
        $generatorItem = $this->getBaseMock(AffiliationGenerator::class);
        $generatorItem->method('generate')
            ->willReturn($response);
        $this->factory->method('buildGenerator')
            ->willReturn($generatorItem);
        $link = new Link();
        $link->setLink('link');
        $owner = $this->getBaseMock(HasLinkWithAffiliation::class);
        $owner->method('getAffiliation')
            ->willReturn(new Affiliation('test'));
        $owner->method('getLink')
            ->willReturn($link);
        $generator = $this->getGenerator();
        $result = $generator->generate($owner);
        $this->assertEquals($response, $result);
    }
}
