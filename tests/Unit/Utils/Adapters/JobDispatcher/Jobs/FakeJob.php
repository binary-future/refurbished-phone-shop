<?php


namespace Tests\Unit\Utils\Adapters\JobDispatcher\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FakeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * FakeJob constructor.
     * @param string $arg1
     * @param string $arg2
     */
    public function __construct(string $arg1, string $arg2)
    {
    }

    public function handle()
    {

    }

}
