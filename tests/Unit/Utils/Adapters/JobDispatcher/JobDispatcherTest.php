<?php

namespace Tests\Unit\Utils\Adapters\JobDispatcher;

use App\Utils\Adapters\JobDispatcher\JobDispatcher;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;
use Tests\Unit\Utils\Adapters\JobDispatcher\Jobs\FakeJob;

class JobDispatcherTest extends TestCase
{
    use BaseMock;

    private function getJobDispatcher(): JobDispatcher
    {
        return new JobDispatcher();
    }

    public function testPositiveDispatch()
    {
        $this->expectsJobs(FakeJob::class);

        $jobDispatcher = $this->getJobDispatcher();
        $jobDispatcher->dispatch(FakeJob::class, ['arg1', 'arg2']);
    }

    public function testNegativeDispatch()
    {
        $this->expectException(\InvalidArgumentException::class);

        $jobDispatcher = $this->getJobDispatcher();
        $jobDispatcher->dispatch(\stdClass::class, []);
    }
}
