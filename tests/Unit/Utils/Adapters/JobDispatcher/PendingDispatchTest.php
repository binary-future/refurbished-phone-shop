<?php

namespace Tests\Unit\Utils\Adapters\JobDispatcher;

use App\Utils\Adapters\JobDispatcher\JobDispatcher;
use App\Utils\Adapters\JobDispatcher\PendingDispatch;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;
use Tests\Unit\Utils\Adapters\JobDispatcher\Jobs\FakeJob;

class PendingDispatchTest extends TestCase
{
    use BaseMock;

    private $proxiedPendingDispatch;

    protected function setUp(): void
    {
        parent::setUp();
        $this->proxiedPendingDispatch = $this->getBaseMock(\Illuminate\Foundation\Bus\PendingDispatch::class);
    }

    private function getPendingDispatch(): PendingDispatch
    {
        return new PendingDispatch($this->proxiedPendingDispatch);
    }

    public function testPositiveDispatch()
    {
        $queue = 'queue';
        $this->proxiedPendingDispatch->expects($this->once())
            ->method('onQueue')
            ->with($queue)
            ->willReturn($this->proxiedPendingDispatch);
        $pendingDispatch = $this->getPendingDispatch();

        $pendingDispatch->onQueue($queue);
    }

    protected function tearDown(): void
    {
        unset($this->proxiedPendingDispatch);
        parent::tearDown();
    }

}
