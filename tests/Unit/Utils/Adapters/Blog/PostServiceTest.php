<?php

namespace Tests\Unit\Utils\Adapters\Blog;

use App\Utils\Adapters\Blog\Exceptions\BlogRequestException;
use App\Utils\Adapters\Blog\Post;
use App\Utils\Adapters\Blog\PostService;
use App\Utils\Adapters\Blog\Request\RequestHandler;
use App\Utils\Adapters\Blog\Translators\PostsTranslator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PostServiceTest extends TestCase
{
    use BaseMock;

    private $postsTranslator;
    private $requestHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->postsTranslator = $this->getBaseMock(PostsTranslator::class);
        $this->requestHandler = $this->getBaseMock(RequestHandler::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->postsTranslator,
            $this->requestHandler
        );
        parent::tearDown();
    }

    private function getPostService(): PostService
    {
        return new PostService($this->postsTranslator, $this->requestHandler);
    }

    /**
     * @param int $amount
     * @param string $tag
     * @param array $requestPosts
     * @param array $postsObjects
     * @throws BlogRequestException
     * @dataProvider postsDataProvider
     */
    public function testByTags(int $amount, string $tag, array $requestPosts, array $postsObjects)
    {
        $this->requestHandler->expects($this->once())
            ->method('handle')
            ->willReturn($requestPosts);
        $this->postsTranslator->expects($this->once())
            ->method('translate')
            ->with($requestPosts)
            ->willReturn($postsObjects);
        $service = $this->getPostService();

        $actualPosts = $service->byTags([$tag], $amount);

        $this->assertEquals(collect($postsObjects), $actualPosts);
    }

    /**
     * @param int $amount
     * @param string $tag
     * @param array $requestPosts
     * @param array $postsObjects
     * @throws BlogRequestException
     * @dataProvider postsDataProvider
     */
    public function testByTagsTranslatorThrowsException(
        int $amount,
        string $tag,
        array $requestPosts,
        array $postsObjects
    ) {
        $this->requestHandler->expects($this->once())
            ->method('handle')
            ->willReturn($requestPosts);
        $this->postsTranslator->expects($this->once())
            ->method('translate')
            ->with($requestPosts)
            ->willThrowException(new BlogRequestException());
        $service = $this->getPostService();

        $this->expectException(BlogRequestException::class);

        $service->byTags([$tag], $amount);
    }

    public function postsDataProvider(): array
    {
        $tag = 'tag';

        $amount1 = 3;
        $requestPosts1 = $this->getPostsData($amount1, [$tag]);
        $postsObjects1 = $this->getPostsObjects($amount1, [$tag]);

        $amount2 = 1;
        $requestPosts2 = $this->getPostsData($amount2, [$tag]);
        $postsObjects2 = $this->getPostsObjects($amount2, [$tag]);

        return [
            [$amount1, $tag, $requestPosts1, $postsObjects1],
            [$amount2, $tag, $requestPosts2, $postsObjects2],
        ];
    }

    /**
     * @param int $amount
     * @param string $exceptTag
     * @param array $requestPosts
     * @param array $postsObjects
     * @throws BlogRequestException
     * @dataProvider postsDataProvider
     */
    public function testLastPostsExceptTags(
        int $amount,
        string $exceptTag,
        array $requestPosts,
        array $postsObjects
    ) {
        $this->requestHandler->expects($this->once())
            ->method('handle')
            ->willReturn($requestPosts);
        $this->postsTranslator->expects($this->once())
            ->method('translate')
            ->with($requestPosts)
            ->willReturn($postsObjects);
        $service = $this->getPostService();

        $actualPosts = $service->lastPostsExceptTags([$exceptTag], $amount);

        $this->assertEquals(collect($postsObjects), $actualPosts);
    }

    /**
     * @param int $amount
     * @param string $exceptTag
     * @param array $requestPosts
     * @param array $postsObjects
     * @throws BlogRequestException
     * @dataProvider lastPostsDataExceptTagsProvider
     */
    public function testLastPostsExceptTagsTranslatorThrowsException(
        int $amount,
        string $exceptTag,
        array $requestPosts,
        array $postsObjects
    ) {
        $this->requestHandler->expects($this->once())
            ->method('handle')
            ->willReturn($requestPosts);
        $this->postsTranslator->expects($this->once())
            ->method('translate')
            ->with($requestPosts)
            ->willThrowException(new BlogRequestException());
        $service = $this->getPostService();

        $this->expectException(BlogRequestException::class);

        $service->lastPostsExceptTags([$exceptTag], $amount);
    }

    public function lastPostsDataExceptTagsProvider(): array
    {
        $exceptTag = 'exceptTag';
        $tag = 'tag';

        $amount1 = 3;
        $requestPosts1 = $this->getPostsData($amount1, [$tag]);
        $postsObjects1 = $this->getPostsObjects($amount1, [$tag]);

        $amount2 = 1;
        $requestPosts2 = $this->getPostsData($amount2, [$tag]);
        $postsObjects2 = $this->getPostsObjects($amount2, [$tag]);

        return [
            [$amount1, $exceptTag, $requestPosts1, $postsObjects1],
            [$amount2, $exceptTag, $requestPosts2, $postsObjects2],
        ];
    }

    /**
     * @param int $amount
     * @param array $requestPosts
     * @param array $postsObjects
     * @throws BlogRequestException
     * @dataProvider lastPostsProvider
     */
    public function testLastPosts(
        int $amount,
        array $requestPosts,
        array $postsObjects
    ) {
        $this->requestHandler->expects($this->once())
            ->method('handle')
            ->willReturn($requestPosts);
        $this->postsTranslator->expects($this->once())
            ->method('translate')
            ->with($requestPosts)
            ->willReturn($postsObjects);
        $service = $this->getPostService();

        $actualPosts = $service->lastPosts($amount);

        $this->assertEquals(collect($postsObjects), $actualPosts);
    }

    /**
     * @param int $amount
     * @param array $requestPosts
     * @param array $postsObjects
     * @throws BlogRequestException
     * @dataProvider lastPostsProvider
     */
    public function testLastPostsTranslatorThrowsException(
        int $amount,
        array $requestPosts,
        array $postsObjects
    ) {
        $this->requestHandler->expects($this->once())
            ->method('handle')
            ->willReturn($requestPosts);
        $this->postsTranslator->expects($this->once())
            ->method('translate')
            ->with($requestPosts)
            ->willThrowException(new BlogRequestException());
        $service = $this->getPostService();

        $this->expectException(BlogRequestException::class);

        $service->lastPosts($amount);
    }

    public function lastPostsProvider(): array
    {

        $amount1 = 3;
        $requestPosts1 = $this->getPostsData($amount1, []);
        $postsObjects1 = $this->getPostsObjects($amount1, []);

        $amount2 = 1;
        $requestPosts2 = $this->getPostsData($amount2, []);
        $postsObjects2 = $this->getPostsObjects($amount2, []);

        return [
            [$amount1, $requestPosts1, $postsObjects1],
            [$amount2, $requestPosts2, $postsObjects2],
        ];
    }

    /**
     * @param array $tags
     * @param int $amount
     * @return Post[]
     */
    public function getPostsObjects(int $amount, array $tags = ['tag']): array
    {
        $post = new Post(1, 'title', 'content', 'shortTitle', null, $tags);
        return array_pad([], $amount, $post);
    }

    /**
     * @param array $tags
     * @param int $amount
     * @return array[]
     */
    public function getPostsData(int $amount, array $tags = ['tag']): array
    {
        $post = [
            'id' => 1, 'title' => 'title', 'content' => 'content',
            'short_title' => 'short_title', 'image' => 'image',
            'tags' => $tags
        ];
        return array_pad([], $amount, $post);
    }
}
