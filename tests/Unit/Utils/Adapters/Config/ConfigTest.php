<?php

namespace Tests\Unit\Utils\Adapters\Config;

use App\Utils\Adapters\Config\Config;
use Illuminate\Contracts\Config\Repository;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class ConfigTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Repository::class);
    }

    private function getConfig()
    {
        return new Config($this->repository);
    }

    /**
     * @param string $alias
     * @param $result
     * @dataProvider configDataProvider
     */
    public function testConfigCallsRepository(string $alias, $result)
    {
        $this->repository->expects($this->once())
            ->method('get')
            ->with($alias)
            ->willReturn($result);
        $config = $this->getConfig();
        $response = $config->get($alias);
        $this->assertEquals($result, $response);
    }

    public function configDataProvider()
    {
        return [
            ['alias', 'result'],
            ['alias1', []]
        ];
    }
}
