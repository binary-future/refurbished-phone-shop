<?php

namespace Tests\Unit\Utils\Adapters\Cache;

use App\Utils\Adapters\Cache\CacheLaravel;
use App\Utils\Adapters\Cache\Exceptions\ConnectionException;
use Illuminate\Support\Facades\Cache;
use Tests\TestCase;

class CacheLaravelTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    private function getCache()
    {
        return new CacheLaravel();
    }

    /**
     * @param $key
     * @param $default
     * @param $result
     * @dataProvider getDataProvider
     */
    public function testPositiveGet(string $key, $default, $result)
    {
        Cache::shouldReceive('get')->once()
            ->with($key, $default)
            ->andReturn($result);
        $cache = $this->getCache();

        $actualResult = $cache->get($key, $default);

        $this->assertEquals($result, $actualResult);
    }

    public function getDataProvider(): array
    {
        return [
            ['config.key', 'default', ['key']],
            ['config.key', 'default', 'default'],
        ];
    }

    public function testNegativeGet()
    {
        $this->expectException(ConnectionException::class);

        Cache::shouldReceive('get')->once()
            ->andThrow(new \Exception());
        $cache = $this->getCache();

        $cache->get('anykey');
    }

    /**
     * @param string $key
     * @param $value
     * @param $minutes \DateTimeInterface|\DateInterval|float|int
     * @param $result
     * @dataProvider addPutDataProvider
     */
    public function testPositiveAdd(string $key, $value, $minutes, $result)
    {
        Cache::shouldReceive('add')->once()
            ->with($key, $value, $minutes)
            ->andReturn($result);
        $cache = $this->getCache();

        $actualResult = $cache->add($key, $value, $minutes);

        $this->assertEquals($result, $actualResult);
    }

    public function testNegativeAdd()
    {
        $this->expectException(ConnectionException::class);

        Cache::shouldReceive('add')->once()
            ->andThrow(new \Exception());
        $cache = $this->getCache();

        $cache->add('anykey', 'anyvalue', new \DateTime('now'));
    }

    public function addPutDataProvider(): array
    {
        return [
            ['config.key', 'value', new \DateTime('now'), true],
            ['config.key', 'value', new \DateInterval('P2Y4DT6H8M'), true],
            ['config.key', 'value', 5.25, true],
            ['config.key', 'value', 1, true],
        ];
    }

    /**
     * @param string $key
     * @param $value
     * @param $minutes \DateTimeInterface|\DateInterval|float|int
     * @param $result
     * @dataProvider addPutDataProvider
     */
    public function testPositivePut(string $key, $value, $minutes, $result)
    {
        Cache::shouldReceive('put')->once()
            ->with($key, $value, $minutes)
            ->andReturn($result);
        $cache = $this->getCache();

        $cache->put($key, $value, $minutes);
    }

    public function testNegativePut()
    {
        $this->expectException(ConnectionException::class);

        Cache::shouldReceive('put')->once()
            ->andThrow(new \Exception());
        $cache = $this->getCache();

        $cache->put('anykey', 'anyvalue', new \DateTime('now'));
    }

    /**
     * @param string $key
     * @param $result
     * @dataProvider hasDataProvider
     */
    public function testPositiveHas(string $key, $result)
    {
        Cache::shouldReceive('has')->once()
            ->with($key)
            ->andReturn($result);
        $cache = $this->getCache();

        $actualResult = $cache->has($key);

        $this->assertEquals($result, $actualResult);
    }

    public function hasDataProvider(): array
    {
        return [
            ['config.key', true],
            ['config.key', false],
        ];
    }

    public function testNegativeHas()
    {
        $this->expectException(ConnectionException::class);

        Cache::shouldReceive('has')->once()
            ->andThrow(new \Exception());
        $cache = $this->getCache();

        $cache->has('anykey');
    }

    public function testPositiveForever()
    {
        $key = 'key';
        $value = 'value';
        Cache::shouldReceive('forever')->once()
            ->with($key, $value);
        $cache = $this->getCache();

        $cache->forever($key, $value);
    }

    public function testNegativeForever()
    {
        $this->expectException(ConnectionException::class);

        Cache::shouldReceive('forever')->once()
            ->andThrow(new \Exception());
        $cache = $this->getCache();

        $cache->forever('anykey', 'anyvalue');
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

}
