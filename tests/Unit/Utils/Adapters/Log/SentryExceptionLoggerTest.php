<?php

namespace Tests\Unit\Utils\Adapters\Log;

use App\Utils\Adapters\Log\SentryExceptionLogger;
use Mockery;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class SentryExceptionLoggerTest extends TestCase
{
    use BaseMock;

    // here used Sentry\Laravel\ServiceProvider::$abstract value
    private $sentryAbstract = 'sentry';

    /** @var SentryExceptionLogger */
    private $logger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->logger = new SentryExceptionLogger();
    }

    protected function tearDown(): void
    {
        unset($this->logger);
        parent::tearDown();
    }

    public function testCaptureException(): void
    {
        $exception = new \Exception();
        $this->instance(
            $this->sentryAbstract,
            // instead of stdClass should be Sentry\Laravel\Facade, but it's final and can't be mocked
            Mockery::mock(\stdClass::class, static function ($mock) use ($exception) {
                $mock->shouldReceive('captureException')->once()->with($exception);
            })
        );

        $this->logger->captureException($exception);
    }
}
