<?php

namespace Tests\Unit\Utils\DataTypes\Enum;

/**
 * Class InheritedEnumFixture.
 *
 * @method static InheritedEnumFixture VALUE()
 */
class InheritedEnumFixture extends EnumFixture
{
    public const VALUE = 'value';
}
