<?php

namespace Tests\Unit\Utils\DataTypes\Enum;

use PHPUnit\Framework\TestCase;

final class EnumTest extends TestCase
{
    /**
     * getValue()
     */
    public function testGetValue(): void
    {
        $value = new EnumFixture(EnumFixture::FOO);
        $this->assertEquals(EnumFixture::FOO, $value->getValue());

        $value = new EnumFixture(EnumFixture::BAR);
        $this->assertEquals(EnumFixture::BAR, $value->getValue());

        $value = new EnumFixture(EnumFixture::NUMBER);
        $this->assertEquals(EnumFixture::NUMBER, $value->getValue());
    }

    /**
     * getKey()
     */
    public function testGetKey(): void
    {
        $value = new EnumFixture(EnumFixture::FOO);
        $this->assertEquals('FOO', $value->getKey());
        $this->assertNotEquals('BA', $value->getKey());
    }

    /**
     * @dataProvider invalidValueProvider
     * @param mixed $value
     */
    public function testCreatingEnumWithInvalidValue($value): void
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('is not part of the enum ' . EnumFixture::class);
        new EnumFixture($value);
    }

    /**
     * Contains values not existing in EnumFixture
     * @return array
     */
    public function invalidValueProvider(): array
    {
        return [
            'string' => [ 'test' ],
            'int' => [ 1234 ],
        ];
    }

    /**
     * __toString()
     * @dataProvider toStringProvider
     * @param mixed $expected
     * @param EnumFixture $enumObject
     */
    public function testToString($expected, EnumFixture $enumObject): void
    {
        $this->assertSame($expected, (string) $enumObject);
    }

    public function toStringProvider(): array
    {
        return [
            [ EnumFixture::FOO, new EnumFixture(EnumFixture::FOO) ],
            [ EnumFixture::BAR, new EnumFixture(EnumFixture::BAR) ],
            [ (string) EnumFixture::NUMBER, new EnumFixture(EnumFixture::NUMBER) ],
        ];
    }

    /**
     * keys()
     */
    public function testKeys(): void
    {
        $values = EnumFixture::keys();
        $expectedValues = array(
            'FOO',
            'BAR',
            'NUMBER',
            'PROBLEMATIC_NUMBER',
            'PROBLEMATIC_NULL',
            'PROBLEMATIC_EMPTY_STRING',
            'PROBLEMATIC_BOOLEAN_FALSE',
        );

        $this->assertSame($expectedValues, $values);
    }

    /**
     * values()
     */
    public function testValues(): void
    {
        $values = EnumFixture::values();
        $expectedValues = array(
            'FOO' => new EnumFixture(EnumFixture::FOO),
            'BAR' => new EnumFixture(EnumFixture::BAR),
            'NUMBER' => new EnumFixture(EnumFixture::NUMBER),
            'PROBLEMATIC_NUMBER' => new EnumFixture(EnumFixture::PROBLEMATIC_NUMBER),
            'PROBLEMATIC_NULL' => new EnumFixture(EnumFixture::PROBLEMATIC_NULL),
            'PROBLEMATIC_EMPTY_STRING' => new EnumFixture(EnumFixture::PROBLEMATIC_EMPTY_STRING),
            'PROBLEMATIC_BOOLEAN_FALSE' => new EnumFixture(EnumFixture::PROBLEMATIC_BOOLEAN_FALSE),
        );

        $this->assertEquals($expectedValues, $values);
    }

    /**
     * toArray()
     */
    public function testToArray(): void
    {
        $values = EnumFixture::toArray();
        $expectedValues = array(
            'FOO' => EnumFixture::FOO,
            'BAR' => EnumFixture::BAR,
            'NUMBER' => EnumFixture::NUMBER,
            'PROBLEMATIC_NUMBER' => EnumFixture::PROBLEMATIC_NUMBER,
            'PROBLEMATIC_NULL' => EnumFixture::PROBLEMATIC_NULL,
            'PROBLEMATIC_EMPTY_STRING' => EnumFixture::PROBLEMATIC_EMPTY_STRING,
            'PROBLEMATIC_BOOLEAN_FALSE' => EnumFixture::PROBLEMATIC_BOOLEAN_FALSE,
        );

        $this->assertSame($expectedValues, $values);
    }

    /**
     * __callStatic()
     */
    public function testStaticAccess(): void
    {
        $this->assertEquals(new EnumFixture(EnumFixture::FOO), EnumFixture::FOO());
        $this->assertEquals(new EnumFixture(EnumFixture::BAR), EnumFixture::BAR());
        $this->assertEquals(new EnumFixture(EnumFixture::NUMBER), EnumFixture::NUMBER());
    }

    public function testBadStaticAccess(): void
    {
        $this->expectException(\BadMethodCallException::class);
        $this->expectExceptionMessage('No static method or enum constant \'UNKNOWN\' in class ' . EnumFixture::class);

        // phpstan:ignoreError "Call to an undefined static method
        // Tests\Unit\Utils\DataTypes\Enum\EnumFixture::UNKNOWN()."
        EnumFixture::UNKNOWN();
    }

    /**
     * isValid()
     * @dataProvider isValidProvider
     * @param mixed $value
     * @param bool $isValid
     */
    public function testIsValid($value, bool $isValid): void
    {
        $this->assertSame($isValid, EnumFixture::isValid($value));
    }

    public function isValidProvider(): array
    {
        return [
            /**
             * Valid values
             */
            ['foo', true],
            [42, true],
            [null, true],
            [0, true],
            ['', true],
            [false, true],
            /**
             * Invalid values
             */
            ['baz', false]
        ];
    }

    /**
     * isValidKey()
     */
    public function testIsValidKey(): void
    {
        $this->assertTrue(EnumFixture::isValidKey('FOO'));
        $this->assertFalse(EnumFixture::isValidKey('BAZ'));
        $this->assertTrue(EnumFixture::isValidKey('PROBLEMATIC_NULL'));
    }

    /**
     * search()
     * @see https://github.com/myclabs/php-enum/issues/13
     * @dataProvider searchProvider
     * @param mixed $value
     * @param mixed $expected
     */
    public function testSearch($value, $expected): void
    {
        $this->assertSame($expected, EnumFixture::search($value));
    }

    public function searchProvider(): array
    {
        return [
            ['foo', 'FOO'],
            [0, 'PROBLEMATIC_NUMBER'],
            [null, 'PROBLEMATIC_NULL'],
            ['', 'PROBLEMATIC_EMPTY_STRING'],
            [false, 'PROBLEMATIC_BOOLEAN_FALSE'],
            ['bar I do not exist', false],
            [[], false],
        ];
    }

    /**
     * equals()
     */
    public function testEquals(): void
    {
        $foo = new EnumFixture(EnumFixture::FOO);
        $number = new EnumFixture(EnumFixture::NUMBER);
        $anotherFoo = new EnumFixture(EnumFixture::FOO);
        $objectOfDifferentClass = new \stdClass();
        $notAnObject = 'foo';

        $this->assertTrue($foo->equals($foo));
        $this->assertFalse($foo->equals($number));
        $this->assertTrue($foo->equals($anotherFoo));
        $this->assertFalse($foo->equals(null));
        $this->assertFalse($foo->equals($objectOfDifferentClass));
        $this->assertFalse($foo->equals($notAnObject));
    }

    /**
     * equals()
     */
    public function testEqualsComparesProblematicValuesProperly(): void
    {
        $false = new EnumFixture(EnumFixture::PROBLEMATIC_BOOLEAN_FALSE);
        $emptyString = new EnumFixture(EnumFixture::PROBLEMATIC_EMPTY_STRING);
        $null = new EnumFixture(EnumFixture::PROBLEMATIC_NULL);

        $this->assertTrue($false->equals($false));
        $this->assertFalse($false->equals($emptyString));
        $this->assertFalse($emptyString->equals($null));
        $this->assertFalse($null->equals($false));
    }

    /**
     * equals()
     */
    public function testEqualsConflictValues(): void
    {
        $this->assertFalse(EnumFixture::FOO()->equals(EnumConflict::FOO()));
    }

    /**
     * jsonSerialize()
     */
    public function testJsonSerialize(): void
    {
        $this->assertJsonEqualsJson('"foo"', json_encode(new EnumFixture(EnumFixture::FOO)));
        $this->assertJsonEqualsJson('"bar"', json_encode(new EnumFixture(EnumFixture::BAR)));
        $this->assertJsonEqualsJson('42', json_encode(new EnumFixture(EnumFixture::NUMBER)));
        $this->assertJsonEqualsJson('0', json_encode(new EnumFixture(EnumFixture::PROBLEMATIC_NUMBER)));
        $this->assertJsonEqualsJson('null', json_encode(new EnumFixture(EnumFixture::PROBLEMATIC_NULL)));
        $this->assertJsonEqualsJson('""', json_encode(new EnumFixture(EnumFixture::PROBLEMATIC_EMPTY_STRING)));
        $this->assertJsonEqualsJson('false', json_encode(new EnumFixture(EnumFixture::PROBLEMATIC_BOOLEAN_FALSE)));
    }

    public function testNullableEnum(): void
    {
        $this->assertNull(EnumFixture::PROBLEMATIC_NULL()->getValue());
        $this->assertNull((new EnumFixture(EnumFixture::PROBLEMATIC_NULL))->getValue());
        $this->assertNull((new EnumFixture(EnumFixture::PROBLEMATIC_NULL))->jsonSerialize());
    }

    public function testBooleanEnum(): void
    {
        $this->assertFalse(EnumFixture::PROBLEMATIC_BOOLEAN_FALSE()->getValue());
        $this->assertFalse((new EnumFixture(EnumFixture::PROBLEMATIC_BOOLEAN_FALSE))->jsonSerialize());
    }

    public function testConstructWithSameEnumArgument(): void
    {
        $enum = new EnumFixture(EnumFixture::FOO);

        $enveloped = new EnumFixture($enum);

        $this->assertEquals($enum, $enveloped);
    }

    private function assertJsonEqualsJson($json1, $json2): void
    {
        $this->assertJsonStringEqualsJsonString($json1, $json2);
    }

    public function testSerialize(): void
    {
        // split string for Pretty CI: "Line exceeds 120 characters"
        $bin = '4f3a34333a2254657374735c556e69745c5574696c735c4461746154797065735c456e756d5c45' .
            '6e756d46697874757265223a313a7b733a383a22002a0076616c7565223b733a333a22666f6f223b7d';

        $this->assertEquals($bin, bin2hex(serialize(EnumFixture::FOO())));
    }

    public function testUnserialize(): void
    {
        // split string for Pretty CI: "Line exceeds 120 characters"
        $bin = '4f3a34333a2254657374735c556e69745c5574696c735c4461746154797065735c456e756d5c45' .
            '6e756d46697874757265223a313a7b733a383a22002a0076616c7565223b733a333a22666f6f223b7d';

        /* @var $value EnumFixture */
        $value = unserialize(pack('H*', $bin));

        $this->assertEquals(EnumFixture::FOO, $value->getValue());
        $this->assertTrue(EnumFixture::FOO()->equals($value));
    }

    /**
     * @see https://github.com/myclabs/php-enum/issues/95
     */
    public function testEnumValuesInheritance(): void
    {
        $this->expectException(\UnexpectedValueException::class);
        $this->expectExceptionMessage('Value \'value\' is not part of the enum ' . EnumFixture::class);

        $inheritedEnumFixture = InheritedEnumFixture::VALUE();

        new EnumFixture($inheritedEnumFixture);
    }
}
