<?php

namespace Tests\Unit\Utils\DataTypes\Enum;

use App\Utils\DataTypes\Enum\Enum;

/**
 * Class EnumConflict
 *
 * @method static EnumConflict FOO()
 * @method static EnumConflict BAR()
 *
 * @author Daniel Costa <danielcosta@gmail.com>
 * @author Mirosław Filip <mirfilip@gmail.com>
 */
class EnumConflict extends Enum
{
    public const FOO = 'foo';
    public const BAR = 'bar';
}
