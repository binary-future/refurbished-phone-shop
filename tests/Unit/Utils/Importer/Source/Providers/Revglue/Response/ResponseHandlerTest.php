<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Revglue\Response;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Revglue\Response\ResponseHandler;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ResponseHandlerTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getHandler(): ResponseHandler
    {
        return new ResponseHandler($this->config);
    }

    /**
     * @param array $configs
     * @param array $params
     * @param string $context
     * @param array $content
     * @dataProvider validDataProvider
     * @throws SourceException
     */
    public function testResponseBuilding($configs, $params, $context, $content): void
    {
        $scenario = $this->getScenario($context);
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $handler = $this->getHandler();

        $response = $handler->handle($params, $scenario);

        $this->assertEquals($content, $response->getContent());
    }

    public function validDataProvider(): array
    {
        return [
            [
                ['context' => 'stores'],
                ['response' => [
                    'success' => true,
                    'stores' => ['content']]
                ],
                'context',
                ['content']
            ]
        ];
    }

    private function getScenario($context)
    {
        $scenario = $this->getBaseMock(Scenario::class);
        $scenario->expects($this->once())
            ->method('getContext')
            ->willReturn($context);

        return $scenario;
    }
}
