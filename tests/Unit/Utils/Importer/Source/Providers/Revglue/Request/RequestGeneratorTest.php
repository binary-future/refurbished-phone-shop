<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Revglue\Request\Contracts\RequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Revglue\Request\RequestGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class RequestGeneratorTest extends TestCase
{
    use BaseMock;

    private $container;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->container = $this->getBaseMock(Container::class);
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->container,
            $this->config
        );
        parent::tearDown();
    }

    private function getTestObject(): RequestGenerator
    {
        return new RequestGenerator($this->container, $this->config);
    }

    /**
     * @param string $context
     * @param array $config
     * @param string $generator
     * @param string $result
     * @dataProvider validDataProvider
     * @throws SourceException
     */
    public function testGenerateLinkByScenario($context, $config, $generator, $result): void
    {
        $scenario = $this->getScenario($context);
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($config);
        $this->container->expects($this->once())
            ->method('resolve')
            ->willReturn($this->getGenerator($generator, $result));
        $service = $this->getTestObject();

        $response = $service->generateLink($scenario);

        $this->assertEquals($result, $response);
    }

    public function validDataProvider(): array
    {
        return [
            [
                'context',
                ['context' => RequestLinkGenerator::class],
                RequestLinkGenerator::class,
                'link'
            ]
        ];
    }


    private function getGenerator(string $generator, string $result)
    {
        $mock = $this->getBaseMock($generator);
        $mock->method('generateLink')
            ->willReturn($result);

        return $mock;
    }

    private function getScenario($context)
    {
        $scenario = $this->getBaseMock(Scenario::class);
        $scenario->expects($this->once())
            ->method('getContext')
            ->willReturn($context);

        return $scenario;
    }

    /**
     * @throws SourceException
     */
    public function testGeneratorThrowsException(): void
    {
        $scenario = $this->getScenario('context');
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn([]);
        $service = $this->getTestObject();

        $this->expectException(SourceException::class);

        $service->generateLink($scenario);
    }
}
