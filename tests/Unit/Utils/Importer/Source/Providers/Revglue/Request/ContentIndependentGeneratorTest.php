<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Source\Providers\Revglue\Request\ContentIndependentGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ContentIndependentGeneratorTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getGenerator(): ContentIndependentGenerator
    {
        return new ContentIndependentGenerator($this->config);
    }

    /**
     * @param string $content
     * @param array $configs
     * @param string $result
     * @dataProvider validDataProvider
     */
    public function testGenerateLink(string $content, array $configs, $result): void
    {
        $scenario = $this->getScenario($content);
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $response = $generator->generateLink($scenario);

        $this->assertEquals($result, $response);
    }

    public function validDataProvider(): array
    {
        return [
            [
                'content',
                [
                    'key' => 'key',
                    'domain' => 'domain',
                    'type' => 'type',
                    'product' => [
                        'content' => 'content'
                    ]
                ],
                'domain/content/type/key'
            ]
        ];
    }

    private function getScenario($content)
    {
        $scenario = $this->getBaseMock(Scenario::class);
        $scenario->expects($this->once())
            ->method('getContext')
            ->willReturn($content);

        return $scenario;
    }
}
