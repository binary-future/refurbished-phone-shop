<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\RevglueDealsImport;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Source\Providers\Revglue\Request\ContentDependentGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ContentDependentGeneratorTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getGenerator(): ContentDependentGenerator
    {
        return new ContentDependentGenerator($this->config);
    }

    /**
     * @param array $configs
     * @param string $result
     * @dataProvider validDataProvider
     */
    public function testGenerateLink(array $configs, $result): void
    {
        $scenario = $this->getScenario();
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $response = $generator->generateLink($scenario);

        $this->assertEquals($result, $response);
    }

    public function validDataProvider(): array
    {
        return [
            [
                [
                    'key' => 'key',
                    'domain' => 'domain',
                    'type' => 'type',
                    'product' => [
                        RevglueDealsImport::CONTEXT => 'content'
                    ]
                ],
                'domain/content/type/key/1/2'
            ]
        ];
    }

    private function getScenario(): RevglueDealsImport
    {
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $scenario = new RevglueDealsImport($datafeed);
        $scenario->setCurrentPage(2);

        return $scenario;
    }
}
