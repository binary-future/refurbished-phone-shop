<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Webgains;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\WebgainsDealsImport;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Webgains\RequestLinkGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class RequestLinkGeneratorTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getGenerator(): RequestLinkGenerator
    {
        return new RequestLinkGenerator($this->config);
    }

    /**
     * @param array $configs
     * @param string $response
     * @param WebgainsDealsImport $scenario
     * @throws SourceException
     * @dataProvider validDataProvider
     */
    public function testValidGenerating($configs, $response, $scenario): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $result = $generator->generateLink($scenario);

        $this->assertEquals($response, $result);
    }

    public function validDataProvider(): array
    {
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $scenario = new WebgainsDealsImport($datafeed);

        $key = 'key';
        $domain = 'domain';
        $format = 'csv';
        $separator = 'comma';
        $compression = 'none';
        return [
            [
                [
                    'key' => $key,
                    'domain' => $domain,
                    'format' => $format,
                    'separator' => $separator,
                    'compression' => $compression,
                ],
                "$domain?action=download&campaign=156977&feeds=1&categories=1879,1904&fields=extended" .
                '&fieldIds=deeplink,image_url,merchant_category,price,product_id,product_name,brand,Colour,' .
                'contract_length,contract_type,data_allowance,handset_price,image_url,inclusive_minutes,' .
                "inclusive_texts,mobile_network,network_promotion,normal_price,stock_code&format=$format" .
                "&separator=$separator&zipformat=$compression&allowedtags=all&stripNewlines=0&apikey=$key",
                $scenario,
            ],
        ];
    }

    /**
     * @param WebgainsDealsImport $scenario
     * @param array $configs
     * @dataProvider invalidDataProvider
     * @throws SourceException
     */
    public function testGeneratorThrowsExceptionIfCannotGenerateLink($scenario, $configs): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $this->expectException(SourceException::class);

        $generator->generateLink($scenario);
    }

    public function invalidDataProvider(): array
    {
        $validConfigs = [
            'key' => 'key',
            'domain' => 'domain',
            'format' => 'csv',
            'separator' => 'comma',
            'compression' => 'none',
        ];
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $validScenario = new WebgainsDealsImport($datafeed);
        $invalidScenario = new WebgainsDealsImport(new DatafeedInfo());
        $invalidConfigs = [];

        return [
            [$validScenario, $invalidConfigs],
            [$invalidScenario, $validConfigs],
            [$invalidScenario, $invalidConfigs],
        ];
    }
}
