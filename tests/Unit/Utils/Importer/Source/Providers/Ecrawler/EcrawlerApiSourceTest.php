<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Ecrawler;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\AwinDealsImport;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Scenarios\EcrawlerDealsImport;
use App\Domain\Store\Store;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Encoders\Contracts\JsonEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Awin\AwinApiSource;
use App\Utils\Importer\Source\Providers\Ecrawler\EcrawlerApiSource;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class EcrawlerApiSourceTest extends TestCase
{
    use BaseMock;

    private $contentGetter;

    private $encoder;

    private $linkGeneratorFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->contentGetter = $this->getBaseMock(ContentGetter::class);
        $this->encoder = $this->getBaseMock(JsonEncoder::class);
        $this->linkGeneratorFactory = $this->getBaseMock(LinkGeneratorFactory::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->contentGetter,
            $this->encoder,
            $this->linkGeneratorFactory
        );
        parent::tearDown();
    }

    private function getSource(): EcrawlerApiSource
    {
        return new EcrawlerApiSource(
            $this->contentGetter,
            $this->encoder,
            $this->linkGeneratorFactory
        );
    }

    /**
     * @return RequestLinkGenerator|MockObject
     */
    private function getRequestLinkGenerator()
    {
        return $this->getBaseMock(RequestLinkGenerator::class);
    }

    public function testSourceThrowsExceptionIfInvalidScenario(): void
    {
        $source = $this->getSource();

        $this->expectException(SourceException::class);

        $source->getContent($this->getBaseMock(Scenario::class));
    }

    /**
     * @throws SourceException
     */
    public function testSourceValidScenario(): void
    {
        $encodedResponse = ['response'];
        $requestLinkGenerator = $this->getRequestLinkGenerator();
        $requestLinkGenerator->expects($this->once())
            ->method('generateLink')
            ->willReturn('link');
        $this->linkGeneratorFactory->expects($this->once())
            ->method('getRequestGenerator')
            ->willReturn($requestLinkGenerator);
        $this->contentGetter->expects($this->once())
            ->method('getContent')
            ->willReturn('content');
        $this->encoder->expects($this->once())
            ->method('decode')
            ->willReturn($encodedResponse);
        $datafeed = $this->getBaseMock(DatafeedInfo::class);
        $source = $this->getSource();

        $result = $source->getContent(new EcrawlerDealsImport($datafeed));

        $this->assertEquals($encodedResponse, $result->getContent());
    }
}
