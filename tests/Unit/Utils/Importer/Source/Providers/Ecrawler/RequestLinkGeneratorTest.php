<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Ecrawler;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\EcrawlerDealsImport;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Ecrawler\RequestLinkGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class RequestLinkGeneratorTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getGenerator(): RequestLinkGenerator
    {
        return new RequestLinkGenerator($this->config);
    }

    /**
     * @param array $configs
     * @param string $response
     * @param EcrawlerDealsImport $scenario
     * @throws SourceException
     * @dataProvider validDataProvider
     */
    public function testValidGenerating(
        array $configs,
        string $response,
        EcrawlerDealsImport $scenario
    ): void {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $result = $generator->generateLink($scenario);

        $this->assertEquals($response, $result);
    }

    public function validDataProvider(): array
    {
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $scenario = new EcrawlerDealsImport($datafeed);

        return [
            [
                [
                    'key' => 'key',
                    'domain' => 'domain',
                ],
                'domain/1?api_key=key',
                $scenario,
            ],
        ];
    }

    /**
     * @param EcrawlerDealsImport $scenario
     * @param array $configs
     * @dataProvider invalidDataProvider
     * @throws SourceException
     */
    public function testGeneratorThrowsExceptionIfCannotGenerateLink($scenario, $configs): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $this->expectException(SourceException::class);

        $generator->generateLink($scenario);
    }

    public function invalidDataProvider(): array
    {
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $validScenario = new EcrawlerDealsImport($datafeed);
        $invalidScenario = new EcrawlerDealsImport(new DatafeedInfo());
        $invalidConfigs = [];

        return [
            [$validScenario, $invalidConfigs],
            [$invalidScenario, $invalidConfigs]
        ];
    }
}
