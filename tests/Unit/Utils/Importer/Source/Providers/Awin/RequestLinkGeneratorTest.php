<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Awin;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\AwinDealsImport;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Awin\RequestLinkGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class RequestLinkGeneratorTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getGenerator(): RequestLinkGenerator
    {
        return new RequestLinkGenerator($this->config);
    }

    /**
     * @param array $configs
     * @param string $response
     * @param AwinDealsImport $scenario
     * @throws SourceException
     * @dataProvider validDataProvider
     */
    public function testValidGenerating(
        array $configs,
        string $response,
        AwinDealsImport $scenario
    ): void {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();
        $result = $generator->generateLink($scenario);
        $this->assertEquals($response, $result);
    }

    public function validDataProvider(): array
    {
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $scenario = new AwinDealsImport($datafeed);

        $columnsLine = implode(',', $scenario->getFields());

        return [
            [
                [
                    'key' => 'key',
                    'domain' => 'domain',
                    'type' => 'csv',
                    'compression' => 'zip',
                ],
                "domain/apikey/key/language/en/fid/1/columns/$columnsLine/format/csv/compression/zip/adultcontent/1/",
                $scenario,
            ],
        ];
    }

    /**
     * @param AwinDealsImport $scenario
     * @param array $configs
     * @dataProvider invalidDataProvider
     * @throws SourceException
     */
    public function testGeneratorThrowsExceptionIfCannotGenerateLink($scenario, $configs): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $generator = $this->getGenerator();

        $this->expectException(SourceException::class);

        $generator->generateLink($scenario);
    }

    public function invalidDataProvider(): array
    {
        $datafeed = new DatafeedInfo();
        $datafeed->setExternalId(1);
        $validScenario = new AwinDealsImport($datafeed);
        $invalidScenario = new AwinDealsImport(new DatafeedInfo());
        $invalidConfigs = [];

        return [
            [$validScenario, $invalidConfigs],
            [$invalidScenario, $invalidConfigs]
        ];
    }
}
