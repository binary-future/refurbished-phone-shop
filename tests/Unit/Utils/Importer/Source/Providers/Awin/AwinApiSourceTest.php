<?php

namespace Tests\Unit\Utils\Importer\Source\Providers\Awin;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\AwinDealsImport;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Domain\Store\Store;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Awin\AwinApiSource;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class AwinApiSourceTest extends TestCase
{
    use BaseMock;

    private $config;

    private $contentGetter;

    private $storage;

    private $zipper;

    private $encoder;

    private $linkGeneratorFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->contentGetter = $this->getBaseMock(ContentGetter::class);
        $this->storage = $this->getBaseMock(Storage::class);
        $this->zipper = $this->getBaseMock(Zipper::class);
        $this->encoder = $this->getBaseMock(CSVEncoder::class);
        $this->linkGeneratorFactory = $this->getBaseMock(LinkGeneratorFactory::class);
    }

    /**
     * @return AwinApiSource
     */
    private function getSource()
    {
        return new AwinApiSource(
            $this->config,
            $this->contentGetter,
            $this->storage,
            $this->zipper,
            $this->encoder,
            $this->linkGeneratorFactory
        );
    }

    protected function tearDown(): void
    {
        unset(
            $this->config,
            $this->contentGetter,
            $this->storage,
            $this->zipper,
            $this->encoder,
            $this->linkGeneratorFactory
        );
        parent::tearDown();
    }

    /**
     * @return RequestLinkGenerator|MockObject
     */
    private function getRequestLinkGenerator()
    {
        return $this->getBaseMock(RequestLinkGenerator::class);
    }

    public function testSourceThrowsExceptionIfInvalidConfigs(): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn([]);
        $source = $this->getSource();

        $this->expectException(SourceException::class);

        $source->getContent(new AwinDealsImport(new DatafeedInfo()));
    }

    public function testSourceThrowsExceptionIfInvalidScenario(): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($this->getValidConfigs());
        $source = $this->getSource();

        $this->expectException(SourceException::class);

        $source->getContent($this->getBaseMock(Scenario::class));
    }

    public function testSourceValidScenario(): void
    {
        $response = ['response'];
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($this->getValidConfigs());
        $this->contentGetter->expects($this->once())
            ->method('getContent')
            ->willReturn('content');
        $this->storage->expects($this->once())
            ->method('put')
            ->willReturn(true);
        $this->zipper->expects($this->any())
            ->method('getNames')
            ->willReturn(['name']);
        $this->zipper->expects($this->any())
            ->method('extractByName')
            ->willReturn(true);
        $this->encoder->expects($this->once())
            ->method('decode')
            ->willReturn($response);
        $requestLinkGenerator = $this->getRequestLinkGenerator();
        $requestLinkGenerator->expects($this->once())
            ->method('generateLink')
            ->willReturn('link');
        $this->linkGeneratorFactory->expects($this->once())
            ->method('getRequestGenerator')
            ->willReturn($requestLinkGenerator);
        $store = $this->getBaseMock(Store::class);
        $store->expects($this->once())->method('getSlug')
            ->willReturn('slug');
        $datafeed = $this->getBaseMock(DatafeedInfo::class);
        $datafeed->expects($this->once())->method('getStore')
            ->willReturn($store);
        $source = $this->getSource();

        $result = $source->getContent(new AwinDealsImport($datafeed));

        $this->assertEquals($response, $result->getContent());
    }

    public function testSourceThrowsExceptionIfNotZipContent(): void
    {
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($this->getValidConfigs());
        $this->contentGetter->expects($this->once())
            ->method('getContent')
            ->willReturn('content');
        $this->storage->expects($this->once())
            ->method('put')
            ->willReturn(true);
        $this->zipper->expects($this->any())
            ->method('getNames')
            ->willReturn([]);
        $this->encoder->expects($this->never())
            ->method('decode');
        $requestLinkGenerator = $this->getRequestLinkGenerator();
        $requestLinkGenerator->expects($this->once())
            ->method('generateLink')
            ->willReturn('link');
        $this->linkGeneratorFactory->expects($this->once())
            ->method('getRequestGenerator')
            ->willReturn($requestLinkGenerator);
        $store = $this->getBaseMock(Store::class);
        $store->expects($this->once())->method('getSlug')->willReturn('store');
        $datafeed = $this->getBaseMock(DatafeedInfo::class);
        $datafeed->expects($this->once())->method('getStore')->willReturn($store);
        $source = $this->getSource();

        $this->expectException(SourceException::class);

        $source->getContent(new AwinDealsImport($datafeed));
    }

    private function getValidConfigs(): array
    {
        return [
            'base_dir' => 'datafeeds',
            'prefix' => 'app',
            'zip_file' => 'datafeeds/zip/datafeed.zip',
        ];
    }
}
