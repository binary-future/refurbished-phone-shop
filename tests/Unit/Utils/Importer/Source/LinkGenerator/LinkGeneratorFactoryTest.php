<?php

namespace Tests\Unit\Utils\Importer\Source\LinkGenerator;

use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\LinkGenerator\LinkGeneratorFactory;
use App\Utils\Importer\Source\Providers\Awin\RequestLinkGenerator as AwinRequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Ecrawler\RequestLinkGenerator as EcrawlerRequestLinkGenerator;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

class LinkGeneratorFactoryTest extends TestCase
{
    use BaseMock;

    private $container;

    protected function setUp(): void
    {
        parent::setUp();
        $this->container = $this->getBaseMock(Container::class);
    }

    protected function tearDown(): void
    {
        unset($this->container);
        parent::tearDown();
    }

    private function getFactory(): LinkGeneratorFactory
    {
        return new LinkGeneratorFactory($this->container);
    }

    /**
     * @param string $datafeedName
     * @param string $expectedClass
     * @throws \App\Utils\Importer\Exceptions\SourceException
     * @dataProvider generationDataProvider
     */
    public function testGeneration(
        string $datafeedName,
        string $expectedClass
    ): void {
        $generator = $this->getBaseMock($expectedClass);
        $this->container->expects($this->once())
            ->method('resolve')
            ->with($expectedClass)
            ->willReturn($generator);
        $factory = $this->getFactory();

        $actualGenerator = $factory->getRequestGenerator($datafeedName);

        $this->assertInstanceOf($expectedClass, $actualGenerator);
    }

    public function generationDataProvider(): array
    {
        return [
//            [LinkGeneratorFactory::DATAFEED_REVGLUE, RevglueRequestGenerator::class],
            [LinkGeneratorFactory::DATAFEED_AWIN, AwinRequestLinkGenerator::class],
            [LinkGeneratorFactory::DATAFEED_ECRAWLER, EcrawlerRequestLinkGenerator::class],
//            [LinkGeneratorFactory::DATAFEED_WEBGAINS, WebgainsRequestLinkGenerator::class],
        ];
    }

    public function testGenerationIfContainerThrowException(): void
    {
        $this->container->expects($this->once())
            ->method('resolve')
            ->willThrowException(new SourceException());
        $factory = $this->getFactory();

        $this->expectException(SourceException::class);

        $factory->getRequestGenerator(LinkGeneratorFactory::DATAFEED_AWIN);
    }

    public function testGenerationIfGeneratorAbsent(): void
    {
        $this->container->expects($this->never())
            ->method('resolve');
        $factory = $this->getFactory();

        $this->expectException(SourceException::class);

        $factory->getRequestGenerator('randomname');
    }
}
