<?php

namespace Tests\Unit\Utils\Importer\Handlers;

use App\Application\Services\Importer\Handler;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;
use App\Utils\Importer\Handlers\Factory;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class FactoryTest extends TestCase
{
    use BaseMock;

    private $config;

    private $container;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->container = $this->getBaseMock(Container::class);
    }

    private function getFactory()
    {
        return new Factory($this->config, $this->container);
    }

    /**
     * @throws ImportHandlerBuildException
     */
    public function testBuildHandler()
    {
        $alias = 'alias';
        $configs = [$alias =>
            [
                'source' => Source::class,
                'transformer' => Transformer::class,
                'importer' => Importer::class,
                'handler' => \App\Utils\Importer\Handlers\Handler::class
            ]
        ];
        $scenario = $this->getScenario($alias);
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $this->manageContainer($configs[$alias]);
        $factory = $this->getFactory();

        $result = $factory->buildHandler($scenario);

        $this->assertInstanceOf(Handler::class, $result);
    }

    private function manageContainer(array $configs)
    {
        $mocks = [];
        foreach ($configs as $className) {
            $mocks[$className] = [$className, [], $this->getBaseMock($className)];
        }

        $this->container->method('resolve')
            ->willReturnMap($mocks);
    }

    private function getScenario(string $alias)
    {
        $scenario = $this->getBaseMock(Scenario::class);
        $scenario->method('getContext')
            ->willReturn($alias);

        return $scenario;
    }

    /**
     * @param string $alias
     * @param array $configs
     * @dataProvider invalidConfigsDataProvider
     */
    public function testHandlerThrowsException(string $alias, array $configs)
    {
        $scenario = $this->getScenario($alias);
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn($configs);
        $this->manageContainer($configs[$alias]);
        $factory = $this->getFactory();
        $this->expectException(ImportHandlerBuildException::class);
        $factory->buildHandler($scenario);
    }

    public function invalidConfigsDataProvider()
    {
        return [
            [
                'alias',
                ['alias' =>
                    [
                        'transformer' => Transformer::class,
                        'importer' => Importer::class,
                        'source' => Source::class,
                    ]
                ]
            ],
            [
                'alias',
                ['alias' =>
                    [
                        'transformer' => Transformer::class,
                        'importer' => Importer::class,
                        'source' => Handler::class,
                        'handler' => Handler::class,
                    ]
                ]
            ],
        ];
    }
}
