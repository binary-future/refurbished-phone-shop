<?php


namespace Tests\Unit\Domain\Shared\Image\Services\Thumb;


use App\Domain\Shared\Image\Services\Thumb\Exceptions\ThumbGenerationException;
use App\Domain\Shared\Image\Services\Thumb\ThumbGenerator;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\File\Resizer\Contract\ImageResizer;
use App\Utils\File\Resizer\Exception\ImageResizerException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ThumbGeneratorTest extends TestCase
{
    use BaseMock;

    private $resizer;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->resizer = $this->getBaseMock(ImageResizer::class);
    }

    private function getGenerator(): ThumbGenerator
    {
        return new ThumbGenerator($this->config, $this->resizer);
    }

    public function testEmptyResultIfNotLocalImage()
    {
        $this->config->method('get')->willReturn([]);
        $generator = $this->getGenerator();
        $result = $generator->generateThumb(new Image());
        $this->assertTrue($result->isEmpty());
    }

    public function testEmptyResultIfThumb()
    {
        $this->config->method('get')->willReturn([]);
        $generator = $this->getGenerator();
        $image = new Image();
        $image->parent_id = 1;
        $result = $generator->generateThumb($image);
        $this->assertTrue($result->isEmpty());
    }

    /**
     * @param array $config
     * @param Image $image
     * @param $thumbPath
     * @dataProvider validDataProvider
     */
    public function testThumbsGeneration(array $config, Image $image, $thumbPath)
    {
        $this->config->method('get')->willReturn($config);
        $this->resizer->method('resize')->willReturn(public_path($thumbPath));
        $generator = $this->getGenerator();
        $result = $generator->generateThumb($image);
        $this->assertEquals($thumbPath, $result->first()->getPath());
        $this->assertEquals($image->id, $result->first()->parent_id);
    }

    public function validDataProvider()
    {
        $configs = $this->validConfig();
        $phone = new Phone();
        $phone->id = 10;
        $image = new Image();
        $image->id = 1;
        $image->setIsLocal(true);
        $image->setPath('image.png');
        $image->setOwner($phone);

        return [
            [$configs, $image, 'image_300x300.png']
        ];
    }

    /**
     * @param array $config
     * @param Image $image
     * @throws ThumbGenerationException
     * @dataProvider invalidConfigDataProvider
     */
    public function testThrowExceptionIfInvalidConfig(?array $config, Image $image)
    {
        $this->config->method('get')->willReturn($config);
        $this->expectException(ThumbGenerationException::class);
        $generator = $this->getGenerator();
        $generator->generateThumb($image);
    }

    public function invalidConfigDataProvider()
    {
        $image = new Image();
        $image->setIsLocal(true);

        return [
            [null, $image],
        ];
    }

    /**
     * @throws ThumbGenerationException
     */
    public function testThrowsExceptionIfResizerFailed()
    {
        $image = new Image();
        $image->setIsLocal(true);
        $this->config->method('get')->willReturn($this->validConfig());
        $this->resizer->method('resize')->will($this->throwException(new ImageResizerException()));
        $this->expectException(ThumbGenerationException::class);
        $generator = $this->getGenerator();
        $generator->generateThumb($image);
    }

    private function validConfig(): array
    {
        return [
            [
                'height' => 300,
                'width' => 300
            ],
        ];
    }
}
