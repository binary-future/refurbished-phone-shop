<?php

namespace Tests\Unit\Domain\Shared\Image\Specifications;

use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Image\Specifications\ImageSpecification;
use App\Utils\Specification\Specifications\UrlSpecification;
use Tests\TestCase;

class ImageSpecificationTest extends TestCase
{
    private $specification;

    protected function setUp(): void
    {
        parent::setUp();
        $this->specification = new ImageSpecification(new UrlSpecification());
    }

    /**
     * @param string $path
     * @dataProvider validDataProvider
     */
    public function testPositiveSatisfaction(string $path): void
    {
        $image = new Image();
        $image->setPath($path);
        $image->setIsLocal(false);

        $result = $this->specification->isSatisfy($image);

        $this->assertTrue($result);
    }

    public function validDataProvider(): array
    {
        return [
            ['https://www.revglue.com/giffgaff-logo.png'],
            ['https://www.revglue.com/giffgaff-logo.jpg'],
            ['https://www.revglue.com/giffgaff-logo.JPEG'],
            ['https://www.revglue.com/giffgaff-logo.Gif'],
        ];
    }

    /**
     * @param mixed $value
     * @dataProvider invalidDataProvider
     */
    public function testNegativeSatisfaction($value): void
    {
        $result = $this->specification->isSatisfy($value);

        $this->assertFalse($result);
    }

    public function invalidDataProvider(): array
    {
        $imageWithEmptyPath = new Image();
        $imageWithEmptyPath->setPath('');
        $imageWithoutExtension = new Image();
        $imageWithoutExtension->setPath('https://www.revglue.com/logo');
        $imageWithInvalidExtension = new Image();
        $imageWithInvalidExtension->setPath('https://www.revglue.com/logo.tiff');

        return [
            ['string'],
            [new Image()],
            [$imageWithEmptyPath],
            [$imageWithoutExtension],
            [$imageWithInvalidExtension],
        ];
    }
}
