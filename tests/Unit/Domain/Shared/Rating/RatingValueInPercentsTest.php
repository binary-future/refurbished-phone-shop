<?php

namespace Tests\Unit\Domain\Shared\Rating;

use App\Domain\Shared\Rating\RatingValueInPercents;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class RatingValueInPercentsTest extends TestCase
{
    use BaseMock;

    /**
     * @param int $expectedRating
     * @param mixed $rating
     * @dataProvider createRatingValueInPercentsPosDataProvider
     */
    public function testCreateRatingValueInPercentsPos(int $expectedRating, $rating): void
    {
        $object = new RatingValueInPercents($rating);

        $this->assertEquals($expectedRating, $object->getRating());
    }

    public function createRatingValueInPercentsPosDataProvider(): array
    {
        return [
            [RatingValueInPercents::MAX_RATING, RatingValueInPercents::MAX_RATING],
            [RatingValueInPercents::MIN_RATING, RatingValueInPercents::MIN_RATING],
            [-0, 0],
            [10, 10.5],
            [10, '10'],
        ];
    }

    /**
     * @param int $rating
     * @dataProvider createRatingValueInPercentsNegDataProvider
     */
    public function testCreateRatingValueInPercentsNeg(int $rating): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new RatingValueInPercents($rating);
    }

    public function createRatingValueInPercentsNegDataProvider(): array
    {
        return [
            [-1],
            [101],
        ];
    }
}
