<?php

namespace Tests\Unit\Domain\Shared\Link\Specifications;

use App\Domain\Shared\Link\Link;
use App\Utils\Specification\Contracts\Specification;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class LinkSpecificationTest extends TestCase
{
    use BaseMock;

    private $specification;

    protected function setUp(): void
    {
        parent::setUp();
        $this->specification = $this->getBaseMock(Specification::class);
    }

    private function getTestSpecification()
    {
        return new \App\Domain\Shared\Link\Specifications\LinkSpecification($this->specification);
    }

    public function testValidDescription()
    {
        $link = new Link();
        $link->setLink('http://example.com');
        $this->specification->method('isSatisfy')
            ->willReturn(true);
        $testObject = $this->getTestSpecification();
        $this->assertTrue($testObject->isSatisfy($link));
    }

    /**
     * @param $link
     * @dataProvider invalidDataProvider
     */
    public function testInvalidDescription($link)
    {
        $this->specification->method('isSatisfy')
            ->willReturn(false);
        $result = $this->specification->isSatisfy($link);
        $this->assertFalse($result);
    }

    public function invalidDataProvider()
    {
        $link = new Link();
        $link->setLink('');
        return [
            ['string'],
            [new Link()],
            [$link],
        ];
    }
}
