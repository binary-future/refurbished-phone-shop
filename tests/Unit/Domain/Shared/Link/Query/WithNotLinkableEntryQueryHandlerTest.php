<?php


namespace Tests\Unit\Domain\Shared\Link\Query;

use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\ByKeyQuery;
use App\Domain\Deals\Deal\Query\ByKeyQueryHandler;
use App\Domain\Deals\Deal\Repository\Deals;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Link\Query\WithNoLinkableEntryQuery;
use App\Domain\Shared\Link\Query\WithNoLinkableEntryQueryHandler;
use App\Domain\Shared\Link\Repository\Links;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class WithNotLinkableEntryQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Links::class);
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }

    private function getHandler(): WithNoLinkableEntryQueryHandler
    {
        return new WithNoLinkableEntryQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $this->repository->method('findBy')
            ->willReturn(collect([new Link()]));
        $handler = $this->getHandler();

        $response = $handler->handle(new WithNoLinkableEntryQuery(Link::TYPE_DEAL));

        $this->assertInstanceOf(Link::class, $response->first());
    }
}
