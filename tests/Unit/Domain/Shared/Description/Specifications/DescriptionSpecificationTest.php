<?php

namespace Tests\Unit\Domain\Shared\Description\Specifications;

use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\Specifications\DescriptionSpecification;
use Tests\TestCase;

final class DescriptionSpecificationTest extends TestCase
{
    private $specification;

    protected function setUp(): void
    {
        parent::setUp();
        $this->specification = new DescriptionSpecification();
    }

    public function testValidDescription()
    {
        $description = new Description();
        $description->setContent('Hello');
        $result = $this->specification->isSatisfy($description);
        $this->assertTrue($result);
    }

    /**
     * @param $description
     * @dataProvider invalidDataProvider
     */
    public function testInvalidDescription($description)
    {
        $result = $this->specification->isSatisfy($description);
        $this->assertFalse($result);
    }

    public function invalidDataProvider()
    {
        $description = new Description();
        $description->setContent('');
        return [
            ['string'],
            [new Description()],
            [$description],
        ];
    }
}
