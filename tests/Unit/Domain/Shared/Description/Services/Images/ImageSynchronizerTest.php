<?php


namespace Tests\Unit\Domain\Shared\Description\Services\Images;


use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\DescriptionImage;
use App\Domain\Shared\Description\Repository\DescriptionImageRepository;
use App\Domain\Shared\Description\Services\Images\DescriptionImageParser;
use App\Domain\Shared\Description\Services\Images\ImageSynchronizer;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class ImageSynchronizerTest extends TestCase
{
    use BaseMock;

    private $syncService;

    private $repository;

    private $imageParser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(DescriptionImageRepository::class);
        $this->imageParser = $this->getBaseMock(DescriptionImageParser::class);
        $this->syncService = new ImageSynchronizer($this->repository, $this->imageParser);
    }

    /**
     * @param Description $description
     * @param Collection $images
     * @param bool $syncResponse
     * @dataProvider syncDataProvider
     */
    public function testSyncronization(Description $description, Collection $images, bool $syncResponse)
    {
        $this->imageParser->method('parseImages')
            ->with($description)
            ->willReturn($images);
        $this->repository->method('sync')
            ->with($description, $images)
            ->willReturn($syncResponse);

        $this->assertEquals($syncResponse, $this->syncService->synchronize($description));
    }

    public function syncDataProvider()
    {
        $description = new Description();
        $description->id = 1;
        $images = collect(
            [
                new DescriptionImage('path', 1),
                new DescriptionImage('path1', 1),
            ]
        );
        $syncResponse = true;

        return [
            [$description, $images, $syncResponse],
            [$description, collect(), $syncResponse],
            [$description, $images, false]
        ];
    }
}
