<?php

namespace Tests\Unit\Domain\Shared\Description\Services\Images;

use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\DescriptionImage;
use App\Domain\Shared\Description\Services\Images\DescriptionImageParser;
use App\Utils\Adapters\Config\Contracts\Config;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DescriptionImageParserTest extends TestCase
{
    use BaseMock;

    private $config;

    /**
     * @var DescriptionImageParser
     */
    private $parser;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
        $this->config->method('get')->willReturn(['storage_suffix' => 'storage']);
        $this->parser = new DescriptionImageParser($this->config);
    }

    /**
     * @param Description $description
     * @param Collection $images
     * @dataProvider descriptionDataProvider
     */
    public function testImageParsing(Description $description, Collection $images)
    {
        $result = $this->parser->parseImages($description);
        $this->assertEquals($images->count(), $result->count());
        $this->assertEquals($images, $result);
    }

    public function descriptionDataProvider()
    {
        $description = new Description();
        $description->id = 1;
        $description->setContent('<p class="asd"><img class="asdd" src="storage/image1"><img src="image2"></p>');
        $images = collect([
            new DescriptionImage('storage/image1', 1),
        ]);

        $descriptionEmpty = new Description();
        $descriptionEmpty->id = 2;
        $descriptionEmpty->setContent('<p></p>');

        return [
            [$description, $images],
            [$descriptionEmpty, collect()]
        ];
    }
}
