<?php

namespace Tests\Unit\Domain\Store\Command;

use App\Domain\Store\Commands\DeleteStoreCommand;
use App\Domain\Store\Commands\DeleteStoreCommandHandler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DeleteStoreCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Stores::class);
    }

    private function getHandler()
    {
        return new DeleteStoreCommandHandler($this->repository);
    }

    public function testHandler()
    {
        $store = new Store();
        $this->repository->method('delete')
            ->willReturn(true);
        $handler = $this->getHandler();
        $response = $handler->handle(new DeleteStoreCommand($store));
        $this->assertEquals($store, $response);
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }
}
