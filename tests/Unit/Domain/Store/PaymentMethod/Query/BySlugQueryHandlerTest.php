<?php

namespace Tests\Unit\Domain\Store\PaymentMethod\Query;

use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Query\BySlugQuery;
use App\Domain\Store\PaymentMethod\Query\BySlugQueryHandler;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BySlugQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(PaymentMethods::class);
    }

    private function getHandler()
    {
        return new BySlugQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $this->repository->method('findBy')
            ->willReturn(collect([new PaymentMethod()]));
        $handler = $this->getHandler();

        $response = $handler->handle(new BySlugQuery('slug'));

        $this->assertInstanceOf(PaymentMethod::class, $response->first());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->repository);
    }
}
