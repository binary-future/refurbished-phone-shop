<?php

namespace Tests\Unit\Domain\Store\PaymentMethod\Command;

use App\Domain\Store\PaymentMethod\Commands\CreatePaymentMethodCommand;
use App\Domain\Store\PaymentMethod\Commands\CreatePaymentMethodCommandHandler;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class CreatePaymentMethodCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(PaymentMethods::class);
    }

    private function getHandler()
    {
        return new CreatePaymentMethodCommandHandler($this->repository);
    }

    public function testHandler()
    {
        $paymentMethod = new PaymentMethod();
        $this->repository->method('save')
            ->willReturn($paymentMethod);
        $handler = $this->getHandler();

        $response = $handler->handle(new CreatePaymentMethodCommand($paymentMethod));

        $this->assertEquals($paymentMethod, $response);
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }
}
