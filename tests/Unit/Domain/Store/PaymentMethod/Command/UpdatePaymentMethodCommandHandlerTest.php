<?php

namespace Tests\Unit\Domain\Store\PaymentMethod\Command;

use App\Domain\Shared\Image\Image;
use App\Domain\Store\PaymentMethod\Commands\UpdatePaymentMethodCommand;
use App\Domain\Store\PaymentMethod\Commands\UpdatePaymentMethodCommandHandler;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UpdatePaymentMethodCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(PaymentMethods::class);
    }

    private function getHandler()
    {
        return new UpdatePaymentMethodCommandHandler($this->repository);
    }

    public function testHandler()
    {
        $image = new Image();
        $paymentMethod = $this->getBaseMock(PaymentMethod::class);
        $paymentMethod->expects($this->once())->method('setRelation')
            ->with(PaymentMethod::RELATION_IMAGE, $image);
        $paymentMethod->expects($this->once())->method('fill');
        $this->repository->method('save')
            ->willReturn($paymentMethod);
        $handler = $this->getHandler();

        $response = $handler->handle(
            new UpdatePaymentMethodCommand($paymentMethod, [], [PaymentMethod::RELATION_IMAGE => $image])
        );

        $this->assertEquals($paymentMethod, $response);
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }
}
