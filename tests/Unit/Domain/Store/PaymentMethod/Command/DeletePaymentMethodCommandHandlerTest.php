<?php

namespace Tests\Unit\Domain\Store\PaymentMethod\Command;

use App\Domain\Store\Commands\DeleteStoreCommand;
use App\Domain\Store\Commands\DeleteStoreCommandHandler;
use App\Domain\Store\PaymentMethod\Commands\DeletePaymentMethodCommand;
use App\Domain\Store\PaymentMethod\Commands\DeletePaymentMethodCommandHandler;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DeletePaymentMethodCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(PaymentMethods::class);
    }

    private function getHandler()
    {
        return new DeletePaymentMethodCommandHandler($this->repository);
    }

    public function testHandler()
    {
        $paymentMethod = new PaymentMethod();
        $this->repository->method('delete')
            ->willReturn(true);
        $handler = $this->getHandler();

        $response = $handler->handle(new DeletePaymentMethodCommand($paymentMethod));

        $this->assertEquals($paymentMethod, $response);
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }
}
