<?php

namespace Tests\Unit\Domain\Store\Query;

use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\ByAliasAndBrandIdQuery;
use App\Domain\Phone\Model\Query\ByAliasAndBrandIdQueryHandler;
use App\Domain\Phone\Model\Repository\Models;
use App\Domain\Store\Query\ActiveQuery;
use App\Domain\Store\Query\ActiveQueryHandler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ActiveQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Stores::class);
    }

    private function getHandler()
    {
        return new ActiveQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $this->repository->method('findBy')
            ->willReturn(collect([new Store()]));
        $handler = $this->getHandler();
        $response = $handler->handle(new ActiveQuery());
        $this->assertInstanceOf(Store::class, $response->first());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->repository);
    }
}
