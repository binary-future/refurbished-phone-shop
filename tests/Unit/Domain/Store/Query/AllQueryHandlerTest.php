<?php


namespace Tests\Unit\Domain\Store\Query;

use App\Domain\Store\Query\AllQuery;
use App\Domain\Store\Query\AllQueryHandler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class AllQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Stores::class);
    }

    private function getHandler()
    {
        return new AllQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $this->repository->method('findBy')
            ->willReturn(collect([new Store()]));
        $handler = $this->getHandler();
        $response = $handler->handle(new AllQuery());
        $this->assertInstanceOf(Store::class, $response->first());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->repository);
    }
}
