<?php


namespace Tests\Unit\Domain\Store\Query;


use App\Domain\Store\Query\ActiveQuery;
use App\Domain\Store\Query\ActiveQueryHandler;
use App\Domain\Store\Query\ByAliasQuery;
use App\Domain\Store\Query\ByAliasQueryHandler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ByAliasQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Stores::class);
    }

    private function getHandler()
    {
        return new ByAliasQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $this->repository->method('findBy')
            ->willReturn(collect([new Store()]));
        $handler = $this->getHandler();
        $response = $handler->handle(new ByAliasQuery('alias'));
        $this->assertInstanceOf(Store::class, $response->first());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->repository);
    }
}
