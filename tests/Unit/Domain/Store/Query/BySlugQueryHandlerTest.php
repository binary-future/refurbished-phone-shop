<?php


namespace Tests\Unit\Domain\Store\Query;


use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Query\BySlugQueryHandler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BySlugQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Stores::class);
    }

    private function getHandler()
    {
        return new BySlugQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $this->repository->method('findBy')
            ->willReturn(collect([new Store()]));
        $handler = $this->getHandler();
        $response = $handler->handle(new BySlugQuery('slug'));
        $this->assertInstanceOf(Store::class, $response->first());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->repository);
    }
}
