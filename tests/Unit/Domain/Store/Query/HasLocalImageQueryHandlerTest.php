<?php


namespace Tests\Unit\Domain\Store\Query;


use App\Domain\Store\Query\HasLocalImageQuery;
use App\Domain\Store\Query\HasLocalImageQueryHandler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class HasLocalImageQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $stores;

    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->stores = $this->getBaseMock(Stores::class);
        $this->handler = new HasLocalImageQueryHandler($this->stores);
    }

    public function testHandling()
    {
        $stores = collect(new Store());
        $this->stores->method('findBy')
            ->willReturn($stores);

        $this->assertEquals($stores, $this->handler->handle(new HasLocalImageQuery()));
    }

    protected function tearDown(): void
    {
        unset($this->stores, $this->handler);
        parent::tearDown();
    }
}
