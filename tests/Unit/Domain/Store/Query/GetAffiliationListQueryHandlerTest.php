<?php


namespace Tests\Unit\Domain\Store\Query;


use App\Domain\Store\Affiliation\Affiliation;
use App\Domain\Store\Query\GetAffiliationListQuery;
use App\Domain\Store\Query\GetAffiliationListQueryHandler;
use App\Utils\Adapters\Config\Contracts\Config;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class GetAffiliationListQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    public function testHandling()
    {
        $this->config->method('get')
            ->willReturn(['test' => 'test']);
        $handler = new GetAffiliationListQueryHandler($this->config);
        $result = $handler->handle(new GetAffiliationListQuery());
        $this->assertInstanceOf(Collection::class, $result);
        $this->assertInstanceOf(Affiliation::class, $result->first());
    }

}
