<?php

namespace Tests\Unit\Domain\User\Query;

use App\Domain\User\Query\GetUserByIdQuery;
use App\Domain\User\Query\GetUserByIdQueryHandler;
use App\Domain\User\Repository\Users;
use App\Domain\User\User;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class GetUserByIdQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $users;

    /**
     * @var GetUserByIdQueryHandler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->users = $this->getBaseMock(Users::class);
        $this->handler = new GetUserByIdQueryHandler($this->users);
    }

    public function testHandling()
    {
        $users = collect([new User()]);
        $this->users->expects($this->once())
            ->method('findBy')
            ->willReturn($users);
        $response = $this->handler->handle(new GetUserByIdQuery(1));
        $this->assertEquals($users, $response);
    }

    protected function tearDown(): void
    {
        unset($this->users, $this->handler);
        parent::tearDown();
    }
}
