<?php

namespace Tests\Unit\Domain\User\Query;

use App\Domain\User\Query\GetNotSuperAdminUsersExceptUserQuery;
use App\Domain\User\Query\GetNotSuperAdminUsersExceptUserQueryHandler;
use App\Domain\User\Repository\Users;
use App\Domain\User\User;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class GetNotSuperAdminUsersExceptUserQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $repository;

    /**
     * @var GetNotSuperAdminUsersExceptUserQueryHandler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(Users::class);
    }

    private function getHandler()
    {
        return new GetNotSuperAdminUsersExceptUserQueryHandler($this->repository);
    }

    public function testHandlerQuery()
    {
        $users = collect([new User()]);
        $this->repository->method('findBy')
            ->willReturn($users);
        $handler = $this->getHandler();
        $response = $handler->handle(new GetNotSuperAdminUsersExceptUserQuery(new User()));
        $this->assertInstanceOf(User::class, $response->first());
        $this->assertEquals($users, $response);
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->handler);
        parent::tearDown();
    }
}
