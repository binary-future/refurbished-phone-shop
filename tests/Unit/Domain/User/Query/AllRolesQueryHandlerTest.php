<?php

namespace Tests\Unit\Domain\User\Query;

use App\Domain\User\Query\AllRolesQuery;
use App\Domain\User\Query\AllRolesQueryHandler;
use App\Domain\User\Repository\Roles;
use App\Domain\User\Role;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class AllRolesQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $roles;

    /**
     * @var AllRolesQueryHandler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->roles = $this->getBaseMock(Roles::class);
        $this->handler = new AllRolesQueryHandler($this->roles);
    }

    public function testHandling()
    {
        $roles = collect([new Role()]);
        $this->roles->expects($this->once())
            ->method('findBy')
            ->willReturn($roles);
        $result = $this->handler->handle(new AllRolesQuery());
        $this->assertEquals($roles, $result);
    }

    protected function tearDown(): void
    {
        unset($this->roles, $this->handler);
        parent::tearDown();
    }
}
