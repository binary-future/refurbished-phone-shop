<?php
/**
 * Created by PhpStorm.
 * User: Levas
 * Date: 28.05.2019
 * Time: 12:06
 */

namespace Tests\Unit\Domain\User\Query;


use App\Domain\User\Query\AllRolesExceptRoleQuery;
use App\Domain\User\Query\AllRolesExceptRoleQueryHandler;
use App\Domain\User\Repository\Roles;
use App\Domain\User\Role;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class AllRolesExceptRoleQueryHandlerTest extends TestCase
{
    use BaseMock;

    private $roles;

    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->roles = $this->getBaseMock(Roles::class);
        $this->handler = new AllRolesExceptRoleQueryHandler($this->roles);
    }

    public function testHandling()
    {
        $roles = collect([new Role()]);
        $this->roles->expects($this->once())
            ->method('findBy')
            ->willReturn($roles);
        $result = $this->handler->handle(new AllRolesExceptRoleQuery('role'));
        $this->assertEquals($roles, $result);
    }

    protected function tearDown(): void
    {
        unset($this->roles, $this->handler);
        parent::tearDown();
    }
}
