<?php

namespace Tests\Unit\Domain\User\Command;

use App\Domain\User\Command\DeleteUserCommand;
use App\Domain\User\Command\DeleteUserCommandHandler;
use App\Domain\User\Repository\Users;
use App\Domain\User\User;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DeleteUserCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $users;

    /**
     * @var DeleteUserCommandHandler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->users = $this->getBaseMock(Users::class);
        $this->handler = new DeleteUserCommandHandler($this->users);
    }

    public function testHandling()
    {
        $this->users->method('delete')
            ->willReturn(true);
        $user = new User();
        $this->assertEquals($user, $this->handler->handle(new DeleteUserCommand($user)));
    }

    protected function tearDown(): void
    {
        unset($this->users, $this->handler);
        parent::tearDown();
    }
}
