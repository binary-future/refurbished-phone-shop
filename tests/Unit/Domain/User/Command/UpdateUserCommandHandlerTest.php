<?php

namespace Tests\Unit\Domain\User\Command;

use App\Domain\User\Command\UpdateUserCommand;
use App\Domain\User\Command\UpdateUserCommandHandler;
use App\Domain\User\Repository\Users;
use App\Domain\User\User;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UpdateUserCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $users;

    /**
     * @var UpdateUserCommandHandler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->users = $this->getBaseMock(Users::class);
        $this->handler = new UpdateUserCommandHandler($this->users);
    }

    public function testHandling()
    {
        $user = new User();
        $this->users->method('update')->willReturn($user);
        $this->assertEquals($user, $this->handler->handle(new UpdateUserCommand($user, [])));
    }
}
