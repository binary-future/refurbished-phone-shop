<?php

namespace Tests\Unit\Domain\User\Command;

use App\Domain\User\Command\CreateUserCommand;
use App\Domain\User\Command\CreateUserCommandHandler;
use App\Domain\User\Repository\Users;
use App\Domain\User\User;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class CreateUserCommandHandlerTest extends TestCase
{
    use BaseMock;

    private $users;

    /**
     * @var CreateUserCommandHandler
     */
    private $handler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->users = $this->getBaseMock(Users::class);
        $this->handler = new CreateUserCommandHandler($this->users);
    }

    public function testHandling()
    {
        $user = new User();
        $this->users->expects($this->once())->method('save')->willReturn($user);
        $response = $this->handler->handle(new CreateUserCommand($user));
        $this->assertEquals($user, $response);
    }

    protected function tearDown(): void
    {
        unset($this->users, $this->handler);
        parent::tearDown();
    }
}
