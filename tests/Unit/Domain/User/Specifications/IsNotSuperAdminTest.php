<?php

namespace Tests\Unit\Domain\User\Specifications;

use App\Domain\User\Role;
use App\Domain\User\Specifications\IsNotSuperAdmin;
use App\Domain\User\User;
use Tests\Unit\Traits\BaseMock;
use Tests\Unit\Utils\Specification\Specifications\BaseSpecificationTest;

final class IsNotSuperAdminTest extends BaseSpecificationTest
{
    use BaseMock;

    protected function getSpecification()
    {
        return new IsNotSuperAdmin();
    }

    public function validValuesDataProvider()
    {
        $role = new Role();
        $role->setTitle(Role::EDITOR);
        $user = $this->getBaseMock(User::class);
        $user->method('getRole')->willReturn($role);
        $userWithoutRole = $this->getBaseMock(User::class);
        $userWithoutRole->method('getRole')->willReturn(null);

        return [
            [$user],
            [$userWithoutRole],
        ];
    }

    public function inValidValuesDataProvider()
    {
        $role = new Role();
        $role->setTitle(Role::SUPER_ADMIN);
        $user = $this->getBaseMock(User::class);
        $user->method('getRole')->willReturn($role);

        return [
            [$user],
            ['user'],
        ];
    }
}