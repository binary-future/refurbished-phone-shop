<?php

namespace Tests\Unit\Domain\User\Specifications;

use App\Domain\User\Specifications\IsSamePassword;
use App\Utils\Adapters\Encryptor\Contract\Encryptor;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class IsSamePasswordTest extends TestCase
{
    use BaseMock;

    /**
     * @var IsSamePassword
     */
    private $specification;

    private $encryptor;

    protected function setUp(): void
    {
        parent::setUp();
        $this->encryptor = $this->getBaseMock(Encryptor::class);
        $this->specification = new IsSamePassword($this->encryptor);
    }

    public function testValid()
    {
        $this->encryptor->method('checkHash')->willReturn(true);
        $this->assertTrue($this->specification->isSatisfy('pass', ['hash' => 'hash']));
    }

    /**
     * @param $value
     * @param $params
     * @dataProvider invalidDataProvider
     */
    public function testIfInvalidParams($value, $params)
    {
        $this->assertFalse($this->specification->isSatisfy($value, $params));
    }

    public function invalidDataProvider()
    {
        return [
            [1, ['hash' => 'hash']],
            [[], ['hash' => 'hash']],
            ['pass', ['hash' => 1]],
            ['pass', ['hashs' => 'hash']],
        ];
    }

    public function testEcryptorFailed()
    {
        $this->encryptor->method('checkHash')->willReturn(false);
        $this->assertFalse($this->specification->isSatisfy('pass', ['hash' => 'hash']));
    }

    protected function tearDown(): void
    {
        unset($this->specification, $this->encryptor);
        parent::tearDown();
    }
}
