<?php

namespace Tests\Unit\Domain\User\Specifications;

use App\Domain\User\Specifications\IsNotHimself;
use App\Domain\User\User;
use Tests\Unit\Utils\Specification\Specifications\BaseSpecificationTest;

final class IsNotHimselfTest extends BaseSpecificationTest
{
    protected function getSpecification()
    {
        return new IsNotHimself();
    }

    public function validValuesDataProvider()
    {
        $user = new User();
        $user->id = 1;
        $authUser = new User();
        $authUser->id = 2;
        return [
            [$user, ['auth' => $authUser]]
        ];
    }

    public function inValidValuesDataProvider()
    {
        $user = new User();
        $user->id = 1;
        $authUser = new User();
        $authUser->id = 2;

        return [
            ['user', ['auth' => $user]],
            [$user, ['auth' => $user]],
            [$user, ['auth' => 'user']],
            [$user, ['auth' => null]],
            [$user, ['auths' => $authUser]],
        ];
    }
}