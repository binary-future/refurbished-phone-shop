<?php

namespace Tests\Unit\Application\Services\Store\Rating;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Store\Rating\StoreRatingManager;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreRatingManagerTest extends TestCase
{
    use BaseMock;

    private $commandBus;
    /**
     * @var StoreRatingManager
     */
    private $storeRatingManager;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->storeRatingManager = new StoreRatingManager($this->commandBus);
    }

    protected function tearDown(): void
    {
        unset($this->commandBus, $this->storeRatingManager);
        parent::tearDown();
    }

    /**
     * @param Rating $rating
     * @param Link $link
     * @dataProvider prepareRatingDataProvider
     */
    public function testPrepareRatingHasRatingHasLink(Rating $rating, Link $link): void
    {
        $storeLink = $this->getBaseMock(Link::class);
        $storeRating = $this->getBaseMock(Rating::class);
        $store = new Store();
        $store->id = 1;
        $store->setRelation(Store::RELATION_RATING, $storeRating);

        $storeRating->expects($this->exactly(2))->method('getLink')
            ->willReturn($storeLink);
        $storeRating->expects($this->once())->method('setRating')
            ->with($rating->getRating());
        $storeLink->expects($this->once())->method('setLink')
            ->with($link->getLink());

        $actualRating = $this->storeRatingManager->prepareRating($store, $rating, $link);

        $this->assertEquals($storeRating, $actualRating);
    }

    /**
     * @param Rating $rating
     * @param Link $link
     * @dataProvider prepareRatingDataProvider
     */
    public function testPrepareRatingHasRatingDoesntHaveLink(Rating $rating, Link $link): void
    {
        $ratingId = 1;
        $storeRating = $this->getBaseMock(Rating::class);
        $storeRating->expects($this->once())->method('getOwnerKey')
            ->willReturn($ratingId);
        $storeRating->expects($this->once())->method('getOwnerType')
            ->willReturn(Rating::OWNER_TYPE);
        $storeRating->expects($this->once())->method('getLink')
            ->willReturn(null);
        $store = new Store();
        $store->id = 1;
        $store->setRelation(Store::RELATION_RATING, $storeRating);

        $storeRating->expects($this->once())->method('setRating')
            ->with($rating->getRating());
        $storeRating->expects($this->once())->method('setRelation')
            ->with(Rating::RELATION_LINK, $link);

        $actualRating = $this->storeRatingManager->prepareRating($store, $rating, $link);

        $this->assertEquals($ratingId, $link->getLinkableId());
        $this->assertEquals(Rating::OWNER_TYPE, $link->getLinkableType());
        $this->assertEquals($storeRating, $actualRating);
    }

    /**
     * @param Rating $rating
     * @param Link $link
     * @dataProvider prepareRatingDataProvider
     */
    public function testPrepareRatingDoesntHaveRatingAndLinkSuccess(Rating $rating, Link $link): void
    {
        $store = new Store();
        $store->id = 1;
        $store->setRelation(Store::RELATION_RATING, null);
        $ratingId = 1;
        $storeRating = $this->getBaseMock(Rating::class);
        $storeRating->expects($this->once())->method('getOwnerKey')
            ->willReturn($ratingId);
        $storeRating->expects($this->once())->method('getOwnerType')
            ->willReturn(Rating::OWNER_TYPE);
        $storeWithRating = clone $store;
        $storeWithRating->setRelation(Store::RELATION_RATING, $storeRating);

        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($storeWithRating);
        $storeRating->expects($this->once())->method('setRelation')
            ->with(Rating::RELATION_LINK, $link);

        $actualRating = $this->storeRatingManager->prepareRating($store, $rating, $link);

        $this->assertEquals($ratingId, $link->getLinkableId());
        $this->assertEquals(Rating::OWNER_TYPE, $link->getLinkableType());
        $this->assertEquals($store->getKey(), $rating->getRatableId());
        $this->assertEquals(Store::OWNER_TYPE, $rating->getRatableType());
        $this->assertEquals($storeRating, $actualRating);
    }

    /**
     * @param Rating $rating
     * @param Link $link
     * @dataProvider prepareRatingDataProvider
     */
    public function testPrepareRatingCreateRatingStoreWithoutRating(Rating $rating, Link $link): void
    {
        $store = new Store();
        $store->id = 1;
        $store->setRelation(Store::RELATION_RATING, null);
        $storeWithoutRating = clone $store;

        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($storeWithoutRating);

        $this->expectException(\RuntimeException::class);

        $this->storeRatingManager->prepareRating($store, $rating, $link);
    }

    public function prepareRatingDataProvider(): array
    {
        $rating = new Rating([Rating::FIELD_RATING => 50]);
        $link = new Link([Link::FIELD_LINK => 'link']);

        return [
            [$rating, $link]
        ];
    }
}
