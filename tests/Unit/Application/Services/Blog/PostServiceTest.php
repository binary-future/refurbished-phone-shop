<?php

namespace Tests\Unit\Application\Services\Blog;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Blog\Contracts\PostService;
use App\Utils\Adapters\Blog\Post;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PostServiceTest extends TestCase
{
    use BaseMock;

    private $postRepo;

    /**
     * @var \App\Application\Services\Blog\PostService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->postRepo = $this->getBaseMock(PostService::class);
        $this->service = new \App\Application\Services\Blog\PostService($this->postRepo);
    }

    protected function tearDown(): void
    {
        unset($this->postRepo, $this->service);
        parent::tearDown();
    }

    /**
     * @param PhoneModel $model
     * @param array $tags
     * @param Collection $tagPosts
     * @param Collection $generalPosts
     * @param Collection $response
     * @dataProvider notEnoughDataProvider
     */
    public function testIfNotEnoughPostsByTag(
        PhoneModel $model,
        array $tags,
        Collection $tagPosts,
        Collection $generalPosts,
        Collection $response
    ) {
        $this->postRepo->expects($this->once())->method('byTags')
            ->with($tags, 3)->willReturn($tagPosts);
        $this->postRepo->expects($this->once())
            ->method('lastPostsExceptTags')->with(['homepage'], 3 - $tagPosts->count())
            ->willReturn($generalPosts);

        $actualResponse = $this->service->searchByModel($model);

        $this->assertEquals($response, $actualResponse);
    }

    public function notEnoughDataProvider()
    {
        $tagPostsFirst = $this->getPostsFixture(0);
        $generalPostsFirst = $this->getPostsFixture(3);
        $tagPostsSecond = $this->getPostsFixture(2);
        $generalPostsSecond = $this->getPostsFixture(1);

        return [
            [
                $this->getModelFixture(),
                $this->getTagsFixture(),
                $tagPostsFirst,
                $generalPostsFirst,
                $tagPostsFirst->merge($generalPostsFirst)
            ],
            [
                $this->getModelFixture(),
                $this->getTagsFixture(),
                $tagPostsSecond,
                $generalPostsSecond,
                $tagPostsSecond->merge($generalPostsSecond)
            ],
        ];
    }

    /**
     * @param PhoneModel $model
     * @param Collection $posts
     * @dataProvider tagPostsDataProvider
     */
    public function testTagPostsSearch(PhoneModel $model, Collection $posts)
    {
        $this->postRepo->expects($this->once())->method('byTags')->willReturn($posts);
        $this->postRepo->expects($this->never())->method('lastPosts');
        $this->assertEquals($posts, $this->service->searchByModel($model));
    }

    public function tagPostsDataProvider()
    {
        return [
            [$this->getModelFixture(), $this->getPostsFixture(3)]
        ];
    }

    private function getModelFixture(): PhoneModel
    {
        $brand = new Brand();
        $brand->name = 'Apple';
        $model = new PhoneModel();
        $model->setName('iPhone 6s');
        $model->setRelation('brand', $brand);

        return $model;
    }

    private function getPostsFixture(int $count = 3): Collection
    {
        $posts = collect();
        foreach (range(1, $count) as $number) {
            $posts->push(new Post($number, '', '', '', '', []));
        }

        return $posts;
    }

    private function getTagsFixture(): array
    {
        return ['iphone 6s', 'apple'];
    }

    public function testPostRepoByTagThrowsException()
    {
        $this->postRepo->method('byTags')->will($this->throwException(new \Exception()));

        $this->assertEquals(collect(), $this->service->searchByModel($this->getModelFixture()));
    }

    public function testPostRepoLastPublishedThrowsException()
    {
        $posts = $this->getPostsFixture(1);
        $this->postRepo->expects($this->once())->method('byTags')->willReturn($posts);
        $this->postRepo->method('lastPosts')->will($this->throwException(new \Exception()));

        $this->assertEquals($posts, $this->service->searchByModel($this->getModelFixture()));
    }
}
