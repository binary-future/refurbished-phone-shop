<?php

namespace Tests\Unit\Application\Services\Search\Filters\Deals\QueryAssemblers;

use App\Application\Services\Search\Filters\Deals\QueryAssemblers\FiltersSearchQueryAssembler;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class FiltersSearchQueryAssemblerTest extends TestCase
{
    use BaseMock;

    private $serializer;

    /**
     * @var FiltersSearchQueryAssembler
     */
    private $assembler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->assembler = new FiltersSearchQueryAssembler($this->serializer);
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->assembler);
        parent::tearDown();
    }

    /**
     * @param array $params
     * @param array $transformedParams
     * @param FiltersSearchQuery $query
     * @dataProvider noSortDataProvider
     */
    public function testBuildingWithNoSortValue(
        array $params,
        array $transformedParams,
        FiltersSearchQuery $query
    ): void {
        $this->serializer->expects($this->once())->method('fromArray')
            ->with(FiltersSearchQuery::class, $transformedParams)
            ->willReturn($query);
        $result = $this->assembler->buildQuery($params);

        $this->assertEquals($query, $result);
    }

    public function noSortDataProvider(): array
    {
        $expectedParams = [
            'sort' => DealsCriteriaDictionary::CRITERIA_SORT_BY_MONTHLY_COST,
        ];
        $query = new FiltersSearchQuery();

        return [
            ['params' => [], $expectedParams, $query]
        ];
    }
}
