<?php

namespace Tests\Unit\Application\Services\Search\Filters\Defaults;

use App\Application\Services\Search\Filters\Contracts\PrimaryDealsAdjustedFilters;
use App\Application\Services\Search\Filters\Defaults\PrimaryFilterInitializer;
use App\Domain\Deals\Contract\Network;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PrimaryFilterInitializerTest extends TestCase
{
    use BaseMock;

    private const FILTER_NETWORKS = 'networks';

    /**
     * @var PrimaryFilterInitializer
     */
    private $initializer;
    private $logger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->logger = $this->getBaseMock(ChannelLogger::class);
        $this->initializer = new PrimaryFilterInitializer($this->logger);
    }

    protected function tearDown(): void
    {
        unset($this->logger, $this->initializer);
        parent::tearDown();
    }

    /**
     * @param Collection $networks
     * @param PrimaryDealsAdjustedFilters $adjustedFilters
     * @dataProvider posDataProvider
     */
    public function testPosInit(Collection $networks, PrimaryDealsAdjustedFilters $adjustedFilters): void
    {
        $this->logger->expects($this->never())->method('error');
        $this->initializer->init($networks, $adjustedFilters);
    }

    public function posDataProvider(): array
    {
        $network = new Network([
            Network::FIELD_SLUG => ''
        ]);
        $network->id = 1;
        $networks = new Collection([$network]);

        $adjustedFilters1 = $this->getBaseMock(PrimaryDealsAdjustedFilters::class);
        $adjustedFilters1->expects($this->once())->method('getNetworks')->willReturn([]);
        $adjustedFilters1->expects($this->once())->method('setNetworks')->with([1]);

        $adjustedFilters2 = $this->getBaseMock(PrimaryDealsAdjustedFilters::class);
        $adjustedFilters2->expects($this->once())->method('getNetworks')->willReturn([2]);
        $adjustedFilters2->expects($this->once())->method('setNetworks')->with([2]);

        return [
            [$networks, $adjustedFilters1],
            [$networks, $adjustedFilters2],
        ];
    }

    /**
     * @param Collection $networks
     * @param PrimaryDealsAdjustedFilters $adjustedFilters
     * @dataProvider ifNetworksEmptyDataProvider
     */
    public function testInitIfNetworksEmpty(Collection $networks, PrimaryDealsAdjustedFilters $adjustedFilters): void
    {
        $this->logger->expects($this->once())->method('error');

        $this->initializer->init($networks, $adjustedFilters);
    }

    public function ifNetworksEmptyDataProvider(): array
    {
        $networks = new Collection();

        $adjustedFilters = $this->getBaseMock(PrimaryDealsAdjustedFilters::class);
        $adjustedFilters->expects($this->once())->method('getNetworks')->willReturn([]);
        $adjustedFilters->expects($this->once())->method('setNetworks')->with([]);

        return [
            [$networks, $adjustedFilters],
        ];
    }
}
