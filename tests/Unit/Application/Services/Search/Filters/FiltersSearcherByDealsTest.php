<?php

namespace Tests\Unit\Application\Services\Search\Filters;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use App\Application\Services\Search\Filters\DealIndex;
use App\Application\Services\Search\Filters\FiltersSearcherByDeals;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class FiltersSearcherByDealsTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $serializer;

    /**
     * @var FiltersSearcherByDeals
     */
    private $filters;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->filters = new FiltersSearcherByDeals($this->serializer, $this->queryBus);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->serializer, $this->filters);
        parent::tearDown();
    }

    public function testIfSerializerThrowsException(): void
    {
        $brand = new Brand();
        $products = collect([new Phone()]);
        $adjustedFilters = $this->getBaseMock(AdjustedFilters::class);
        $response = $this->getFailedResponse($products);
        $this->serializer->method('fromArray')
            ->will($this->throwException(new \Exception()));
        $this->queryBus->expects($this->never())
            ->method('dispatch');

        $result = $this->filters->generate($brand, $products, $adjustedFilters);

        $this->assertResponsesEquals($response, $result);
    }

    public function testIfQueryBusThrowsException()
    {
        $brand = new Brand();
        $products = collect([new Phone()]);
        $adjustedFilters = $this->getBaseMock(AdjustedFilters::class);
        $response = $this->getFailedResponse($products);
        $this->serializer->method('fromArray')
            ->willReturn(new FiltersSearchQuery());
        $this->queryBus->method('dispatch')
            ->will($this->throwException(new \Exception()));

        $result = $this->filters->generate($brand, $products, $adjustedFilters);

        $this->assertResponsesEquals($response, $result);
    }

    public function testIfNoProducts(): void
    {
        $brand = new Brand();
        $products = collect();
        $adjustedFilters = $this->getBaseMock(AdjustedFilters::class);
        $response = $this->getFailedResponse($products);
        $this->serializer->expects($this->never())
            ->method('fromArray');
        $this->queryBus->expects($this->never())
            ->method('dispatch');

        $result = $this->filters->generate($brand, $products, $adjustedFilters);

        $this->assertResponsesEquals($response, $result);
    }

    /**
     * @dataProvider validDataProvider
     * @param DealIndex $dealIndex
     * @param Collection $products
     * @param Collection $networks
     * @param Collection $conditions
     * @param Collection $monthly
     * @param Collection $totalCost
     * @param Collection $activePhones
     */
    public function testGenerating(
        DealIndex $dealIndex,
        Collection $products,
        Collection $networks,
        Collection $conditions,
        Collection $monthly,
        Collection $totalCost,
        Collection $activePhones
    ): void {
        $brand = new Brand();
        $adjustedFilters = $this->getBaseMock(AdjustedFilters::class);
        $query = new FiltersSearchQuery();
        $query->setNetworks([]);

        $this->serializer->method('fromArray')->willReturn($query);
        $this->queryBus->expects($this->any())->method('dispatch')
            ->will($this->onConsecutiveCalls($activePhones, $networks, $conditions, $monthly, $totalCost));

        $result = $this->filters->generate($brand, $products, $adjustedFilters);

        $this->assertResponsesEquals($dealIndex, $result);
    }

    public function validDataProvider(): array
    {
        $monthlyCosts = collect([1.2]);
        $totalCosts = collect([5.5]);
        $networks = collect(['slug']);
        $enabledConditions = collect([Condition::PRISTINE()->getValue()]);
        $phoneFirst = new Phone();
        $phoneFirst->id = 1;
        $phoneSecond = new Phone();
        $phoneSecond->id = 2;
        $activePhones = collect([1]);
        $products = collect([$phoneFirst, $phoneSecond]);
        $dealIndex = new DealIndex(
            collect([$phoneFirst]),
            $networks,
            $enabledConditions,
            $monthlyCosts,
            $totalCosts,
        );

        return [
            [
                $dealIndex,
                $products,
                $networks,
                $enabledConditions,
                $monthlyCosts,
                $totalCosts,
                $activePhones,
            ]
        ];
    }

    private function assertResponsesEquals(DealIndex $expected, DealIndex $actual): void
    {
        $this->assertEquals(
            $expected->getNetworks(),
            $actual->getNetworks(),
            'Invalid Networks'
        );
        $this->assertEquals(
            $expected->getTotalCosts(),
            $actual->getTotalCosts(),
            'Invalid Total Cost'
        );
        $this->assertEquals(
            $expected->getMonthlyCosts(),
            $actual->getMonthlyCosts(),
            'Invalid Monthly Cost'
        );
        $this->assertEquals(
            $expected->getProducts(),
            $actual->getProducts(),
            'Invalid Products'
        );
    }

    private function getFailedResponse(Collection $products): DealIndex
    {
        return new DealIndex($products, collect(), collect(), collect(), collect());
    }
}
