<?php


namespace Tests\Unit\Application\Services\Search;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\DealsInfoSearcher;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Phone\Phone;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DealsInfoSearcherTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getSearcher()
    {
        return new DealsInfoSearcher($this->queryBus);
    }

    public function testSearchIfPhonesEmpty()
    {
        $searcher = $this->getSearcher();
        $result = $searcher->searchDealsInfo(collect());
        $this->assertNull($result->getBestDeal());
        $this->assertNull($result->getCheapestTotalCostDeal());
        $this->assertEmpty($result->getPhoneDealsInfo());
    }

    /**
     * @param Phone $phone
     * @param Deal $deal
     * @dataProvider validDataProvider
     */
    public function testSearchDealsInfo(Phone $phone, Deal $deal)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->willReturn(collect([$deal]));
        $searcher = $this->getSearcher();
        $result = $searcher->searchDealsInfo(collect([$phone]));
        $this->assertEquals($deal, $result->getBestDeal());
        $this->assertEquals($deal, $result->getCheapestTotalCostDeal());
        $this->assertNotEmpty($result->getPhoneDealsInfo());
    }

    public function validDataProvider()
    {
        $phone = new Phone();
        $phone->setColorId(1);
        $deal = new Deal();

        return [
            [$phone, $deal]
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
