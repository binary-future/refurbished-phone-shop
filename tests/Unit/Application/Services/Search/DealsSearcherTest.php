<?php

namespace Tests\Unit\Application\Services\Search;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\DealsSearcher;
use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Pagination\Paginator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class DealsSearcherTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->queryBus);
        parent::tearDown();
    }

    private function getSearcher(): DealsSearcher
    {
        return new DealsSearcher($this->serializer, $this->queryBus);
    }

    public function testIfPhoneEmpty(): void
    {
        $adjustedFilters = $this->getBaseMock(AdjustedFilters::class);
        $this->serializer->expects($this->never())
            ->method('fromArray');
        $this->queryBus->expects($this->never())
            ->method('dispatch');
        $searcher = $this->getSearcher();

        $result = $searcher->searchPaginated($adjustedFilters, collect(), 10, 1);

        $this->assertEquals(true, $result->isEmpty());
    }

    public function testSearch(): void
    {
        $adjustedFilters = $this->getBaseMock(AdjustedFilters::class);
        $deals = new Paginator(collect([new Deal()]), 10);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new FiltersSearchQuery());
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($deals);
        $searcher = $this->getSearcher();

        $result = $searcher->searchPaginated($adjustedFilters, collect([new Phone()]), 10, 1);

        $this->assertEquals(true, $result->isNotEmpty());
    }
}
