<?php


namespace Tests\Unit\Application\Services\Search\Translators;


use App\Application\Services\Search\Translators\ModelNameTranslator;
use Tests\TestCase;

final class ModelNameTranslatorTest extends TestCase
{
    /**
     * @var ModelNameTranslator
     */
    private $translator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->translator = new ModelNameTranslator();
    }

    /**
     * @param array $data
     * @param array $result
     * @dataProvider validDataProvider
     */
    public function testTranslation(array $data, array $result)
    {
        $response = $this->translator->translate($data);
        $this->assertEquals($result, $response);
    }

    public function validDataProvider()
    {
        return [
            [[], []],
            [['name' => 'ihone'], ['name' => 'iphone']],
            [['name' => 'aple'], ['name' => 'apple']],
            [['name' => 'smsung'], ['name' => 'samsung']],
            [['name' => 'glaxy'], ['name' => 'galaxy']],
        ];
    }
}
