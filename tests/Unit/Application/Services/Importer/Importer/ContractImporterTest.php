<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\DealsImportCommand;
use App\Application\Services\Importer\Importer\ContractImporter;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Network;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ContractImporterTest extends TestCase
{
    use BaseMock;

    /** @var QueryBus|MockObject */
    private $queryBus;

    /** @var CommandBus|MockObject */
    private $commandBus;

    /** @var Importer|MockObject */
    private $networkImporter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->networkImporter = $this->getBaseMock(Importer::class);
    }

    private function getImporter(): ContractImporter
    {
        return new ContractImporter($this->queryBus, $this->commandBus, $this->networkImporter);
    }

    /**
     * @param DealsImportCommand $importCommand
     * @param Network $network
     * @dataProvider importCommandDataProvider
     */
    public function testWhenContractExist(DealsImportCommand $importCommand, Network $network): void
    {
        $this->networkImporter->expects($this->once())
            ->method('import')
            ->with($importCommand)
            ->willReturn($network);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Contract()]));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertInstanceOf(Contract::class, $response);
    }

    public function importCommandDataProvider(): array
    {
        $network = new Network();
        $network->id = 1;
        $contract = new Contract();
        $importCommand = new DealsImportCommand();
        $importCommand->setNetwork($network);
        $importCommand->setContract($contract);

        return [
            [$importCommand, $network]
        ];
    }

    /**
     * @param DealsImportCommand $importCommand
     * @param Network $network
     * @dataProvider importCommandDataProvider
     */
    public function testWhenContractIsAbsent(DealsImportCommand $importCommand, Network $network): void
    {
        $this->networkImporter->expects($this->once())
            ->method('import')
            ->with($importCommand)
            ->willReturn($network);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Collection());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Contract());
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertInstanceOf(Contract::class, $response);
    }

    public function testWhenNetworkIsAbsent(): void
    {
        $importCommand = new DealsImportCommand();
        $this->networkImporter->expects($this->once())
            ->method('import')
            ->with($importCommand)
            ->willReturn(null);
        $this->queryBus->expects($this->never())
            ->method('dispatch');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertNull($response);
    }
}
