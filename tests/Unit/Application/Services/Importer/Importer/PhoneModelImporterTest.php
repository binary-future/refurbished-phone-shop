<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\PhonesImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\PhoneModelImporter;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Specs\PhoneSpecs;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PhoneModelImporterTest extends TestCase
{
    use BaseMock;

    /** @var MockObject|QueryBus */
    private $queryBus;

    /** @var MockObject|CommandBus */
    private $commandBus;

    /** @var MockObject|Importer */
    private $importer;

    /**
     * @var PhoneModelImporter
     */
    private $testImporter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->importer = $this->getBaseMock(Importer::class);
        $this->testImporter = new PhoneModelImporter($this->queryBus, $this->commandBus, $this->importer);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->commandBus,
            $this->importer,
            $this->testImporter
        );
        parent::tearDown();
    }

    private function getImporter()
    {
        return $this->testImporter;
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenModelExist(PhonesImportCommand $importCommand)
    {
        $phoneModel = $importCommand->getPhoneModel();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->willReturn(collect([$phoneModel]));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->importer->expects($this->never())
            ->method('import');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals($phoneModel, $response);
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenModelExistAndUpdated(PhonesImportCommand $importCommand)
    {
        $phoneModel = $importCommand->getPhoneModel();
        $specs = $this->getBaseMock(PhoneSpecs::class);
        $importCommand->setSpecs($specs);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->willReturn(collect([$phoneModel]));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($phoneModel);
        $this->importer->expects($this->never())
            ->method('import');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals($phoneModel, $response);
    }

    public function importCommandDataProvider()
    {
        $brand = new Brand();
        $model = new PhoneModel([
            PhoneModel::FIELD_SLUG => 'slug',
            PhoneModel::FIELD_ALIAS => 'alias'
        ]);
        $importCommand = new PhonesImportCommand();
        $importCommand->setBrand($brand);
        $importCommand->setPhoneModel($model);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenModelNotExist(PhonesImportCommand $importCommand)
    {
        $phoneModel = $importCommand->getPhoneModel();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $brand = new Brand();
        $brand->id = 1;
        $this->importer->expects($this->once())
            ->method('import')
            ->willReturn($brand);
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($phoneModel);
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals($phoneModel, $response);
    }
}
