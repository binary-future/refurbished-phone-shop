<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\DealsImportCommand;
use App\Application\Services\Importer\Importer\ProductImporter;
use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ProductImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getImporter()
    {
        return new ProductImporter($this->queryBus, $this->commandBus);
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenPhoneExist(DealsImportCommand $importCommand): void
    {
        $phoneModel = new PhoneModel();
        $phone = new Phone();
        $phone->setRelation(Phone::RELATION_MODEL, $phoneModel);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$phone]));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($phoneModel);
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals($phone, $response);
    }

    public function importCommandDataProvider(): array
    {
        $phone = new Phone();
        $phone->setCheckHash('checkhash');
        $importCommand = new DealsImportCommand();
        $importCommand->setProduct($phone);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenPhoneNotExist(DealsImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertNull($response);
    }
}
