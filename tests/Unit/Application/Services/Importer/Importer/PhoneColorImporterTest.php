<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\PhonesImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\PhoneColorImporter;
use App\Domain\Phone\Phone\Color;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PhoneColorImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getImporter(): PhoneColorImporter
    {
        return new PhoneColorImporter($this->queryBus, $this->commandBus);
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenColorExist(PhonesImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$importCommand->getColor()]));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals($importCommand->getColor(), $response);
    }

    public function importCommandDataProvider(): array
    {
        $color = new Color();
        $color->setAlias('Name');
        $importCommand = new PhonesImportCommand();
        $importCommand->setColor($color);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenColorNotExist(PhonesImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($importCommand->getColor());
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals($importCommand->getColor(), $response);
    }
}
