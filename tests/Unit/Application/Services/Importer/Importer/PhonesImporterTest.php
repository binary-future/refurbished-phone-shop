<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\PhonesImportCommand;
use App\Application\Services\Importer\Importer\Contracts\ImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\Exceptions\ImportException;
use App\Application\Services\Importer\Importer\PhonesImporter;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Adapters\Log\Contracts\Logger;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhonesImporterTest extends TestCase
{
    use BaseMock;

    private $importer;

    private $queryBus;

    private $commandBus;

    private $logger;

    private $colorImporter;

    private $phoneModelImporter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->logger = $this->getBaseMock(Logger::class);
        $this->colorImporter = $this->getBaseMock(Importer::class);
        $this->phoneModelImporter = $this->getBaseMock(Importer::class);
        $this->importer = new PhonesImporter(
            $this->queryBus,
            $this->commandBus,
            $this->logger,
            $this->colorImporter,
            $this->phoneModelImporter
        );
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->colorImporter,
            $this->commandBus,
            $this->logger,
            $this->importer,
            $this->phoneModelImporter
        );
        parent::tearDown();
    }

    private function getImporter(): PhonesImporter
    {
        return $this->importer;
    }

    public function testImporterThrowsExceptionIfWrongCommand(): void
    {
        $importer = $this->getImporter();

        $this->expectException(ImportException::class);

        $importer->import($this->getBaseMock(ImportCommand::class));
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     * @throws ImportException
     */
    public function testWhenPhoneExist(PhonesImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Phone()]));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals('not_changed', $response);
    }

    public function importCommandDataProvider(): array
    {
        $brand = new Brand();
        $brand->setName('Brand');
        $phoneModel = new PhoneModel();
        $phoneModel->setName('Name');
        $color = new Color();
        $phone = new Phone();
        $phone->setCheckHash('hash');
        $importCommand = new PhonesImportCommand();
        $importCommand->setColor($color);
        $importCommand->setBrand($brand);
        $importCommand->setPhoneModel($phoneModel);
        $importCommand->setPhone($phone);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @throws ImportException
     * @dataProvider importCommandDataProvider
     */
    public function testWhenPhoneNotExist(PhonesImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Phone());
        $color = new Color();
        $color->id = 1;
        $this->colorImporter->method('import')
            ->willReturn($color);
        $phoneModel = new PhoneModel();
        $phoneModel->id = 1;
        $this->phoneModelImporter->method('import')
            ->willReturn($phoneModel);
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals('created', $response);
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @throws ImportException
     * @dataProvider importCommandDataProvider
     */
    public function testWhenColorImporterThrowsException(PhonesImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->colorImporter->method('import')
            ->will($this->throwException(new \Exception()));
        $importer = $this->getImporter();

        $this->expectException(ImportException::class);

        $importer->import($importCommand);
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @throws ImportException
     * @dataProvider importCommandDataProvider
     */
    public function testWhenPhoneModelImporterThrowsException(PhonesImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $color = new Color();
        $color->id = 1;
        $this->colorImporter->method('import')
            ->willReturn($color);
        $this->phoneModelImporter->method('import')
            ->will($this->throwException(new \Exception()));
        $importer = $this->getImporter();

        $this->expectException(ImportException::class);

        $importer->import($importCommand);
    }
}
