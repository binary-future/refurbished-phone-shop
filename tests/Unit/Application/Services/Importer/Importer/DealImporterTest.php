<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\DealsImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\DealImporter;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Payment;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Link\Link;
use App\Utils\Adapters\Log\Contracts\Logger;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DealImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $productImporter;

    private $contractImporter;

    private $brandImporter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->brandImporter = $this->getBaseMock(Importer::class);
        $this->productImporter = $this->getBaseMock(Importer::class);
        $this->contractImporter = $this->getBaseMock(Importer::class);
    }

    private function getImporter(): DealImporter
    {
        return new DealImporter(
            $this->queryBus,
            $this->commandBus,
            $this->brandImporter,
            $this->productImporter,
            $this->contractImporter
        );
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenDealExist(DealsImportCommand $importCommand): void
    {
        $phone = new Phone();
        $phone->id = 1;
        $contract = new Contract();
        $contract->id = 1;
        $this->brandImporter->expects($this->once())
            ->method('import');
        $this->productImporter->expects($this->once())
            ->method('import')
            ->willReturn($phone);
        $this->contractImporter->expects($this->once())
            ->method('import')
            ->willReturn($contract);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Deal()]));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Deal());
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals('updated', $response);
    }

    public function importCommandDataProvider(): array
    {
        $phone = new Phone();
        $phone->setCheckHash('hash');
        $link = new Link();
        $deal = new Deal();
        $deal->setStoreId(1);
        $payment = new Payment(1, 1, 1);
        $deal->setPayment($payment);
        $importCommand = new DealsImportCommand();
        $importCommand->setProduct($phone);
        $importCommand->setLink($link);
        $importCommand->setDeal($deal);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testPhoneAbsent(DealsImportCommand $importCommand): void
    {
        $this->productImporter->expects($this->once())
            ->method('import')
            ->willReturn(null);
        $this->queryBus->expects($this->never())
            ->method('dispatch');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals('skipped', $response);
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenDealNotExist(DealsImportCommand $importCommand)
    {
        $phone = new Phone();
        $phone->id = 1;
        $this->productImporter->expects($this->once())
            ->method('import')
            ->willReturn($phone);
        $this->contractImporter->expects($this->once())
            ->method('import')
            ->willReturn(null);
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals('skipped_by_network', $response);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->commandBus,
            $this->brandImporter,
            $this->productImporter,
            $this->contractImporter
        );
        parent::tearDown();
    }
}
