<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\PhonesImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\PhoneBrandImporter;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Log\Contracts\Logger;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhoneBrandImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getImporter()
    {
        return new PhoneBrandImporter($this->queryBus, $this->commandBus);
    }

    /**
     * @param $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenBrandExist(PhonesImportCommand $importCommand)
    {
        $brand = $importCommand->getBrand();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$brand]));

        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals($brand, $response);
    }

    public function importCommandDataProvider()
    {
        $brand = new Brand();
        $brand->setAlias('Alias');
        $importCommand = new PhonesImportCommand();
        $importCommand->setBrand($brand);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param PhonesImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenBrandNotExist(PhonesImportCommand $importCommand)
    {
        $brand = $importCommand->getBrand();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($brand);
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals($brand, $response);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus);
        parent::tearDown();
    }
}
