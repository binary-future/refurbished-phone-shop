<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\DealsImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\NetworkImporter;
use App\Domain\Deals\Contract\Network;
use App\Utils\Adapters\Config\Contracts\Config;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class NetworkImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->commandBus,
            $this->config,
        );
        parent::tearDown();
    }


    private function getImporter(): NetworkImporter
    {
        return new NetworkImporter(
            $this->queryBus,
            $this->commandBus,
            $this->config
        );
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenNetworkExist(DealsImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Network()]));
        $this->config->expects($this->never())
            ->method('get');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertInstanceOf(Network::class, $response);
    }

    public function importCommandDataProvider(): array
    {
        $network = new Network();
        $network->setAlias('Alias');
        $network->setName('name');
        $importCommand = new DealsImportCommand();
        $importCommand->setNetwork($network);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenNetworkIsAbsentAndNewNetworksAllowed(DealsImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn(true);
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Network());
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertInstanceOf(Network::class, $response);
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenNetworkIsAbsentAndNewNetworksNOTAllowed(DealsImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->config->expects($this->once())
            ->method('get')
            ->willReturn(false);
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertNull($response);
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider noNetworkDataProvider
     */
    public function testWhenNoNetworkData(DealsImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->never())
            ->method('dispatch');
        $this->config->expects($this->never())
            ->method('get');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $this->expectException(\TypeError::class);

        $importer->import($importCommand);
    }

    public function noNetworkDataProvider(): array
    {
        return [
            [new DealsImportCommand()]
        ];
    }
}
