<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\Services\Importer\Importer\Commands\ImageImportCommand;
use App\Application\Services\Importer\Importer\NetworkImageImporter;
use App\Domain\Deals\Contract\Network;
use App\Domain\Shared\Image\Image;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class NetworkImageImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $uploader;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->uploader = $this->getBaseMock(Uploader::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getImporter()
    {
        return new NetworkImageImporter($this->queryBus, $this->uploader, $this->commandBus);
    }

    public function testIfNoNetworkFound()
    {
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $importer = $this->getImporter();
        $result = $importer->import($this->getCommand());
        $this->assertEquals('skipped', $result);
    }

    private function getCommand()
    {
        $network = new Network();
        $network->setSlug('slug');
        $image = new Image();
        $image->setPath(public_path('images/no-image.jpg'));
        $command = new ImageImportCommand();
        $command->setOwner($network);
        $command->setImage($image);

        return $command;
    }

    public function testIfUploaderFailed()
    {
        $network = new Network();
        $network->id = 1;
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->will($this->throwException(new \Exception()));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$network]));
        $importer = $this->getImporter();
        $result = $importer->import($this->getCommand());
        $this->assertEquals('skipped', $result);
    }

    public function testCannotUpdateOwner()
    {
        $network = new Network();
        $network->id = 1;
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->will($this->throwException(new \Exception()));
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$network]));
        $importer = $this->getImporter();
        $result = $importer->import($this->getCommand());
        $this->assertEquals('skipped', $result);
    }

    public function testImportSucceed()
    {
        $network = new Network();
        $network->id = 1;
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Network());
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$network]));
        $importer = $this->getImporter();
        $result = $importer->import($this->getCommand());
        $this->assertEquals('success', $result);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->uploader, $this->commandBus);
        parent::tearDown();
    }
}
