<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\DealsImportCommand;
use App\Application\Services\Importer\Importer\DealBrandImporter;
use App\Domain\Phone\Brand\Brand;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DealBrandImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->commandBus
        );
        parent::tearDown();
    }

    private function getImporter(): DealBrandImporter
    {
        return new DealBrandImporter($this->queryBus, $this->commandBus);
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenBrandExist(DealsImportCommand $importCommand): void
    {
        $brandCommand = $importCommand->getBrand();
        $brandResponse = new Brand();
        $brandResponse->id = 1;
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$brandCommand]));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($brandResponse);
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals($brandResponse->getKey(), $response->getKey());
    }

    public function importCommandDataProvider(): array
    {
        $brand = new Brand();
        $brand->setSlug('brand');
        $importCommand = new DealsImportCommand();
        $importCommand->setBrand($brand);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param DealsImportCommand $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenBrandNotExist(DealsImportCommand $importCommand): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();

        $response = $importer->import($importCommand);

        $this->assertEquals(null, $response);
    }
}
