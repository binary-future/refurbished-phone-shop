<?php

namespace Tests\Unit\Application\Services\Importer\Importer\Proxies\Transactions;

use App\Application\Services\Importer\Importer\Contracts\ImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\Proxies\Transactions\TransactionImporter;
use App\Utils\Adapters\Transaction\Contracts\Transaction;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

class TransactionImporterTest extends TestCase
{
    use BaseMock;

    private $transaction;
    private $importer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->transaction = $this->getBaseMock(Transaction::class);
        $this->importer = $this->getBaseMock(Importer::class);
    }

    private function getImporter()
    {
        return new TransactionImporter($this->transaction, $this->importer);
    }

    private function getCommand()
    {
        return $this->getBaseMock(ImportCommand::class);
    }

    public function testCommit()
    {
        $response = 'status';
        $command = $this->getCommand();
        $this->transaction->expects($this->once())
            ->method('beginTransaction');
        $this->importer->expects($this->once())
            ->method('import')
            ->with($command)
            ->willReturn($response);
        $this->transaction->expects($this->once())
            ->method('commit');
        $importer = $this->getImporter();

        $actualResponse = $importer->import($command);

        $this->assertEquals($response, $actualResponse);
    }

    public function testRollback()
    {
        $response = TransactionImporter::STATUS_TRANSACTION_FAILED;
        $command = $this->getCommand();
        $this->transaction->expects($this->once())
            ->method('beginTransaction');
        $this->importer->expects($this->once())
            ->method('import')
            ->with($command)
            ->willThrowException(new \Exception());
        $this->transaction->expects($this->once())
            ->method('rollback');
        $importer = $this->getImporter();

        $actualResponse = $importer->import($command);

        $this->assertEquals($response, $actualResponse);
    }

    protected function tearDown(): void
    {
        unset($this->importer, $this->transaction);
        parent::tearDown();
    }


}
