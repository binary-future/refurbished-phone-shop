<?php

namespace Tests\Unit\Application\Services\Importer\Importer\Proxies\Logging;

use App\Application\Services\Importer\Importer\Contracts\ImportCommand;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\Proxies\Logging\MemoryTimeLoggingImporter;
use App\Utils\Adapters\Log\Contracts\Logger;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

class MemoryTimeLoggingImporterTest extends TestCase
{
    use BaseMock;

    private $importer;
    private $logger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->logger = $this->getBaseMock(Logger::class);
        $this->importer = $this->getBaseMock(Importer::class);
    }

    private function getImporter(): MemoryTimeLoggingImporter
    {
        return new MemoryTimeLoggingImporter($this->logger, $this->importer);
    }

    private function getCommand()
    {
        return $this->getBaseMock(ImportCommand::class);
    }

    public function testPositiveImport()
    {
        $response = 'status';
        $command = $this->getCommand();
        $this->importer->expects($this->once())
            ->method('import')
            ->with($command)
            ->willReturn($response);
        $this->logger->expects($this->atLeastOnce())
            ->method('line');
        $this->logger->expects($this->once())
            ->method('info');
        $importer = $this->getImporter();

        $actualResponse = $importer->import($command);

        $this->assertEquals($response, $actualResponse);
    }

    public function testImportThrowsException()
    {
        $response = MemoryTimeLoggingImporter::STATUS_LOGGING_FAILED;
        $command = $this->getCommand();
        $this->importer->expects($this->once())
            ->method('import')
            ->with($command)
            ->willThrowException(new \Exception());
        $this->logger->expects($this->once())
            ->method('error');
        $importer = $this->getImporter();

        $actualResponse = $importer->import($command);

        $this->assertEquals($response, $actualResponse);
    }

    protected function tearDown(): void
    {
        unset($this->importer, $this->logger);
        parent::tearDown();
    }


}
