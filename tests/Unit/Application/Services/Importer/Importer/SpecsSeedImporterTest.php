<?php


namespace Tests\Unit\Application\Services\Importer\Importer;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\SpecsSeedCommand;
use App\Application\Services\Importer\Importer\SpecsSeedImporter;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Specs\PhoneSpecs;
use App\Utils\Adapters\Log\Contracts\Logger;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class SpecsSeedImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $logger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->logger = $this->getBaseMock(Logger::class);
    }

    private function getImporter()
    {
        return new SpecsSeedImporter($this->queryBus, $this->commandBus, $this->logger);
    }

    /**
     * @param $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenModelPresent($importCommand)
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([new PhoneModel()])));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new PhoneModel());
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals('imported', $response);
    }

    public function importCommandDataProvider()
    {
        $brand = new Brand();
        $brand->setAlias('Alias');
        $brand->setName('name');
        $phoneModel = new PhoneModel();
        $phoneModel->setName('name');
        $phoneModel->setAlias('alias');
        $specs = new PhoneSpecs();
        $importCommand = new SpecsSeedCommand();
        $importCommand->setBrand($brand);
        $importCommand->setPhoneModel($phoneModel);
        $importCommand->setSpecs($specs);

        return [
            [$importCommand]
        ];
    }

    /**
     * @param $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenBrandNotExist($importCommand)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals('skipped', $response);
    }

    /**
     * @param $importCommand
     * @dataProvider importCommandDataProvider
     */
    public function testWhenModelNotExist($importCommand)
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect()));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $importer = $this->getImporter();
        $response = $importer->import($importCommand);
        $this->assertEquals('skipped', $response);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus, $this->logger);
        parent::tearDown();
    }
}
