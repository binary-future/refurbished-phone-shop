<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\Services\Importer\Importer\Commands\StoreImportCommand;
use App\Application\Services\Importer\Importer\StoreImporter;
use App\Domain\Store\Store;
use App\Utils\Adapters\Log\Contracts\Logger;
use Illuminate\Support\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvokedCount;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreImporterTest extends TestCase
{
    use BaseMock;

    private const STATUS_CREATED = 'created';
    private const STATUS_UPDATED = 'updated';
    private const STATUS_NOT_CHANGED = 'not_changed';

    /**
     * @var QueryBus|MockObject
     */
    private $queryBus;
    /**
     * @var CommandBus|MockObject
     */
    private $commandBus;
    /**
     * @var Logger|MockObject
     */
    private $logger;
    /**
     * @var DatafeedsInfo|MockObject
     */
    private $datafeedsInfo;

    protected function setUp(): void
    {
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->logger = $this->getBaseMock(Logger::class);
        $this->datafeedsInfo = $this->getBaseMock(DatafeedsInfo::class);
        parent::setUp();
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->commandBus,
            $this->logger,
            $this->datafeedsInfo,
        );
        parent::tearDown();
    }

    private function getImporter(): StoreImporter
    {
        return new StoreImporter(
            $this->queryBus,
            $this->commandBus,
            $this->logger,
            $this->datafeedsInfo,
        );
    }

    private function getDefaultStore(): Store
    {
        $store = new Store();
        $store->setName('Store');
        $store->setAlias('alias');

        return $store;
    }

    /**
     * @param StoreImportCommand $command
     * @dataProvider createStoreWithoutDatafeedDataProvider
     */
    public function testImportCreateStoreWithoutDatafeed(
        StoreImportCommand $command
    ): void {
        $this->logger->expects($this->exactly(2))
            ->method('info')
            ->withConsecutive([$command->getStore()->getName()], [self::STATUS_CREATED]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Collection());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Store());
        $this->datafeedsInfo->expects($this->never())
            ->method('save');
        $this->datafeedsInfo->expects($this->never())
            ->method('findBy');
        $importer = $this->getImporter();

        $status = $importer->import($command);

        $this->assertEquals(self::STATUS_CREATED, $status);
    }

    public function createStoreWithoutDatafeedDataProvider(): array
    {
        $commandStore = $this->getDefaultStore();

        $command = new StoreImportCommand();
        $command->setStore($commandStore);

        return [
            [ $command ]
        ];
    }

    /**
     * @param StoreImportCommand $command
     * @param Store $createdStore
     * @dataProvider createStoreWithDatafeedDataProvider
     */
    public function testImportCreateStoreWithDatafeed(
        StoreImportCommand $command,
        Store $createdStore
    ): void {
        $this->logger->expects($this->exactly(2))
            ->method('info')
            ->withConsecutive([$command->getStore()->getName()], [self::STATUS_CREATED]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Collection());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($createdStore);
        $this->datafeedsInfo->expects($this->once())
            ->method('save');
        $this->datafeedsInfo->expects($this->never())
            ->method('findBy');
        $importer = $this->getImporter();

        $status = $importer->import($command);

        $this->assertEquals(self::STATUS_CREATED, $status);
    }

    public function createStoreWithDatafeedDataProvider(): array
    {
        $commandStore = $this->getDefaultStore();

        $datafeed = new DatafeedInfo();

        $command = new StoreImportCommand();
        $command->setStore($commandStore);
        $command->setDatafeed($datafeed);

        $createdStore = $commandStore;
        $createdStore->id = 1;

        return [
            [ $command, $createdStore ]
        ];
    }

    /**
     * @param StoreImportCommand $command
     * @param Store $queryStore
     * @dataProvider updateStoreWithDatafeedDataProvider
     */
    public function testImportUpdateStoreWithDatafeed(
        StoreImportCommand $command,
        Store $queryStore
    ): void {
        $this->logger->expects($this->exactly(2))
            ->method('info')
            ->withConsecutive([$command->getStore()->getName()], [self::STATUS_UPDATED]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Collection([$queryStore]));
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->datafeedsInfo->expects($this->once())
            ->method('findBy')
            ->willReturn(new Collection());
        $this->datafeedsInfo->expects($this->once())
            ->method('save');
        $importer = $this->getImporter();

        $status = $importer->import($command);

        $this->assertEquals(self::STATUS_UPDATED, $status);
    }

    public function updateStoreWithDatafeedDataProvider(): array
    {
        $commandStore = $this->getDefaultStore();

        $datafeed = new DatafeedInfo();

        $command = new StoreImportCommand();
        $command->setStore($commandStore);
        $command->setDatafeed($datafeed);

        $queryStore = $commandStore;
        $queryStore->id = 1;

        return [
            [ $command, $queryStore ]
        ];
    }

    /**
     * @param StoreImportCommand $command
     * @param Store $queryStore
     * @param DatafeedInfo|null $foundDatafeed
     * @dataProvider notChangedStoreDataProvider
     */
    public function testImportNotChangedStore(
        StoreImportCommand $command,
        Store $queryStore,
        ?DatafeedInfo $foundDatafeed
    ): void {
        $this->logger->expects($this->exactly(2))
            ->method('info')
            ->withConsecutive([$command->getStore()->getName()], [self::STATUS_NOT_CHANGED]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Collection([$queryStore]));
        if ($foundDatafeed) {
            $this->datafeedsInfo
                ->expects($this->once())
                ->method('findBy')
                ->willReturn(new Collection([$foundDatafeed]));
        } else {
            $this->datafeedsInfo
                ->expects($this->never())
                ->method('findBy');
        }
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->datafeedsInfo->expects($this->never())
            ->method('save');
        $importer = $this->getImporter();

        $status = $importer->import($command);

        $this->assertEquals(self::STATUS_NOT_CHANGED, $status);
    }

    public function notChangedStoreDataProvider(): array
    {
        $datafeed = new DatafeedInfo();
        $foundDatafeed = $datafeed;

        $commandStore = $this->getDefaultStore();

        $queryStore = $commandStore;
        $queryStore->id = 1;

        $commandWithoutDatafeed = new StoreImportCommand();
        $commandWithoutDatafeed->setStore($commandStore);

        $commandWithDatafeed = clone $commandWithoutDatafeed;
        $commandWithDatafeed->setDatafeed($datafeed);

        return [
            [ $commandWithoutDatafeed, $queryStore, null ],
            [ $commandWithDatafeed, $queryStore, $foundDatafeed, $this->once() ],
        ];
    }
}
