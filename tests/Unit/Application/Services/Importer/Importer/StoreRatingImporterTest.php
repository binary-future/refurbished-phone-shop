<?php

namespace Tests\Unit\Application\Services\Importer\Importer;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Importer\Commands\StoreRatingsImportCommand;
use App\Application\Services\Importer\Importer\StoreRatingImporter;
use App\Application\Services\Store\Rating\Contracts\StoreRatingManager;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class StoreRatingImporterTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $importer;
    private $storeRatingManager;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->storeRatingManager = $this->getBaseMock(StoreRatingManager::class);
        $this->importer = new StoreRatingImporter(
            $this->queryBus,
            $this->commandBus,
            $this->storeRatingManager
        );
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus, $this->storeRatingManager, $this->importer);
        parent::tearDown();
    }

    /**
     * @param StoreRatingsImportCommand $command
     * @dataProvider importCommandDataProvider
     */
    public function testImportUpsertRatingSuccess(StoreRatingsImportCommand $command): void
    {
        $store = new Store();
        $store->setRelation(
            Store::RELATION_RATING,
            $this->getBaseMock(Rating::class)
        );

        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->storeRatingManager->expects($this->once())->method('prepareRating')
            ->with($store, $command->getRating(), $command->getLink());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($store);

        $response = $this->importer->import($command);

        $this->assertEquals('success', $response);
    }

    /**
     * @param StoreRatingsImportCommand $command
     * @dataProvider importCommandDataProvider
     */
    public function testImportSkipped(StoreRatingsImportCommand $command)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $this->commandBus->expects($this->never())
            ->method('dispatch');

        $response = $this->importer->import($command);

        $this->assertEquals('skipped', $response);
    }

    public function importCommandDataProvider(): array
    {
        $command = new StoreRatingsImportCommand();
        $rating = new Rating([Rating::FIELD_RATING => 50]);
        $link = new Link([Link::FIELD_LINK => 'link']);
        $store = new Store([Store::FIELD_ALIAS => 'alias']);
        $command->setRating($rating);
        $command->setStore($store);
        $command->setLink($link);

        return [
            [$command]
        ];
    }
}
