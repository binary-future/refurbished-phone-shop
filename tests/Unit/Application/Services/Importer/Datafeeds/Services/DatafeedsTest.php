<?php


namespace Tests\Unit\Application\Services\Importer\Datafeeds\Services;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\Services\Importer\Datafeeds\Services\Datafeeds;
use App\Domain\Store\Store;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DatafeedsTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $datafeeds;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->datafeeds = $this->getBaseMock(DatafeedsInfo::class);
    }

    private function getService()
    {
        return new Datafeeds($this->serializer, $this->datafeeds);
    }

    public function testStoring()
    {
        $datafeedInfo = new DatafeedInfo();
        $datafeedInfo->setExternalId(1);
        $datafeedInfo->setApiId(1);
        $store = new Store();
        $store->id = 1;
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($datafeedInfo);
        $this->datafeeds->expects($this->any())
            ->method('findBy')
            ->willReturn(collect());
        $this->datafeeds->expects($this->once())
            ->method('save')
            ->willReturn($datafeedInfo);

        $service = $this->getService();

        $this->assertInstanceOf(DatafeedInfo::class, $service->store($store, []));
    }

    public function testStoringIfInvalidParams()
    {
        $store = new Store();
        $store->id = 1;
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new DatafeedInfo());
        $this->datafeeds->expects($this->any())
            ->method('findBy')
            ->willReturn(collect());
        $this->datafeeds->expects($this->never())
            ->method('save');
        $service = $this->getService();
        $this->assertFalse($service->store($store, []));
    }

    public function testRemoving()
    {
        $this->datafeeds->expects($this->once())
            ->method('delete')
            ->willReturn(true);
        $datafeeds = $this->getService();
        $this->assertTrue($datafeeds->remove(new DatafeedInfo()));
    }
}
