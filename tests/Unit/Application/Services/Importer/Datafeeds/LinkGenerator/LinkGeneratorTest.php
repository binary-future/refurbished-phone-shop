<?php

namespace Tests\Unit\Application\Services\Importer\Datafeeds\LinkGenerator;


use App\Application\Services\Importer\Datafeeds\DatafeedApi;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\LinkGenerator\LinkGenerator;
use App\Application\Services\Importer\Datafeeds\Selectors\ImportScenarioSelector;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class LinkGeneratorTest extends TestCase
{
    use BaseMock;

    private $scenarioSelector;
    private $linkGeneratorFactory;

    protected function setUp(): void
    {
        parent::setUp();
        $this->scenarioSelector = $this->getBaseMock(ImportScenarioSelector::class);
        $this->linkGeneratorFactory = $this->getBaseMock(LinkGeneratorFactory::class);
    }

    protected function tearDown(): void
    {
        unset($this->scenarioSelector, $this->linkGeneratorFactory);
        parent::tearDown();
    }

    private function getGenerator()
    {
        return new LinkGenerator($this->scenarioSelector, $this->linkGeneratorFactory);
    }

    private function getApi()
    {
        return new DatafeedApi([DatafeedApi::FIELD_NAME => 'name']);
    }

    private function getDatafeed()
    {
        return $this->getBaseMock(DatafeedInfo::class);
    }

    public function testGetLink()
    {
        $api = $this->getApi();
        $datafeed = $this->getDatafeed();
        $datafeed->expects($this->once())
            ->method('getApi')
            ->willReturn($api);
        $scenario = $this->getBaseMock(Scenario::class);
        $requestGenerator = $this->getBaseMock(RequestLinkGenerator::class);
        $requestGenerator->expects($this->once())
            ->method('generateLink')
            ->willReturn('link');
        $this->scenarioSelector->expects($this->once())
            ->method('select')
            ->willReturn($scenario);
        $this->linkGeneratorFactory->expects($this->once())
            ->method('getRequestGenerator')
            ->willReturn($requestGenerator);
        $generator = $this->getGenerator();

        $link = $generator->getLink($datafeed, ImportScenarioSelector::DEALS);

        $this->assertEquals('link', $link);
    }

    /**
     * @throws ImportHandlerBuildException
     * @throws \App\Utils\Importer\Exceptions\SourceException
     */
    public function testGetLinkWhenScenarioSelectorThrowsException()
    {
        $datafeed = $this->getDatafeed();
        $this->scenarioSelector->expects($this->once())
            ->method('select')
            ->willReturn(null);
        $this->linkGeneratorFactory->expects($this->never())
            ->method('getRequestGenerator');
        $generator = $this->getGenerator();

        $this->expectException(ImportHandlerBuildException::class);

        $generator->getLink($datafeed, ImportScenarioSelector::DEALS);
    }

    /**
     * @throws ImportHandlerBuildException
     * @throws \App\Utils\Importer\Exceptions\SourceException
     */
    public function testGetLinkWhenLinkGeneratorFactoryThrowsException()
    {
        $api = $this->getApi();
        $datafeed = $this->getDatafeed();
        $datafeed->expects($this->once())
            ->method('getApi')
            ->willReturn($api);
        $scenario = $this->getBaseMock(Scenario::class);
        $this->scenarioSelector->expects($this->once())
            ->method('select')
            ->willReturn($scenario);
        $this->linkGeneratorFactory->expects($this->once())
            ->method('getRequestGenerator')
            ->willThrowException(new SourceException());
        $generator = $this->getGenerator();

        $this->expectException(SourceException::class);

        $generator->getLink($datafeed, ImportScenarioSelector::DEALS);
    }

}
