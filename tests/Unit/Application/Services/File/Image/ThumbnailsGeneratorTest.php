<?php


namespace Tests\Unit\Application\Services\File\Image;


use App\Application\Services\File\Image\ThumbnailsGenerator;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Events\ImageSaved;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Image\Services\Thumb\ThumbGenerator;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ThumbnailsGeneratorTest extends TestCase
{
    use BaseMock;

    private $generator;

    private $testGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->generator = $this->getBaseMock(ThumbGenerator::class);
        $this->testGenerator = new ThumbnailsGenerator($this->generator);
    }

    /**
     * @param ImageSaved $event
     * @dataProvider shouldNotGenerateDataProvider
     */
    public function testShouldNotGenerate(ImageSaved $event)
    {
        $this->generator->expects($this->never())->method('generateThumb');
        $this->assertNull($this->testGenerator->handle($event));
    }

    public function shouldNotGenerateDataProvider()
    {
        $thumbImage = new Image();
        $thumbImage->parent_id = 1;
        $thumbImage->setImagableType(Phone::OWNER_TYPE);

        $thumbImageTwo = new Image();
        $thumbImageTwo->setImagableType("type");

        return [
            [new ImageSaved($thumbImage)],
            [new ImageSaved($thumbImageTwo)],
        ];
    }

    /**
     * @param ImageSaved $event
     * @dataProvider shouldGenerateDataProvider
     */
    public function testShouldGenerate(ImageSaved $event)
    {
        $this->generator
            ->expects($this->once())
            ->method('generateThumb')
            ->with($event->getImage())
            ->willReturn(collect([]));

        $this->assertNull($this->testGenerator->handle($event));
    }

    /**
     * @param ImageSaved $event
     * @dataProvider shouldGenerateDataProvider
     */
    public function testGeneratorThrowsException(ImageSaved $event)
    {
        $this->generator
            ->expects($this->once())
            ->method('generateThumb')
            ->with($event->getImage())
            ->will($this->throwException(new \Exception()));

        $this->assertNull($this->testGenerator->handle($event));
    }

    public function shouldGenerateDataProvider()
    {
        $thumbImage = new Image();
        $thumbImage->setImagableType(Phone::OWNER_TYPE);

        return [
            [new ImageSaved($thumbImage)],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->generator, $this->testGenerator);
        parent::tearDown();
    }
}
