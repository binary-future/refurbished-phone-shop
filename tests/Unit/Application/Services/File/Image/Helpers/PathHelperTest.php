<?php

namespace Tests\Unit\Application\Services\File\Image\Helpers;

use App\Application\Services\File\Image\Exceptions\ImagePathException;
use App\Application\Services\File\Image\Helpers\PathHelper;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Config\Contracts\Config;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PathHelperTest extends TestCase
{
    use BaseMock;

    private $config;

    protected function setUp(): void
    {
        parent::setUp();
        $this->config = $this->getBaseMock(Config::class);
    }

    protected function tearDown(): void
    {
        unset($this->config);
        parent::tearDown();
    }

    private function getPathHelper(): PathHelper
    {
        return new PathHelper($this->config);
    }

    /**
     * @param array $config
     * @param Phone $phone
     * @param Image $image
     * @param string $result
     * @throws ImagePathException
     * @dataProvider validDataProvider
     */
    public function testPositiveGetImagePath(
        array $config,
        Phone $phone,
        Image $image,
        string $result
    ) {
        $this->config->expects($this->once())->method('get')
            ->willReturn($config);
        $helper = $this->getPathHelper();

        $actualResult = $helper->getImagePath($phone, $image);

        $this->assertEquals($result, $actualResult);
    }

    public function validDataProvider(): array
    {
        $directory = 'dir';
        $ownerName = 'ownername';
        $extension = 'png';
        $phone1 = $this->getBaseMock(Phone::class);
        $phone1->expects($this->once())->method('getOwnerName')->willReturn($ownerName);
        $phone2 = $this->getBaseMock(Phone::class);
        $phone2->expects($this->once())->method('getOwnerName')->willReturn($ownerName);
        $config = [ get_class($phone1) => $directory ];
        $image1 = new Image([
            Image::FIELD_IS_MAIN => true,
            Image::FIELD_PATH => "path/to/image.$extension",
        ]);
        $image1->id = 1;
        $result1 = "$directory/$ownerName.$extension";

        $image2 = new Image([
            Image::FIELD_IS_MAIN => false,
            Image::FIELD_PATH => "path/to/image.$extension",
        ]);
        $image2->id = 1;
        $result2 = "$directory/$ownerName-1.$extension";

        return [
            [$config, $phone1, $image1, $result1],
            [$config, $phone2, $image2, $result2],
        ];
    }

    /**
     * @throws ImagePathException
     */
    public function testNegativeGetImagePath()
    {
        $this->config->expects($this->once())->method('get')
            ->willReturn([]);
        $phone = $this->getBaseMock(Phone::class);
        $image = $this->getBaseMock(Image::class);
        $helper = $this->getPathHelper();

        $this->expectException(ImagePathException::class);

        $actualResult = $helper->getImagePath($phone, $image);
    }

    public function testTransformStoragePathToRelativeUrl()
    {
        $path = 'public/path/to/image.png';
        $expectedPath = 'storage/path/to/image.png';
        $helper = $this->getPathHelper();

        $actualPath = $helper->transformStoragePathToRelativeUrl($path);

        $this->assertEquals($expectedPath, $actualPath);
    }
}
