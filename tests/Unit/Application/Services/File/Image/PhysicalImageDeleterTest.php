<?php


namespace Tests\Unit\Application\Services\File\Image;


use App\Application\Services\File\Image\PhysicalImageDeleter;
use App\Domain\Shared\Image\Events\ImageDeleted;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Storage\Contracts\Storage;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhysicalImageDeleterTest extends TestCase
{
    use BaseMock;

    private $storage;

    private $deleter;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storage = $this->getBaseMock(Storage::class);
        $this->deleter = new PhysicalImageDeleter($this->storage);
    }

    /**
     * @param ImageDeleted $event
     * @param string $changedPath
     * @dataProvider localImageDataProvider
     */
    public function testLocalImage(ImageDeleted $event, string $changedPath)
    {
        $this->storage->expects($this->once())->method('delete')->with($changedPath)->willReturn(true);
        $this->assertNull($this->deleter->handle($event));
    }

    public function localImageDataProvider()
    {
        $image = new Image();
        $image->setIsLocal(true);
        $image->setPath('storage/app.jpeg');
        $changedPath = 'public/app.jpeg';

        return [
            [new ImageDeleted($image), $changedPath]
        ];
    }

    /**
     * @param ImageDeleted $event
     * @dataProvider notLocalImageDataProvider
     */
    public function testNotLocalImage(ImageDeleted $event)
    {
        $this->storage->expects($this->never())->method('delete');
        $this->assertNull($this->deleter->handle($event));
    }

    public function notLocalImageDataProvider()
    {
        $image = new Image();
        $image->setIsLocal(false);

        return [
            [new ImageDeleted($image)]
        ];
    }

    protected function tearDown(): void
    {
        unset($this->storage, $this->deleter);
        parent::tearDown();
    }
}
