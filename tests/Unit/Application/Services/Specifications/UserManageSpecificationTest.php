<?php

namespace Tests\Unit\Application\Services\Specifications;

use App\Application\Services\Specifications\UserManageSpecification;
use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Specification\Factory;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserManageSpecificationTest extends TestCase
{
    use BaseMock;

    private $specification;

    private $factory;

    private $authService;

    /**
     * @var UserManageSpecification
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();
        $this->specification = $this->getBaseMock(Specification::class);
        $this->factory = $this->getBaseMock(Factory::class);
        $this->authService = $this->getBaseMock(Auth::class);
        $this->service = new UserManageSpecification($this->authService, $this->factory);
    }

    public function testIfSatisfy()
    {
        $this->specification->method('isSatisfy')->willReturn(true);
        $this->factory->method('buildSpecification')->willReturn($this->specification);
        $this->authService->method('getAuthUser')->willReturn(new User());
        $this->assertTrue($this->service->isSatisfy(new User()));
    }

    public function testIfNotAuthUser()
    {
        $this->factory->expects($this->never())->method('buildSpecification');
        $this->authService->method('getAuthUser')->willReturn(null);
        $this->assertFalse($this->service->isSatisfy(new User()));
    }

    public function testIfFactoryFailed()
    {
        $this->factory->expects($this->once())->method('buildSpecification')->will($this->throwException(new \Exception()));
        $this->authService->method('getAuthUser')->willReturn(new User());
        $this->assertFalse($this->service->isSatisfy(new User()));
    }

    public function testIfNotUserGiven()
    {
        $this->assertFalse($this->service->isSatisfy([]));
    }

    protected function tearDown(): void
    {
        unset($this->specification, $this->factory, $this->authService, $this->service);
        parent::tearDown();
    }
}
