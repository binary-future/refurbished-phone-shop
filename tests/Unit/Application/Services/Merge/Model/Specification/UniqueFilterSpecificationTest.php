<?php


namespace Tests\Unit\Application\Services\Merge\Model\Specification;


use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\Services\Merge\Model\Specifications\UniqueFilterSpecification;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UniqueFilterSpecificationTest extends TestCase
{
    use BaseMock;

    private $filters;

    /**
     * @var UniqueFilterSpecification
     */
    private $specifications;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->specifications = new UniqueFilterSpecification($this->filters);
    }

    /**
     * @param array $params
     * @param Collection $filters
     * @param bool $response
     * @dataProvider validStructureDataProvider
     */
    public function testSpecification(array $params, Collection $filters, bool $response)
    {
        $this->filters->expects($this->once())->method('findBy')->willReturn($filters);

        $this->assertEquals($response, $this->specifications->isSatisfy($params));
    }

    public function validStructureDataProvider()
    {
        $params = ['brand_id' => 1, 'parent_id' => null, 'name' => 'filter'];
        $paramsTwo = $params;
        $paramsTwo['filter_id'] = 1;
        $validFilterOne = new Filter();
        $validFilterOne->id = 1;
        $paramsThree = $params;
        $paramsThree['filter_id'] = 2;

        return [
            [$params, collect(), true],
            [$paramsTwo, collect([$validFilterOne]), true],
            [$paramsThree, collect([$validFilterOne]), false],
        ];
    }

    public function testInvalidData()
    {
        $this->filters->expects($this->never())->method('findBy');
        $this->assertFalse($this->specifications->isSatisfy([]));
    }
}
