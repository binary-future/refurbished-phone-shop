<?php


namespace Tests\Unit\Application\UseCases\Main;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Main\SitemapsCase;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class SitemapsCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getCase()
    {
        return new SitemapsCase($this->queryBus);
    }

    public function testExecution()
    {
        $stores = collect([new Store()]);
        $brands = collect([new Brand()]);
        $models = collect([new PhoneModel()]);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls($stores, $brands, $brands, $models));
        $case = $this->getCase();
        $response = $case->execute();
        $this->assertEquals($stores, $response->getStores());
        $this->assertInstanceOf(Store::class, $response->getStores()->first());
        $this->assertEquals($models, $response->getModels());
        $this->assertInstanceOf(Brand::class, $response->getBrands()->first());
        $this->assertEquals($brands, $response->getBrands());
        $this->assertInstanceOf(PhoneModel::class, $response->getModels()->first());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
