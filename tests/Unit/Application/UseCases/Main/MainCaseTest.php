<?php

namespace Tests\Unit\Application\UseCases\Main;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Main\MainCase;
use App\Application\UseCases\Post\Contracts\TagPostsCase;
use App\Application\UseCases\Post\Responses\TagPosts;
use App\Domain\Store\Store;
use App\Utils\Adapters\Blog\Post;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class MainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $topModelsRepository;
    private $latestModelsRepository;
    private $tagPostsCase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->topModelsRepository = $this->getBaseMock(TopModels::class);
        $this->latestModelsRepository = $this->getBaseMock(LatestModels::class);
        $this->tagPostsCase = $this->getBaseMock(TagPostsCase::class);
    }

    private function getCase(): MainCase
    {
        return new MainCase(
            $this->queryBus,
            $this->topModelsRepository,
            $this->latestModelsRepository,
            $this->tagPostsCase
        );
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->topModelsRepository,
            $this->latestModelsRepository,
            $this->tagPostsCase
        );
        parent::tearDown();
    }

    public function testExecution()
    {
        $stores = collect([new Store()]);
        $topModels = collect([new TopModel()]);
        $latestModels = collect([new LatestModel()]);
        $tagPostsResponse = new TagPosts(collect([
            new Post(1, 'title', 'content', 'shortTitle', null, ['tag'])
        ]));
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($stores);
        $this->topModelsRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($topModels);
        $this->latestModelsRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($latestModels);
        $this->tagPostsCase->expects($this->once())
            ->method('execute')
            ->willReturn($tagPostsResponse);
        $case = $this->getCase();

        $response = $case->execute();

        $this->assertEquals($stores, $response->getStores());
        $this->assertEquals($topModels, $response->getTopModels());
        $this->assertEquals($latestModels, $response->getLatestModels());
        $this->assertEquals($tagPostsResponse->getPosts(), $response->getTaggedPosts());
    }
}
