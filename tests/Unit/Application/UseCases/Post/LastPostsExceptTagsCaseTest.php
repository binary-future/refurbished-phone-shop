<?php

namespace Tests\Unit\Application\UseCases\Post;

use App\Application\UseCases\Post\LastPostsExceptTagsCase;
use App\Utils\Adapters\Blog\Contracts\PostService;
use App\Utils\Adapters\Blog\Post;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class LastPostsExceptTagsCaseTest extends TestCase
{
    use BaseMock;

    private $postService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->postService = $this->getBaseMock(PostService::class);
    }

    protected function tearDown(): void
    {
        unset($this->postService);
        parent::tearDown();
    }


    private function getCase(): LastPostsExceptTagsCase
    {
        return new LastPostsExceptTagsCase($this->postService);
    }

    public function testExecute()
    {
        $posts = collect([$this->getPost(), $this->getPost(), $this->getPost()]);
        $this->postService->expects($this->once())
            ->method('lastPostsExceptTags')
            ->willReturn($posts);
        $case = $this->getCase();

        $result = $case->execute(['exceptedTag'], 3);

        $this->assertEquals($posts, $result->getPosts());
    }

    private function getPost(): Post
    {
        return new Post(1, 'title', 'content', 'shortTitle', null, ['tag']);
    }
}
