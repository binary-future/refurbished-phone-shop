<?php

namespace Tests\Unit\Application\UseCases\Post;

use App\Application\UseCases\Post\LastPostsExceptTagsCase;
use App\Application\UseCases\Post\TagPostsCase;
use App\Utils\Adapters\Blog\Contracts\PostService;
use App\Utils\Adapters\Blog\Post;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class TagPostsCaseTest extends TestCase
{
    use BaseMock;

    private $postService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->postService = $this->getBaseMock(PostService::class);
    }

    protected function tearDown(): void
    {
        unset($this->postService);
        parent::tearDown();
    }

    private function getCase(): TagPostsCase
    {
        return new TagPostsCase($this->postService);
    }

    public function testExecute()
    {
        $posts = collect([$this->getPost(), $this->getPost(), $this->getPost()]);
        $this->postService->expects($this->once())
            ->method('byTags')
            ->willReturn($posts);
        $case = $this->getCase();

        $result = $case->execute(['includedTag'], 3);

        $this->assertEquals($posts, $result->getPosts());
    }

    private function getPost(): Post
    {
        return new Post(1, 'title', 'content', 'shortTitle', null, ['includedTag']);
    }
}
