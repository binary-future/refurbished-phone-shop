<?php


namespace Tests\Unit\Application\UseCases\Download;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Downloader;
use App\Application\UseCases\Download\Contracts\OwnerDownloadImageCase;
use App\Domain\Shared\Contracts\HasImage;
use App\Utils\Adapters\Log\Contracts\Logger;
use App\Application\Services\Bus\CommandBus;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

abstract class OwnerDownloadImageCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $downloader;

    private $logger;

    /**
     * @var OwnerDownloadImageCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->downloader = $this->getBaseMock(Downloader::class);
        $this->logger = $this->getBaseMock(Logger::class);
        $this->case = $this->getCase($this->queryBus, $this->commandBus, $this->downloader, $this->logger);
    }

    abstract protected function getCase($queryBus, $commandBus, $downloader, $logger);

    /**
     * @param Collection $owners
     * @param Collection $images
     * @param HasImage $owner
     * @dataProvider validDataProvider
     */
    public function testExecution(Collection $owners, Collection $images, HasImage $owner)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($owners);
        $this->downloader->method('download')
            ->willReturn($images);
        $this->commandBus->method('dispatch')
            ->willReturn($owner);

        $response = $this->case->execute();

        $this->assertTrue($response->isSuccess());
        $this->assertEquals($owners, $response->getOwners());
        $this->assertEquals($this->getType(), $response->getType());
    }

    abstract public function validDataProvider();

    /**
     * @param Collection $owners
     * @param Collection $images
     * @param HasImage $owner
     * @dataProvider validDataProvider
     */
    public function testIfDownloaderThrowsException(Collection $owners, Collection $images, HasImage $owner)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($owners);
        $this->downloader->method('download')
            ->will($this->throwException(new \Exception()));
        $this->commandBus->expects($this->never())
            ->method('dispatch');

        $response = $this->case->execute();

        $this->assertFalse($response->isSuccess());
        $this->assertEquals($this->getType(), $response->getType());
    }

    /**
     * @param Collection $owners
     * @param Collection $images
     * @param HasImage $owner
     * @dataProvider validDataProvider
     */
    public function testIfCommandBusThrowsException(Collection $owners, Collection $images, HasImage $owner)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($owners);
        $this->downloader->method('download')
            ->willReturn($images);
        $this->commandBus->method('dispatch')
            ->will($this->throwException(new \Exception()));

        $response = $this->case->execute();

        $this->assertFalse($response->isSuccess());
        $this->assertEquals($this->getType(), $response->getType());
    }

    abstract protected function getType(): string;

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus, $this->downloader, $this->logger, $this->case);
        parent::tearDown();
    }
}
