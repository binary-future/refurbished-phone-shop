<?php


namespace Tests\Unit\Application\UseCases\Download;


use App\Application\UseCases\Download\StoresDownloadImageCase;
use App\Domain\Shared\Image\Image;
use App\Domain\Store\Store;

final class StoreDownloadImageCaseTest extends OwnerDownloadImageCaseTest
{
    protected function getCase($queryBus, $commandBus, $downloader, $logger)
    {
        return new StoresDownloadImageCase($queryBus, $downloader, $commandBus, $logger);
    }

    public function validDataProvider()
    {
        $store = new Store();
        $store->setSlug('slug');
        $stores = collect([$store]);
        $image = new Image();

        return [
            [$stores, collect([$image]), $store],
        ];
    }

    protected function getType(): string
    {
        return 'stores';
    }
}
