<?php


namespace Tests\Unit\Application\UseCases\Download;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Downloader;
use App\Application\UseCases\Download\Contracts\OwnerDownloadImageCase;
use App\Application\UseCases\Download\DownloadImageCase;
use App\Application\UseCases\Download\Responses\DownloadImageResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Log\Contracts\Logger;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DownloadImageCaseTest extends TestCase
{
    use BaseMock;

    private $storeImageCase;

    private $phonesImageCase;

    /**
     * @var DownloadImageCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storeImageCase = $this->getBaseMock(OwnerDownloadImageCase::class);
        $this->phonesImageCase = $this->getBaseMock(OwnerDownloadImageCase::class);
        $this->case = new DownloadImageCase($this->storeImageCase, $this->phonesImageCase);
    }

    /**
     * @param DownloadImageResponse $stores
     * @param DownloadImageResponse $phones
     * @param array $caseResponse
     * @dataProvider downloadResponsesDataProvider
     */
    public function testExecution(DownloadImageResponse $stores, DownloadImageResponse $phones, array $caseResponse)
    {
        $this->storeImageCase->method('execute')
            ->willReturn($stores);
        $this->phonesImageCase->method('execute')
            ->willReturn($phones);
        $this->assertEquals($caseResponse, $this->case->execute());
    }

    public function downloadResponsesDataProvider()
    {
        $stores = new DownloadImageResponse(collect(), 'stores');
        $phones = new DownloadImageResponse(collect(), 'phones');
        $response = [$stores, $phones];

        return [
            [$stores, $phones, $response]
        ];
    }

    protected function tearDown(): void
    {
        unset($this->storeImageCase, $this->phonesImageCase, $this->case);
        parent::tearDown();
    }
}
