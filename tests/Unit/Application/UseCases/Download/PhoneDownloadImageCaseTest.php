<?php


namespace Tests\Unit\Application\UseCases\Download;


use App\Application\UseCases\Download\PhonesDownloadImageCase;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use Tests\Unit\Traits\BaseMock;

final class PhoneDownloadImageCaseTest extends OwnerDownloadImageCaseTest
{
    use BaseMock;

    protected function getCase($queryBus, $commandBus, $downloader, $logger)
    {
        return new PhonesDownloadImageCase($queryBus, $downloader, $commandBus, $logger);
    }

    public function validDataProvider()
    {
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getOwnerName')
            ->willReturn('Name');
        $phones = collect([$phone]);
        $image = new Image();

        return [
            [$phones, collect([$image]), $phone],
        ];
    }

    protected function getType(): string
    {
        return 'phones';
    }
}

