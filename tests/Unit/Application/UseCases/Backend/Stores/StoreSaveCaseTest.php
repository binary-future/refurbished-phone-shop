<?php

namespace Tests\Unit\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\Services\Importer\Datafeeds\Services\Contracts\Datafeeds;
use App\Application\UseCases\Backend\Stores\Requests\CreateStoreRequest;
use App\Application\UseCases\Backend\Stores\StoreSaveCase;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\Store;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreSaveCaseTest extends TestCase
{
    use BaseMock;

    private $commandBus;

    private $serializer;

    private $uploader;

    private $slugGenerator;

    private $datafeeds;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->uploader = $this->getBaseMock(Uploader::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->datafeeds = $this->getBaseMock(Datafeeds::class);
    }

    private function getUseCase()
    {
        return new StoreSaveCase(
            $this->commandBus,
            $this->serializer,
            $this->uploader,
            $this->slugGenerator,
            $this->datafeeds
        );
    }

    /**
     * @param CreateStoreRequest $request
     * @dataProvider requestWithoutLogoDataProvider
     */
    public function testExecutionIfLogoAbsent(CreateStoreRequest $request)
    {
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->serializer->expects($this->any())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new Store(), new Link()));
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Store());
        $case = $this->getUseCase();

        $response = $case->execute($request);

        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(Store::class, $response->getStore());
    }

    public function requestWithoutLogoDataProvider()
    {
        $request = new CreateStoreRequest();
        $request->setName('name');
        $request->setLink('link');
        $request->setGuarantee(1);
        $request->setTerms(1);
        $request->setAffiliation('test');
        $request->setIsActive(1);
        $request->setDatafeed(['info']);

        return [
            [$request]
        ];
    }

    /**
     * @param CreateStoreRequest $request
     * @dataProvider requestWithLogoDataProvider
     */
    public function testExecutionWithLogo(CreateStoreRequest $request)
    {
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->serializer->expects($this->any())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new Store(), new Link(), new Description()));
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Store());
        $this->datafeeds->expects($this->once())
            ->method('store')
            ->willReturn(true);
        $case = $this->getUseCase();
        $response = $case->execute($request);
        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(Store::class, $response->getStore());
    }

    public function requestWithLogoDataProvider()
    {
        $request = new CreateStoreRequest();
        $request->setName('name');
        $request->setLink('link');
        $request->setGuarantee(1);
        $request->setTerms(1);
        $request->setLogo($this->getBaseMock(UploadedFile::class));
        $request->setAffiliation('test');
        $request->setIsActive(1);
        $request->setDatafeed(['info']);

        return [
            [$request]
        ];
    }

    public function testExecutionReturnsFailedResponseIfWrongData()
    {
        $case = $this->getUseCase();
        $response = $case->execute(new CreateStoreRequest());
        $this->assertFalse($response->isSuccess());
    }
}
