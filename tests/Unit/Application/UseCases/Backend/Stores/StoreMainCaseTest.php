<?php

namespace Tests\Unit\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Stores\StoreMainCase;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new StoreMainCase($this->queryBus);
    }

    public function testExecution()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Store()]));
        $case = $this->getUseCase();
        $response = $case->execute();
        $this->assertInstanceOf(Store::class, $response->getStores()->first());
    }
}
