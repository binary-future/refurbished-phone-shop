<?php

namespace Tests\Unit\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsAPIs;
use App\Application\UseCases\Backend\Stores\StoreCreateCase;
use App\Domain\Store\Affiliation\Affiliation;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreCreateCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $datafeeds;
    private $paymentMethods;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->datafeeds = $this->getBaseMock(DatafeedsAPIs::class);
        $this->paymentMethods = $this->getBaseMock(PaymentMethods::class);
    }

    private function getCase()
    {
        return new StoreCreateCase($this->queryBus, $this->datafeeds, $this->paymentMethods);
    }

    public function testHandling()
    {
        $affiliation = new Affiliation('test');
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$affiliation]));
        $this->datafeeds->method('findBy')
            ->willReturn(collect());
        $this->paymentMethods->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $case = $this->getCase();

        $result = $case->execute();

        $this->assertEquals($affiliation, $result->getAffiliations()->first());
    }
}
