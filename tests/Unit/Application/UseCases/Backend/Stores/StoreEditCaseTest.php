<?php

namespace Tests\Unit\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\LinkGenerator\Contracts\LinkGenerator;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsAPIs;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\UseCases\Backend\Stores\StoreEditCase;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class StoreEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $api;

    private $datafeed;

    private $linkGenerator;
    private $paymentMethods;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->api = $this->getBaseMock(DatafeedsAPIs::class);
        $this->datafeed = $this->getBaseMock(DatafeedsInfo::class);
        $this->linkGenerator = $this->getBaseMock(LinkGenerator::class);
        $this->paymentMethods = $this->getBaseMock(PaymentMethods::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->api,
            $this->datafeed,
            $this->linkGenerator,
            $this->paymentMethods
        );
        parent::tearDown();
    }


    private function getUseCase()
    {
        return new StoreEditCase(
            $this->queryBus,
            $this->datafeed,
            $this->api,
            $this->linkGenerator,
            $this->paymentMethods
        );
    }

    public function testExecution()
    {
        $store = new Store();
        $datafeed = new DatafeedInfo();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$store]), collect()));
        $this->datafeed->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$datafeed]));
        $this->api->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->paymentMethods->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->linkGenerator->expects($this->once())
            ->method('getLink')
            ->willReturn('link');
        $case = $this->getUseCase();

        $response = $case->execute('slug');

        $this->assertEquals($store, $response->getStore());
        $this->assertEquals('link', $response->getDatafeedDownloadLink());
    }

    public function testCaseThrowsExceptionIfStoreNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $this->datafeed->expects($this->never())
            ->method('findBy');
        $this->api->expects($this->never())
            ->method('findBy');
        $this->paymentMethods->expects($this->never())
            ->method('findBy');
        $this->linkGenerator->expects($this->never())
            ->method('getLink');
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute('slug');
    }

    public function testCaseLinkNullIfNotFound()
    {
        $store = new Store();
        $datafeed = new DatafeedInfo();
        $this->queryBus->expects($this->exactly(2))
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$store]), collect()));
        $this->datafeed->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$datafeed]));
        $this->api->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->paymentMethods->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->linkGenerator->expects($this->once())
            ->method('getLink')
            ->willThrowException(new \Exception());
        $case = $this->getUseCase();

        $response = $case->execute('slug');

        $this->assertEquals($store, $response->getStore());
        $this->assertNull($response->getDatafeedDownloadLink());
    }
}
