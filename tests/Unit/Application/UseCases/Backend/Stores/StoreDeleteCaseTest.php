<?php


namespace Tests\Unit\Application\UseCases\Backend\Stores;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Stores\StoreDeleteCase;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class StoreDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getUseCase()
    {
        return new StoreDeleteCase($this->queryBus, $this->commandBus);
    }

    public function testExecution()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Store()]));
        $case = $this->getUseCase();
        $response = $case->execute('slug');
        $this->assertInstanceOf(Store::class, $response->getStore());
    }

    public function testCaseThrowsExceptionIfStoreNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('slug');
    }
}
