<?php


namespace Tests\Unit\Application\UseCases\Backend\Import\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Import\Model\WithoutDealsCase;
use App\Domain\Phone\Model\PhoneModel;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class WithoutDealsCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var WithoutDealsCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new WithoutDealsCase($this->queryBus);
    }

    public function testExecution()
    {
        $models = collect([new PhoneModel()]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($models);
        $result = $this->case->execute();
        $this->assertEquals($models, $result->getModels());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }
}
