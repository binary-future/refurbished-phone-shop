<?php


namespace Tests\Unit\Application\UseCases\Backend\Import\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Import\Model\InactiveListCase;
use App\Domain\Phone\Model\PhoneModel;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class InactiveListCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var InactiveListCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new InactiveListCase($this->queryBus);
    }

    public function testExecution()
    {
        $inactiveModels = collect([new PhoneModel()]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($inactiveModels);
        $result = $this->case->execute();
        $this->assertEquals($inactiveModels, $result->getModels());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }
}
