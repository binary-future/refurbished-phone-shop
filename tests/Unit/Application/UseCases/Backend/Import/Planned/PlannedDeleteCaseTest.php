<?php


namespace Tests\Unit\Application\UseCases\Backend\Import\Planned;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\PlannedDeleteCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PlannedDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $imports;

    /**
     * @var PlannedDeleteCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->imports = $this->getBaseMock(PlannedImports::class);
        $this->case = new PlannedDeleteCase($this->imports);
    }

    public function testThrowsExceptionIfNoPlan()
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(1);
    }

    /**
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testIfRepositoryThrowsException(PlannedImport $import)
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$import]));
        $this->imports->expects($this->once())
            ->method('delete')
            ->with($import)
            ->will($this->throwException(new \Exception()));

        $response = $this->case->execute(1);
        $this->assertFalse($response->isSuccess());
    }

    /**
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testExecution(PlannedImport $import)
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$import]));
        $this->imports->expects($this->once())
            ->method('delete')
            ->with($import)
            ->willReturn(true);

        $response = $this->case->execute(1);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals($import, $response->getPlannedImport());
    }

    /**
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testFailedDeleting(PlannedImport $import)
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$import]));
        $this->imports->expects($this->once())
            ->method('delete')
            ->with($import)
            ->willReturn(false);

        $response = $this->case->execute(1);
        $this->assertFalse($response->isSuccess());
        $this->assertEquals($import, $response->getPlannedImport());
    }

    public function requestDataProvider()
    {
        $date = '2019-12-12';
        $import = new PlannedImport();
        $import->setImportDate($date);

        return [
            [$import],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->imports, $this->case);
        parent::tearDown();
    }
}
