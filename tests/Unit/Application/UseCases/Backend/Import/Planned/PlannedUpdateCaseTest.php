<?php


namespace Tests\Unit\Application\UseCases\Backend\Import\Planned;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\PlannedUpdateCase;
use App\Application\UseCases\Backend\Import\Planned\Requests\UpdatePlan;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PlannedUpdateCaseTest extends TestCase
{
    use BaseMock;

    private $imports;

    /**
     * @var PlannedUpdateCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->imports = $this->getBaseMock(PlannedImports::class);
        $this->case = new PlannedUpdateCase($this->imports);
    }

    public function testThrowsExceptionIfNoPlan()
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(new UpdatePlan(1, '1231'));
    }

    /**
     * @param string $date
     * @param UpdatePlan $request
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testIfRepositoryThrowsException(UpdatePlan $request, PlannedImport $import)
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$import]));
        $this->imports->expects($this->once())
            ->method('update')
            ->will($this->throwException(new \Exception()));

        $response = $this->case->execute($request);
        $this->assertFalse($response->isSuccess());
    }

    /**
     * @param string $date
     * @param UpdatePlan $request
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testExecution(UpdatePlan $request, PlannedImport $import)
    {
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$import]));
        $this->imports->expects($this->once())
            ->method('update')
            ->with($import, $request->getParams())
            ->willReturn($import);

        $response = $this->case->execute($request);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals($import, $response->getPlannedImport());
    }

    public function requestDataProvider()
    {
        $date = '2019-12-12';
        $request = new UpdatePlan(1, $date);
        $import = new PlannedImport();
        $import->setImportDate($date);

        return [
            [$request, $import],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->imports, $this->case);
        parent::tearDown();
    }
}
