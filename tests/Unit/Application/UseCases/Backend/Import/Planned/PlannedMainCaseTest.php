<?php

namespace Tests\Unit\Application\UseCases\Backend\Import\Planned;

use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\PlannedMainCase;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PlannedMainCaseTest extends TestCase
{
    use BaseMock;

    private $imports;

    /**
     * @var PlannedMainCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->imports = $this->getBaseMock(PlannedImports::class);
        $this->case = new PlannedMainCase($this->imports);
    }

    protected function tearDown(): void
    {
        unset($this->imports, $this->case);
        parent::tearDown();
    }

    public function testExecution()
    {
        $plannedImports = collect([new PlannedImport()]);
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn($plannedImports);

        $response = $this->case->execute();

        $this->assertEquals($response->getPlannedImports(), $plannedImports);
    }

    public function testEmptyResponse()
    {
        $plannedImports = collect();
        $this->imports->expects($this->once())
            ->method('findBy')
            ->willReturn($plannedImports);

        $response = $this->case->execute();

        $this->assertEquals($response->getPlannedImports(), $plannedImports);
    }
}
