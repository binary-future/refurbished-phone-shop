<?php

namespace Tests\Unit\Application\UseCases\Backend\Import\Planned;

use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\PlannedSaveCase;
use App\Application\UseCases\Backend\Import\Planned\Requests\CreatePlan;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PlannedSaveCaseTest extends TestCase
{
    use BaseMock;

    private $serializer;

    private $imports;

    /**
     * @var PlannedSaveCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->imports = $this->getBaseMock(PlannedImports::class);
        $this->case = new PlannedSaveCase($this->serializer, $this->imports);
    }

    public function testIfSerializerThrowsException()
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->will($this->throwException(new \Exception()));
        $this->imports->expects($this->never())
            ->method('save');

        $response = $this->case->execute(new CreatePlan('1231'));
        $this->assertFalse($response->isSuccess());
    }

    /**
     * @param string $date
     * @param CreatePlan $request
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testIfRepositoryThrowsException(CreatePlan $request, PlannedImport $import)
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($import);
        $this->imports->expects($this->once())
            ->method('save')
            ->will($this->throwException(new \Exception()));

        $response = $this->case->execute($request);
        $this->assertFalse($response->isSuccess());
    }

    /**
     * @param string $date
     * @param CreatePlan $request
     * @param PlannedImport $import
     * @dataProvider requestDataProvider
     */
    public function testExecution(CreatePlan $request, PlannedImport $import)
    {
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($import);
        $this->imports->expects($this->once())
            ->method('save')
            ->with($import)
            ->willReturn($import);

        $response = $this->case->execute($request);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals($import, $response->getPlannedImport());
    }

    public function requestDataProvider()
    {
        $date = '2019-12-12';
        $request = new CreatePlan($date);
        $import = new PlannedImport();
        $import->setImportDate($date);

        return [
            [$request, $import],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->serializer, $this->imports, $this->case);
        parent::tearDown();
    }
}
