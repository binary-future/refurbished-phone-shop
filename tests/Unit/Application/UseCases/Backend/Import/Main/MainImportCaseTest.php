<?php


namespace Tests\Unit\Application\UseCases\Backend\Import\Main;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Main\MainImportCase;
use App\Domain\Phone\Model\PhoneModel;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class MainImportCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $plannedImports;

    /**
     * @var MainImportCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->plannedImports = $this->getBaseMock(PlannedImports::class);
        $this->case = new MainImportCase($this->queryBus, $this->plannedImports);
    }

    public function testExecution()
    {
        $inactiveModels = collect([new PhoneModel()]);
        $modelsWithoutDeals = $inactiveModels->push(new PhoneModel());
        $plannedImports = collect([new PlannedImport()]);
        $this->queryBus->method('dispatch')
            ->will($this->onConsecutiveCalls($inactiveModels, $modelsWithoutDeals));
        $this->plannedImports->expects($this->once())
            ->method('findBy')
            ->willReturn($plannedImports);
        $result = $this->case->execute();
        $this->assertEquals($inactiveModels, $result->getInActiveModels());
        $this->assertEquals($modelsWithoutDeals, $result->getModelsWithoutDeals());
        $this->assertEquals($plannedImports, $result->getPlannedImports());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }
}
