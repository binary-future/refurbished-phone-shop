<?php

namespace Tests\Unit\Application\UseCases\Backend\Main;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Main\MainPageCase;
use App\Application\UseCases\Backend\Main\Responses\MainPageResponse;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Store\Store;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

class MainPageCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $positiveReports;
    private $negativeReports;
    private $plannedImports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->positiveReports = $this->getBaseMock(PositiveReports::class);
        $this->negativeReports = $this->getBaseMock(NegativeReports::class);
        $this->plannedImports = $this->getBaseMock(PlannedImports::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->positiveReports,
            $this->negativeReports,
            $this->plannedImports
        );
        parent::tearDown();
    }

    /**
     * @return MainPageCase
     */
    public function getCase(): MainPageCase
    {
        return new MainPageCase(
            $this->queryBus,
            $this->positiveReports,
            $this->negativeReports,
            $this->plannedImports
        );
    }

    /**
     * @param Collection $stores
     * @param Collection $positiveDealsReports
     * @param Collection $negativeDealsReports
     * @param Collection $inactiveModels
     * @param Collection $positivePhonesReports
     * @param Collection $negativePhonesReports
     * @param Collection $modelsWithoutDeals
     * @param Collection $plannedImports
     * @param MainPageResponse $expectedResponse
     * @throws \Exception
     * @dataProvider executeDataProvider
     */
    public function testExecute(
        Collection $stores,
        Collection $positiveDealsReports,
        Collection $negativeDealsReports,
        Collection $inactiveModels,
        Collection $positivePhonesReports,
        Collection $negativePhonesReports,
        Collection $modelsWithoutDeals,
        Collection $plannedImports,
        MainPageResponse $expectedResponse
    ) {
        $this->queryBus->expects($this->exactly(3))->method('dispatch')
            ->willReturnOnConsecutiveCalls($stores, $inactiveModels, $modelsWithoutDeals);
        $this->positiveReports->expects($this->exactly(2))->method('findBy')
            ->willReturnOnConsecutiveCalls($positiveDealsReports, $positivePhonesReports);
        $this->negativeReports->expects($this->exactly(2))->method('findBy')
            ->willReturnOnConsecutiveCalls($negativeDealsReports, $negativePhonesReports);
        $this->plannedImports->expects($this->once())->method('findBy')
            ->willReturn($plannedImports);
        $case = $this->getCase();

        $actualResponse = $case->execute();

        $this->assertEquals($expectedResponse, $actualResponse);
    }

    /**
     * @return array
     */
    public function executeDataProvider(): array
    {
        $stores = collect([new Store()]);
        $positiveDealsReports = collect([new PositiveReport([PositiveReport::FIELD_SOURCE_ID => 1])]);
        $negativeDealsReports = collect([new NegativeReport([NegativeReport::FIELD_SOURCE_ID => 2])]);
        $inactiveModels = collect([new PhoneModel()]);
        $positivePhonesReports = collect([new PositiveReport([PositiveReport::FIELD_SOURCE_ID => 1])]);
        $negativePhonesReports = collect([new NegativeReport([NegativeReport::FIELD_SOURCE_ID => 2])]);
        $modelsWithoutDeals = collect([new PhoneModel()]);
        $plannedImports = collect([new PlannedImport()]);

        $expectedResponse = new MainPageResponse(
            $stores,
            $positiveDealsReports->groupBy(PositiveReport::FIELD_SOURCE_ID),
            $negativeDealsReports->groupBy(PositiveReport::FIELD_SOURCE_ID),
            $inactiveModels,
            $positivePhonesReports->groupBy(PositiveReport::FIELD_SOURCE_ID),
            $negativePhonesReports->groupBy(PositiveReport::FIELD_SOURCE_ID),
            $modelsWithoutDeals,
            $plannedImports
        );

        return [
            [
                $stores, $positiveDealsReports, $negativeDealsReports, $inactiveModels,
                $positivePhonesReports, $negativePhonesReports, $modelsWithoutDeals, $plannedImports,
                $expectedResponse
            ]
        ];
    }
}
