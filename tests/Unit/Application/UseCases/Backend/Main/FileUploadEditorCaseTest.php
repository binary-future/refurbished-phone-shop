<?php


namespace Tests\Unit\Application\UseCases\Backend\Main;

use App\Application\UseCases\Backend\Main\FileUploadEditorCase;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\File\Upload\Contract\FileUploader;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class FileUploadEditorCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var FileUploadEditorCase
     */
    private $case;

    /**
     * @var FileUploader
     */
    private $fileUploader;

    private $config;

    /**
     * @var UploadedFile
     */
    private $uploadedFile;

    protected function setUp(): void
    {
        parent::setUp();
        $this->fileUploader = $this->getBaseMock(FileUploader::class);
        $this->config = $this->getBaseMock(Config::class);
        $this->config->method('get')->willReturn(
            [
                'base_extension' => 'jpg',
                'storage_suffix' => 'storage/upload',
                'base_upload_path' => 'public/upload/descriptions',
            ]
        );
        $this->case = new FileUploadEditorCase($this->config, $this->fileUploader);
        $this->uploadedFile = $this->getUploadedFile();
    }

    public function testUploaderThrowsException()
    {
        $this->fileUploader->method('store')
            ->will($this->throwException(new \Exception()));
        $response = $this->case->execute($this->uploadedFile);

        $this->assertFalse($response->isSucceed());
    }

    public function testExecution()
    {
        $location = 'location';
        $this->fileUploader->method('store')
            ->willReturn($location);
        $response = $this->case->execute($this->uploadedFile);

        $this->assertEquals($location, $response->getLocation());
        $this->assertTrue($response->isSucceed());
    }

    private function getUploadedFile(): UploadedFile
    {
        $reflectionClass = new \ReflectionClass(self::class);
        $uploadedFile = new UploadedFile(
            $reflectionClass->getFileName(),
            'name',
            'image'
        );

        return $uploadedFile;
    }

    protected function tearDown(): void
    {
        unset($this->case, $this->fileUploader, $this->uploadedFile);
        parent::tearDown();
    }
}
