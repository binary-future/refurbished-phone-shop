<?php

namespace Tests\Unit\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\Request\UserUpdateRequest;
use App\Application\UseCases\Backend\User\User\UserUpdateCase;
use App\Domain\User\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserUpdateCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var UserUpdateCase
     */
    private $case;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new UserUpdateCase($this->queryBus, $this->commandBus);
    }

    /**
     * @param UserUpdateRequest $request
     * @dataProvider validCommandDataProvider
     */
    public function testExecution(UserUpdateRequest $request)
    {
        $user = new User();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$user]));
        $this->commandBus->expects($this->once())->method('dispatch')->willReturn($user);
        $response = $this->case->execute($request);
        $this->assertEquals($user, $response->getUser());
        $this->assertTrue($response->isSucceed());
    }

    public function validCommandDataProvider()
    {
        $request = new UserUpdateRequest();
        $request->setUserId(1);
        $request->setName('name');
        $request->setEmail('email');
        $request->setRole(1);

        return [
            [$request]
        ];
    }

    /**
     * @param UserUpdateRequest $request
     * @dataProvider validCommandDataProvider
     */
    public function testIFCommandThrowsException(UserUpdateRequest $request)
    {
        $user = new User();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$user]));
        $this->commandBus->expects($this->once())->method('dispatch')->will($this->throwException(new \Exception()));
        $response = $this->case->execute($request);
        $this->assertFalse($response->isSucceed());
    }

    /**
     * @param UserUpdateRequest $request
     * @dataProvider validCommandDataProvider
     */
    public function testThrowsExceptionIfNoUser(UserUpdateRequest $request)
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    protected function tearDown(): void
    {
        unset($this->commandBus, $this->queryBus, $this->case);
        parent::tearDown();
    }
}
