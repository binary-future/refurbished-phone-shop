<?php

namespace Tests\Unit\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\UserCreateCase;
use App\Domain\User\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserCreateCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var UserCreateCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new UserCreateCase($this->queryBus);
    }

    public function testExecution()
    {
        $roles = collect([new Role()]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($roles);
        $response = $this->case->execute();
        $this->assertEquals($roles, $response->getRoles());
    }

    public function testThrowsExceptionIfNoRoles()
    {
        $roles = collect();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($roles);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute();
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }
}
