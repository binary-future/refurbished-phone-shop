<?php

namespace Tests\Unit\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\UserDeleteCase;
use App\Domain\User\User;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserDeleteCaseTest extends  TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $specification;

    /**
     * @var UserDeleteCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->specification = $this->getBaseMock(Specification::class);
        $this->case = new UserDeleteCase($this->queryBus, $this->commandBus, $this->specification);
    }

    public function testExecution()
    {
        $user = new User();
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$user]));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($user);
        $response = $this->case->execute(1);
        $this->assertEquals($user, $response->getUser());
        $this->assertTrue($response->isSuccess());
    }

    public function testThrowsExceptionIfNoUser()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect()));
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(1);
    }

    public function testReturnsFailedResponseIfCommandBusThrowsException()
    {
        $user = new User();
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$user]));
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->will($this->throwException(new \Exception()));
        $response = $this->case->execute(1);
        $this->assertEquals($user, $response->getUser());
        $this->assertFalse($response->isSuccess());
    }

    public function testThrowsExceptionIfSpecificationNotPass()
    {
        $user = new User();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$user])));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(1);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case, $this->commandBus, $this->specification);
        parent::tearDown();
    }
}
