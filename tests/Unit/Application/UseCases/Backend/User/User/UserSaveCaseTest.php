<?php

namespace Tests\Unit\Application\UseCases\Backend\User\User;


use App\Application\Services\Bus\CommandBus;
use App\Application\UseCases\Backend\User\User\Request\UserSaveRequest;
use App\Application\UseCases\Backend\User\User\UserSaveCase;
use App\Domain\User\User;
use App\Utils\Adapters\Encryptor\Contract\Encryptor;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserSaveCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var UserSaveCase
     */
    private $case;

    private $commandBus;

    private $encryptor;

    private $serializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->encryptor = $this->getBaseMock(Encryptor::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->case = new UserSaveCase($this->commandBus, $this->encryptor, $this->serializer);
    }

    /**
     * @param UserSaveRequest $request
     * @dataProvider validCommandDataProvider
     */
    public function testExecution(UserSaveRequest $request)
    {
        $user = new User();
        $this->encryptor->expects($this->once())->method('encrypt')->willReturn('hash');
        $this->serializer->expects($this->once())->method('fromArray')->willReturn(new User());
        $this->commandBus->expects($this->once())->method('dispatch')->willReturn($user);
        $response = $this->case->execute($request);
        $this->assertEquals($user, $response->getUser());
        $this->assertTrue($response->isSucceed());
    }

    public function validCommandDataProvider()
    {
        $request = new UserSaveRequest();
        $request->setName('name');
        $request->setEmail('email');
        $request->setPassword('password');
        $request->setRole(1);

        return [
            [$request]
        ];
    }

    /**
     * @param UserSaveRequest $request
     * @dataProvider validCommandDataProvider
     */
    public function testIFCommandThrowsException(UserSaveRequest $request)
    {
        $this->encryptor->expects($this->once())->method('encrypt')->willReturn('hash');
        $this->serializer->expects($this->once())->method('fromArray')->willReturn(new User());
        $this->commandBus->expects($this->once())->method('dispatch')->will($this->throwException(new \Exception()));
        $response = $this->case->execute($request);
        $this->assertFalse($response->isSucceed());
    }

    /**
     * @param UserSaveRequest $request
     * @dataProvider validCommandDataProvider
     */
    public function testIFSerializerThrowsException(UserSaveRequest $request)
    {
        $this->encryptor->expects($this->once())->method('encrypt')->willReturn('hash');
        $this->serializer->expects($this->once())->method('fromArray')->will($this->throwException(new \Exception()));
        $response = $this->case->execute($request);
        $this->assertFalse($response->isSucceed());
    }

    /**
     * @param UserSaveRequest $request
     * @dataProvider invalidRequestDataProvider
     */
    public function testIfInvalidRequest(UserSaveRequest $request)
    {
        $response = $this->case->execute($request);
        $this->assertFalse($response->isSucceed());
    }

    public function invalidRequestDataProvider()
    {
        $request = new UserSaveRequest();
        return [
            [$request]
        ];
    }

    protected function tearDown(): void
    {
        unset($this->commandBus, $this->encryptor, $this->case, $this->serializer);
        parent::tearDown();
    }
}
