<?php

namespace Tests\Unit\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\UserListCase;
use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserListCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $authService;

    /**
     * @var UserListCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->authService = $this->getBaseMock(Auth::class);
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = $this->getUseCase();
    }

    private function getUseCase()
    {
        return new UserListCase($this->authService, $this->queryBus);
    }

    public function testExecution()
    {
        $user = new User();
        $users = collect([new User()]);
        $this->authService->expects($this->once())
            ->method('getAuthUser')
            ->willReturn($user);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($users);
        $response = $this->case->execute();
        $this->assertInstanceOf(User::class, $response->getUsers()->first());
        $this->assertEquals($users, $response->getUsers());
        $this->assertEquals($user, $response->getUser());
    }

    public function testThrowsExceptionIfNoAuthUser()
    {
        $this->authService->expects($this->once())
            ->method('getAuthUser')
            ->willReturn(null);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute();
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->authService, $this->case);
        parent::tearDown();
    }
}
