<?php

namespace Tests\Unit\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\UserEditCase;
use App\Domain\User\Role;
use App\Domain\User\User;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UserEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $specification;

    /**
     * @var UserEditCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->specification = $this->getBaseMock(Specification::class);
        $this->case = new UserEditCase($this->queryBus, $this->specification);
    }

    public function testExecution()
    {
        $user = new User();
        $roles = collect([new Role()]);
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$user]), $roles));
        $response = $this->case->execute(1);
        $this->assertEquals($user, $response->getUser());
        $this->assertEquals($roles, $response->getRoles());
    }

    public function testThrowsExceptionIfNoUser()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect()));
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(1);
    }

    public function testThrowsExceptionIfNoRoles()
    {
        $user = new User();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$user]), collect()));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(true);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(1);
    }

    public function testThrowsExceptionIfSpecificationNotPass()
    {
        $user = new User();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$user])));
        $this->specification->expects($this->once())
            ->method('isSatisfy')
            ->willReturn(false);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(1);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case, $this->specification);
        parent::tearDown();
    }
}
