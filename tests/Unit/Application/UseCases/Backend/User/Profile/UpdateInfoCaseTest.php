<?php

namespace Tests\Unit\Application\UseCases\Backend\User\Profile;

use App\Application\Services\Bus\CommandBus;
use App\Application\UseCases\Backend\User\Profile\Requests\UpdateInfoRequest;
use App\Application\UseCases\Backend\User\Profile\UpdateInfoCase;
use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UpdateInfoCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var UpdateInfoCase
     */
    private $case;

    private $auth;

    private $specification;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = $this->getBaseMock(Auth::class);
        $this->specification = $this->getBaseMock(Specification::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->case = new UpdateInfoCase($this->auth, $this->specification, $this->commandBus);
    }

    protected function tearDown(): void
    {
        unset($this->case, $this->auth, $this->specification, $this->commandBus);
        parent::tearDown();
    }

    /**
     * @param string $password
     * @param UpdateInfoRequest $request
     * @dataProvider validRequestDataProvider
     */
    public function testExecution(string $password, UpdateInfoRequest $request)
    {
        $user = new User();
        $user->setPassword($password);
        $this->auth->method('getAuthUser')->willReturn($user);
        $this->specification->method('isSatisfy')->willReturn(true);
        $this->commandBus->method('dispatch')->willReturn($user);
        $response = $this->case->execute($request);
        $this->assertEquals($user, $response->getUser());
        $this->assertTrue($response->isSucceed());
    }

    public function validRequestDataProvider()
    {
        $password = 'string';
        $request = new UpdateInfoRequest();
        $request->setEmail('email');
        $request->setName('name');
        $request->setCheckPassword($password);

        return [
            [$password, $request]
        ];
    }

    /**
     * @param string $password
     * @param UpdateInfoRequest $request
     * @dataProvider validRequestDataProvider
     */
    public function testThrowsExceptionIfNoAuthUser(string $password, UpdateInfoRequest $request)
    {
        $this->auth->method('getAuthUser')->willReturn(null);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    /**
     * @param string $password
     * @param UpdateInfoRequest $request
     * @dataProvider validRequestDataProvider
     */
    public function testIfSpecificationFailed(string $password, UpdateInfoRequest $request)
    {
        $user = new User();
        $user->setPassword($password);
        $this->auth->method('getAuthUser')->willReturn($user);
        $this->specification->method('isSatisfy')->willReturn(false);
        $response = $this->case->execute($request);
        $this->assertEquals($user, $response->getUser());
        $this->assertFalse($response->isSucceed());
        $this->assertNotNull($response->getMessage());
    }

    /**
     * @param string $password
     * @param UpdateInfoRequest $request
     * @dataProvider validRequestDataProvider
     */
    public function testIfCommandThrowsException(string $password, UpdateInfoRequest $request)
    {
        $user = new User();
        $user->setPassword($password);
        $this->auth->method('getAuthUser')->willReturn($user);
        $this->specification->method('isSatisfy')->willReturn(true);
        $this->commandBus->method('dispatch')->will($this->throwException(new \Exception()));
        $response = $this->case->execute($request);
        $this->assertEquals($user, $response->getUser());
        $this->assertFalse($response->isSucceed());
    }

    /**
     * @param UpdateInfoRequest $request
     * @dataProvider invalidRequestDataProvider
     */
    public function testIfInvalidRequest(UpdateInfoRequest $request)
    {
        $user = new User();
        $user->setPassword('password');
        $this->auth->method('getAuthUser')->willReturn($user);
        $response = $this->case->execute($request);
        $this->assertEquals($user, $response->getUser());
        $this->assertFalse($response->isSucceed());
    }

    public function invalidRequestDataProvider()
    {
        return [
            [new UpdateInfoRequest()]
        ];
    }
}
