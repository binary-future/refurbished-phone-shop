<?php

namespace Tests\Unit\Application\UseCases\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\ProfileViewCase;
use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ProfileViewCaseTest extends TestCase
{
    use BaseMock;

    private $auth;

    /**
     * @var ProfileViewCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->auth = $this->getBaseMock(Auth::class);
        $this->case = new ProfileViewCase($this->auth);
    }

    public function testHandling()
    {
        $user = new User();
        $this->auth->method('getAuthUser')->willReturn($user);
        $response = $this->case->execute();
        $this->assertEquals($user, $response->getUser());
    }

    public function testThrowsExceptionIfNoUserAuthorized()
    {
        $this->auth->method('getAuthUser')->willReturn(null);
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute();
    }

    protected function tearDown(): void
    {
        unset($this->auth, $this->case);
        parent::tearDown();
    }
}
