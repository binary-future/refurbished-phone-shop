<?php

namespace Tests\Unit\Application\UseCases\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Contracts\UpdateInfoCase;
use App\Application\UseCases\Backend\User\Profile\Requests\UpdateInfoRequest;
use App\Application\UseCases\Backend\User\Profile\Responses\ProfileUpdateResponse;
use App\Application\UseCases\Backend\User\Profile\UpdatePasswordCase;
use App\Domain\User\User;
use App\Utils\Adapters\Encryptor\Contract\Encryptor;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UpdatePasswordCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var UpdatePasswordCase
     */
    private $case;

    private $encryptor;

    /**
     * @var UpdateInfoCase
     */
    private $updateProfileCase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->updateProfileCase = $this->getBaseMock(UpdateInfoCase::class);
        $this->encryptor = $this->getBaseMock(Encryptor::class);
        $this->case = new UpdatePasswordCase($this->updateProfileCase, $this->encryptor);
    }

    /**
     * @param UpdateInfoRequest $request
     * @param ProfileUpdateResponse $response
     * @dataProvider validDataProvider
     */
    public function testExecution(UpdateInfoRequest $request, ProfileUpdateResponse $response)
    {
        $this->encryptor->method('encrypt')->willReturn('password');
        $this->updateProfileCase->method('execute')->willReturn($response);
        $result = $this->case->execute($request);
        $this->assertEquals($response, $result);
    }

    public function validDataProvider()
    {
        $request = new UpdateInfoRequest();
        $request->setCheckPassword('password');
        $request->setPassword('password');

        $response = new ProfileUpdateResponse(new User());

        return [
            [$request, $response]
        ];
    }
}
