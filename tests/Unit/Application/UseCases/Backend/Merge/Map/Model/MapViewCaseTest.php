<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Map\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Merge\Map\Model\MapViewCase;
use App\Domain\Phone\Model\PhoneModel;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class MapViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var MapViewCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new MapViewCase($this->queryBus);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }

    public function testExecution()
    {
        $models = collect([new PhoneModel()]);
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn($models);
        $response = $this->case->execute();
        $this->assertEquals($models, $response->getModels());
    }
}
