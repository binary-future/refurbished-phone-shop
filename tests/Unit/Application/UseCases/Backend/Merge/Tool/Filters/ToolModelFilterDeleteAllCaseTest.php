<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;


use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteAllCase;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolModelFilterDeleteAllCaseTest extends TestCase
{
    use BaseMock;

    private $filters;

    /**
     * @var ToolModelFilterDeleteAllCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolModelFilterDeleteAllCase($this->filters);
    }

    protected function tearDown(): void
    {
        unset($this->filters, $this->case);
        parent::tearDown();
    }

    public function testFiltersDeletion()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->any())->method('deleteSeveral')->willReturn(true);
        $response = $this->case->execute();
        $this->assertTrue($response->isSucceed());
    }

    public function testFiltersThrowsException()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->once())->method('deleteSeveral')->will($this->throwException(new \Exception()));
        $response = $this->case->execute();
        $this->assertFalse($response->isSucceed());
    }

    private function getFilter(): Filter
    {
        $filter = new Filter();
        $filter->setBrandId(1);

        return $filter;
    }
}
