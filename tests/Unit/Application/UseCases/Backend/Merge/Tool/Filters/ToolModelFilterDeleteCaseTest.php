<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteCase;
use App\Domain\Phone\Brand\Brand;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolModelFilterDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $filters;

    private $queryBus;

    /**
     * @var ToolModelFilterDeleteCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new ToolModelFilterDeleteCase($this->filters, $this->queryBus);
    }

    protected function tearDown(): void
    {
        unset($this->filters, $this->case);
        parent::tearDown();
    }

    public function testFilterDeletion()
    {
        $filter = $this->getFilter();
        $brand = new Brand();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('delete')->willReturn(true);
        $response = $this->case->execute(12);
        $this->assertEquals($response->getBrand(), $brand);
        $this->assertEquals($response->getFilter(), $filter);
        $this->assertTrue($response->isSucceed());
    }

    public function testThrowsExceptionIfNoFilter()
    {
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect());
        $this->filters->expects($this->never())->method('delete');
        $this->queryBus->expects($this->never())->method('dispatch');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(12);
    }

    public function testThrowsExceptionIfNoBrand()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->filters->expects($this->never())->method('delete');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(12);
    }

    public function testFiltersThrowsException()
    {
        $filter = $this->getFilter();
        $brand = new Brand();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('delete')->will($this->throwException(new \Exception()));
        $response = $this->case->execute(12);
        $this->assertEquals($response->getBrand(), $brand);
        $this->assertEquals($response->getFilter(), $filter);
        $this->assertFalse($response->isSucceed());
    }

    private function getFilter(): Filter
    {
        $filter = new Filter();
        $filter->setBrandId(1);

        return $filter;
    }
}
