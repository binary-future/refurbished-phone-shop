<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;

use App\Application\Services\Merge\Color\Filter;
use App\Application\Services\Merge\Color\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolColorFilterDeleteCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolColorFilterDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $filters;

    private $queryBus;

    /**
     * @var ToolColorFilterDeleteCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolColorFilterDeleteCase($this->filters);
    }

    protected function tearDown(): void
    {
        unset($this->filters, $this->case);
        parent::tearDown();
    }

    public function testFilterDeletion()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->once())->method('delete')->willReturn(true);
        $response = $this->case->execute(12);
        $this->assertEquals($response->getFilter(), $filter);
        $this->assertTrue($response->isSucceed());
    }

    public function testThrowsExceptionIfNoFilter()
    {
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect());
        $this->filters->expects($this->never())->method('delete');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(12);
    }

    public function testFiltersThrowsException()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->once())->method('delete')->will($this->throwException(new \Exception()));
        $response = $this->case->execute(12);
        $this->assertEquals($response->getFilter(), $filter);
        $this->assertFalse($response->isSucceed());
    }

    private function getFilter(): Filter
    {
        $filter = new Filter();

        return $filter;
    }
}
