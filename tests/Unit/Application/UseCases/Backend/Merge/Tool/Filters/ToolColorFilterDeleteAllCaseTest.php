<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;

use App\Application\Services\Merge\Color\Filter;
use App\Application\Services\Merge\Color\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolColorFilterDeleteAllCase;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolColorFilterDeleteAllCaseTest extends TestCase
{
    use BaseMock;

    private $filters;

    /**
     * @var ToolColorFilterDeleteAllCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolColorFilterDeleteAllCase($this->filters);
    }

    protected function tearDown(): void
    {
        unset($this->filters, $this->case);
        parent::tearDown();
    }

    public function testFiltersDeletion()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->any())->method('deleteSeveral')->willReturn(true);
        $response = $this->case->execute();
        $this->assertTrue($response->isSucceed());
    }

    public function testFiltersThrowsException()
    {
        $filter = $this->getFilter();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->once())->method('deleteSeveral')->will($this->throwException(new \Exception()));
        $response = $this->case->execute();
        $this->assertFalse($response->isSucceed());
    }

    private function getFilter(): Filter
    {
        $filter = new Filter();

        return $filter;
    }
}
