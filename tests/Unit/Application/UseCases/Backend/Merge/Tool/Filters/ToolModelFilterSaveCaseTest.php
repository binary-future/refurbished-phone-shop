<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Requests\ModelFilterSaveRequest;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterSaveCase;
use App\Domain\Phone\Brand\Brand;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolModelFilterSaveCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $filters;

    /**
     * @var ToolModelFilterSaveCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolModelFilterSaveCase($this->queryBus, $this->filters);
    }

    public function testThrowsExceptionIfNoBrandFound()
    {
        $request = new ModelFilterSaveRequest();
        $request->setBrandId(1);
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->filters->expects($this->never())->method('findBy');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    public function testIfFilterNotExist()
    {
        $request = new ModelFilterSaveRequest();
        $request->setBrandId(1);
        $request->setFilterId(1);
        $brand = new Brand();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    public function testIfParentNotExits()
    {
        $request = new ModelFilterSaveRequest();
        $request->setBrandId(1);
        $request->setParentId(1);
        $request->setFilterId(2);
        $brand = new Brand();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    public function testFilterCreation()
    {
        $filter = new Filter();
        $request = new ModelFilterSaveRequest();
        $request->setBrandId(1);
        $request->setName('name');
        $brand = new Brand();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('create')->willReturn($filter);
        $response = $this->case->execute($request);
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($filter, $response->getFilter());
        $this->assertTrue($response->isSucceed());
    }

    public function testInvalidResponseIfParentAndFilterAreTheSame()
    {
        $request = new ModelFilterSaveRequest();
        $request->setBrandId(1);
        $request->setName('name');
        $request->setFilterId(1);
        $request->setParentId(1);
        $brand = new Brand();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $response = $this->case->execute($request);
        $this->assertFalse($response->isSucceed());
    }

    public function testFilterUpdating()
    {
        $filter = new Filter();
        $request = new ModelFilterSaveRequest();
        $request->setBrandId(1);
        $request->setName('name');
        $request->setFilterId(1);
        $brand = new Brand();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->once())->method('update')->willReturn($filter);
        $response = $this->case->execute($request);
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($filter, $response->getFilter());
        $this->assertTrue($response->isSucceed());
    }
}
