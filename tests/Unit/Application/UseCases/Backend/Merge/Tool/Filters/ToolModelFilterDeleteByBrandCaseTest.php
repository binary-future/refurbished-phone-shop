<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteByBrandCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteCase;
use App\Domain\Phone\Brand\Brand;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolModelFilterDeleteByBrandCaseTest extends TestCase
{
    use BaseMock;

    private $filters;

    private $queryBus;

    /**
     * @var ToolModelFilterDeleteByBrandCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new ToolModelFilterDeleteByBrandCase($this->queryBus, $this->filters);
    }

    protected function tearDown(): void
    {
        unset($this->filters, $this->case);
        parent::tearDown();
    }

    public function testFiltersDeletion()
    {
        $filter = $this->getFilter();
        $brand = $this->getBrand();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->any())->method('deleteSeveral')->willReturn(true);
        $response = $this->case->execute('brand');
        $this->assertEquals($response->getBrand(), $brand);
        $this->assertTrue($response->isSucceed());
    }

    public function testThrowsExceptionIfNoBrand()
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->filters->expects($this->never())->method('findBy');
        $this->filters->expects($this->never())->method('deleteSeveral');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('brand');
    }

    public function testFiltersThrowsException()
    {
        $filter = $this->getFilter();
        $brand = $this->getBrand();
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->expects($this->once())->method('deleteSeveral')->will($this->throwException(new \Exception()));
        $response = $this->case->execute('brand');
        $this->assertEquals($response->getBrand(), $brand);
        $this->assertFalse($response->isSucceed());
    }

    private function getFilter(): Filter
    {
        $filter = new Filter();
        $filter->setBrandId(1);

        return $filter;
    }

    private function getBrand(): Brand
    {
        $brand = new Brand();
        $brand->id = 1;

        return $brand;
    }
}
