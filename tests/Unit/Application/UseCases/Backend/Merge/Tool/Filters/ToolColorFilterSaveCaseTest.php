<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Filters;


use App\Application\Services\Merge\Color\Filter;
use App\Application\Services\Merge\Color\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Requests\ColorFilterSaveRequest;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolColorFilterSaveCase;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolColorFilterSaveCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $filters;

    /**
     * @var ToolColorFilterSaveCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolColorFilterSaveCase($this->filters);
    }

    public function testIfFilterNotExist()
    {
        $request = new ColorFilterSaveRequest();
        $request->setFilterId(1);
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    public function testIfParentNotExits()
    {
        $request = new ColorFilterSaveRequest();
        $request->setParentId(1);
        $request->setFilterId(2);
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($request);
    }

    public function testFilterCreation()
    {
        $filter = new Filter();
        $request = new ColorFilterSaveRequest();
        $request->setName('name');
        $this->filters->expects($this->once())->method('create')->willReturn($filter);
        $response = $this->case->execute($request);
        $this->assertEquals($filter, $response->getFilter());
        $this->assertTrue($response->isSucceed());
    }

    public function testInvalidResponseIfParentAndFilterAreTheSame()
    {
        $request = new ColorFilterSaveRequest();
        $request->setName('name');
        $request->setFilterId(1);
        $request->setParentId(1);
        $response = $this->case->execute($request);
        $this->assertFalse($response->isSucceed());
    }

    public function testFilterUpdating()
    {
        $filter = new Filter();
        $request = new ColorFilterSaveRequest();
        $request->setName('name');
        $request->setFilterId(1);
        $this->filters->expects($this->once())->method('findBy')->willReturn(collect([$filter]));
        $this->filters->expects($this->once())->method('update')->willReturn($filter);
        $response = $this->case->execute($request);
        $this->assertEquals($filter, $response->getFilter());
        $this->assertTrue($response->isSucceed());
    }
}
