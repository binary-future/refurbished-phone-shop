<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Models;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Models\ToolViewCase;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $filters;

    /**
     * @var ToolViewCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolViewCase($this->queryBus, $this->filters);
    }

    public function testThrowsExceptionIfNotBrand()
    {
        $this->queryBus->method('dispatch')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('brand');
    }

    /**
     * @param Brand $brand
     * @param Collection $models
     * @param Collection $filters
     * @dataProvider validDataProvider
     */
    public function testExecution(Brand $brand, Collection $models, Collection $filters)
    {
        $this->queryBus->method('dispatch')->willReturn(collect([$brand]));
        $this->filters->method('findBy')->willReturn($filters);
        $response = $this->case->execute('brand');
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($models, $response->getModels());
        $this->assertEquals($filters, $response->getFilters());
    }

    public function validDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;
        $models = collect([new PhoneModel()]);
        $brand->setRelation('models', $models);
        $filters = collect([new Filter()]);

        return [
            [$brand, $models, $filters]
        ];
    }
}
