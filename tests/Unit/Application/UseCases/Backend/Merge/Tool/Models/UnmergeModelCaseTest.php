<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Models;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\Merger\Unmerger;
use App\Application\UseCases\Backend\Merge\Tool\Models\UnmergeModelCase;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class UnmergeModelCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $unmerger;

    /**
     * @var UnmergeModelCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->unmerger = $this->getBaseMock(Unmerger::class);
        $this->case = new UnmergeModelCase($this->queryBus, $this->unmerger);
    }

    public function testThrowsExceptionIfNoModel()
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->unmerger->expects($this->never())->method('unmerge');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute(12);
    }

    public function testIfUnmergerThrowsException()
    {
        $model = new PhoneModel();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$model]));
        $this->unmerger->expects($this->once())
            ->method('unmerge')
            ->with($model)
            ->will($this->throwException(new \Exception()));
        $response = $this->case->execute(12);
        $this->assertEquals($response->getModel(), $model);
        $this->assertFalse($response->isSucceed());
    }

    public function testUnmerging()
    {
        $model = new PhoneModel();
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$model]));
        $this->unmerger->expects($this->once())
            ->method('unmerge')
            ->with($model)
            ->willReturn($model);
        $response = $this->case->execute(12);
        $this->assertEquals($response->getModel(), $model);
        $this->assertTrue($response->isSucceed());
    }
}
