<?php


namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Models;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Models\ToolMainCase;
use App\Domain\Phone\Brand\Brand;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $filters;

    /**
     * @var ToolMainCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolMainCase($this->queryBus, $this->filters);
    }

    /**
     * @param Collection $brands
     * @param Collection $filters
     * @dataProvider validDataProvider
     */
    public function testExecution(Collection $brands, Collection $filters)
    {
        $this->queryBus->method('dispatch')->willReturn($brands);
        $this->filters->method('findBy')->willReturn($filters);
        $result = $this->case->execute();
        $this->assertEquals($brands, $result->getBrands());
        $this->assertEquals($filters, $result->getFilters());
    }

    public function validDataProvider()
    {
        return [
            [collect([new Brand()]), collect([])]
        ];
    }
}
