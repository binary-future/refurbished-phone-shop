<?php

namespace Tests\Unit\Application\UseCases\Backend\Merge\Tool\Color;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Color\Filter;
use App\Application\Services\Merge\Color\Repository\Filters;
use App\Application\UseCases\Backend\Merge\Tool\Color\ToolMainCase;
use App\Domain\Phone\Phone\Color;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ToolMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $filters;

    /**
     * @var ToolMainCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->filters = $this->getBaseMock(Filters::class);
        $this->case = new ToolMainCase($this->queryBus, $this->filters);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->filters);
        parent::tearDown();
    }

    public function testExecution()
    {
        $colors = collect([new Color()]);
        $filters = collect([new Filter()]);
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn($colors);
        $this->filters->expects($this->once())->method('findBy')->willReturn($filters);
        $response = $this->case->execute();
        $this->assertEquals($colors, $response->getColors());
        $this->assertEquals($filters, $response->getFilters());
    }
}
