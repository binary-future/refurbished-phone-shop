<?php

namespace Tests\Unit\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodsMainCase;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PaymentMethodsMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase(): PaymentMethodsMainCase
    {
        return new PaymentMethodsMainCase($this->queryBus);
    }

    public function testExecution(): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new PaymentMethod()]));
        $case = $this->getUseCase();

        $response = $case->execute();

        $this->assertInstanceOf(
            PaymentMethod::class,
            $response->getPaymentMethods()->first()
        );
    }
}
