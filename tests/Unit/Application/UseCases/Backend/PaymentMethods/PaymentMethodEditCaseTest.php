<?php

namespace Tests\Unit\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodEditCase;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PaymentMethodEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
        );
        parent::tearDown();
    }


    private function getUseCase()
    {
        return new PaymentMethodEditCase(
            $this->queryBus,
        );
    }

    public function testExecution()
    {
        $paymentMethod = new PaymentMethod();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$paymentMethod])));
        $case = $this->getUseCase();

        $response = $case->execute('slug');

        $this->assertEquals($paymentMethod, $response->getPaymentMethod());
    }

    public function testCaseThrowsExceptionIfStoreNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute('slug');
    }
}
