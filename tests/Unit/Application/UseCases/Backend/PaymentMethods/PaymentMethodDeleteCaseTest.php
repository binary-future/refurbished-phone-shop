<?php

namespace Tests\Unit\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodDeleteCase;
use App\Application\UseCases\Backend\Stores\StoreDeleteCase;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PaymentMethodDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getUseCase()
    {
        return new PaymentMethodDeleteCase($this->queryBus, $this->commandBus);
    }

    public function testExecution()
    {
        $paymentMethod = new PaymentMethod();

        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$paymentMethod]));
        $case = $this->getUseCase();

        $response = $case->execute('slug');

        $this->assertEquals($paymentMethod, $response->getPaymentMethod());
    }

    public function testCaseThrowsExceptionIfStoreNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute('slug');
    }
}
