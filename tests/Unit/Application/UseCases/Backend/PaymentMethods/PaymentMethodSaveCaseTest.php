<?php

namespace Tests\Unit\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodSaveCase;
use App\Application\UseCases\Backend\PaymentMethods\Requests\SavePaymentMethodRequest;
use App\Domain\Shared\Image\Image;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PaymentMethodSaveCaseTest extends TestCase
{
    use BaseMock;

    private $commandBus;

    private $serializer;

    private $uploader;

    private $slugGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->uploader = $this->getBaseMock(Uploader::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
    }

    private function getUseCase()
    {
        return new PaymentMethodSaveCase(
            $this->commandBus,
            $this->serializer,
            $this->uploader,
            $this->slugGenerator
        );
    }

    /**
     * @param SavePaymentMethodRequest $request
     * @dataProvider requestWithoutLogoDataProvider
     */
    public function testExecutionIfLogoAbsent(SavePaymentMethodRequest $request): void
    {
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new PaymentMethod()));
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new PaymentMethod());
        $case = $this->getUseCase();

        $response = $case->execute($request);

        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(PaymentMethod::class, $response->getPaymentMethod());
    }

    public function requestWithoutLogoDataProvider()
    {
        $request = new SavePaymentMethodRequest();
        $request->setName('name');

        return [
            [$request]
        ];
    }

    /**
     * @param SavePaymentMethodRequest $request
     * @dataProvider requestWithLogoDataProvider
     */
    public function testExecutionWithLogo(SavePaymentMethodRequest $request)
    {
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->will($this->onConsecutiveCalls(new PaymentMethod()));
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new PaymentMethod());
        $case = $this->getUseCase();

        $response = $case->execute($request);

        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(PaymentMethod::class, $response->getPaymentMethod());
    }

    public function requestWithLogoDataProvider()
    {
        $request = new SavePaymentMethodRequest();
        $request->setName('name');
        $request->setLogo($this->getBaseMock(UploadedFile::class));

        return [
            [$request]
        ];
    }

    public function testExecutionReturnsFailedResponseIfWrongData()
    {
        $case = $this->getUseCase();
        $response = $case->execute(new SavePaymentMethodRequest());
        $this->assertFalse($response->isSuccess());
    }
}
