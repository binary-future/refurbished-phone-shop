<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\UseCases\Backend\Reports\NegativeDeleteCase;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class NegativeDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $negativeReports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->negativeReports = $this->getBaseMock(NegativeReports::class);
    }

    private function getCase()
    {
        return new NegativeDeleteCase($this->queryBus, $this->negativeReports);
    }

    public function testThrowsExceptionIfStoreAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 'type');
    }

    public function testExecution()
    {
        $reports = collect([new NegativeReport()]);
        $store = new Store();
        $store->id = 1;
        $type = 'type';
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->negativeReports->expects($this->any())
            ->method('findBy')
            ->willReturn($reports);
        $this->negativeReports->method('deleteSeveral')
            ->willReturn(true);
        $case = $this->getCase();
        $response = $case->execute('slug', $type);
        $this->assertInstanceOf(Store::class, $response->getStore());
        $this->assertTrue($response->isDeleted());
        $this->assertEquals($type, $response->getType());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->negativeReports);
        parent::tearDown();
    }
}
