<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\UseCases\Backend\Reports\NegativeViewCase;
use App\Domain\Store\Store;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class NegativeViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $negativeReports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->negativeReports = $this->getBaseMock(NegativeReports::class);
    }

    private function getCase()
    {
        return new NegativeViewCase($this->queryBus, $this->negativeReports);
    }

    public function testThrowsExceptionIfStoreAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 'type');
    }

    public function testExecution()
    {
        $reports = $this->getBaseMock(LengthAwarePaginator::class);
        $store = new Store();
        $store->id = 1;
        $type = 'type';
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->negativeReports->expects($this->once())
            ->method('findPaginated')
            ->willReturn($reports);
        $case = $this->getCase();
        $response = $case->execute('slug', $type);
        $this->assertInstanceOf(Store::class, $response->getStore());
        $this->assertEquals($store->getKey(), $response->getStore()->getKey());
        $this->assertEquals($reports, $response->getReports());
        $this->assertEquals($type, $response->getType());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->negativeReports);
        parent::tearDown();
    }
}
