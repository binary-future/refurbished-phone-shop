<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\UseCases\Backend\Reports\ByStoreDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\PositiveDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\NegativeDeleteCase;
use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;
use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ByStoreDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $positiveReportDeleteCase;

    private $negativeReportDeleteCase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->positiveReportDeleteCase = $this->getBaseMock(PositiveDeleteCase::class);
        $this->negativeReportDeleteCase = $this->getBaseMock(NegativeDeleteCase::class);
    }

    private function getCase()
    {
        return new ByStoreDeleteCase($this->positiveReportDeleteCase, $this->negativeReportDeleteCase);
    }

    public function testWhenPositiveError()
    {
        $positiveReponse = new PositiveDeleteResponse(new Store(), 'type', false);
        $negativeResponse = new NegativeDeleteResponse(new Store(), 'type', true);
        $this->positiveReportDeleteCase->expects($this->once())
            ->method('execute')
            ->willReturn($positiveReponse);
        $this->negativeReportDeleteCase->expects($this->once())
            ->method('execute')
            ->willReturn($negativeResponse);
        $case = $this->getCase();
        $response = $case->execute('slug', 'type');
        $this->assertFalse($response->isDeleted());
    }

    public function testWhenNegativeError()
    {
        $positiveReponse = new PositiveDeleteResponse(new Store(), 'type', true);
        $negativeResponse = new NegativeDeleteResponse(new Store(), 'type', false);
        $this->positiveReportDeleteCase->expects($this->once())
            ->method('execute')
            ->willReturn($positiveReponse);
        $this->negativeReportDeleteCase->expects($this->once())
            ->method('execute')
            ->willReturn($negativeResponse);
        $case = $this->getCase();
        $response = $case->execute('slug', 'type');
        $this->assertFalse($response->isDeleted());
    }

    public function testSucceedDeleting()
    {
        $positiveReponse = new PositiveDeleteResponse(new Store(), 'type', true);
        $negativeResponse = new NegativeDeleteResponse(new Store(), 'type', true);
        $this->positiveReportDeleteCase->expects($this->once())
            ->method('execute')
            ->willReturn($positiveReponse);
        $this->negativeReportDeleteCase->expects($this->once())
            ->method('execute')
            ->willReturn($negativeResponse);
        $case = $this->getCase();
        $response = $case->execute('slug', 'type');
        $this->assertTrue($response->isDeleted());
    }

    protected function tearDown(): void
    {
        unset($this->positiveReportDeleteCase, $this->negativeReportDeleteCase);
        parent::tearDown();
    }
}
