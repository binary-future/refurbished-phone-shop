<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\UseCases\Backend\Reports\SingleNegativeDeleteCase;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class SingleNegativeDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $negativeReports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->negativeReports = $this->getBaseMock(NegativeReports::class);
    }

    private function getCase()
    {
        return new SingleNegativeDeleteCase($this->queryBus, $this->negativeReports);
    }

    public function testThrowsExceptionIfStoreAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 1);
    }

    public function testThrowsExceptionIfReportAbsent()
    {
        $store = new Store();
        $store->id = 1;
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->negativeReports->expects($this->any())
            ->method('findBy')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 1);
    }

    public function testExecution()
    {
        $type = 'type';
        $report = new NegativeReport();
        $report->setTargetType($type);
        $reports = collect([$report]);
        $store = new Store();
        $store->id = 1;
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->negativeReports->expects($this->any())
            ->method('findBy')
            ->willReturn($reports);
        $this->negativeReports->method('delete')
            ->willReturn(true);

        $case = $this->getCase();
        $response = $case->execute('siug', 1);

        $this->assertEquals($store, $response->getStore());
        $this->assertEquals($type, $response->getType());
        $this->assertTrue($response->isDeleted());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->negativeReports);
        parent::tearDown();
    }

}
