<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\AllReportsCase;
use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class AllReportsCaseTest extends TestCase
{
    use BaseMock;

    private $positiveReports;

    private $negativeReports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->positiveReports = $this->getBaseMock(PositiveReports::class);
        $this->negativeReports = $this->getBaseMock(NegativeReports::class);
    }

    private function getCase()
    {
        return new AllReportsCase($this->positiveReports, $this->negativeReports);
    }

    public function testExecution()
    {
        $positiveReports = collect();
        $negativeReports = collect();
        $result = new AllReportsResponse($positiveReports, $negativeReports);
        $this->positiveReports->expects($this->once())
            ->method('findBy')
            ->willReturn($positiveReports);
        $this->negativeReports->expects($this->once())
            ->method('findBy')
            ->willReturn($negativeReports);
        $case = $this->getCase();
        $response = $case->execute('type');

        $this->assertEquals($result, $response);
    }

    protected function tearDown(): void
    {
        unset($this->positiveReports, $this->negativeReports);
        parent::tearDown();
    }
}
