<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\SinglePositiveDeleteCase;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class SinglePositiveDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $reports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->reports = $this->getBaseMock(PositiveReports::class);
    }

    private function getCase()
    {
        return new SinglePositiveDeleteCase($this->queryBus, $this->reports);
    }

    public function testThrowsExceptionIfStoreAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 1);
    }

    public function testThrowsExceptionIfReportAbsent()
    {
        $store = new Store();
        $store->id = 1;
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->reports->expects($this->any())
            ->method('findBy')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 1);
    }

    public function testExecution()
    {
        $type = 'type';
        $report = new PositiveReport();
        $report->setTargetType($type);
        $reports = collect([$report]);
        $store = new Store();
        $store->id = 1;
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->reports->expects($this->any())
            ->method('findBy')
            ->willReturn($reports);
        $this->reports->method('delete')
            ->willReturn(true);
        $case = $this->getCase();
        $response = $case->execute('siug', 1);
        $this->assertEquals($store, $response->getStore());
        $this->assertEquals($type, $response->getType());
        $this->assertTrue($response->isDeleted());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->reports);
        parent::tearDown();
    }
}
