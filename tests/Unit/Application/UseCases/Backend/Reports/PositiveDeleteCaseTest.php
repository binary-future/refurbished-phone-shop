<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\PositiveDeleteCase;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PositiveDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $reports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->reports = $this->getBaseMock(PositiveReports::class);
    }

    private function getCase()
    {
        return new PositiveDeleteCase($this->queryBus, $this->reports);
    }

    public function testThrowsExceptionIfStoreAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 'type');
    }

    public function testExecution()
    {
        $reports = collect([new PositiveReport()]);
        $store = new Store();
        $store->id = 1;
        $type = 'type';
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->reports->expects($this->any())
            ->method('findBy')
            ->willReturn($reports);
        $this->reports->method('deleteSeveral')
            ->willReturn(true);
        $case = $this->getCase();
        $response = $case->execute('slug', $type);
        $this->assertInstanceOf(Store::class, $response->getStore());
        $this->assertTrue($response->isDeleted());
        $this->assertEquals($type, $response->getType());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->reports);
        parent::tearDown();
    }
}
