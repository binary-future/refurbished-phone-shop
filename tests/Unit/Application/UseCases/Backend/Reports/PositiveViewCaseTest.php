<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\PositiveViewCase;
use App\Domain\Store\Store;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PositiveViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $reports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->reports = $this->getBaseMock(PositiveReports::class);
    }
    private function getCase()
    {
        return new PositiveViewCase($this->queryBus, $this->reports);
    }

    public function testThrowsExceptionIfStoreAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('siug', 'type');
    }

    public function testExecution()
    {
        $reports = $this->getBaseMock(LengthAwarePaginator::class);
        $store = new Store();
        $store->id = 1;
        $type = 'type';
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$store]));
        $this->reports->expects($this->once())
            ->method('findPaginated')
            ->willReturn($reports);
        $case = $this->getCase();
        $response = $case->execute('siug', $type);
        $this->assertInstanceOf(Store::class, $response->getStore());
        $this->assertEquals($store->getKey(), $response->getStore()->getKey());
        $this->assertEquals($reports, $response->getReports());
        $this->assertEquals($type, $response->getType());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->reports);
        parent::tearDown();
    }
}
