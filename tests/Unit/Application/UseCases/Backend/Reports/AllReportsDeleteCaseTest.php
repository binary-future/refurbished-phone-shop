<?php


namespace Tests\Unit\Application\UseCases\Backend\Reports;


use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\AllReportsDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\AllReportsCase;
use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class AllReportsDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $allReportsCase;

    private $negativeReports;

    private $positiveReports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->allReportsCase = $this->getBaseMock(AllReportsCase::class);
        $this->negativeReports = $this->getBaseMock(NegativeReports::class);
        $this->positiveReports = $this->getBaseMock(PositiveReports::class);
    }

    private function getCase()
    {
        return new AllReportsDeleteCase($this->allReportsCase, $this->negativeReports, $this->positiveReports);
    }

    public function testNegativeReportsDeleteError()
    {
        $allReports = new AllReportsResponse(collect(), collect());
        $this->negativeReports->method('deleteSeveral')
            ->will($this->throwException(new \Exception()));
        $this->positiveReports->method('deleteSeveral')
            ->willReturn(true);
        $this->allReportsCase->method('execute')
            ->willReturn($allReports);
        $case = $this->getCase();
        $response = $case->execute('type');
        $this->assertFalse($response->isDeleted());
    }

    public function testPositiveReportsDeleteError()
    {
        $allReports = new AllReportsResponse(collect(), collect());
        $this->negativeReports->method('deleteSeveral')
            ->willReturn(true);
        $this->positiveReports->method('deleteSeveral')
            ->will($this->throwException(new \Exception()));
        $this->allReportsCase->method('execute')
            ->willReturn($allReports);
        $case = $this->getCase();
        $response = $case->execute('type');
        $this->assertFalse($response->isDeleted());
    }

    public function testSucceedDeleting()
    {
        $allReports = new AllReportsResponse(collect(), collect());
        $this->negativeReports->method('deleteSeveral')
            ->willReturn(true);
        $this->positiveReports->method('deleteSeveral')
            ->willReturn(true);
        $this->allReportsCase->method('execute')
            ->willReturn($allReports);
        $case = $this->getCase();
        $response = $case->execute('type');
        $this->assertTrue($response->isDeleted());
    }

    protected function tearDown(): void
    {
        unset($this->allReportsCase, $this->negativeReports, $this->positiveReports);
        parent::tearDown();
    }
}
