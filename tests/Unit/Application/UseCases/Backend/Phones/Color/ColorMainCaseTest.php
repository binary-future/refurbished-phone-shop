<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Color;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\ColorMainCase;
use App\Domain\Phone\Phone\Color;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ColorMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getCase()
    {
        return new ColorMainCase($this->queryBus);
    }

    public function testExecution()
    {
        $colors = collect([new Color()]);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls($colors, $colors));
        $case = $this->getCase();
        $result = $case->execute();
        $this->assertEquals($colors, $result->getColors());
        $this->assertEquals($colors, $result->getInactiveColors());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
