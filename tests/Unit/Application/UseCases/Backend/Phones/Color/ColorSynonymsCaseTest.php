<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Color;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\ColorSynonymsCase;
use App\Domain\Phone\Phone\Color;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ColorSynonymsCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var ColorSynonymsCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new ColorSynonymsCase($this->queryBus);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->queryBus, $this->case);
    }

    public function testThrowsExceptionIfNoColor()
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('slug');
    }

    public function testThrowsExceptionIfColorIsSynonym()
    {
        $color = new Color();
        $color->setParentId(1);
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect([$color]));
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('slug');
    }

    /**
     * @param Color $color
     * @param Collection $synonyms
     * @param Collection $potentialSynonyms
     * @dataProvider validDataProvider
     */
    public function testExecution(Color $color, Collection $synonyms, Collection $potentialSynonyms)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$color]), $potentialSynonyms));
        $response = $this->case->execute('slug');
        $this->assertEquals($response->getColor(), $color);
        $this->assertEquals($response->getSynonyms(), $synonyms);
        $this->assertEquals($response->getPotentialSynonyms(), $potentialSynonyms);
    }

    public function validDataProvider()
    {
        $color = new Color();
        $color->id = 10;
        $color->setName('color');
        $synonym = new Color();
        $color->id = 1;
        $synonyms = collect($synonym);
        $color->setRelation('synonyms', $synonyms);
        $potentialSynonyms = collect([new Color()]);

        return [
            [$color, $synonyms, $potentialSynonyms]
        ];
    }
}
