<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Color;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\ColorUpdateCase;
use App\Application\UseCases\Backend\Phones\Color\Requests\ColorUpdateRequest;
use App\Domain\Phone\Phone\Color;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ColorUpdateCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $slugGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
    }

    private function getCase()
    {
        return new ColorUpdateCase($this->queryBus, $this->commandBus, $this->slugGenerator);
    }

    /**
     * @dataProvider validRequestDataProvider
     * @param string $slug
     * @param ColorUpdateRequest $request
     */
    public function testThrowsExceptionIfColorAbsent(string $slug, ColorUpdateRequest $request)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());

        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->slugGenerator->expects($this->never())
            ->method('generate');
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute($slug, $request);
    }

    /**
     * @param string $slug
     * @param ColorUpdateRequest $request
     * @dataProvider validRequestDataProvider
     */
    public function testReturnFailedResponseIfErrorWasOccurred(string $slug, ColorUpdateRequest $request)
    {
        $color = new Color();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$color]));
        $this->slugGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('slug');
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->will($this->throwException(new \Exception()));
        $case = $this->getCase();
        $result = $case->execute($slug, $request);
        $this->assertFalse($result->isSuccess());
        $this->assertEquals($color, $result->getColor());
    }

    /**
     * @param string $slug
     * @param ColorUpdateRequest $request
     * @dataProvider validRequestDataProvider
     */
    public function testExecution(string $slug, ColorUpdateRequest $request)
    {
        $color = new Color();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$color]));
        $this->slugGenerator->expects($this->once())
            ->method('generate')
            ->willReturn('slug');
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($color);
        $case = $this->getCase();
        $result = $case->execute($slug, $request);
        $this->assertTrue($result->isSuccess());
        $this->assertEquals($color, $result->getColor());
    }

    public function validRequestDataProvider()
    {
        $request = new ColorUpdateRequest();
        $request->setName('name');
        $request->setHex('hex');

        return [
            ['slug', $request]
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus, $this->slugGenerator);
        parent::tearDown();
    }
}
