<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Color;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\ColorEditCase;
use App\Domain\Phone\Phone\Color;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ColorEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getCase()
    {
        return new ColorEditCase($this->queryBus);
    }

    public function testExecution()
    {
        $color = new Color();
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$color]));
        $case = $this->getCase();
        $result = $case->execute('slug');
        $this->assertEquals($color, $result->getColor());
    }

    public function testThrowsExceptionIfColorAbsent()
    {
        $this->queryBus->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('slug');
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
