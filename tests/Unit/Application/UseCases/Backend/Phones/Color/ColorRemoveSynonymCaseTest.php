<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Color;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\ColorRemoveSynonymCase;
use App\Domain\Phone\Phone\Color;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ColorRemoveSynonymCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    /**
     * @var ColorRemoveSynonymCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->case = new ColorRemoveSynonymCase($this->queryBus, $this->commandBus);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus, $this->case);
        parent::tearDown();
    }

    public function testThrowsExceptionIfNoColor()
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->commandBus->expects($this->never())->method('dispatch');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('slug', 12);
    }

    public function testThrowsExceptionIfNoSynonym()
    {
        $color = new Color();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$color]), collect()));
        $this->commandBus->expects($this->never())->method('dispatch');
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('slug', 12);
    }

    /**
     * @param Color $color
     * @param Color $synonym
     * @dataProvider invalidDataProvider
     */
    public function testFailedResponseIfInvalidSynonymOrColor(Color $color, Color $synonym)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$color]), collect([$synonym])));
        $this->commandBus->expects($this->never())->method('dispatch');
        $response = $this->case->execute('slug', 1);
        $this->assertEquals($color, $response->getColor());
        $this->assertFalse($response->isSucceed());
    }

    public function invalidDataProvider()
    {
        $colorFirst = new Color();
        $colorFirst->id = 10;
        $synonymFirst = new Color();
        $synonymFirst->setParentId(9);

        return [
            [$colorFirst, $synonymFirst],
        ];
    }

    /**
     * @param Color $color
     * @param Color $synonym
     * @dataProvider validDataProvider
     */
    public function testFailedResponseIfCommandBusThrowsException(Color $color, Color $synonym)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$color]), collect([$synonym])));
        $this->commandBus->expects($this->once())->method('dispatch')->will($this->throwException(new \Exception()));
        $response = $this->case->execute('slug', 1);
        $this->assertEquals($color, $response->getColor());
        $this->assertFalse($response->isSucceed());
    }

    /**
     * @param Color $color
     * @param Color $synonym
     * @dataProvider validDataProvider
     */
    public function testExecution(Color $color, Color $synonym)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$color]), collect([$synonym])));
        $this->commandBus->expects($this->once())->method('dispatch')->willReturn($color);
        $response = $this->case->execute('slug', 1);
        $this->assertEquals($color, $response->getColor());
        $this->assertTrue($response->isSucceed());
    }

    public function validDataProvider()
    {
        $color = new Color();
        $color->id = 1;
        $synonym = new Color();
        $synonym->setParentId(1);

        return [
            [$color, $synonym]
        ];
    }
}
