<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Phone;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\UseCases\Backend\Phones\Phone\PhoneGalleryAddImageCase;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhoneGalleryAddImageCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $uploader;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->uploader = $this->getBaseMock(Uploader::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getCase()
    {
        return new PhoneGalleryAddImageCase($this->queryBus, $this->uploader, $this->commandBus);
    }

    public function testThrowsExceptionIfPhoneNotFound()
    {
        $this->queryBus->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute($this->getBaseMock(UploadedFile::class), 1);
    }

    public function testReturnsFailedResponseIfUploaderFailed()
    {
        $phone = new Phone();
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$phone]));
        $this->uploader->method('storeImage')
            ->will($this->throwException(new \Exception()));
        $case = $this->getCase();
        $response = $case->execute($this->getBaseMock(UploadedFile::class), 1);
        $this->assertFalse($response->isSuccess());
    }

    public function testExecution()
    {
        $phone = new Phone();
        $phone->id = 1;
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$phone]));
        $this->uploader->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->method('dispatch')
            ->willReturn($phone);
        $case = $this->getCase();
        $response = $case->execute($this->getBaseMock(UploadedFile::class), 1);
        $this->assertTrue($response->isSuccess());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->uploader, $this->commandBus);
        parent::tearDown();
    }
}
