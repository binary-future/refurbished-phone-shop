<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Phone;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Phone\PhoneGalleryDeleteImageCase;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhoneGalleryDeleteImageCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
    }

    private function getCase()
    {
        return new PhoneGalleryDeleteImageCase($this->queryBus, $this->commandBus);
    }

    public function testThrowsExceptionIfNoPhone()
    {
        $this->queryBus->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute(1, 2);
    }

    public function testTrowsExceptionIfNoImage()
    {
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getImages')
            ->willReturn(collect());
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$phone]));
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute(1, 2);
    }

    public function testFailedResponseIfError()
    {
        $id = 1;
        $image = new Image();
        $image->id = $id;
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getImages')
            ->willReturn(collect([$image]));
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$phone]));
        $this->commandBus->method('dispatch')
            ->will($this->throwException(new \Exception()));
        $case = $this->getCase();
        $response = $case->execute($id, $id);
        $this->assertFalse($response->isSuccess());
    }

    public function testExecution()
    {
        $id = 1;
        $image = new Image();
        $image->id = $id;
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getImages')
            ->willReturn(collect([$image]));
        $this->queryBus->method('dispatch')
            ->willReturn(collect([$phone]));
        $this->commandBus->method('dispatch')
            ->willReturn($phone);
        $case = $this->getCase();
        $response = $case->execute($id, $id);
        $this->assertTrue($response->isSuccess());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus);
        parent::tearDown();
    }
}
