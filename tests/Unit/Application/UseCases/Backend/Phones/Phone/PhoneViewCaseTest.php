<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Phone;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Phone\PhoneViewCase;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class PhoneViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getCase()
    {
        return new PhoneViewCase($this->queryBus);
    }

    public function testThrowsExceptionIfNoPhone()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute(1);
    }

    public function testExecution()
    {
        $brand = new Brand();
        $model = $this->getBaseMock(PhoneModel::class);
        $model->method('getBrand')->willReturn($brand);
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getModel')->willReturn($model);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$phone]));
        $case = $this->getCase();
        $response = $case->execute(1);
        $this->assertEquals($phone, $response->getPhone());
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($model, $response->getModel());
    }
}
