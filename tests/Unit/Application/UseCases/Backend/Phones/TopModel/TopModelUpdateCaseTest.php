<?php

namespace Tests\Unit\Application\UseCases\Backend\Phones\TopModel;

use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelUpdateRequest;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelUpdateCase;
use PHPUnit\Framework\MockObject\Rule\InvokedCount;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class TopModelUpdateCaseTest extends TestCase
{
    use BaseMock;

    private $repository;
    private $orderer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(TopModels::class);
        $this->orderer = $this->getBaseMock(Orderer::class);
    }

    protected function tearDown(): void
    {
        unset($this->repository, $this->orderer);
        parent::tearDown();
    }

    private function getCase()
    {
        return new TopModelUpdateCase($this->repository, $this->orderer);
    }

    /**
     * @dataProvider executionDataProvider
     * @param int $oldOrder
     * @param int $newOrder
     * @param InvokedCount $orderBetweenInvokedCount
     * @param InvokedCount $saveInvokedCount
     */
    public function testExecution(
        int $oldOrder,
        int $newOrder,
        InvokedCount $orderBetweenInvokedCount,
        InvokedCount $saveInvokedCount
    ) {
        // prepare top model
        $topModel = $this->getTopModel($oldOrder);
        $topModelsCount = abs($oldOrder - $newOrder);
        $topModels = collect(array_fill(0, $topModelsCount, $topModel));
        // prepare request
        $request = $this->getUpdateRequest($newOrder, $topModel);
        // expectations
        $this->orderer->expects($orderBetweenInvokedCount)
            ->method('orderBetween')
            ->willReturn($topModels);
        $this->repository->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$topModel]));
        $this->repository->expects($saveInvokedCount)
            ->method('save')
            ->willReturn($topModel);
        $case = $this->getCase();

        $response = $case->execute($request);

        $this->assertEquals($topModel, $response->getTopModel());
    }

    public function executionDataProvider()
    {
        return [
            [1, 2, $this->exactly(1), $this->exactly(2)],
            [4, 1, $this->exactly(1), $this->exactly(4)],
            [1, 1, $this->never(), $this->never()],
        ];
    }

    /**
     * @param int $newOrder
     * @param TopModel $topModel
     * @return TopModelUpdateRequest
     */
    private function getUpdateRequest(int $newOrder, TopModel $topModel): TopModelUpdateRequest
    {
        $request = new TopModelUpdateRequest();
        $request->setTopModelId(1);
        $request->setOrder($newOrder);
        return $request;
    }

    /**
     * @param int $oldOrder
     * @return TopModel
     */
    private function getTopModel(int $oldOrder): TopModel
    {
        $topModel = new TopModel();
        $topModel->setOrder($oldOrder);
        return $topModel;
    }
}
