<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\TopModel;


use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelCreateRequest;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelCreateCase;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class TopModelCreateCaseTest extends TestCase
{
    use BaseMock;

    private $repository;
    private $orderer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(TopModels::class);
        $this->orderer = $this->getBaseMock(Orderer::class);
    }

    private function getCase()
    {
        return new TopModelCreateCase($this->repository, $this->orderer);
    }

    public function testExecution()
    {
        // prepare top model
        $phoneModel = new PhoneModel();
        $phoneModel->setAttribute('id', 1); // force setting id
        $topModel = $this->getBaseMock(TopModel::class);
        // prepare relation
        $relation = $this->getBaseMock(BelongsTo::class);
        // prepare request
        $request = new TopModelCreateRequest();
        $request->setModelId($phoneModel->getKey());
        $request->setOrder(1);
        // expectations
        $this->repository->expects($this->once())
            ->method('create')
            ->willReturn($topModel);
        $this->repository->expects($this->once())
            ->method('save')
            ->willReturn($topModel);
        $this->orderer->expects($this->once())
            ->method('addedTo')
            ->willReturn(collect([$topModel]));
        $case = $this->getCase();

        $response = $case->execute($request);

        $this->assertEquals($topModel, $response->getTopModel());
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }
}
