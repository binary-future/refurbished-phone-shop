<?php

namespace Tests\Unit\Application\UseCases\Backend\Phones\TopModel;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelMainCase;
use App\Domain\Phone\Model\PhoneModel;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class TopModelMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->repository = $this->getBaseMock(TopModels::class);
    }

    private function getCase()
    {
        return new TopModelMainCase($this->repository, $this->queryBus);
    }

    public function testExecution()
    {
        $phoneModels = collect([new PhoneModel()]);
        $topModel = $this->getBaseMock(TopModel::class);
        $topModel->expects($this->once())
            ->method('getModel')
            ->willReturn(new PhoneModel());
        $topModels = collect([$topModel]);

        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($phoneModels);
        $this->repository->expects($this->once())
            ->method('findBy')
            ->willReturn($topModels);
        $case = $this->getCase();

        $response = $case->execute();

        $this->assertEquals($phoneModels, $response->getModels());
        $this->assertEquals($topModels, $response->getTopModels());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->repository);
        parent::tearDown();
    }
}
