<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\TopModel;


use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelDeleteRequest;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelDeleteCase;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class TopModelDeleteCaseTest extends TestCase
{
    use BaseMock;

    private $repository;
    private $orderer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getBaseMock(TopModels::class);
        $this->orderer = $this->getBaseMock(Orderer::class);
    }

    private function getCase()
    {
        return new TopModelDeleteCase($this->repository, $this->orderer);
    }

    public function testExecution()
    {
        $topModels = collect([new TopModel]);
        // prepare request
        $request = new TopModelDeleteRequest(1);
        // expectations
        $this->orderer->expects($this->once())
            ->method('deletedFrom')
            ->willReturn($topModels);
        $this->repository->expects($this->once())
            ->method('findBy')
            ->willReturn($topModels);
        $this->repository->expects($this->once())
            ->method('save');
        $this->repository->expects($this->once())
            ->method('delete')
            ->willReturn(true);
        $case = $this->getCase();

        $response = $case->execute($request);

        $this->assertTrue($response->isSuccess());
    }

    protected function tearDown(): void
    {
        unset($this->repository);
        parent::tearDown();
    }
}
