<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Brand;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\Services\Transformers\DescriptionTransformer;
use App\Application\UseCases\Backend\Phones\Brand\BrandUpdateCase;
use App\Application\UseCases\Backend\Phones\Brand\Requests\UpdateBrandRequest;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BrandUpdateCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $uploader;

    private $commandBus;

    private $slugGenerator;

    private $descriptionTransformer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->uploader = $this->getBaseMock(Uploader::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->descriptionTransformer = $this->getBaseMock(DescriptionTransformer::class);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->uploader, $this->commandBus, $this->slugGenerator, $this->descriptionTransformer);
        parent::tearDown();
    }

    protected function getCase()
    {
        return new BrandUpdateCase(
            $this->queryBus,
            $this->uploader,
            $this->commandBus,
            $this->slugGenerator,
            $this->descriptionTransformer
        );
    }

    /**
     * @param UpdateBrandRequest $request
     * @param Brand $brand
     * @dataProvider withoutLogoDataProvider
     */
    public function testExecutionIfLogoAbsent(UpdateBrandRequest $request, Brand $brand)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$brand]));
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->descriptionTransformer->expects($this->once())
            ->method('transform')
            ->willReturn(new Description());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($brand);
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals($brand, $response->getBrand());
    }

    public function withoutLogoDataProvider()
    {
        $brand = new Brand();
        $request = $this->generateBrandUpdateRequest();

        return [
            [$request, $brand]
        ];
    }

    private function generateBrandUpdateRequest($params = []): UpdateBrandRequest
    {
        if (empty($params)) {
            return new UpdateBrandRequest('slug', 'name', null, []);
        }

        return UpdateBrandRequest::fromArray($params);
    }

    public function testCaseThrowsExceptionIfNoBrand()
    {
        $request = $this->generateBrandUpdateRequest();
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $this->slugGenerator->expects($this->never())
            ->method('generate');
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $this->descriptionTransformer->expects($this->never())
            ->method('transform');
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute($request);
    }

    /**
     * @param UpdateBrandRequest $request
     * @param Brand $brand
     * @dataProvider withLogoDataProvider
     */
    public function testExecutionWithLogo(UpdateBrandRequest $request, Brand $brand)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$brand]));
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($brand);
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertTrue($response->isSuccess());
        $this->assertEquals($brand, $response->getBrand());
    }

    public function withLogoDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;
        $uploadedFile = $this->getBaseMock(UploadedFile::class);
        $request = $this->generateBrandUpdateRequest([
            'slug' => 'slug',
            'name' => 'name',
            'logo' => $uploadedFile,
            'description' => []
        ]);

        return [
            [$request, $brand]
        ];
    }
}
