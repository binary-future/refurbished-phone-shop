<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Brand;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Brand\BrandEditCase;
use App\Domain\Phone\Brand\Brand;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BrandEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new BrandEditCase($this->queryBus);
    }

    public function testCaseThrowsExceptionIfBrandNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('Slug');
    }

    /**
     * @param $brands
     * @param $models
     */
    public function testCaseReturnsResponse()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Brand()]));
        $case = $this->getUseCase();
        $response = $case->execute('slug');
        $this->assertInstanceOf(Brand::class, $response->getBrand());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
