<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Brand;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Brand\BrandMainCase;
use App\Domain\Phone\Brand\Brand;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BrandMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new BrandMainCase($this->queryBus);
    }

    public function testExecution()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Brand()]));
        $case = $this->getUseCase();
        $response = $case->execute();
        $this->assertInstanceOf(Brand::class, $response->getBrands()->first());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
