<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\OldPhonesListCase;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class OldPhonesListCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var OldPhonesListCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new OldPhonesListCase($this->queryBus);
    }

    /**
     * @param Collection $models
     * @dataProvider executionDataProvider
     */
    public function testExecution(Collection $models)
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn($models);
        $response = $this->case->execute();
        $this->assertEquals($models, $response->getModels());
    }

    public function executionDataProvider()
    {
        return [
            [collect()],
            [collect([new PhoneModel()])]
        ];
    }
}
