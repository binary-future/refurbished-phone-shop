<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\ModelMainCase;
use App\Domain\Phone\Brand\Brand;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getCase()
    {
        return new ModelMainCase($this->queryBus);
    }

    public function testExecution()
    {
        $brands = collect([new Brand()]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($brands);
        $case = $this->getCase();
        $response = $case->execute();
        $this->assertEquals($brands, $response->getBrands());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
