<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\ModelListCase;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelsResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelListCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $latestModels;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->latestModels = $this->getBaseMock(LatestModels::class);
    }

    private function getCase()
    {
        return new ModelListCase($this->queryBus, $this->latestModels);
    }

    public function testThrowsExceptionIfBrandAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('brand');
    }

    /**
     * @dataProvider executionDataProvider
     * @param Brand $brand
     * @param Collection $models
     * @param LatestModel|null $latestModel
     * @param Collection $expectedModels
     */
    public function testExecution(
        Brand $brand,
        Collection $models,
        ?LatestModel $latestModel,
        Collection $expectedModels
    ) {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), $models));
        $this->latestModels->expects($this->once())
            ->method('findBy')
            ->willReturn($latestModel ? collect([$latestModel]) : collect());

        $case = $this->getCase();
        $response = $case->execute('brand');
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($expectedModels, $response->getModels());
    }

    public function executionDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;

        $phoneModel = new PhoneModel();
        $phoneModel->id = 1;
        $models = collect([$phoneModel]);

        $latestModel = $this->getBaseMock(LatestModel::class);
        $latestModel->expects($this->any())
            ->method('getModel')
            ->willReturn($phoneModel);

        $expectedModels1 = collect([new ModelsResponse($phoneModel, $latestModel)]);
        $expectedModels2 = collect([new ModelsResponse($phoneModel, null)]);

        return [
            [$brand, $models, $latestModel, $expectedModels1],
            [$brand, $models, null, $expectedModels2],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->latestModels);
        parent::tearDown();
    }
}
