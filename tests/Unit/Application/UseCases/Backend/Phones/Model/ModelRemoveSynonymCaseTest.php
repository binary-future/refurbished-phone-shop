<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\ModelRemoveSynonymCase;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelRemoveSynonymRequest;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelRemoveSynonymCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var
     */
    private $queryBus;

    /**
     * @var
     */
    private $commandBus;

    /**
     * @var ModelRemoveSynonymCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->case = new ModelRemoveSynonymCase($this->queryBus, $this->commandBus);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }



    public function testCaseThrowsExceptionIfNoBrand()
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($this->getSynonymRequest());
    }

    public function testCaseThrowsExceptionIfNoModel()
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())->method('dispatch')->willReturn(collect([$brand]), collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($this->getSynonymRequest());
    }


    public function testCaseThrowsExceptionIfNoSynonym()
    {
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $model->id = 1;
        $this->queryBus->expects($this->any())->method('dispatch')->willReturn(collect([$brand]), collect([$model]), collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute($this->getSynonymRequest());
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @param PhoneModel $synonymModel
     * @dataProvider succeedScenarioDataProvider
     */
    public function testSucceedExecution(
        Brand $brand,
        PhoneModel $model,
        PhoneModel $synonymModel
    ) {
        $this->queryBus->expects($this->any())
            ->method('dispatch')->willReturn(collect([$brand]), collect([$model]), collect([$synonymModel]));
        $this->commandBus->method('dispatch')->willReturn($synonymModel);
        $response = $this->case->execute($this->getSynonymRequest());
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($model, $response->getModel());
        $this->assertTrue($response->isSucceed());
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @param PhoneModel $synonymModel
     * @dataProvider succeedScenarioDataProvider
     */
    public function testIfCommandBusThrowsException(
        Brand $brand,
        PhoneModel $model,
        PhoneModel $synonymModel
    ) {
        $this->queryBus->expects($this->any())
            ->method('dispatch')->willReturn(collect([$brand]), collect([$model]), collect([$synonymModel]));
        $this->commandBus->method('dispatch')->will($this->throwException(new \Exception()));
        $response = $this->case->execute($this->getSynonymRequest());
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($model, $response->getModel());
        $this->assertFalse($response->isSucceed());
    }


    public function succeedScenarioDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $model->id = 1;
        $synonym = new PhoneModel();
        $synonym->parent_id = null;
        $synonym->setRelation('originalPhones', collect());

        return [
            [$brand, $model, $synonym]
        ];
    }

    private function getSynonymRequest(): ModelRemoveSynonymRequest
    {
        return new ModelRemoveSynonymRequest('brand', 'model', 50);
    }
}
