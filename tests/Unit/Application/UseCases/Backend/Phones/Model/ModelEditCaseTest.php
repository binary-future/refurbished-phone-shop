<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\ModelEditCase;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class ModelEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $latestModels;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->latestModels = $this->getBaseMock(LatestModels::class);
    }

    private function getCase()
    {
        return new ModelEditCase($this->queryBus, $this->latestModels);
    }

    public function testThrowsExceptionIfBrandAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('brand', 'model');
    }

    public function testThrowsExceptionIfModelAbsent()
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect()));
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('brand', 'model');
    }

    /**
     * @dataProvider executionDataProvider
     * @param Brand $brand
     * @param PhoneModel $model
     * @param LatestModel $latestModel
     */
    public function testExecution(Brand $brand, PhoneModel $model, ?LatestModel $latestModel)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([$model])));
        $this->latestModels->expects($this->once())
            ->method('findBy')
            ->willReturn(collect($latestModel ? [$latestModel] : null));
        $case = $this->getCase();

        $response = $case->execute('brand', 'model');

        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($model, $response->getPhoneModel());
        $this->assertEquals((bool) $latestModel, $response->isLatestModel());
    }

    public function executionDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $model->id = 1;
        $latestModel = new LatestModel(['model_id' => $model->getKey()]);

        return [
            [$brand, $model, $latestModel],
            [$brand, $model, null],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->latestModels);
        parent::tearDown();
    }
}
