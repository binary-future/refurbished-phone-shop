<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\ModelViewCase;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $latestModels;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->latestModels = $this->getBaseMock(LatestModels::class);
    }

    private function getCase()
    {
        return new ModelViewCase($this->queryBus, $this->latestModels);
    }

    public function testThrowsExceptionIfBrandAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('brand', 'model');
    }

    public function testThrowsExceptionIfModelAbsent()
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect()));
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('brand', 'model');
    }

    /**
     * @dataProvider executionDataProvider
     * @param Brand $brand
     * @param Phone $phone
     * @param LatestModel|null $latestModel
     */
    public function testExecution(Brand $brand,
                                  Phone $phone,
                                  ?LatestModel $latestModel)
    {
        $phones = collect([$phone]);
        $originalPhones = collect([$phone]);
        $model = $this->getBaseMock(PhoneModel::class);
        $model->expects($this->once())
            ->method('getKey')
            ->willReturn(1);
        $model->expects($this->any())
            ->method('getPhones')
            ->willReturn($phones);
        $model->expects($this->once())
            ->method('getOriginalPhones')
            ->willReturn($originalPhones);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([$model])));
        $this->latestModels->expects($this->once())
            ->method('findBy')
            ->willReturn($latestModel ? collect([$latestModel]) : collect());
        $case = $this->getCase();

        $response = $case->execute('brand', 'model');

        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($model, $response->getPhoneModel());
        $this->assertEquals($phones, $response->getPhones());
        $this->assertEquals($latestModel, $response->getLatestModel());
    }

    public function executionDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;

        $phone = new Phone();
        $phone->setRelation('color', new Color());

        $latestModel = new LatestModel();

        return [
            [$brand, $phone, $latestModel],
            [$brand, $phone, null],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->latestModels);
        parent::tearDown();
    }
}
