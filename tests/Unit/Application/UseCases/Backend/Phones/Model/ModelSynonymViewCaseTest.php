<?php


namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\ModelSynonymViewCase;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelSynonymViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var ModelSynonymViewCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = new ModelSynonymViewCase($this->queryBus);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }

    public function testThrowsExceptionIfNoBrand()
    {
        $this->queryBus->expects($this->once())->method('dispatch')->willReturn(collect());
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('brand', 'model');
    }

    public function testThrowsExceptionIfNoModel()
    {
        $brand = $this->getBrand();
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect()));
        $this->expectException(ModelNotFoundException::class);
        $this->case->execute('brand', 'model');
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @param Collection $synonyms
     * @param Collection $potentialSynonyms
     * @dataProvider executionDataProvider
     */
    public function testExecution(Brand $brand, PhoneModel $model, Collection $synonyms, Collection $potentialSynonyms)
    {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([$model]), $potentialSynonyms));
        $response = $this->case->execute('brand', 'model');
        $this->assertEquals($brand, $response->getBrand());
        $this->assertEquals($model, $response->getModel());
        $this->assertEquals($model->getSynonyms(), $response->getSynonyms());
        $this->assertEquals($potentialSynonyms, $response->getPotentialSynonyms());
    }

    public function executionDataProvider()
    {
        $brand = $this->getBrand();
        $model = $this->getModel();
        $model->setRelation('brand', $brand);

        return [
            [$brand, $model, $model->getSynonyms(), collect()],
            [$brand, $model, $model->getSynonyms(), collect([new PhoneModel()])]
        ];
    }


    private function getBrand(): Brand
    {
        $brand = new Brand();
        $brand->id = 1;
        $brand->setName('brand');

        return $brand;
    }

    private function getModel(): PhoneModel
    {
        $model = new PhoneModel();
        $model->setName('name');
        $model->setRelation('synonyms', collect());
        $model->id = 1;

        return $model;
    }
}
