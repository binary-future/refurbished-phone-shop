<?php

namespace Tests\Unit\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\ModelUpdateCase;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelUpdateRequest;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Faq\Faq;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelUpdateCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $commandBus;

    private $slugGenerator;
    private $latestModels;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
        $this->latestModels = $this->getBaseMock(LatestModels::class);
    }

    private function getCase()
    {
        return new ModelUpdateCase(
            $this->queryBus,
            $this->commandBus,
            $this->slugGenerator,
            $this->latestModels
        );
    }

    public function testThrowsExceptionIfBrandNotFound()
    {
        $request = new ModelUpdateRequest();
        $request->setBrand('brand');
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute($request);
    }

    public function testThrowsExceptionIfModelNotFound()
    {
        $request = new ModelUpdateRequest();
        $request->setBrand('brand');
        $request->setModel('model');
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect()));
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute($request);
    }

    public function testReturnsFailedResponseIfCommandThrowsException()
    {
        $request = new ModelUpdateRequest();
        $request->setBrand('brand');
        $request->setModel('model');
        $request->setName('name');
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $model->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([$model])));
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->commandBus->method('dispatch')
            ->willThrowException(new \Exception());
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertFalse($response->isSuccess());
    }

    /**
     * @dataProvider executionDataProvider
     * @param ModelUpdateRequest $request
     * @param Brand $brand
     * @param PhoneModel $model
     * @param Faq $faq
     * @param LatestModel $latestModel
     */
    public function testExecution(
        ModelUpdateRequest $request,
        Brand $brand,
        PhoneModel $model,
        Faq $faq,
        LatestModel $latestModel
    ) {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([$model]), collect([$faq])));
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->commandBus->method('dispatch')
            ->will($this->onConsecutiveCalls(null, $model));

        if ($request->isLatest()) {
            $this->latestModels->expects($this->once())
                ->method('create')
                ->willReturn($latestModel);
        } else {
            $this->latestModels->expects($this->once())
                ->method('findBy')
                ->willReturn(collect([$latestModel]));
        }
        $case = $this->getCase();

        $response = $case->execute($request);

        $this->assertTrue($response->isSuccess());
        $this->assertEquals($response->getModel(), $model);
    }

    public function executionDataProvider()
    {
        $request1 = new ModelUpdateRequest();
        $request1->setBrand('brand');
        $request1->setModel('model');
        $request1->setName('name');
        $request1->setIsLatest(true);
        $request1->setFaqs([
            [
                Faq::FIELD_ID => 1,
                Faq::FIELD_QUESTION => 'question',
                Faq::FIELD_ANSWER => 'answer',
            ]
        ]);

        $request2 = clone $request1;
        $request2->setIsLatest(false);

        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $model->id = 1;
        $faq = new Faq();
        $faq->id = 1;
        $latestModel = $this->getBaseMock(LatestModel::class);

        return [
            [$request1, $brand, $model, $faq, $latestModel],
            [$request2, $brand, $model, $faq, $latestModel],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->commandBus, $this->slugGenerator, $this->latestModels);
        parent::tearDown();
    }
}
