<?php

namespace Tests\Unit\Application\UseCases\Backend\Networks;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Networks\NetworkEditCase;
use App\Domain\Deals\Contract\Network;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class NetworkEditCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new NetworkEditCase($this->queryBus);
    }

    public function testExecution()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Network()]));
        $case = $this->getUseCase();
        $response = $case->execute('slug');
        $this->assertInstanceOf(Network::class, $response->getNetwork());
    }

    public function testCaseThrowsExceptionIfNetworkNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('slug');
    }
}
