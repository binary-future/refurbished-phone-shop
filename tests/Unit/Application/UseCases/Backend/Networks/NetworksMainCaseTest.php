<?php

namespace Tests\Unit\Application\UseCases\Backend\Networks;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Networks\NetworksMainCase;
use App\Domain\Deals\Contract\Network;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class NetworksMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new NetworksMainCase($this->queryBus);
    }

    public function testExecution()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Network()]));
        $case = $this->getUseCase();
        $response = $case->execute();
        $this->assertInstanceOf(Network::class, $response->getNetworks()->first());
    }
}
