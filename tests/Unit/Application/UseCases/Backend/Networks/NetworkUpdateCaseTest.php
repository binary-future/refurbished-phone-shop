<?php

namespace Tests\Unit\Application\UseCases\Backend\Networks;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\UseCases\Backend\Networks\NetworksUpdateCase;
use App\Domain\Deals\Contract\Network;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class NetworkUpdateCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $uploader;

    private $commandBus;

    private $slugGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->uploader = $this->getBaseMock(Uploader::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->slugGenerator = $this->getBaseMock(Generator::class);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->uploader, $this->commandBus, $this->slugGenerator);
        parent::tearDown();
    }

    protected function getCase()
    {
        return new NetworksUpdateCase(
            $this->queryBus,
            $this->uploader,
            $this->commandBus,
            $this->slugGenerator
        );
    }

    public function testExecutionIfLogoAbsent()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Network()]));
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new Network());
        $case = $this->getCase();
        $response = $case->execute('slug', ['name' => 'name']);
        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(Network::class, $response->getNetwork());
    }

    public function testCaseThrowsExceptionIfNoOperator()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $this->slugGenerator->expects($this->never())
            ->method('generate');
        $this->uploader->expects($this->never())
            ->method('storeImage');
        $this->commandBus->expects($this->never())
            ->method('dispatch');
        $case = $this->getCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('slug', ['name' => 'name']);
    }

    public function testExecutionWithLogo()
    {
        $network = new Network();
        $network->id = 1;
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$network]));
        $this->slugGenerator->method('generate')
            ->willReturn('slug');
        $this->uploader->expects($this->once())
            ->method('storeImage')
            ->willReturn(new Image());
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($network);
        $case = $this->getCase();
        $response = $case->execute(
            'slug',
            ['name' => 'name', 'logo' => $this->getBaseMock(UploadedFile::class)]
        );
        $this->assertTrue($response->isSuccess());
        $this->assertInstanceOf(Network::class, $response->getNetwork());
    }

    public function testExecutionReturnsFailedResponseIfWrongData()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Network()]));
        $case = $this->getCase();
        $response = $case->execute('slug', []);
        $this->assertFalse($response->isSuccess());
    }
}
