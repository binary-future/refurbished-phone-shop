<?php


namespace Tests\Unit\Application\UseCases\Store;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Store\StoreViewCase;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    /**
     * @var StoreViewCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = $this->getCase();
    }

    private function getCase()
    {
        return new StoreViewCase($this->queryBus);
    }

    public function testExecution()
    {
        $store = new Store();
        $phones = collect([new Phone()]);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$store]), $phones));
        $case = $this->case;
        $response = $case->execute('slug');
        $this->assertEquals($store, $response->getStore());
        $this->assertEquals($phones, $response->getPhones());
    }

    public function testThrowsExceptionIfNoStore()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->case;
        $this->expectException(ModelNotFoundException::class);
        $case->execute('slug');
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }
}
