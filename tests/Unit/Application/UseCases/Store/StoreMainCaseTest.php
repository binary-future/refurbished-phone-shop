<?php


namespace Tests\Unit\Application\UseCases\Store;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Store\StoreMainCase;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreMainCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getCase()
    {
        return new StoreMainCase($this->queryBus);
    }

    public function testExecution()
    {
        $stores = collect([new Store()]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($stores);
        $case = $this->getCase();
        $response = $case->execute();
        $this->assertEquals($stores, $response->getStores());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
