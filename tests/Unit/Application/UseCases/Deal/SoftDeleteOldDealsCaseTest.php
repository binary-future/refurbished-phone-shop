<?php

namespace Tests\Unit\Application\UseCases\Deal;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Deal\SoftDeleteOldDealsCase;
use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Domain\Store\Store;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class SoftDeleteOldDealsCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var SoftDeleteOldDealsCase
     */
    private $case;

    private $queryBus;

    private $commandBus;

    private $positiveReports;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->positiveReports = $this->getBaseMock(PositiveReports::class);

        $this->case = $this->getCase();
    }

    /**
     * @param SoftDealsClearResponse $response
     * @param Collection $stores
     * @param Collection $reports
     * @dataProvider responseDataProvider
     */
    public function testExecution(SoftDealsClearResponse $response, Collection $stores, Collection $reports)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($stores);
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($response);
        $this->positiveReports->method('findBy')
            ->willReturn($reports);
        $result = $this->case->execute();
        $this->assertEquals($result, [$response]);
        $this->assertFalse($result[0]->isError());
    }

    public function responseDataProvider()
    {
        $store = $this->getStore();
        $report = $this->getBaseMock(PositiveReport::class);
        $report->method('getCreatedAt')
            ->willReturn(Carbon::now());

        return [
            [
                new SoftDealsClearResponse($store, 1, 0),
                collect([$store]),
                collect([$report]),
            ]
        ];
    }

    /**
     * @param array $response
     * @param Collection $stores
     * @dataProvider skippedDataProvider
     */
    public function testSkippStoreIfNoPositiveReports(array $response, Collection $stores)
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($stores);
        $this->positiveReports->method('findBy')
            ->willReturn(collect());

        $this->assertEquals($this->case->execute(), $response);
        $this->assertTrue($response[0]->isError());
    }

    public function skippedDataProvider()
    {
        $store = $this->getStore();
        $response = new SoftDealsClearResponse($store, 0, 0);
        $response->setError(true);

        return [
            [
                [$response],
                collect([$store]),
            ]
        ];
    }

    /**
     * @param SoftDealsClearResponse $response
     * @param Collection $stores
     * @param Collection $reports
     * @dataProvider commandErrorDataProvider
     */
    public function testIfQueryBusThrowsException(
        SoftDealsClearResponse $response,
        Collection $stores,
        Collection $reports
    ) {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($stores);
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->will($this->throwException(new \Exception()));
        $this->positiveReports->method('findBy')
            ->willReturn($reports);
        $result = $this->case->execute();
        $this->assertEquals($result, [$response]);
        $this->assertTrue($result[0]->isError());
    }

    public function commandErrorDataProvider()
    {
        $store = $this->getStore();
        $report = $this->getBaseMock(PositiveReport::class);
        $report->method('getCreatedAt')
            ->willReturn(Carbon::now());
        $response = new SoftDealsClearResponse($store, 0, 0);
        $response->setError(true);

        return [
            [
                $response,
                collect([$store]),
                collect([$report]),
            ]
        ];
    }

    private function getCase()
    {
        return new SoftDeleteOldDealsCase($this->queryBus, $this->commandBus, $this->positiveReports);
    }

    private function getStore()
    {
        $store = new Store();
        $store->id = 1;
        $store->setName('store');

        return $store;
    }

    protected function tearDown(): void
    {
        unset($this->case, $this->commandBus, $this->positiveReports);
        parent::tearDown();
    }
}
