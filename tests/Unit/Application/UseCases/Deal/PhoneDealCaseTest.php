<?php


namespace Tests\Unit\Application\UseCases\Deal;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Deal\PhoneDealCase;
use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class PhoneDealCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new PhoneDealCase($this->queryBus);
    }

    public function testCaseThrowsExceptionIfDealNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute(1);
    }

    public function testCaseThrowsExceptionIfDealProductNotPhone()
    {
        $product = $this->getBaseMock(Product::class);
        $deal = $this->getBaseMock(Deal::class);
        $deal->method('getProduct')
            ->willReturn($product);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$deal]));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute(1);
    }

    public function testExecution()
    {
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('isActive')->willReturn(true);
        $deal = $this->getBaseMock(Deal::class);
        $deal->method('getProduct')
            ->willReturn($phone);
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$deal]), collect()));
        $case = $this->getUseCase();
        $response = $case->execute(1);
        $this->assertEquals($deal, $response->getDeal());
        $this->assertEquals($phone, $response->getPhone());
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }
}
