<?php

namespace Tests\Unit\Application\UseCases\Deal;

use App\Application\Services\Bus\CommandBus;
use App\Application\UseCases\Deal\HardDeleteOldDealsCase;
use App\Application\UseCases\Deal\Requests\HardDeleteOldDealsRequest;
use App\Application\UseCases\Deal\Responses\HardDealsClearResponse;
use App\Domain\Deals\Deal\DomainResponse\DeleteDealsDomainResponse;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class HardDeleteOldDealsCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var HardDeleteOldDealsCase
     */
    private $case;

    private $commandBus;

    private $channelLogger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->channelLogger = $this->getBaseMock(ChannelLogger::class);
        $this->case = $this->getCase();
    }

    protected function tearDown(): void
    {
        unset($this->case, $this->commandBus, $this->channelLogger);
        parent::tearDown();
    }

    /**
     * @param int $deletedDeals
     * @param int $deletedLinks
     * @param HardDealsClearResponse $response
     * @dataProvider responseDataProvider
     */
    public function testExecution(
        int $deletedDeals,
        int $deletedLinks,
        HardDealsClearResponse $response
    ) {
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(new DeleteDealsDomainResponse($deletedDeals, $deletedLinks));
        $this->channelLogger->expects($this->never())
            ->method('error');
        $request = new HardDeleteOldDealsRequest(1);

        $result = $this->case->execute($request);

        $this->assertEquals($response, $result);
        $this->assertFalse($result->isError());
    }

    public function responseDataProvider()
    {
        return [
            [1, 1, new HardDealsClearResponse(1, 1)]
        ];
    }

    /**
     * @param HardDealsClearResponse $response
     * @dataProvider commandErrorDataProvider
     */
    public function testIfCommandBusThrowsException(
        HardDealsClearResponse $response
    ) {
        $exception = new \Exception();
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->will($this->throwException($exception));
        $this->channelLogger->expects($this->once())
            ->method('channel')
            ->willReturn($this->channelLogger);
        $this->channelLogger->expects($this->once())
            ->method('error')
            ->with($exception);
        $request = new HardDeleteOldDealsRequest(1);

        $result = $this->case->execute($request);

        $this->assertEquals($result, $response);
        $this->assertTrue($result->isError());
    }

    public function commandErrorDataProvider(): array
    {
        $response = new HardDealsClearResponse(0, 0);
        $response->setError(true);

        return [
            [$response]
        ];
    }

    private function getCase(): HardDeleteOldDealsCase
    {
        return new HardDeleteOldDealsCase($this->commandBus, $this->channelLogger);
    }
}
