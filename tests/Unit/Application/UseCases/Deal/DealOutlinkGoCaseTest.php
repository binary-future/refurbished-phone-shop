<?php

namespace Tests\Unit\Application\UseCases\Deal;

use App\Application\Services\Affiliation\Contracts\Generator;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Deal\DealOutlinkGoCase;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Shared\Link\Link;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DealOutlinkGoCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $generator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->generator = $this->getBaseMock(Generator::class);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->generator);
        parent::tearDown();
    }

    private function getCase()
    {
        return new DealOutlinkGoCase($this->queryBus, $this->generator);
    }

    public function testExecution()
    {
        $link = new Link();
        $deal = $this->getBaseMock(Deal::class);
        $deal->method('getLink')
            ->willReturn($link);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$deal]));
        $this->generator->method('generate')
            ->willReturn('string');
        $case = $this->getCase();

        $response = $case->execute(1, 'url');

        $this->assertEquals($response->getDeal(), $deal);
        $this->assertEquals($response->getLink(), $link);
    }

    public function testThrowExceptionIfNoDeal()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute(1, 'url');
    }

    public function testThrowsExceptionIfDealNotHaveLink()
    {
        $deal = $this->getBaseMock(Deal::class);
        $deal->method('getLink')
            ->willReturn(null);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$deal]));
        $case = $this->getCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute(1, 'url');
    }
}
