<?php


namespace Tests\Unit\Application\UseCases\Deal;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\UseCases\Deal\ClearApiDealsCase;
use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ClearApiDealsCaseTest extends TestCase
{
    use BaseMock;

    /**
     * @var ClearApiDealsCase
     */
    private $case;

    private $datafeeds;

    private $commandBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->datafeeds = $this->getBaseMock(DatafeedsInfo::class);
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->case = new ClearApiDealsCase($this->datafeeds, $this->commandBus);
    }

    /**
     * @param SoftDealsClearResponse $response
     * @param Store $store
     * @dataProvider validDataProvider
     */
    public function testExecution(SoftDealsClearResponse $response, Store $store)
    {
        $datafeed = $this->getBaseMock(DatafeedInfo::class);
        $datafeed->method('getStore')->willReturn($store);
        $this->datafeeds->method('findBy')->willReturn(collect([$datafeed]));
        $this->commandBus->method('dispatch')->willReturn($response);
        $result = $this->case->execute('api');
        $this->assertEquals([$response], $result);
        $this->assertFalse($result[0]->isError());
    }

    public function validDataProvider()
    {
        $store = new Store();
        $store->id = 1;
        $response = new SoftDealsClearResponse($store, 1, 0);

        return [
            [$response, $store]
        ];
    }

    public function testIfNoDatafeeds()
    {
        $this->datafeeds->method('findBy')->willReturn(collect());
        $this->assertEquals([], $this->case->execute('api'));
    }

    /**
     * @dataProvider errorDataProvider
     * @param SoftDealsClearResponse $response
     * @param Store $store
     */
    public function testIfCommandBusThrowsException(SoftDealsClearResponse $response, Store $store)
    {
        $datafeed = $this->getBaseMock(DatafeedInfo::class);
        $datafeed->method('getStore')->willReturn($store);
        $this->datafeeds->method('findBy')->willReturn(collect([$datafeed]));
        $this->commandBus->method('dispatch')->will($this->throwException(new \Exception()));
        $result = $this->case->execute('api');
        $this->assertEquals([$response], $result);
        $this->assertTrue($result[0]->isError());
    }

    public function errorDataProvider()
    {
        $store = new Store();
        $store->id = 1;
        $response = new SoftDealsClearResponse($store, 0, 0);
        $response->setError(true);

        return [
            [$response, $store]
        ];
    }

    protected function tearDown(): void
    {
        unset($this->case, $this->datafeeds, $this->commandBus);
        parent::tearDown();
    }
}
