<?php

namespace Tests\Unit\Application\UseCases\Deal;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Deal\DealOutlinkCase;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DealOutlinkCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus);
        parent::tearDown();
    }

    private function getCase()
    {
        return new DealOutlinkCase($this->queryBus);
    }

    public function testExecution()
    {
        $store = $this->getBaseMock(Store::class);
        $deal = $this->getBaseMock(Deal::class);
        $deal->expects($this->once())
            ->method('getStore')
            ->willReturn($store);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([$deal]));
        $case = $this->getCase();

        $response = $case->execute(1);

        $this->assertEquals($response->getStore(), $store);
        $this->assertEquals($response->getUrlToGo(), null);
    }

    public function testThrowExceptionIfNoDeal()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute(1);
    }
}
