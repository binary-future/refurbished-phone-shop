<?php

namespace Tests\Unit\Application\UseCases\Deal;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Filters\Deals\QueryAssemblers\FiltersSearchQueryAssembler;
use App\Application\UseCases\Deal\GenerateDealsFiltersCacheCase;
use App\Application\UseCases\Deal\Responses\GenerateDealsFiltersCacheResponse;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Adapters\Cache\Contracts\Cache;
use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

class GenerateDealsFiltersCacheCaseTest extends TestCase
{
    use BaseMock;

    private $assembler;
    private $queryBus;
    private $cache;

    protected function setUp(): void
    {
        parent::setUp();

        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->cache = $this->getBaseMock(Cache::class);
        $this->assembler = $this->getBaseMock(FiltersSearchQueryAssembler::class);
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->cache, $this->assembler);
        parent::tearDown();
    }

    private function getCase(): GenerateDealsFiltersCacheCase
    {
        return new GenerateDealsFiltersCacheCase(
            $this->queryBus,
            $this->cache,
            $this->assembler
        );
    }

    /**
     * @param Collection $brands
     * @param Collection $models
     * @param FiltersSearchQuery $query
     * @param array $dealFilters
     * @param string $cacheKey
     * @param array $expectedCacheValue
     * @dataProvider executeDataProvider
     */
    public function testExecute(
        Collection $brands,
        Collection $models,
        FiltersSearchQuery $query,
        array $dealFilters,
        string $cacheKey,
        array $expectedCacheValue
    ) {
        $this->queryBus->expects($this->atLeastOnce())->method('dispatch')
            ->willReturnOnConsecutiveCalls($brands, $models, ...$dealFilters);
        $this->assembler->expects($this->once())->method('buildQuery')
            ->willReturn($query);
        $this->cache->expects($this->once())->method('forever')
            ->with($cacheKey, $expectedCacheValue);
        $case = $this->getCase();

        $iterableResult = $case->execute();
        foreach ($iterableResult as $item) {
            $this->assertInstanceOf(GenerateDealsFiltersCacheResponse::class, $item);
            $this->assertEquals(true, $item->isSuccess());
        }

        $this->assertInstanceOf(\Iterator::class, $iterableResult);
    }

    public function executeDataProvider(): array
    {
        $brand = new Brand([Brand::FIELD_SLUG => 'brand']);
        $brand->id = 1;
        $brands = collect([$brand]);

        $phone = new Phone();
        $phones = collect([$phone]);

        $model = $this->getBaseMock(PhoneModel::class);
        $model->method('getPhones')->willReturn($phones);
        $models = collect([$model]);

        $query = new FiltersSearchQuery();
        $query->setProducts($phones);

        $networksId = [2, 5, 6, 7];
        $conditions = [Condition::PRISTINE()->getValue(), Condition::VERY_GOOD()->getValue()];
        $monthlyCosts = [20, 25, 26, 35];
        $totalCosts = [0];
        $dealFilters = [
            collect($networksId),
            collect($conditions),
            collect($monthlyCosts),
            collect($totalCosts),
        ];

        $cacheKey = 'deal:filters:' . $brand->getSlug();
        $expectedCacheValue = [
            Contract::FIELD_NETWORK_ID  => $networksId,
            Contract::FIELD_CONDITION  => $conditions,
            Deal::FIELD_MONTHLY_COST => $monthlyCosts,
            Deal::FIELD_TOTAL_COST => $totalCosts,
        ];

        return [
            [$brands, $models, $query, $dealFilters, $cacheKey, $expectedCacheValue]
        ];
    }

    public function testQueryBusThrowException()
    {
        $this->queryBus->expects($this->atLeastOnce())->method('dispatch')
            ->willThrowException(new \Exception());

        $case = $this->getCase();

        $iterableResult = $case->execute();
        foreach ($iterableResult as $item) {
            $this->assertInstanceOf(GenerateDealsFiltersCacheResponse::class, $item);
            $this->assertEquals(false, $item->isSuccess());
        }

        $this->assertInstanceOf(\Iterator::class, $iterableResult);
    }
}
