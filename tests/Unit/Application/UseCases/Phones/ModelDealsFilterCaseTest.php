<?php

namespace Tests\Unit\Application\UseCases\Phones;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Contracts\DealsSearcher;
use App\Application\Services\Search\DealsInfoSearcher;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\DealIndex;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Application\UseCases\Phones\ModelDealsFilterCase;
use App\Application\UseCases\Phones\Requests\ModelDealsFilterRequest;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorAlias;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class ModelDealsFilterCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $searcher;

    private $filters;

    private $dealsInfoSearcher;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->searcher = $this->getBaseMock(DealsSearcher::class);
        $this->filters = $this->getBaseMock(FiltersSearcher::class);
        $this->dealsInfoSearcher = $this->getBaseMock(DealsInfoSearcher::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->searcher,
            $this->filters,
            $this->dealsInfoSearcher
        );
        parent::tearDown();
    }

    private function getUseCase(): ModelDealsFilterCase
    {
        return new ModelDealsFilterCase(
            $this->queryBus,
            $this->searcher,
            $this->filters,
            $this->dealsInfoSearcher
        );
    }

    public function testCaseThrowsExceptionIfBrandNotFound()
    {
        $request = new ModelDealsFilterRequest();
        $request->setBrand('brand');
        $request->setModel('model');
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute($request);
    }

    public function testCaseThrowsExceptionIfModelNotFound()
    {
        $request = new ModelDealsFilterRequest();
        $request->setBrand('brand');
        $request->setModel('model');
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([])));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute($request);
    }

    /**
     * @param Collection $brands
     * @param Collection $models
     * @param LengthAwarePaginatorAlias $deals
     * @param DealIndex $dealIndex
     * @dataProvider validDataProvider
     */
    public function testCaseReturnsResponse(
        Collection $brands,
        Collection $models,
        LengthAwarePaginatorAlias $deals,
        DealIndex $dealIndex
    ) {
        $request = new ModelDealsFilterRequest();
        $request->setBrand('brand');
        $request->setModel('model');
        $this->queryBus->expects($this->atLeastOnce())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls($brands, $models, collect(), collect()));
        $this->searcher->method('searchPaginated')
            ->willReturn($deals);
        $this->filters->method('generate')
            ->willReturn($dealIndex);
        $this->dealsInfoSearcher->expects($this->once())
            ->method('searchDealsInfo')
            ->willReturn(new DealsInfoSearchResponse());
        $case = $this->getUseCase();

        $response = $case->execute($request);

        $this->assertEquals($brands->first(), $response->getBrand());
        $this->assertEquals($models->first(), $response->getModel());
        $this->assertEquals($deals, $response->getDeals());
        $this->assertEquals($dealIndex, $response->getDealIndex());
    }

    public function validDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $phone = new Phone();
        $phone->id = 1;
        $model->setRelation('phones', collect([$phone]));
        $dealIndex = new DealIndex(collect(), collect(), collect(), collect(), collect());

        return [
            [
                collect([$brand]),
                collect([$model]),
                $this->getBaseMock(LengthAwarePaginatorAlias::class),
                $dealIndex
            ],
        ];
    }
}
