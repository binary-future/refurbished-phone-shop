<?php


namespace Tests\Unit\Application\UseCases\Phones;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Phones\ModelColorViewCase;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelColorViewCaseTest extends TestCase
{
    use BaseMock;

    use BaseMock;

    private $queryBus;

    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->case = $this->getUseCase();
    }

    private function getUseCase()
    {
        return new ModelColorViewCase($this->queryBus);
    }

    public function testCaseThrowsExceptionIfBrandNotFound()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('Slug', 'model', 'color');
    }

    public function testCaseThrowsExceptionIfModelNotFound()
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([])));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('Slug', 'model', 'color');
    }

    /**
     * @param string $color
     * @param Collection $brands
     * @param Collection $models
     * @dataProvider invalidColorDataProvider
     */
    public function testCaseThrowsExceptionIfColorNotFound(
        string $color,
        Collection $brands,
        Collection $models
    ) {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls($brands, $models));
        $case = $this->getUseCase();
        $this->expectException(ModelNotFoundException::class);
        $case->execute('Slug', 'model', $color);
    }

    public function invalidColorDataProvider()
    {
        $color = 'color';
        $colorEntity = new Color();
        $colorEntity->setSlug('slug');
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getColor')
            ->willReturn($colorEntity);
        $brand = new Brand();
        $brand->id = 1;
        $phoneModel = $this->getBaseMock(PhoneModel::class);
        $phoneModel->method('getPhones')
            ->willReturn(collect([$phone]));

        return [
            [$color, collect([$brand]), collect([$phoneModel])],
        ];
    }

    /**
     * @param string $color
     * @param Collection $brands
     * @param Collection $models
     * @param Color $colorEntity
     * @param Collection $deals
     * @dataProvider validDataProvider
     */
    public function testCaseReturnsResponse(
        string $color,
        Collection $brands,
        Collection $models,
        Color $colorEntity,
        Collection $deals
    ) {
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls($brands, $models, $deals));
        $case = $this->getUseCase();
        $response = $case->execute('slug', 'model', $color);
        $this->assertEquals($brands->first(), $response->getBrand());
        $this->assertEquals($models->first(), $response->getModel());
        $this->assertEquals($colorEntity, $response->getColor());
        $this->assertEquals($deals, $response->getDeals());
    }

    public function validDataProvider()
    {
        $color = 'color';
        $colorEntity = new Color();
        $colorEntity->setSlug($color);
        $phone = $this->getBaseMock(Phone::class);
        $phone->method('getColor')
            ->willReturn($colorEntity);
        $brand = new Brand();
        $brand->id = 1;
        $phoneModel = $this->getBaseMock(PhoneModel::class);
        $phoneModel->method('getPhones')
            ->willReturn(collect([$phone]));
        $brand = new Brand();
        $brand->id = 1;
        $deals = collect([new Deal()]);

        return [
            [
                $color,
                collect([$brand]),
                collect([$phoneModel]),
                $colorEntity,
                $deals
            ],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->queryBus, $this->case);
        parent::tearDown();
    }
}
