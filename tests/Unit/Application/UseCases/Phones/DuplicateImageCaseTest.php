<?php

namespace Tests\Unit\Application\UseCases\Phones;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Contracts\PathHelper;
use App\Application\UseCases\Phones\DuplicateImageCase;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class DuplicateImageCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;
    private $commandBus;
    private $storage;
    private $serializer;
    private $pathhelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->getBaseMock(CommandBus::class);
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->storage = $this->getBaseMock(Storage::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->pathhelper = $this->getBaseMock(PathHelper::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->commandBus,
            $this->queryBus,
            $this->storage,
            $this->serializer,
            $this->pathhelper
        );
        parent::tearDown();
    }


    private function getUseCase(): DuplicateImageCase
    {
        return new DuplicateImageCase(
            $this->commandBus,
            $this->queryBus,
            $this->storage,
            $this->serializer,
            $this->pathhelper
        );
    }

    /**
     * @param Collection $phonesWithoutImage
     * @param Collection $relatedPhones
     * @param string $relatedImagePath
     * @param string $imagePath
     * @param string $imageStoragePath
     * @param Image $image
     * @dataProvider validDataProvider
     */
    public function testExecution(
        Collection $phonesWithoutImage,
        Collection $relatedPhones,
        string $relatedImagePath,
        string $imagePath,
        string $imageStoragePath,
        Image $image
    ) {
        $this->queryBus->expects($this->exactly(2))
            ->method('dispatch')
            ->willReturnOnConsecutiveCalls($phonesWithoutImage, $relatedPhones);
        $this->pathhelper->expects($this->exactly(2))
            ->method('getImagePath')
            ->willReturnOnConsecutiveCalls($relatedImagePath, $imagePath);
        $this->storage->expects($this->once())
            ->method('exists')
            ->willReturn(false);
        $this->storage->expects($this->once())
            ->method('copy')
            ->willReturn(true);
        $this->pathhelper->expects($this->once())
            ->method('transformStoragePathToRelativeUrl')
            ->willReturn($imageStoragePath);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn($image);
        $this->commandBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($phonesWithoutImage->first());
        $case = $this->getUseCase();

        foreach ($case->execute() as $response) {
            $this->assertEquals(true, $response->isSuccess());
        }
    }

    public function validDataProvider(): array
    {
        $relatedImagePath = 'storage/path/to/image-32GB.png';
        $imagePath = 'public/path/to/image-64GB.png';
        $imageStoragePath = 'storage/path/to/image-64GB.png';

        $relatedImage = new Image([
            Image::FIELD_OWNER_ID => 1,
            Image::FIELD_OWNER_TYPE => Image::TYPE_PHONES,
            Image::FIELD_IS_LOCAL => 1,
            Image::FIELD_IS_MAIN => 1,
            Image::FIELD_PATH => $relatedImagePath,
            Image::FIELD_PARENT_ID => null,
            Image::FIELD_HEIGHT => null,
            Image::FIELD_WIDTH => null,
        ]);
        $relatedImage->id = 1;
        $relatedImages = collect([$relatedImage]);
        $relatedPhone = $this->getBaseMock(Phone::class);
        $relatedPhone->expects($this->once())->method('getImages')->willReturn($relatedImages);
        $relatedPhones = collect([$relatedPhone]);

        $image = new Image([
            Image::FIELD_OWNER_ID => 1, // $phoneWithoutImage->getKey()
            Image::FIELD_OWNER_TYPE => $relatedImage->getImagableType(),
            Image::FIELD_IS_LOCAL => $relatedImage->isLocal(),
            Image::FIELD_IS_MAIN => $relatedImage->isMain(),
            Image::FIELD_PATH => $imageStoragePath,
            Image::FIELD_PARENT_ID => null,
            Image::FIELD_HEIGHT => $relatedImage->getSize() ? $relatedImage->getSize()->getHeight() : null,
            Image::FIELD_WIDTH => $relatedImage->getSize() ? $relatedImage->getSize()->getWidth() : null,
        ]);
        $image->id = 1;
        $images = collect([$image]);

        $phoneWithoutImage = $this->getBaseMock(Phone::class);
        $phoneWithoutImage->expects($this->once())->method('getColorId')->willReturn(1);
        $phoneWithoutImage->expects($this->once())->method('getModelId')->willReturn(1);
        $phoneWithoutImage->expects($this->atLeastOnce())->method('getKey')->willReturn(1);
        $phoneWithoutImage->expects($this->once())->method('getImages')->willReturn($images);
        $phonesWithoutImage = collect([$phoneWithoutImage]);

        return [
            [$phonesWithoutImage, $relatedPhones, $relatedImagePath, $imagePath, $imageStoragePath, $image]
        ];
    }
}
