<?php

namespace Tests\Unit\Application\UseCases\Phones;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Phones\BrandsListCase;
use App\Domain\Phone\Brand\Brand;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class BrandListCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
    }

    private function getUseCase()
    {
        return new BrandsListCase($this->queryBus);
    }

    public function testExecution()
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([new Brand()]));
        $case = $this->getUseCase();
        $response = $case->execute();
        $this->assertInstanceOf(Brand::class, $response->getBrands()->first());
    }
}
