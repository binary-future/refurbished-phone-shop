<?php

namespace Tests\Unit\Application\UseCases\Phones;

use App\Application\Services\Blog\PostService;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Contracts\DealsSearcher;
use App\Application\Services\Search\DealsInfoSearcher;
use App\Application\Services\Search\Filters\Contracts\FilterInitializer;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\DealIndex;
use App\Application\Services\Search\Filters\ModelDealsAdjustedFilters;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Application\UseCases\Phones\ModelViewCase;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $searcher;

    private $filtersSearcher;

    private $postService;
    private $dealsSearcher;
    private $filterInitializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->searcher = $this->getBaseMock(DealsInfoSearcher::class);
        $this->dealsSearcher = $this->getBaseMock(DealsSearcher::class);
        $this->filtersSearcher = $this->getBaseMock(FiltersSearcher::class);
        $this->postService = $this->getBaseMock(PostService::class);
        $this->filterInitializer = $this->getBaseMock(FilterInitializer::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->searcher,
            $this->dealsSearcher,
            $this->filtersSearcher,
            $this->postService,
            $this->filterInitializer,
        );
        parent::tearDown();
    }

    private function getUseCase(): ModelViewCase
    {
        return new ModelViewCase(
            $this->queryBus,
            $this->searcher,
            $this->dealsSearcher,
            $this->filtersSearcher,
            $this->postService,
            $this->filterInitializer,
        );
    }

    public function testCaseThrowsExceptionIfBrandNotFound(): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $this->filterInitializer->expects($this->never())->method('init');
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute('Slug', 'model');
    }

    public function testCaseThrowsExceptionIfModelNotFound()
    {
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->any())
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([])));
        $this->filterInitializer->expects($this->never())->method('init');
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute('Slug', 'model');
    }

    /**
     * @param Collection $brands
     * @param Collection $models
     * @param Collection $phones
     * @param Collection $conditions
     * @param Paginator $deals
     * @param ModelDealsAdjustedFilters $adjustedFilters
     * @dataProvider validDataProvider
     */
    public function testCaseReturnsResponse(
        Collection $brands,
        Collection $models,
        Collection $phones,
        Collection $conditions,
        Paginator $deals,
        ModelDealsAdjustedFilters $adjustedFilters
    ): void {
        $searcherResponse = new DealsInfoSearchResponse();
        $filters = new DealIndex(collect(), collect(), collect(), collect(), collect());
        $this->queryBus->expects($this->exactly(5))
            ->method('dispatch')
            ->will($this->onConsecutiveCalls($brands, $models, $phones, collect(), $conditions));
        $this->dealsSearcher->expects($this->once())
            ->method('searchPaginated')
            ->willReturn($deals);
        $this->filtersSearcher->expects($this->once())
            ->method('generate')
            ->willReturn($filters);
        $this->filterInitializer->expects($this->once())
            ->method('init')
            ->willReturn($adjustedFilters);
        $this->searcher->expects($this->once())
            ->method('searchDealsInfo')
            ->willReturn($searcherResponse);
        $this->postService->expects($this->once())
            ->method('searchByModel')
            ->willReturn(collect());
        $case = $this->getUseCase();

        $response = $case->execute('slug', 'model');

        $this->assertEquals($brands->first(), $response->getBrand());
        $this->assertEquals($models->first(), $response->getModel());
        $this->assertEquals($phones, $response->getPhones());
        $this->assertEquals($deals, $response->getDeals());
        $this->assertEquals($filters, $response->getEnabledFilters());
        $this->assertEquals($searcherResponse, $response->getDealsInfo());
        $this->assertEquals($conditions, $response->getConditions());
    }

    public function validDataProvider(): array
    {
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $phone = new Phone();
        $phone->id = 1;
        $adjustedFilters = new ModelDealsAdjustedFilters();
        $condition = Condition::PRISTINE()->getValue();

        return [
            [
                collect([$brand]),
                collect([$model]),
                collect([$phone]),
                collect([$condition]),
                $this->getBaseMock(Paginator::class),
                $adjustedFilters,
            ],
        ];
    }
}
