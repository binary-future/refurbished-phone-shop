<?php

namespace Tests\Unit\Application\UseCases\Phones;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Contracts\DealsSearcher;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\DealIndex;
use App\Application\UseCases\Phones\BrandViewDealsFilterCase;
use App\Application\UseCases\Phones\Requests\BrandDealsFilterRequest;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Network;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

class BrandViewDealsFilterCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $dealsSearcher;

    private $filtersSearcher;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->dealsSearcher = $this->getBaseMock(DealsSearcher::class);
        $this->filtersSearcher = $this->getBaseMock(FiltersSearcher::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->filtersSearcher,
            $this->dealsSearcher
        );
        parent::tearDown();
    }

    private function getUseCase(): BrandViewDealsFilterCase
    {
        return new BrandViewDealsFilterCase(
            $this->queryBus,
            $this->dealsSearcher,
            $this->filtersSearcher,
        );
    }

    public function testCaseThrowsExceptionIfBrandNotFound()
    {
        $request = new BrandDealsFilterRequest();
        $request->setBrand('brand');
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect());
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute($request);
    }

    public function testCaseReturnEmptyResponseIfModelNotFound()
    {
        $dealIndex = $this->generateFiltersResponse();
        $request = new BrandDealsFilterRequest();
        $request->setBrand('brand');
        $brand = new Brand();
        $brand->id = 1;
        $this->queryBus->expects($this->exactly(2))
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect([])));
        $this->filtersSearcher->method('generate')
            ->willReturn($dealIndex);
        $case = $this->getUseCase();

        $result = $case->execute($request);

        $this->assertTrue($result->getPhones()->isEmpty());
        $this->assertTrue($result->getDeals()->isEmpty());
        $this->assertEquals($dealIndex, $result->getDealIndex());
    }

    /**
     * @param BrandDealsFilterRequest $request
     * @param Collection $phones
     * @param Collection $models
     * @param Collection $brands
     * @param Collection $networks
     * @param Collection $conditions
     * @dataProvider validDataProvider
     */
    public function testExecution(
        BrandDealsFilterRequest $request,
        Collection $phones,
        Collection $models,
        Collection $brands,
        Collection $networks,
        Collection $conditions
    ) {
        $this->queryBus->expects($this->exactly(4))
            ->method('dispatch')
            ->willReturnOnConsecutiveCalls(
                $brands,
                $models,
                $networks,
                $conditions
            );
        $this->dealsSearcher->method('searchPaginated')
            ->willReturn(new LengthAwarePaginator(collect(), 0, 10));
        $dealIndex = $this->generateFiltersResponse();
        $this->filtersSearcher->method('generate')
            ->willReturn($dealIndex);
        $case = $this->getUseCase();

        $response = $case->execute($request);

        $this->assertEquals($phones, $response->getPhones());
        $this->assertEquals($networks, $response->getAllNetworks());
        $this->assertEquals($conditions, $response->getAllConditions());
        $this->assertEquals($dealIndex, $response->getDealIndex());
    }

    public function validDataProvider(): array
    {
        $request = new BrandDealsFilterRequest();
        $request->setBrand('brand');
        $phone = new Phone();
        $phone->id = 1;
        $phones = collect([1 => $phone]);
        $model = $this->getBaseMock(PhoneModel::class);
        $model->method('getPhones')->willReturn(collect($phones));
        $models = collect([$model]);
        $brand = new Brand();
        $brand->id = 1;
        $brands = collect([$brand]);
        $network = new Network();
        $networks = collect([$network]);
        $conditions = collect([Condition::PRISTINE()->getValue()]);

        return [
            [$request, $phones, $models, $brands, $networks, $conditions],
        ];
    }

    private function generateFiltersResponse(): DealIndex
    {
        return new DealIndex(collect(), collect(), collect(), collect(), collect());
    }
}
