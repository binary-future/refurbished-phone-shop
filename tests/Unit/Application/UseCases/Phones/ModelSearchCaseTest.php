<?php


namespace Tests\Unit\Application\UseCases\Phones;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Phones\ModelSearchCase;
use App\Application\UseCases\Phones\Requests\ModelSearchRequest;
use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\SearchQuery;
use App\Utils\Serializer\Contracts\Serializer;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class ModelSearchCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $serializer;

    private $translator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->serializer = $this->getBaseMock(Serializer::class);
        $this->translator = $this->getBaseMock(Translator::class);
    }

    private function getCase()
    {
        return new ModelSearchCase($this->queryBus, $this->serializer, $this->translator);
    }

    public function testExecution()
    {
        $request = new ModelSearchRequest();
        $request->setName('Name');
        $models = collect([new PhoneModel()]);
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn($models);
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new SearchQuery());
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertEquals($models, $response->getModels());
    }

    public function testSerializerExceptionReaction()
    {
        $request = new ModelSearchRequest();
        $request->setName('Name');
        $this->queryBus->expects($this->never())
            ->method('dispatch');
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->will($this->throwException(new \Exception()));
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertTrue($response->getModels()->isEmpty());
    }

    public function testBusExceptionReaction()
    {
        $request = new ModelSearchRequest();
        $request->setName('Name');
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->will($this->throwException(new \Exception()));
        $this->serializer->expects($this->once())
            ->method('fromArray')
            ->willReturn(new SearchQuery());
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertTrue($response->getModels()->isEmpty());
    }

    public function testIfEmptySearchParams()
    {
        $request = new ModelSearchRequest();
        $this->queryBus->expects($this->never())
            ->method('dispatch');
        $this->serializer->expects($this->never())
            ->method('fromArray');
        $case = $this->getCase();
        $response = $case->execute($request);
        $this->assertTrue($response->getModels()->isEmpty());
    }
}
