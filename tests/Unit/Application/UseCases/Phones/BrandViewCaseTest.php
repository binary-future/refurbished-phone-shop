<?php

namespace Tests\Unit\Application\UseCases\Phones;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Contracts\DealsSearcher;
use App\Application\Services\Search\Filters\BrandDealsAdjustedFilters;
use App\Application\Services\Search\Filters\Contracts\FilterInitializer;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\DealIndex;
use App\Application\UseCases\Phones\BrandViewCase;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Network;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class BrandViewCaseTest extends TestCase
{
    use BaseMock;

    private $queryBus;

    private $filtersSearcher;

    private $dealsSearcher;
    private $filterInitializer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->queryBus = $this->getBaseMock(QueryBus::class);
        $this->filtersSearcher = $this->getBaseMock(FiltersSearcher::class);
        $this->dealsSearcher = $this->getBaseMock(DealsSearcher::class);
        $this->filterInitializer = $this->getBaseMock(FilterInitializer::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->queryBus,
            $this->filtersSearcher,
            $this->dealsSearcher,
            $this->filterInitializer,
        );
        parent::tearDown();
    }

    private function getUseCase(): BrandViewCase
    {
        return new BrandViewCase(
            $this->queryBus,
            $this->filtersSearcher,
            $this->dealsSearcher,
            $this->filterInitializer,
        );
    }

    private function getDealIndex(
        array $products = [],
        array $networks = [],
        array $conditions = [],
        array $monthlyCosts = [],
        array $totalCosts = []
    ): DealIndex {
        return new DealIndex(
            collect($products),
            collect($networks),
            collect($conditions),
            collect($monthlyCosts),
            collect($totalCosts),
        );
    }

    public function testCaseThrowsExceptionIfBrandNotFound(): void
    {
        $this->queryBus->expects($this->once())
            ->method('dispatch')
            ->willReturn(collect([]));
        $case = $this->getUseCase();

        $this->expectException(ModelNotFoundException::class);

        $case->execute('Slug');
    }

    public function testCaseThrowsExceptionIfModelNotFound(): void
    {
        $brand = new Brand();
        $brand->id = 1;
        $enabledFilters = $this->getDealIndex();
        $this->queryBus->expects($this->exactly(4))
            ->method('dispatch')
            ->will($this->onConsecutiveCalls(collect([$brand]), collect(), collect(), collect()));
        $this->filtersSearcher->expects($this->once())
            ->method('generate')
            ->willReturn($enabledFilters);
        $case = $this->getUseCase();

        $response = $case->execute('Slug');

        $this->assertEquals($brand, $response->getBrand());
        $this->assertTrue($response->getModels()->isEmpty());
    }

    /**
     * @param Collection $brands
     * @param Collection $models
     * @param Collection $networks
     * @param Collection $conditions
     * @dataProvider validDataProvider
     */
    public function testCaseReturnsResponse(
        Collection $brands,
        Collection $models,
        Collection $networks,
        Collection $conditions
    ) {
        $this->queryBus->expects($this->exactly(4))
            ->method('dispatch')
            ->willReturnOnConsecutiveCalls(
                $brands,
                $models,
                $networks,
                $conditions
            );
        $this->filterInitializer->expects($this->once())
            ->method('init')
            ->willReturn(new BrandDealsAdjustedFilters());
        $enabledFilters = $this->getDealIndex();
        $this->filtersSearcher->expects($this->once())
            ->method('generate')
            ->willReturn($enabledFilters);
        $this->dealsSearcher->expects($this->once())->method('searchPaginated')
            ->willReturn($this->getBaseMock(Paginator::class));
        $case = $this->getUseCase();

        $response = $case->execute('slug');

        $this->assertEquals($brands->first(), $response->getBrand());
        $this->assertEquals($models, $response->getModels());
        $this->assertEquals($enabledFilters, $response->getEnabledFilters());
        $this->assertEquals($networks, $response->getAllNetworks());
        $this->assertEquals($conditions, $response->getAllConditions());
    }

    public function validDataProvider()
    {
        $brand = new Brand();
        $brand->id = 1;
        $model = new PhoneModel();
        $model->setRelation(PhoneModel::RELATION_PHONES, collect([new Phone()]));
        $network = new Network();
        $condition = Condition::PRISTINE()->getValue();

        return [
            [collect([$brand]), collect([$model]), collect([$network]), collect([$condition])],
        ];
    }
}
