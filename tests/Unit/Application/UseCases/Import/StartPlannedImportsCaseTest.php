<?php


namespace Tests\Unit\Application\UseCases\Import;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\Services\Importer\Response\ImportResponse;
use App\Application\UseCases\Import\Contracts\PhonesImportCase;
use App\Application\UseCases\Import\StartPlannedImportsCase;
use App\Domain\Store\Store;
use Tests\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StartPlannedImportsCaseTest extends TestCase
{
    use BaseMock;

    private $plannedImports;

    private $importCase;

    /**
     * @var StartPlannedImportsCase
     */
    private $case;

    protected function setUp(): void
    {
        parent::setUp();
        $this->plannedImports = $this->getBaseMock(PlannedImports::class);
        $this->importCase = $this->getBaseMock(PhonesImportCase::class);
        $this->case = new StartPlannedImportsCase($this->plannedImports, $this->importCase);
    }

    public function testExecutionIfNoImportsArePlanned()
    {
        $this->plannedImports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect());
        $this->importCase->expects($this->never())
            ->method('execute');
        $response = $this->case->execute();
        $this->assertFalse($response->isSuccess());
    }

    /**
     * @param array $response
     * @param string $message
     * @dataProvider failedImportResponseDataProvider
     */
    public function testExecutionIfImportFailed(array $response, string $message)
    {
        $plannedImport = new PlannedImport();
        $this->plannedImports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$plannedImport]));
        $this->importCase->expects($this->once())
            ->method('execute')
            ->willReturn($response);
        $plannedImport->changeStatus(PlannedImport::STATUS_NOT_PASSED);
        $this->plannedImports->expects($this->once())
            ->method('save')
            ->with($plannedImport)
            ->willReturn($plannedImport);

        $result = $this->case->execute();
        $this->assertFalse($result->isSuccess());
        $this->assertEquals($message, $result->getMessage());
    }

    public function failedImportResponseDataProvider()
    {
        $message = 'failed message';

        return [
            [[$this->generateFailedImportResponse($message)], $message],
            [[$this->generatePassedImportResponse(), $this->generateFailedImportResponse($message)], $message],
        ];
    }

    /**
     * @param array $response
     * @dataProvider validImportResponseDataProvider
     */
    public function testExecutionIfImportPassed(array $response)
    {
        $plannedImport = new PlannedImport();
        $this->plannedImports->expects($this->once())
            ->method('findBy')
            ->willReturn(collect([$plannedImport]));
        $this->importCase->expects($this->once())
            ->method('execute')
            ->willReturn($response);
        $plannedImport->changeStatus(PlannedImport::STATUS_PASSED);
        $this->plannedImports->expects($this->once())
            ->method('save')
            ->with($plannedImport)
            ->willReturn($plannedImport);

        $result = $this->case->execute();
        $this->assertTrue($result->isSuccess());
        $this->assertNull($result->getMessage());
    }

    public function validImportResponseDataProvider()
    {
        return [
            [[$this->generatePassedImportResponse()]],
            [[$this->generatePassedImportResponse(), $this->generatePassedImportResponse()]],
        ];
    }

    private function generateFailedImportResponse(string $message = null): array
    {
        $response = new ImportResponse();
        $response->setSuccess(false);
        $response->add('message', $message);

        return [
            'store' => new Store(),
            'response' => $response
        ];
    }

    private function generatePassedImportResponse(): array
    {
        $response = new ImportResponse();
        $response->setSuccess(true);

        return [
            'store' => new Store(),
            'response' => $response
        ];
    }
}
