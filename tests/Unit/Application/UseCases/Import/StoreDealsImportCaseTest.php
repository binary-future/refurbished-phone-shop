<?php

namespace Tests\Unit\Application\UseCases\Import;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Selectors\ImportScenarioSelector;
use App\Application\Services\Importer\Handler;
use App\Application\Services\Importer\HandlerFactory;
use App\Application\Services\Importer\Reports\Generators\StoreReportGenerator;
use App\Application\Services\Importer\Response\ImportResponse;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\UseCases\Import\StoreDealsImportCase;
use App\Domain\Store\Store;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Traits\BaseMock;

final class StoreDealsImportCaseTest extends TestCase
{
    use BaseMock;

    private $importScenarioSelector;
    private $storeReportGenerator;
    private $factory;
    private $channelLogger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->importScenarioSelector = $this->getBaseMock(ImportScenarioSelector::class);
        $this->storeReportGenerator = $this->getBaseMock(StoreReportGenerator::class);
        $this->factory = $this->getBaseMock(HandlerFactory::class);
        $this->channelLogger = $this->getBaseMock(ChannelLogger::class);
    }

    private function getCase(): StoreDealsImportCase
    {
        return new StoreDealsImportCase(
            $this->importScenarioSelector,
            $this->storeReportGenerator,
            $this->factory,
            $this->channelLogger
        );
    }

    private function getScenario()
    {
        return $this->getBaseMock(Scenario::class);
    }

    private function getHandler()
    {
        return $this->getBaseMock(Handler::class);
    }

    private function getDatafeedInfo()
    {
        return $this->getBaseMock(DatafeedInfo::class);
    }

    public function testPositiveExecute()
    {
        $response = new ImportResponse();
        $store = new Store();
        $datafeed = $this->getDatafeedInfo();
        $datafeed->expects($this->once())->method('getStore')
            ->willReturn($store);
        $scenario = $this->getScenario();
        $handler = $this->getHandler();
        $this->importScenarioSelector->expects($this->once())->method('select')
            ->willReturn($scenario);
        $this->factory->expects($this->once())->method('buildHandler')
            ->willReturn($handler);
        $handler->expects($this->once())->method('handleImport')
            ->willReturn($response);
        $this->storeReportGenerator->expects($this->once())->method('generate')
            ->with($response, $store);
        $case = $this->getCase();

        $actualResponse = $case->execute($datafeed);

        $this->assertEquals($response, $actualResponse);
    }

    public function testExecuteIfScenarioIsNull()
    {
        $datafeed = $this->getDatafeedInfo();
        $this->importScenarioSelector->expects($this->once())->method('select')
            ->willReturn(null);
        $case = $this->getCase();

        $actualResponse = $case->execute($datafeed);

        $this->assertEquals(false, $actualResponse->isSuccess());
        $this->assertIsString($actualResponse->get('error'));
    }

    public function testExecuteIfFactoryThrowsException()
    {
        $datafeed = $this->getDatafeedInfo();
        $scenario = $this->getScenario();
        $this->importScenarioSelector->expects($this->once())->method('select')
            ->willReturn($scenario);
        $this->factory->expects($this->once())->method('buildHandler')
            ->willThrowException(new ImportHandlerBuildException('message'));
        $case = $this->getCase();

        $actualResponse = $case->execute($datafeed);

        $this->assertEquals(false, $actualResponse->isSuccess());
        $this->assertIsString($actualResponse->get('error'));
    }

    public function testExecuteIfStoreReportGeneratorThrowsException()
    {
        $response = new ImportResponse();
        $store = new Store();
        $datafeed = $this->getDatafeedInfo();
        $datafeed->expects($this->once())->method('getStore')
            ->willReturn($store);
        $scenario = $this->getScenario();
        $handler = $this->getHandler();
        $this->importScenarioSelector->expects($this->once())->method('select')
            ->willReturn($scenario);
        $this->factory->expects($this->once())->method('buildHandler')
            ->willReturn($handler);
        $handler->expects($this->once())->method('handleImport')
            ->willReturn($response);
        $this->storeReportGenerator->expects($this->once())->method('generate')
            ->willThrowException(new \Exception('exception'));
        $case = $this->getCase();

        $actualResponse = $case->execute($datafeed);

        $this->assertEquals($response, $actualResponse);
    }

    protected function tearDown(): void
    {
        unset(
            $this->importScenarioSelector,
            $this->storeReportGenerator,
            $this->factory,
            $this->channelLogger
        );

        parent::tearDown();
    }
}
