/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    require('bootstrap');
    window.datepicker = require('bootstrap-datepicker');
    window.sortable = require('jquery-ui-sortable-npm');
    let dt = require( 'datatables.net' )(window, $);
} catch (e) {}
