require('./bootstrap');

require('bootstrap/js/dist/util');
require('bootstrap4-toggle');

require('./components/alert-message');
require('./components/logo-upload');
require('./components/delete-modal');
require('./components/report-highcharts');
require('./components/model-list-table');
require('./components/file-upload');
require('./components/phone-gallery');
require('./components/editor');
require('./components/faqs');
require('./components/date-input');
require('./components/datetimepicker-input');
require('./components/planned-import-update');
require('./components/merge-tool');
require('./components/model-synonyms');
require('./components/top-models-columns');
