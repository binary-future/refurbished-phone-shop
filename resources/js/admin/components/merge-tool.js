/*________________________________________________________________________________
 *
 * MERGE TOOL SCRIPTS
 * -------------------------------------------------------------------------------
 *
 * Methods for saving synonyms
 *________________________________________________________________________________
 */

/**
 * Current function is used for saving the synonyms for one group
 */
$('.save-group').on('click', function () {
    savingProcessTogle();
    let group = getGroup(this);
    hideDropdown(group);
    let result = collectDataByGroup(group);
    if (! result || result.length === 0) {
        nothingToSaveToggle();
        return false;
    }

    const token = getToken();
    const url = getUrl();
    hideDropdown(group);
    const sendData = $.param({
        '_token' : token,
        'models' : {
            0 : result
        }
    });
    sendRequest(sendData, url);
});

/**
 * Current function is used for saving all synonyms table state
 */
$('.save-all').on('click', function () {
    savingProcessTogle();
    const groups = document.getElementsByClassName('save-group');
    let result = [];
    for (let i = 0; i < groups.length; i++) {
        let group = getGroup(groups.item(i));
        let data = collectDataByGroup(group);
        if (data) {
            result.push(data);
        }
    }

    const token = getToken();
    const url = getUrl();
    const sendData = $.param({
        '_token' : token,
        'models' : result
    });
    sendRequest(sendData, url);
});

/**
 * Takes group info from given element
 *
 * @param element
 * @returns {*}
 */
function getGroup(element) {
    return $(element).data('group');
};

/**
 * Collects synonym and base group connection info
 * by group
 *
 * @param group
 * @returns {*}
 */
function collectDataByGroup(group) {
    const baseId = getBaseModelId(group);
    if (! baseId) {
        return false;
    }
    let synonyms = getSynonymsIds(group);

    return {
        'parent' : baseId,
        'synonyms' : JSON.stringify(synonyms)
    };
};

/**
 * Collect base model id of given group
 *
 * @param group
 * @returns {jQuery}
 */
function getBaseModelId(group) {
    const list = getBaseModelList(group);
    let modelItem = getModelElements(list).item(0);

    return getModelId(modelItem);
};

/**
 * Collect synonyms ids of given group
 *
 * @param group
 * @returns {Array}
 */
function getSynonymsIds(group) {
    const list = getSynonymsList(group);
    const models = getModelElements(list);
    let ids = [];
    for(let i = 0; i < models.length; i++) {
        let modelItem = models.item(i);
        ids[i] = getModelId(modelItem);
    }

    return ids;
};

/**
 * Collect model id from item
 *
 * @param modelItem
 * @returns {jQuery}
 */
function getModelId(modelItem) {
    return $(modelItem).data('id')
};

/**
 * Returns base model list
 *
 * @param group
 * @returns {Node}
 */
function getBaseModelList(group) {
    return document.getElementsByClassName('base-' + group).item(0);
};

/**
 * Returns synonyms list
 *
 * @param group
 * @returns {Node}
 */
function getSynonymsList(group) {
    return document.getElementsByClassName('synonyms-' + group).item(0);
};

/**
 * Returns collection of model items elements
 *
 * @param list
 * @returns {NodeList|*}
 */
function getModelElements(list) {
    return list.getElementsByClassName('model-item');
};

/**
 * Returns url for request sending
 *
 * @returns {jQuery}
 */
function getUrl() {
    return $('#merge-table').data('href');
};

/**
 * Returns token value
 *
 * @returns {jQuery}
 */
function getToken() {
    return $('#merge-table').data('token');
};

/**
 * Sends request for saving
 *
 * @param data
 * @param url
 */
function sendRequest(data, url) {
    $.ajax({
        data: data,
        type: 'POST',
        url: url,
        success: function (data) {
            location.reload();
        },
        error: function(data) {
            $('#error').html('<span><strong>Error!</strong></span>');
            $('#error').toggle();
        }
    });
};

/*______________________________________________________________________________
 *
 * DRAG AND DROP SCRIPT
 *______________________________________________________________________________
 */
$(document).ready(function () {
    $("ul.base-model-sortable").sortable({
        connectWith: [".base-model-sortable", '.potential-synonyms-sortable'],
        containment: "body",
        scroll: true,
        scrollSensitivity: 100,
        scrollSpeed: 100,
        helper: "clone",
        cursor: "move",
        items: "> li:not(:first)",
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
        zIndex: 99999,
        receive: function (event, ui) {
        }
    });

    $("ul.potential-synonyms-sortable").sortable({
        connectWith: [".base-model-sortable", '.potential-synonyms-sortable', '.not-filtered-sortable'],
        containment: "body",
        scroll: true,
        scrollSensitivity: 100,
        scrollSpeed: 100,
        helper: "clone",
        cursor: "move",
        items: "> li:not(:first)",
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
        zIndex: 99999,
        receive: function (event, ui) {
        }
    });

    $("ul.not-filtered-sortable").sortable({
        connectWith: [".base-model-sortable", '.potential-synonyms-sortable', '.not-filtered-sortable'],
        containment: "body",
        scroll: true,
        scrollSensitivity: 100,
        scrollSpeed: 100,
        helper: "clone",
        cursor: "move",
        items: "> li:not(:first)",
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
        },
        zIndex: 99999,
        receive: function (event, ui) {
        }
    });
});

/*______________________________________________________________________________
 *
 * MODAL WINDOWS SCRIPTS
 *______________________________________________________________________________
 */

/**
 * Add filter children button
 */
$(document).on("click", ".add-children", function () {
    let parent = $(this).data('parent');
    $("#filterId").val('');
    $("#filter-name").val('');
    $("#parentId").val(parent);
});

/**
 * Filter data editing
 */
$(document).on("click", ".edit-filter", function () {
    let filter = $(this).data('filter');
    let name = $(this).data('name');
    $("#parentId").val('');
    $("#filterId").val(filter);
    $("#filter-name").val(name);
});

$('.add-new-filter').on('click', function () {
    $("#parentId").val('');
    $("#filterId").val('');
});

/**
 * Hides the dropdown filter menu
 *
 * @param group
 * @returns {boolean}
 */
function hideDropdown(group) {
    $('#menu-' + group).dropdown("toggle");

    return false;
};

/**
 * Toggle error message
 */
function nothingToSaveToggle() {
    $('#error').html('<span><strong>Nothing to save</strong></span>');
    $('#error').toggle();
};

/**
 * Hide the error message by clicking
 */
$('#error').click(function () {
    nothingToSaveToggle();
});

/**
 * Hide the warning message by clicking
 */
$('#saving-process').click(function () {
    savingProcessTogle();
});

/**
 * Toggle warning message
 */
function savingProcessTogle() {
    $('#saving-process').toggle();
};

$("#unknown-filter").on("keyup", function() {
    let value = $(this).val().toLowerCase();
    $("#unknown-list li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});

$('#confirm-unmerge').on('show.bs.modal', function (e) {
    $(this).find('.unmerge-modal-form').attr('action', $(e.relatedTarget).data('href'));
    $(this).find('#unmerge-model-name').text($(e.relatedTarget).data('model-name'));
});
