const Highcharts = require('highcharts');

require('highcharts/modules/exporting')(Highcharts);

$(document).ready(function () {
    let charts = $('.reports-graph');
    for (let i = 0; i < charts.length; i++) {
        let type = $(charts[i]).data('type');
        generateChartsByType(type);
    }
    return false;

});

function generateChartsByType(type) {
    let url = getRequestUrl(type);
    if (! url) {
        return false;
    }
    showLoading(type);
    getReportsData(url, type);
}

function getRequestUrl(type) {
    return $('#reports-graph-' + type).data('href');
}

function getReportsData(url, type) {
    $.ajax({
        type: 'get',
        url: url,
        success: function (data) {
            hideLoading(type);
            Highcharts.chart('reports-graph-' + type, data.charts);
        },
        error: function () {
            hideLoading(type);
            return false;
        }
    });
}

function showLoading(type) {
    $('#reports-graph' + type).addClass('loading');
    $('.loading-bar-' + type).show();
}

function hideLoading(type) {
    $('#reports-graph-' + type).removeClass('loading');
    $('.loading-bar-' + type).hide();
}