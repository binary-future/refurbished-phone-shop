require('bootstrap/js/dist/toast');

/**
 * @param title
 * @param message
 * @param statusClass
 *  - primary
 *  - secondary
 *  - success
 *  - danger
 *  - warning
 *  - info
 *  - light
 *  - dark
 */
export default function doToast(title, message, statusClass) {
    const toast = '<div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true">'
            + '<div class="toast-header">'
            + '<strong class="mr-auto">' + title + '</strong>'
            + '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">'
            + '<span aria-hidden="true">×</span>'
            + '</button>'
            + '</div>'
            + '<div class="toast-body alert-' + statusClass + '">'
            + message
            + '</div>'
            + '</div>';
    $('.toast-list').append(toast);

    $('.toast').toast();
}
