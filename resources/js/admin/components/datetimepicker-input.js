moment = require('moment');
require("tempusdominus-bootstrap-4");

$('.datetimepicker-group').each(function (i, el) {
    const serverDate = new Date(new Date().toLocaleString("en-UK", {timeZone: onecompare.timezone}));
    $(el).datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        stepping: 10,
        minDate: serverDate,
        defaultDate: serverDate,
        icons: {
            time: 'fad fa-clock',
            date: 'fad fa-calendar',
            up: 'fad fa-arrow-up',
            down: 'fad fa-arrow-down',
            previous: 'fad fa-chevron-left',
            next: 'fad fa-chevron-right',
            today: 'fad fa-calendar-check-o',
            clear: 'fad fa-trash',
            close: 'fad fa-times'
        },
        timeZone: onecompare.timezone
    });
});
