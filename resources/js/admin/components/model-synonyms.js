$("ul.synonyms-model").sortable({
    connectWith: [".potential-synonyms"],
    containment: "body",
    scroll: true,
    scrollSensitivity: 100,
    scrollSpeed: 100,
    helper: "clone",
    cursor: "move",
    items: "> li:not(:first)",
    start: function(e, ui){
        ui.placeholder.height(ui.item.height());
    },
    zIndex: 99999,
    receive: function (event, ui) {
        const url = $('#synonyms').attr('data-href');
        const modelId = $(ui.item).attr('data-id');
        sendSynonymsRequest(url, modelId);
    }
});

$("ul.potential-synonyms").sortable({
    connectWith: [".synonyms-model"],
    containment: "body",
    scroll: true,
    scrollSensitivity: 100,
    scrollSpeed: 100,
    helper: "clone",
    cursor: "move",
    items: "> li:not(:first)",
    start: function(e, ui){
        ui.placeholder.height(ui.item.height());
    },
    zIndex: 99999,
    receive: function (event, ui) {
        const url = $('#potential-synonyms').attr('data-href');
        const modelId = $(ui.item).attr('data-id');
        sendSynonymsRequest(url, modelId);
    }
});

function sendSynonymsRequest(url, modelId, reload = false)
{
    $('#updating').show();
    $('#error').hide();
    $('#success').hide();
    const _token = $('#synonyms').attr('data-token');
    const sendData = $.param({
        '_token' : _token,
        'synonym' : modelId
    });

    $.ajax({
        data: sendData,
        type: 'POST',
        url: url,
        success: function (data) {
            $('#updating').hide();
            let message = '<span><strong>' + data.message + '</strong></span>';
            if(data.status == 'success') {
                $('#success').html(message);
                $('#success').show();
            } else {
                $('#error').html(message);
                $('#error').show();
            }
        },
        error: function(data) {
            $('#updating').hide();
            $('#error').html('<span><strong>Error!</strong></span>');
            $('#error').show();
        }
    });

    if (reload) {
        setTimeout(() => location.reload(), 2000);
    }
}

let autocomplete = require('autocompleter');
if (document.getElementById('synonyms-search')) {
    autocomplete({
        onSelect: function(item) {
            console.log(1, item.id);
            const url = $('#synonyms').attr('data-href');
            sendSynonymsRequest(url, item.id, true);
        },
        input:  document.getElementById('synonyms-search'),
        minLength: 2,
        emptyMsg: 'We couldn\'t find any device(s) matching your search',
        render: function(item, currentValue) {
            let html = '<div class="container autocomplete-item-container p-2 border-bottom">' +
                '<div class="row">' +
                '<div class="col-2 pl-2 text-center border-right">' +
                '<img style="height: 60px;"' +
                ' class="img img-responsive center-block" src="' + item.image + '">' +
                '</div>' +
                '<div class="col-10 text-left font-weight-bold">' +
                item.name +
                '</div>' +
                '</div>' +
                '</div>';
            let div = $.parseHTML(html);
            return div[0];
        },
        fetch: function(text, callback) {
            text = text.toLowerCase();
            $.ajax({
                url: $('#synonyms-search').data('url'),
                type: "GET",
                dataType: "json",
                data: {name: text},
                success: function (data) {
                    let models = data.models;
                    if (! models) {
                        models = [];
                    }
                    callback(models);
                },
            });
        },
        debounceWaitMs: 200,
    });
}

$(document).on('submit', '#synonym-search-form', function (e) {
    e.preventDefault();
});