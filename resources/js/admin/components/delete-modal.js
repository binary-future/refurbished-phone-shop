$('#confirm-delete').on('show.bs.modal', function (e) {
    $(this).find('.delete-modal-form').attr('action', $(e.relatedTarget).data('href'));
});