import LazyLoad from "vanilla-lazyload";

const imagesLazyLoad = new LazyLoad({
    elements_selector: ".lazy",
    load_delay: 100 //adjust according to use case
});

export default imagesLazyLoad;
