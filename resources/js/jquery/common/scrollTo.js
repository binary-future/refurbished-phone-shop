export default function scrollTo(elem) {
    const position = $(elem).offset().top;

    $("body, html").animate({
        scrollTop: position
    });
}
