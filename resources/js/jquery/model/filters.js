import scrollTo from "../common/scrollTo";
import imagesLazyLoad from "../common/lazyload";
import * as pagination from "./filters/pagination";
import * as panel from "./filters/dealsPanel";
import * as dealsStorage from "./filters/dealsStorage";
import * as resetButtons from "./filters/resetButtons";
import * as loadingBar from "./filters/loadingBar";

const Switch = require('weatherstar-switch');

let activeFilters = [];
let originalValues = [];
let maxDataSelected = true;
let switches = [];

$('body').on('change', '.model-filter', modelFilterChanged);

function modelFilterChanged() {
    let checked = isChecked(this);
    let group = getFiltersGroup(this);
    if (!isMultiple(this)) {
        uncheckGroup(group);
    }

    if (checked) {
        switchCheckForDuplicates(this, true);
        pushFilterToState(this);
        pushSynonymSliderToState(this);
    } else {
        switchCheckForDuplicates(this, false);
        if (!isMultiple(this) || isLastMultiple(this)) {
            removeFilterFromState(this);
            removeFilterFromState(getFilterSynonym(this));
        }
    }
    uncheckFilters();
    proceedFiltering();
}

function refreshPagination() {
    pagination.reset();
}

function switchCheckForDuplicates(filter, value) {
    $('input[data-id="' + $(filter).attr('data-id') + '"]').each(function (key, item) {
        $(item).prop('checked', value);
    });
}

function isMultiple(filter) {
    return $(filter).attr('data-multiple') === 'true';
}

function isLastMultiple(filter) {
    let filters = $('input[name="' + $(filter).attr('name') + '"]');
    let filterId = $(filter).attr('data-id');
    let result = [];
    for (let i = 0; i < filters.length; i++) {
        let filterItem = filters[i];
        let filterItemId = $(filterItem).attr('data-id');
        if (isChecked(filterItem) && filterItemId !== filterId) {
            result.push(filterItemId);
        }
    }

    return uniqueArray(result).length === 0;
}

function removeFilterFromState(filter) {
    if (!activeFilters.length > 0 || !isPresentInState(filter)) {
        return false;
    }
    let index = getFilterIndex(filter);
    removeFromStateBeforeIndex(index);
}

function removeFromStateBeforeIndex(index) {
    if (activeFilters.length <= 1) {
        activeFilters = [];
        return false;
    }

    activeFilters = activeFilters.slice(0, index);
}

function getFiltersGroup(filter) {
    return $('input[name=' + $(filter).attr('name') + ']');
}

function isChecked(filter) {
    return $(filter).prop('checked');
}

function pushFilterToState(filter) {
    filter = isMobile(filter) ? getNonMobileFilter(filter) : filter;
    return shouldChangeState(filter) ? changeFilter(filter) : addFilter(filter);
}

function shouldChangeState(filter) {
    return activeFilters.length > 0 && isPresentInState(filter);
}

function isPresentInState(filter) {
    let index = getFilterIndex(filter);

    return index !== -1;
}

function getFilterIndex(filter) {
    return activeFilters.indexOf(parseFilterAttr(filter));
}

function parseFilterAttr(filter) {
    return $(filter).attr('name');
}

function changeFilter(filter) {
    let index = getFilterIndex(filter);
    activeFilters[index] = parseFilterAttr(filter);
    removeFromStateAfterIndex(index);
    let synonym = getFilterSynonym(filter);
    if (synonym) {
        addFilter(synonym);
    }
}

function removeFromStateAfterIndex(index) {
    activeFilters = activeFilters.slice(0, index + 1);
}

function addFilter(filter) {
    if (!isPresentInState(filter)) {
        activeFilters[activeFilters.length] = parseFilterAttr(filter);
    }
}

$('body').on('slideStart', '.slider-filter', function (e) {
    originalValues[$(this).attr('id')] = $(this).val();
});

$('body').on('slideStop', '.slider-filter', function (e) {
    if (!isSlider(this) || !isChanged(this)) {
        return false;
    }
    updateSlideLabel(this);
    pushFilterToState(this);
    pushSynonymRadioToState(this);
    updateSynonymSlider(this);
    uncheckFilters();
    proceedFiltering();
});

function isChanged(filter) {
    return originalValues[$(filter).attr('id')] !== $(filter).val();
}

function updateSlideLabel(filter) {

    let id = $(filter).attr('data-id');
    let values = uniqueArray($(filter).val().split(','));
    values = processValues(values, id);
    let value = values.length > 1 ? values.join(' - ') : values.shift();
    $('.' + id + '-value').each(function (key, item) {
        $(item).text(value);
    });

    manageMaxDataValueSelect(filter);
}

function uniqueArray(arr) {
    const distinct = (value, index, self) => {
        return self.indexOf(value) === index;
    };

    return arr.filter(distinct);
}

function processValues(values, id) {
    const valuesProcessor = {
        "monthly_cost_slider": function (values) {
            return values.map(function (value) {
                return '£' + value;
            });
        },
        "upfront_cost_slider": function (values) {
            return values.map(function (value) {
                return '£' + value;
            });
        },
        "data_slider": function (values) {
            let sliders = $('.slider-filter[data-id=data_slider]');
            let maxValue = sliders ? $(sliders[0]).attr('data-slider-max') : null;

            return values.map(function (value) {
                return maxValue === value ? 'Unltd' : value / 1000 + 'GB';
            });
        },
    };
    const processor = valuesProcessor[id];

    return processor ? processor(values) : values;
}

function initializeFilters() {
    $(".slider-filter").slider();
    initSwitches();
}

function pushSynonymSliderToState(filter) {
    let synonymName = addSliderSuffix(filter);
    let slider = $('input[name=' + synonymName + ']');
    if (slider.length > 0) {
        slider.each(function (key, item) {
            pushFilterToState(item);
            updateSlider(item, filter);
        });
    }
}

function updateSynonymSlider(slider) {
    let sliders = $('.slider-filter[data-id=' + $(slider).attr('data-id') + ']');
    sliders.each(function (key, item) {
        $(item).slider('setValue', $(slider).val().split(',').map(function (item) {
            return +item;
        }), false, false);
    });
}

function updateSlider(slider, filter) {
    let minValue = $(slider).attr('data-slider-min');
    let maxValue = $(slider).attr('data-slider-max');
    let value = $(filter).val();
    let values = [+minValue, +value];
    if ($(filter).hasClass('strict-filter')) {
        values = [+value, +value];
    } else if ($(filter).hasClass('range-min-filter')) {
        values = [+value, +maxValue];
    }
    $(slider).slider('setValue', values, false, false);
    updateSlideLabel(slider);
}

function pushSynonymRadioToState(filter) {
    let synonymName = removeSliderSuffix(filter);
    let synonymGroup = $('input[name=' + synonymName + ']');
    if (synonymGroup.length === 0) {
        return false;
    }
    pushFilterToState(synonymGroup[0]);
    uncheckGroup(synonymGroup);
    let maxFilterValue = getSliderMaxSelectedValue($(filter).val());
    let minFilterValue = getSliderMinSelectedValue($(filter).val());
    if (!maxFilterValue) {
        return false;
    }
    let synonym = $('input[name=' + synonymName + '][value=' + maxFilterValue + ']');
    if (synonym.length === 0 || ($(synonym[0]).hasClass('strict-filter') && maxFilterValue !== minFilterValue)) {
        return false;
    }
    synonym.each(function (key, item) {
        $(item).prop('checked', true);
    });
}

function getSliderMaxSelectedValue(value) {
    return value.split(',').pop();
}

function getSliderMinSelectedValue(value) {
    return value.split(',').shift();
}

function uncheckGroup(group) {
    for (let i = 0; i < group.length; i++) {
        let filter = group[i];
        $(filter).prop('checked', false);
    }
}

function getNonMobileFilter(filter) {
    const name = removeMobileSuffix(filter),
        dataId = $(filter).data('id');
    return $('input[name=' + name + '][data-id=' + dataId + ']');
}

function isSlider(filter) {
    return $(filter).attr('name').indexOf('slider') !== -1;
}

function isMobile(filter) {
    return $(filter).attr('name').indexOf('mb') !== -1;
}

function removeSliderSuffix(filter) {
    return $(filter).attr('name').replace('_slider', '');
}

function removeMobileSuffix(filter) {
    return $(filter).attr('name').replace('_mb', '');
}

function addSliderSuffix(filter) {
    return $(filter).attr('name') + '_slider';
}

function getFilterSynonym(filter) {
    let synonymName = isSlider(filter) ? removeSliderSuffix(filter) : addSliderSuffix(filter);

    return $('input[name=' + synonymName + ']')[0];
}

function manageMaxDataValueSelect(slider) {
    if (!shouldCheckMaxDataValue(slider)) {
        return false;
    }

    maxDataSelected = getSliderMaxSelectedValue($(slider).val()) == $(slider).attr('data-slider-max');
}

function shouldCheckMaxDataValue(slider) {
    return $(slider).attr('name') === 'data_slider';
}

function addSliderMaxFlag(url) {
    if (maxDataSelected) {
        url = url + '&max_data=true';
    }

    return url;
}

// Switch button initializing
function initSwitches() {
    const $checkboxSwitches = $('.checkbox-switch');

    switches = [];
    if ($checkboxSwitches.next('.switch').length === 0) {
        $checkboxSwitches.map((i, s) => {
            switches.push(new Switch(s, {
                disabled: $(s).attr('disabled'),
                onChange: (state) => {
                    switches.forEach((s) => {
                        state && !s.getChecked() && s.on()
                        || !state && s.getChecked() && s.off();
                    });
                }
            }));
        });
    }
}

$('body').on('click', '.switch', function () {
    modelFilterChanged.call($(this).prev('.checkbox-switch')[0]);
});

initSwitches();
resetButtons.init();
pagination.initButtons();

export default proceedFiltering;
