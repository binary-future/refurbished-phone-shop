const $panel = $("#model-deals");
const $noDeals = $(".no-deals");
const dealsList = '.deals-list';


export function showNoDeals() {
    $noDeals.removeClass('d-none');
}

export function hideNoDeals() {
    $noDeals.addClass('d-none');
}

/**
 * @param {string} dealsHtml
 */
export function appendDeals(...dealsHtml) {
    $panel.find(dealsList).append(...dealsHtml);
}

/**
 * @param {string} html
 */
export function rewriteDeals(html) {
    $panel.find(dealsList).html(html);
}

/**
 * @param {string} html
 */
export function renderPanelContent(html) {
    $panel.html(html);
}

/**
 * @returns {string}
 */
export function getEndpoint() {
    return $panel.data('href');
}

/**
 * @param {string} className
 */
export function addClass(className) {
    $panel.addClass(className);
}

/**
 * @param {string} className
 */
export function removeClass(className) {
    $panel.removeClass(className);
}

export default appendDeals;

