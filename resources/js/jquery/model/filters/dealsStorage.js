const storage = [];

/**
 * @param {number} count
 * @returns {*[]} Returns empty array storage has nothing
 */
export function shift(count = 10) {
    return storage.splice(0, count);
}

/**
 * @param {string} deals
 */
export function push(...deals) {
    storage.push(...deals);
}

/**
 * Flush storage
 */
export function flush() {
    storage.splice(0);
}

