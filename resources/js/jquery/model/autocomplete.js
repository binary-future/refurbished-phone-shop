let autocomplete = require('autocompleter');
if (document.getElementById('model-search')) {
    autocomplete({
        onSelect: function(item) {
            location.href = item.url;
        },
        input:  document.getElementById('model-search'),
        minLength: 2,
        emptyMsg: 'We couldn\'t find any device(s) matching your search',
        render: function(item, currentValue) {
            let html = '<div class="container autocomplete-item-container p-2 border-bottom">' +
                '<div class="row">' +
                '<div class="col-md-4 col-xl-2 pl-2 text-center border-right">' +
                '<img style="height: 60px;"' +
                ' class="img img-responsive center-block" src="' + item.image + '">' +
                '</div>' +
                '<div class="col-md-8 col-xl-10 text-left font-weight-bold">' +
                item.name +
                '</div>' +
                '</div>' +
                '</div>';
            let div = $.parseHTML(html);
            return div[0];
        },
        fetch: function(text, callback) {
            text = text.toLowerCase();
            $.ajax({
                url: $('#model-search').data('url'),
                type: "GET",
                dataType: "json",
                data: {name: text},
                success: function (data) {
                    let models = data.models;
                    if (! models) {
                        models = [];
                    }
                    callback(models);
                }

            });
        },
        debounceWaitMs: 200,
    });
}
