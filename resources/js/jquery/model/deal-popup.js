$('#deal-info-modal').on('show.bs.modal', function (e) {
    const $dealBtn = $(e.relatedTarget);
    const url = $dealBtn.data('href');
    $.get(url)
       .done(function (response) {
           $('#deal-content').html(response.html);
       });

    window.open($dealBtn.attr('href'), '_blank');
});
