<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Date</th>
        <th scope="col" class="text-center">Created</th>
        <th scope="col" class="text-center">Updated</th>
        <th scope="col" class="text-center">Not Changed</th>
        <th scope="col" class="text-center">Skipped</th>
        <th scope="col" class="text-center"><i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td class="text-center">{{ $report->created_at }}</td>
            <td class="text-center">{{ $report->getCreated() }}</td>
            <td class="text-center">{{ $report->getUpdated() }}</td>
            <td class="text-center">{{ $report->getNotChanged() }}</td>
            <td class="text-center">{{ $report->getSkipped() }}</td>
            <td class="text-center">
                @include('backend.components.buttons.delete',
                    [
                        'href' => route('admin.reports.single.positive.delete', ['store' => $store->getSlug(), 'report' => $report->getKey()]),
                        'hide' => true,
                        'outline' => true,
                    ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
