<table class="table table-hover mt-2">
    <thead class="bg-danger text-white">
    <tr>
        <th scope="col" class="text-center">Date</th>
        <th scope="col" class="text-center">Error Message</th>
        <th scope="col" class="text-center"><i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($reports as $report)
        <tr>
            <td class="text-left">{{ $report->created_at }}</td>
            <td class="text-left">
                {{ $report->getMessage() }}
            </td>
            <td class="text-center">
                @include('backend.components.buttons.delete',
                    [
                        'href' => route('admin.reports.single.negative.delete', ['store' => $store->getSlug(), 'report' => $report->getKey()]),
                        'hide' => true,
                        'outline' => true,
                    ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
