@extends('backend.layouts.app')

@section('title'){{ sprintf('%s Deals Import Positive Reports', $store->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $store->getName() }} Deals Import Positive Reports</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3><span class="font-weight-bold">{{ $store->getName() }}</span> {{ ucfirst($type) }} Import Positive Reports ({{ $reports->total() }})</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <a href="#" data-toggle="modal" data-target="#confirm-delete"
                   data-href="{{ route('admin.reports.positive.delete', ['store' => $store->getSlug(), 'type' => $type]) }}" class="btn btn-outline-danger mb-2"><i class="fad fa-trash"></i>
                    Delete all reports
                </a>
                @include('backend.reports.components.positive-reports-table', ['reports' => $reports])
            </div>
            <div class="col-12 justify-content-center d-flex">
                {{ $reports->links() }}
            </div>
            @include('backend.components.modals.delete-modal', ['target' => 'this data'])
        </div>
    </div>
@endsection
