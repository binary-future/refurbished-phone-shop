@extends('backend.layouts.app')

@section('title'){{ sprintf('Payment Methods: edit %s', $paymentMethod->getName()) }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item"><a href="{{ route('admin.payment-methods.main') }}">Payment Methods</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $paymentMethod->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        @include('backend.components.messages.message')
                        <h3 class="pull-left">Edit {{ $paymentMethod->getName() }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.payment-methods.components.payment-methods-edit-form', [
                    'paymentMethod' => $paymentMethod,
                 ])
            </div>
        </div>
    </div>
@endsection
