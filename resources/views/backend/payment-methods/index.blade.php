@extends('backend.layouts.app')

@section('title'){{ 'Payment Methods' }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Payment Methods</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Payment Methods list</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @if(Auth::user()->isAdmin())
                    @include('backend.components.buttons.add', ['href' => route('admin.payment-methods.create'), 'entity' => 'payment method'])
                @endif
                @include('backend.payment-methods.components.payment-methods-table', ['payment-methods' => $paymentMethods])
            </div>
            @if(Auth::user()->isAdmin())
                @include('backend.components.modals.delete-modal', ['target' => 'This payment method'])
            @endif
        </div>
    </div>
@endsection
