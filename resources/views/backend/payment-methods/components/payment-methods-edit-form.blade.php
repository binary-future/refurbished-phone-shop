<form method="POST" action="{{ route('admin.payment-methods.update', ['slug' => $paymentMethod->getSlug()]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="store_id" value="{{ $paymentMethod->getKey() }}">
    @if(Auth::user()->isAdmin())
        <div class="form-group row">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="name"
                       value="{{ $paymentMethod->getName() }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }} row">
            <label for="logo" class="col-md-4 control-label">Logo</label>
            <div class="col-md-8 row">
                <div class="col-10">
                    @if ($paymentMethod->getImage())
                        <img id="logo-image"
                             src="{{ asset($paymentMethod->getImage()->getPath()) }}"
                             class="img img-fluid" alt="{{ $paymentMethod->getName() }}">
                    @else
                        <img id="logo-image" src="{{ asset('images/no-image.jpg') }}"
                             class="img img-fluid" alt="{{ $paymentMethod->getName() }}">
                    @endif
                </div>
                <div class="col-2">
                    <label class="btn btn-primary">
                        Browse <input id="logo" type="file" class="form-control file" name="logo"
                                      style="display: none;">
                    </label>
                    <span class="image-name"></span>
                </div>
                @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @endif
    <div class="pre-post-process d-none"></div>
    @include('backend.components.buttons.update')
</form>
