<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Logo</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($paymentMethods as $paymentMethod)
        <tr>
            <td class="text-left">{{ $paymentMethod->getName() }}</td>
            <td class="text-center">
                @if($paymentMethod->getImage())
                    <img class="img img-thumbnail" style="width: 200px;" src="{{ asset($paymentMethod->getImage()->getPath()) }}" alt="{{ $paymentMethod->getName() }}">
                @else
                    <i class="fad fa-times text-danger"></i>
                @endif
            </td>
            <td class="text-center">
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.payment-methods.edit', ['paymentMethod' => $paymentMethod->getSlug()]),
                        'hide' => true
                    ]
                )
                @if(Auth::user()->isAdmin())
                    @include('backend.components.buttons.delete',
                    [
                        'href' => route('admin.payment-methods.delete', ['paymentMethod' => $paymentMethod->getSlug()]),
                        'hide' => true
                    ])
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
