<form method="POST" action="{{ route('admin.payment-methods.save') }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="name" class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="name"
                   placeholder="Enter payment method name" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }} row">
        <label for="logo" class="col-md-4 control-label">Logo</label>
        <div class="col-md-8 row">
            <div class="col-10">
                <img id="logo-image" src="{{ asset('images/no-image.jpg') }}"
                         class="img img-fluid" alt="image">
            </div>
            <div class="col-2">
                <label class="btn btn-primary">
                    Browse <input id="logo" type="file" class="form-control file" name="logo"
                                  style="display: none;">
                </label>
                <span class="image-name"></span>
            </div>
            @if ($errors->has('logo'))
                <span class="help-block">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="pre-post-process d-none"></div>
    @include('backend.components.buttons.create')
</form>
