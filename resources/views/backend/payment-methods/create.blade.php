@extends('backend.layouts.app')

@section('title'){{ 'Payment Methods: create' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.payment-methods.main') }}">Payment Methods</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add new Payment Method</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>Create new Payment Method</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.payment-methods.components.payment-methods-create-form')
            </div>
        </div>
    </div>
@endsection
