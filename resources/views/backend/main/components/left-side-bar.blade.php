<h3 class="text-light mt-5 h6 text-uppercase">Main</h3>
<div class="list-group mb-3">
	<a href="{{ route('admin.models.main') }}" class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold">Device Models</a>
    <a href="{{ route('admin.colors.main') }}" class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold">Device Colours</a>
    <a href="{{ route('admin.top-models.main') }}" class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold">Top Models</a>
    <a class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold" href="{{ route('admin.brands.main') }}">Brands</a>
    <a href="{{ route('admin.stores.main') }}" class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold">Stores <span class="badge badge-pill badge-success p-2">{{ $storesCount }}</span></a>
    <a href="{{ route('admin.networks.main') }}" class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold">Networks</a>
</div>

<h3 class="text-light mt-5 h6 text-uppercase">Advanced</h3>
<div class="list-group mb-3">
    <a class="list-group-item list-group-item-action list-group-item-light font-weight-bold" href="#" data-toggle="modal" data-target="#uploadForm"><i class="fad fa-upload"></i> Upload Files</a>
	<a href="{{ route('admin.users.main') }}" class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold">Users</a>
	<a class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold" href="{{ route('admin.import.model.main') }}">Import</a>
    <a class="justify-content-between d-flex list-group-item list-group-item-action list-group-item-light font-weight-bold" href="{{ route('admin.merge.main') }}">Merge</a>
</div>
