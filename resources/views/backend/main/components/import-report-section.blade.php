<div class="row">
    <div class="col-12 text-center mt-3">
        <h3 class="text-center">{{ ucfirst($type) }} import reports</h3><br/>
        <div id="reports-graph-{{ $type }}" class="reports-graph w-100" data-type="{{ $type }}" data-href="{{ route('admin.reports.all', ['type' => $type]) }}">
           @include('backend.components.bars.loading-bar', ['custom' => $type])
        </div>
    </div>
    <div class="col-12">
       @include('backend.main.components.import-reports-table', ['stores' => $stores, 'positiveReports' => $positiveReports, 'negativeReports' => $negativeReports, 'type' => $type])
    </div>
</div>