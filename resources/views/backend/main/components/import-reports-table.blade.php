<a href="#" data-toggle="modal" data-target="#confirm-delete"
   data-href="{{ route('admin.reports.all.delete', ['type' => $type]) }}" class="btn btn-outline-danger mb-2"><i class="fad fa-trash"></i>
    Delete all reports
</a>
<table class="table table-hover table-bordered text-center">
    <thead class="text-uppercase bg-light-blue">
    <tr>
        <th scope="col" class="text-center">Store name</th>
        <th scope="col" class="text-center">Last date</th>
        <th scope="col" class="text-center">Passed</th>
        <th scope="col" class="text-center">Failed</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($stores as $store)
        <tr>
            <td class="text-uppercase font-weight-bold">
                {{ $store->getName() }}
            </td>
            @if($positiveReports->has($store->getKey()) || $negativeReports->has($store->getKey()))
                <td>{{ $store->getLastParsingDate() }}</td>
                <td>
                    @if($positiveReports->has($store->getKey()) && $positiveReports->get($store->getKey())->isNotEmpty())
                        <a href="{{ route('admin.reports.positive.view', ['store' => $store->getSlug(), 'type' => $type]) }}"><span class="badge badge-pill badge-success p-2"><strong>{{ $positiveReports->get($store->getKey())->count() }}</strong></span></a>
                    @else
                        <i class="fad fa-times-circle text-danger fa-lg"></i>
                    @endif
                </td>
                <td>
                    @if($negativeReports->has($store->getKey()) && $negativeReports->get($store->getKey())->isNotEmpty())
                        <a href="{{ route('admin.reports.negative.view', ['store' => $store->getSlug(), 'type' => $type]) }}"><span class="badge badge-pill badge-danger p-2"><strong>{{ $negativeReports->get($store->getKey())->count() }}</strong></span></a>
                    @else
                        <i class="fad fa-times-circle text-danger fa-lg"></i>
                    @endif
                </td>
            @else
                <td class="text-uppercase" colspan="3">You don't have reports for "{{ $store->getName() }}" for the last day</td>
            @endif
            <td>
                <a class="btn btn-primary" href="{{ route('admin.reports.positive.view', ['store' => $store->getSlug(), 'type' => $type]) }}">
                    <i class="fad fa-eye fa-lg"></i> Passed
                </a>
                <a class="btn btn-secondary" href="{{ route('admin.reports.negative.view', ['store' => $store->getSlug(), 'type' => $type]) }}">
                    <i class="fad fa-eye fa-lg"></i> Failed
                </a>
                <a data-toggle="modal" data-target="#confirm-delete" href="#"
                   data-href="{{ route('admin.reports.by.store.delete', ['store' => $store->getSlug(), 'type' => $type]) }}" class="btn btn-outline-danger">
                    <i class="fad fa-trash fa-lg"></i> Clear all
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
