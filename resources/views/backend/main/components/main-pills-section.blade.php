<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Deals import</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Phones import</a>
    </li>
</ul>
<div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        @include('backend.main.components.import-report-section', [
        'stores' => $stores,
        'negativeReports' => $negativeReports,
        'positiveReports' => $positiveReports,
        'type' => \App\Application\Services\Importer\Reports\Contracts\ReportsTypes::DEALS
      ])
    </div>
    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
        <div class="w-100 border-bottom border-success">
            @include('backend.import.components.model.model-import-brief-section', [
                'inActiveModel' => $inActiveModel,
                'withoutDealsModels' => $withoutDealsModels,
                'plannedImports' => $plannedImports,
            ])
        </div>
        @include('backend.main.components.import-report-section', [
        'stores' => $stores,
        'negativeReports' => $phonesNegativeReports,
        'positiveReports' => $phonesPositiveReports,
        'type' => \App\Application\Services\Importer\Reports\Contracts\ReportsTypes::PHONES
      ])
    </div>
</div>