@extends('backend.layouts.app')

@section('title'){{ sprintf('Import management') }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item active" aria-current="page">Import</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>Phone models import management</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                @include('backend.import.components.model.model-import-brief-section', [
                'inActiveModel' => $inactive,
                'withoutDealsModels' => $withoutDeals,
                'plannedImports' => $plannedImport
            ])
            </div>
        </div>
    </div>
@endsection
