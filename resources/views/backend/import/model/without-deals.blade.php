@extends('backend.layouts.app')

@section('title'){{ sprintf('Models without deals list') }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.import.model.main') }}">Import</a></li>
    <li class="breadcrumb-item active" aria-current="page">Models without deals</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <h3>Active models without deals({{ $models->count() }})</h3>
            </div>
            <div class="col-12">
                <div class="row row-eq-height">
                    @if($models->isNotEmpty())
                        @include('backend.import.components.model.models-inactive-list-table', ['models' => $models])
                    @else
                        <div class="col-12 text-center">No models without deals were found</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
