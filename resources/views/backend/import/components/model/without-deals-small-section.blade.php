<h5 class="border-bottom border-success font-weight-bold text-center pb-2">Models without deals:</h5>
<div class="list-group">
    @foreach($models as $model)
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            {{ $model->getBrand()->getName() }} {{ $model->getName() }}
        </div>
    @endforeach
</div>
<div class="w-100 mt-2 justify-content-end d-flex">
    <a class="btn btn-outline-info" href="{{ route('admin.import.model.without.deals') }}">View more <i class="fad fa-eye"></i></a>
</div>
