<h5 class="font-weight-bold text-center border-bottom border-success pb-2">Last imported models:</h5>
<div class="list-group">
    @foreach($models as $model)
        <div class="list-group-item list-group-item-action flex-column align-items-start">
            {{ $model->getBrand()->getName() }} {{ $model->getName() }}
        </div>
    @endforeach
</div>
<div class="w-100 mt-2 justify-content-end d-flex">
    <a class="btn btn-outline-info" href="{{ route('admin.import.model.inactive') }}">View more <i class="fad fa-eye"></i></a>
</div>
