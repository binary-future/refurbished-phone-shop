<table class="table table-hover mt-2" id="model-list-table">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Image</th>
        <th scope="col" class="text-center">Phones count</th>
        <th scope="col" class="text-center">Imported</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        <tr>
            <td class="text-left font-weight-bold">
                <a class="text-dark" href="{{ route('admin.models.view', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()]) }}">
                    {{ $model->getBrand()->getName() }} {{ $model->getName() }}
                </a>
            </td>
            <td class="text-center w-25">
                <img alt="{{ $model->getName() }}" class="img img-thumbnail w-50" src="{{ asset($model->getImage() ? $model->getImage()->getPath() : 'images/no-image.jpg') }}">
            </td>
            <td class="text-center">
                <span class="badge badge-pill badge-primary p-2">{{ $model->getPhones()->count() }}</span>
            </td>
            <td class="text-center">
                {{ $model->getCreatedAt() }}
            </td>
            <td class="text-center">
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.models.edit', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()]),
                        'hide' => true,
                        'outline' => true,
                    ]
                )
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
