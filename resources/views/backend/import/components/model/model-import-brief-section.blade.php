<div class="row justify-content-around row-eq-height">
    <div class="col-3 my-2 p-2">
        @include('backend.import.components.model.inactive-small-section', ['models' => $inActiveModel])
    </div>
    <div class="col-3 my-2 p-2">
       @include('backend.import.components.model.without-deals-small-section', ['models' => $withoutDealsModels])
    </div>
    <div class="col-3 my-2 p-2">
        @include('backend.import.components.model.planned-import-small-section', ['list' => $plannedImports])
    </div>
</div>