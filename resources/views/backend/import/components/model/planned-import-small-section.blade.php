<h5 class="border-bottom border-success font-weight-bold text-center pb-2">Planned imports:</h5>
@forelse($list as $planned)
    <div class="list-group">
            <div class="list-group-item list-group-item-action justify-content-between d-flex align-items-start {{ $planned->isOverdue() ? 'bg-warning' : '' }}">
                <span>{{ $planned->getImportDate() }} </span><span class="badge badge-pill {{ $planned->isOverdue() ? 'badge-danger' : ($planned->isPassed() ? 'badge-success' : 'badge-primary')  }} p-2">
                    <i class="{{ $planned->isPassed() ? 'fad fa-check' : 'fad fa-times' }}"></i>
                </span>
            </div>
    </div>
@empty
    <div>No imports were planned</div>
@endforelse
<div class="w-100 mt-2 justify-content-end d-flex">
    <a class="btn btn-outline-info" href="{{ route('admin.import.planned.main') }}">View more <i class="fad fa-eye"></i></a>
</div>
