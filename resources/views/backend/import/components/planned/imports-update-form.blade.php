<div class="modal fade import-modal" id="update-form" tabindex="-1" role="dialog"
     aria-labelledby="uploadFormLabel" aria-hidden="true">
    <div class="modal-dialog" style="top: 25%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header d-flex justify-content-between">
                <h4 class="modal-title" id="uploadFormLabel">
                    Update planned import
                </h4>
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body" style="min-height: 250px;">
                <div class="w-100 text-right"></div>
                <form method="POST" class="update-modal-form" action="#">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <label for="update-date" class="col-md-4 control-label">Date</label>
                        <div class="col-md-8">
                            <div class="input-group date datetimepicker-group" id="datetimepicker-update" data-target-input="nearest">
                                <input type="text" id="update-date" name="date" class="form-control datetimepicker-input" data-target="#datetimepicker-update">
                                <div class="input-group-append" data-target="#datetimepicker-update" data-toggle="datetimepicker">
                                    <span class="input-group-text"><i class="fad fa-th"></i></span>
                                </div>
                            </div>
                            @if ($errors->has('date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    @include('backend.components.buttons.update')
                </form>
            </div>
        </div>
    </div>
</div>
