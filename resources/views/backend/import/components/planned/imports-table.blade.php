<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Date</th>
        <th scope="col" class="text-center">Status</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($imports as $import)
        <tr class="text-center {{ $import->isOverdue() ? 'bg-warning' : ''}}">
            <td class="text-left">{{ $import->getImportDate() }}</td>
            <td class="text-center">
                <span class="badge badge-pill {{ $import->isOverdue() ? 'badge-danger' : ($import->isPassed() ? 'badge-success' : 'badge-primary')  }} p-2">
                    <i class="{{ $import->isPassed() ? 'fad fa-check' : 'fad fa-times' }}"></i>
                </span>
            </td>
            <td class="text-center">
                @if($import->isPassed() && ! $import->isOverdue())
                    <button class="btn btn-outline-info" disabled
                    ><i class="fad fa-edit"></i></button>
                @else
                    <a class="btn btn-outline-info" href="#"
                       data-toggle="modal" data-target="#update-form"
                       data-href="{{ route('admin.import.planned.update', ['import' => $import->getKey()]) }}"
                       data-date = " {{ $import->getImportDate() }}"
                    ><i class="fad fa-edit"></i></a>
                @endif
                @include('backend.components.buttons.delete',
                [
                    'href' => route('admin.import.planned.delete', ['store' => $import->getKey()]),
                    'hide' => true,
                    'outline' => true
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
