@extends('backend.layouts.app')

@section('title'){{ 'Planned imports' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.import.model.main') }}">Import</a></li>
    <li class="breadcrumb-item active" aria-current="page">Planned imports</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        @if ($errors->has('date'))
                            <div class="alert alert-danger">
                                {{ $errors->first('date') }}
                            </div>
                        @endif
                        <h3>Planned imports list</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <a class="btn btn-primary" href="#"
                   data-toggle="modal" data-target="#uploadForm"><i class="fad fa-plus"></i> Plan new import</a>
                @include('backend.import.components.planned.imports-table', ['imports' => $imports])
            </div>
            @include('backend.import.components.planned.imports-create-form')
            @include('backend.import.components.planned.imports-update-form')
            @include('backend.components.modals.delete-modal', ['target' => 'This planned import'])
        </div>
    </div>
@endsection
