@extends('backend.layouts.app')

@section('title'){{ sprintf('Edit %s', $color->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.colors.main') }}">Colors</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $color->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>
                            Edit {{ $color->getName() }}
                            <a href="{{ route('admin.colors.synonyms', ['color' => $color->getSlug()]) }}" class="btn btn-outline-success {{ $color->isSynonym() ? 'disabled' : '' }}" title="Synonyms">
                                <i class="fad fa-user"></i>
                            </a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.color-edit-form', ['color' => $color])
            </div>
        </div>
    </div>
@endsection
