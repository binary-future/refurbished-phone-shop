@extends('backend.layouts.app')

@section('title'){{ sprintf('%s Synonyms', $color->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.colors.main') }}">Phones color</a></li>
    <li class="breadcrumb-item active">{{ $color->getName() }} Synonyms</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <div id="updating" class="text-center alert alert-warning" style="display: none">
                            <span><strong>Updating...</strong></span>
                        </div>
                        <div id="success" class="text-center alert alert-success" style="display: none">
                            <span><strong>Success!</strong></span>
                        </div>
                        <div id="error" class="text-center alert alert-danger" style="display: none">
                            <span><strong>Error!</strong></span>
                        </div>
                        <h4>
                            {{ $color->getName() }} Synonyms <span class="badge badge-pill badge-primary">{{ $synonyms->count() }}</span>
                            @include('backend.components.buttons.edit', ['href' => route('admin.colors.edit', ['color' => $color->getSlug()]), 'outline' => true, 'hide' => true])
                            <a href="{{ route('admin.merge.tool.color.main') }}" class="btn btn-outline-dark"><i class="fad fa-cogs"></i></a>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.color-synonyms-section', [
                    'color' => $color,
                    'colors' => $synonyms,
                    'potentialSynonyms' => $potentialSynonyms])
            </div>
        </div>
    </div>
@endsection
