@extends('backend.layouts.app')

@section('title'){{ 'Colors of Devices' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item active">Phones color</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h4>Colors List <span class="badge badge-pill badge-primary">{{ $colors->count() }}</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.color-table', ['colors' => $colors, 'inactiveColorsIds' => $inactiveColorsIds])
            </div>
        </div>
    </div>
@endsection
