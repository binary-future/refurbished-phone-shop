@extends('backend.layouts.app')

@section('title'){{ $model->getName() }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.list', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }} devices</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $model->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                @include('backend.components.messages.message')
                <div class="row">
                    <div class="col-4">
                        <img alt="{{ $model->getName() }}" class="img img-thumbnail" src="{{ asset($model->getImage() ? $model->getImage()->getPath() : 'images/no-image.jpg') }}">
                    </div>
                    <div class="col-8">
                        <h4>{{ $brand->getName() }}  {{ $model->getName() }}
                            @include('backend.components.buttons.edit', ['href' => route('admin.models.edit', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]), 'outline' => true, 'hide' => true])
                        </h4>
                        <div class="w-100">
                            @include('backend.phone.components.model-info', ['model' => $model, 'latestModel' => $latestModel])
                        </div>
                        <div class="w-100">
                            @include('backend.phone.components.model-color-list', ['colors' => $colors])
                        </div>
                        <div class="w-100 d-flex justify-content-end">
                            <a href="{{ route('models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}" class="btn btn-outline-primary">See on Site <i class="fad fa-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            @if(Auth::user()->isAdmin())
                <div class="col-12">
                    <h6 class="text-center font-weight-bold">Phones list</h6>
                        @include('backend.phone.components.phones-list', ['phones' => $phones])
                </div>
                @if ($model->hasSynonyms())
                    <div class="col-12">
                        <h6 class="text-center font-weight-bold">Synonyms Phones list</h6>
                        @include('backend.phone.components.phones-list', ['phones' => $synonymsPhones])
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
