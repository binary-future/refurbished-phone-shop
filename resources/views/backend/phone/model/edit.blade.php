@extends('backend.layouts.app', ['showEditor' => true])

@section('title'){{ sprintf('Edit %s %s', $brand->getName(), $model->getName()) }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.list', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }} devices</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">{{ $brand->getName() }} {{ $model->getName() }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Editing</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Edit {{ $brand->getName() }} {{ $model->getName() }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.model-edit-form', ['model' => $model, 'isLatestModel' => $isLatestModel, 'brand' => $brand])
            </div>
        </div>
    </div>
@endsection
