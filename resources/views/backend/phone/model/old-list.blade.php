@extends('backend.layouts.app')

@section('title'){{ "Old models list" }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h4>Choose model <span class="badge badge-pill badge-primary">{{ $models->count() }}</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <table class="table table-hover mt-2 model-list-table" id="model-list-table">
                    <thead class="bg-success text-white">
                    <tr>
                        <th scope="col" class="text-center">Name</th>
                        <th scope="col" class="text-center">Image</th>
                        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($models as $model)
                        <tr>
                            <td class="text-left font-weight-bold">
                                <a class="text-dark" href="{{ route('admin.models.view', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()]) }}">
                                    {{ $model->getBrand()->getName() }} {{ $model->getName() }}
                                </a>
                            </td>
                            <td class="text-center">
                                @if($model->getImage())
                                    <img class="img img-thumbnail" style="width: 200px;" src="{{ asset($model->getImage()->getPath()) }}" alt="{{ $model->getName() }}">
                                @else
                                    <i class="fad fa-times text-danger"></i>
                                @endif
                            </td>
                            <td class="text-center">
                                @include('backend.components.buttons.edit',
                                    [
                                        'href' => route('admin.models.edit', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()]),
                                        'hide' => true,
                                        'outline' => true,
                                    ]
                                )
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
