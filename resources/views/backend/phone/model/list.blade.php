@extends('backend.layouts.app')

@section('title'){{ $brand->getName() . ' Devices' }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $brand->getName() }} devices</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h4>Choose model <span class="badge badge-pill badge-primary">{{ $models->count() }}</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.model-list-table', ['models' => $models])
            </div>
        </div>
    </div>
@endsection
