@extends('backend.layouts.app')

@section('title'){{ 'Models search result' }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
    <li class="breadcrumb-item active" aria-current="page">Search result</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-8">
                        <h4>{{ $models->total() }} Devices were found <span class="badge badge-pill badge-primary"></span></h4>
                    </div>
                    <div class="col-4">
                        @include('backend.phone.components.model-search-form')
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.model-search-result', ['models' => $models])
            </div>
        </div>
    </div>
@endsection
