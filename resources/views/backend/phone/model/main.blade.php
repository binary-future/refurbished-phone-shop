@extends('backend.layouts.app')

@section('title'){{ 'Models brands list' }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Models search</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <h4>Choose device brand from list or search <span class="badge badge-pill badge-primary"></span></h4>
                    </div>
                    <div class="col-md-6 col-12">
                        @include('backend.phone.components.model-search-form')
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.model-brands-list', ['brands' => $brands])
            </div>
        </div>
    </div>
@endsection
