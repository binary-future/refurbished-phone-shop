@extends('backend.layouts.app')

@section('title'){{ $model->getName() }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.list', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }} devices</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">{{ $brand->getName() }} {{ $model->getName() }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">Synonyms</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($model->isSynonym())
                <div class="col-12 border-bottom border-success mb-3 pb-2">
                    <h3 class="text-center">You cannot add synonyms to {{ $brand->getName() }} {{ $model->getName() }}, because it is synonym itself! </h3>
                </div>
            @else
                <div class="col-12 border-bottom border-success mb-3 pb-2">
                    @include('backend.components.messages.message')
                    <div id="updating" class="text-center alert alert-warning" style="display: none">
                        <span><strong>Updating...</strong></span>
                    </div>
                    <div id="success" class="text-center alert alert-success" style="display: none">
                        <span><strong>Success!</strong></span>
                    </div>
                    <div id="error" class="text-center alert alert-danger" style="display: none">
                        <span><strong>Error!</strong></span>
                    </div>
                </div>
                <div class="col-12 mb-3 border-bottom border-success">
                    <h4 class="text-center"><strong>Synonyms of {{ $model->getName() }}:</strong>
                        <a class="btn btn-primary"
                           href="{{ route('admin.merge.tool.model.view', ['brand' => $brand->getSlug()]) }}">
                            <i class="fad fa-arrow-circle-left"></i> Merge Tool
                        </a>
                    </h4>
                </div>
                <div class="col-12 mb-3 justify-content-end d-flex">
                    <form id="synonym-search-form" class="form-inline" method="POST" action="{{ '#' }}">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input id="synonyms-search"
                                   data-url="{{ route('admin.models.autocomplete') }}"
                                   class="form-control"
                                   placeholder="Enter synonyms name"
                                   aria-label="Search"
                                   type="text"
                                   name="search"
                                   size="50"
                                   autocomplete="off"
                                   required
                                   >
                        </div>
                    </form>
                </div>
                <div class="col-12">
                    @include('backend.phone.components.model-synonyms-section', ['synonyms' => $synonyms, 'potentialSynonyms' => $potentialSynonyms])
                </div>
            @endif
        </div>
    </div>
@endsection
