<table class="table table-hover mt-2" id="color-list-table">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Hex</th>
        <th scope="col" class="text-center">Has active phones</th>
        <th scope="col" class="text-center">Is synonym?</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($colors as $color)
        <tr>
            <td class="text-left font-weight-bold {{ $inactiveColorsIds->contains($color->getKey()) ? 'text-muted' : '' }}">
                {{ $color->getName() }}
            </td>
            <td class="text-center">
                @if($color->getHex())
                    <span class="color-dot" style="background-color: {{ '#' . $color->getHex() }};"></span>
                @else
                    <i class="fad fa-times text-danger"></i>
                @endif
            </td>
            <td class="text-center">
                <i class="{{ $inactiveColorsIds->contains($color->getKey()) ? 'fad fa-times text-danger' : 'fad fa-check text-success' }}"></i>
            </td>
            <td class="text-center">
                <i class="{{ $color->isSynonym() ? 'fad fa-check text-success' : 'fad fa-times text-danger' }}"></i>
            </td>
            <td class="text-center">
                <a href="{{ route('admin.colors.synonyms', ['color' => $color->getSlug()]) }}" class="btn btn-outline-success {{ $color->isSynonym() ? 'disabled' : '' }}" title="Synonyms">
                    <i class="fad fa-user"></i>
                </a>
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.colors.edit', ['color' => $color->getSlug()]),
                        'hide' => true,
                        'outline' => true,
                    ]
                )
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
