<tr>
    <td class="text-left font-weight-bold">
        <a class="text-dark" href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">{{ $model->getName() }}</a>
    </td>
    <td class="text-center">
        <span class="badge badge-pill badge-primary p-2">{{ $model->getPhones()->count() }}</span>
        <span class="badge badge-pill badge-success p-2">{{ $model->getOriginalPhones()->count() }}</span>
    </td>
    <td class="text-center">
        <i class="fad {{ $latestModel ? 'fa-check text-success' : 'fa-times text-danger'}}">
            <span class="d-none">{{ (int) (bool) $latestModel }}</span>
        </i>
    </td>
    <td class="text-center">
        <i class="fad {{ $model->isActive() ? 'fa-check text-success' : 'fa-times text-danger'}}">
            <span class="d-none">{{ (int) $model->isActive() }}</span>
        </i>
    </td>
    <td class="text-center">
        <i class="fad {{ $model->isSynonym() ? 'fa-check text-success' : 'fa-times text-danger'}}">
            <span class="d-none">{{ (int) $model->isSynonym() }}</span>
        </i>
    </td>
    <td class="text-center">
        @include('backend.components.buttons.edit',
            [
                'href' => route('admin.models.edit', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]),
                'hide' => true,
                'outline' => true,
            ]
        )
    </td>
</tr>
