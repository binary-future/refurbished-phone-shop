<form method="POST" action="{{ route('admin.models.update', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="model_id" value="{{ $model->getKey() }}">
    <input type="hidden" name="brand_id" value="{{ $model->getBrandId() }}">
    @if(Auth::user()->isAdmin())
        <div class="form-group row">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="name"
                       value="{{ $model->getName() }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="is_active" class="col-md-4 control-label">Active</label>
            <div class="col-md-8">
                <select name="is_active" class="form-control custom-select" id="is_active">
                    <option {{ $model->isActive() ? 'selected' : '' }}
                            value="{{ \App\Domain\Phone\Model\PhoneModel::ACTIVE }}">Yes
                    </option>
                    <option {{ !$model->isActive() ? 'selected' : '' }}
                            value="{{ \App\Domain\Phone\Model\PhoneModel::IN_ACTIVE }}">No
                    </option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="is_latest" class="col-md-4 control-label">Latest</label>
            <div class="col-md-8">
                <select name="is_latest" class="form-control custom-select" id="is_latest">
                    <option {{ $isLatestModel ? 'selected' : '' }}
                            value="{{ App\Application\Services\Model\LatestModel\LatestModel::LATEST }}">Yes
                    </option>
                    <option {{ !$isLatestModel ? 'selected' : '' }}
                            value="{{ App\Application\Services\Model\LatestModel\LatestModel::NOT_LATEST }}">No
                    </option>
                </select>
            </div>
        </div>
    @endif
    <div class="form-group row">
        <label for="description" class="col-md-4 control-label">Description</label>
        <div class="col-md-8">
            <textarea id="description" type="text" class="tiny form-control" name="description[content]" data-url="{{ route('admin.editor.file.upload') }}">
                @if($model->getDescription())
                    {{ $model->getDescription()->getContent() }}
                @endif
            </textarea>
            @if ($errors->has('description.content'))
                <span class="help-block">
                    <strong>{{ $errors->first('description.content') }}</strong>
                </span>
            @endif
        </div>
    </div>
    @include('backend.phone.components.model-edit-faqs', ['model' => $model])
    <div class="pre-post-process d-none"></div>
    @include('backend.components.buttons.update')
</form>
