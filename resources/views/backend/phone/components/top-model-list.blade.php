<div class="row">
    <div class="offset-6 col-6">
        <label for="models-filter">Models filter: </label>
        <div class="input-group mb-3">
            <input type="text" class="form-control" id="models-filter">
        </div>
    </div>
    <div class="col-6 d-flex flex-column">
        <h3>Top models</h3>
        <ul id="topModels" class="connectedSortable list-group col drag-n-drop-col">
            @foreach($topModels as $model)
                @include('backend.phone.components.top-model-list-item', [
                    'topModelId' => $model->getKey(),
                    'model' => $model->getModel(),
                ])
            @endforeach
        </ul>
    </div>
    <div class="col-6 d-flex flex-column">
        <h3>Regular models</h3>
        <ul id="simpleModels" class="connectedSortable list-group col drag-n-drop-col">
            @foreach($models as $model)
                @include('backend.phone.components.top-model-list-item', [
                    'model' => $model,
                ])
            @endforeach
        </ul>
    </div>
</div>
