<table class="table table-hover mt-2 model-list-table" id="model-list-table">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Phones/Original count</th>
        <th scope="col" class="text-center">Is Latest?</th>
        <th scope="col" class="text-center">Is Active?</th>
        <th scope="col" class="text-center">Is Synonym?</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($models as $model)
        @include('backend.phone.components.model-list-item-table', ['model' => $model->getPhoneModel(), 'latestModel' => $model->getLatestModel()])
    @endforeach
    </tbody>
</table>
