<div class="col-1 p-1 align-self-center border-left text-center">
    <a href="{{ route('admin.colors.edit', ['color' => $color->getSlug()]) }}">
    @if($color->getHex())
        <span class="color-dot" style="background-color: {{ '#' . $color->getHex() }};"></span>

    @else
        <span class="text-center font-weight-light">{{ $color->getName() }}</span>
    @endif
    </a>
</div>
