<div class="col-md-12" style="display: flex;">
    <div style="display: flex; flex: 0 50%; width: 100%">
        <ul id='synonyms'
            data-token="{{ csrf_token() }}"
            data-href="{{ route('admin.colors.synonyms.add', ['color' => $color->getSlug()]) }}"
            class="list-group synonyms-model synonyms rounded"
            style="border: 1px dotted lightblue; width: 95%;">
            <li class="list-group-item synonyms clearfix list-group-item-success">
                <div class="col-md-8 col-md-offset-2">
                    <h4>Synonyms</h4>
                    <span class="align-middle">Drag potential synonym here</span>
                </div>
            </li>
            @foreach($synonyms as $synonym)
                <li data-id="{{ $synonym->getKey() }}"
                    class="synonyms list-group-item list-group-item-default clearfix"
                    style="cursor:pointer;"
                >
                    <span class="align-middle">{{ $synonym->getName() }}</span>
                </li>
            @endforeach
        </ul>
    </div>
    <div style="display: flex; flex: 0 50%; width: 100%">
        <ul class="list-group potential-synonyms"
            id="potential-synonyms"
            data-href="{{ route('admin.colors.synonyms.remove', ['color' => $color->getSlug()]) }}"
            style="border: 1px dotted lightblue; width: 95%;">
            <li class="list-group-item potential clearfix list-group-item-info">
                <div class="col-md-8 col-md-offset-2">
                    <h4>Potential synonyms</h4>
                    <span class="align-middle">Drag here to remove synonym</span>
                </div>
            </li>
            @foreach($potentialSynonyms as $synonym)
                <li data-id="{{ $synonym->getKey() }}" class="potential list-group-item list-group-item-default clearfix"
                    style="cursor:pointer;">
                    <span class="align-middle">{{ $synonym->getName() }}</span>
                </li>
            @endforeach
        </ul>
    </div>
</div>
