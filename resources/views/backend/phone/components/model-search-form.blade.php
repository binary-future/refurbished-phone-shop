<form class="form-inline col-12" method="GET" action="{{ route('admin.models.search') }}">
    <div class="input-group jumbotron-form col-9">
        <input class="form-control" name="name" type="text" placeholder="Type device name">
    </div>
    <button class="btn btn-secondary col-2 btn-lg input-group ml-1 my-sm-0" type="submit"><i class="fal fa-search"></i></button>
</form>
