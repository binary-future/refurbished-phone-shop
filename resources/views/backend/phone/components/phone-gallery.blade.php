<div class="w-100">
    <div class="row">
        @include('backend.components.bars.loading-bar')
        <div class="col-12">
            <h3 class="text-center">Gallery</h3>
        </div>
        <div class="col-12">
            <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#uploadImage"><i class="fad fa-upload"></i> Add image</a>
            <table class="table table-hover mt-2">
                <thead class="bg-success text-white">
                <tr>
                    <th scope="col" class="text-center">Image</th>
                    <th scope="col" class="text-center" id="is-main-col">Main?</th>
                    <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($phone->getImages() as $image)
                    <tr>
                        <td class="w-25">
                            <div class="w-50 shadow-sm border-success border p-2">
                                <img src="{{ asset($image->getPath()) }}" alt="image" class="img img-thumbnail">
                            </div>
                        </td>
                        <td class="text-center">
                            <input type="checkbox" {{ $image->isMain() ? 'checked' : '' }}
                                data-href="{{ route('admin.phones.gallery.image.update', ['phone' => $phone->getKey(), 'image' => $image->getKey()]) }}"
                                data-toggle="toggle" data-onstyle="outline-success"
                                data-name="{{ \App\Domain\Shared\Image\Image::FIELD_IS_MAIN }}"
                                data-offstyle="outline-secondary"
                                class="is-main-checkbox"
                            >
                        </td>
                        <td class="text-center">
                            @include('backend.components.buttons.delete',
                                [
                                    'href' => route('admin.phones.gallery.image.delete', ['phone' => $phone->getKey(), 'image' => $image->getKey()]),
                                    'hide' => true,
                                    'outline' => true
                                ])
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            @include('backend.components.modals.delete-modal', ['target' => 'This image'])
            @include('backend.phone.components.gallery-image-upload-form', ['phone' => $phone])
        </div>
    </div>
</div>
