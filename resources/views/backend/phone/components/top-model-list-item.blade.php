<li class="ui-state-highlight list-group-item align-items-center" data-model-id="{{ $model->getKey() }}" data-top-model-id="{{ $topModelId ?? null }}">
    <span class="font-weight-bold">
        <a class="model-name text-dark" href="{{ route('admin.models.view', [
            'brand' => $model->getBrand()->getSlug(),
            'model' => $model->getSlug()
            ]) }}">{{ $model->getName() }}</a>
    </span>
    <span class="badge badge-pill badge-primary p-2 ml-2">{{ $model->getPhones()->count() }}</span>
    <span class="ml-auto">
        @include('backend.components.buttons.edit',
            [
                'href' => route('admin.models.edit', [
                    'brand' => $model->getBrand()->getSlug(),
                    'model' => $model->getSlug()
                    ]),
                'hide' => true,
                'outline' => true,
            ]
        )
    </span>
</li>

