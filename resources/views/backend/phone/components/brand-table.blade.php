<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Logo</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($brands as $brand)
        <tr>
            <td class="text-left">{{ $brand->getName() }}</td>
            <td class="text-center">
                @if($brand->getImage())
                    <img class="img img-thumbnail" style="width: 200px;" src="{{ asset($brand->getImage()->getPath()) }}" alt="{{ $brand->getName() }}">
                @else
                    <i class="fad fa-times text-danger"></i>
                @endif
            </td>
            <td class="text-center">
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.brands.edit', ['brand' => $brand->getSlug()]),
                        'hide' => true
                    ]
                )
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
