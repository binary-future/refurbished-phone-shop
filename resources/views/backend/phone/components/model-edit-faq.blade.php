<div class="faq-item d-flex w-100 justify-content-between m-3">
    @if(isset($id))
        <input type="hidden" name="faqs[{{ $index }}][id]" value="{{ $id }}">
    @endif
    <div>
        <label for="q{{ $number }}">Question {{ $number }}</label>
        <textarea id="q{{ $number }}" class="form-control"
                  name="faqs[{{ $index }}][question]"
                  rows="5" cols="50" placeholder="Question {{ $number }}">{{ $question ?? '' }}</textarea>
    </div>
    <div>
        <label for="a{{ $number }}">Answer {{ $number }}</label>
        <textarea id="a{{ $number }}" class="form-control"
                  name="faqs[{{ $index }}][answer]"
                  rows="5" cols="50" placeholder="Answer {{ $number }}">{{ $answer ?? '' }}</textarea>
    </div>
</div>
