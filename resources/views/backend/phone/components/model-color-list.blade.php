@IF($colors->isNotEmpty())
        <div class="row">
            <div class="col-12 text-left">Colors:</div>
            @foreach($colors as $color)
                @include('backend.phone.components.model-color-item', ['color' => $color])
            @endforeach
        </div>
@endif
