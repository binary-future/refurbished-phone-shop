<form method="POST" action="{{ route('admin.colors.update', ['color' => $color->getSlug()]) }}">
    {{ csrf_field() }}
    <input type="hidden" name="color_id" value="{{ $color->getKey() }}">
    <div class="form-group row">
        <div class="input-group">
            <label for="name" class="col-md-4 control-label">Name</label>
            <input id="name" type="text" class="form-control" name="name"
                   value="{{ $color->getName() }}" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <div class="input-group">
            <label for="hex" class="col-md-4 control-label">Hex</label>
            <div class="input-group-prepend">
                <div class="input-group-text">#</div>
            </div>
            <input type="text" class="form-control" id="hex" name="hex"
                   @if($color->getHex())
                   value="{{ $color->getHex() }}"
                   @else
                   placeholder="Enter hex value"
                    @endif
            >
        </div>
        @if ($errors->has('hex'))
            <span class="help-block">
                    <strong>{{ $errors->first('hex') }}</strong>
                </span>
        @endif
    </div>
    <div class="form-group row">
        <div class="input-group">
            <label for="bg-color-class" class="col-md-4 control-label">Background class</label>
            <input id="bg-color-class" type="text" class="form-control"
                   value=".bg-{{ $color->getSlug() }}"
                   disabled
            >
        </div>
    </div>

    @include('backend.components.buttons.update')
</form>
