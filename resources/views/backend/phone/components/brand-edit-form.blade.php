<form method="POST" action="{{ route('admin.brands.update', ['brand' => $brand->getSlug()]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="brand_id" value="{{ $brand->getKey() }}">
    @if(Auth::user()->isAdmin())
        <div class="form-group row">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="name"
                       value="{{ $brand->getName() }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }} row">
            <label for="logo" class="col-md-4 control-label">Logo</label>
            <div class="col-md-8 row">
                <div class="col-10">
                    @if ($brand->getImage())
                        <img id="logo-image"
                             src="{{ asset($brand->getImage()->getPath()) }}"
                             class="img img-fluid" alt="{{ $brand->getName() }}">
                    @else
                        <img id="logo-image" src="{{ asset('images/no-image.jpg') }}"
                             class="img img-fluid" alt="{{ $brand->getName() }}">
                    @endif
                </div>
                <div class="col-2">
                    <label class="btn btn-primary">
                        Browse <input id="logo" type="file" class="form-control file" name="logo"
                                      style="display: none;">
                    </label>
                    <span class="image-name"></span>
                </div>
                @if ($errors->has('logo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('logo') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    @endif
    <div class="form-group row">
        <label for="description" class="col-md-4 control-label">Description</label>
        <div class="col-md-8">
              <textarea id="description" type="text" class="tiny form-control" name="description[content]" data-url="{{ route('admin.editor.file.upload') }}">
                @if($brand->getDescription())
                      {{ $brand->getDescription()->getContent() }}
                  @endif
            </textarea>
            @if ($errors->has('description.content'))
                <span class="help-block">
                    <strong>{{ $errors->first('description.content') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="pre-post-process d-none"></div>
    @include('backend.components.buttons.update')
</form>
