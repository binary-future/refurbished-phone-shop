<div class="col-12 col-lg-3 col-sm-6">
<div class="card text-dark bg-light mb-3 mx-1 border border-info rounded">
        <div class="card-header bg-primary text-white">{{ $model->getName() }}</div>
        <div class="card-body bg-white text-center">
            <img class="card-img-bottom card-img mx-auto my-2" src=
            "{{ asset($model->getImage() ? $model->getImage()->getPath() : 'images/no-image.jpg') }}">
        </div>
        <div class="card-footer text-center">
            <a class="btn btn-outline-primary" href="{{ route('admin.models.view', ['brand' => isset($brand) ? $brand->getSlug() : $model->getBrand()->getSlug(), 'model' => $model->getSlug()]) }}">
                View device
            </a>
        </div>
    </div>
</div>
