<table class="table table-bordered mt-2 model-list-table">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Image</th>
        <th scope="col" class="text-center">Color</th>
        <th scope="col" class="text-center">Capacity</th>
        <th scope="col" class="text-center"><i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($phones as $phone)
        <tr>
            <td class="text-center w-25">
                @if($phone->getImage())
                    <img alt="image" class="img img-thumbnail w-50" src="{{ asset($phone->getImage()->getPath()) }}">
                @else
                    <i class="fad fa-times text-danger">
                @endif
            </td>
            <td class="text-center">
                @if($phone->getColor())
                    {{ $phone->getColor()->getName() }}
                @else
                    <i class="fad fa-times text-danger">
                @endif
            </td>
            <td class="text-center">
                {{ $phone->getCapacity() ?: 'None' }}
            </td>
            <td class="text-center">
                <a href="{{ route('admin.phones.view', ['phone' => $phone->getKey()]) }}" class="btn btn-outline-primary"><i class="fad fa-eye"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
