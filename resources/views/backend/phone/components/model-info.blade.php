<p>Specs: <i class="fad {{ $model->getSpecs() ? 'fa-check text-success' : 'fa-times text-danger'}}"></i></p>
<p>Description: <i class="fad {{ $model->getDescription() ? 'fa-check text-success' : 'fa-times text-danger'}}"></i></p>
<p>Active: <i class="fad {{ $model->isActive() ? 'fa-check text-success' : 'fa-times text-danger'}}"></i></p>
<p>Latest: <i class="fad {{ $latestModel ? 'fa-check text-success' : 'fa-times text-danger'}}"></i></p>
@if(Auth::user()->isAdmin())
    <p>
        @if ($model->isSynonym())
            This model is synonyms for
            <a href="{{ route('admin.models.view', ['brand' => $model->getBaseModel()->getBrand()->getSlug(), 'model' => $model->getBaseModel()->getSlug()]) }}">
                {{ $model->getBaseModel()->getName() }}
            </a>
            <br/>
        @endif
    </p>
    <p>
        @if ($model->hasSynonyms())
            This model have {{ $model->getSynonyms()->count() }} synonym(s): <br/>
            @foreach($model->getSynonyms() as $child)
                <a href="{{ route('admin.models.view', ['brand' => $child->getBrand()->getSlug(), 'model' => $child->getSlug()]) }}">
                    {{ $child->getName() }}
                </a>
                <br/>
            @endforeach
        @endif
    </p>
    @if (! $model->isSynonym())
        <p><a class="btn btn-outline-success" href="{{ route('admin.models.view.synonyms', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}"><i class="fad fa-users"></i> Manage Synonyms</a></p>
    @endif
@endif
