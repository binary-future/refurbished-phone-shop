<div class="modal fade" id="uploadImage" tabindex="-1" role="dialog"
     aria-labelledby="uploadFormLabel" aria-hidden="true">
    <div class="modal-dialog" style="top: 25%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header d-flex justify-content-between">
                <h4 class="modal-title" id="uploadFormLabel">
                    Upload Image
                </h4>
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body" style="min-height: 250px;">
                <form role="form" method="POST" action="{{ route('admin.phones.gallery.image.add', ['phone' => $phone->getKey()]) }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                        <label for="file" class="col control-label">Image</label>
                        <div class="col">
                            <input id="file" type="file" class="form-control" name="file" required>
                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary"><i class="fad fa-upload"></i> Upload</button>
                </form>
            </div>
        </div>
    </div>
</div>
