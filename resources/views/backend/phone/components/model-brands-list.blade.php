<div class="row">
    @foreach($brands as $brand)
        <div class="col-3 mb-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('admin.models.list', ['brand' => $brand->getSlug()]) }}" class="text-dark text-decoration-none">
                        <h6 class="font-weight-bold d-flex justify-content-between">{{ $brand->getName() }} <span class="badge badge-pill badge-primary">{{ $brand->getModels()->count() }}</span></h6>
                    </a>
                </div>
            </div>
        </div>
    @endforeach
</div>