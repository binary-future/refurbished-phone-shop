<div class="form-group row">
    <label for="faqs" class="col-md-4 control-label">Faqs</label>
    <div class="col-md-8">
        <div class="faq-container">
            @foreach($model->getFaqs() as $ind => $faq)
                @include('backend.phone.components.model-edit-faq', [
                    'id' => $faq->getKey(),
                    'number' => $ind + 1,
                    'index' => $ind,
                    'question' => $faq->getQuestion()->getQuestion(),
                    'answer' => $faq->getAnswer()->getAnswer()
                ])
            @endforeach
            @include('backend.phone.components.model-edit-faq', ['number' => $model->getFaqs()->count() + 1, 'index' => $model->getFaqs()->count()])
        </div>
        <button id="add-faq" class="btn btn-primary">Add Faq</button>
    </div>
</div>

@push('pre-js')
    <script id="faq-template" type="text/x-handlebars-template">
        @include('backend.phone.components.model-edit-faq', ['number' => '@{{ number }}', 'index' => '@{{ index }}'])
    </script>
@endpush
