<div class="row">
    @foreach($models as $model)
        @include('backend.phone.components.model-item', ['model' => $model])
    @endforeach
    @if(method_exists($models, 'links'))
        <div class="w-100"></div>
        <div class="col-12 justify-content-center d-flex">
            {{ $models->links('backend.components.paginator.default') }}
        </div>
    @endif
</div>
