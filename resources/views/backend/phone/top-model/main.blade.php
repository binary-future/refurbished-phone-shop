@extends('backend.layouts.app')

@section('title'){{ 'Models brands list' }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Top models</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                @include('backend.phone.components.top-model-list', ['models' => $models])
            </div>
        </div>
    </div>
@endsection
