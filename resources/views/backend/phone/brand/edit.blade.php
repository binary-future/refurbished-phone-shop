@extends('backend.layouts.app')

@section('title'){{ sprintf('Edit %s', $brand->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.brands.main') }}">Brands</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $brand->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Edit {{ $brand->getName() }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.phone.components.brand-edit-form', ['brand' => $brand])
            </div>
        </div>
    </div>
@endsection