@extends('backend.layouts.app')

@section('title'){{ $brand->getName() }} {{ $model->getName() }} {{ $phone->getColor() ? $phone->getColor()->getName() : '' }} {{ $phone->getCapacity() }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.main') }}">Models search</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.list', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }} devices</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">{{ $brand->getName() }} {{ $model->getName() }}</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $brand->getName() }} {{ $model->getName() }} {{ $phone->getColor() ? $phone->getColor()->getName() : '' }} {{ $phone->getCapacity() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div id="error-section" class="col-12 border-bottom border-success mb-3 pb-2">
                @include('backend.components.messages.message')
                <h3 class="text-left">{{ $brand->getName() }} {{ $model->getName() }} {{ $phone->getColor() ? $phone->getColor()->getName() : '' }} {{ $phone->getCapacity() }}</h3>
            </div>
            <div class="col-12" id="phone-gallery">
                @include('backend.phone.components.phone-gallery', ['phone' => $phone])
            </div>
        </div>
    </div>
@endsection
