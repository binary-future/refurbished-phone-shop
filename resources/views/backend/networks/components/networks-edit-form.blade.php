<form method="POST" action="{{ route('admin.networks.update', ['network' => $network->getSlug()]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="network_id" value="{{ $network->getKey() }}">
    <div class="form-group row">
        <label for="name" class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="name"
                   value="{{ $network->getName() }}" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }} row">
        <label for="logo" class="col-md-4 control-label">Logo</label>
        <div class="col-md-8 row">
            <div class="col-10">
                @if ($network->getImage())
                    <img id="logo-image"
                         src="{{ asset($network->getImage()->getPath()) }}"
                         class="img img-thumbnail w-50" alt="{{ $network->getName() }}">
                @else
                    <img id="logo-image" src="{{ asset('images/no-image.jpg') }}"
                         class="img img-fluid" alt="{{ $network->getName() }}">
                @endif
            </div>
            <div class="col-2">
                <label class="btn btn-primary">
                    Browse <input id="logo" type="file" class="form-control file" name="logo"
                                  style="display: none;">
                </label>
                <span class="image-name"></span>
            </div>
            @if ($errors->has('logo'))
                <span class="help-block">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
            @endif
        </div>
    </div>
    @include('backend.components.buttons.update')
</form>
