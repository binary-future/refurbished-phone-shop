<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Logo</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($networks as $network)
        <tr>
            <td class="text-left">{{ $network->getName() }}</td>
            <td class="text-center">
                @if($network->getImage())
                    <img class="img img-thumbnail" style="width: 200px;" src="{{ asset($network->getImage()->getPath()) }}" alt="{{ $network->getName() }}">
                @else
                    <i class="fad fa-times text-danger"></i>
                @endif
            </td>
            <td class="text-center">
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.networks.edit', ['network' => $network->getSlug()]),
                        'hide' => true
                    ]
                )
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
