@extends('backend.layouts.app')

@section('title'){{ sprintf('%s editing', $network->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.networks.main') }}">Networks</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $network->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>Edit {{ $network->getName() }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.networks.components.networks-edit-form', ['network' => $network])
            </div>
        </div>
    </div>
@endsection
