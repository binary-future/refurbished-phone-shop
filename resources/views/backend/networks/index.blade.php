@extends('backend.layouts.app')

@section('title'){{ 'Networks' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item active" aria-current="page">Networks</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Networks list</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.networks.components.network-table', ['networks' => $networks])
            </div>
        </div>
    </div>
@endsection
