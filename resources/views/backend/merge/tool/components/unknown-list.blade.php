<h3>Unknown</h3>
<input id="unknown-filter" class="form-control" type="text" placeholder="Filter...">
<div class="clearfix" style="overflow-x: hidden !important;">
    <ul id="unknown-list" class="list-group not-filtered-sortable">
        <li class="list-group-item list-group-item-success clearfix">Drag here to remove synonym</li>
        @foreach($models as $model)
            <li class="list-group-item list-group-item-warning model-item"
                data-id="{{ $model->getKey() }}">
                <a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">
                    {{ $model->getName() }}
                </a>
            </li>
        @endforeach
    </ul>
</div>
