<div class="w-100">
    <span class="mr-2 font-weight-bold">{{ $filter->getName() }}</span>
        <div class="btn-group position-relative">
            <a class="btn btn-light btn-sm dropdown-toggle py-0 mt-n1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fad fa-ellipsis-v"></i>
            </a>
            <div id="menu-{{ $filter->getKey() }}" class="dropdown-menu">
                <a href="#" data-toggle="modal"
                   class="dropdown-item add-children"
                   data-target="#filter-form"
                   data-parent="{{ $filter->getKey() }}"
                >
                    <i class="fad fa-plus small"></i> Add Filter</a>
                <a href="#" data-toggle="modal"
                   class="edit-filter dropdown-item"
                   data-target="#filter-form"
                   data-filter="{{ $filter->getKey() }}"
                   data-name="{{ $filter->getFilter() }}"
                >
                    <i class="fad fa-edit"></i> Edit</a>
                <div class="dropdown-divider"></div>
                <a href="#" data-toggle="modal"
                   data-target="#confirm-delete"
                   data-href="{{ $deleteRoute }}"
                   class="dropdown-item"
                >
                    <i class="fad fa-trash"></i> Delete
                </a>
                <a href="#" data-group="{{ $filter->getKey() }}" class="save-group dropdown-item">
                    <i class="fad fa-save"></i> Save
                </a>
            </div>
        </div>
</div>
