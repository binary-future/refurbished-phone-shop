<ul style="width: 100%;"
    class="list-group synonyms-{{ $filter->getKey() }} potential-synonyms-sortable">
    <li class="list-group-item list-group-item-success clearfix">Drag here to add synonyms</li>
    @foreach($colors as $color)
        <li class="list-group-item list-group-item-default model-item"
            data-id="{{ $color->getKey() }}">
            <a href="#">
                {{ $color->getName() }}
            </a>
        </li>
    @endforeach
</ul>
