<ul class="list-group clearfix base-{{ $filter->getKey() }} base-model-sortable" style="width: 100%;">
    <li class="list-group-item list-group-item-success clearfix">Drag here to add base model</li>
    @if($model)
        <li class="list-group-item list-group-item-info model-item"
            data-id="{{ $model->getKey() }}">
            <a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">
                {{ $model->getName() }}
                <span class="badge badge-dark">{{ $model->getSynonyms()->count() }}</span>
            </a>
            <a href="#" data-toggle="modal"
               data-target="#confirm-unmerge"
               data-model-name="{{ $model->getName() }}"
               data-href="{{ route('admin.merge.tool.model.unmerge.model', ['model' => $model->getKey()]) }}"
               title="Unmerge synonyms"
            >
                <span class="badge badge-danger"><i class="fad fa-trash small"></i></span></a>
        </li>
    @endif
</ul>
