<div class="modal fade import-modal" id="filter-form" tabindex="-1" role="dialog"
     aria-labelledby="filterFormLabel" aria-hidden="true">
    <div class="modal-dialog" style="top: 25%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header d-flex justify-content-between">
                <h4 class="modal-title" id="filterFormLabel">
                    Filter
                </h4>
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body" style="min-height: 250px;">
                <form role="form" method="POST" action="{{ route('admin.merge.tool.color.filter.save') }}">
                    {{ csrf_field() }}
                    <input id="filterId" type="hidden" name="filter_id" value="">
                    <input id="parentId" type="hidden" name="parent_id" value="">
                    <div class="row">
                        <label for="filter-name" class="col-2 control-label">Name*</label>
                        <div class="col-10">
                            <input id="filter-name" type="text" class="form-control" name="name"
                                   placeholder="Enter filter name" required>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="col-12 text-right mt-2">
                            <button type="submit" class="btn btn-outline-primary"><i class="fad fa-save"></i> Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
