<table id="merge-table"
       class="table table-hover"
       data-token="{{ csrf_token() }}"
       data-href="{{ route('admin.merge.tool.model.merge.synonym') }}">
    <thead style="display: flex;">
    <tr style="display: flex; width: 100%;">
        <th class="col-md-4">Group</th>
        <th class="col-md-4">Basic Model</th>
        <th class="col-md-4">Synonyms</th>
    </tr>
    </thead>
    <tbody>
    @foreach($filtered as $group)
        <tr style="display: flex;">
            <td style="display: flex; flex: 0 50%; width: 33.33%">
                @include('backend.merge.tool.components.filter-row', [
                    'filter' => $group->getFilter(),
                     'deleteRoute' => route('admin.merge.tool.model.filter.delete', ['filter' => $group->getFilter()->getKey()])
                 ])
            </td>
            <td style="display: flex; flex: 0 50%; width: 33.33%">
                @include('backend.merge.tool.components.base-model-row', [
                    'filter' => $group->getFilter(),
                    'model' => $group->getBaseModel(),
                ])
            </td>
            <td style="display: flex; flex: 0 50%; width: 33.33%">
                @include('backend.merge.tool.components.synonyms-row', [
                    'filter' => $group->getFilter(),
                    'models' => $group->getSynonyms(),
            ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
