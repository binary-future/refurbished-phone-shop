<ul style="width: 100%;"
    class="list-group synonyms-{{ $filter->getKey() }} potential-synonyms-sortable">
    <li class="list-group-item list-group-item-success clearfix">Drag here to add synonyms</li>
    @foreach($models as $model)
        <li class="list-group-item list-group-item-default model-item"
            data-id="{{ $model->getKey() }}">
            <a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">
                {{ $model->getName() }}
            </a>
        </li>
    @endforeach
</ul>
