<ul class="list-group clearfix base-{{ $filter->getKey() }} base-model-sortable" style="width: 100%;">
    <li class="list-group-item list-group-item-success clearfix">Drag here to add base color</li>
    @if($color)
        <li class="list-group-item list-group-item-info model-item"
            data-id="{{ $color->getKey() }}">
            <a href="{{ route('admin.colors.synonyms', ['color' => $color->getSlug()]) }}">
                {{ $color->getName() }}
                <span class="badge badge-dark">{{ $color->getSynonyms()->count() }}</span>
            </a>
            <a href="#" data-toggle="modal"
               data-target="#confirm-unmerge"
               data-model-name="{{ $color->getName() }}"
               data-href="{{ route('admin.merge.tool.color.unmerge.color', ['color' => $color->getKey()]) }}"
               title="Unmerge synonyms"
            >
                <span class="badge badge-danger"><i class="fad fa-trash small"></i></span></a>
        </li>
    @endif
</ul>
