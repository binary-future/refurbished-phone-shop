<div class="modal fade" id="confirm-unmerge"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog delete-modal">
        <div class="modal-content">
            <div class="modal-header">
                Confirmation
            </div>
            <div class="modal-body">
                You really want to unmerge all <span id="unmerge-model-name"></span> synonyms?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger text-white" href="#"
                   onclick="event.preventDefault(); document.getElementById('unmerge-form').submit();">
                    <i class="fad fa-trash"></i> Confirm
                </a>
                <form id="unmerge-form" class="unmerge-modal-form" action="#" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
