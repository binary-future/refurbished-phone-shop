<h3>Unknown</h3>
<input id="unknown-filter" class="form-control" type="text" placeholder="Filter...">
<div class="clearfix" style="overflow-x: hidden !important;">
    <ul id="unknown-list" class="list-group not-filtered-sortable">
        <li class="list-group-item list-group-item-success clearfix">Drag here to remove synonym</li>
        @foreach($colors as $color)
            <li class="list-group-item list-group-item-warning model-item"
                data-id="{{ $color->getKey() }}">
                {{ $color->getName() }}
            </li>
        @endforeach
    </ul>
</div>
