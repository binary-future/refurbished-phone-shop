<table id="merge-table"
       class="table table-hover"
       data-token="{{ csrf_token() }}"
       data-href="{{ route('admin.merge.tool.color.merge.synonym') }}">
    <thead style="display: flex;">
    <tr style="display: flex; width: 100%;">
        <th class="col-md-4">Group</th>
        <th class="col-md-4">Basic Model</th>
        <th class="col-md-4">Synonyms</th>
    </tr>
    </thead>
    <tbody>
    @foreach($filtered as $group)
        <tr style="display: flex;">
            <td style="display: flex; flex: 0 50%; width: 33.33%">
                @include('backend.merge.tool.components.filter-row', [
                'filter' => $group->getFilter(),
                 'deleteRoute' => route('admin.merge.tool.color.filter.delete', ['filterId' => $group->getFilter()->getKey()])
             ])
            </td>
            <td style="display: flex; flex: 0 50%; width: 33.33%">
                @include('backend.merge.tool.components.base-color-row', [
                    'filter' => $group->getFilter(),
                    'color' => $group->getBaseColor(),
                ])
            </td>
            <td style="display: flex; flex: 0 50%; width: 33.33%">
                @include('backend.merge.tool.components.synonyms-color-row', [
                    'filter' => $group->getFilter(),
                    'colors' => $group->getSynonyms(),
            ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
