@extends('backend.layouts.app')

@section('title'){{ sprintf('Colors Merge Tool') }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.merge.main') }}">Merge</a></li>
    <li class="breadcrumb-item active" aria-current="page">Colors Merge Tool</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12 text-center">
                        @include('backend.components.messages.message')
                        <div id="error" class="text-center alert alert-danger" style="display: none; cursor: pointer;">
                            <span><strong>Nothing to save</strong></span>
                        </div>
                        <div id="saving-process" class="text-center alert alert-warning" style="display: none; cursor: pointer;">
                            <span><strong>Saving process... Wait please</strong></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <a class="btn btn-primary" href="{{ route('admin.colors.main') }}">
                    <i class="fad fa-arrow-circle-left"></i> Colors section</a>
                <a class="btn btn-default add-new-filter" href="#" data-toggle="modal"
                   data-target="#filter-form"><i class="fad fa-plus"></i> Add Filter</a>
                <a class="btn btn-dark save-all" href="#"><i class="fad fa-save"></i> Save</a>
                <a  class="btn btn-danger" href="#" data-toggle="modal"
                    data-target="#confirm-delete"
                    data-href="{{ route('admin.merge.tool.color.filter.delete.all') }}"
                ><i class="fad fa-trash"></i> Clear Filters</a>
                <a class="btn btn-warning" href="{{ route('admin.merge.map.color.view') }}">
                    <i class="fad fa-map"></i> Synonyms map
                </a>
            </div>
            <div class="col-12">
                <div class="row justify-content-around row-eq-height">
                    <div class="col-8">
                        @include('backend.merge.tool.components.synonyms-color-table', ['filtered' => $mergeTool->getFiltered()])
                    </div>
                    <div class="col-4 merge-group-scroll">
                        @include('backend.merge.tool.components.unknown-color-list', ['colors' => $mergeTool->getUnfiltered()->getModels()])
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.merge.tool.components.filter-color-form-modal')
    @include('backend.merge.tool.components.unmerge-synonyms-modal')
    @include('backend.components.modals.delete-modal', ['target' => 'filter(s)'])
@endsection
