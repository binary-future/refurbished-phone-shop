@extends('backend.layouts.app')

@section('title'){{ 'Model Merge Tool' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.merge.main') }}">Merge</a></li>
    <li class="breadcrumb-item active" aria-current="page">Models Merge Tool</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Merging Models</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <a class="btn btn-warning" href="{{ route('admin.merge.map.model.view') }}">
                    <i class="fad fa-map"></i> Merge Map
                </a>
                <a class="btn btn-danger" href="#" data-toggle="modal" data-target="#confirm-delete"
                   data-href="{{ route('admin.merge.tool.model.filter.delete.all') }}"
                >
                    <i class="fad fa-trash"></i> Clear Filters</a>
            </div>
            <div class="col-12">
                <div class="row justify-content-around row-eq-height">
                    @foreach($brands as $brand)
                        <a class="col-md-4 col-6 text-decoration-none text-dark mb-2" href="{{ route('admin.merge.tool.model.view', ['brand' => $brand->getSlug()]) }}">
                            <div class="card border border-dark merge-brand-item">
                                <div class="card-header text-center d-flex justify-content-between">
                                    <span class="font-weight-bold">{{ strtoupper($brand->getName()) }}</span>
                                    <span>Models <span class="badge badge-success">{{ $brand->getModels()->count() }}</span>
                                        Filters <span class="badge badge-secondary">{{ isset($filters[$brand->getKey()]) ? $filters->get($brand->getKey())->count() : 0 }}</span></span>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
            @include('backend.components.modals.delete-modal', ['target' => 'all filter(s)'])
        </div>
    </div>
@endsection
