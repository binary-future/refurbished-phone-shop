@extends('backend.layouts.app')

@section('title'){{ 'Colors merge map' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.merge.main') }}">Merge</a></li>
    <li class="breadcrumb-item active" aria-current="page">Color merge map</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12 text-center">
                        @include('backend.components.messages.message')
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <a class="btn btn-primary" href="{{ route('admin.merge.tool.color.main') }}">
                    <i class="fad fa-arrow-circle-left"></i> Merge Tool</a>
            </div>
            <div class="col-12">
                <h3 class="text-center">Base colors ({{ $colors->count() }})</h3>
            </div>
            <div class="col-12">
                @include('backend.merge.map.components.color-map-table', ['colors' => $colors])
            </div>
        </div>
    </div>
@endsection
