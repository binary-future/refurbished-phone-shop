<table class="table table-hover model-list-table">
    <thead class="bg-success text-white text-uppercase">
    <tr>
        <th class="col-md-6">Base Color</th>
        <th class="col-md-6">Synonyms</th>
    </tr>
    </thead>
    <tbody>
        @foreach($colors as $color)
            <tr>
                <td>
                    <a href="{{ route('admin.colors.synonyms', ['color' => $color->getSlug()]) }}"
                    >
                   {{ $color->getName() }}</a>
                </td>
                <td>
                    @foreach($color->getSynonyms() as $child)
                        {{ $child->getName() }} <br/>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
