<table class="table table-hover model-list-table">
    <thead class="bg-success text-white text-uppercase">
    <tr>
        <th class="col-md-6">Base Model</th>
        <th class="col-md-6">Synonyms</th>
    </tr>
    </thead>
    <tbody>
        @foreach($models as $model)
            <tr>
                <td>
                    <a href="{{ route('admin.models.view.synonyms', [
                            'brand' => $model->getBrand()->getSlug(),
                            'model' => $model->getSlug()
                        ]) }}"
                    >
                    {{ $model->getBrand()->getName() }} {{ $model->getName() }}</a>
                </td>
                <td>
                    @foreach($model->getSynonyms() as $child)
                        {{ $child->getName() }} <br/>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
