@extends('backend.layouts.app')

@section('title'){{ 'Merge Section' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item active" aria-current="page">Merge</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Merging</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row justify-content-around row-eq-height">
                    <div class="col-5 my-2 p-2">
                        <h5 class="text-center">Models</h5>
                        <ul class="list-group ">
                            <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0">
                                Base Models count:
                                <span class="font-weight-bold">{{ $models->count() }}</span>
                            </li>
                            <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0">
                                Synonym Models count:
                                <span class="font-weight-bold">{{ $synonymModels->count() }}</span>
                            </li>
                            <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0 border-bottom border-success">
                                Filters count:
                                <span class="font-weight-bold">{{ $modelsFilter->count() }}</span>
                            </li>
                        </ul>
                        <div class="w-100 text-right">
                            <a href="{{ route('admin.merge.tool.model.main') }}" class="btn btn-outline-info"><i class="fad fa-cogs"></i> Tool</a>
                            <a href="{{ route('admin.merge.map.model.view') }}" class="btn btn-outline-success"><i class="fad fa-map"></i> Map</a>
                        </div>
                    </div>
                    <div class="col-1 border-right"></div>
                    <div class="col-5 my-2 p-2">
                        <h5 class="text-center">Colours</h5>
                        <ul class="list-group ">
                            <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0">
                                Colours count:
                                <span class="font-weight-bold">{{ $colors->count() }}</span>
                            </li>
                            <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0">
                                Synonym Colors count:
                                <span class="font-weight-bold">{{ $synonymColors->count() }}</span>
                            </li>
                            <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0 border-bottom border-success">
                                Filters count:
                                <span class="font-weight-bold">{{ $colorsFilter->count() }}</span>
                            </li>
                        </ul>
                        <div class="w-100 text-right">
                            <a href="{{ route('admin.merge.tool.color.main') }}" class="btn btn-outline-info"><i class="fad fa-cogs"></i> Tool</a>
                            <a href="{{ route('admin.merge.map.color.view') }}" class="btn btn-outline-success"><i class="fad fa-map"></i> Map</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
