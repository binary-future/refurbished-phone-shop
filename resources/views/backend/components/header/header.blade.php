<nav class="navbar navbar-expand-lg navbar-light bg-light text-dark">
    <div class="{{ url()->current() !== route('admin.home') ? 'container' : 'container-fluid' }}">
		 @if(Auth::user())
        <a class="navbar-brand small" href="{{ route('home') }}/admin">@section('brand')<img src="https://www.onecompare.com/images/logo-temp.svg" alt="" class="img img-fluid w-50 mb-2"> Admin  @show</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
		 @endif
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
                @if(Auth::user())

				    <div class="d-flex">
                        @include('backend.phone.components.model-search-form')
                    </div>

                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link text-dark dropdown-toggle font-weight-bold" data-toggle="dropdown" role="button" aria-expanded="false"
                               aria-haspopup="true">
                                Sections<span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                @if(Auth::user()->isAdmin())
								    <li><a class="dropdown-item" href="{{ route('admin.models.main') }}">Device Models</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.colors.main') }}">Device Colours</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.top-models.main') }}">Top Models</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.brands.main') }}">Brands</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.stores.main') }}">Stores</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.payment-methods.main') }}">Payment Methods</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.networks.main') }}">Networks</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.users.main') }}">Users</a></li>
								    <li><a class="dropdown-item" href="{{ route('admin.import.model.main') }}">Import</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.merge.main') }}">Merge</a></li>
                                @else
								    <li><a class="dropdown-item" href="{{ route('admin.models.main') }}">Device Models</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.brands.main') }}">Brands</a></li>
                                    <li><a class="dropdown-item" href="{{ route('admin.stores.main') }}">Stores</a></li>
                                @endif
                            </ul>
                        </li>
                    </ul>
                    <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link text-dark dropdown-toggle text-muted" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                               <i class="fal fa-user text-secondary small mb-2"></i> {{ Auth::user()->name }} <span class="caret text-muted"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="{{ route('admin.profile.view') }}">
                                        Profile
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>


                @endif
            </ul>
        </div>
    </div>
</nav>
