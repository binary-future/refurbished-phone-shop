<div class="modal fade" id="confirm-delete"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog delete-modal">
        <div class="modal-content">
            <div class="modal-header">
                Confirmation
            </div>
            <div class="modal-body">
                You really want to delete {{ $target }}?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger text-white" href="#"
                   onclick="event.preventDefault(); document.getElementById('delete-form').submit();">
                    <i class="fad fa-trash"></i> Confirm
                </a>
                <form id="delete-form" class="delete-modal-form" action="#" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
