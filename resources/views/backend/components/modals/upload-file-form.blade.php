<div class="modal fade import-modal" id="uploadForm" tabindex="-1" role="dialog"
     aria-labelledby="uploadFormLabel" aria-hidden="true">
    <div class="modal-dialog" style="top: 25%;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header d-flex justify-content-between">
                <h4 class="modal-title" id="uploadFormLabel">
                    Upload Files
                </h4>
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <!-- Modal Body -->
            <div class="modal-body" style="min-height: 250px;">
                <div class="w-100 text-right"><a href="#" id="clear-selection" class="text-success d-none">[Clear]</a></div>
                <form role="form" method="POST" action="{{ route('admin.file.upload') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="entity-target" class="col control-label">Target</label>
                        <select id="entity-target" name="target" class="form-group custom-select">
                            <option id="default" value="" selected>--Select--</option>
                            <option value="brands">Brands</option>
                            <option value="phones">Phones</option>
                            <option value="networks">Networks</option>
                        </select>
                    </div>
                    <div id="file-type-select" class="form-group">
                    </div>
                    <div id="file-input" class="form-group{{ $errors->has('file') ? ' has-error' : '' }}" style="display: none">
                        <label for="file" class="col control-label">File</label>

                        <div class="col">
                            <input id="file" type="file" class="form-control-file" name="file" required>
                            @if ($errors->has('file'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-primary"><i class="fad fa-upload"></i> Upload</button>
                </form>
            </div>
        </div>
    </div>
</div>
