<a class="btn {{ isset($outline) && $outline ? 'btn-outline-danger' : 'btn-danger'}}"
   href="#" data-toggle="modal" data-target="#confirm-delete"
   data-href="{{ $href }}">
    <i class="fad fa-trash"></i> {{ isset($hide) && $hide ? '' : 'Delete' }}
</a>
