<a class="btn {{ isset($outline) && $outline ? 'btn-outline-info' : 'btn-info'}}" href="{{ $href }}"><i class="fad fa-edit"></i> {{ isset($hide) && $hide ? '' : 'Edit' }}</a>
