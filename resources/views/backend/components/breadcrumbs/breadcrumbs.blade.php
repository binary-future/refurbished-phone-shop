<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            @yield('breadcrumbs-content')
        </ol>
    </nav>
</div>