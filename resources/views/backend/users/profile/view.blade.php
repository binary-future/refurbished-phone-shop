@extends('backend.layouts.app')

@section('title'){{ sprintf('%s Profile', $user->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item active" aria-current="page">Profile</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Edit your profile</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 p-2 border-bottom border-success">
                <h6 class="text-center my-2">Edit main info</h6>
                @include('backend.users.components.edit-email-form', ['user' => $user])
            </div>
            <div class="col-12 p-2">
                <h6 class="text-center my-2">Change password</h6>
                @include('backend.users.components.edit-password-form')
            </div>
        </div>
    </div>
@endsection
