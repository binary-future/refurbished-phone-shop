@extends('backend.layouts.app')

@section('title'){{ sprintf('Users: edit %s', $user->getName()) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.users.main') }}">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit {{ $user->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>Edit {{ $user->getName() }}</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.users.components.user-edit-form', ['user' => $user, 'roles' => $roles])
            </div>
        </div>
    </div>
@endsection
