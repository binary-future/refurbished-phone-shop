@extends('backend.layouts.app')

@section('title'){{ 'Create new user' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.users.main') }}">Users</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add new user</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>Create new user</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.users.components.user-create-form', ['roles' => $roles])
            </div>
        </div>
    </div>
@endsection
