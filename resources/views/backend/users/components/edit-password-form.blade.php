<form class="form-horizontal" method="POST" action="{{ route('admin.profile.update.password') }}">
    {{ csrf_field() }}
    <div class="form-group row {{ $errors->has('check_password') ? ' has-error' : '' }}">
        <label for="check_password" class="col-md-4 control-label">Old Password</label>

        <div class="col-md-8">
            <input id="check_password" type="password" class="form-control"
                   name="check_password" placeholder="Enter your old password" required>

            @if ($errors->has('check_password'))
                <span class="help-block">
                    <strong>{{ $errors->first('check_password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">New Password</label>
        <div class="col-md-8">
            <input id="password" type="password" class="form-control"
                   name="password" placeholder="Enter your new password" required>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="password-confirm" class="col-md-4 control-label">Confirm New Password</label>

        <div class="col-md-8">
            <input id="password-confirm" type="password" class="form-control"
                   name="password_confirmation" placeholder="Confirm new password" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            <button type="submit" class="btn btn-primary"><i class="fad fa-save"></i>
                Update Password
            </button>
        </div>
    </div>
</form>
