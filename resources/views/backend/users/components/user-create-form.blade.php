<form method="POST" action="{{ route('admin.users.save') }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="name" class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="name"
                   value="{{ old('name') }}" placeholder="Enter user name" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-md-4 control-label">Email</label>
        <div class="col-md-8">
            <input id="email" type="email" class="form-control" name="email"
                   value="{{ old('email') }}" placeholder="Enter email" required>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="col-md-4 control-label">Password</label>
        <div class="col-md-8">
            <input id="password" type="password" class="form-control"
                   name="password" placeholder="Enter user password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
        <div class="col-md-8">
            <input id="password-confirm" type="password" class="form-control"
                   name="password_confirmation" placeholder="Confirm password" required>
        </div>
    </div>

    <div class="form-group row">
        <label for="role" class="col-md-4 control-label">Role</label>
        <div class="col-md-8">
            <select name="role"
                    class="form-control custom-select"
                    id="role">
                @foreach($roles as $role)
                    <option value="{{ $role->getKey() }}">
                        {{ ucfirst($role->getTitle()) }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    @include('backend.components.buttons.create')
</form>
