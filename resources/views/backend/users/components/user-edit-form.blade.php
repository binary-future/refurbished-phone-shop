<form method="POST" action="{{ route('admin.users.update', ['user' => $user->getKey()]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="name" class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="name"
                   value="{{ $user->getName() }}" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-md-4 control-label">Email</label>
        <div class="col-md-8">
            <input id="email" type="email" class="form-control" name="email"
                   value="{{ $user->getEmail() }}" required>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="role" class="col-md-4 control-label">Role</label>
        <div class="col-md-8">
            <select name="role"
                    class="form-control custom-select"
                    id="role">
                @foreach($roles as $role)
                    <option {{ $role->getKey() === $user->getRoleId() ? 'selected' : '' }} value="{{ $role->getKey() }}">
                        {{ ucfirst($role->getTitle()) }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    @include('backend.components.buttons.update')
</form>
