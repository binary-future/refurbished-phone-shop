<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Email</th>
        <th scope="col" class="text-center">Role</th>
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td class="text-center">{{ $user->getName() }}</td>
            <td class="text-center">{{ $user->getEmail() }}</td>
            <td class="text-center">
                @if($user->getRole())
                    {{ $user->getRole()->getTitle() }}
                @else
                    <i class="fad fa-times text-danger"></i>
                @endif
            </td>
            <td class="text-center">
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.users.edit', ['user' => $user->getKey()]),
                        'hide' => true,
                        'outline' => true,
                    ]
                )
                @include('backend.components.buttons.delete',
                    [
                        'href' => route('admin.users.delete', ['user' => $user->getKey()]),
                        'hide' => true,
                        'outline' => true,
                    ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
