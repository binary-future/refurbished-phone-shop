<form class="form-horizontal" method="POST" action="{{ route('admin.profile.update.info') }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="userId" value="{{ $user->id }}">
    <div class="form-group row">
        <label for="name" class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="name"
                   value="{{ $user->name }}" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="email" class="col-md-4 control-label">Email</label>
        <div class="col-md-8">
            <input id="email" type="email" class="form-control" name="email"
                   value="{{ $user->email }}" required>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row {{ $errors->has('check_password') ? ' has-error' : '' }}">
        <label for="check_password" class="col-md-4 control-label">Password</label>
        <div class="col-md-8">
            <input id="check_password" type="password" class="form-control" name="check_password"
                   placeholder="Enter your password to confirm changes" required>
            @if ($errors->has('check_password'))
                <span class="help-block">
                    <strong>{{ $errors->first('check_password') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-primary"><i class="fad fa-save"></i>
                Update Info
            </button>
        </div>
    </div>
</form>
