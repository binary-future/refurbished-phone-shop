<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title', 'Dashboard') - Admin panel</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<meta name="robots" content="noindex,nofollow">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/admin.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

		<link href="https://fonts.googleapis.com/css?family=Raleway&display=swap" rel="stylesheet">
		<link href="https://use.typekit.net/qqr1ykw.css?display=swap" rel="stylesheet">

        @stack('css')

        <meta property="og:url" content="{{ request()->url() }}"/>

        @include('backend.global.javascript-variables')
    </head>
    <body>
        <div id="app">
            @include('backend.components.header.header')
            @if(url()->current() !== route('admin.home'))
                <div class="w-100 mb-3"></div>
                @include('backend.components.breadcrumbs.breadcrumbs')
            @endif
            @yield('content')

            <div class="toast-box" aria-live="polite" aria-atomic="true">
                <div class="toast-list">
                    <!-- container for notification toasts -->
                </div>
            </div>
        </div>

        @stack('pre-js')
        <script src="{{ mix('/js/admin.js') }}"></script>
        @stack('js')
    </body>
</html>
