@extends('backend.layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-2 col-12 bg-dark">
                @include('backend.main.components.left-side-bar', ['storesCount' => $storesCount])
            </div>
            <div class="col-md-10 col-12">
                @include('backend.components.messages.message')
                <div class="w-100 mt-3">
                    @include('backend.main.components.main-pills-section', [
                        'stores' => $stores,
                        'negativeReports' => $negativeReports,
                        'positiveReports' => $positiveReports,
                        'phonesNegativeReports' => $phonesNegativeReports,
                        'phonesPositiveReports' => $phonesPositiveReports,
                        'inactiveModels' => $inActiveModel,
                        'withoutDealsModels' => $withoutDealsModels,
                        'plannedImports' => $plannedImports,
                    ])
                </div>
                @include('backend.components.modals.delete-modal', ['target' => 'Reports'])
                @include('backend.components.modals.upload-file-form')
            </div>
        </div>
    </div>
@endsection
