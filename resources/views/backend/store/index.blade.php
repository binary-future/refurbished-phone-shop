@extends('backend.layouts.app')

@section('title'){{ 'Stores' }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item active" aria-current="page">Stores</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        @include('backend.components.messages.message')
                        <h3>Stores list</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @if(Auth::user()->isAdmin())
                    @include('backend.components.buttons.add', ['href' => route('admin.stores.create'), 'entity' => 'store'])
                @endif
                @include('backend.store.components.store-table', ['stores' => $stores])
            </div>
            @if(Auth::user()->isAdmin())
                @include('backend.components.modals.delete-modal', ['target' => 'This store'])
            @endif
        </div>
    </div>
@endsection
