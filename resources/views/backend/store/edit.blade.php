@extends('backend.layouts.app')

@section('title'){{ sprintf('Stores: edit %s', $store->getName()) }}@endsection

@section('breadcrumbs-content')
    @if(Auth::user()->isAdmin())
        <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    @endif
    <li class="breadcrumb-item"><a href="{{ route('admin.stores.main') }}">Stores</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $store->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        @include('backend.components.messages.message')
                        <h3 class="pull-left">Edit {{ $store->getName() }}</h3>
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ $datafeedDownloadLink }}" target="_blank">Download deals datafeed</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.store.components.store-edit-form', [
                    'store' => $store,
                    'affiliations' => $affiliations,
                    'apiList' => $apiList,
                    'datafeed' => $datafeed
                 ])
            </div>
        </div>
    </div>
@endsection
