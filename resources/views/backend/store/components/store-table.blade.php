<table class="table table-hover mt-2">
    <thead class="bg-success text-white">
    <tr>
        <th scope="col" class="text-center">Name</th>
        <th scope="col" class="text-center">Logo</th>
        @if(Auth::user()->isAdmin())
            <th scope="col" class="text-center">Active?</th>
        @endif
        <th scope="col" class="text-center">Manage <i class="fad fa-cogs"></i></th>
    </tr>
    </thead>
    <tbody>
    @foreach($stores as $store)
        <tr>
            <td class="text-left">{{ $store->getName() }}</td>
            <td class="text-center">
                @if($store->getImage())
                    <img class="img img-thumbnail" style="width: 200px;" src="{{ asset($store->getImage()->getPath()) }}" alt="{{ $store->getName() }}">
                @else
                    <i class="fad fa-times text-danger"></i>
                @endif
            </td>
            @if(Auth::user()->isAdmin())
                <td class="text-center">
                    @if($store->isActive())
                        <i class="fad fa-check text-success"></i>
                    @else
                        <i class="fad fa-times text-danger"></i>
                    @endif
                </td>
            @endif
            <td class="text-center">
                @include('backend.components.buttons.edit',
                    [
                        'href' => route('admin.stores.edit', ['store' => $store->getSlug()]),
                        'hide' => true
                    ]
                )
                @if(Auth::user()->isAdmin())
                    @include('backend.components.buttons.delete',
                    [
                        'href' => route('admin.stores.delete', ['store' => $store->getSlug()]),
                        'hide' => true
                    ])
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
