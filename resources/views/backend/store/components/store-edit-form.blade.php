<form method="POST" action="{{ route('admin.stores.update', ['slug' => $store->getSlug()]) }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="store_id" value="{{ $store->getKey() }}">
    @if(Auth::user()->isAdmin())
        <div class="form-group row">
            <label for="name" class="col-md-4 control-label">Name</label>
            <div class="col-md-8">
                <input id="name" type="text" class="form-control" name="name"
                       value="{{ $store->getName() }}" required>
                @if ($errors->has('name'))
                    <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="link" class="col-md-4 control-label">Url</label>
            <div class="col-md-8">
                <input id="link" type="text" class="form-control" name="link"
                       value="{{ $store->getLink() ? $store->getLink()->getLink() : '' }}" required>
                @if ($errors->has('link'))
                    <span class="help-block">
                    <strong>{{ $errors->first('link') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="guarantee" class="col-md-4 control-label">Guarantee</label>
            <div class="col-md-8 input-group">
                <input id="guarantee" type="number" class="form-control" name="guarantee"
                       value="{{ $store->getGuarantee() }}" min="0" max="60" required>
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">months</span>
                </div>
                @if ($errors->has('guarantee'))
                    <span class="help-block">
                    <strong>{{ $errors->first('guarantee') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="is_active" class="col-md-4 control-label">Active</label>
            <div class="col-md-8">
                <select name="is_active" class="form-control custom-select" id="is_active">
                    <option {{ $store->isActive() === \App\Domain\Store\Store::STATUS_INACTIVE ? 'selected' : '' }}
                            value="{{ \App\Domain\Store\Store::STATUS_INACTIVE }}">No
                    </option>
                    <option {{ $store->isActive() == \App\Domain\Store\Store::STATUS_ACTIVE ? 'selected' : '' }}
                            value="{{ \App\Domain\Store\Store::STATUS_ACTIVE }}">Yes
                    </option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="affiliation" class="col-md-4 control-label">Affiliation</label>
            <div class="col-md-8">
                <select name="{{ \App\Domain\Store\Store::FIELD_AFFILIATION }}"
                        class="form-control custom-select"
                        id="affiliation">
                    @foreach($affiliations as $affiliation)
                        <option {{ $store->getAffiliation() && $store->getAffiliation()->isEqual($affiliation) ? 'selected' : '' }}
                                value="{{ $affiliation->getType() }}"
                        >
                            {{ ucfirst($affiliation->getType()) }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        @if($apiList->isNotEmpty())
            <div class="form-group row">
                <label for="datafeed-api" class="col-md-4 control-label">Api</label>
                <div class="col-md-8">
                    <select name="datafeed[api_id]"
                            class="form-control custom-select"
                            id="datafeed-api">

                        <option {{ ! $datafeed ? 'selected' : ''}} value="">
                            {{ $datafeed ? 'None api' : '-- Select Api --' }}
                        </option>
                        @foreach($apiList as $api)
                            <option {{ $datafeed && $datafeed->getApiId() === $api->getKey() ? 'selected' : '' }}
                                    value="{{ $api->getKey() }}"
                            >
                                {{ucfirst($api->getName()) }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="datafeed-key" class="col-md-4 control-label">Datafeed ID</label>
                <div class="col-md-8">
                    <input id="datafeed-key" type="text" class="form-control" name="datafeed[external_id]"
                           value="{{ $datafeed ? $datafeed->getExternalId() : '' }}" @if(!$datafeed)placeholder="Enter numeric datafeed id" @endif>
                    @if ($errors->has('datafeed.external_id'))
                        <span class="help-block">
                    <strong>{{ $errors->first('datafeed.external_id') }}</strong>
                </span>
                    @endif
                </div>
            </div>
        @endif

        @if($paymentMethods->isNotEmpty())
            <div class="form-group row">
                <label for="payment-methods" class="col-md-4 control-label">Payment Methods</label>
                <div class="col-md-8">
                    <select name="paymentMethods[]"
                            class="form-control custom-select"
                            multiple
                            id="payment-methods">

                        <option value="" {{ $store->getPaymentMethods()->isEmpty() ? 'selected' : '' }}>-- None --</option>
                        @foreach($paymentMethods as $paymentMethod)
                            <option value="{{ $paymentMethod->getKey() }}"
                                {{ $store->getPaymentMethods()->contains($paymentMethod->getKey()) ? 'selected' : '' }}
                            >
                                {{ ucfirst($paymentMethod->getName()) }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif
        <div class="form-group row">
            <label for="terms" class="col-md-4 control-label">Terms</label>
            <div class="input-group col-md-8">
                <input id="terms" type="number" class="form-control" name="terms"
                       placeholder="Enter terms" value="{{ $store->getTerms() }}" min="0" max="60">
                <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2">months</span>
                </div>
                @if ($errors->has('terms'))
                    <span class="help-block">
                    <strong>{{ $errors->first('terms') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="rating" class="col-md-4 control-label">Trustpilot rating</label>
            <div class="input-group col-md-8">
                <input id="rating" type="number" class="form-control" name="rating[rating]" step="0.1"
                       placeholder="Enter rating" value="{{ translate_rating($store->getRating()->getRating()->getRating()) }}" min="0" max="5">
                @if ($errors->has('rating.rating'))
                    <span class="help-block">
                    <strong>{{ $errors->first('rating.rating') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="ratingLink" class="col-md-4 control-label">Trustpilot url</label>
            <div class="input-group col-md-8">
                <input id="ratingLink" type="text" class="form-control" name="rating[link]"
                       placeholder="Enter trustpilot url" value="{{ $store->getRating()->getLink()->getLink() }}">
                @if ($errors->has('rating.link'))
                    <span class="help-block">
                    <strong>{{ $errors->first('rating.link') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }} row">
            <label for="logo" class="col-md-4 control-label">Logo</label>
            <div class="col-md-8 row">
                <div class="col-10">
                    @if ($store->getImage())
                        <img id="logo-image"
                             src="{{ asset($store->getImage()->getPath()) }}"
                             class="img img-fluid" alt="{{ $store->getName() }}">
                    @else
                        <img id="logo-image" src="{{ asset('images/no-image.jpg') }}"
                             class="img img-fluid" alt="{{ $store->getName() }}">
                    @endif
                </div>
                <div class="col-2">
                    <label class="btn btn-primary">
                        Browse <input id="logo" type="file" class="form-control file" name="logo"
                                      style="display: none;">
                    </label>
                    <span class="image-name"></span>
                </div>
                @if ($errors->has('logo'))
                    <span class="help-block">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
                @endif
            </div>
        </div>
    @endif
    <div class="form-group row">
        <label for="description" class="col-md-4 control-label">Description</label>
        <div class="col-md-8">
              <textarea id="description" type="text" class="tiny form-control" name="description[content]" data-url="{{ route('admin.editor.file.upload') }}">
                @if($store->getDescription())
                    {{ $store->getDescription()->getContent() }}
                @endif
            </textarea>
            @if ($errors->has('description.content'))
                <span class="help-block">
                    <strong>{{ $errors->first('description.content') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="pre-post-process d-none"></div>
    @include('backend.components.buttons.update')
</form>
