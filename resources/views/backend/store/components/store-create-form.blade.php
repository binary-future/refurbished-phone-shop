<form method="POST" action="{{ route('admin.stores.save') }}"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group row">
        <label for="name" class="col-md-4 control-label">Name</label>
        <div class="col-md-8">
            <input id="name" type="text" class="form-control" name="name"
                   placeholder="Enter store name" required>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="link" class="col-md-4 control-label">Url</label>
        <div class="col-md-8">
            <input id="link" type="text" class="form-control" name="link"
                   placeholder="Enter store link" required>
            @if ($errors->has('link'))
                <span class="help-block">
                    <strong>{{ $errors->first('link') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="guarantee" class="col-md-4 control-label">Guarantee</label>
        <div class="col-md-8 input-group">
            <input id="guarantee" type="number" class="form-control" name="guarantee"
                   value="0" min="0" max="60" required>
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">months</span>
            </div>
            @if ($errors->has('guarantee'))
                <span class="help-block">
                    <strong>{{ $errors->first('guarantee') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="is_active" class="col-md-4 control-label">Active</label>
        <div class="col-md-8">
            <select name="is_active" class="form-control custom-select" id="is_active">
                <option value="{{ \App\Domain\Store\Store::STATUS_INACTIVE }}">No
                </option>
                <option value="{{ \App\Domain\Store\Store::STATUS_ACTIVE }}">Yes
                </option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="affiliation" class="col-md-4 control-label">Affiliation</label>
        <div class="col-md-8">
            <select name="{{ \App\Domain\Store\Store::FIELD_AFFILIATION }}"
                    class="form-control custom-select"
                    id="affiliation">
                @foreach($affiliations as $affiliation)
                    <option value="{{ $affiliation->getType() }}">
                        {{ ucfirst($affiliation->getType()) }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>

    @if($apiList->isNotEmpty())
        <div class="form-group row">
            <label for="datafeed-api" class="col-md-4 control-label">Api</label>
            <div class="col-md-8">
                <select name="datafeed[api_id]"
                        class="form-control custom-select"
                        id="datafeed-api">

                    <option value="">
                        -- Select Api --
                    </option>
                    @foreach($apiList as $api)
                        <option value="{{ $api->getKey() }}"
                        >
                            {{ucfirst($api->getName()) }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="datafeed-key" class="col-md-4 control-label">Datafeed ID</label>
            <div class="col-md-8">
                <input id="datafeed-key" type="text" class="form-control" name="datafeed[external_id]"
                       placeholder="Enter numeric datafeed id">
                @if ($errors->has('datafeed.external_id'))
                    <span class="help-block">
                    <strong>{{ $errors->first('datafeed.external_id') }}</strong>
                </span>
                @endif
            </div>
        </div>
    @endif

    @if($paymentMethods->isNotEmpty())
        <div class="form-group row">
            <label for="payment-methods" class="col-md-4 control-label">Payment Methods</label>
            <div class="col-md-8">
                <select name="paymentMethods[]"
                        class="form-control custom-select"
                        multiple
                        id="payment-methods">

                    <option value="" selected>-- None --</option>
                    @foreach($paymentMethods as $paymentMethod)
                        <option value="{{ $paymentMethod->getKey() }}">
                            {{ ucfirst($paymentMethod->getName()) }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif

    <div class="form-group row">
        <label for="terms" class="col-md-4 control-label">Terms</label>
        <div class="input-group col-md-8">
            <input id="terms" type="number" class="form-control" name="terms"
                   placeholder="Enter terms" value="0" min="0" max="60">
            <div class="input-group-append">
                <span class="input-group-text" id="basic-addon2">months</span>
            </div>
            @if ($errors->has('terms'))
                <span class="help-block">
                    <strong>{{ $errors->first('terms') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }} row">
        <label for="logo" class="col-md-4 control-label">Logo</label>
        <div class="col-md-8 row">
            <div class="col-10">
                <img id="logo-image" src="{{ asset('images/no-image.jpg') }}"
                         class="img img-fluid" alt="image">
            </div>
            <div class="col-2">
                <label class="btn btn-primary">
                    Browse <input id="logo" type="file" class="form-control file" name="logo"
                                  style="display: none;">
                </label>
                <span class="image-name"></span>
            </div>
            @if ($errors->has('logo'))
                <span class="help-block">
                    <strong>{{ $errors->first('logo') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 control-label">Description</label>
        <div class="col-md-8">
              <textarea id="description" type="text" class="tiny form-control" name="description[content]" data-url="{{ route('admin.editor.file.upload') }}">
              </textarea>
            @if ($errors->has('description.content'))
                <span class="help-block">
                    <strong>{{ $errors->first('description.content') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="pre-post-process d-none"></div>
    @include('backend.components.buttons.create')
</form>
