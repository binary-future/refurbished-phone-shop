@extends('backend.layouts.app')

@section('title'){{ 'Stores: create' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Main</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.stores.main') }}">Stores</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add new store</li>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 border-bottom border-success mb-3 pb-2">
                <div class="row">
                    <div class="col-12">
                        <h3>Create new store</h3>
                    </div>
                </div>
            </div>
            <div class="col-12">
                @include('backend.store.components.store-create-form', ['affiliations' => $affiliations, 'apiList' => $apiList])
            </div>
        </div>
    </div>
@endsection
