@extends('frontend.layouts.app')

@section('title'){{ 'Accessibility | oneCOMPARE.com' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Accessibility</li>
@endsection

<style type="text/css">

.about-phone {
	position: relative;
	opacity: 0.73;
	margin-bottom: -110px!important;
	z-index: -1;

	
	}


</style>

@section('content')
    <div class="container pb-5 mt-5 pt-5">
        <h1 class="h6 text-uppercase text-muted text-left font-weight-normal mb-5">
            Accessibility
        </h1>
		
		
		<div class="row">
		
			<div class="col-md-4 col-12">
				
				<h4 class="h2 font-weight-light text-dark">We're trying...</h4>
			
			
			</div>
			
			<div class="col-md-8 col-12">
				
				<p class="lead">oneCOMPARE is committed to ensuring our website is accessible to as many visitors as possible, regardless of physical abilities or software. We have taken the following steps to achieve this goal:</p>

<strong>Standards</strong>
				<ul>
				
				<li>This site is built using XHTML 1.0 Strict. (Our mobile site is built using HTML5 and CSS3)</li>
					<li>The site uses valid CSS 2.1 for visual layout and styling.</li>
   <li> The site uses structured semantic markup. For example, table markup is used for data tables, lists are marked up using lists elements, headers are marked up as headers and so on.</li>
   <li> All links can be followed in any browser, even if scripting is turned off in the visitor's browser.</li>
					
				</ul>

<strong>Images</strong>
				
				<ul>

				<li>  Content images on the site use descriptive ALT attributes.</li>
				<li>Purely decorative graphics include null ALT attributes.</li>
					
				</ul>

<strong>Visual Design</strong>
				<ul>
    <li>All text is sized using relative sizes to allow it to be resized by users of any browser.</li>
    <li>All main content is structured so as to be readable in any browser, regardless of whether it supports images, stylesheets, scripting or plugins.</li>
				</ul>
			
			</div>
			
		</div>

</div>



@endsection
