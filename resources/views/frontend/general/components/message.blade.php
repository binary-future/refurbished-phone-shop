@if(session()->has('emailSent'))
    <div class="alert alert-success">
        {{ session()->get('emailSent') }}
    </div>
@elseif(session()->has('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div>
@endif
