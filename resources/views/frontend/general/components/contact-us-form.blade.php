{!! NoCaptcha::renderJs('en') !!}
<form method="POST" data-form-title="Contact" action="{{ route('contact.us.send') }}">
    {{ csrf_field() }}
    <div class="form-group">
        <input type="text" class="form-control font-weight-light" name="name" required=""
               placeholder="First name*" data-form-field="Name" value="{{ old('name') }}">
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <input type="email" class="form-control font-weight-light" name="email" required=""
               placeholder="Email*" data-form-field="Email" value="{{ old('email') }}">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <textarea class="form-control font-weight-light" name="message"
            placeholder="Message / suggestion / bug..." rows="7"
            data-form-field="Message">{{ old('message') }}</textarea>
        @if ($errors->has('message'))
            <span class="help-block">
                <strong>{{ $errors->first('message') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group">
        <div class="col-md-6 col-md-offset-4">
            {!! NoCaptcha::display() !!}
            @if ($errors->has('g-recaptcha-response'))
                <span class="help-block">
                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div>
        <button type="submit" class="btn btn-secondary float-right">Get in touch <i
            class="fal fa-smile"></i></button>
    </div>
</form>
