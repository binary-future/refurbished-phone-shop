@extends('frontend.layouts.app')

@section('title'){{ 'About Us, oneCompare.com Reviews | oneCompare' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">About oneCompare.com</li>
@endsection

<style type="text/css">

.about-phone {
	position: relative;
	opacity: 0.73;
	margin-bottom: -110px!important;
	z-index: -1;


	}


</style>

@section('content')
    <div class="container pb-5 mt-5 pt-5">
        <h1 class="h6 text-uppercase text-muted text-left font-weight-normal mb-5">
            About oneCompare
        </h1>


		<div class="row">

		<div class="col-md-6 col-12">

			<h4 class="h2 font-weight-light text-dark">We're crazy about tech...</h4>

            @include('frontend.components.common.lazy-image', [
                'class' => 'about-phone img-fluid d-md-block d-none animated slideInUp',
                'lazySrc' => '/images/about-phone.png',
                'alt' => ''
            ])
		</div>

		<div class="col-md-6 col-12 my-auto">
			<p class="lead">oneCompare might be new around these parts, but we're determined to make our mark by helping you save money on your next device.</p>

			<p>Whether it's an iPhone, Samsung Galaxy or something a little different to the norm, oneCompare checks prices on over 10,000 deals every hour of the day. </p>

				<p>By using our innovative sliders you're sure to find the right deal for you - no other website gives you access to so many filters &amp; choices at your fingertips! Give us a try and see for yourself how easy it is.</p>

		</div>

		</div>

    </div>


<div class="container-fluid mt-5 text-white bg-dark pt-5 pb-5" style="z-index: 1;">

	<div class="container pt-5 pb-5">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-12 text-center p-5">

				<i class="fal fa-acorn fa-4x mx-auto mb-4 "></i><br>

				<h5 class="mb-3">Nuts about phones!</h5>
				<p>It helps to love what you do, and the oneCompare team are pretty mad on tech.</p>


			</div>

			<div class="col-md-3 col-sm-6 col-12 text-center p-5">

				<i class="fal fa-box-check fa-4x mx-auto mb-4 "></i><br>

				<h5 class="mb-3">In-stock checks</h5>
				<p>If the product you want isn't in stock, we'll let you know (and you can pre-order).</p>


			</div>

			<div class="col-md-3 col-sm-6 col-12 text-center p-5">

				<i class="fal fa-user-headset fa-4x mx-auto mb-4 "></i><br>

				<h5 class="mb-3">UK customer support</h5>
				<p>We're here if you need us, <a href="/contact" style="color: inherit;">get in touch using the contact page</a>.</p>


			</div>

			<div class="col-md-3 col-sm-6 col-12 text-center p-5">

				<i class="fal fa-wheelchair fa-4x mx-auto mb-4 "></i><br>

				<h5 class="mb-3">Accessibility approved</h5>
				<p>The oneCompare website complies with the latest web standards in accessibility.</p>


			</div>



		</div>

	</div>


</div>


<div class="container-fluid bg-white">

<div class="container mt-5 mb-5 pt-5 pb-5">
	<div class="row mb-5 pb-5">

        <div class="col-md-4 col-12">
            <h4 class="h2 font-weight-light text-dark">Our partners</h4>
        </div>

	<div class="col-md-8 col-12 h-100">

		<div class="row mx-auto text-center my-auto align-items-center">
			<div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/ee.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/three.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                'class' => 'text-center my-auto mt-3',
                'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/bt-mobile.svg',
                'alt' => '',
                'style' => 'max-width: 80px; max-height: 80px;'
            ])
            </div>
		</div>

		<div class="row mx-auto text-center my-auto align-items-center">
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/giffgaff.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                'class' => 'text-center my-auto mt-3',
                'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/id.svg',
                'alt' => '',
                'style' => 'max-width: 80px; max-height: 80px;'
            ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/sky-mobile.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
		</div>

        <div class="row mx-auto text-center my-auto align-items-center">
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/virgin-media.png',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/o2.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/fonehous-co-uk.png',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
		</div>

        <div class="row mx-auto text-center my-auto align-items-center">
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'rounded text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/buy-mobiles.jpg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/affordable-mobiles.png',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'rounded text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/carphone-warehouse.png',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
		</div>

        <div class="row mx-auto text-center my-auto align-items-center">
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3 p-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/networks/vodafone.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/mobile-phones-direct.png',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/mobilescouk.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
		</div>

        <div class="row mx-auto text-center my-auto align-items-center">
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/e2save.svg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/tesco-mobile.jpg',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
            <div class="col-4 mt-3">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'text-center my-auto mt-3',
                    'lazySrc' => 'https://j4y2v6g3.stackpathcdn.com/storage/images/logos/stores/metrofone.png',
                    'alt' => '',
                    'style' => 'max-width: 80px; max-height: 80px;'
                ])
            </div>
		</div>

	</div>

	</div>


	<div class="alert alert-primary mt-5 p-3 mb-5 w-75 mx-auto text-center">
        <strong>Psst.... Got a question?</strong><br>
        Don't forget to <a href="/help">check the FAQs page!</a>
    </div>
</div>


</div>



@endsection
