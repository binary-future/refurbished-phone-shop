@extends('frontend.layouts.app')

@section('title'){{ 'Contact Us' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 justify-content-center d-flex">
                @include('frontend.general.components.message')
            </div>
            <div class="col-lg-12 mb-5">
                <h1 class="text-center font-weight-light h3">Get in touch</h1>
                <p class="w-50 text-center mx-auto alert alert-info">If you have a question or want to suggest a feature then please get
                    in touch with a member of the team using the details below.</p>
            </div>
        </div>
        <section class="mt-1">
            <div class="row">
                <div class="col-sm-12 mx-auto mb-5 pb-2">
                    <h5 class="mt-5 mb-3">Contact details</h5>
                    <p> General enquiries - Read the <a href="{{ route('help') }}" title="Frequently Asked Questions"><i
                                    class="fal fa-headset"></i> FAQs</a> first, then use the below form; the usual response time is 4 hours (during
                        normal UK working hours). </p>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 mx-auto border border-light p-3 rounded">
                           @include('frontend.general.components.contact-us-form')
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
