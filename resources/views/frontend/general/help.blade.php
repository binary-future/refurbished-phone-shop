@extends('frontend.layouts.app')

@section('title'){{ 'FAQs & Advice' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">FAQs & Advice</li>
@endsection

@section('content')
    <div class="container pb-5">
        <div class="row">
            <div class="col-md-6 font-weight-light text-center mx-auto">
                <h1 class="h2 text-center font-weight-bold mb-2">
                    FAQs &amp; Advice
                </h1>
                <h2 class="h5 font-weight-light mb-4 text-muted">Need some guidance, we are here. All the most common
                    questions &amp; answers at your fingertips <i class="fal fa-hand-point-down"></i></h2>
            </div>
            <div class="w-100">
                <ol class="faq list-unstyled pt-3 pb-4">
                    <li>
                        <h3 class="mt-5">Do you compare the entire market?</h3>
                        We try to, but not every vendor wants to work with us sadly. At the last count, we compared
                        prices from 12 different online stores - with big names such as o2, Mobiles.co.uk, Carphone
                        Warehouse and many more. In fact, according to our internal research, <strong>oneCOMPARE looks
                            at prices from more websites and vendors than our nearest large competitor</strong> - and we
                        are adding more stores all the time.
                    </li>
                    <li>
                        <h3 class="mt-5">Can I buy a contract from another network?</h3>
                        Absolutely - for a few years now, all mobiles phone contracts must have been sold as "unlocked"
                        so if you've signed up to another network in the past, you can unlock your handset and change
                        providers/networks at any time. You can keep your number too.
                    </li>
                    <li>
                        <h3 class="mt-5">Do you sell SIM-only contracts?</h3>
                        Not yet - but we will shortly. Please keep checking back.

                    </li>
                    <li><h3 class="mt-5">Some of the information you are displaying is incorrect / I noticed a bug</h3>
                        oneCOMPARE is still in 'beta' so there may be bugs, glitches and other issues. Any feedback or
                        bug reports are most welcome via the <a href="{{ route('contact.us') }}">contact page</a>.
                    </li>
                    <li>
                        <h3 class="mt-5">Do you sell mobile phones directly?</h3>
                        No. We are a mobile contract comparison service - you buy the mobile and/or the contract from
                        the vendor directly. We do not sell any mobiles directly to you - if you have a query about a
                        handset or contract then you will need to contract the vendor or network who sold it to you.
                    </li>
                    <li>
                        <h3 class="mt-5">Do you get paid commission?</h3>

                        For some deals that are posted on oneCOMPARE, we may receive a small commission. However unlike
                        other websites we do NOT promote specific deals over others based on how much commission we
                        receive. The amount we get paid (if we do) does not make any difference at all to the sorting of
                        the deals listed on our website - fact!
                    </li>
                    <li>
                        <h3 class="mt-5">I have other questions - how do I get in touch?</h3>

                        The easiest and fastest way is to use <a href="{{ route('contact.us') }}">the Contact Form here</a>. The
                        oneCOMPARE team aim to reply within 1 business day to all queries.
                    </li>

                </ol>
                <div class="mt-4 mb-4">
                    <h4>Incoming search terms:</h4>
                    <ul class="faq">
                        <li><a href="{{ route('manufacturers.view', ['brand' => 'apple']) }}">iPhone UK deals</a></li>
                        <li><a href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-7-plus']) }}">iPhone 7 Plus deals</a></li>
                        <li><a href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s8']) }}">Galaxy S8 deals</a></li>
                        <li><a href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s10e']) }}">Galaxy S10e deals UK</a></li>
                        <li><a href="/mobile-news/">News</a></li>
                        <li><a href="{{ route('manufacturers.view', ['brand' => 'sony']) }}">SONY deals &amp; handsets</a></li>
                        <li><a href="{{ route('manufacturers.view', ['brand' => 'google']) }}">UK Google Pixel deals</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
