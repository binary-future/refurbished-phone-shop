@extends('frontend.layouts.app')

@section('title'){{ 'Our terms | oneCompare' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Terms &amp; Conditions</li>
@endsection

@section('content')
    <div class="container pb-5 mt-5 pt-5">
        <h1 class="h6 text-uppercase text-muted text-left font-weight-normal mb-5">
           Terms &amp; Conditions
        </h1>
		
		
		<div class="row">
		
			<div class="col-md-4 col-12">
				
				<h4 class="h2 font-weight-light text-dark">Boring legal stuff...</h4>
			
			
			</div>
			
			<div class="col-md-8 col-12">
				
				<p class="lead">
				These Terms and Conditions outline the rules and regulations of your use of this Website. oneCompare is a free resource and is owned and created by Uffiliate Ltd, a limited company registered in England &amp; Wales, whose registered office is: Platt Barn, Bullen Farm, East Peckham, Kent, TN12 5LX, United Kingdom, and whose VAT registration number is: GB159663958.</p>

<p class="font-weight-bold">By accessing this Website we assume you accept these Terms and Conditions. Do not continue to use this Website if you do not agree to all of the Terms and Conditions stated on this page.</p>

<h3>Definitions</h3>
<p>The following terminology applies to these Terms and Conditions and all further Agreements unless otherwise noted:<br>
 - "Client", "You" and "Your" refers to you, the individual accessing this Website. <br>
- "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company, as listed in paragraph 1. <br>
- "Party", "Parties", or "Us", refers to both the Client and ourselves. <br>
- "The Website", "oneCompare" or simply "Website" refers to this website, accessible at https://www.onecompare.com<br><br>


Any use of the above terminology or other words in the singular, plural, capitalisation and/or he/she or they, are taken as interchangeable and therefore as referring to the same.</p>

<h3>Cookies</h3>

<p>We employ the use of cookies. By accessing oneCompare, you agreed to use cookies in agreement with the Uffiliate Ltd's Privacy Policy.</p>

<p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our Website to enable the functionality of certain areas to make it easier for people visiting our Website. Some of our affiliate/advertising partners may also use cookies. You may read our <a href="/privacy">Cookie Policy</a> for more information.</p>

<h3>License</h3>

<p>Unless otherwise stated, Uffiliate Ltd and/or its licensors own the intellectual property rights for all material on the Website. All intellectual property rights are reserved. You may access the Website for your own personal use subject to the restrictions set in these Terms and Conditions.</p>

<p>You must not:</p>
<ul>
    <li>Republish material from oneCompare</li>
    <li>Sell, rent or sub-license material from oneCompare</li>
    <li>Reproduce, duplicate or copy material from oneCompare</li>
    <li>Redistribute content from oneCompare</li>
</ul>

<h3>iFrames</h3>

<p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>

<h3>Content Liability</h3>

<p> The material on the Website is provided "as is", without any conditions, warranties or other terms of any kind. We do not guarantee that the Website, or any content on it, will be free from errors or omissions or that the Website, or any content on it, will always be available or be uninterrupted. Access to the Website is permitted on a temporary basis. We may suspend, withdraw, discontinue or change all or any part of the Website without notice. We will not be liable to you if for any reason the Website is unavailable at any time or for any period. </p>
				
				<p>You are solely responsible for your interactions with merchants. You hereby release Us from any and all claims or liability related to any product or service of a merchant, any action or inaction by a merchant, including a merchant's failure to comply with applicable law. </p>
				
				<p>In no event shall oneCompare be liable in contract, tort (including negligence) or otherwise for any loss of profits, goodwill, savings or business (whether any of the foregoing are direct or indirect), or for any special, indirect, incidental or consequential damages. </p>
				
				<p>The information provided on this Website is intended as "information only" and does not constitute advice. Therefore, it must not be relied on to assist in making or refraining from making a decision, or to assist in deciding on a course of action. </p>
				
				<p>We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other proprietary material due to your use of the Website or to your downloading of any content on it, or on any website linked to it or to any email sent by oneCompare to you. </p>
				
				<p>Any failure by any party to these Terms and Conditions to enforce at any time any term or condition under these Terms and Conditions will not be considered a waiver of that party's right thereafter to enforce each and every term and condition of these Terms and Conditions. </p>

<h3>Term and Termination</h3>

<p>These Terms and Conditions will remain in full force and effect while you are a User of the Website.

<p> We may, at any time and for any reason, terminate these Terms and Conditions with you and deny you access to the Website.</p>

<p> In the event of termination of these Terms and Conditions, and for any reason, you must not attempt to use the Website. </p>

<h3>General Terms</h3>

<p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us at any moment. We will consider requests to remove links but we are not obligated to, nor to respond to you directly.</p>

<p class="lead">We do not guarantee that the information on this Website is correct. You are strongly advised to double-check in particular the details of any offer(s) that are presented to you, before proceeding to purchasing this offer. For the avoidance of doubt this must include verifying pricing information, any contract term(s), any promotion(s) or discount(s), inclusive gift(s), any general terms, and so on. </p>
	
	<p>We obtain our data from various third-party sources and provide it to you, without cost, on an E&amp;OE basis. Any omissions or errors therein are likely the responsibility of the provider of that data. Customers must purchase the deal directly from the retailer's website - no transactions take place on our Website. Deals and vouchers offered are subject to availability or specific terms and conditions and may not be available to all customers. You must refer to individual partner’s terms and conditions before applying or purchasing a product(s) from one of our partner(s), and you hereby accept that you must additionally undertake all due diligence and fully verify the accuracy of the product(s) prior to purchasing from, or entering into a contract with, our partner(s).</p>
				
<h3>Affiliate disclosure</h3>
				
<p>We may receive a small commission when you click our links and subsequently make a purchase from one of our partner(s). However, this does not impact how we present this data to you. We will always aim to keep things fair and balanced, in order to help our consumers make the best choices.</p>

<h3>Disclaimer</h3>

<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our Website and the use of this Website. Nothing in this disclaimer will:</p>

<ul>
    <li>limit or exclude our or your liability for death or personal injury;</li>
    <li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>
    <li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>
    <li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>
</ul>

<p>The limitations and prohibitions of liability set in this section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>

<p> These Terms and Conditions and their performance shall be governed by and construed in accordance with the laws of England and Wales and the parties hereby submit to the exclusive jurisdiction of the courts of England and Wales. </p>

				
				<p>If any provision of these Terms and Conditions is declared void, illegal or unenforceable, the remainder of these Terms and Conditions will be valid and enforceable to the extent permitted by applicable law. In such event, the parties agree to use their best efforts to replace the invalid or unenforceable provision by a provision that, to the extent permitted by the applicable law, achieves the purposes intended under the invalid or unenforceable provision. </p>
				
			<p>Nothing in these Terms and Conditions shall give, directly or indirectly, any third party any enforceable benefit or any right of action against oneCompare and such third parties shall not be entitled to enforce any term of these Terms and Conditions against oneCompare. </p>	
				
				<p class="text-muted small float-right">Version  1.03</p>
			
			</div>
			
		</div>

</div>



@endsection
