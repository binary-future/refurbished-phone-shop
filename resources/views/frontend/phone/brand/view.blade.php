@extends('frontend.layouts.app')

@section('title') {!! sprintf('%s Deals &amp; Contracts 2019, 🥇 𝗖𝗼𝗺𝗽𝗮𝗿𝗲 𝗖𝗵𝗲𝗮𝗽𝗲𝘀𝘁 𝗠𝗼𝗻𝘁𝗵𝗹𝘆 𝗢𝗳𝗳𝗲𝗿𝘀', brand_name($brand->getName())) !!}@endsection
@section('description'){{ sprintf('Find cheap %s mobile phone deals at oneCompare ✅ Compare the cheapest UK contracts NOW...', $brand->getName() === 'Apple' ? 'iPhone' : $brand->getName())}}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Mobile Contracts</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.main') }}">Makes</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ brand_name($brand->getName()) }} deals</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 py-3 py-md-5 bg-millennial-pink-2 text-dark">
                <div class="container">
                    <div class="row">

						 <header class="col-12 mb-2">
                             <h1 class="text-center text-md-left h2">Compare cheap {{ brand_name($brand->getName()) }} mobile deals</h1>
							 <span class="h4 text-muted text-center text-md-left">Choose from <span class="highlight px-1">630+</span> pay monthly contracts</span>
						    @include('frontend.phone.components.brand.brand-info', ['brand' => $brand, 'models' => $chunkedModels])
                        </header>
                        <div class="col-12 my-auto d-none d-md-block text-center">
							<p class="lead">Compare the cheapest pay monthly deals from 1,000s of {{ brand_name($brand->getName()) }} deals. oneCompare check prices every hour of the day from all the leading retailers. More filters, colours and options than other websites! Choose your handset above, or refine your filters to find a perfect UK contract offer.</p>
                        </div>
                        @if($lastChecked)
                            @include('frontend.phone.components.brand.brand-lastchecked-date', ['lastCheckedRss' => $brand->getUpdatedAt()->toRssString(), 'lastCheckedDisplayable' => $lastChecked])
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


<div class="container-fluid py-2" id="model-deals-wrap" style="background: #F5F1ED;">
    <main class="container" role="main">
        <div class="row">
            <div class="col-3">
                @include('frontend.phone.components.brand.brand-view-side-bar', ['filters' => $sideBarFilters, 'sortByDropdown' => $sortByDropdown])
            </div>
            <div class="col-lg-9 col-12">
                <div class="row">
                    <div class="col-12 text-right d-lg-block d-none">
                        <div class="row justify-content-end">
                            @include('frontend.components.dropdowns.deals-sort-by', ['select' => $sortByDropdown])
                        </div>

                    </div>
                    <div class="col-12" id="model-deals" data-href="{{ route('manufacturers.view.filter', ['brand' => $brand->getSlug()]) }}">
                        @include('frontend.deals.components.deals.deals-list', ['deals' => $deals, 'phones' => $phones])
                    </div>
                </div>
            </div>
            @if($brand->getDescription())
		</div>
	</main>
</div>
<div class="container-fluid">
	<div class="row">
                <article class="col-12 bg-dark py-5 seo-desc" role="article">

                    @include('frontend.phone.components.brand.brand-description', ['brand' => $brand, 'description' => $brand->getDescription()])
                </article>
            @endif
            @include('frontend.components.bars.loading-bar-search')
        </div>
    </div>
    @include('frontend.deals.components.deals.deals-info-modal')
@endsection

@push('js')
    <script type='text/javascript'>
        /* <![CDATA[ */
        var rbl_blockAdverts = {"blockG":"1","rules":[]};
        /* ]]> */
    </script>
    <script src="{{ asset('/js/anti.js') }}"></script>
@endpush
