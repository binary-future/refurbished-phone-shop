@extends('frontend.layouts.app')

@section('title'){{ 'All Makes - Find your next pay monthly contract deal'}}@endsection

@section('description'){{ 'Browse by manufacturer to find your next pay monthly contract deal. From Apple to Xiaomi - we have every major brand of mobile phone maker here at oneCompare. Find best deals now.'}}@endsection


@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Mobile Contracts</a></li>
    <li class="breadcrumb-item active" aria-current="page">Makes - All</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 mx-auto text-center mb-5">
                <h1 class="text-center h3 mb-2">Find a phone - browse by make</h1>
				<h2 class="text-muted h6">Compare pay monthly contracts and deals from over 25 top brands including Apple, Huawei, LG, Samsung &amp; more. Select your preferred brand to start.</h6>
            </div>
			
						
			
				<div class="col-md-12 col-12 mb-5 bg-millennial-pink-2 py-5 rounded text-white">
					<h3 class="text-dark h5 text-center mb-4"><i class="fad fa-award pr-1"></i> Most popular deals:</h3>
					<div class="row">
						<div class="col-md-6 col-12 mx-auto text-center text-muted">
							<ul class="faq lead">

								<li><a href="/apple/iphone-xs-deals">iPhone Xs</a> by <a href="/apple">Apple</a></li>
								<li><a href="/apple/iphone-xr-deals">iPhone Xr</a> by <a href="/apple">Apple</a></li>
								<li><a href="/samsung/galaxy-note-10-deals">Galaxy Note 10</a> by <a href="/samsung">Samsung</a></li>
								<li><a href="/huawei/p20-deals">P20 deals</a> by <a href="/huawei">Huawei</a></li>
								<li><a href="/samsung/galaxy-s9-deals">Galaxy S9</a>  by <a href="/samsung">Samsung</a></li>
							</ul>

						</div>
					
						<div class="col-md-6 col-12 mx-auto text-center text-muted">
							<ul class="faq lead">
								<li><a href="/samsung/galaxy-s8-plus-deals">Samsung Galaxy S8 Plus</a></li>
								<li><a href="/huawei/mate-20-x-deals">Huawei Mate 20 X</a></li>
								<li><a href="/apple/iphone-se-deals">Apple iPhone SE</a></li>
								<li><a href="/huawei/honor-20-deals">HONOR 20 contracts</a></li>
								<li><a href="/huawei/p30-pro-deals">Huawei P30 Pro deals</a></li>
							</ul>
						</div>

				  </div>
			</div>
			
            <div class="col-md-12 col-12">
									<h3 class="text-dark h5 text-center mb-4">Or browse by make:</h3>

                @include('frontend.phone.components.brand.brand-list', ['brands' => $brands])
            </div>
			

        </div>
    </div>
@endsection