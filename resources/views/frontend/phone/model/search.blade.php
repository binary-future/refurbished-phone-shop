@extends('frontend.layouts.app')

@section('title'){{ 'Search results | oneCompare' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ 'Search' }}</li>
@endsection

@section('content')
   
    <div class="container">
        <div class="row">
            @if($models->count())
                <div class="col-12 my-2">
                    <h1 class="text-center h4"><i class="fad fa-search pr-2"></i> Found {{ $models->total() }} {{ $models->total() > 1 ? 'devices' : 'device' }}...</h1>
                </div>
                <div class="row col-12 mt-4 py-5 bg-millennial-pink-2">
                    @include('frontend.phone.components.models.models-list', ['models' => $models])
         
                    {{ $models->links() }}
			     </div>
					<p class="font-weight-bold text-center mx-auto my-4">Not what you wanted? <a href="/">Try another search...</a></p>
             
            @else
                <div class="col-12 my-4 py-3 text-center">
                    <h3 class="text-center pb-3">Nooooo! No phone(s) found <i class="fad fa-frown"></i></h3>
					
					<p class="lead">Make sure that you're typing only the phone/device name, for example <span class="font-weight-bold">"Galaxy S9"</span> - not "Galaxy S9 deals". Try searching again using the search box. </p>
					
					<p>If you're still lost, then try <a href="https://www.onecompare.com/manufacturers"><strong>browsing by manufacturer </strong><i class="fal fa-angle-right align-middle pb-1"></i> </a></p>
                </div>
            @endif
        </div>
    </div>

 

@endsection