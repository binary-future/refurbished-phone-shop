@extends('frontend.layouts.app')

@section('title'){!! filter_content($pageTitle) !!}@endsection
@section('description'){{ filter_content(sprintf('Buying a refurbished %s %s? ✅ Compare Sim Free prices from over 20 websites. ✓ Unlocked, second-hand price comparison', $brand->getName(), $model->getName() )) }}@endsection

@push('dns-prefetch')
    <link rel="dns-prefetch" href="//www.youtube.com"/>
    <link rel="dns-prefetch" href="//static.doubleclick.net"/>
@endpush

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.main') }}">Makes</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.view', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }}</a></li>
   <!--  <li class="breadcrumb-item active d-none d-md-block" aria-current="page">{{ $model->getName() }} refurbished deals</li> -->
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            @include('frontend.components.bars.loading-bar-search')
           <div class="bg-green w-100 shadow-lg">
			<div class="col-12 __prod-hero py-4">
				    @if(! \Request::is('/'))
    @include('frontend.components.breadcrumbs.breadcrumbs')
    @endif

					<div class="container">
					@include('frontend.phone.components.models.model-info', [
						'brand' => $brand,
						'model' => $model,
						'phones' => $phones,
						'filters' => $phonesFilter ?? null,
						'lastChecked' => $lastChecked,
						'phoneImages' => $phoneImages,
					])
					</div>
				</div>
            </div>

		</div>
    </div>

    <div class="container-fluid" style="background: #fcfcfc;">
		<div class="container">
            <main class="row">
                <aside class="col-md-3 col-12">
                    @include('frontend.phone.components.models.model-side-bar', ['filters' => $sideBarFilters])
                </aside>
                <div class="col-lg-9 col-12" id="model-deals-wrap">
                    <div class="row">
                        <div class="col-12 text-right d-lg-block d-none">
                            <div class="row justify-content-end">
                                @include('frontend.components.dropdowns.deals-sort-by', ['select' => $sortByDropdown])
                            </div>
                        </div>
                        <div class="col-12" id="model-deals" data-href="{{ route('models.view.filter', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">
                            @include('frontend.deals.components.deals.deals-list', ['deals' => $deals, 'phones' => $phones])
                        </div>
                    </div>
                </div>
                @if($model->getDescription())
                    <div class="col-12 my-2 my-md-5">
                        @include('frontend.phone.components.models.model-description', ['model' => $model, 'description' => $model->getDescription()])
                    </div>
                @endif
            </main>
        </div>
    </div>

    @if(! empty($specs))
    <div class="container-fluid bg-blue py-5">
        <div class="container bg-light pt-4 rounded">
            <div class="row">
                <div class="col-12">
                    @include('frontend.phone.components.models.specs-section', ['model' => $model, 'brand' => $brand, 'specs' => $specs])
                </div>
            </div>
        </div>
    </div>
    @endif

    @if($model->getFaqs()->isNotEmpty())
        <div class="container-fluid bg-blue py-5">
            <div class="container bg-light pt-4 rounded">
                <div class="row">
                    <div class="col-12">
                        @include('frontend.phone.components.models.faq-deals', ['model' => $model])
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if($dealsInfo->isValid())
        <div class="container-fluid bg-secondary py-5">
            <div class="container bg-light p-4 p-sm-5 rounded-sm">
                @include('frontend.phone.components.models.contracts-deals-faq', ['dealsInfo' => $dealsInfo, 'model' => $model, 'brand' => $brand])
            </div>
        </div>
    @endif
    @include('frontend.deals.components.deals.deals-info-modal')
@endsection

@push('js')
    @include('frontend.phone.components.models.product-schema', ['model' => $model, 'brand' => $brand, 'dealsInfo' => $dealsInfo, 'image' => $model->getImage()])
    <script type='text/javascript'>
        /* <![CDATA[ */
        var rbl_blockAdverts = {"blockG":"1","rules":[]};
        /* ]]> */
    </script>
    <script src="{{ asset('/js/anti.js') }}"></script>
@endpush
