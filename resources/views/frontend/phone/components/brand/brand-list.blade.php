<div class="row">
    @foreach($brands as $brand)
        @include('frontend.phone.components.brand.brand-item', ['brand' => $brand])
    @endforeach
</div>