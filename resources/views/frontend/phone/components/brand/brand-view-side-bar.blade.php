@include('frontend.phone.components.models.model-filters-mobile', ['filters' => $filters, 'sortByDropdown' => $sortByDropdown])
<div class="row mt-5">
    <div class="col-6 d-none d-lg-block">
        <h5 class="font-weight-bold text-left border-bottom border-success pb-3">Filter by:</h5>
    </div>
    <div class="col-6 text-right d-none d-lg-block">
        <a href="#" id="filters-clear" class="font-weight-light d-none btn btn-outline-secondary btn-sm "><i class="fal fa-undo-alt pr-1 "></i> Reset</a>
    </div>
    <div class="col-12 d-none d-lg-block">
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getModels(), 'template' => 'frontend.components.filters.filter-phone-models'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getConditions(), 'template' => 'frontend.components.filters.filter-condition'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getMonthlyCostSlider(), 'template' => 'frontend.components.filters.filter-monthly-cost-slider'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getMonthlyCost(), 'template' => 'frontend.components.filters.filter-monthly-cost'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getTotalCostSlider(), 'template' => 'frontend.components.filters.filter-total-cost-slider'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getTotalCost(), 'template' => 'frontend.components.filters.filter-total-cost'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getNetworks(), 'template' => 'frontend.components.filters.filter-network'])
    </div>
</div>
