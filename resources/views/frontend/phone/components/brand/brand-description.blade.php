
<div class="col-md-8 offset-md-2 col-12 container-seo bg-white p-3 p-md-5 shadow-sm rounded text-justify text-md-left ">
	<h2 class="font-weight-normal h3 pt-4 my-4">{{ $brand->getName() }} contract deals: What you need to know</h2>
    {!! $description->getContent() !!}
</div>
