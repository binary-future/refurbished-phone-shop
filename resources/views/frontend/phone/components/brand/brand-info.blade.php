<div class="row row-eq-height">
   
    <div class=" col-12 py-5">
        <div class="row justify-content-center align-items-center">
                @include('frontend.phone.components.models.related-models-section', ['models' => $chunkedModels])
         
        </div>
    </div>
</div>