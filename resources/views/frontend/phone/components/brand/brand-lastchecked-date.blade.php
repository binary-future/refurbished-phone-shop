<div class="w-100 text-right text-muted-more small d-none d-lg-block">
    <i class="fal fa-clock"></i> Prices last checked:
    <span class="date">
        <time itemprop="lastReviewed" datetime="{{ $lastCheckedRss }}">{{ $lastCheckedDisplayable }}</time>
    </span>
</div>
