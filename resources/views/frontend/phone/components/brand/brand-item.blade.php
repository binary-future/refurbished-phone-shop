<a href="{{ route('manufacturers.view', ['brand' => $brand->getSlug()]) }}">
	<div class="col-6 col-md-4 col-lg-3 col-sm-6 mb-2">
		<div class="card text-dark bg-light mb-3 mx-0 mx-sm-1 border border-muted rounded h-100 shadow-sm">
			<div class="card-body bg-white text-center">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'card-img-bottom card-img mx-auto my-2',
                    'lazySrc' => image_path($brand->getImage()),
                    'alt' => '',
                ])
			</div>
			<div class="card-footer text-center bg-light">
				<a class="btn-sm text-dark font-weight-normal" href="{{ route('manufacturers.view', ['brand' => $brand->getSlug()]) }}">View {{ $brand->getModels()->count() }} {{ $brand->getName() }} devices <i class="fad fa-angle-double-right pl-1"></i> </a>

			</div>
		</div>
	</div>
</a>
