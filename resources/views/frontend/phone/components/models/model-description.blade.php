<article class="w-100 container-seo bg-white rounded shadow-sm my-4 p-3 p-md-5 text-dark text-justify text-md-left text-justify">
  <h2 class="h3 font-weight-normal my-4"> About {{ $brand->getName() }} {{ $model->getName() }} - Review, Specs + more</h2>
  {!! $description->getContent() !!} </article>
