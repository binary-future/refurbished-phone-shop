<div id="relative-models" class="carousel slide" data-ride="ride">
    <ol class="carousel-indicators">
        @foreach($models as $chunkNumber => $chunk)
            <li data-target="#relative-models" data-slide-to="{{ $chunkNumber }}"  @if($loop->first)class="active"@endif></li>
        @endforeach
    </ol>

    <div class="carousel-inner">
        @foreach($models as $chunk)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <div class="w-100 text-center">
                    <div class="row row-eq-height">
                        @foreach($chunk as $model)
                            <div class="col hcarousel-model-item d-flex" >
                                <div class="row ">
                                    <div class="col-12 align-self-start text-white">
<div class="text-center">               <h6 class="text-muted">{{ $model->getBrand()->getName() }}</h6>
                                        <h6 class="font-weight-bold text-muted">{{ $model->getName() }}</h6></div>
                                    </div>
                                    <div class="col-12 align-self-center">
                                        <a href="{{ route('models.view', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()]) }}">
                                            @include('frontend.components.common.lazy-image', [
                                                'class' => 'img-thumbnail no-bg',
                                                'lazySrc' => image_path($model->getThumb()),
                                                'alt' => $model->getName(),
                                                'style' => 'max-height: 200px'
                                            ])
                                        </a>
                                    </div>
                                    <div class="w-100 align-self-end mt-2">
                                        <a href="{{ route('models.view', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()]) }}" class="btn btn-outline-secondary btn-sm stretched-link">See deals</a>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</div>
