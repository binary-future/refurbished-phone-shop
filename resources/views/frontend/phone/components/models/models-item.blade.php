<div class="col-6 col-lg-3 col-md-4" style="overflow:hidden">
	<a href="{{ route('models.view', ['brand' => isset($brand) ? $brand->getSlug() : $model->getBrand()->getSlug(), 'model' => $model->getSlug()]) }}" title="SIM-Free {{ $model->getName() }}"  class="text-dark font-weight-bold">
    <div class="card text-dark bg-white mb-5 mt-0 rounded-lg shadow border-hover">

        <div class="card-body bg-white text-center">
            @include('frontend.components.common.lazy-image', [
                'class' => 'img-fluid mx-auto my-1',
                'lazySrc' => thumb_image($model),
                'alt' => 'Found ' . $model->getPhones()->count() . ' models',
                'style' => 'max-height:122px;'
            ])
        </div>
        <div class="pb-3 my-2 text-center bg-white h6">

         Refurbished {{ $model->getName() }} <i class="fal fa-angle-right small pl-1 align-middle"></i>

        </div>

    </div>
	</a>
</div>

