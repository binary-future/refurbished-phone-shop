<div class="row">
    <div class="col-lg-2 col-4 my-auto mr-auto">
        @include('frontend.components.common.lazy-image', [
            'class' => 'img img-fluid default-phone phone-color-image',
            'lazySrc' => image_path($model->getImage()),
            'alt' => $model->getName()
        ])
        @if ($model->getImage())
            @foreach($phoneImages as $phoneImage)
                @include('frontend.components.common.lazy-image', [
                    'class' => 'img img-fluid d-none phone-color-image phone-color-' . $phoneImage->getColor(),
                    'lazySrc' => image_path($phoneImage),
                    'alt' => $model->getName()
                ])
            @endforeach
        @endif

    </div>
    <div class="col-lg-10 col-8 my-auto pl-3">

        <h1 class="text-left text-black font-weight-bold">{{ $model->getName() }} Refurbished
            @if(auth()->user())
                <a href="{{ route('admin.models.view', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}" class="btn btn-outline-success"><i class="fad fa-edit"></i></a>
            @endif

        </h1>
        <h2 class="h4 text-black d-none d-md-block">Compare used {{ $brand->getName() }} {{ $model->getName() }} &amp; SIM-Free prices</h2>
        <div class="row">
              <div class="col-12">@include('frontend.components.filters.filter-container', ['filters' => $filters->getColors(), 'template' => 'frontend.components.filters.filter-phone-colors'])</div>
            <div class="col-12">@include('frontend.components.filters.filter-container', ['filters' => $filters->getCapacity(), 'template' => 'frontend.components.filters.filter-phone-capacity'])</div>
        </div>

        @if($lastChecked)
            @include('frontend.phone.components.models.model-lastchecked-date', ['lastCheckedRss' => $model->getUpdatedAt()->toRssString(), 'lastCheckedDisplayable' => $lastChecked])
        @endif
    </div>
 </div>

