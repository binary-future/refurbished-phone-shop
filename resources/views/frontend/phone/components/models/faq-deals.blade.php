<section class="row large">
    <div class="col-12 p4 p-md-5">
        <h2 class="text-center text-md-left h3 font-weight-normal"><i class="fad fa-pie text-muted pr-1"></i>FAQs</h2>
        <ul class="my-3 pl-4">
            @foreach($model->getFaqs() as $ind => $faq)
                @if(! empty($faq->getQuestion()->getQuestion()) && ! empty($faq->getAnswer()->getAnswer()))
                    <li><a href="#question-{{$ind}}">{{ $faq->getQuestion()->getQuestion() }}</a></li>
                @endif
            @endforeach
        </ul>
        @foreach($model->getFaqs() as $ind => $faq)
            @if(! empty($faq->getQuestion()->getQuestion()) && ! empty($faq->getAnswer()->getAnswer()))
                <h3 id="question-{{$ind}}" class="mt-2 mt-md-5">{{ $faq->getQuestion()->getQuestion() }}</h3>
                <p>{{$faq->getAnswer()->getAnswer()}}</p>
            @endif
        @endforeach
    </div>
</section>
