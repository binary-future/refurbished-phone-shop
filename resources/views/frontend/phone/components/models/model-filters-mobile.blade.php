
<!-- modal popup for mobile -->

<div class="bg-black-50 shadow-up d-lg-none font-weight-normal fixed-bottom w-100 p-3 rounded-0 text-dark">
	<div class="row mx-auto">

		<div class="col-6">
			<button type="button" class="btn w-100 p-2 shadow-sm border border-primary bg-primary text-white rounded" data-toggle="modal" data-target="#mobileFilters">
				 <i class="fal fa-sliders-h pr-2 text-white"></i> Filter by...
			</button>
		</div>

		<div class="col-6">
		   @include('frontend.components.dropdowns.deals-sort-by', ['select' => $sortByDropdown])
		</div>

	</div>


</div>

<div class="modal fade animated fadeInUp" id="mobileFilters" tabindex="-1" role="dialog" aria-labelledby="mobileFiltersTitle" aria-hidden="true" style=" animation-duration: 0.2s;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content bg-millennial-pink-2 row">
				<button type="button" class="close fa-2x text-right pr-2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            <div class="modal-body text-center mx-auto px-3">
				<p class="text-center"><i class="fal fa-lightbulb pr-1 text-muted"></i> Drag the sliders to find your perfect plan!</p>


                @if (method_exists($filters, 'getModels'))
                    @include('frontend.components.filters.filter-container', ['filters' => $filters->getModels(), 'template' => 'frontend.components.filters.filter-phone-models'])
                @endif
                @if (method_exists($filters, 'getMonthlyCostSlider'))
                    @include('frontend.components.filters.filter-container', ['filters' => $filters->getMonthlyCostSlider(), 'template' => 'frontend.components.filters.filter-monthly-cost-slider'])
                @endif
                @include('frontend.components.filters.filter-container', ['filters' => $filters->getMonthlyCost(), 'template' => 'frontend.components.filters.filter-monthly-cost'])
                @if (method_exists($filters, 'getUpfrontCostSlider'))
                    @include('frontend.components.filters.filter-container', ['filters' => $filters->getTotalCostSlider(), 'template' => 'frontend.components.filters.filter-total-cost-slider'])
                @endif
                @include('frontend.components.filters.filter-container', ['filters' => $filters->getTotalCost(), 'template' => 'frontend.components.filters.filter-total-cost'])
                @include('frontend.components.filters.filter-container', ['filters' => $filters->getNetworks(), 'template' => 'frontend.components.filters.filter-network'])


				<div class="col-12 p-0 pb-4">

				<div class="col-12 pb-3 mb-2">
                    <a href="#" class="font-weight-light btn btn-outline-secondary btn-sm float-left my-auto w-100 py-2" id="filters-clear-mb"><i class="fal fa-undo-alt "></i> Reset</a>
                </div>



            </div>
				 <div class="col-12">
					<a href="#" id="" data-dismiss="modal" class="font-weight-light btn btn-primary my-auto float-right w-100 py-3">Update <i class="fal pl-1 fa-angle-right"></i></a>

				</div>
            </div>
        </div>
    </div>
</div>
<!-- /mobile popup -->
