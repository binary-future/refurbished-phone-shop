<div class="col-12 text-center">
    <h1 class="text-center h3 font-weight-bold">
         {{ $model->getName() }} {{ $color->getName() }} deals &amp; monthly contracts</h1>
</div>

<div class="col-12 row ">
    <div class="col-12 text-center">
        @include('frontend.components.common.lazy-image', [
            'class' => 'img-fluid mx-auto my-3 model-color-image',
            'lazySrc' => image_path($model->getImageByPhoneColor($color)),
            'alt' => $model->getName()
        ])
    </div>
    <div class="col-md-6 col-12 text-center mx-auto my-auto">
        <p class="lead">
			<strong><a href="{{ route('models.view', [
                            'brand' => $brand->getSlug(),
                            'model' => $model->getSlug()
                        ]) }}">{{ $model->getName() }}</a> in {{ $color->getName() }}</strong> - good
            choice! oneCompare compares the very best pay monthly contracts, as well as SIM Free Deals for
            the colour {{ $color->getName() }}.
        </p>
        <p>
            {{ $color->getName() }}-coloured {{ $brand->getName() }} phones are all the rage so
            make sure you compare prices first before paying. No other website checks all the
            biggest stores like Carphone Warehouse, BuyMobiles.net, Mobiles.co.uk and more for the
            cheapest {{ $color->getName() }} handset deals.
        </p>

        <a href="{{ route('models.view', [
                            'brand' => $brand->getSlug(),
                            'model' => $model->getSlug()
                        ]) }}" class="btn btn-success mt-2 btn-lg shadow px-4">View all {{ $color->getName() }} {{ $model->getName() }} deals <i
                    class="ml-2 small align-middle fal fa-arrow-right"></i></a>
    </div>
</div>
