<div class="row">
    <div class="col-12 p4 p-md-5">
        <h2 class="font-weight-normal h4 text-center text-md-left"><i class="fad fa-clipboard-list-check pr-1 text-muted"></i> {{ $brand->getName() }} {{ $model->getName() }}: Specifications</h2>
    </div>
    <div class="w-100"></div>
    @foreach($specs as $spec)
        <div class="col-md-6 col-12 mb-2 p-2 p-md-5 px-md-5">
            <div class="w-100">
                @include('frontend.phone.components.models.specs-item', ['spec' => $spec])
            </div>
        </div>
    @endforeach
</div>
