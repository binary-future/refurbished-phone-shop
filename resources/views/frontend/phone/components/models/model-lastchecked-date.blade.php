<div class="w-100 text-right text-white font-weight-light small d-none d-lg-block">
    <i class="fal fa-clock small"></i> Prices last updated:
    <span class="date">
        <time itemprop="lastReviewed" datetime="{{ $lastCheckedRss }}">{{ $lastCheckedDisplayable }}</time>
    </span>
</div>
