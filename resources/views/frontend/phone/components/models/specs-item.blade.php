@if($spec->isValid())
<ul class="list-group border-none">
    <li class="list-group-item bg-light d-flex justify-content-between align-items-center border-0">
        <h5 class="font-weight-bold">{{ $spec->getName() }}</h5>
    </li>
    <li class="list-group-item border-0 bg-light my-0 py-0"><div class="w-100 border-bottom border-success"></div></li>
    @foreach($spec->getContent() as $key => $value)
        <li class="list-group-item bg-light d-flex justify-content-between align-items-center border-0 py-2">
            <span class="font-weight-bold">{{ $key }}:</span>
            @if(is_bool($value))
                <i class="{{ $value ? 'fad fa-check text-success' : 'fad fa-times text-danger' }}"></i>
            @else
                {{ $value }}
            @endif
        </li>
    @endforeach
</ul>
@endif