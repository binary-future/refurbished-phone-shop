<script type="application/ld+json">
      {
        "@context" : "https://schema.org",
        "@type" : "Product",
        "model" : "{{ $model->getName() }}",
        "brand" : "{{ $brand->getName() }}",
        "name" : "{{ filter_content($brand->getName() . ' ' . $model->getName()) }}",
        "sku" : "065{{ $model->getKey() }}",
        @if ($image)
            "image" : ["{{ asset($image->getPath()) }}"],
        @endif
        "description" : "Compare {{ filter_content($brand->getName() . ' ' . $model->getName()) }} deals ✓ Checking 670+ best pay monthly contracts &amp; prices every hour. Find your perfect plan now.",

        @if (isset($rating))
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue" : "{{ $rating->getRatingInStars() }}",
                    "bestRating" : "5",
                    "reviewCount" : "{{ $rating->getReviews() }}"
            },

        @else

            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue" : "4.89",
                "bestRating" : "5",
                "reviewCount" : "67"
            },

        @endif
        @if ($dealsInfo->isValid())
            "offers" : {
                "@type" : "Offer",
                "price" : "22.00",
				"availability" : "https://schema.org/InStock",
				"itemCondition"	:  "https://schema.org/NewCondition",
				"url" : "{{ url()->current() }}",
                "priceCurrency" : "GBP"
            }
        @endif

    }
</script>

<script type="application/ld+json">
{
    "@context": "https:\/\/schema.org",
    "@type": "FAQPage",
    "mainEntity": [
    @foreach($model->faqs as $faq)
        @if(! $faq->getQuestion() || ! $faq->getAnswer())
            @continue
        @endif
        {
            "@type": "Question",
            "name": "{{ $faq->getQuestion()->getQuestion() }}",
            "text": "",
            "acceptedAnswer": {
                "@type": "Answer",
                "text": "{{ $faq->getAnswer()->getAnswer() }}"
            }
        }
        @if (! $loop->last)
            ,
        @endif
    @endforeach
    ]
}</script>
