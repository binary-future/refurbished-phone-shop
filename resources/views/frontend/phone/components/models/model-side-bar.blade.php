@include('frontend.phone.components.models.model-filters-mobile', ['filters' => $filters])
<div class="row mt-5">
    <div class="col-12 d-none d-lg-block mt-3">
        <span class="font-weight-normal text-dark text-left border-bottom border-success pb-2"><i class="fal fa-sliders-h pr-1 small"></i> Find your perfect phone:</span>
    </div>
    	<div class="col-12 text-center d-none d-lg-block mt-3">
        <a href="#" id="filters-clear" class="font-weight-bold d-none btn btn-primary px-4 filters-clear"><i class="fal fa-undo-alt pr-1 "></i> Reset</a>
    </div>


    <div class="col-12 d-none d-lg-block text-center text-md-left mt-3">
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getConditions(), 'template' => 'frontend.components.filters.filter-condition'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getMonthlyCostSlider(), 'template' => 'frontend.components.filters.filter-monthly-cost-slider'])
		@include('frontend.components.filters.filter-container', ['filters' => $filters->getMonthlyCost(), 'template' => 'frontend.components.filters.filter-monthly-cost'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getTotalCostSlider(), 'template' => 'frontend.components.filters.filter-total-cost-slider'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getTotalCost(), 'template' => 'frontend.components.filters.filter-total-cost'])
        @include('frontend.components.filters.filter-container', ['filters' => $filters->getNetworks(), 'template' => 'frontend.components.filters.filter-network'])
    </div>
</div>
