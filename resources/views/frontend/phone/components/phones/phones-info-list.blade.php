<div class="row row-eq-height">
    @foreach($phones as $phone)
        @include('frontend.phone.components.phones.phones-info-item', ['phone' => $phone])
    @endforeach
</div>