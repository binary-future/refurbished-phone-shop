<div class="col-md-3 col-sm-6 col-12 my-2">
    <div class="row h-100">
        <div class="col-11 d-flex p-3 bg-light shadow-sm h-100">
            <div class="row">
                <div class="col-12 mb-2 text-center">
                    <h6>{{ $phone->getBrand()->getName() }} <span class="font-weight-bold">{{ $phone->getModel()->getName() }}</span></h6>
                </div>
                <div class="col-12 text-center mb-2">
                    @include('frontend.components.common.lazy-image', [
                        'class' => 'img img-thumbnail no-bg',
                        'lazySrc' => image_path($phone->getImage()),
                        'alt' => $phone->getModel()->getName(),
                        'style' => 'max-height: 200px'
                    ])
                </div>
                @if($phone->getColors()->isNotEmpty())
                    <div class="col-12 my-2">
                        <div class="row justify-content-center">
                            @foreach($phone->getColors() as $color)
                                <div class="col-2 bg-light d-flex justify-content-center align-items-center p-0">
                                    @if($color->getHex())
                                        <span class="color-dot" style="background-color: {{ '#' . $color->getHex() }}; border: 1px solid black;" title="{{ $color->getName() }}"></span>
                                    @else
                                        <span class="small font-weight-bold">
                                            {{ $color->getName() }}
                                        </span>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
                <div class="col-12 text-center">
                    <a href="{{ route('models.view', ['brand' => $phone->getBrand()->getSlug(), 'model' => $phone->getModel()->getSlug()]) }}" class="btn btn-outline-primary">See Deals</a>
                </div>
            </div>
        </div>
    </div>
</div>
