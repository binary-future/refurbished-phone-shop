@extends('frontend.layouts.app')

@section('title'){{ sprintf('Costs, info for %s %s %s Deal', $phone->getModel()->getName(), $phone->getColor() ? $phone->getColor()->getName() : '', $phone->getCapacity()) }}@endsection

@section('canonical'){{ route('models.view', [
        'brand' => $phone->getModel()->getBrand()->getSlug(),
        'model' => $phone->getModel()->getSlug()
        ]) }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Mobile Contracts</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.main') }}">Makes</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.view', ['brand' => $phone->getModel()->getBrand()->getSlug()]) }}">{{ $phone->getModel()->getBrand()->getName() }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('models.view', [
        'brand' => $phone->getModel()->getBrand()->getSlug(),
        'model' => $phone->getModel()->getSlug()
        ]) }}">
            {{ $phone->getModel()->getName() }} deals</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $phone->getModel()->getName() }} {{ $phone->getColor() ? $phone->getColor()->getName() : '' }} {!! present_gigabyte($phone->getCapacity()) !!} deal</li>
@endsection

@section('content')

    <div class="container-fluid bg-default shadow-sm text-light py-2 py-md-4 ">

	    <div class="container">
            <div class="row">
                <div class="col-md-2 col-5 my-auto">
                    @include('frontend.components.common.lazy-image', [
                        'class' => 'img img-fluid phone-color-image float-left',
                        'lazySrc' => image_path($phone->getImage()),
                        'alt' => 'Your device'
                    ])
                </div>

                <div class="col-md-7 col-7 my-auto">
                    <h1 class="text-left h4 pt-3 pb-2 font-weight-normal my-auto text-light">
                        <span class="small">{{ $phone->getModel()->getBrand()->getName() }}</span><br/>
                        {{ $phone->getModel()->getName() }} <span class="price-sans small">({{ $phone->getColor() ? $phone->getColor()->getName() : '' }} {!! present_gigabyte($phone->getCapacity()) !!})</span>
				    </h1>

                    <h2 class="h6 text-uppercase text-muted">
                    Contract costs, terms and more
                    </h2>
			    </div>

                <div class="col-md-3 col-8 my-auto mx-auto text-center">

                    <a href="{{ route('deal.outlink', ['deal' => $deal->getKey()]) }}" rel="nofollow"
                       target="_blank" class="btn btn-lg btn-warning shadow-sm w-100 my-4 py-2 mx-auto">Buy Now <i class="fad fa-external-link pl-2"></i></a>

                    <a href="{{ route('deal.outlink', ['deal' => $deal->getKey()]) }}" rel="nofollow"
                       target="_blank">
                        @include('frontend.components.common.lazy-image', [
                            'class' => 'd-none d-md-block img img-fluid w-50 my-2 text-center mx-auto rounded',
                            'lazySrc' => image_path($deal->getStore()->getImage()),
                            'alt' => $deal->getStore()->getName()
                        ])
                        <p class="d-md-none mb-3 text-center">
                            {{ $deal->getStore()->getName() }}
                        </p>
                    </a>
			    </div>

            </div>
        </div>
    </div>
<div class="container-fluid" style="background: #F5F1ED">

	<div class="container bg-white p-5 my-5 shadow-sm">
        <div class="row">
			<div class="row">
                <div class="col-md-7 col-12">

                    @if($deal->getContract())
                        @include('frontend.deals.components.deals.phone-deals-contract-info', ['deal' => $deal, 'phone' => $phone])
                    @endif
                </div>

                <div class="col-md-4  offset-md-1 col-12 pb-3 bg-white">
                    <h6 class="pt-3">Costs breakdown</h6>
                    @include('frontend.deals.components.deals.total-cost-calculation-modal')
			    </div>

			</div>

        </div>
		<div class="mx-auto text-center pt-5 my-5">
			<p>Not what you're looking for?</p>
			<a class="btn btn-success mx-auto btn-lg shadow-lg" href="{{ route('models.view', [
			'brand' => $phone->getModel()->getBrand()->getSlug(),
			'model' => $phone->getModel()->getSlug()
			]) }}"> <i class="fal fa-sliders-h pr-2"></i> Find {{ $phone->getModel()->getName() }} deals <i class="fal fa-angle-right pl-2"></i></a>
		</div>
    </div>
</div>

    @if($relatedDeals->isNotEmpty())
        <div class="container-fluid bg-millennial-pink-2 p-5">
            <div class="container">
                <div class="row">
                    <div class="col-12 mt-5">
                        <h5 class="text-dark font-weight-normal text-center">Today's popular {{ $phone->getModel()->getBrand()->getName() }} {{ $phone->getModel()->getName() }} deals 🔥</h5>
                    </div>
                    <div class="col-12">
                        @include('frontend.deals.components.deals.relative-deals-section', ['deals' => $relatedDeals])
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
