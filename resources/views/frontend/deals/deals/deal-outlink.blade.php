<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thanks.....</title>
    <meta name="description" content="Thanks for using OneCompare">

    <link rel="canonical" href="@yield('canonical', request()->url() )"/>

    <link rel="dns-prefetch" href="//cdnjs.cloudflare.com"/>
    <link rel="dns-prefetch" href="//static.getclicky.com"/>
    <link rel="dns-prefetch" href="//in.getclicky.com"/>

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
    <link rel="manifest" href="/manifest.json" crossorigin="use-credentials">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/images/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Styles -->

    <link rel="stylesheet" href="{{ mix('css/main.css') }}">
    <link rel="stylesheet" href="{{ mix('css/custom.min.css') }}">

    <script>var clicky_site_ids = clicky_site_ids || []; clicky_site_ids.push(101182486);</script>
    <script async src="//static.getclicky.com/js"></script>
</head>

<body itemscope itemtype="http://schema.org/WebPage" class="no-js">

    <div id="app" class="page-outlink">
        <div class="text-center">
            <img src="{{ asset('images/logo-temp.svg') }}" alt="OneCompare Logo" class="img img-fluid img-logo my-4">
            <h3>Thanks for using oneCompare</h3>
            <div class="js-only">
                <p>We are redirecting you to {{ $store->getImage() === null ? $store->getName() : '' }}</p>

                @if ($store->getImage())
                    <img src="{{ image_path($store->getImage()) }}" alt="{{ $store->getName() }}" class="img img-fluid img-store" alt="" />
                @endif
                <p class="mt-4">If you are not redirected, <a href="{{ $urlToGo }}" rel="nofollow"><strong>click here</strong></a></p>
            </div>
            <noscript>
                <p class="lead mt-4 alert-danger">Your browser don't support JavaScript. Please, <a href="{{ $urlToGo }}" rel="nofollow"><strong>click here</strong></a> to be redirected</p> instead.
            </noscript>
        </div>
    </div>

    <script>
        document.getElementsByTagName('body')[0].classList.remove('no-js');

        document.addEventListener('DOMContentLoaded', function() {
            setTimeout(function () {
                window.location = "{{ $urlToGo }}";
            }, 1000);
        });
    </script>
</body>
