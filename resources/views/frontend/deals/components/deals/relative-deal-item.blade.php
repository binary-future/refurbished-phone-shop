<div class="col-12 my-2 text-center bg-light rounded shadow border border-muted">
    <div class="row">
        <div class="col-md-1 col-4 my-auto">
			<span class="mx-auto small text-muted">Network:</span><br>

            @include('frontend.components.common.lazy-image', [
                'class' => 'img img-fluid rounded mx-auto',
                'lazySrc' => image_path($dealItem->getContract()->getNetwork()->getImage()),
                'alt' => $dealItem->getContract()->getNetwork()->getName()
            ])
        </div>

        <div class="col-md-5 col-8">@include('frontend.deals.components.deals.payment-item', ['payment' => $dealItem->getPayment()])</div>

        <div class="col-md-2 col-6 p-2 my-auto">
            <div class="w-100 h-100 bg-light p-3">
                <span class="h3 font-weight-bold price-sans">
                    {!! $dealItem->getContract()->getDataInGB() === null ? 'Unlimited' : present_gigabyte($dealItem->getContract()->getDataInGB()) !!}
				</span>
                <br/>
                <span class="small font-weight-light">data</span>
            </div>
        </div>
        <div class="col-md-2 col-6 p-2 my-auto">
            <div class="w-100 h-100 bg-muted p-3">
                <span class="h3 font-weight-bold price-sans">
                    &pound;{{ trailing_zeros($dealItem->getPayment()->getMonthlyCost()) }}
				</span>
				<br/>
                <span class="small font-weight-light">per month</span>

            </div>
        </div>
        <div class="col-md-2 col-12 my-auto">
            <div class="w-100 p-2">
                <a href="{{ route('deal.outlink', ['deal' => $dealItem->getKey()]) }}" rel="nofollow" class="btn mt-3 btn-success" onclick="location.href='{{ route('mobile.deal', ['deal' => $dealItem->getKey()]) }}';" target="_blank">View <i class="fal fa-arrow-right pl-2"></i></a>
            </div>
            <div class="w-50 p-2 mx-auto">
				<a href="{{ route('deal.outlink', ['deal' => $dealItem->getKey()]) }}" rel="nofollow" onclick="location.href='{{ route('mobile.deal', ['deal' => $dealItem->getKey()]) }}';" target="_blank">
                    @include('frontend.components.common.lazy-image', [
                        'class' => 'd-none d-md-block img img-fluid',
                        'lazySrc' => image_path($dealItem->getStore()->getImage()),
                        'alt' => $dealItem->getStore()->getName()
                    ])
                    <p class="d-md-none mb-3 text-center">
                        {{ $dealItem->getStore()->getName() }}
                    </p>
				</a>
            </div>
        </div>
    </div>
</div>
