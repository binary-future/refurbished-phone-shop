<div class="col-md-4 col-sm-6 col-12 my-2">
    <div class="text-center border border-top-0 rounded shadow bg-white p-4">
        <div class="row align-items-center">
            <div class="col-12">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'img img-fluid text-center w-25',
                    'lazySrc' => image_path($deal->getContract()->getNetwork()->getImage()),
                    'alt' => $deal->getContract()->getNetwork()->getName()
                ])
            </div>
            <div class="col-9">@include('frontend.deals.components.deals.payment-item', ['payment' => $deal->getPayment()])</div>
            <div class="col-6 p-2">
                <div class="w-100 h-100 bg-med-blue p-3">
                    <span class="h5 font-weight-bold price-sans">
                        {!! ! is_null($deal->getContract()->getDataInGB()) ? present_gigabyte($deal->getContract()->getDataInGB()) : 'Unlimited' !!}<br/>
                        <span class="small font-weight-light">data</span>
                    </span>
                </div>
            </div>
            <div class="col-6 p-2 pt-0">
                <div class="w-100 h-100 bg-med-blue p-3">
                    <span class="h5 font-weight-bold price-sans">
                        &pound;{{ trailing_zeros($deal->getPayment()->getMonthlyCost()) }}<br/>
                        <span class="small font-weight-light">per month</span>
                    </span>
                </div>
            </div>
            <div class="col-md-4 d-none d-md-block">
                <div class=" p-2 mt-3 w-100 mx-auto">
                    @include('frontend.components.common.lazy-image', [
                        'class' => 'img img-fluid',
                        'lazySrc' => image_path($deal->getStore()->getImage()),
                        'alt' => $deal->getStore()->getName()
                    ])
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="w-100">
                    <a href="{{ route('deal.outlink', ['deal' => $deal->getKey()]) }}"
                       rel="nofollow"
                       class="btn btn-success"
                       data-toggle="modal" data-target="#deal-info-modal"
                       data-href="{{ route('mobile.deal.popup', ['deal' => $deal->getKey()]) }}"
                    >Open Deal <i class="fal fa-arrow-right small"></i></a>
                </div>
                <div class="w-100 mt-2">
                    <a href="{{ route('mobile.deal', ['deal' => $deal->getKey()]) }}">More info ></a>
                </div>
            </div>
        </div>
    </div>
</div>
