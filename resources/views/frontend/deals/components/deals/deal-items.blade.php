@foreach($deals as $deal)
    @if($phones->has($deal->getProductId()))
        @include('frontend.deals.components.deals.deal-item', ['deal' => $deal, 'phone' => $phones[$deal->getProductId()]])
    @endif
@endforeach

