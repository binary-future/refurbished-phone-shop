<div class="col-12 my-4 mb-md-2 mb-5 text-center border border-white shadow rounded p-3 bg-white hover-border deal-item">
    <div class="row align-items-center">
        <header class="col-md-1 col-2 my-auto pr-0">
            <div class="w-100">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'img img-fluid text-right pl-1',
                    'lazySrc' => image_path($deal->getContract()->getNetwork()->getImage()),
                    'alt' => $deal->getContract()->getNetwork()->getName(),
                    'style' => 'max-height: 64px'
                ])
            </div>
            @if($deal->getStore()->getRating())
                <div class="w-100">
                    <a href="{{ $deal->getStore()->getRating()->getLink()->getLink() }}" rel="nofollow" target="_blank">
                        @include('frontend.components.common.lazy-image', [
                            'class' => 'mg-fluid w-100 mx-auto',
                            'lazySrc' => asset('/images/ratings/stars-' . rating_to_image_stars($deal->getStore()->getRating()->getRating()->getRating()). '.svg'),
                            'alt' => $deal->getStore()->getRating()->getLink()->getLink(),
                            'title' => 'View Trustpilot reviews'
                        ])
                    </a>
                </div>
            @endif
        </header>
        <section class="col-md-4 col-10">
            <div class="row">
                <div class="col-6 my-auto">
                    <a href="{{ route('deal.outlink', ['deal' => $deal->getKey()]) }}" rel="nofollow"
                       data-toggle="modal" data-target="#deal-info-modal"
                       data-href="{{ route('mobile.deal.popup', ['deal' => $deal->getKey()]) }}"
                       target="_blank"
                    >
                        @include('frontend.components.common.lazy-image', [
                            'class' => 'img img-fluid',
                            'lazySrc' => thumb_image($phone),
                            'alt' => $phone->getModel()->getName()
                        ])
                    </a>
                </div>
                <div class="col-6 my-auto text-md-left text-center pr-md-0 pr-5">
                    <div class="h5 font-weight-normal">
                        {{ $phone->getModel()->getBrand()->getName() }} {{ $phone->getModel()->getName() }}: {!! present_gigabyte($phone->getCapacity()) !!} <br>
                        <em>{{ $phone->getColor()->getName() }}</em>
                    </div>
                    <div class="w-100 mt-4 small">
                        <a class="text-muted-more" data-toggle="collapse" href="#collapseDealDescription{{$deal->getKey()}}" role="button" aria-expanded="false" aria-controls="collapseDealDescription{{$deal->getKey()}}">
                            Full deal info <i class="fad fa-info-circle"></i>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <section class="col-md-3 col-6">
            <div class="w-100 h-100">
                @if($deal->getContract())
                    <ul class="list-group">
                        <li class="d-flex bg-muted justify-content-between align-items-center border-0">
                            Guarantee
                            <span class="price-sans pl-1 font-weight-bold">{{ $deal->getStore()->getGuarantee() }} months</span>
                        </li>
                        <li class="d-flex bg-muted justify-content-between align-items-center border-0">
                            Condition
                            <span class="price-sans pl-1 font-weight-bold text-capitalize"><deal-condition :deal="$deal" /></span>
                        </li>
                        <li class="d-flex bg-muted justify-content-between align-items-center border-0">
                            <div class="row">
                                @foreach($deal->getStore()->getPaymentMethods() as $paymentMethod)
                                    @include('frontend.components.common.lazy-image', [
                                        'class' => 'img img-fluid rounded col-4',
                                        'lazySrc' => image_path($paymentMethod->getImage()),
                                        'alt' => $paymentMethod->getName(),
                                        'title' => $paymentMethod->getName(),
                                    ])
                                @endforeach
                            </div>
                        </li>
                    </ul>
                @else
                    <span class="font-weight-bold">No contract data found</span>
                @endif
            </div>
        </section>
        <div class="col-md-2 col-6 bg-med-blue py-0">

                <div class="font-weight-bold text-center price-sans pb-0 pt-3 price-month">
                    &pound;{{ trailing_zeros($deal->getPayment()->getMonthlyCost()) }}</div>
            <span class="font-weight-light small text-center price-per-month">per month</span>
            <div class="alert alert-light font-weight-light text-dark p-1 mt-4" style=" cursor:default"
                 data-toggle="tooltip" title="This is the price you pay upfront for the handset."><span
                        class="price-sans font-weight-bold">{{
                $deal->getPayment()->getUpfrontCost()
                ? html_entity_decode('&pound;') . trailing_zeros($deal->getPayment()->getUpfrontCost())
                : 'FREE'
                }}</span><br>

                upfront cost
            </div>

        </div>
        <div class="col-md-2 col-12">
            <div class="w-75 mx-auto d-none d-md-block text-center">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'img img-fluid rounded mb-2',
                    'lazySrc' => image_path($deal->getStore()->getImage()),
                    'alt' => $deal->getStore()->getName()
                ])
            </div>
            <p class="d-md-none mb-3 text-center">
                {{ $deal->getStore()->getName() }}
            </p>
            <div class="w-100">
                <a href="{{ route('deal.outlink', ['deal' => $deal->getKey()]) }}" rel="nofollow"
                   class="btn btn-success shadow-sm get-deal-btn w-100 stretched-link py-md-1 py-3" target="_blank"
                   data-toggle="modal" data-target="#deal-info-modal"
                   data-href="{{ route('mobile.deal.popup', ['deal' => $deal->getKey()]) }}"
                >Buy <i class="fal fa-angle-right small pl-1"></i></a>
            </div>

        </div>
        <div class="col-12 collapse" id="collapseDealDescription{{$deal->getKey()}}">
            <p class="text-justify">
                {{ $deal->getDescription()->getContent() }}
            </p>
        </div>
    </div>
</div>
