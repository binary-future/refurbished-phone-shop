    <p class="font-weight-light small">Here's the breakdown of all your costs for this contract (excluding other costs such as phonecalls outside your plan, etc)</p>
<div class="accordian w-100 mt-3 border-bottom border-secondary cursor-pointer shadow-sm" id="accordionCosts">

    <ul class="list-group border-none shadow-sm rounded text-dark"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">


		<li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold">Show costs breakdown <i class="fal fa-angle-up pl-3"></i> <i class="fal fa-angle-down pl-3"></i> </span>

        </li>


		<div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordionCosts">

        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold">To pay now</span>
            <span class="font-weight-bold price-sans">{{
                $deal->getPayment()->getUpfrontCost()
                ? html_entity_decode('&pound;') . trailing_zeros($deal->getPayment()->getUpfrontCost())
                : 'Free'
                }}</span>
        </li>

        <li class="list-group-item border-0  my-0 py-0"><div class="w-100 border-bottom border-success"></div></li>
        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold"><i class="fal fa-comment-smile"></i> This contract has these costs</span>
        </li>
        <li class="list-group-item border-0  my-0 py-0"><div class="w-100 border-bottom border-success"></div></li>
        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            Monthly cost
            <span class="font-weight-bold price-sans">&pound;{{ trailing_zeros($deal->getPayment()->getMonthlyCost()) }}</span>
        </li>
        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold lead">Overall cost</span>
        </li>
        <li class="list-group-item border-0  my-0 py-0"><div class="w-100 border-bottom border-success"></div></li>
        <li class="list-group-item  d-flex justify-content-end align-items-center border-0 text-right">
            <span class="font-weight-bold price-sans">&pound;{{ trailing_zeros($deal->getPayment()->getTotalCost()) }}</span>
        </li>
		</div>
    </ul>
</div>
