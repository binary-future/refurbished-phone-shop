    <p class="font-weight-light small">Here's the breakdown of all your costs for this contract (excluding other costs such as phonecalls outside your plan, etc)</p>
<div class=" w-100 mt-4 border-bottom border-secondary">

    <ul class="list-group border-none shadow rounded text-dark">


		<li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold h4">Full costs breakdown </span>

        </li>


		<div id="collapseOne" class="" aria-labelledby="headingOne" data-parent="#accordionCostsLarge">

        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold">To pay now</span>
            <span class="font-weight-bold price-sans">{{
                $deal->getPayment()->getUpfrontCost()
                ? html_entity_decode('&pound;') . trailing_zeros($deal->getPayment()->getUpfrontCost())
                : 'Free'
                }}</span>
        </li>

        <li class="list-group-item border-0  my-0 py-0"><div class="w-100 border-bottom border-success"></div></li>
        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold"><i class="fal fa-comment-smile"></i> This contract has the following costs</span>
        </li>
        <li class="list-group-item border-0  my-0 py-0"><div class="w-100 border-bottom border-success"></div></li>
        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            Monthly cost
            <span class="font-weight-bold price-sans">&pound;{{ trailing_zeros($deal->getPayment()->getMonthlyCost()) }}</span>
        </li>
        <li class="list-group-item  d-flex justify-content-between align-items-center border-0">
            <span class="font-weight-bold lead alert-info p-2">Overall cost</span>
            <span class="font-weight-bold price-sans">&pound;{{ trailing_zeros($deal->getPayment()->getTotalCost()) }}</span>
        </li>
		</div>
    </ul>
</div>
