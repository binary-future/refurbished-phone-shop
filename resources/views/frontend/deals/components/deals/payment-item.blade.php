<ul class="list-group ">
    <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0">
        Upfront Cost
        <span class="font-weight-bold">{{
            $deal->getPayment()->getUpfrontCost()
            ? html_entity_decode('&pound;') . trailing_zeros($deal->getPayment()->getUpfrontCost())
            : 'Free'
            }}</span>
    </li>
    <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0 border-bottom border-success">
        Total Cost
        <span class="font-weight-bold">&pound;{{ trailing_zeros($payment->getTotalCost()) }}</span>
    </li>
    <li class="list-group-item bg-white d-flex justify-content-between align-items-center border-0 border-bottom border-success">
        Contract term
        <span class="font-weight-bold">{{ $payment->getTerm() }} months</span>
    </li>
</ul>
