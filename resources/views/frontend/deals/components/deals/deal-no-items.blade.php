<div class="no-deals col-12 text-center py-3 py-md-5 bg-white border shadow rounded-sm mt-2 {{ $isShow ? '' : 'd-none' }}">
    <span class="h5 font-weight-normal py-5">Sorry! No valid deals were found.</span>

    <p>Try changing options, or <strong>reset the filters</strong>.</p>
    <p><i class="fal fa-2x mt-3 fa-frown"></i></p>

    <p>
        <a href="#" id="filters-clear-empty" class="font-weight-light btn btn-outline-secondary w-25 btn-sm filters-clear">
            <i class="fal fa-undo-alt pr-1 "></i> Reset
        </a>
    </p>
</div>
