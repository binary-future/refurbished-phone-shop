<div class="row">
    <div class="deals-list w-100">
        @include('frontend.deals.components.deals.deal-items', ['deals' => $deals, 'phones' => $phones])
    </div>

    @include('frontend.deals.components.deals.deal-no-items', ['isShow' => $deals->isEmpty()])

    @include('frontend.deals.components.deals.deals-list-pagination', ['deals' => $deals])
</div>
