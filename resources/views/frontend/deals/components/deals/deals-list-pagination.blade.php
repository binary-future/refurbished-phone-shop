@if(method_exists($deals, 'links'))
    <div class="pagination-wrap col-12 d-flex justify-content-center my-3">
        {{ $deals->links('frontend.components.paginator.custom') }}
    </div>
@endif
