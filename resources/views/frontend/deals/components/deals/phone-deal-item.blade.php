<div class="col-12 my-2 text-center border border-top-0 rounded shadow p-3 bg-white ">
    <div class="row align-items-center">
        <div class="col-3">
            <div class="row">
                <div class="col-5">
                    @include('frontend.components.common.lazy-image', [
                        'class' => 'img img-fluid',
                        'lazySrc' => thumb_image($phone),
                        'alt' => $phone->getModel()->getName()
                    ])
                </div>
                <div class="col-7">!!!
                    <h6>{{ $phone->getModel()->getBrand()->getName() }} {{ $phone->getModel()->getName() }} {{ present_gigabyte($phone->getCapacity()) }} {{ $phone->getColor()->getName() }}</h6>
                    <span class="small">{{
                        $deal->getPayment()->getUpfrontCost()
                        ? html_entity_decode('&pound;') . trailing_zeros($deal->getPayment()->getUpfrontCost())
                        : 'Free'
                        }} upfront</span>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="w-100">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'img img-fluid text-center w-50',
                    'lazySrc' => image_path($deal->getContract()->getNetwork()->getImage()),
                    'alt' => $deal->getContract()->getNetwork()->getName()
                ])
            </div>
        </div>
        <div class="col-3">
            <div class="w-100 h-100">
                @if($deal->getContract())
                <ul class="list-group">
                </ul>
                @else
                    <span class="font-weight-bold">No contract data found</span>
                @endif
            </div>
        </div>
        <div class="col-2">
            <div class="w-100 h-100 bg-med-blue p-3">
                <h4 class=" h5 font-weight-bold">
                    &pound;{{ trailing_zeros($deal->getPayment()->getMonthlyCost()) }}<br/>
                    <span class="small">per month</span>
                </h4>
            </div>
            <span class="small"><span class="font-weight-bold">&pound;{{ trailing_zeros($deal->getPayment()->getTotalCost()) }}</span> total cost</span>
        </div>
        <div class="col-2">
            <div class="p-2 w-75 mx-auto">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'img img-fluid',
                    'lazySrc' => image_path($deal->getStore()->getImage()),
                    'alt' => $deal->getStore()->getName()
                ])
            </div>
            <div class="w-100 mt-3 ">
                <a href="{{ route('deal.outlink', ['deal' => $deal->getKey()]) }}"
                   rel="nofollow"
                   class="btn  btn-success"
                   data-toggle="modal" data-target="#deal-info-modal"
                   data-href="{{ route('mobile.deal.popup', ['deal' => $deal->getKey()]) }}"
                >Get Deal <i class="fal fa-arrow-right"></i></a>
            </div>
            <div class="w-100 mt-2">
                <a href="{{ route('mobile.deal', ['deal' => $deal->getKey()]) }}">More info ></a>
            </div>
        </div>
    </div>
</div>
