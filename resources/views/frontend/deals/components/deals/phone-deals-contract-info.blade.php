
	<h4>Contract summary</h4>
<div class="row">
		<div class="col-md-6 col-6 my-auto text-center">
			<div class="w-100 h-100 -blue p-3">
				<span class="font-weight-normal h4">
					<i class="fal fa-calendar-check rounded bg-secondary text-white p-2 mb-3"></i><br>
					<span class="font-weight-bold price-sans">&pound;{{ trailing_zeros($deal->getPayment()->getMonthlyCost()) }}</span><br/>
					<span class="small">Per month</span>
				</span>
			</div>


		</div>
</div>
		<ul class="list-group border-none text-center text-sm-left my-auto">
			 <li class="list-group-item  border-0">
			  <i class="fal fa-shopping-cart pr-2 pt-1 text-muted"></i>  Upfront cost
				<span class="font-weight-bold  price-sans float-none float-md-right lead">{{
                $deal->getPayment()->getUpfrontCost()
                ? html_entity_decode('&pound;') . trailing_zeros($deal->getPayment()->getUpfrontCost())
                : 'Free'
                }}</span>
			</li>
			<li class="list-group-item  border-0 border-bottom border-success">
				@if($deal->getContract()->getNetwork()->getImage())
		            <i class="fal fa-sms pr-2 pt-1 text-muted"></i>Network
				    <span class="font-weight-bold  float-none float-md-right price-sans lead">
                        @include('frontend.components.common.lazy-image', [
                            'class' => 'img img-fluid',
                            'lazySrc' => image_path($deal->getContract()->getNetwork()->getImage()),
                            'alt' => $deal->getContract()->getNetwork()->getName(),
                            'style' => 'max-height: 30px;'
                        ])
                    </span>
                @else
                    {{ $deal->getContract()->getNetwork()->getName() }}
                @endif
			</li>
		</ul>





<div class="w-100 py-4 mb-1 border-bottom border-secondary"></div>
	<p class="small text-muted pt-3">oneCompare does not sell pay monthly contracts directly, click the CONTINUE button above to purchase this deal from the retailer's website. In case of any errors with this data please contact us.</p>
