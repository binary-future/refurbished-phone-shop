<div class="col-12">
    
    <h3 class="font-weight-normal text-dark w-100 shrink-sm h4"><i class="fal fa-check-square pr-1"></i> We've opened that deal for you.</h3>

		<h5 class="mb-3 h6 text-uppercase text-muted">Here's the summary:</h5>
		
		<div class="row">
        <div class="col-md-4 col-4 mx-auto my-auto text-center">
            <img class="img img-fluid phone-color-image" src="{{ image_path($phone->getImage()) }}" alt="" />
          
 
        </div>

    <div class="col-md-8 col-8 bg-light">
		
	<h4 class="text-left h5 font-weight-normal my-auto text-muted">
        <span class="small">{{ $phone->getModel()->getBrand()->getName() }}</span><br/>
        {{ $phone->getModel()->getName() }} ({{ $phone->getColor() ? $phone->getColor()->getName() : '' }} {{ $phone->getCapacity() }})
    </h4>
		
		<div class="mt-2 pb-3">
        @include('frontend.deals.components.deals.total-cost-calculation-modal-modal')
		</div>
    </div>
</div>
</div>
