<div class="row">
    @forelse($deals as $deal)
        @include('frontend.deals.components.deals.deals-grid-item', ['deal' => $deal])
    @empty
        <div class="col-12">
            <h3 class="text-center">Sorry! No deals were found</h3>
        </div>
    @endforelse
    <div class="w-100"></div>
    @if(method_exists($deals, 'links'))
        <div class="col-12 d-flex justify-content-center my-3">
            {{ $deals->links() }}
        </div>
    @endif
    @include('frontend.components.bars.loading-bar-search')
</div>
