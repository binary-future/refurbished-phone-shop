<div id="relative-deals" class="carousel slide pt-3" data-ride="carousel">
    <ol class="carousel-indicators ">
        @foreach($deals as $key => $deal)
            <li data-target="#relative-deals" data-slide-to="{{ $key }}" @if($loop->first)class="active"@endif></li>
        @endforeach
    </ol>
    <div class="carousel-inner mb-5">
        @foreach($deals as $dealItem)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <div class="w-100 text-center">
                    @include('frontend.deals.components.deals.relative-deal-item', ['dealItem' => $dealItem])
                </div>
            </div>
        @endforeach
    </div>
</div>