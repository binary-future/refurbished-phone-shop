<div class="row">
    @forelse($deals as $deal)
        @if($phones->has($deal->getProductId()))
            @include('frontend.deals.components.deals.deal-item', ['deal' => $deal, 'phone' => $phones[$deal->getProductId()]])
        @endif
    @empty
        <div class="col-12">
            <h3 class="text-center">Sorry! No deals were found</h3>
        </div>
    @endforelse
    <div class="w-100"></div>
    @if(method_exists($deals, 'links'))
        <div class="col-12 d-flex justify-content-center my-3">
            {{ $deals->links('frontend.components.paginator.custom') }}
        </div>
    @endif
</div>