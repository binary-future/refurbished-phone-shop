<div class="modal fade" id="deal-info-modal"
     tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content bg-light text-dark">

            <div class="modal-body">            <div class=" d-flex justify-content-end">
                <button type="button" class="close fa-3x text-dark p-1"
                        data-dismiss="modal">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
                <div id="deal-content"></div>
            </div>
        </div>
    </div>
</div>
