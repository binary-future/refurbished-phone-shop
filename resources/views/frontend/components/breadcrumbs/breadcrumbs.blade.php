<div class="container d-none d-sm-block mt-0 mt-md-3 ">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            @yield('breadcrumbs-content')
        </ol>
    </nav>
</div>