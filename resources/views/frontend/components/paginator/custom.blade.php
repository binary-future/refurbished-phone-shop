@if ($paginator->hasPages())
<ul class="pagination" role="navigation">
    @if ($paginator->hasMorePages())
        <li class="page-item">
            <button class="btn btn-outline-success page-link px-4" data-page="{{ $paginator->currentPage() + 1 }}" aria-label="@lang('pagination.next')">Load more <i class="fal small fa-plus pl-1"></i></button>
        </li>
    @endif
</ul>
@endif

