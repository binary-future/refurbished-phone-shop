<div class="mt-0 jumbotron text-black pb-5 pb-lg-0 position-relative jumbo-fix rounded-0">
    <div class="container">
        <h1 class="text-center text-lg-left mb-5 mb-lg-2 pt-2 pt-lg-0 mt-0 mt-lg-4">Find your next <span class="highlight pl-1 pr-1 rotate">refurbished</span> mobile phone</h1>
		<h2 class="h3 mb-5 d-none d-lg-block">Compare used &amp; SIM-Free handset prices now.</h2>

        <div class="w-100 d-flex justify-content-left">
            <form class="form-inline" method="GET" action="{{ route('search.model') }}">
                <div class="input-group-lg jumbotron-form">
                    <input id="model-search" class="form-control shadow" size="45" name="name" type="text" placeholder="e.g. iPhone 8 Plus, Galaxy S10" data-url="{{ route('search.autocomplete') }}" autocomplete="off">
                </div>
                <button class="btn btn-success btn-lg font-weight-normal input-group-lg my-2 my-lg-0 mx-auto border-radius-left-none shadow text-center rounded-xs" type="submit">Compare prices <i class="fal fa-angle-right ml-1 small"></i></button>
            </form>
            @include('frontend.components.common.lazy-image', [
                'class' => 'lazy home-phone img-fluid d-lg-block d-none',
                'lazySrc' => '/images/home-iphone.png',
                'alt' => 'Phones',
            ])
        </div>

	</div>
</div>
