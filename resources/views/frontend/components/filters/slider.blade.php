<div class="row pr-2 mt-3">
    <div class="col-12">
        <h6 class="text-left font-weight-bold">
            <i class="fal fa-arrows-h text-muted"></i>
            <span class="{{ $filters->getName() }}-value-label">{{ $filters->getTitle() }}: <span class="{{ $filters->getName() }}-value">{{ $filters->getValue() }} - {{ $filters->getMaxValue() }}</span></span>
        </h6>
        @include('frontend.components.filters.slider-item', ['filters' => $filters])
    </div>
</div>

