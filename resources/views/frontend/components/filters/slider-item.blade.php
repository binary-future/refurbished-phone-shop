<input data-id="{{ $filters->getName() }}"
       name="{{ $filters->getName() }}"
       {{ $filters->isEnabled() ? '' : 'disabled' }}
       class="slider-filter w-100" type="text"
       data-slider-min="{{ $filters->getMinValue() }}"
       data-slider-max="{{ $filters->getMaxValue() }}"
       data-slider-step="{{ $filters->getStep() }}"
       data-slider-value="[{{ $filters->getValue() }}, {{ $filters->getMaxValue() }}]"
       @if ($filters->getTicks())
       data-slider-ticks="{{ $filters->getTicks() }}"
       @endif
       {{ $filters->isLockOnTicks() ? 'data-slider-lock-to-ticks=true' : '' }}
       {{ $filters->isUnlimited() ? 'data-slider-unlimited=true' : '' }}
/>
