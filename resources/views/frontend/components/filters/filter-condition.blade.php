@if($filters->isEnabled() && ! empty($filters->getFilters()))
    <div class="row pr-3 mt-3 mx-auto">
        <div class="col-12 py-1">
            <span class="text-center text-md-left font-weight-light h6"><i class="fad fa-satellite-dish mr-1 text-muted"></i> Conditions</span>
        </div>
        @foreach($filters->getFilters() as $filterItem)
            <div class="col-md-12 col-12 px-1 mx-auto">
                <label>
                    <input type="checkbox" name="{{ $filterItem->getName() }}" value="{{$filterItem->getValue()->getKey()}}"
                           class="card-input-element d-none model-filter"
                           data-id="{{ $filterItem->getName() }}-{{$filterItem->getValue()->getKey()}}"
                           data-multiple="true"
                           {{ $filterItem->isEnabled() ? '' : 'disabled' }}
                           {{ $filterItem->isChecked() ? 'checked' : '' }}
                    >
                    <div class="card card-body text-capitalize bg-light d-flex flex-row justify-content-center align-items-center p-1 {{ $filterItem->isEnabled() ? '' : 'disabled-filter__n' }}">
                        {{ $filterItem->getValue()->getValue() }}
                    </div>
                </label>
            </div>
        @endforeach
    </div>
@endif
