@if($filters->isEnabled() && ! empty($filters->getFilters()))
    <div class="row pt-2 pr-2 text-center mx-auto mx-lg-0">
        <div class="col-12 pl-1">
            <p class="text-center text-lg-left d-none h6 text-dark d-lg-block">Size:</p>
        </div>
        <div class="displayedCapacity col-12">
            <div class="col-lg-1 col-12 px-0 px-lg-1 d-flex mx-auto mx-lg-0 capacity-wrap">
                <label>
                    <input type="radio" name="capacity_mb"
                           value="all"
                           class="card-input-element d-none model-filter"
                           checked
                           data-id="capacity-all"
                    >
                    <div class="card card-body  flex-row justify-content-center align-items-center p-0">
                        <span class="text-center font-weight-bold d-none d-lg-block">All</span>
                        <span class="text-center font-weight-bold d-lg-none">All sizes</span>
                    </div>
                </label>
            </div>

            @foreach($filters->getFilters() as $filter)
                <div class="col-lg-1 col-12 px-0 px-lg-1 mx-auto mx-lg-0 d-lg-block capacity-wrap">
                    <label>
                        <input type="radio" name="{{ $filter->getName() }}_mb" value="{{$filter->getValue() ?: 'none'}}"
                               class="card-input-element d-none model-filter"
                               data-id="{{ $filter->getName() }}-{{$filter->getValue() ?: 'none'}}"
                               {{ $filter->isEnabled() ? '' : 'disabled' }}
                        >
                        <div class="card card-body flex-row justify-content-center align-items-center p-0 {{ $filter->isEnabled() ? '' : 'disabled-filter__n' }}">
                            <span class="text-center font-weight-bold small">
                                @if (! $filter->getValue())
                                    {{ 'None' }}
                                @else
                                    {!! present_gigabyte($filter->getValue()) !!}
                                @endif
                            </span>
                        </div>
                    </label>
                </div>
            @endforeach
        </div>

        <div class="accordion d-lg-none w-100 text-center mx-auto" id="accordionCapacity" id="headingCapacity">
            <button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#collapseCapacity" aria-expanded="true" aria-controls="collapseCapacity">
                Show all sizes <i class="fad pl-1 fa-chevron-circle-down pl-2"></i>
        	</button>

			 <div id="collapseCapacity" class="collapse hide" aria-labelledby="headingCapacity" data-parent="#accordionCapacity">

                 <div class="col-lg-1 col-12 px-0 px-lg-1 d-flex mx-auto mx-lg-0 capacity-wrap">
                     <label>
                         <input type="radio" name="capacity"
                                value="all"
                                class="card-input-element d-none model-filter capacity-filter"
                                checked
                                data-id="capacity-all"
                         >
                         <div class="card card-body flex-row justify-content-center align-items-center p-0">
                             <span class="text-center font-weight-bold d-none d-lg-block">All</span>
                             <span class="text-center font-weight-bold d-lg-none">All sizes</span>
                         </div>
                     </label>
                 </div>
                @foreach($filters->getFilters() as $filter)
                    <div class="col-lg-2 col-12 px-0 px-lg-1 d-flex mx-auto mx-lg-0 capacity-wrap">
                        <label>
                            <input type="radio" name="{{ $filter->getName() }}" value="{{$filter->getValue() ?: 'none'}}"
                                   class="card-input-element d-none model-filter capacity-filter"
                                   data-id="{{ $filter->getName() }}-{{$filter->getValue() ?: 'none'}}"
                                   {{ $filter->isEnabled() ? '' : 'disabled' }}
                            >
                            <div class="card card-body bg-white flex-row justify-content-center align-items-center p-0 {{ $filter->isEnabled() ? '' : 'disabled-filter__n' }}">
                                <span class="text-center font-weight-bold small">
                                    @if (! $filter->getValue())
                                        {{ 'None' }}
                                    @else
                                        {!! present_gigabyte($filter->getValue()) !!}
                                    @endif
                                </span>
                            </div>
                        </label>
                    </div>
                @endforeach
			</div>
		</div>
    </div>
@endif
