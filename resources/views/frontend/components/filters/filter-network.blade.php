@if($filters->isEnabled() && ! empty($filters->getFilters()))
    <div class="row pr-3 mt-3 mx-auto">
        <div class="col-12 py-1">
            <span class="text-center text-md-left font-weight-light h6"><i class="fad fa-satellite-dish mr-1 text-muted"></i> Network(s)</span>
        </div>
        @foreach($filters->getFilters() as $filterItem)
            <div class="col-md-4 col-3 px-1 mx-auto">
                <label>
                    <input type="checkbox" name="{{ $filterItem->getName() }}" value="{{$filterItem->getValue()->getKey()}}"
                           class="card-input-element d-none model-filter"
                           data-id="{{ $filterItem->getName() }}-{{$filterItem->getValue()->getSlug()}}"
                           data-multiple="true"
                           {{ $filterItem->isEnabled() ? '' : 'disabled' }}
                           {{ $filterItem->isChecked() ? 'checked' : '' }}
                    >
                    <div class="card card-body bg-light d-flex flex-row justify-content-center align-items-center p-1 {{ $filterItem->isEnabled() ? '' : 'disabled-filter__n' }}">
                        @if ($filterItem->getValue()->getImage())
                            @include('frontend.components.common.lazy-image', [
                                'lazySrc' => image_path($filterItem->getValue()->getImage()),
						        'class' => 'img-fluid',
                                'alt' => $filterItem->getValue()->getName(),
                                'style' => 'max-height: 100%'
                            ])
                        @else
                            {{ $filterItem->getValue()->getName() }}
                        @endif
                    </div>
                </label>
            </div>
        @endforeach
    </div>
@endif
