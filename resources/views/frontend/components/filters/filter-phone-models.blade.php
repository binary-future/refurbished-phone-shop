@if($filters->isEnabled() && ! empty($filters->getFilters()))
    <div class="row pr-2 mt-3">
        <div class="col-12">
            <span class="text-center text-md-left h6 font-weight-normal"> <i class="fad fa-mobile-android  text-muted pr-1"></i> Show deals for:</span>
        </div>
        @foreach($filters->getFilters() as $filter)
            <div class="col-12 px-1 model-filter-item {{ $loop->iteration > 6 ? 'd-none should-hide' : ''}}">
                <label>
                    <input type="checkbox" name="{{ $filter->getName() }}" value="{{$filter->getValue()->getSlug()}}"
                           class="card-input-element d-none model-filter"
                           data-id="{{ $filter->getName() }}-{{$filter->getValue()->getSlug()}}"
                           {{ $filter->isEnabled() ? '' : 'disabled' }}
                    >
                    <div class="card card-body bg-light d-flex flex-row justify-content-center align-items-center p-1 font-weight-normal {{ $filter->isEnabled() ? '' : 'disabled-filter' }}">
                 
                        {{ $filter->getValue()->getBrand()->getName() }} {{ $filter->getValue()->getName() }}
                    </div>
                </label>
            </div>
        @endforeach
        <div class="col-12 text-right">
            <a href="#" class="font-weight-normal text-danger show-more {{ count($filters->getFilters()) <= 6 ? 'd-none' : '' }}">[Show more...]</a>
            <a href="#" class="font-weight-normal text-danger show-less d-none">[Show less...]</a>
        </div>
    </div>
@endif
