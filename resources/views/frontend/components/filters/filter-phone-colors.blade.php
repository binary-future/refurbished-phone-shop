@if($filters->isEnabled() && ! empty($filters->getFilters()))
    <div class="row pt-3 pr-2 text-center mx-auto">
        <div class="col-12 pl-1">
            <p class="text-center h6 text-md-left text-dark d-none d-md-block">Colour:</p>
        </div>
        <div class="displayedColors col-8">
            <div class="col-12 col-lg-1 px-0 px-md-1 d-flex color-wrap">
                <label>
                    <input type="radio" name="color"
                           value="all"
                           class="card-input-element d-none model-filter color-filter"
                           checked
                           data-id="color-all"
                    >
                    <div class="bg-green flex-row justify-content-center align-items-center p-0">
                        <span class="text-center font-weight-bold d-none d-md-block">All</span>
                         <span class="text-center font-weight-bold d-md-none d-flex align-items-center">
                              <span class="color-dot mr-2" style="background: linear-gradient(to right, orange , yellow, green, pink, blue, violet);"> </span>
                             All colours</span>
                    </div>
                </label>
            </div>

             @foreach($filters->getFilters() as $filter)
                <div class="col-12 col-lg-1 px-0 px-md-1 d-lg-block color-wrap cursor-pointer"  title="{{ $filter->getValue()->getName() }}" data-toggle="tooltip">
                    <label>
                        <input type="radio" name="{{ $filter->getName() }}"
                               value="{{ $filter->getValue() ? $filter->getValue()->getKey() : '' }}"
                               class="card-input-element model-filter color-filter d-none"
                               data-id="{{ $filter->getName() }}-{{$filter->getValue() ? $filter->getValue()->getKey(): 'none'}}"
                               {{ $filter->isEnabled() ? '' : 'disabled' }}
                        >
                        <div class="card-color flex-row justify-content-center align-items-center p-0 cursor-pointer {{ $filter->isEnabled() ? '' : 'disabled-filter__n' }}">
                            @if($filter->getValue()->getHex())
                                <span class="color-dot" style="background-color: {{ '#' . $filter->getValue()->getHex() }};"></span>
                            <span class="pl-2 small d-md-none d-block">{{ $filter->getValue()->getName() }}</span>

                            @else
                                <span class="text-center small font-weight-light">
                                    @if(!$filter->getValue())
                                        {{ 'None' }}
                                    @else
                                        {{ $filter->getValue()->getName() }}
                                    @endif
                                </span>
                            @endif

                        </div>
                    </label>
                </div>
            @endforeach
        </div>

		<div class="accordion d-lg-none w-100 text-center mx-auto" id="accordionColors">
			 <button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#collapseColors" aria-expanded="true" aria-controls="collapseColors">
          Show all colours <i class="fad pl-1 fa-chevron-circle-down"></i>
        </button>

			 <div id="collapseColors" class="collapse hide" aria-labelledby="accordionColors" data-parent="#accordionColors">
                 <div class="col-12 col-lg-1 px-0 px-md-1 d-flex">
                     <label>
                         <input type="radio" name="color_mb"
                                value="all"
                                class="card-input-element model-filter color-filter d-none"
                                checked
                                data-id="color-all"
                         >
                         <div class="card card-body bg-green flex-row justify-content-center align-items-center p-0">
                             <span class="text-center font-weight-bold d-none d-md-block">All</span>
                             <span class="text-center font-weight-bold d-md-none d-flex align-items-center">
                                 <span class="color-dot mr-2" style="background: linear-gradient(to right, orange , yellow, green, pink, blue, violet);"> </span>
                                 All colours
                             </span>
                         </div>
                     </label>
                 </div>
        @foreach($filters->getFilters() as $filter)
            <div class="col-12 col-lg-2 px-0 px-md-1 d-flex mx-auto"  title="{{ $filter->getValue()->getName() }}" data-toggle="tooltip">
                <label>
                    <input type="radio" name="{{ $filter->getName() }}_mb"
                           value="{{ $filter->getValue() ? $filter->getValue()->getKey() : '' }}"
                           class="card-input-element model-filter color-filter d-none"
                           data-id="{{ $filter->getName() }}-{{$filter->getValue() ? $filter->getValue()->getKey(): 'none'}}"
                           {{ $filter->isEnabled() ? '' : 'disabled' }}
                    >
                    <div class="card card-color flex-row justify-content-center align-items-center p-0 {{ $filter->isEnabled() ? '' : 'disabled-filter__n' }}">
                        @if($filter->getValue()->getHex())
                            <span class="color-dot" style="background-color: {{ '#' . $filter->getValue()->getHex() }};"></span>
						    <span class="pl-2 small d-lg-none d-block">{{ $filter->getValue()->getName() }}</span>
                        @else
                            <span class="text-center small font-weight-light">
                                @if(!$filter->getValue())
                                    {{ 'None' }}
                                @else
                                    {{ $filter->getValue()->getName() }}
                                @endif
                            </span>
                        @endif
                    </div>
                </label>
            </div>
        @endforeach
			</div>
		</div>




    </div>
@endif
