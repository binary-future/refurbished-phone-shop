@if($filters || !empty($filters))
    @php($name = (is_object($filters) && method_exists($filters, 'getName')) ? $filters->getName() : current($filters)->getName())
    <div class="{{ 'filter-container-' . $name }} w-100">
        @include($template, ['filters' => $filters, 'mobile' => isset($mobile)])
    </div>
@endif
