<div class="row pr-3 mt-3 slider {{ $filters->getMinValue() === $filters->getMaxValue() || ! $filters->isEnabled() ? 'disabled' : 'enabled' }}">
    <div class="col-12">
        <span class="text-center text-md-left d-block font-weight-light h6">
            <i class="fal fa-arrows-h text-muted"></i>
            <span class="{{ $filters->getName() }}-value-label">
                @if($filters->isEnabled())
                    {{ $filters->getTitle() }}:
                    <span class="pl-2 {{ $filters->getName() }}-value price-sans large font-weight-bold">&pound;{{ $filters->getValue() }} - &pound;{{ $filters->getMaxValue() }}</span>
                @else
                    {{ $filters->getTitle() }}
                @endif
            </span>
        </span>
        @include('frontend.components.filters.slider-item', ['filters' => $filters])
    </div>
</div>


