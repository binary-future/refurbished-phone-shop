@if($filters->isEnabled() && ! empty($filters->getFilters()))
    <div class="row pr-2 mt-3 mt-md-2 pb-4 text-center text-md-right">
        <div class="col-12">
            <span class="text-left font-weight-normal h6 cursor-pointer" data-toggle="collapse" href="#collapseUpfront" role="button" aria-expanded="false" aria-controls="collapseUpfront">Popular upfront costs <i class="fal fa-angle-up pl-1"></i>
			<i class="fal fa-angle-down pl-1"></i></span>
		
        </div>


		
		<div class="collapse col row mx-auto" id="collapseUpfront">
        @foreach($filters->getFilters() as $filter)
            <div class="col px-1 d-flex">
                <label>
                    <input type="checkbox"
                           name="{{ $filter->getName() }}"
                           value="{{ $filter->getValue() }}"
                           class="card-input-element d-none model-filter"
                           data-id="{{ $filter->getName() }}-{{$filter->getValue()}}"
                            {{ count($filters->getFilters()) === 1 ? 'checked disabled' : ''}}
                    >
                    <div class="card card-body bg-light d-flex flex-row justify-content-center align-items-center p-0">
                        <span class="text-center h5 font-weight-bold my-auto">
                            @if($filter->getValue() == 0)
							<span class="price-sans">{{ 'Free' }}</span>

                            @else
                                <span class="small">Up to</span><br/>
							<span class="price-sans">&pound;{{ $filter->getValue() }}</span>
                            @endif
                        </span>
                    </div>
                </label>
            </div>
        @endforeach
		</div>
    </div>
@endif