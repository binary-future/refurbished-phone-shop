<img class="lazy {{ $class ?? '' }}"
     src="{{ $placeholder ?? "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mMUrAcAAKcAkqLcIOsAAAAASUVORK5CYII=" }}"
     @if (isset($lazySrc))
        data-src="{{ $lazySrc }}"
     @endif
     @if (isset($srcset))
        data-srcset="{{ $srcset }}"
     @endif
     @if (isset($srcsizes))
        data-sizes="{{ $sizes }}"
     @endif
     @if (isset($style))
        style="{{ $style }}"
     @endif
     alt="{{ $alt }}"
     @if (isset($title))
        title="{{ $title }}"
     @endif
>
