<nav class="navbar navbar-expand-lg navbar-light bg-white py-0 py-md-3 mb-2 mb-lg-0" style="z-index: 11;" role="navigation">
    <div class="container pl-0">

        <a class="navbar-brand mb-2 text-hide" href="{{ route('home') }}"><img data-src="" alt="" class="lazy "></a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse bg-mobile-menu pl-2 pl-lg-0 pb-3 pb-md-0" id="navbarSupportedContent">
            <ul class="navbar-nav ml-0 ml-md-4 mr-auto font-weight-light ">
                <li class="nav-item dropdown position-static pr-2">
                    <a class="nav-link font-weight-400 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">iPhone <i class="fal fa-angle-down  small text-muted pl-1 d-inline"></i></a>
                    <div class="dropdown-menu shadow-down text-white bg-mobile-menu w-100 py-3 border-0" aria-labelledby="navbarDropdown">
                        <div class="row mx-auto w-sm-50 w-100">

                            <div class="col-lg-4 offset-lg-2 col-12">
                                <span class="h6 font-weight-bold">Latest iPhones</span>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-11-pro-max']) }}"><span class="text-muted">Refurbished</span> iPhone 11 Pro Max</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-11-pro']) }}"><span class="text-muted">Refurbished</span> iPhone 11 Pro</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-11']) }}"><span class="text-muted">Refurbished</span> iPhone 11</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-xs-max']) }}"><span class="text-muted">Refurbished</span> iPhone Xs Max</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-xs']) }}"><span class="text-muted">Refurbished</span> iPhone Xs</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-xr']) }}"><span class="text-muted">Refurbished</span> iPhone Xr</a>
                            </div>

                            <div class="col-lg-4 col-12">
                                <span class="h6 font-weight-bold">Cheaper models</span>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-8-plus']) }}"><span class="text-muted">Refurbished</span> iPhone 8 Plus</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-8']) }}"><span class="text-muted">Refurbished</span> iPhone 8</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-7-plus']) }}"><span class="text-muted">Refurbished</span> iPhone 7 Plus</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-7']) }}"><span class="text-muted">Refurbished</span> iPhone 7</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-6s-plus']) }}"><span class="text-muted">Refurbished</span> iPhone 6s Plus</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-primary font-weight-normal small text-right" href="{{ route('manufacturers.view', ['brand' => 'apple']) }}">View all iPhones <i class="fal fa-angle-right pl-1"></i></a>
                            </div>

                        </div>
                    </div>
                </li>
                <li class="nav-item dropdown position-static pr-2">
                    <a class="nav-link font-weight-400 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Samsung Galaxy <i class="fal fa-angle-down small text-muted pl-1 d-inline"></i></a>

                     <div class="dropdown-menu shadow-down bg-mobile-menu w-100 py-3 multi-column columns-2 border-0" aria-labelledby="navbarDropdown">
                          <div class="row mx-auto w-sm-50 w-100">

                             <div class="col-lg-4 offset-lg-2 col-12">
                                <span class="h6 font-weight-bold">Latest models</span>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s10-5g']) }}"><span class="text-muted">Refurbished</span> Galaxy S10 5G</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s10-lite']) }}"><span class="text-muted">Refurbished</span> Galaxy S10 Lite</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s20-ultra']) }}"><span class="text-muted">Refurbished</span> Galaxy S20 Ultra</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s10e']) }}"><span class="text-muted">Refurbished</span> Galaxy S10e</a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s9-plus']) }}"><span class="text-muted">Refurbished</span> Galaxy S9 plus</a>
                                <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s9']) }}"><span class="text-muted">Refurbished</span> Galaxy S9</a>
                                <div class="dropdown-divider"></div>

                             </div>

                             <div class="col-lg-4" >
                                 <span class="h6 font-weight-bold">Cheaper models</span>

                                 <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-a90-5g']) }}"><span class="text-muted">Refurbished</span> Galaxy A90 5G</a>
                                 <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-a80']) }}"><span class="text-muted">Refurbished</span> Galaxy A80</a>
                                 <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-a71']) }}"><span class="text-muted">Refurbished</span> Galaxy A71</a>
                                 <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-a51']) }}"><span class="text-muted">Refurbished</span> Galaxy A51</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-note-10-lite']) }}"><span class="text-muted">Refurbished</span> Galaxy Note 10 Lite</a>
                                 <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-note-10-plus']) }}"><span class="text-muted">Refurbished</span> Galaxy Note 10+</a>
                                 <div class="dropdown-divider"></div>

                                 <a class="dropdown-item  text-primary font-weight-normal small text-right" href="{{ route('manufacturers.view', ['brand' => 'samsung']) }}">View all Samsung <i class="fal fa-angle-right pl-2"></i></a>
                             </div>
                         </div>
                     </div>
                </li>
                <li class="nav-item dropdown position-static">
                    <a class="nav-link font-weight-400 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Other Phones <i class="fal fa-angle-down small text-muted pl-1 d-inline"></i></a>

                    <div class="dropdown-menu shadow-down bg-mobile-menu w-100 py-3 multi-column columns-2 border-0" aria-labelledby="navbarDropdown">
                      <div class="row mx-auto w-sm-50 w-100">
                         <div class="col-lg-4 offset-lg-2">
                            <span class="h6 font-weight-bold">Huawei</span>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'p30-pro']) }}"><span class="text-muted">Refurbished</span> Huawei P30 Pro</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'p30']) }}"><span class="text-muted">Refurbished</span> Huawei P30</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'p20-pro']) }}"><span class="text-muted">Refurbished</span> Huawei P20 Pro</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'p20']) }}"><span class="text-muted">Refurbished</span> Huawei P20</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'honor-view-20']) }}"><span class="text-muted">Refurbished</span> Honor View 20</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'honor-20-pro']) }}"><span class="text-muted">Refurbished</span> Honor 20 Pro</a>
                             <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'honor-20']) }}"><span class="text-muted">Refurbished</span> Honor 20</a>
                         </div>

                         <div class="col-lg-4">
                            <span class="h6 font-weight-bold">Google</span>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'google', 'model' => 'pixel-4-xl']) }}">Pixel 4 XL</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'google', 'model' => 'pixel-4']) }}">Pixel 4</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'google', 'model' => 'pixel-3a-xl']) }}">Pixel 3a XL</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'google', 'model' => 'pixel-3a']) }}">Pixel 3a</a>
                            <span class="h6 font-weight-bold">Others</span>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'nokia', 'model' => '72']) }}">Nokia 7.2</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'oneplus', 'model' => '7-pro-5g']) }}">OnePlus 7 Pro 5G</a>
                            <a class="dropdown-item" href="{{ route('models.view', ['brand' => 'xiaomi', 'model' => 'mi-mix-3-5g']) }}">Xiaomi Mi Mix 3</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item  text-primary font-weight-normal small text-right" href="{{ route('manufacturers.main') }}">View all phones <i class="fal fa-angle-right pl-2"></i></a>
                         </div>

                      </div>
                    </div>
                </li>
            </ul>
        </div>

        @if(request()->route() && request()->route()->getName() === 'home')

        @else
            <div class="navbar-collapse collapse w-100 order-3 dual-collapse2 header-search">
                <ul class="navbar-nav ml-auto">
                      <li class="nav-item dropdown">
                            <a class="nav-link font-weight-400" href="{{ route('stores.main') }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-search"></i></a>

                            <div class="p-0 border-0 navbar-container" aria-labelledby="navbarDropdown">
                                <form class="form-inline" method="GET" action="{{ route('search.model') }}">
                                    <div class="input-group-lg">
                                        <input id="model-search" class="form-control shadow" size="45" name="name" type="text" placeholder="e.g. iPhone 8 Plus, Galaxy S10" data-url="{{ route('search.autocomplete') }}" autocomplete="off">
                                    </div>
                                    <button class="btn btn-success btn-lg font-weight-normal input-group-lg my-2 my-lg-0 mx-auto border-radius-left-none shadow text-center rounded-xs border-0" type="submit"><i class="far fa-search"></i></button>
                                </form>
                            </div>
                      </li>
                </ul>
            </div>
        @endif

	</div>
</nav>
