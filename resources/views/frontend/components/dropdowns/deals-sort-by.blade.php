<div class="mt-0 mt-lg-3 pb-0 pb-lg-1">

    <select class="custom-select select-filter shadow-sm " id="sort-by-filter" name="{{ $select->getName() }}">
        @foreach($select->getOptions() as $option)
            <option value="{{ $option->getValue() }}" {{ $option->isSelected() ? 'selected' : '' }}>{{ $option->getName() }}</option>
        @endforeach
    </select>
</div>