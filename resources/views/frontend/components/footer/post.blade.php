<div class="col-sm-4 col-12">
    <a href="/mobile-news/{{ $post->getTitle() }}" title="{{ $post->getShortTitle() }}" >
        <div class="card">
            <div class="col-sm-12 col-4 d-flex pb-2 pb-md-0 justify-content-center">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'h-100 w-100 rounded d-none d-sm-block',
                    'lazySrc' => $post->getImage(),
                    'alt' => '',
                    'style' => 'filter: grayscale(30%);'
                ])
            </div>
            <div class="col-sm-12 col-12 card-body mt-0 mt-md-3">
                <h5 class="card-title h4 text-success shrink-sm">{{ $post->getShortTitle() }}</h5>
                <div class="card-text mb-5 text-dark d-none d-md-block">
                    {!! $post->getContent() !!}
                </div>
                <div class="text-muted">
                    <i class="fal fa-clock"></i> 2 min read
                </div>
            </div>
        </div>
    </a>
</div>

