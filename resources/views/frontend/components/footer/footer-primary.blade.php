@include('frontend.components.footer.posts')

<footer class="bg-dark text-secondary pt-5 pb-5">
    <div class="container text-center text-md-left pb-5">
        <div class="row">

			<div class="col-12 text-center mx-auto py-5">

				<img data-src="/images/logo-white.png" alt="" class="lazy img-fluid" />

			</div>


            <div class="col-md-3 mb-md-0 mt-5 pt-5">
                <ul class="list-unstyled">
                    <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('home') }}">Home</a>
                    </li>
					 <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('manufacturers.view', ['brand' => 'apple']) }}" title="Browse iPhone contract deals">All iPhone deals</a>
                    </li>
					 <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('manufacturers.view', ['brand' => 'samsung']) }}" title="Samsung Galaxy deals (UK)">All Samsung deals</a>
                    </li>
					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('manufacturers.view', ['brand' => 'lg']) }}" title="Browse all LG contracts">All LG deals</a>
                    </li>
					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('manufacturers.view', ['brand' => 'sony']) }}" title="All Sony mobile phone deals">All Sony deals</a>
                    </li>

					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('manufacturers.view', ['brand' => 'xiaomi']) }}" title="All Xiaomi mobile phone deals">All Xiaomi deals</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-3 mb-md-0 mt-5 pt-5">

                <ul class="list-unstyled">

					 <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('stores.main') }}">Stores we compare</a>
                    </li>
					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('about') }}">About us</a>
                    </li>
                    <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('help') }}">Help</a>
                    </li>

					 <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('accessibility') }}">Accessibility</a>
                    </li>

					<li class="py-1">
                        <a class="text-decoration-none text-light" href="/mobile-news">Mobile News</a>
                    </li>

                    <li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('contact.us') }}">Contact us</a>
                    </li>
                </ul>
            </div>


			<div class="col-md-3 mb-md-0 mt-5 pt-5">

                <ul class="list-unstyled">
					<li class="font-weight-bold small"><span>Top phones:</span></li>


					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('models.view', ['brand' => 'apple', 'model' => 'iphone-11-pro']) }}">iPhone 11 Pro</a>
                    </li>

					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s20-plus-5g']) }}">Galaxy S20+ 5G</a>
                    </li>
                 	<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('models.view', ['brand' => 'samsung', 'model' => 'galaxy-s10e']) }}">Galaxy S10e <span class="text-light">deals</span></a>
                    </li>
					<li class="py-1">

						<a class="text-decoration-none text-light" href="{{ route('models.view', ['brand' => 'huawei', 'model' => 'p30-pro']) }}">Huawei P30 Pro</a>

					</li>

				</ul>
			</div>
				<div class="col-md-3 mb-md-0 mt-5 pt-5">
				 <ul class="list-unstyled">

					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('terms') }}">Terms &amp; conditions</a>
                    </li>
					<li class="py-1">
                        <a class="text-decoration-none text-light" href="{{ route('privacy') }}">Privacy + cookies</a>
                    </li>
                </ul>
				</div>


			  <div class="col-12 mt-md-0 mt-5 pt-5 border-top border-muted">
                <div class="text-center text-white">


						<span class="font-weight-light">&copy; refshop <?php echo date("Y"); ?>.</span>

					<div class="footer-social pt-2 mx-auto text-center">
						<a href="#" rel="noopener" target="_blank"><i class="fab fa-facebook"></i><span class="sr-only">Facebook</span></a>
						<a href="#" rel="noopener" target="_blank"><i class="fab fa-twitter"></i><span class="sr-only">Twitter</span></a>
						<a href="#" rel="noopener" target="_blank"><i class="fab fa-instagram"></i><span class="sr-only">Instagram</span></a>
						<a href="#"><i class="fab fa-youtube"></i><span class="sr-only">YouTube</span></a>
					</div>


                </div>
            </div>
        </div>
    </div>

</footer>

<noscript></noscript>
