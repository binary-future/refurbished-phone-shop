@php($posts = $tagPosts ?? $posts)
@if($posts->isNotEmpty())
    <div class="container mb-5">
        <div class="row row-eq-height">
            <div class="w-100">
                <h3 class="text-dark text-center text-md-left mb-5  pt-5 h4">You might like these:</h3>
            </div>
            @yield('footer-posts')
            @foreach($posts as $post)
                @include('frontend.components.footer.post', [ 'post' => $post ])
            @endforeach

        </div>
    </div>
@endif


