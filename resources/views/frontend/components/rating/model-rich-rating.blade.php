<div itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating">
    <span itemprop="itemReviewed">{{ $brand->getName() }} {{ $model->getName() }}</span> is rated
    <span class="font-weight-bold" itemprop="ratingValue">{{ $rating->getRatingInStars() }}</span> out of
    <span class="font-weight-bold" itemprop="bestRating">5</span> from
    <span itemprop="reviewCount">{{ $rating->getReviews() }}</span> reviews.
</div>