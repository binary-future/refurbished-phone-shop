@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            You have new notification
        @endcomponent
    @endslot

    {{-- Body --}}
    <strong>From:</strong> {{ $name }}.<br/>
    <strong>Email:</strong> {{ $email }}.<br/>
    <strong>Message:</strong> {{ $message }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent