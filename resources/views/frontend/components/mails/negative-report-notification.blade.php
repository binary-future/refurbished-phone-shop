@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ $store->getName() }} import failed!!
        @endcomponent
    @endslot

    {{-- Body --}}
    <strong>Date:</strong> {{ $report->getCreatedAt() }}.<br/>
    <strong>Message:</strong> {{ $report->getMessage() }}

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent