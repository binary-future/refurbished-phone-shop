<div class="col-12">
    <h1 class="font-weight-bold h4 mb-3">About {{ $store->getName() }} - Reviews, Deals and more
        @if(auth()->user())
            <a href="{{ route('admin.stores.edit', ['store' => $store->getSlug()]) }}" class="btn btn-outline-success"><i class="fad fa-edit"></i></a>
        @endif
    </h1>
    <div class="col-12 mb-5">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-12 justify-content-center d-flex">
                <a href="{{ route('stores.view', ['store' => $store->getSlug()]) }}" >
                    @include('frontend.components.common.lazy-image', [
                        'class' => 'img img-fluid img-thumbnail',
                        'lazySrc' => image_path($store->getImage()),
                        'alt' => $store->getName() . ' reviews'
                    ])
                </a>
            </div>
            <div class="col-md-8 col-sm-6 col-12">
                <table class="table table-hover">
                    <thead style="height: 1px">
                    <tr>
                        <th class="col-3 py-0"></th>
                        <th class="col-9-md col-12 py-0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($store->getLink())
                        <tr>
                            <td >Website:</td>
                            <td class="justify-content-center d-flex">
                                <a href="{{ $store->getLink()->getLink() }}" rel="nofollow" target="_blank">
                                    {{ $store->getLink()->getLink() }}
                                </a>
                            </td>
                        </tr>
                    @endif
                    <tr>
                        <td>Trustpilot reviews:</td>
                        <td class="text-center">
                                <i class="fad fa-times text-danger"></i> No rating yet
                        </td>
                    </tr>
                    <tr>
                        <td class="display-desktop">About {{ $store->getName() }}:</td>
                        <td class="justify-content-center d-flex">
                            Description will be soon
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
