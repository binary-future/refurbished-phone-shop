<div class="row">
    @foreach($stores as $store)
        @include('frontend.stores.components.store-item', ['store' => $store])
        <div class="col-12 text-right mb-3 border-bottom border-success">
            <a class="btn btn-outline-deep-blue mb-2" href="{{ route('stores.view', ['store' => $store->getSlug()]) }}" title="View our {{ $store->getName() }} reviews">
                    <i class="fad fa-star"></i> Read our review</a>

        </div>
    @endforeach
</div>
