
    <div class="col-12 mt-3 mb-2 py-5">
        <h3 class="text-center h2 text-black font-weight-bold">Comparing refurbished prices, 24/7</h3>
		<p class="text-muted text-center">We check over 15 leading UK shops to bring you the best deals on used handsets.</p>


    <div class="row mt-5 pt-5">

        @foreach($stores as $store)
            <div class="col-md-3 col-sm-6 col-6 mb-3 d-flex justify-content-center">
                @include('frontend.components.common.lazy-image', [
                    'class' => 'lazy border border-light shadow-sm img-fluid bg-light rounded-lg p-3',
                    'lazySrc' => image_path($store->getImage()),
                    'alt' => $store->getName() . ' logo',
                    'style' => 'max-height: 130px;'
                ])
            </div>
        @endforeach

	</div>

    <div class="col-12 text-center my-5">
        <a href="{{ route('stores.main') }}" class="btn btn-primary text-blue font-weight-400 px-3">View the full list <i class="fal fa-arrow-right pl-2 small"></i></a>
    </div>

</div>
