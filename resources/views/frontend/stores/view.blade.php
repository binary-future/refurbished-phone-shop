@extends('frontend.layouts.app')

@section('title'){{ $store->getName() }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('stores.main') }}">Stores</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $store->getName() }}</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 my-2">
                @include('frontend.stores.components.store-item', ['store' => $store])
            </div>
            @if($store->getLink())
                <div class="col-12 text-right">
                    <a class="btn btn-outline-deep-blue mb-2"
                       rel="nofollow" target="_blank"
                       href="{{ $store->getLink()->getLink() }}">
                        <i class="fad fa-external-link-alt"></i> Visit {{ $store->getName() }}</a>
                </div>
            @endif
            <div class="col-12">
                <h3 class="text-center font-weight-bold"> Details </h3>
                @if($store->getDescription())
                    <div class="w-100">
                        {!! $store->getDescription()->getContent() !!}
                    </div>
                @else
                    <p>Description will be added soon</p>
                @endif
            </div>
        </div>
    </div>
    @if($phones->isNotEmpty())
        <div class="container-fluid bg-millennial-pink-2 py-5 mt-5">
            <div class="container pt-4 rounded">
                <div class="row">
                    <div class="col-12">
                        @include('frontend.phone.components.phones.phones-info-list', ['phones' => $phones])
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
