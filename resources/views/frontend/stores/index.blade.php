@extends('frontend.layouts.app')

@section('title'){{ 'Stores'}}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Stores</li>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 my-2">
                <h3 class="text-center font-weight-bold">All Stores</h3>
            </div>
            <div class="col-12">
                @include('frontend.stores.components.stores-list', ['stores' => $stores])
            </div>
        </div>
    </div>
@endsection
