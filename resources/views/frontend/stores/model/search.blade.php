@extends('frontend.layouts.app')

@section('title'){{ 'Search' }}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ 'Search' }}</li>
@endsection

@section('content')
    @include('frontend.components.jumbotron.jumbotron')
    <div class="container">
        <div class="row">
            @if($models->count())
                <div class="col-12 my-2">
                    <h1 class="text-center font-weight-bold">We found {{ $models->total() }} {{ $models->total() > 1 ? 'devices' : 'device' }} matching your request</h1>
                </div>
                <div class="col-12">
                    @include('frontend.phone.components.models.models-list', ['models' => $models])
                </div>
                <div class="col-12 justify-content-center d-flex">
                    {{ $models->links() }}
                </div>
            @else
                <div class="col-12 my-2">
                    <h1 class="text-center">Sorry! Nothing was found <i class="fal fa-frown"></i></h1>
                </div>
            @endif
        </div>
    </div>
@endsection
