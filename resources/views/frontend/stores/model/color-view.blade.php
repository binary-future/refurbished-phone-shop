@extends('frontend.layouts.app')

@section('title'){!! sprintf('%s %s %s - Best Pay Monthly deals 🥇', $color->getName(), $brand->getName(), $model->getName()) !!}@endsection
@section('description'){{ sprintf('Compare %s %s %s deals ✓ Find the best pay monthly contract in any colour! ✓ Find your perfect phone &amp; plan', $color->getName(),  $brand->getName(), $model->getName() )}}@endsection

@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.main') }}">Mobile Contracts</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.view', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }}</a></li>
    <li class="breadcrumb-item"><a href="{{ route('models.view', [
        'brand' => $brand->getSlug(),
        'model' => $model->getSlug()
        ]) }}">
            {{ $model->getName() }} deals</a></li>
    <li class="breadcrumb-item active" aria-current="page">{{ $color->getName() }}</li>

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 my-2 pt-4 pb-4 bg-millennial-pink">
                <div class="row">
                    @include('frontend.phone.components.models.model-color-info', ['brand' => $brand, 'model' => $model, 'color' => $color])
                </div>
            </div>
        </div>
    </div>
    <div class="container">
		<h2 class="mb-3 mt-5 h3">Latest {{ $model->getName() }} Pay Monthly Deals in {{ $color->getName() }} </h2>
        <div class="row">
            <div class="col-12">
                @include('frontend.deals.components.deals.deals-grid-list', ['deals' => $deals])
            </div>
            <div class="col-12 text-center mb-4 pb-5 pt-5">
                <a href="{{ route('models.view', [
                            'brand' => $brand->getSlug(),
                            'model' => $model->getSlug()
                        ]) }}" class="btn btn-primary btn-lg mt-2">Compare all {{ $model->getName() }} deals <i class="fal fa-arrow-right small ml-1"></i></a>
            </div>
        </div>
    </div>

    @include('frontend.deals.components.deals.deals-info-modal')
@endsection
