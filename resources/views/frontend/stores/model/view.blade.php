@extends('frontend.layouts.app')

@section('title'){!! sprintf('%s %s DEALS - 🥇 Contracts from &pound;4/month ', $brand->getName(), $model->getName(), $bestDeal ? $bestDeal->getPayment()->getMonthlyCost() : 0) !!}@endsection
@section('description'){{ sprintf('Compare %s %s deals UK ✅ More COLOURS, contracts than anyone else ✅ Find the best price monthly plan from 850+ offers', $brand->getName(), $model->getName() )}}@endsection


@section('breadcrumbs-content')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Mobile Contracts</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.main') }}">Makes</a></li>
    <li class="breadcrumb-item"><a href="{{ route('manufacturers.view', ['brand' => $brand->getSlug()]) }}">{{ $brand->getName() }}</a></li>
    <li class="breadcrumb-item active d-none d-md-block" aria-current="page">{{ $model->getName() }} deals</li>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            @include('frontend.components.bars.loading-bar-search')
           <div class="bg-default pt-4 pb-4 w-100 shadow-lg">
			<div class="col-12 __prod-hero">


					<div class="container">
					@include('frontend.phone.components.models.model-info', [
						'brand' => $brand,
						'model' => $model,
						'phones' => $phones,
						'filters' => $phonesFilter ?? null,
						'relativeModels' => $relativeModels,
						'lastChecked' => $lastChecked,
						'phoneImages' => $phoneImages,
					])
					</div>
				</div>
            </div>
		</div>
    </div>
    <div class="container-fluid" style="background: #eee;">
		<div class="container">
        <main class="row">
            <div class="col-md-3 col-12">
                @include('frontend.phone.components.models.model-side-bar', ['filters' => $sideBarFilters])
            </div>
            <div class="col-lg-9 col-12">
                <div class="row">
                    <div class="col-12 text-right d-lg-block d-none">
                        <div class="row justify-content-end">
                            @include('frontend.components.dropdowns.deals-sort-by', ['select' => $sortByDropdown])
                        </div>
                    </div>
                    <div class="col-12" id="model-deals" data-href="{{ route('models.view.filter', ['brand' => $brand->getSlug(), 'model' => $model->getSlug()]) }}">
                        @include('frontend.deals.components.deals.deals-list', ['deals' => $deals, 'phones' => $phones])
                    </div>
                </div>
            </div>
            @if($model->getDescription())
                <div class="col-12 my-2">
                    @include('frontend.phone.components.models.model-description', ['model' => $model, 'description' => $model->getDescription()])
                </div>
            @endif
        </main>
    </div>
</div>

    @if(! empty($specs))
    <div class="container-fluid bg-millennial-pink-2 py-5">
        <div class="container bg-light pt-4 rounded">
            <div class="row">
                <div class="col-12">
                    @include('frontend.phone.components.models.specs-section', ['model' => $model, 'brand' => $brand, 'specs' => $specs])
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($dealsInfo->isValid())
        <div class="container-fluid bg-secondary py-5">
            <div class="container bg-light p-5 rounded">
                @include('frontend.phone.components.models.contracts-deals-faq', ['dealsInfo' => $dealsInfo, 'model' => $model, 'brand' => $brand])
            </div>
        </div>
    @endif
    @include('frontend.deals.components.deals.deals-info-modal')
@endsection

@push('js')
    @include('frontend.phone.components.models.product-schema', ['model' => $model, 'brand' => $brand, 'dealsInfo' => $dealsInfo, 'image' => $model->getImage()])
@endpush
