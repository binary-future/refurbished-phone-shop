<!doctype html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<link rel="dns-prefetch" href="https://cdnjs.cloudflare.com"/>
@if(cdn_path())
<link rel="dns-prefetch" href="{{ cdn_path() }}"/>
@endif
<link rel="dns-prefetch" href="https://kit.fontawesome.com"/>
<link rel="dns-prefetch" href="https://kit-pro.fontawesome.com"/>
 @stack('dns-prefetch')
<link rel="preload" href="https://in.getclicky.com" crossorigin/>
<link rel="preload" href="https://static.getclicky.com" crossorigin/>
<link rel="preload" href="https://i.ytimg.com" crossorigin/>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>@yield('title', config('app.name') )</title>
<meta name="description" content="@yield('description')">
@if ($__env->yieldContent('canonical') !== 'hide')
<link rel="canonical" href="@yield('canonical', request()->url() )"/>
@endif

<!-- CSRF Token -->

<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('images/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('images/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('images/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('images/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('images/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('images/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png') }}">
<link rel="manifest" href="/manifest.json" crossorigin="use-credentials">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<!-- Styles -->

<link rel="stylesheet" href="{{ mix('css/main.css') }}" />
<!-- <link rel="stylesheet" href="{{ mix('css/custom.min.css') }}" /> -->
	<link rel="stylesheet" href="/css/custom.css" />
<meta property="og:locale" content="en_GB" />
<meta property="og:title" content="@yield('title')" />
<meta property="og:type" content="website" />
<meta property="og:image" content="" />
<meta property="og:site_name" content="" />
<meta property="og:url" content="{{ request()->url() }}" />
<meta property="og:description" content="@yield('description')" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="@yield('title')" />
<meta name="twitter:description" content="@yield('description')" />
<meta name="twitter:image" content="{{ asset('/images/favicon-96x96.png') }}" />
<meta name="twitter:url" content="{{ request()->url() }}" />
<meta name="twitter:site" content="" />
<script>var clicky_site_ids = clicky_site_ids || []; clicky_site_ids.push(101241445);</script>
<script async src="//static.getclicky.com/js"></script>
</head>
<body itemscope itemtype="http://schema.org/WebPage">
<div id="app" class="@stack('page-class')">
  <div class="page-content">
    <noscript>
    <p class="alert alert-danger">You need to turn on your Javascript to use refshop. Some parts of this website will not work while this is disabled. <a href="https://www.enable-javascript.com/" target="_blank">Read more</a> </p>
    </noscript>
    @include('frontend.components.header.header-primary')
    @if(\Request::is('/'))
    @include('frontend.components.jumbotron.jumbotron')
    @endif
    @yield('content')
    @include('frontend.components.footer.footer-primary') </div>
</div>
<script defer src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
<script defer src="https://kit.fontawesome.com/361c32bca1.js" crossorigin="anonymous"></script>
<script>
    window.addEventListener("load", function(){
        window.cookieconsent.initialise({
            "palette": {
                "popup": {
                    "background": "rgba(255,255,255,1)",
                     "text": "#555"
                },
                "button": {

                            "background": "#db004d",
                            "text": "#ffffff"
                }
            },
			decoratorismissOnScroll: 10,
    		dismissTimeOut: 10,
            "position": "bottom-left",
            "content": {
                "message": "refshop uses cookies to make sure you're getting the best experience from us. By clicking 'Agree' below you agree to our",
                "dismiss": "Agree",
                "link": "cookie policy",
                "href": ""
            }
        })});
</script>
<script src="{{ mix('/js/app.js') }}"></script>
<script type="text/javascript">

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
@stack('js')
</body>
</html>
