@extends('frontend.layouts.app')

@section('description', 'Compare prices on unlocked, refurbished phones ✅ iPhone, Samsung Galaxy &amp; more. ✅ Find your perfect second-hand, used device now')

@section('title'){{ 'Unlocked iPhone, Samsung Galaxy phones | Compare SIM-Free, refurbished prices 🥇' }}@endsection

@section('content')
    <div class="container-fluid bg-white text-dark pb-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-2">
                    @include('frontend.stores.components.stores-main-page', ['stores' => $stores])
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid bg-dark text-white pb-5">
		<div class="container">
        <div class="row row-eq-height">
            <div class="col-12 my-3 py-5">
                <h3 class="mt-5 mb-3 text-center h2 font-weight-bold">Most wanted &#128293;</h3>
            </div>
            @include('frontend.phone.components.models.models-list', ['models' => $topModels])
        </div>
		</div>
    </div>
    <div class="container-fluid bg-blue pb-5">
		<div class="container">
			<div class="row">
				<div class="col-12 my-3 text-center">
					<h3 class="mt-5 mb-3 h2 text-white font-weight-bold">Hot new refurbs</h3>
					<p class="text-white">Here are the latest used devices and handsets:</p>
				</div>
				@include('frontend.phone.components.models.models-list', ['models' => $latestModels])
			</div>
		</div>
    </div>

	<div class="container-fluid bg-white pt-5 pb-5 text-left p-1">
		<div class="container">


			   <h3 class="mt-5 mb-3 text-left h4 text-muted text-uppercase">Find your perfect phone partner <span style="color: #E30010;">&#10084;</span></h3>
			<div class="row seo-desc">

<div class="col-12">
			<p class="lead">You love your phone, but why pay more for it? </p>

				</div>
<div class="col-md-6 col-12">
				<p>There's only one goal in mind here - easy price comparisons for all the best phones, devices and tablets on contract in the UK. We're trying to shake up the deals comparison market by making things a bit more
				exciting for you (if that can ever be exciting...). Whether it's a new phone, a refurb or anything in between - start
				comparing now.</p>

			<p>From attractive deals like our SIM-Only short-term contract comparisons or just a shiny new smartphone
				like the <a href="/apple/iphone-xr-deals" title="Best iPhone Xr contracts 2019">iPhone Xr</a> for 24 months - make sure you remember to use oneCompare as your first
				port-of-call. We might not have the big name cachet that others do yet, but we're determined to make
				shopping for contracts and phones a fun experience!</p>

			<h3 class="mt-3 text-dark h4 font-weight-light ">A comparison tool - you actually need. Over 100 filter combinations.</h3>
			<p>Isn't it frustrating being restricted by specific filters - what, for example, do you do if you're
				looking for a <a href="/samsung/galaxy-s10e-deals">Samsung Galaxy S10e</a> on contract but want it only on EE and don't want to spend more than
				&pound;40 a month? <br>
				<br>
				Most other websites will not allow you to filter down your selection to this level, but oneCompare is
				different. Drag our famous sliders from left to right, to create your perfect deal - there are over 10,000 possible combinations of different deals.</p>

			</div>

			<div class="col-md-6 col-12">


			<h3 class="mt-3 text-dark h4 font-weight-light ">How does oneCompare make money?</h3>
			<p>If you buy a phone or contract through our website, oneCompare may receive a small fee/commission for
				referring you to that deal. However this does not affect the placement or promotion of any deals. We do
				not push deals where we might get paid more to the top of the list - unlike other competitors. Only the
				best deals suitable for your filters and search terms are shown.</p>
			<p>Occasionally we may howevever show suitable 'exclusive' deals that we have carefully negotiated with the
				network or store. We will highlight these with a blue 'promoted' badge, but their placement within
				search results will not be affected.</p>


			<h3 class="mt-3 text-dark h4 font-weight-light ">How do you decide on the sorting order of deals in your comparison tables?</h3>
			<p>Quite simply - by using the filters and search terms you have chosen! You can filter by a plethora of
				options - by monthly cost, by amount of data (GB), by the length of the contract (eg 6 months only). All
				of these and many more filters make it easy for you to find the sweetest deal.</p>

			<h3 class="mt-3 text-dark h4 font-weight-light ">Why do you not compare some networks?</h3>
			<p>We try to compare as many as possible - and we're adding more all the time. If you have a network that
				you do not see listed please contact us and we will try to add it. </p>

			</div>

			</div>

		</div>
	</div>


	<section class="main bg-millennial-pink-2 pt-5 pb-5  mb-5 text-dark"
			 >
		<div class="container">
			<div class="section-header text-center pt-5 pb-5">
				<h2 class="star-title">Why <span>oneCompare?</span></h2>
				<p>You won't find anything that comes close, that's a promise</p>
			</div>
			<div class="row feature-grid-large-row">


				<div class="col-sm-6 feature-grid-large-first">
					<div class="row">
						<div class="col-sm-2 order-sm-10">
							<i class="fa-2x fal fa-tasks icon-red"></i></div>
						<div class="col-sm-10 order-sm-2">
							<h3>We always want more</h3>
							<p>Unlike other websites, we compare prices and deals from all the UK networks - Three, ee, o2,
								Vodafone, Plusnet Mobile, Virgin Media, Tesco Mobile, ID - and now even Sky Mobile.</p>
						</div>
					</div>
				</div>


				<div class="col-sm-6 feature-grid-large-second">
					<div class="row">
						<div class="col-sm-2">
							<i class="fal fa-2x fa-hand-spock icon-red"></i>
						</div>
						<div class="col-sm-10">
							<h3>Over 50,000 live deals</h3>
							<p>There's no need to check any other website if you're buying a smartphone - because, quite
								simply, we compare more prices than anyone else. We've got 560,238 mobile
								contracts &amp; combinations.</p>
						</div>
					</div>
				</div>


			</div>
			<div class="feature-grid-large-divider"></div>
			<div class="row feature-grid-large-row">
				<div class="col-sm-6 feature-grid-large-first">
					<div class="row">
						<div class="col-sm-2 order-sm-10">
							<i class="fal fa-2x fa-sim-card icon-red"></i></div>
						<div class="col-sm-10 order-sm-2">
							<h3>As free as a SIM</h3>
							<p>SIM only, SIM Free - what ever way you buy your next smartphone, it always pays to compare
								deals first. And with our great SIM-only calculator, you can find out instantly which one
								will be cheaper.</p>
						</div>
					</div>
				</div>


				<div class="col-sm-6 feature-grid-large-second">
					<div class="row">
						<div class="col-sm-2">
							<i class="fal fa-2x fa-sort-numeric-up icon-red"></i>
						</div>
						<div class="col-sm-10">
							<h3>What's best for me?</h3>
							<p>Unsure if you should buy the phone outright and go SIM-Only? It isn't always cheaper - even
								if you can afford to do so. But fret no more, friendo. Our free &amp; easy calculator knows
								the answer. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('footer-posts')
    @foreach($taggedPosts as $post)
        @include('frontend.components.footer.post', [ 'post' => $post ])
    @endforeach
@endsection
