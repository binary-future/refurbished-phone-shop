@extends('frontend.layouts.app')

@section('title', 'Expired page')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <br>
                <br>
                <h2 class="text-center">The page has expired due to inactivity. Please refresh and try again.</h2>
                <br>
                <br>
            </div>
        </div>
    </div>
@endsection