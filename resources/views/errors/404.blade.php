@extends('frontend.layouts.app')

@section('title', 'Page not found')
@section('canonical', 'hide')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5 mt-3 text-center">
                <br/>
                <br/>
                <br/>
                <h2 class="h4"><i class="fal fa-4x text-green fa-exclamation-circle"></i><br/>
                    <br/>
                    That page you’re looking for can’t be found. Sorry!</h2>
                <p>Please go back to the homepage or search for your deals above.</p>
            </div>
        </div>
    </div>
@endsection
