#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

SHELL = /bin/sh

APP_CONTAINER_NAME := app
WEB_CONTAINER_NAME := web
REDIS_CONTAINER_NAME := redis
REDIS_UI_CONTAINER_NAME := redis-webui
MYSQL_CONTAINER_NAME := mysql
MYSQL_UI_CONTAINER_NAME := phpmyadmin

USER_ID := $(shell id -u)
GROUP_ID := $(shell id -g)

TTY := -T
CONTAINER_USER := www-data
GIT_DIFF_OPTIONS := --cached

root_dir := ${PWD}
deploy_docker_path := .deploy/docker
deploy_data_path := ${deploy_docker_path}/data


docker_bin := $(shell command -v docker 2> /dev/null)
docker_compose_bin := $(shell command -v docker-compose 2> /dev/null)
docker_compose_configured := $(docker_compose_bin) -f $(root_dir)/$(deploy_docker_path)/docker-compose.yml --project-directory $(root_dir)/$(deploy_docker_path)
docker_compose_exec_as_user := $(docker_compose_configured) exec -u ${CONTAINER_USER}

changed_php_files := git --no-pager diff ${GIT_DIFF_OPTIONS} --name-only --diff-filter=MARC | grep '\.php\{0,1\}$\'
changed_php_files_count := $$($(changed_php_files) | wc -l)

.PHONY : help artisan composer shell \
		test lint lintfix \
		install init build watch \
		dc
.DEFAULT_GOAL := help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@echo "\n\
	  Usage example:\n\
	    make composer P=\"install --no-dev\""


# --- [ Development tasks ] -------------------------------------------------------------------------------------------

artisan: ## Start artisan into application container
	$(docker_compose_exec_as_user) "$(APP_CONTAINER_NAME)" php -d memory_limit=256M artisan $(P)

composer: ## Start composer into application container
	$(docker_compose_exec_as_user) $(TTY) "$(APP_CONTAINER_NAME)" composer $(P)

shell: ## Start shell into application container
	$(docker_compose_configured) exec "$(APP_CONTAINER_NAME)" $(SHELL)

dc: ## Work with docker-compose
	$(docker_compose_configured) $(P)

install: ## Install application dependencies into application container
	$(MAKE) composer P="install --no-interaction --ansi --no-suggest"
	npm install
	# $(docker_compose_bin) run --rm "$(NODE_CONTAINER_NAME)" npm install

watch: ## Start watching assets for changes (node)
	npm run watch
	# $(docker_compose_bin) run --rm "$(NODE_CONTAINER_NAME)" npm run watch

init: ## Make full application initialization (install, seed, build assets, etc)
	$(MAKE) composer P="install"
	$(MAKE) artisan P="migrate --force --no-interaction -vvv"
	$(MAKE) artisan P="db:seed --class=DatafeedApiSeeder"
	$(MAKE) artisan P="db:seed --class=RoleSeeder"
	$(MAKE) artisan P="db:seed --class=UserSeeder"
	$(MAKE) artisan P="store:import"

lintfix: ## Execute application fixing of lint errors
	if [ ${changed_php_files_count} != 0 ]; then \
		$(docker_compose_exec_as_user) ${TTY} "${APP_CONTAINER_NAME}" vendor/bin/phpcbf --standard="./phpcs.xml" $$(${changed_php_files}); \
	fi

lint: ## Execute application linting
	if [ ${changed_php_files_count} != 0 ]; then \
		# Syntax linting \
		$(docker_compose_exec_as_user) ${TTY} "${APP_CONTAINER_NAME}" vendor/bin/parallel-lint -e php --exclude vendor --colors $$(${changed_php_files}); \
		# Code style linting \
		$(docker_compose_exec_as_user) ${TTY} "${APP_CONTAINER_NAME}" vendor/bin/phpcs --standard="./phpcs.xml" $$(${changed_php_files}); \
		# PHPStan analytics \
		$(docker_compose_configured) exec ${TTY} "${APP_CONTAINER_NAME}" php -d memory_limit=1G vendor/bin/phpstan analyse --error-format=filtered -l 5 -c phpstan.neon $$(${changed_php_files}); \
	fi

test: ## Execute application tests
	$(docker_compose_configured) exec ${TTY} "${APP_CONTAINER_NAME}" php -d memory_limit=1G vendor/bin/phpunit $(P)

SONAR_HOST_URL := http://localhost:9001
SONAR_USER_HOME := /usr/.sonar
# Usage: make sonar-scanner SONAR_LOGIN=token_here

sonar-scanner: ## Execute sonar-scanner

	mkdir -pv "$(deploy_data_path)/sonar-scanner"
	chown ${USER_ID}:${GROUP_ID} ${deploy_data_path}/sonar-scanner

	docker run \
	-e SONAR_HOST_URL=${SONAR_HOST_URL} \
	-e SONAR_LOGIN=${SONAR_LOGIN} \
	-e SONAR_USER_HOME=${SONAR_USER_HOME} \
	--user="${USER_ID}:${GROUP_ID}" --network host -it \
	-v "${root_dir}:/usr/src" \
	-v "${root_dir}/$(deploy_docker_path)/config/sonar-scanner:/opt/sonar-scanner/conf" \
	-v "${root_dir}/$(deploy_data_path)/sonar-scanner:${SONAR_USER_HOME}" \
	sonarsource/sonar-scanner-cli


build: ## Prepare production build
	npm run prod
