<?php

namespace Blog\Domain\Repository;

use Illuminate\Support\Collection;

interface Posts
{
    public function findLastPosts(int $limit): Collection;

    public function findPostsByTags(array $tags, int $quantity = null): Collection;

    public function findPostsExceptTags(array $exceptTags, int $quantity = null): Collection;
}
