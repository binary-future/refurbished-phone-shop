<?php


namespace Blog\Domain;

use Corcel\Model\Meta\ThumbnailMeta;
use Corcel\Model\Post as WPPost;
use Corcel\Model\Taxonomy;
use Illuminate\Support\Collection;

/**
 * Class Post
 * @package App\Models\Blog
 */
class Post extends WPPost
{
    const MAX_SHORT_CONTENT_LENGTH = 200;
    const MAX_SHORT_TITLE_LENGTH = 60;

    /**
     * @var string
     */
    private $thumbPath;

    /**
     * Make short content for the post
     *
     * @return mixed|string
     */
    public function getShortContent()
    {
        $content = preg_replace('/<[^>]+>/', '', $this->content);

        return strlen($content) > self::MAX_SHORT_CONTENT_LENGTH
            ? sprintf('%s...', substr($content, 0, self::MAX_SHORT_CONTENT_LENGTH - 3))
            : $content;
    }

    /**
     * @return mixed|string
     */
    public function getShortTitle()
    {
        $postTitle = preg_replace('/<[^>]+>/', '', $this->post_title);

        return strlen($postTitle) > self::MAX_SHORT_TITLE_LENGTH
            ? sprintf('%s...', substr($postTitle, 0, self::MAX_SHORT_TITLE_LENGTH - 3))
            : $postTitle;
    }

    /**
     * @return mixed
     */
    public function getThumbnailPath()
    {
        $thumbnail = $this->thumbnail;
        if (! $thumbnail) {
            return null;
        }
        $thumbData = $thumbnail->size(ThumbnailMeta::SIZE_THUMBNAIL);

        return $thumbData['url'] ?? $thumbnail->__toString();
    }

    /**
     * @return string
     */
    public function getThumbPath(): ?string
    {
        return $this->thumbPath ?: $this->getThumbnailPath();
    }

    /**
     * @param string $thumbPath
     */
    public function changeThumbnailPath(string $thumbPath): void
    {
        $this->thumbPath = $thumbPath;
    }

    public function getTags(): Collection
    {
        $taxonomies = $this->taxonomies()->where('taxonomy', 'post_tag')->get();

        return $taxonomies->map(function (Taxonomy $taxonomy) {
            return $taxonomy->term ? $taxonomy->term->name : null;
        });
    }

    public function getPostThumbDate()
    {
        $thumbnail = $this->thumbnail;
        if (! $thumbnail) {
            return null;
        }

        return $thumbnail->attachment ? $thumbnail->attachment->post_date : null;
    }
}
