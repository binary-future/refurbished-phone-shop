<?php

namespace Blog\Utils\Repository;

use Blog\Domain\Post;
use Blog\Domain\Repository\Posts;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

final class PostRepository implements Posts
{
    public function findLastPosts(int $limit): Collection
    {
        return $this->query()->with('thumbnail', 'taxonomies', 'taxonomies.term')
            ->orderBy('post_date', 'desc')
            ->limit($limit)
            ->get();
    }

    public function findPostsByTags(array $tags, int $quantity = null): Collection
    {
        return $this->query()->with('thumbnail', 'taxonomies', 'taxonomies.term')
            ->whereHas('taxonomies', function (Builder $query) use ($tags) {
                $query->where('taxonomy', 'post_tag')
                    ->whereHas('term', function (Builder $query) use ($tags) {
                        $count = 0;
                        foreach ($tags as $tag) {
                            $count ? $query->orWhere('name', 'LIKE', '%' . $tag . '%')
                             : $query->where('name', 'LIKE', '%' . $tag . '%');
                            $count++;
                        }
                    });
            })
            ->orderBy('post_date', 'desc')
            ->limit($quantity)
            ->get();
    }

    /**
     * @param string[] $exceptTags
     * @param int|null $quantity
     * @return Collection
     */
    public function findPostsExceptTags(array $exceptTags, int $quantity = null): Collection
    {
        return $this->query()->with('thumbnail', 'taxonomies', 'taxonomies.term')
            ->whereDoesntHave('taxonomies', static function (Builder $query) use ($exceptTags) {
                $query->where('taxonomy', 'post_tag')
                    ->whereHas('term', static function (Builder $query) use ($exceptTags) {
                        foreach ($exceptTags as $i => $tag) {
                            $where = $i === 0 ? 'orWhere' : 'where';
                            $query->$where('name', 'LIKE', '%' . $tag . '%');
                        }
                    });
            })
            ->orderBy('post_date', 'desc')
            ->limit($quantity)
            ->get();
    }

    private function query(): Builder
    {
        return Post::where([
            ['post_type', '=', 'post'],
            ['post_status', '=', 'publish'],
        ]);
    }
}
