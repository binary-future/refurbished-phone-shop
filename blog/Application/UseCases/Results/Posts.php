<?php


namespace Blog\Application\UseCases\Results;


use Illuminate\Support\Collection;

/**
 * Class LastPosts
 * @package Blog\Application\UseCases\Results
 */
final class Posts
{
    /**
     * @var Collection
     */
    private $posts;

    /**
     * LastPosts constructor.
     * @param Collection $posts
     */
    public function __construct(Collection $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return Collection
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }
}
