<?php

namespace Blog\Application\UseCases;

use Blog\Application\UseCases\Contracts\LastPostCase;
use Blog\Application\UseCases\Results\Posts as Response;
use Blog\Domain\Repository\Posts;

/**
 * Class LastPostsCase
 * @package Blog\Application\UseCases
 */
final class LastPostsCase implements LastPostCase
{
    /**
     * @var Posts
     */
    private $posts;

    /**
     * LastPostsCase constructor.
     * @param Posts $posts
     */
    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param int $limit
     * @return Response
     */
    public function execute(int $limit): Response
    {
        return new Response($this->posts->findLastPosts($limit));
    }
}
