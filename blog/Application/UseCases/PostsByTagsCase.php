<?php

namespace Blog\Application\UseCases;

use Blog\Application\UseCases\Contracts\PostsByTagsCase as Contract;
use Blog\Application\UseCases\Results\Posts as Response;
use Blog\Domain\Repository\Posts;

final class PostsByTagsCase implements Contract
{
    /**
     * @var Posts
     */
    private $posts;

    /**
     * PostsByTagsCase constructor.
     * @param Posts $posts
     */
    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    public function execute(array $tags, int $quantity = null): Response
    {
        return new Response($this->posts->findPostsByTags($tags, $quantity));
    }
}
