<?php

namespace Blog\Application\UseCases\Contracts;

use Blog\Application\UseCases\Results\Posts;

interface LastPostsExceptTagsCase
{
    /**
     * @param string[] $exceptTags
     * @param int|null $quantity
     * @return Posts
     */
    public function execute(array $exceptTags, int $quantity = null): Posts;
}
