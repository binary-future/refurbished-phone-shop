<?php

namespace Blog\Application\UseCases\Contracts;

use Blog\Application\UseCases\Results\Posts;

/**
 * Interface LastPostCase
 * @package Blog\Application\UseCases\Contracts
 */
interface LastPostCase
{
    /**
     * @param int $limit
     * @return Posts
     */
    public function execute(int $limit): Posts;
}
