<?php


namespace Blog\Application\UseCases\Contracts;


use Blog\Application\UseCases\Results\Posts;

/**
 * Interface PostsByTagsCase
 * @package Blog\Application\UseCases\Contracts
 */
interface PostsByTagsCase
{
    /**
     * @param array $tags
     * @param int $quantity
     * @return mixed
     */
    public function execute(array $tags, int $quantity = null): Posts;
}
