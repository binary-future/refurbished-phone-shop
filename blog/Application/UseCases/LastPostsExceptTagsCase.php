<?php

namespace Blog\Application\UseCases;

use Blog\Application\UseCases\Results\Posts as Response;
use Blog\Domain\Repository\Posts;

final class LastPostsExceptTagsCase implements Contracts\LastPostsExceptTagsCase
{
    /**
     * @var Posts
     */
    private $posts;

    /**
     * PostsByTagsCase constructor.
     * @param Posts $posts
     */
    public function __construct(Posts $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param string[] $exceptTags
     * @param int|null $quantity
     * @return Response
     */
    public function execute(array $exceptTags, int $quantity = null): Response
    {
        return new Response($this->posts->findPostsExceptTags($exceptTags, $quantity));
    }
}
