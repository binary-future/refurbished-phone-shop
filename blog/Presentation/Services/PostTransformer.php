<?php


namespace Blog\Presentation\Services;


use Blog\Domain\Post;
use Illuminate\Support\Collection;

class PostTransformer
{
    /**
     * @var ThumbPathGenerator
     */
    private $generator;

    /**
     * PostTransformer constructor.
     * @param ThumbPathGenerator $generator
     */
    public function __construct(ThumbPathGenerator $generator)
    {
        $this->generator = $generator;
    }

    public function transform(Collection $posts): Collection
    {
        $result = [];
        foreach ($posts as $post) {
            if ($post instanceof Post) {
                $result[] = $this->transformSingle($post);
            }
        }
        return collect($result);
    }

    public function transformSingle(Post $post): array
    {

        return [
            'id' => $post->getKey(),
            'title' => $post->post_name,
            'short_title' => $post->getShortTitle(),
            'content' => $post->getShortContent(),
            'image' => $this->generator->generate($post),
            'tags' => $post->getTags(),
        ];
    }
}