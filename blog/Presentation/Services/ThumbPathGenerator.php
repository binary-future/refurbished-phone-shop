<?php

namespace Blog\Presentation\Services;

use Blog\Domain\Post;
use Blog\Utils\Adapters\Config\Contracts\Config;
use Carbon\Carbon;

class ThumbPathGenerator
{

    private const CONFIG_PATH = 'blog.thumbs.image_path';

    /**
     * @var string
     */
    private $mainContentPath;

    /**
     * ThumbPathGenerator constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->mainContentPath = $config->get(self::CONFIG_PATH);
    }

    /**
     * @param Post $post
     * @return string
     */
    public function generate(Post $post): ?string
    {
        $path = $post->getThumbnailPath();
        if (! $path || ! $this->mainContentPath) {
            return $path;
        }

        $dateDependentVolumes = $this->getDateDependentVolumes($post->getPostThumbDate());
        return $this->shouldGenerate($path, $dateDependentVolumes)
            ?  $this->generatePath($path, $dateDependentVolumes)
            : $path;
    }

    /**
     * @param Carbon $date
     * @return string
     */
    private function getDateDependentVolumes(Carbon $date): string
    {
        return sprintf('%s/%s', $date->year, $date->format('m'));
    }

    private function shouldGenerate(string $path, string $dateDependentVolumes): bool
    {
        return preg_match('/\/[0-9]{4}\/[0-9]{2}\//', $path) !== 1;
    }

    /**
     * @param string $path
     * @param $dateDependentPart
     * @return string
     */
    private function generatePath(string $path, $dateDependentPart)
    {
        $imageName = basename($path);

        return sprintf(
            '%s/%s/%s',
            $this->mainContentPath,
            $dateDependentPart,
            $imageName
        );
    }
}
