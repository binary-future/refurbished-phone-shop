<?php


namespace Blog\Presentation\Presenters\Contracts;


use Blog\Application\UseCases\Results\Posts;

/**
 * Interface LastPostPresenter
 * @package Blog\Presentation\Presenters\Contracts
 */
interface PostsPresenter
{
    /**
     * @param Posts $lastPosts
     * @return mixed
     */
    public function present(Posts $lastPosts);
}