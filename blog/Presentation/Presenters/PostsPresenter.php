<?php


namespace Blog\Presentation\Presenters;

use Blog\Application\UseCases\Results\Posts;
use Blog\Presentation\Presenters\Contracts\PostsPresenter as Contract;
use Blog\Presentation\Services\PostTransformer;

final class PostsPresenter implements Contract
{
    /**
     * @var PostTransformer
     */
    private $postsTransformer;

    /**
     * LastPostPresenter constructor.
     * @param PostTransformer $postsTransformer
     */
    public function __construct(PostTransformer $postsTransformer)
    {
        $this->postsTransformer = $postsTransformer;
    }

    public function present(Posts $lastPosts)
    {
        return $this->postsTransformer->transform($lastPosts->getPosts())->toJson();
    }
}