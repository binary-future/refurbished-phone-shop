<?php

namespace Blog\Presentation\Providers;

use Blog\Application\UseCases\Contracts\LastPostCase;
use Blog\Application\UseCases\Contracts\LastPostsExceptTagsCase;
use Blog\Application\UseCases\Contracts\PostsByTagsCase;
use Blog\Application\UseCases\LastPostsCase;
use Blog\Domain\Repository\Posts;
use Blog\Presentation\Presenters\Contracts\PostsPresenter;
use Blog\Utils\Adapters\Config\Contracts\Config;
use Blog\Utils\Repository\PostRepository;
use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(Posts::class, PostRepository::class);
        $this->app->bind(LastPostCase::class, LastPostsCase::class);
        $this->app->bind(PostsPresenter::class, \Blog\Presentation\Presenters\PostsPresenter::class);
        $this->app->bind(PostsByTagsCase::class, \Blog\Application\UseCases\PostsByTagsCase::class);
        $this->app->bind(LastPostsExceptTagsCase::class, \Blog\Application\UseCases\LastPostsExceptTagsCase::class);
        $this->app->bind(Config::class, \Blog\Utils\Adapters\Config\Config::class);
    }
}
