<?php


namespace Blog\Presentation\Actions;


use Blog\Application\UseCases\Contracts\LastPostCase;
use Blog\Presentation\Presenters\Contracts\PostsPresenter;

final class GetLastPosts
{
    /**
     * @var LastPostCase
     */
    private $case;

    /**
     * @var PostsPresenter
     */
    private $presenter;

    /**
     * GetLastPosts constructor.
     * @param LastPostCase $case
     * @param PostsPresenter $presenter
     */
    public function __construct(LastPostCase $case, PostsPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $quantity
     * @return string
     */
    public function __invoke(int $quantity)
    {
        $result = $this->case->execute($quantity);

        return $this->presenter->present($result);
    }
}
