<?php

namespace Blog\Presentation\Actions;

use Blog\Application\UseCases\Contracts\LastPostsExceptTagsCase;
use Blog\Presentation\Presenters\Contracts\PostsPresenter;

final class GetLastPostsExceptTags
{
    /**
     * @var LastPostsExceptTagsCase
     */
    private $case;

    /**
     * @var PostsPresenter
     */
    private $presenter;

    /**
     * GetLastPostsExceptTags constructor.
     * @param LastPostsExceptTagsCase $case
     * @param PostsPresenter $presenter
     */
    public function __construct(LastPostsExceptTagsCase $case, PostsPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string[] $exceptTags
     * @param int|null $quantity
     * @return mixed
     */
    public function __invoke(array $exceptTags, int $quantity = null)
    {
        return $this->presenter->present($this->case->execute($exceptTags, $quantity));
    }
}
