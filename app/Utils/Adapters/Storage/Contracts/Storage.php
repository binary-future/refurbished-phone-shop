<?php

namespace App\Utils\Adapters\Storage\Contracts;

interface Storage
{
    /**
     * @param string|null $name
     * @return Storage
     */
    public function disk(string $name = null): Storage;

    /**
     * @param string $path
     */
    public function delete($path);

    /**
     * @param string $oldPath
     * @param string $newPath
     * @return mixed
     */
    public function move(string $oldPath, string $newPath);

    /**
     * @param string $oldPath
     * @param string $newPath
     * @return bool
     */
    public function copy(string $oldPath, string $newPath);

    /**
     * @param string $path
     * @param $content
     * @return mixed
     */
    public function put(string $path, $content);

    /**
     * @param string $path
     * @return mixed
     */
    public function get(string $path);

    /**
     * @param string $path
     * @return mixed
     */
    public function exists(string $path);

    /**
     * @param string $path
     * @return array
     */
    public function files(string $path): array;

    /**
     * @param string $path
     * @return mixed
     */
    public function lastModified(string $path);
}
