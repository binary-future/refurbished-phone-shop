<?php

namespace App\Utils\Adapters\Storage;

use App\Utils\Adapters\Storage\Contracts\Storage as Contract;
use Illuminate\Support\Facades\Storage as BaseStorage;

/**
 * Class Storage
 * @package App\Helpers\File
 */
final class Storage implements Contract
{
    private $filesystem;

    /**
     * @param string|null $name
     * @return Storage
     */
    public function disk(string $name = null): Contract
    {
        $this->filesystem = BaseStorage::disk($name);

        return $this;
    }

    /**
     * @param string $path
     * @return
     */
    public function delete($path)
    {
        return BaseStorage::delete($path);
    }

    /**
     * @param string $oldPath
     * @param string $newPath
     * @return mixed
     */
    public function move(string $oldPath, string $newPath)
    {
        return BaseStorage::move($oldPath, $newPath);
    }

    /**
     * @param string $oldPath
     * @param string $newPath
     * @return bool
     */
    public function copy(string $oldPath, string $newPath)
    {
        return BaseStorage::copy($oldPath, $newPath);
    }

    /**
     * @param string $path
     * @param $content
     * @return mixed
     */
    public function put(string $path, $content)
    {
        return BaseStorage::put($path, $content);
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function get(string $path)
    {
        return BaseStorage::get($path);
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function exists(string $path)
    {
        return BaseStorage::exists($path);
    }

    public function files(string $path): array
    {
        return BaseStorage::files($path);
    }

    public function lastModified(string $path)
    {
        return BaseStorage::lastModified($path);
    }
}
