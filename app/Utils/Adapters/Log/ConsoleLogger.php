<?php

namespace App\Utils\Adapters\Log;

use App\Utils\Adapters\Log\Contracts\Logger;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class ConsoleLogger
 * @package App\Utils\Adapters\Log
 */
class ConsoleLogger implements Logger
{
    /**
     * @var ConsoleOutput
     */
    private $consoleOutput;

    /**
     * ConsoleLogger constructor.
     * @param ConsoleOutput $consoleOutput
     */
    public function __construct(ConsoleOutput $consoleOutput)
    {
        $this->consoleOutput = $consoleOutput;
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function info(string $message)
    {
        $this->consoleOutput->writeln(sprintf("<info>%s</info>", $message));
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function line(string $message)
    {
        $this->consoleOutput->writeln(sprintf("%s", $message));
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function error(string $message)
    {
        $this->consoleOutput->writeln(sprintf("<error>%s</error>", $message));
    }
}
