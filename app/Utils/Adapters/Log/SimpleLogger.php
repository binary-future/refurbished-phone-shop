<?php

namespace App\Utils\Adapters\Log;

use App\Utils\Adapters\Log\Contracts\Logger;

/**
 * Class SimpleLogger
 * @package App\Utils\Adapters\Log
 */
final class SimpleLogger implements Logger
{
    /**
     * @param string $message
     * @return mixed|void
     */
    public function info(string $message)
    {
        $this->display($message);
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function line(string $message)
    {
        $this->display($message);
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function error(string $message)
    {
        $this->display($message);
    }

    /**
     * @param string $message
     */
    private function display(string $message)
    {
         echo($message . PHP_EOL);
    }
}
