<?php

namespace App\Utils\Adapters\Log;

use App\Utils\Adapters\Log\Contracts\ChannelLogger as Contract;
use Psr\Log\LoggerInterface;

/**
 * Class ChannelLogger
 * @package App\Utils\Adapters\Log
 */
class ChannelLogger implements Contract
{
    /**
     * @var LoggerInterface
     */
    private $logManager;

    private $channel = self::CHANNEL_DAILY;

    /**
     * ConsoleLogger constructor.
     * @param LoggerInterface $logManager
     */
    public function __construct(LoggerInterface $logManager)
    {
        $this->logManager = $logManager;
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function info($message)
    {
        $this->logManager
            ->channel($this->channel)
            ->info($message);
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function line($message)
    {
        $this->info($message);
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function error($message)
    {
        $this->logManager
            ->channel($this->channel)
            ->error($message);
    }

    /**
     * @param string $channel
     * @return Contract
     */
    public function channel(string $channel): Contract
    {
        $this->channel = $channel;
        return $this;
    }
}
