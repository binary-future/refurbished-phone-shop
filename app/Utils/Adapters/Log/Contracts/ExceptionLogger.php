<?php

namespace App\Utils\Adapters\Log\Contracts;

interface ExceptionLogger
{
    /**
     * @param \Throwable $exception
     * @return string|null
     */
    public function captureException(\Throwable $exception): ?string;
}
