<?php

namespace App\Utils\Adapters\Log\Contracts;

/**
 * Interface ChannelLogger
 * @package App\Utils\Adapters\Log\Contracts
 */
interface ChannelLogger
{
    public const CHANNEL_STACK = 'stack';
    public const CHANNEL_DAILY = 'daily';
    public const CHANNEL_SINGLE = 'single';
    public const CHANNEL_DEALS_IMPORT = 'deals-import';
    public const CHANNEL_DEALS_DELETE = 'deals-delete';
    public const CHANNEL_DEALS_DELETE_DAILY = 'deals-delete-daily';
    public const CHANNEL_DUPLICATION_IMAGES = 'duplication-images';

    /**
     * @param string $channel
     * @return ChannelLogger
     */
    public function channel(string $channel): ChannelLogger;

    /**
     * @param $message
     * @return mixed
     */
    public function info($message);

    /**
     * @param $message
     * @return mixed
     */
    public function line($message);

    /**
     * @param $message
     * @return mixed
     */
    public function error($message);
}
