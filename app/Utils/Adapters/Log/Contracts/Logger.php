<?php

namespace App\Utils\Adapters\Log\Contracts;

/**
 * Interface Logger
 * @package App\Utils\Adapters\Log\Contracts
 */
interface Logger
{
    /**
     * @param string $message
     * @return mixed
     */
    public function info(string $message);

    /**
     * @param string $message
     * @return mixed
     */
    public function line(string $message);

    /**
     * @param string $message
     * @return mixed
     */
    public function error(string $message);
}
