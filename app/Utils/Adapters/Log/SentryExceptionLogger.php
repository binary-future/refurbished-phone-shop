<?php

namespace App\Utils\Adapters\Log;

use App\Utils\Adapters\Log\Contracts\ExceptionLogger;
use Sentry\Laravel\Facade as Sentry;

class SentryExceptionLogger implements ExceptionLogger
{
    /**
     * @param \Throwable $exception
     * @return string|null
     */
    public function captureException(\Throwable $exception): ?string
    {
        return Sentry::captureException($exception);
    }
}
