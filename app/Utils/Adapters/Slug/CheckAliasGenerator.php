<?php

namespace App\Utils\Adapters\Slug;

use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator as Contract;
use App\Utils\Adapters\Slug\Contracts\Generator;

/**
 * Class CheckAliasGenerator
 * @package App\Utils\Adapters\Slug
 */
final class CheckAliasGenerator implements Contract
{
    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * CheckAliasGenerator constructor.
     * @param Generator $slugGenerator
     */
    public function __construct(Generator $slugGenerator)
    {
        $this->slugGenerator = $slugGenerator;
    }

    /**
     * @param string $value
     * @return string
     */
    public function generate(string $value): string
    {
        $slug = $this->getSlug($value);

        return $this->prepareAlias($slug);
    }

    /**
     * @param string $value
     * @return string
     */
    private function getSlug(string $value)
    {
        return $this->slugGenerator->generate($value, ' ');
    }

    /**
     * @param string $slug
     * @return string
     */
    private function prepareAlias(string $slug): string
    {
        $slugParts = explode(' ', $slug);
        natsort($slugParts);

        return implode(' ', array_unique($slugParts));
    }

    public function generateHash(string $value): string
    {
        return hash('md5', $this->generate($value));
    }
}
