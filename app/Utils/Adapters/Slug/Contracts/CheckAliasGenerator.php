<?php

namespace App\Utils\Adapters\Slug\Contracts;

/**
 * Interface CheckAliasGenerator
 * @package App\Utils\Adapters\Slug\Contracts
 */
interface CheckAliasGenerator
{
    /**
     * @param string $value
     * @return string
     */
    public function generate(string $value): string;

    /**
     * @param string $value
     * @return string
     */
    public function generateHash(string $value): string;
}
