<?php

namespace App\Utils\Adapters\Slug\Contracts;

/**
 * Interface Generator
 * @package App\Utils\Adapter\Slug\Contracts
 */
interface Generator
{
    /**
     * @param string $title
     * @param string $separator
     * @param string $language
     * @return string
     */
    public function generate(string $title, string $separator = '-', string $language = 'en'): string;
}
