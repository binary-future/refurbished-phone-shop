<?php

namespace App\Utils\Adapters\Slug;

use App\Utils\Adapters\Slug\Contracts\Generator as Contract;

/**
 * Class Generator
 * @package App\Utils\Adapter\Slug
 */
final class Generator implements Contract
{
    /**
     * @param string $title
     * @param string $separator
     * @param string $language
     * @return string
     */
    public function generate(string $title, string $separator = '-', string $language = 'en'): string
    {
        return str_slug($title, $separator, $language);
    }
}
