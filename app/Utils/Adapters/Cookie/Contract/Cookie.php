<?php

namespace App\Utils\Adapters\Cookie\Contract;

/**
 * Interface Cookie
 * @package App\Utils\Adapters\Cookie\Contract
 */
interface Cookie
{
    public const DEFAULT_HOURS = 43200;

    /**
     * @param string $name
     * @param $value
     * @param int $time
     * @return mixed
     */
    public function queue(string $name, $value, int $time = self::DEFAULT_HOURS);
}
