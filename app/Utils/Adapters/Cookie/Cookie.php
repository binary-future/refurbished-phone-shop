<?php


namespace App\Utils\Adapters\Cookie;

use Illuminate\Support\Facades\Cookie as Facade;
use App\Utils\Adapters\Cookie\Contract\Cookie as Contract;

final class Cookie implements Contract
{
    /**
     * Put cookie into queue
     *
     * @param string $name
     * @param string $value
     * @param int $time
     * @return mixed
     */
    public function queue(string $name, $value, int $time = self::DEFAULT_HOURS)
    {
        Facade::queue($name, $value, $time);
    }
}
