<?php


namespace App\Utils\Adapters\JobDispatcher;


final class PendingDispatch
{
    /**
     * @var \Illuminate\Foundation\Bus\PendingDispatch
     */
    private $pendingDispatch;

    public function __construct(\Illuminate\Foundation\Bus\PendingDispatch $pendingDispatch)
    {
        $this->pendingDispatch = $pendingDispatch;
    }

    public function onQueue(string $queue): PendingDispatch
    {
        $this->pendingDispatch->onQueue($queue);

        return $this;
    }
}
