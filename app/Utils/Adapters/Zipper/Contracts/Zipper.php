<?php

namespace App\Utils\Adapters\Zipper\Contracts;

interface Zipper
{
    /**
     * @param string $filePath
     * @param string $extractTo
     * @return mixed
     */
    public function extract(string $filePath, string $extractTo);

    /**
     * @param string $filePath
     * @return mixed
     */
    public function getNames(string $filePath);

    /**
     * @param string $filePath
     * @param string $extractTo
     * @param array $names
     * @return mixed
     */
    public function extractByName(string $filePath, string $extractTo, array $names);
}