<?php

namespace App\Utils\Adapters\Zipper;

use Chumper\Zipper\Facades\Zipper as Facade;
use Chumper\Zipper\Zipper as BaseZipper;
use App\Utils\Adapters\Zipper\Contracts\Zipper as Contract;

/**
 * Class Zipper
 * @package App\Helpers\Zipper
 */
class Zipper implements Contract
{
    /**
     * @param string $filePath
     * @param string $extractTo
     * @return mixed
     */
    public function extract(string $filePath, string $extractTo)
    {
        return Facade::make($filePath)->extractTo($extractTo);
    }

    /**
     * @param string $filePath
     * @return mixed
     */
    public function getNames(string $filePath)
    {
        return Facade::make($filePath)->listFiles();
    }

    /**
     * @param string $filePath
     * @param string $extractTo
     * @param array $names
     * @return mixed
     */
    public function extractByName(string $filePath, string $extractTo, array $names)
    {
        return Facade::make($filePath)->extractTo($extractTo, $names, BaseZipper::WHITELIST | BaseZipper::EXACT_MATCH);
    }
}
