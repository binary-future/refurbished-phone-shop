<?php


namespace App\Utils\Adapters\Filesystem;

use App\Utils\Adapters\Filesystem\Contracts\Filesystem as Contract;
use Illuminate\Filesystem\Filesystem as TargetFilesystem;

/**
 * Class Filesystem
 * @package App\Utils\Adapters\Filesystem
 */
final class Filesystem implements Contract
{
    /**
     * @var TargetFilesystem
     */
    private $filesystem;

    /**
     * Filesystem constructor.
     * @param TargetFilesystem $filesystem
     */
    public function __construct(TargetFilesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @param array $paths
     * @return bool
     */
    public function delete(array $paths): bool
    {
        return $this->filesystem->delete($paths);
    }
}
