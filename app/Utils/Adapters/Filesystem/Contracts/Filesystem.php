<?php


namespace App\Utils\Adapters\Filesystem\Contracts;


/**
 * Interface Filesystem
 * @package App\Utils\Adapters\Filesystem\Contracts
 */
interface Filesystem
{
    /**
     * @param array $paths
     * @return bool
     */
    public function delete(array $paths): bool;
}
