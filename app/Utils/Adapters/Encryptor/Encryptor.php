<?php

namespace App\Utils\Adapters\Encryptor;

use App\Utils\Adapters\Encryptor\Contract\Encryptor as Contract;
use Illuminate\Support\Facades\Hash;

/**
 * Class Encryptor
 * @package App\Utils\Adapters\Encryptor
 */
final class Encryptor implements Contract
{
    /**
     * @param string $string
     * @return string
     */
    public function encrypt(string $string): string
    {
        return bcrypt($string);
    }

    /**
     * @param string $string
     * @param string $hash
     * @return bool
     */
    public function checkHash(string $string, string $hash): bool
    {
        return Hash::check($string, $hash);
    }
}
