<?php

namespace App\Utils\Adapters\Encryptor\Contract;

/**
 * Interface Encryptor
 * @package App\Utils\Adapters\Encryptor\Contract
 */
interface Encryptor
{
    /**
     * @param string $string
     * @return string
     */
    public function encrypt(string $string): string;

    /**
     * @param string $string
     * @param string $hash
     * @return bool
     */
    public function checkHash(string $string, string $hash): bool;
}