<?php


namespace App\Utils\Adapters\Cache;


use App\Utils\Adapters\Cache\Contracts\Cache as Contract;
use App\Utils\Adapters\Cache\Exceptions\ConnectionException;
use Illuminate\Support\Facades\Cache;

class CacheLaravel implements Contract
{
    /**
     * @param string $key
     * @param $value
     * @param $minutes \DateTimeInterface|\DateInterval|float|int
     * @return bool
     */
    public function add(string $key, $value, $minutes): bool
    {
        try {
            return Cache::add($key, $value, $minutes);
        } catch (\Throwable $exception) {
            throw ConnectionException::invalidConnection($exception);
        }
    }

    /**
     * @param string $key
     * @param $value
     * @param $minutes \DateTimeInterface|\DateInterval|float|int
     * @return void
     */
    public function put(string $key, $value, $minutes): void
    {
        try {
            Cache::put($key, $value, $minutes);
        } catch (\Throwable $exception) {
            throw ConnectionException::invalidConnection($exception);
        }
    }

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, $default = null)
    {
        try {
            return Cache::get($key, $default);
        } catch (\Throwable $exception) {
            throw ConnectionException::invalidConnection($exception);
        }
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function has(string $key)
    {
        try {
            return Cache::has($key);
        } catch (\Throwable $exception) {
            throw ConnectionException::invalidConnection($exception);
        }
    }

    /**
     * @param string $key
     * @param $value
     */
    public function forever(string $key, $value): void
    {
        try {
            Cache::forever($key, $value);
        } catch (\Throwable $exception) {
            throw ConnectionException::invalidConnection($exception);
        }
    }
}
