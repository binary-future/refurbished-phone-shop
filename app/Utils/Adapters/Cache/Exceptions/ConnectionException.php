<?php


namespace App\Utils\Adapters\Cache\Exceptions;


class ConnectionException extends \RuntimeException
{
    public static function invalidConnection(\Throwable $exception)
    {
        return new self('Connection invalid', $exception->getCode(), $exception);
    }
}
