<?php

namespace App\Utils\Adapters\Cache\Contracts;

interface Cache
{
    /**
     * @param string $key
     * @param $value
     * @param $minutes \DateTimeInterface|\DateInterval|float|int
     * @return bool
     */
    public function add(string $key, $value, $minutes): bool;

    /**
     * @param string $key
     * @param $value
     * @param $minutes \DateTimeInterface|\DateInterval|float|int
     * @return void
     */
    public function put(string $key, $value, $minutes): void;

    /**
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get(string $key, $default = null);

    /**
     * @param string $key
     * @return mixed
     */
    public function has(string $key);

    /**
     * @param string $key
     * @param $value
     */
    public function forever(string $key, $value): void;
}
