<?php

namespace App\Utils\Adapters\UrlGenerator;

class LaravelUrlGenerator implements \App\Utils\Adapters\UrlGenerator\Contracts\UrlGenerator
{
    /**
     * @var \Illuminate\Contracts\Routing\UrlGenerator
     */
    private $urlGenerator;

    /**
     * LaravelUrlGenerator constructor.
     * @param \Illuminate\Contracts\Routing\UrlGenerator $urlGenerator
     */
    public function __construct(\Illuminate\Contracts\Routing\UrlGenerator $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param string $name
     * @param array $parameters
     * @param bool $absolute
     * @return string
     *
     * @throws \InvalidArgumentException
     */
    public function route(string $name, array $parameters = [], bool $absolute = true): string
    {
        return $this->urlGenerator->route($name, $parameters, $absolute);
    }
}
