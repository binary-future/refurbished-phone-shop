<?php

namespace App\Utils\Adapters\UrlGenerator\Contracts;

interface UrlGenerator
{
    /**
     * @param string $name
     * @param array $parameters
     * @param bool $absolute
     * @return string
     */
    public function route(string $name, array $parameters = [], bool $absolute = true): string;
}
