<?php


namespace App\Utils\Adapters\Blog\Exceptions;


/**
 * Class BlogRequestException
 * @package App\Utils\Adapters\Blog\Exceptions
 */
class BlogRequestException extends \Exception
{
    /**
     * @param object $request
     * @param string $message
     * @return BlogRequestException
     */
    public static function cannotMakeRequest(object $request, string $message)
    {
        return new self(sprintf(
            'Cannot make request (%s). %s',
            get_class($request),
            $message
        ));
    }

    /**
     * @param string $message
     * @return BlogRequestException
     */
    public static function cannotHandleResponse(string $message)
    {
        return new self(
            sprintf('Cannot manage response. %s', $message)
        );
    }
}
