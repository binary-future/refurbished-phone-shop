<?php

namespace App\Utils\Adapters\Blog\Translators;

use App\Utils\Adapters\Blog\Post;

/**
 * Class PostsTranslator
 * @package App\Utils\Adapters\Blog\Translators
 */
class PostsTranslator
{
    /**
     * @param array[] $params
     * @return Post[]
     */
    public function translate(array $params): array
    {
        return array_map(static function (array $postData) {
            return new Post(
                $postData['id'],
                $postData['title'],
                $postData['content'],
                $postData['short_title'],
                $postData['image'] ?? null,
                $postData['tags'] ?? []
            );
        }, $params);
    }
}
