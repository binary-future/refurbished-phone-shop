<?php

namespace App\Utils\Adapters\Blog\Contracts;

use Illuminate\Support\Collection;

interface PostService
{
    public function lastPosts(int $quantity): Collection;

    public function byTags(array $tags, int $quantity = null): Collection;

    public function lastPostsExceptTags(array $exceptTags, int $quantity = null): Collection;
}
