<?php

namespace App\Utils\Adapters\Blog;

use App\Utils\Adapters\Blog\Contracts\PostService as Contract;
use App\Utils\Adapters\Blog\Exceptions\BlogRequestException;
use App\Utils\Adapters\Blog\Request\RequestHandler;
use App\Utils\Adapters\Blog\Request\Requests\ByTags;
use App\Utils\Adapters\Blog\Request\Requests\LastPosts;
use App\Utils\Adapters\Blog\Request\Requests\LastPostsExceptTags;
use App\Utils\Adapters\Blog\Translators\PostsTranslator;
use Illuminate\Support\Collection;

/**
 * Class PostService
 * @package App\Utils\Adapters\Blog
 */
final class PostService implements Contract
{
    /**
     * @var PostsTranslator
     */
    private $translator;

    /**
     * @var RequestHandler
     */
    private $handler;

    /**
     * PostService constructor.
     * @param PostsTranslator $translator
     * @param RequestHandler $requestHandler
     */
    public function __construct(PostsTranslator $translator, RequestHandler $requestHandler)
    {
        $this->translator = $translator;
        $this->handler = $requestHandler;
    }

    /**
     * @param int $quantity
     * @return Collection
     * @throws Exceptions\BlogRequestException
     */
    public function lastPosts(int $quantity): Collection
    {
        $response = $this->handler->handle(new LastPosts($quantity));

        return $this->generateResponse($response);
    }

    /**
     * @param array $tags
     * @param int|null $quantity
     * @return Collection
     * @throws BlogRequestException
     */
    public function byTags(array $tags, int $quantity = null): Collection
    {
        $response = $this->handler->handle(new ByTags($tags, $quantity));

        return $this->generateResponse($response);
    }

    /**
     * @param string[] $exceptTags
     * @param int|null $quantity
     * @return Collection
     * @throws BlogRequestException
     */
    public function lastPostsExceptTags(array $exceptTags, int $quantity = null): Collection
    {
        $response = $this->handler->handle(new LastPostsExceptTags($exceptTags, $quantity));

        return $this->generateResponse($response);
    }

    /**
     * @param array $posts
     * @return Collection
     * @throws BlogRequestException
     */
    private function generateResponse(array $posts): Collection
    {
        try {
            return collect($this->translator->translate($posts));
        } catch (\Throwable $exception) {
            throw BlogRequestException::cannotHandleResponse($exception->getMessage());
        }
    }
}
