<?php


namespace App\Utils\Adapters\Blog;


final class Post
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $shortTitle;

    /**
     * @var string|null
     */
    private $image;

    /**
     * @var array
     */
    private $tags = [];

    /**
     * Post constructor.
     * @param int $id
     * @param string $title
     * @param string $content
     * @param string $shortTitle
     * @param string|null $image
     * @param array $tags
     */
    public function __construct(
        int $id,
        string $title,
        string $content,
        string $shortTitle,
        ?string $image,
        array $tags
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->shortTitle = $shortTitle;
        $this->image = $image;
        $this->tags = $tags;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getShortTitle(): string
    {
        return $this->shortTitle;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }
}
