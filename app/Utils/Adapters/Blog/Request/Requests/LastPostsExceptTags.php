<?php

namespace App\Utils\Adapters\Blog\Request\Requests;

/**
 * Class LastPostsExceptTags
 * @package App\Utils\Adapters\Blog\Request\Requests
 */
final class LastPostsExceptTags implements Request
{
    /**
     * @var string[]
     */
    private $tags;

    /**
     * @var int|null
     */
    private $quantity;

    /**
     * LastPostsExceptTags constructor.
     * @param string[] $tags
     * @param int|null $quantity
     */
    public function __construct(array $tags, ?int $quantity)
    {
        $this->tags = $tags;
        $this->quantity = $quantity;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return array_filter([$this->tags, $this->quantity]);
    }
}
