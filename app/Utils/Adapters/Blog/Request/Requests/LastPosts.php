<?php


namespace App\Utils\Adapters\Blog\Request\Requests;

/**
 * Class LastPosts
 * @package App\Utils\Adapters\Blog\Request\Requests
 */
final class LastPosts implements Request
{
    /**
     * @var int
     */
    private $quantity;

    /**
     * LastPosts constructor.
     * @param int $quantity
     */
    public function __construct(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            $this->quantity
        ];
    }
}

