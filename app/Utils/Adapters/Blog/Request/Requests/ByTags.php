<?php

namespace App\Utils\Adapters\Blog\Request\Requests;

/**
 * Class ByTags
 * @package App\Utils\Adapters\Blog\Request\Requests
 */
final class ByTags implements Request
{
    /**
     * @var array
     */
    private $tags;

    /**
     * @var int|null
     */
    private $quantity;

    /**
     * ByTags constructor.
     * @param array $tags
     * @param int|null $quantity
     */
    public function __construct(array $tags, ?int $quantity)
    {
        $this->tags = $tags;
        $this->quantity = $quantity;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return array_filter([$this->tags, $this->quantity]);
    }
}
