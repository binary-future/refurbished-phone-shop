<?php

namespace App\Utils\Adapters\Blog\Request\Requests;

/**
 * Interface Request
 * @package App\Utils\Adapters\Blog\Request\Requests
 */
interface Request
{
    /**
     * @return array
     */
    public function getParams(): array;
}
