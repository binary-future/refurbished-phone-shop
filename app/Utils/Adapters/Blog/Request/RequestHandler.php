<?php

namespace App\Utils\Adapters\Blog\Request;

use App\Utils\Adapters\Blog\Exceptions\BlogRequestException;
use App\Utils\Adapters\Blog\Request\Requests\ByTags;
use App\Utils\Adapters\Blog\Request\Requests\LastPosts;
use App\Utils\Adapters\Blog\Request\Requests\LastPostsExceptTags;
use App\Utils\Adapters\Blog\Request\Requests\Request;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Encoders\Contracts\JsonEncoder;
use Blog\Presentation\Actions\GetLastPosts;
use Blog\Presentation\Actions\GetLastPostsExceptTags;
use Blog\Presentation\Actions\GetPostsByTags;

/**
 * Class RequestHandler
 * @package App\Utils\Adapters\Blog\Request
 */
class RequestHandler
{
    /**
     * @var JsonEncoder
     */
    private $jsonEncoder;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $schema = [
        LastPosts::class => GetLastPosts::class,
        ByTags::class => GetPostsByTags::class,
        LastPostsExceptTags::class => GetLastPostsExceptTags::class,
    ];

    /**
     * RequestHandler constructor.
     * @param JsonEncoder $jsonEncoder
     * @param Container $container
     */
    public function __construct(JsonEncoder $jsonEncoder, Container $container)
    {
        $this->jsonEncoder = $jsonEncoder;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @return array
     * @throws BlogRequestException
     */
    public function handle(Request $request): array
    {
        try {
            return $this->proceed($request);
        } catch (\Throwable $exception) {
            throw BlogRequestException::cannotMakeRequest($request, $exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    private function proceed(Request $request): array
    {
        $action = $this->container->resolve($this->schema[get_class($request)]);
        $response = $action(...$request->getParams());

        return $this->jsonEncoder->decode($response);
    }
}
