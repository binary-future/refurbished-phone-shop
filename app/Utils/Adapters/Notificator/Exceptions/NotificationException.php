<?php


namespace App\Utils\Adapters\Notificator\Exceptions;


use Illuminate\Notifications\Notification;

/**
 * Class NotificationException
 * @package App\Utils\Adapters\Notificator\Exceptions
 */
final class NotificationException extends \Exception
{
    /**
     * @param Notification $notification
     * @param string|null $error
     * @return NotificationException
     */
    public static function cannotSendNotification(Notification $notification, ?string $error)
    {
        return new self(trim(sprintf('Cannot send notification - %s. %s', get_class($notification), $error ?: '')));
    }
}
