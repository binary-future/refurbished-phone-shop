<?php


namespace App\Utils\Adapters\Notificator;

use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Notificator\Contracts\Notificator as Contract;
use App\Utils\Adapters\Notificator\Exceptions\NotificationException;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Notifications\Notification;

/**
 * Class Notificator
 * @package App\Utils\Adapters\Notificator
 */
final class Notificator implements Contract
{
    private const EMAIL_CONFIG_PATH = 'mail.target_email';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var AnonymousNotifiable
     */
    private $notificator;

    /**
     * @var string
     */
    private $email;

    /**
     * Notificator constructor.
     * @param Config $config
     * @param AnonymousNotifiable $notificator
     */
    public function __construct(Config $config, AnonymousNotifiable $notificator)
    {
        $this->config = $config;
        $this->notificator = $notificator;
        $this->configure();
    }

    /**
     *
     */
    private function configure()
    {
        $this->email = $this->config->get(self::EMAIL_CONFIG_PATH, null);
    }

    /**
     * @param Notification $notification
     * @param string|null $email
     * @return mixed|void
     * @throws NotificationException
     */
    public function notify(Notification $notification, string $email = null)
    {
        $email = $this->getEmail($notification, $email);
        $this->notificator->route('mail', $email)
            ->notify($notification);
    }

    /**
     * @param Notification $notification
     * @param string|null $email
     * @return string
     * @throws NotificationException
     */
    private function getEmail(Notification $notification, ?string $email): string
    {
        if (! $email && ! $this->email) {
            throw NotificationException::cannotSendNotification($notification, 'No email');
        }

        return $email ?: $this->email;
    }
}