<?php

namespace App\Utils\Adapters\Notificator\Contracts;

use Illuminate\Notifications\Notification;

/**
 * Interface Notificator
 * @package App\Utils\Adapters\Notificator\Contracts
 */
interface Notificator
{
    /**
     * @param Notification $notification
     * @param string|null $email
     * @return mixed
     */
    public function notify(Notification $notification, string $email = null);
}
