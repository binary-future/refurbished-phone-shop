<?php


namespace App\Utils\Adapters\Transaction;


use App\Utils\Adapters\Transaction\Contracts\Transaction;
use Illuminate\Database\ConnectionInterface;

class LaravelTransaction implements Transaction
{
    /**
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * LaravelTransaction constructor.
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param callable $closure
     * @param int $deadlockAttempts
     * @return mixed
     * @throws \Throwable
     */
    public function transaction(callable $closure, int $deadlockAttempts = 1)
    {
        return $this->connection->transaction($closure, $deadlockAttempts);
    }

    public function beginTransaction(): void
    {
        $this->connection->beginTransaction();
    }

    public function rollback(): void
    {
        $this->connection->rollback();
    }

    public function commit(): void
    {
        $this->connection->commit();
    }
}
