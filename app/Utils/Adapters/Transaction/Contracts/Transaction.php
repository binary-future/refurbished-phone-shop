<?php

namespace App\Utils\Adapters\Transaction\Contracts;

/**
 * Interface Transaction
 * @package App\Domain\Common\Contracts\Repository
 */
interface Transaction
{
    /**
     * @param callable $closure
     * @param int $deadlockAttempts
     * @return mixed
     * @throws \Throwable
     */
    public function transaction(callable $closure, int $deadlockAttempts = 1);

    public function beginTransaction(): void;

    public function rollback(): void;

    public function commit(): void;
}
