<?php

namespace App\Utils\Adapters\Auth\Contracts;

use App\Domain\User\User;

interface Auth
{
    public function getAuthUser(): ?User;
}