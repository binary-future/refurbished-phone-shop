<?php

namespace App\Utils\Adapters\Auth;

use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth as Contract;

final class Auth implements Contract
{
    public function getAuthUser(): ?User
    {
        return auth()->user();
    }

}