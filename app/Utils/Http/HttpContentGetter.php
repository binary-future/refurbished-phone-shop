<?php

namespace App\Utils\Http;

use App\Utils\Http\Contracts\ContentGetter;

/**
 * Class HttpContentGetter
 * @package App\Utils\Http
 */
final class HttpContentGetter implements ContentGetter
{
    /**
     * @param string $url
     * @return bool|string
     */
    public function getContent(string $url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $content = curl_exec($curl);
        curl_close($curl);

        return $content;
    }
}
