<?php

namespace App\Utils\Http\Contracts;

interface ContentGetter
{
    public function getContent(string $url);
}
