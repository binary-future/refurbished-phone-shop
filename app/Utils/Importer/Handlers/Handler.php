<?php

namespace App\Utils\Importer\Handlers;

use App\Application\Services\Importer\Contextable;
use App\Application\Services\Importer\Handler as Contract;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Loggable;
use App\Application\Services\Importer\Response\Contracts\ImportResponse;
use App\Application\Services\Importer\Response\ImportResponse as Response;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Application\Services\Importer\Transformers\Contracts\Transformer;

class Handler implements Contract, Contextable, Loggable
{
    private const STATUS_SKIPPED = 'skipped';
    private const STATUS_ERROR = 'error';

    /**
     * @var Source
     */
    private $source;

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * @var Importer
     */
    private $importer;

    /**
     * @var ImportResponse
     */
    private $response;

    /**
     * @var bool
     */
    private $logging = true;

    /**
     * @var array
     */
    private $configurableElements = [
        'source', 'transformer', 'importer'
    ];

    /**
     * Handler constructor.
     * @param Source $source
     * @param Transformer $transformer
     * @param Importer $importer
     */
    public function __construct(Source $source, Transformer $transformer, Importer $importer)
    {
        $this->source = $source;
        $this->transformer = $transformer;
        $this->importer = $importer;
    }

    public function setContext($context)
    {
        foreach ($this->configurableElements as $element) {
            if ($this->{$element} instanceof Contextable) {
                $this->{$element}->setContext($context);
            }
        }
    }

    public function handleImport(Scenario $scenario): ImportResponse
    {
        $this->initiateResponse();
        try {
            $this->proceedImport($scenario);
        } catch (\Throwable $exception) {
            $this->response->setSuccess(false);
            $this->handleResponse(self::STATUS_ERROR, $exception->getMessage());
        }

        return $this->response;
    }

    /**
     * @param bool $logging
     * @return mixed|void
     */
    public function setLogging(bool $logging)
    {
        $this->logging = $logging;
    }


    /**
     * @param Scenario $scenario
     */
    private function proceedImport(Scenario $scenario)
    {
        $sourceResponse = $this->source->getContent($scenario);
        if ($sourceResponse->isPaginated()) {
            return $this->importPaginated($sourceResponse, $scenario);
        }

        return $this->importChunk($sourceResponse->getContent(), $scenario);
    }

    private function importChunk(iterable $content, Scenario $scenario)
    {
        $count = 1;
        foreach ($content as $line) {
            if (env('APP_ENV') !== 'production' && $count > 100000) {
                break;
            }
            $this->logging($scenario->getDescription(), $count);
            $this->importLine($line, $scenario);
            $count++;
        }
    }

    private function importPaginated(SourceResponse $response, Scenario $scenario)
    {
        $content = $response->getContent();
        foreach ($content as $chunk) {
            $this->importChunk($chunk, $scenario);
        }
    }

    private function importLine(array $line, Scenario $scenario)
    {
        $transformedData = $this->transformer->transform($line, ['scenario_content' => $scenario->getContent()]);
        if (! $transformedData) {
            return $this->handleResponse(self::STATUS_SKIPPED);
        }

        return $this->handleResponse($this->importer->import($transformedData));
    }

    private function handleResponse(string $status, $value = null)
    {
        $this->response->add($status, $value);
    }

    private function initiateResponse()
    {
        $this->response = new Response();
    }

    private function logging(string $description, int $iteration)
    {
        // TODO: change echo on logger
        if ($this->logging) {
            echo($iteration . PHP_EOL);
            echo($description . PHP_EOL);
        }
    }
}
