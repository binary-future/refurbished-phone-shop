<?php

namespace App\Utils\Importer\Handlers;

use App\Application\Services\Importer\Contextable;
use App\Application\Services\Importer\Handler;
use App\Application\Services\Importer\HandlerFactory;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;

/**
 * Class Factory
 * @package App\Utils\Importer\Handlers
 */
final class Factory implements HandlerFactory
{
    private const CONFIG_PATH = 'importers.handler.schema';
    public const DEFAULT_SCENARIO = 'default';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array
     */
    private $schema;

    /**
     * Factory constructor.
     * @param Config $config
     * @param Container $container
     */
    public function __construct(Config $config, Container $container)
    {
        $this->config = $config;
        $this->container = $container;
        $this->configure();
    }

    private function configure(): void
    {
        $this->schema = $this->config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param Scenario $scenario
     * @return Handler
     * @throws ImportHandlerBuildException
     */
    public function buildHandler(Scenario $scenario): Handler
    {
        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw ImportHandlerBuildException::cannotBuildHandler($scenario, $exception->getMessage());
        }
    }

    /**
     * @param Scenario $scenario
     * @return Handler
     * @throws \ReflectionException
     */
    private function proceed(Scenario $scenario): Handler
    {
        $importSchema = $this->getSchema($scenario->getContext());
        /**
         * @var Handler $handler
         */
        $handler = new $importSchema['handler'](
            $this->getInstance($importSchema['source']),
            $this->getInstance($importSchema['transformer']),
            $this->getInstance($importSchema['importer'])
        );
        if ($handler instanceof Contextable) {
            $handler->setContext($scenario->getContext());
        }

        return $handler;
    }

    /**
     * @param string $context
     * @return array
     */
    private function getSchema(string $context): array
    {
        return $this->schema[$context] ?? $this->schema[self::DEFAULT_SCENARIO];
    }

    /**
     * @param string $className
     * @return object
     * @throws \ReflectionException
     */
    private function getInstance(string $className): object
    {
        return $this->container->resolve($className);
    }
}
