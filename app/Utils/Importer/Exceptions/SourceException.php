<?php

namespace App\Utils\Importer\Exceptions;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;

/**
 * Class SourceException
 * @package App\Utils\Importer\Exceptions
 */
final class SourceException extends \Exception
{
    /**
     * @param Scenario $scenario
     * @param string $error
     * @return SourceException
     */
    public static function cannotGetContentByScenario(Scenario $scenario, string $error): ?SourceException
    {
        return new self(
            sprintf('Cannot get source by given scenario - %s. %s', get_class($scenario), $error)
        );
    }

    /**
     * @param Source $source
     * @param string $error
     * @return SourceException
     */
    public static function cannotConfigureSource(Source $source, string $error)
    {
        return new self(
            sprintf('Cannot configure source - %s. %s', get_class($source), $error)
        );
    }

    /**
     * @param string $datafeedApiName
     * @param \Throwable $exception
     * @return SourceException
     */
    public static function cannotCreateRequestGenerator(string $datafeedApiName, \Throwable $exception)
    {
        return new self(
            "Cannot create request generator - $datafeedApiName.",
            0,
            $exception
        );
    }
}
