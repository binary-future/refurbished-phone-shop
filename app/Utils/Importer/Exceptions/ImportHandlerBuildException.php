<?php

namespace App\Utils\Importer\Exceptions;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Class ImportHandlerBuildException
 * @package App\Utils\Importer\Exceptions
 */
final class ImportHandlerBuildException extends \Exception
{
    /**
     * @param Scenario $scenario
     * @param string|null $error
     * @return ImportHandlerBuildException
     */
    public static function cannotBuildHandler(Scenario $scenario, string $error = null)
    {
        return new self(
            sprintf(
                'Cannot build import handler by given scenario - %s. %s',
                get_class($scenario),
                $error ?: ''
            )
        );
    }

    /**
     * @return ImportHandlerBuildException
     */
    public static function cannotFindScenario()
    {
        return new self('Cannot find scenario');
    }
}
