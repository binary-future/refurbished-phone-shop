<?php

namespace App\Utils\Importer\Source\LinkGenerator;

use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory as Contract;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Awin\RequestLinkGenerator as AwinRequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Ecrawler\RequestLinkGenerator as EcrawlerRequestLinkGenerator;

class LinkGeneratorFactory implements Contract
{
    private const DATAFEED_LINK_GENERATOR_MAP = [
        self::DATAFEED_AWIN      => AwinRequestLinkGenerator::class,
        self::DATAFEED_ECRAWLER  => EcrawlerRequestLinkGenerator::class,
//        self::DATAFEED_REVGLUE   => RevglueRequestGenerator::class,
//        self::DATAFEED_WEBGAINS  => WebgainsRequestLinkGenerator::class
    ];

    /**
     * @var Container
     */
    private $container;

    /**
     * LinkGeneratorFactory constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $datafeedName
     * @return RequestLinkGenerator
     * @throws SourceException
     */
    public function getRequestGenerator(string $datafeedName): RequestLinkGenerator
    {
        try {
            return $this->resolve(
                self::DATAFEED_LINK_GENERATOR_MAP[$datafeedName]
            );
        } catch (\Throwable $exception) {
            throw SourceException::cannotCreateRequestGenerator(
                $datafeedName,
                $exception
            );
        }
    }

    /**
     * @param string $class
     * @return mixed
     * @throws \ReflectionException
     */
    private function resolve(string $class)
    {
        return $this->container->resolve($class);
    }
}
