<?php


namespace App\Utils\Importer\Source\Download;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Response\SourceResponse as Response;

abstract class ZipDownloadSource implements Source
{
    private const SETTINGS_ZIP_FILE = 'zip_file';
    private const SETTINGS_BASE_DIR = 'base_dir';
    private const SETTINGS_DEFAULT_STORAGE_CONTEXT = 'default_storage_context';
    private const DEFAULT_STORAGE_CONTEXT = 'default';

    /**
     * @var ContentGetter
     */
    private $contentGetter;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var Zipper
     */
    private $zipper;

    /**
     * @var CSVEncoder
     */
    private $csvEncoder;

    /**
     * @var mixed
     */
    private $settings;

    /**
     * @var array
     */
    private $shouldDelete = [];

    /**
     * @var Scenario
     */
    private $scenario;
    /**
     * @var string
     */
    private $storageContext;

    /**
     * AwinApiSource constructor.
     * @param Config $config
     * @param ContentGetter $contentGetter
     * @param Storage $storage
     * @param Zipper $zipper
     * @param CSVEncoder $encoder
     */
    public function __construct(
        Config $config,
        ContentGetter $contentGetter,
        Storage $storage,
        Zipper $zipper,
        CSVEncoder $encoder
    ) {
        $this->contentGetter = $contentGetter;
        $this->storage = $storage;
        $this->zipper = $zipper;
        $this->csvEncoder = $encoder;
        $this->settings = $config->get($this->getConfigPath(), []);
    }

    /**
     * @return string
     */
    abstract protected function getConfigPath(): string;

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     * @throws SourceException
     * @throws \Throwable
     */
    public function getContent(Scenario $scenario): SourceResponse
    {
        if (! $this->checkScenario($scenario)) {
            throw SourceException::cannotGetContentByScenario($scenario, 'Invalid scenario');
        }

        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw SourceException::cannotGetContentByScenario($scenario, $exception->getMessage());
        }
    }

    abstract protected function checkScenario(Scenario $scenario): bool;

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     * @throws SourceException
     */
    private function proceed(Scenario $scenario): SourceResponse
    {
        $this->scenario = $scenario;
        $this->storageContext = $this->getStorageContext($scenario);

        $storagePath = $this->storagePath();
        $this->storeApiContent($storagePath, $this->getRequestLink($scenario));

        $contentPath = $this->extractApiContent();
        $data = $this->csvEncoder->decode($contentPath);
        $response =  new Response($data);

        return $response;
    }

    /**
     * @param Scenario $scenario
     * @return string
     */
    protected function getStorageContext(Scenario $scenario): string
    {
        return $this->settings[self::SETTINGS_DEFAULT_STORAGE_CONTEXT]
            ?? self::DEFAULT_STORAGE_CONTEXT;
    }

    /**
     * @return string
     */
    private function storagePath(): string
    {
        return sprintf($this->settings[self::SETTINGS_ZIP_FILE] ?? '', $this->storageContext);
    }

    /**
     * @return string
     */
    private function baseDirWithContext(): string
    {
        return ($this->settings[self::SETTINGS_BASE_DIR] ?? '') . '/' . $this->storageContext;
    }

    /**
     * @param Scenario $scenario
     * @return string
     */
    abstract protected function getRequestLink(Scenario $scenario): string;

    /**
     * @param string $path
     * @param string $link
     */
    private function storeApiContent(string $path, string $link): void
    {
        $content = $this->contentGetter->getContent($link);
        $this->storage->put($path, $content);
        $this->addToDeleteList($path);
    }

    /**
     * @return string
     * @throws SourceException
     */
    private function extractApiContent()
    {
        $extractPath = $this->generateExtractPath();
        $zipFilePath = $this->generateZipFilePath();
        $extractedFileName = $this->getExtractedFileName($zipFilePath);
        if (! $extractedFileName) {
            throw SourceException::cannotGetContentByScenario($this->scenario, 'No content at zip file');
        }
        $extractedFilePath = sprintf('%s/%s', $extractPath, $extractedFileName);
        $this->addToDeleteList(sprintf('%s/%s', $this->baseDirWithContext(), $extractedFileName));
        $this->zipper->extractByName($zipFilePath, $extractPath, [$extractedFileName]);

        return $extractedFilePath;
    }

    /**
     * @param string $path
     */
    private function addToDeleteList(string $path)
    {
        $this->shouldDelete[] = $path;
    }

    /**
     * @return string
     */
    private function generateExtractPath()
    {
        return storage_path(sprintf('%s/%s', $this->settings['prefix'], $this->baseDirWithContext()));
    }

    /**
     * @return string
     */
    private function generateZipFilePath()
    {
        return storage_path(sprintf('%s/%s', $this->settings['prefix'], $this->storagePath()));
    }

    /**
     * @param string $path
     * @return string|null
     */
    private function getExtractedFileName(string $path): ?string
    {
        $names = $this->zipper->getNames($path);

        return array_first($names);
    }

    public function __destruct()
    {
        foreach ($this->shouldDelete as $file) {
            $this->storage->delete($file);
        }
    }
}
