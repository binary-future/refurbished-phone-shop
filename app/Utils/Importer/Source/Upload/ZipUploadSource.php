<?php


namespace App\Utils\Importer\Source\Upload;

use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Adapters\Filesystem\Contracts\Filesystem;

/**
 * Class ZipUploadSource
 * @package App\Utils\Importer\Source\Upload
 */
final class ZipUploadSource extends UploadSource
{
    /**
     * @var Zipper
     */
    private $zipper;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $exportPath;

    /**
     * @var array
     */
    private $extensions = [];

    /**
     * @var array
     */
    private $exportedFiles = [];

    /**
     * ZipUploadSource constructor.
     * @param Config $config
     * @param Storage $storage
     * @param Zipper $zipper
     * @param Filesystem $filesystem
     * @throws \App\Utils\Importer\Exceptions\SourceException
     */
    public function __construct(Config $config, Storage $storage, Zipper $zipper, Filesystem $filesystem)
    {
        parent::__construct($config, $storage);
        $this->zipper = $zipper;
        $this->filesystem = $filesystem;
    }

    /**
     * @param array $configs
     */
    protected function configure(array $configs)
    {
        $configs = $configs[static::class];
        $this->setUploadPath($configs['upload_path']);
        $this->exportPath = $configs['export_path'];
        $this->extensions = $configs['extensions'];
    }

    /**
     * @param string $path
     * @return array
     */
    protected function handleFileContent(string $path)
    {
        $fileNames = $this->getFilesNames($path);
        $filteredNames = $this->filterFileNames($fileNames);
        $this->zipper->extractByName($path, $this->exportPath, $filteredNames);

        return $this->preparePaths($filteredNames);
    }

    /**
     * @param array $files
     * @return array
     */
    protected function preparePaths(array $files)
    {
        $result = [];
        foreach ($files as $file) {
            $path = sprintf('%s/%s', $this->exportPath, $file);
            $result[] = [
                'name' => $file,
                'path' => $path
            ];
            $this->exportedFiles[] = $path;
        }

        return $result;
    }

    /**
     * @param string $path
     * @return mixed
     */
    protected function getFilesNames(string $path)
    {
        return $this->zipper->getNames($path);
    }

    /**
     * @param array $names
     * @return array
     */
    protected function filterFileNames(array $names)
    {
        $result = [];

        foreach ($names as $name) {
            if ($this->isValid($name)) {
                $result[] = $name;
            }
        }

        return $result;
    }

    /**
     * @param string $name
     * @return bool
     */
    protected function isValid(string $name)
    {
        return in_array($this->getExtension($name), $this->extensions);
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function getExtension(string $name)
    {
        $nameParts = explode('.', $name);

        return array_pop($nameParts);
    }

    /**
     * Clean extracted files
     */
    protected function cleanUp()
    {
        $this->filesystem->delete($this->exportedFiles);
    }

    public function __destruct()
    {
        parent::__destruct();
        $this->cleanUp();
    }
}
