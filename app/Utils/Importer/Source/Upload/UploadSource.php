<?php


namespace App\Utils\Importer\Source\Upload;


use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Importer\Exceptions\SourceException;
use Illuminate\Http\UploadedFile;
use App\Utils\Importer\Source\Response\SourceResponse as Response;

/**
 * Class UploadSource
 * @package App\Utils\Importer\Source\Upload
 */
abstract class UploadSource implements Source
{
    private const CONFIG_PATH = 'file.upload.import.configs';

    /**
     * @var string
     */
    private $uploadPath;

    /**
     * @var string
     */
    private $storedFile;

    /**
     * @var Storage
     */
    private $storage;

    public function __construct(Config $config, Storage $storage)
    {
        try {
            $this->configure($config->get(self::CONFIG_PATH, []));
        } catch (\Throwable $exception) {
            throw SourceException::cannotConfigureSource($this, $exception->getMessage());
        }
        $this->storage = $storage;
    }

    abstract protected function configure(array $configs);

    /**
     * @param string $path
     */
    protected function setUploadPath(string $path)
    {
        $this->uploadPath = $path;
    }

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     * @throws SourceException
     */
    public function getContent(Scenario $scenario): SourceResponse
    {
        if (! $scenario->getContent() instanceof UploadedFile) {
            throw SourceException::cannotGetContentByScenario($scenario, 'Invalid scenario content');
        }

        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw SourceException::cannotGetContentByScenario($scenario, $exception->getMessage());
        }
    }

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     */
    private function proceed(Scenario $scenario): SourceResponse
    {
        /**
         * @var UploadedFile $file
         */
        $file = $scenario->getContent();
        $path = $file->store($this->uploadPath);
        $this->storedFile = $path;

        return $this->getData($path);
    }

    protected function getData(string $path)
    {
        $content =  $this->handleFileContent(storage_path(sprintf('app/%s', $path)));

        return new Response($content);
    }

    abstract protected function handleFileContent(string $path);

    public function __destruct()
    {
        $this->storage->delete($this->storedFile);
    }
}
