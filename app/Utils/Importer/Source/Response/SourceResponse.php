<?php

namespace App\Utils\Importer\Source\Response;

use App\Application\Services\Importer\Source\SourceResponse as Contract;

/**
 * Class SourceResponse
 * @package App\Utils\Importer\Source\Response
 */
final class SourceResponse implements Contract
{
    /**
     * @var iterable
     */
    private $content = [];

    /**
     * @var bool
     */
    private $succeed = true;

    /**
     * @var bool
     */
    private $paginated = false;

    /**
     * @var int
     */
    private $pages;

    /**
     * @var int
     */
    private $currentPage = 1;

    /**
     * @var string
     */
    private $message;

    /**
     * SourceResponse constructor.
     * @param iterable $content
     */
    public function __construct(iterable $content)
    {
        $this->content = $content;
    }

    /**
     * @return iterable
     */
    public function getContent(): iterable
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }

    /**
     * @param bool $succeed
     */
    public function setSucceed(bool $succeed): void
    {
        $this->succeed = $succeed;
    }

    /**
     * @return bool
     */
    public function isPaginated(): bool
    {
        return $this->paginated;
    }

    /**
     * @param bool $paginated
     */
    public function setPaginated(bool $paginated): void
    {
        $this->paginated = $paginated;
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     */
    public function setPages(int $pages): void
    {
        $this->pages = $pages;
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return mixed|void
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }
}
