<?php

namespace App\Utils\Importer\Source\Dump;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Importer\Source\Response\SourceResponse as Response;

/**
 * Class CSVDumpSource
 * @package App\Utils\Importer\Source\Dump
 */
final class CSVDumpSource extends DumpSource
{
    /**
     * @var CSVEncoder
     */
    private $encoder;

    /**
     * JsonDumpSource constructor.
     * @param Config $config
     * @param CSVEncoder $encoder
     */
    public function __construct(Config $config, CSVEncoder $encoder)
    {
        parent::__construct($config);
        $this->encoder = $encoder;
    }

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     */
    public function getContent(Scenario $scenario): SourceResponse
    {
        $file = $this->getFile($scenario);

        return new Response($file ? $this->encoder->decode($file) : []);
    }
}
