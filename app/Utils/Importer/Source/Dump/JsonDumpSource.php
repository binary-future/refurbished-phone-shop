<?php


namespace App\Utils\Importer\Source\Dump;


use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Importer\Source\Response\SourceResponse as Response;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Encoders\Contracts\JsonEncoder;

/**
 * Class JsonDumpSource
 * @package App\Utils\Importer\Source\Dump
 */
final class JsonDumpSource extends DumpSource
{
    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * JsonDumpSource constructor.
     * @param Config $config
     * @param JsonEncoder $encoder
     */
    public function __construct(Config $config, JsonEncoder $encoder)
    {
        parent::__construct($config);
        $this->encoder = $encoder;
    }

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     */
    public function getContent(Scenario $scenario): SourceResponse
    {
        $content = $this->getDumpContent($scenario);

        return new Response($content ? $this->encoder->decode($content) : []);
    }
}
