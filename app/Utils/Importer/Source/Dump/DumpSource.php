<?php


namespace App\Utils\Importer\Source\Dump;


use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Utils\Adapters\Config\Contracts\Config;

abstract class DumpSource implements Source
{
    private const DUMP_FILES_LIST_CONFIG = 'importers.dumps.list.list';

    /**
     * @var array
     */
    private $dumpsFiles = [];

    /**
     * DumpSource constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->dumpsFiles = $config->get(self::DUMP_FILES_LIST_CONFIG, []);
    }

    protected function getFile(Scenario $scenario)
    {
        return $this->dumpsFiles[$scenario->getContext()] ?? null;
    }

    protected function getDumpContent(Scenario $scenario)
    {
        $file = $this->getFile($scenario);
        if (! $file) {
            return null;
        }

        return file_get_contents($file);
    }
}
