<?php

namespace App\Utils\Importer\Source\Mock;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Encoders\Contracts\CSVEncoder;

class MockSource implements Source
{
    /**
     * @var CSVEncoder
     */
    private $csvEncoder;

    /**
     * MockSource constructor.
     * @param CSVEncoder $csvEncoder
     */
    public function __construct(CSVEncoder $csvEncoder)
    {
        $this->csvEncoder = $csvEncoder;
    }

    public function getContent(Scenario $scenario): SourceResponse
    {
        $path = storage_path('mocks/affrod.csv');

        $data = $this->csvEncoder->decode($path);

        return new \App\Utils\Importer\Source\Response\SourceResponse($data);
    }

}