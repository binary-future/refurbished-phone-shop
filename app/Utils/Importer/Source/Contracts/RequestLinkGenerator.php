<?php

namespace App\Utils\Importer\Source\Contracts;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

interface RequestLinkGenerator
{
    /**
     * @param Scenario $scenario
     * @return string
     */
    public function generateLink(Scenario $scenario): string;
}
