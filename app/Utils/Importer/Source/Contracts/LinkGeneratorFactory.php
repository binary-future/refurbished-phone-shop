<?php

namespace App\Utils\Importer\Source\Contracts;

use App\Utils\Importer\Exceptions\SourceException;

interface LinkGeneratorFactory
{
    public const DATAFEED_AWIN = 'awin';
    public const DATAFEED_ECRAWLER = 'ecrawler';
    public const DATAFEED_REVGLUE = 'revglue';
    public const DATAFEED_WEBGAINS = 'webgains';

    /**
     * @param string $datafeedName
     * @return RequestLinkGenerator
     * @throws SourceException
     */
    public function getRequestGenerator(string $datafeedName): RequestLinkGenerator;
}
