<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Response\Contracts;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\SourceResponse;

/**
 * Interface RevglueResponseHandler
 * @package App\Utils\Importer\Source\Revglue\Response\Contracts
 */
interface RevglueResponseHandler
{
    /**
     * @param array $response
     * @param Scenario $scenario
     * @return SourceResponse
     */
    public function handle(array $response, Scenario $scenario): SourceResponse;
}
