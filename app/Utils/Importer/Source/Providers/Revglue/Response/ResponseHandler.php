<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Response;

use App\Application\Services\Importer\Scenarios\Contracts\Paginable;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Revglue\Response\Contracts\RevglueResponseHandler;
use App\Utils\Importer\Source\Response\SourceResponse as Response;

/**
 * Class ResponseHandler
 * @package App\Utils\Importer\Source\Providers\Revglue\Response
 */
class ResponseHandler implements RevglueResponseHandler
{
    private const CONFIG_PATH = 'importers.api.revglue.response.indexes';

    /**
     * @var array
     */
    private $contentIndexes = [];

    /**
     * ResponseHandler constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->contentIndexes = $config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param array $response
     * @param Scenario $scenario
     * @return SourceResponse
     * @throws SourceException
     */
    public function handle(array $response, Scenario $scenario): SourceResponse
    {
        if (! $response['response']['success']) {
            throw SourceException::cannotGetContentByScenario($scenario, 'Failed api response');
        }
        $sourceResponse = $this->getResponse($response['response'][$this->getIndex($scenario)]);
        if (isset($response['response']['total_pages']) && $scenario instanceof Paginable) {
            $sourceResponse->setPaginated(true);
            $sourceResponse->setPages($response['response']['total_pages']);
            $sourceResponse->setCurrentPage($scenario->getCurrentPage());
        }

        return $sourceResponse;
    }

    /**
     * @param array $content
     * @return Response
     */
    private function getResponse(array $content = [])
    {
        return new Response($content);
    }

    /**
     * @param Scenario $scenario
     * @return mixed
     */
    private function getIndex(Scenario $scenario)
    {
        return $this->contentIndexes[$scenario->getContext()];
    }
}
