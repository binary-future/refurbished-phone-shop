<?php

namespace App\Utils\Importer\Source\Providers\Revglue;

use App\Application\Services\Importer\Scenarios\Contracts\Paginable;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Encoders\Contracts\JsonEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Revglue\Response\Contracts\RevglueResponseHandler as ResponseHandler;
use App\Utils\Importer\Source\Response\SourceResponse as Response;
use Generator;
use Throwable;

final class RevglueApiSource implements Source
{
    /**
     * @var RequestLinkGenerator
     */
    private $requestLinkGenerator;

    /**
     * @var ContentGetter
     */
    private $contentGetter;

    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * @var ResponseHandler
     */
    private $responseHandler;

    /**
     * RevglueApiSource constructor.
     * @param LinkGeneratorFactory $linkGeneratorFactory
     * @param ContentGetter $contentGetter
     * @param JsonEncoder $encoder
     * @param ResponseHandler $responseHandler
     * @throws SourceException
     */
    public function __construct(
        LinkGeneratorFactory $linkGeneratorFactory,
        ContentGetter $contentGetter,
        JsonEncoder $encoder,
        ResponseHandler $responseHandler
    ) {
        $this->requestLinkGenerator = $linkGeneratorFactory->getRequestGenerator(
            LinkGeneratorFactory::DATAFEED_REVGLUE
        );
        $this->contentGetter = $contentGetter;
        $this->encoder = $encoder;
        $this->responseHandler = $responseHandler;
    }

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     * @throws SourceException
     */
    public function getContent(Scenario $scenario): SourceResponse
    {
        try {
            return $this->proceed($scenario);
        } catch (Throwable $exception) {
            throw SourceException::cannotGetContentByScenario($scenario, $exception->getMessage());
        }
    }

    private function proceed(Scenario $scenario): SourceResponse
    {
        $response = $this->getResponse($scenario);

        if ($scenario instanceof Paginable && $response->isPaginated()) {
            $paginableResponse = new Response($this->getNextPages($scenario, $response));
            $paginableResponse->setPaginated(true);

            return $paginableResponse;
        }

        return $response;
    }

    private function getNextPages(Paginable $scenario, SourceResponse $response): Generator
    {
        while ($scenario->getCurrentPage() <= $response->getPages()) {
            echo(sprintf('%s of %s pages.%s', $scenario->getCurrentPage(), $response->getPages(), PHP_EOL));
            echo(memory_get_usage() / 1048576 . PHP_EOL);
            $nextPageResponse = $this->getResponse($scenario);
            $scenario->setCurrentPage($nextPageResponse->getCurrentPage() + 1);
            yield $nextPageResponse->getContent();
        }
    }

    private function getResponse(Scenario $scenario): SourceResponse
    {
        $link = $this->getLink($scenario);
        $content = $this->getContentData($link);

        return  $this->handleResponse($content, $scenario);
    }

    private function getLink(Scenario $scenario): string
    {
        return $this->requestLinkGenerator->generateLink($scenario);
    }

    private function getContentData(string $link): array
    {
        return $this->encoder->decode($this->contentGetter->getContent($link));
    }

    private function handleResponse(array $content, Scenario $scenario): SourceResponse
    {
        return $this->responseHandler->handle($content, $scenario);
    }
}
