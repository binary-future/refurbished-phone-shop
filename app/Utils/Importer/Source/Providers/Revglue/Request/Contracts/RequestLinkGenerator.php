<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Request\Contracts;

/**
 * Interface RequestLinkGenerator
 * @package App\Utils\Importer\Source\Revglue\Request\Contracts
 */
interface RequestLinkGenerator extends \App\Utils\Importer\Source\Contracts\RequestLinkGenerator
{
}
