<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\Paginable;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Scenarios\RevglueDealsImport;

final class ContentDependentGenerator extends RevglueRequestGenerator
{
    /**
     * @param RevglueDealsImport $scenario
     * @return string
     */
    protected function proceed(Scenario $scenario): string
    {
        /**
         * @var DatafeedInfo $datafeed
         */
        $datafeed = $scenario->getContent();

        return sprintf(
            '%s/%s/%s/%s/%s/%s',
            $this->settings['domain'],
            $this->settings['product'][$scenario->getContext()],
            $this->settings['type'],
            $this->settings['key'],
            $datafeed->getExternalId(),
            $scenario->getCurrentPage() !== Paginable::DEFAULT_CURRENT_PAGE
                ? $scenario->getCurrentPage()
                : ''
        );
    }
}
