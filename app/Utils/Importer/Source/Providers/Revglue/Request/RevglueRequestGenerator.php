<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Source\Providers\Revglue\Request\Contracts\RequestLinkGenerator;

abstract class RevglueRequestGenerator implements RequestLinkGenerator
{
    private const SETTINGS_PATH = 'importers.api.revglue.settings';

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * ContentIndependentGenerator constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->settings = $config->get(self::SETTINGS_PATH, []);
    }

    /**
     * @param Scenario $scenario
     * @return string
     */
    public function generateLink(Scenario $scenario): string
    {
        return $this->proceed($scenario);
    }

    abstract protected function proceed(Scenario $scenario);
}
