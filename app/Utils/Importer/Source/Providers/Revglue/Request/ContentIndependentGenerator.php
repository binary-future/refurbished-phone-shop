<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Class ContentIndependentGenerator
 * @package App\Utils\Importer\Source\Revglue\Request
 */
class ContentIndependentGenerator extends RevglueRequestGenerator
{
    /**
     * @param Scenario $scenario
     * @return string
     */
    protected function proceed(Scenario $scenario)
    {
        return sprintf(
            '%s/%s/%s/%s',
            $this->settings['domain'],
            $this->settings['product'][$scenario->getContext()],
            $this->settings['type'],
            $this->settings['key']
        );
    }
}
