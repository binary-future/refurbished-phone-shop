<?php

namespace App\Utils\Importer\Source\Providers\Revglue\Request;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Providers\Revglue\Request\Contracts\RequestLinkGenerator;

/**
 * Class RequestGenerator
 * @package App\Utils\Importer\Source\Revglue\Request
 */
class RequestGenerator implements RequestLinkGenerator
{
    private const CONFIG_PATH = 'importers.api.revglue.request.schema';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var array
     */
    private $schema = [];
    /**
     * RequestGenerator constructor.
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->config = $config;
        $this->configure();
    }

    /**
     *
     */
    private function configure()
    {
        $this->schema = $this->config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param Scenario $scenario
     * @return string
     * @throws SourceException
     */
    public function generateLink(Scenario $scenario): string
    {
        try {
            $generator = $this->getGenerator($this->schema[$scenario->getContext()]);

            return $generator->generateLink($scenario);
        } catch (\Throwable $exception) {
            throw SourceException::cannotGetContentByScenario($scenario, $exception->getMessage());
        }
    }

    /**
     * @param string $className
     * @return \App\Utils\Importer\Source\Providers\Revglue\Request\Contracts\RequestLinkGenerator
     */
    private function getGenerator(string $className): RequestLinkGenerator
    {
        return $this->container->resolve($className);
    }
}
