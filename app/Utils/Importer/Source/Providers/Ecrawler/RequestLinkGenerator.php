<?php

namespace App\Utils\Importer\Source\Providers\Ecrawler;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\AwinScenario;
use App\Application\Services\Importer\Scenarios\Contracts\EcrawlerScenario;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator as Contract;

class RequestLinkGenerator implements Contract
{
    private const CONFIG_PATH = 'importers.api.ecrawler.settings.request';

    /**
     * @var array
     */
    private $configs;

    /**
     * RequestLinkGenerator constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->configs = $config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param Scenario $scenario
     * @return string
     * @throws SourceException
     */
    public function generateLink(Scenario $scenario): string
    {
        try {
            if (! $scenario instanceof EcrawlerScenario) {
                throw new \InvalidArgumentException(
                    sprintf('Expected %s, given %s', AwinScenario::class, get_class($scenario))
                );
            }

            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw SourceException::cannotGetContentByScenario(
                $scenario,
                sprintf('Cannot generate link. %s', $exception->getMessage()),
            );
        }
    }

    private function proceed(EcrawlerScenario $scenario): string
    {
        /**
         * @var DatafeedInfo $datafeed
         */
        $datafeed = $scenario->getContent();

        return sprintf(
            '%s/%s?api_key=%s',
            $this->configs['domain'],
            $datafeed->getExternalId(),
            $this->configs['key'],
        );
    }
}
