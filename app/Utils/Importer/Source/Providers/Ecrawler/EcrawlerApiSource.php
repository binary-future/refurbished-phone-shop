<?php

namespace App\Utils\Importer\Source\Providers\Ecrawler;

use App\Application\Services\Importer\Scenarios\Contracts\EcrawlerScenario;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Source\Source;
use App\Application\Services\Importer\Source\SourceResponse;
use App\Utils\Encoders\Contracts\JsonEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Response\SourceResponse as Response;
use Throwable;

final class EcrawlerApiSource implements Source
{
    /**
     * @var LinkGeneratorFactory
     */
    private $linkGeneratorFactory;
    /**
     * @var ContentGetter
     */
    private $contentGetter;
    /**
     * @var JsonEncoder
     */
    private $encoder;

    /**
     * AwinApiSource constructor.
     * @param ContentGetter $contentGetter
     * @param JsonEncoder $encoder
     * @param LinkGeneratorFactory $linkGeneratorFactory
     */
    public function __construct(
        ContentGetter $contentGetter,
        JsonEncoder $encoder,
        LinkGeneratorFactory $linkGeneratorFactory
    ) {
        $this->linkGeneratorFactory = $linkGeneratorFactory;
        $this->contentGetter = $contentGetter;
        $this->encoder = $encoder;
    }

    /**
     * @param Scenario $scenario
     * @return SourceResponse
     * @throws SourceException
     */
    public function getContent(Scenario $scenario): SourceResponse
    {
        if (! $scenario instanceof EcrawlerScenario) {
            throw SourceException::cannotGetContentByScenario($scenario, 'Invalid scenario');
        }

        try {
            return $this->getResponse($scenario);
        } catch (Throwable $exception) {
            throw SourceException::cannotGetContentByScenario($scenario, $exception->getMessage());
        }
    }

    /**
     * @param EcrawlerScenario $scenario
     * @return SourceResponse
     * @throws SourceException
     */
    private function getResponse(EcrawlerScenario $scenario): SourceResponse
    {
        $link = $this->getRequestLink($scenario);
        $content = $this->getContentData($link);

        return new Response($content);
    }

    private function getContentData(string $link): array
    {
        return $this->encoder->decode(
            $this->contentGetter->getContent($link)
        );
    }

    /**
     * @param EcrawlerScenario $scenario
     * @return string
     * @throws SourceException
     */
    protected function getRequestLink(EcrawlerScenario $scenario): string
    {
        $linkGenerator = $this->linkGeneratorFactory->getRequestGenerator(
            LinkGeneratorFactory::DATAFEED_ECRAWLER
        );
        return $linkGenerator->generateLink($scenario);
    }
}
