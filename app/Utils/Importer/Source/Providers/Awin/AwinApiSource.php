<?php

namespace App\Utils\Importer\Source\Providers\Awin;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\AwinScenario;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Download\ZipDownloadSource;

/**
 * Class AwinApiSource
 * @package App\Utils\Importer\Source\Awin
 */
final class AwinApiSource extends ZipDownloadSource
{
    private const CONFIG_PATH = 'importers.api.awin.settings.content';

    /**
     * @var LinkGeneratorFactory
     */
    private $linkGeneratorFactory;

    /**
     * AwinApiSource constructor.
     * @param Config $config
     * @param ContentGetter $contentGetter
     * @param Storage $storage
     * @param Zipper $zipper
     * @param CSVEncoder $encoder
     * @param LinkGeneratorFactory $linkGeneratorFactory
     */
    public function __construct(
        Config $config,
        ContentGetter $contentGetter,
        Storage $storage,
        Zipper $zipper,
        CSVEncoder $encoder,
        LinkGeneratorFactory $linkGeneratorFactory
    ) {
        parent::__construct($config, $contentGetter, $storage, $zipper, $encoder);
        $this->linkGeneratorFactory = $linkGeneratorFactory;
    }

    /**
     * @param Scenario $scenario
     * @return string
     */
    protected function getStorageContext(Scenario $scenario): string
    {
        $content = $scenario->getContent();

        if (! $content instanceof DatafeedInfo) {
            return parent::getStorageContext($scenario);
        }

        return $content->getStore()->getSlug();
    }

    protected function getConfigPath(): string
    {
        return self::CONFIG_PATH;
    }

    protected function checkScenario(Scenario $scenario): bool
    {
        return $scenario instanceof AwinScenario;
    }

    /**
     * @param AwinScenario $scenario
     * @return string
     * @throws SourceException
     */
    protected function getRequestLink(Scenario $scenario): string
    {
        $linkGenerator = $this->linkGeneratorFactory->getRequestGenerator(
            LinkGeneratorFactory::DATAFEED_AWIN
        );
        return $linkGenerator->generateLink($scenario);
    }
}
