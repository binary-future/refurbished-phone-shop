<?php

namespace App\Utils\Importer\Source\Providers\Webgains;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Scenarios\WebgainsAffordableDealsImport;
use App\Application\Services\Importer\Scenarios\WebgainsDealsImport;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\Download\ZipDownloadSource;

final class WebgainsApiSource extends ZipDownloadSource
{
    private const CONFIG_PATH = 'importers.api.webgains.settings.content';

    /**
     * @var LinkGeneratorFactory
     */
    private $linkGeneratorFactory;

    /**
     * WebgainsApiSource constructor.
     * @param Config $config
     * @param ContentGetter $contentGetter
     * @param Storage $storage
     * @param Zipper $zipper
     * @param CSVEncoder $encoder
     * @param LinkGeneratorFactory $linkGeneratorFactory
     */
    public function __construct(
        Config $config,
        ContentGetter $contentGetter,
        Storage $storage,
        Zipper $zipper,
        CSVEncoder $encoder,
        LinkGeneratorFactory $linkGeneratorFactory
    ) {
        parent::__construct($config, $contentGetter, $storage, $zipper, $encoder);
        $this->linkGeneratorFactory = $linkGeneratorFactory;
    }

    protected function getConfigPath(): string
    {
        return self::CONFIG_PATH;
    }

    /**
     * @param Scenario $scenario
     * @return string
     * @throws SourceException
     */
    protected function getRequestLink(Scenario $scenario): string
    {
        $linkGenerator = $this->linkGeneratorFactory->getRequestGenerator(
            LinkGeneratorFactory::DATAFEED_WEBGAINS
        );

        return $linkGenerator->generateLink($scenario);
    }

    /**
     * @param Scenario $scenario
     * @return string
     */
    protected function getStorageContext(Scenario $scenario): string
    {
        $content = $scenario->getContent();

        if (! $content instanceof DatafeedInfo) {
            return parent::getStorageContext($scenario);
        }

        return $content->getStore()->getSlug();
    }

    /**
     * @param Scenario $scenario
     * @return bool
     */
    protected function checkScenario(Scenario $scenario): bool
    {
        return $scenario instanceof WebgainsDealsImport
            || $scenario instanceof WebgainsAffordableDealsImport;
    }
}
