<?php

namespace App\Utils\Importer\Source\Providers\Webgains;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Scenarios\WebgainsDealsImport;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Exceptions\SourceException;
use App\Utils\Importer\Source\Contracts\RequestLinkGenerator as Contract;

class RequestLinkGenerator implements Contract
{
    private const CONFIG_PATH = 'importers.api.webgains.settings.request';

    /**
     * @var array
     */
    private $configs = [];

    public function __construct(Config $config)
    {
        $this->configs = $config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param WebgainsDealsImport $scenario
     * @return string
     * @throws SourceException
     */
    public function generateLink(Scenario $scenario): string
    {
        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw SourceException::cannotGetContentByScenario(
                $scenario,
                sprintf('Cannot generate link. %s', $exception->getMessage())
            );
        }
    }

    private function proceed(Scenario $scenario): string
    {
        /**
         * @var DatafeedInfo $datafeed
         */
        $datafeed = $scenario->getContent();

        return sprintf(
            '%s?action=download&campaign=156977&feeds=%s&categories=1879,1904&fields=extended&fieldIds=%s' .
            '&format=%s&separator=%s&zipformat=%s&allowedtags=all&stripNewlines=0&apikey=%s',
            $this->configs['domain'],
            $datafeed->getExternalId(),
            $this->getFields(),
            $this->configs['format'],
            $this->configs['separator'],
            $this->configs['compression'],
            $this->configs['key']
        );
    }

    private function getFields(): string
    {
        return 'deeplink,image_url,merchant_category,price,product_id,product_name,brand,Colour,contract_length,' .
            'contract_type,data_allowance,handset_price,image_url,inclusive_minutes,inclusive_texts,mobile_network,' .
            'network_promotion,normal_price,stock_code';
    }
}
