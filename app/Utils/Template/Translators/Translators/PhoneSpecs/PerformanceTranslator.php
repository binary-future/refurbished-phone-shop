<?php


namespace App\Utils\Template\Translators\Translators\PhoneSpecs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Domain\Phone\Specs\Performance;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

/**
 * Class PerformanceTranslator
 * @package App\Utils\Template\Translators\Translators\PhoneSpecs
 */
final class PerformanceTranslator extends SpecsTranslator
{
    private const GIGA_DIVIDER = 1024;
    private const GHZ_DIVIDER = 1000;

    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var Performance $specGroup
         */
        return $this->handle($specGroup);
    }

    /**
     * @param Performance $performance
     * @return Specs
     */
    private function handle(Performance $performance)
    {
        $values = [];
        $values['Memory Card'] = $performance->isMemoryCard();
        $values['Memory Card Slot'] = $performance->getMemoryCardSlot();
        $values['Chipset'] = $performance->getChipset();
        $values['Processor'] = $this->getProcessor($performance);
        $values['RAM'] = $this->getRam($performance);

        return $this->getResponse($performance->getName(), $values);
    }

    /**
     * @param Performance $performance
     * @return string|null
     */
    private function getProcessor(Performance $performance)
    {
        return $performance->getProcessor()
            ? sprintf('%s Ghz', $performance->getProcessor() / self::GHZ_DIVIDER)
            : null;
    }

    /**
     * @param Performance $performance
     * @return string|null
     */
    private function getRam(Performance $performance)
    {
        return $performance->getRam()
            ? sprintf('%s GB', $performance->getRam() / self::GIGA_DIVIDER)
            : null;
    }
}
