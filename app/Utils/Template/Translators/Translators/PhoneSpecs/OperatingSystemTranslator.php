<?php


namespace App\Utils\Template\Translators\Translators\PhoneSpecs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Domain\Phone\Specs\OperatingSystem;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

final class OperatingSystemTranslator extends SpecsTranslator
{
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var OperatingSystem $specGroup
         */
        return $this->handle($specGroup);
    }

    private function handle(OperatingSystem $operatingSystem)
    {
        $values = [];
        $values['Platform'] = $operatingSystem->getPlatform();
        $values['Operating System'] = $operatingSystem->getOperationSystem();

        return $this->getResponse($operatingSystem->getName(), $values);
    }
}
