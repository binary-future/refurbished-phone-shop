<?php


namespace App\Utils\Template\Translators\Translators\PhoneSpecs;

use App\Domain\Phone\Specs\Camera;
use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

final class CameraTranslator extends SpecsTranslator
{
    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var Camera $specGroup
         */
        return $this->handle($specGroup);
    }

    /**
     * @param Camera $camera
     * @return Specs
     */
    private function handle(Camera $camera)
    {
        $values = [];
        $values['Front Camera'] = $this->getInPixels($camera->getFrontCamera());
        $values['Back Camera'] = $this->getInPixels($camera->getBackCamera());

        return $this->getResponse($camera->getName(), $values);
    }

    private function getInPixels($param)
    {
        return $param ? sprintf('%s MP', $param) : null;
    }
}
