<?php


namespace App\Utils\Template\Translators\Translators\PhoneSpecs;


use App\Domain\Phone\Specs\Autonomy;
use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

final class AutonomyTranslator extends SpecsTranslator
{
    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var Autonomy $specGroup
         */
        return $this->handle($specGroup);
    }

    /**
     * @param Autonomy $autonomy
     * @return Specs
     */
    private function handle(Autonomy $autonomy)
    {
        $values = [];
        $values['Talk Time'] = $this->getInHours($autonomy->getTalkTime());
        $values['Stanby Time'] = $this->getInHours($autonomy->getStanbyTime());
        $values['Battery Capacity'] = $autonomy->getBatteryCapacity();
        $values['Battery Type'] = $autonomy->getBatteryType();

        return $this->getResponse($autonomy->getName(), $values);
    }

    private function getInHours($param)
    {
        return $param ? sprintf('%s hours', $param) : null;
    }
}
