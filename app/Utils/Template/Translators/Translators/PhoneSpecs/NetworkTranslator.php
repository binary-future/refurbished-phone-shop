<?php

namespace App\Utils\Template\Translators\Translators\PhoneSpecs;

use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Domain\Phone\Specs\Network;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

class NetworkTranslator extends SpecsTranslator
{
    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var Network $specGroup
         */
        return $this->handle($specGroup);
    }

    /**
     * @param Network $network
     * @return Specs
     */
    private function handle(Network $network)
    {
        $values = [];
        $values['WIFI'] = $network->isWifi();
        $values['WIFI Type'] = $network->getWifiType();
        $values['WIFI Hotspot'] = $network->isWifiHotspot();
        $values['Bluetooth'] = $network->isBluetooth();
        $values['Bluetooth Type'] = $network->getBluetoothType();
        $values['USB'] = $network->isUsb();

        return $this->getResponse($network->getName(), $values);
    }
}
