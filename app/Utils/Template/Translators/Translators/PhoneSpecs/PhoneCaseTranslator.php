<?php

namespace App\Utils\Template\Translators\Translators\PhoneSpecs;

use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Domain\Phone\Specs\PhoneCase;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

final class PhoneCaseTranslator extends SpecsTranslator
{
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var PhoneCase $specGroup
         */
        return $this->handle($specGroup);
    }

    private function handle(PhoneCase $phoneCase)
    {
        $values = [];
        $values['Dimensions'] = $this->getDimensions($phoneCase);
        $values['Weight'] = $this->getWeight($phoneCase);


        return $this->getResponse($phoneCase->getName(), $values);
    }

    private function getDimensions(PhoneCase $phoneCase)
    {
        return $phoneCase->getHeight() && $phoneCase->getWidth() && $phoneCase->getThickness()
            ? sprintf('%s x %s x %s mm', $phoneCase->getHeight(), $phoneCase->getWidth(), $phoneCase->getThickness())
            : null;
    }

    private function getWeight(PhoneCase $phoneCase)
    {
        return $phoneCase->getWeight()
            ? sprintf('%s g', $phoneCase->getWeight())
            : null;
    }

}