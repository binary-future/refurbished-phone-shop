<?php


namespace App\Utils\Template\Translators\Translators\PhoneSpecs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Domain\Phone\Specs\Display;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

/**
 * Class DisplayTranslator
 * @package App\Utils\Template\Translators\Translators\PhoneSpecs
 */
final class DisplayTranslator extends SpecsTranslator
{
    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var Display $specGroup
         */
        return $this->handle($specGroup);
    }

    /**
     * @param Display $display
     * @return Specs
     */
    private function handle(Display $display)
    {
        $values = [];
        $values['Resolution'] = $display->getResolution();
        $values['Display Type'] = $display->getDisplayType();
        $values['Display Size'] = $display->getDisplaySize();

        return $this->getResponse($display->getName(), $values);
    }
}
