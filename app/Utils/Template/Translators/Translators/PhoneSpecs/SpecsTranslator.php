<?php

namespace App\Utils\Template\Translators\Translators\PhoneSpecs;

use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Presentation\Services\Translators\Contracts\Translator;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;
use App\Utils\Template\Translators\Exceptions\TranslationException;

/**
 * Class SpecsTranslator
 * @package App\Utils\Template\Translators\Translators\PhoneSpecs
 */
abstract class SpecsTranslator implements Translator
{
    /**
     * @param $data
     * @return Specs|mixed
     * @throws TranslationException
     */
    public function translate($data)
    {
        try {
            return $this->proceed($data);
        } catch (\Throwable $exception) {
            throw TranslationException::cannotProceedTranslation($exception->getMessage());
        }
    }

    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    abstract protected function proceed(SpecGroup $specGroup): Specs;

    /**
     * @param string $name
     * @param array $values
     * @return Specs
     */
    protected function getResponse(string $name, array $values): Specs
    {
        $values = array_filter($values, function ($item) {
            return ! is_null($item);
        });

        return new Specs($name, $values);
    }
}
