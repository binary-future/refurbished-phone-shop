<?php


namespace App\Utils\Template\Translators\Translators\PhoneSpecs;

use App\Domain\Phone\Specs\Connection;
use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Presentation\Services\Translators\PhoneSpecs\Specs;

final class ConnectionTranslator extends SpecsTranslator
{
    /**
     * @param SpecGroup $specGroup
     * @return Specs
     */
    protected function proceed(SpecGroup $specGroup): Specs
    {
        /**
         * @var Connection $specGroup
         */
        return $this->handle($specGroup);
    }

    /**
     * @param Connection $connection
     * @return Specs
     */
    private function handle(Connection $connection)
    {
        $values = [];
        $values['Connection Type'] = $connection->getConnectionType();
        $values['GPS'] = $connection->isGps();
        $values['GPS Type'] = $connection->getGpsType();
        $values['SIM Capacity'] = $connection->getSimCapacity();

        return $this->getResponse($connection->getName(), $values);
    }
}
