<?php


namespace App\Utils\Template\Translators\Exceptions;


/**
 * Class TranslationException
 * @package App\Utils\Template\Translators\Exceptions
 */
final class TranslationException extends \Exception
{
    /**
     * @param string $message
     * @return TranslationException
     */
    public static function cannotProceedTranslation(string $message)
    {
        return new self($message);
    }
}