<?php

namespace App\Utils\Template\Filters\Filters;

use App\Presentation\Services\Filters\Contracts\Filter;

final class CheckboxSection implements Filter
{
    /**
     * @var Checkbox[]
     */
    private $checkboxes;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $isEnabled;

    /**
     * RadioSection constructor.
     * @param Checkbox[] $checkboxes
     * @param string $name
     * @param bool $isEnabled
     */
    public function __construct(array $checkboxes, string $name, bool $isEnabled = true)
    {
        $this->checkboxes = $checkboxes;
        $this->name = $name;
        $this->isEnabled = $isEnabled;
    }


    public function getType(): string
    {
        return 'checkbox-section';
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @return Checkbox[]
     */
    public function getFilters(): array
    {
        return $this->checkboxes;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
