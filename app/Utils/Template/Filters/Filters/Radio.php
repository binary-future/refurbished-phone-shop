<?php

namespace App\Utils\Template\Filters\Filters;

use App\Presentation\Services\Filters\Contracts\Filter;

final class Radio implements Filter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * Radio constructor.
     * @param string $name
     * @param string|int $value
     * @param bool $enabled
     */
    public function __construct(string $name, $value, bool $enabled = true)
    {
        $this->name = $name;
        $this->value = $value;
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'radio';
    }

    public function isEnabled(): bool
    {
        return $this->value !== null && $this->enabled;
    }
}
