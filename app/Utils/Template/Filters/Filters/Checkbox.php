<?php

namespace App\Utils\Template\Filters\Filters;

use App\Presentation\Services\Filters\Contracts\Filter;

final class Checkbox implements Filter
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @var bool
     */
    private $enabled;
    /**
     * @var bool
     */
    private $checked;

    /**
     * Checkbox constructor.
     * @param string $name
     * @param mixed $value
     * @param bool $enabled
     * @param bool $checked
     */
    public function __construct(string $name, $value, bool $enabled = true, bool $checked = false)
    {
        if ($value === null) {
            throw new \InvalidArgumentException('Value must be NOT null');
        }

        $this->name = $name;
        $this->value = $value;
        $this->enabled = $enabled;
        $this->checked = $checked;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'checkbox';
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function isChecked(): bool
    {
        return $this->enabled && $this->checked;
    }
}
