<?php

namespace App\Utils\Template\Filters\Filters;

use App\Presentation\Services\Filters\Contracts\Filter;

final class Slider implements Filter
{
    public const OPTION_TICKS = 'ticks';
    public const OPTION_LOCK_ON_TICKS = 'lock-on-ticks';
    public const OPTION_UNLIMITED = 'unlimited';

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $minValue;

    /**
     * @var string
     */
    private $maxValue;

    /**
     * @var string
     */
    private $step;

    /**
     * @var bool
     */
    private $enabled;

    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $options;

    /**
     * Slider constructor.
     * @param string $name
     * @param string $value
     * @param string $minValue
     * @param string $maxValue
     * @param string $step
     * @param string $title
     * @param bool $enabled
     * @param array $options
     */
    public function __construct(
        string $name,
        string $value,
        string $minValue,
        string $maxValue,
        string $step,
        string $title,
        bool $enabled = true,
        array $options = []
    ) {
        $this->name = $name;
        $this->value = $value;
        $this->minValue = $minValue;
        $this->maxValue = $maxValue;
        $this->step = $step;
        $this->enabled = $enabled;
        $this->title = $title;
        $this->options = $options;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'slider';
    }

    public function isEnabled(): bool
    {
        return $this->value !== null && $this->enabled;
    }

    /**
     * @return string
     */
    public function getMinValue(): string
    {
        return $this->minValue;
    }

    /**
     * @return string
     */
    public function getMaxValue(): string
    {
        return $this->maxValue;
    }

    /**
     * @return string
     */
    public function getStep(): string
    {
        return $this->step;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTicks(): ?string
    {
        $ticks = $this->parseOption(self::OPTION_TICKS);
        return is_array($ticks) ? sprintf('[%s]', implode(', ', $ticks)) : $ticks;
    }

    public function isLockOnTicks(): bool
    {
        return (bool) $this->parseOption(self::OPTION_LOCK_ON_TICKS);
    }

    public function isUnlimited(): bool
    {
        return (bool) $this->parseOption(self::OPTION_UNLIMITED);
    }

    private function parseOption($key)
    {
        return $this->options[$key] ?? null;
    }
}
