<?php

namespace App\Utils\Template\Filters\Filters;

use App\Presentation\Services\Filters\Contracts\Filter;

final class RadioSection implements Filter
{
    /**
     * @var Radio[]
     */
    private $filters;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $isEnabled;

    /**
     * RadioSection constructor.
     * @param Radio[] $radios
     * @param string $name
     * @param bool $isEnabled
     */
    public function __construct(array $radios, string $name, bool $isEnabled = true)
    {
        $this->filters = $radios;
        $this->name = $name;
        $this->isEnabled = $isEnabled;
    }


    public function getType(): string
    {
        return 'radio-section';
    }

    public function isEnabled(): bool
    {
        return $this->isEnabled;
    }

    /**
     * @return Radio[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
