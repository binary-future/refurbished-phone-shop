<?php

namespace App\Utils\Template\Filters;

use App\Presentation\Services\Filters\Contracts\Generator;
use App\Presentation\Services\Filters\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Template\Filters\Exceptions\FilterGeneratorException;

abstract class BaseGenerator implements Generator
{
    private const CONFIG_PATH = 'templates.filters';
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var
     */
    private $filtersList;

    /**
     * @var
     */
    private $filtersMap;

    /**
     * BaseGenerator constructor.
     * @param Config $config
     * @param Serializer $serializer
     * @throws FilterGeneratorException
     */
    public function __construct(Config $config, Serializer $serializer)
    {
        $this->config = $config;
        $this->serializer = $serializer;
        $this->configure();
    }

    private function configure()
    {
        try {
            $configs = $this->config->get(self::CONFIG_PATH, []);
            $this->filtersList = $configs['filters'];
            $this->filtersMap = $configs['map'];
        } catch (\Throwable $exception) {
            throw FilterGeneratorException::cannotBuildGenerator($exception->getMessage());
        }
    }

    /**
     * @param Scenario $scenario
     * @return mixed|object
     * @throws FilterGeneratorException
     */
    public function generate(Scenario $scenario)
    {
        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw FilterGeneratorException::cannotGenerateFiltersByScenario($scenario, $exception->getMessage());
        }
    }

    /**
     * @param Scenario $scenario
     * @return mixed
     */
    abstract protected function proceed(Scenario $scenario);

    /**
     * @param string $response
     * @param array $data
     * @return object
     */
    protected function generateResponse(string $response, array $data): object
    {
        return $this->serializer->fromArray($response, $data);
    }

    /**
     * @param array $group
     * @param string $name
     * @param string $type
     * @param array $enabled
     * @return array
     */
    protected function generateFiltersGroup(array $group, string $name, string $type, array $enabled = [])
    {
        $filters = [];
        foreach ($group as $value) {
            $filters[] = $this->generateFilter(
                $type,
                [
                    'name' => $name,
                    'value' => $value,
                    'enabled' => empty($enabled) || in_array($value, $enabled)
                ]
            );
        }

        return $filters;
    }

    /**
     * @param string $type
     * @param array $data
     * @return object
     */
    protected function generateFilter(string $type, array $data): object
    {
        $className = $this->filtersMap[$type];

        return $this->serializer->fromArray($className, $data);
    }

    /**
     * @param Scenario $scenario
     * @return mixed
     */
    protected function getFilters(Scenario $scenario)
    {
        return $this->filtersList[$scenario->getContext()];
    }
}
