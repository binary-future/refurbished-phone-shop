<?php


namespace App\Utils\Template\Filters;

use App\Presentation\Services\Filters\Contracts\Generator as Contract;
use App\Presentation\Services\Filters\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Template\Filters\Exceptions\FilterGeneratorException;

/**
 * Class Generator
 * @package App\Utils\Template\Filters
 */
class Generator implements Contract
{
    private const CONFIG_PATH = 'templates.filters.schema';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var array|mixed
     */
    private $schema = [];

    /**
     * Generator constructor.
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->schema = $config->get(self::CONFIG_PATH, []);
    }


    /**
     * @param Scenario $scenario
     * @return mixed
     * @throws FilterGeneratorException
     */
    public function generate(Scenario $scenario)
    {
        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw FilterGeneratorException::cannotGenerateFiltersByScenario($scenario, $exception->getMessage());
        }
    }

    /**
     * @param Scenario $scenario
     * @return mixed
     */
    private function proceed(Scenario $scenario)
    {
        $generator = $this->getGenerator($this->getGeneratorName($scenario->getContext()));

        return $generator->generate($scenario);
    }

    /**
     * @param string $context
     * @return mixed
     */
    private function getGeneratorName(string $context)
    {
        return $this->schema[$context];
    }

    /**
     * @param string $classname
     * @return Contract
     */
    private function getGenerator(string $classname): Contract
    {
        return $this->container->resolve($classname);
    }
}
