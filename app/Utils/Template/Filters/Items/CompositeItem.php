<?php


namespace App\Utils\Template\Filters\Items;


use App\Utils\Template\Filters\Contracts\Composite;
use App\Utils\Template\Filters\Contracts\ItemGenerator;

/**
 * Class CompositeItem
 * @package App\Utils\Template\Filters\Items
 */
final class CompositeItem implements Composite
{
    /**
     * @var ItemGenerator[]
     */
    private $generators = [];

    /**
     * @param ItemGenerator $generator
     * @return mixed|void
     */
    public function addItem(ItemGenerator $generator)
    {
        $this->generators[] = $generator;
    }

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    public function generate($data, array $options = [])
    {
        foreach ($this->generators as $generator) {
            $data = $generator->generate($data, $options);
        }

        return $data;
    }
}
