<?php


namespace App\Utils\Template\Filters\Items;


use App\Utils\Template\Filters\Contracts\ItemGenerator;
use App\Utils\Template\Filters\Exceptions\FilterGeneratorException;

/**
 * Class BaseItem
 * @package App\Utils\Template\Filters\Items
 */
abstract class BaseItem implements ItemGenerator
{
    /**
     * @param $data
     * @param array $options
     * @return mixed
     * @throws FilterGeneratorException
     */
    public function generate($data, array $options = [])
    {
        try {
            if ($this->isSatisfy($data, $options)) {
                return $this->proceed($data, $options);
            }

            throw FilterGeneratorException::cannotGenerateFilter(
                sprintf('Invalid data for generating (%s)', self::class)
            );
        } catch (\Throwable $exception) {
            throw FilterGeneratorException::cannotGenerateFilter($exception->getMessage());
        }
    }

    /**
     * @param $data
     * @param array $options
     * @return bool
     */
    abstract protected function isSatisfy($data, array $options): bool;

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    abstract protected function proceed($data, array $options);

    /**
     * @return string
     */
    abstract protected function filterName(): string;
}
