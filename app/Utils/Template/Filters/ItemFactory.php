<?php

namespace App\Utils\Template\Filters;

use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Template\Filters\Contracts\Composite;
use App\Utils\Template\Filters\Contracts\ItemGenerator;
use App\Utils\Template\Filters\Exceptions\FilterGeneratorException;

/**
 * Class ItemFactory
 * @package App\Utils\Template\Filters
 */
class ItemFactory
{
    private const CONFIG_PATH = 'templates.filters.items';

    /**
     * @var array
     */
    private $configs = [];

    /**
     * @var Container
     */
    private $container;

    /**
     * ItemFactory constructor.
     * @param Config $config
     * @param Container $container
     */
    public function __construct(Config $config, Container $container)
    {
        $this->configs = $config->get(self::CONFIG_PATH, []);
        $this->container = $container;
    }

    /**
     * @param string $alias
     * @return ItemGenerator
     * @throws FilterGeneratorException
     */
    public function buildGenerator(string $alias): ItemGenerator
    {
        try {
            return $this->proceed($alias);
        } catch (\Throwable $exception) {
            throw FilterGeneratorException::cannotBuildGenerator($exception->getMessage());
        }
    }

    /**
     * @param string $alias
     * @return ItemGenerator
     */
    private function proceed(string $alias): ItemGenerator
    {
        $generator = $this->getInstance($this->configs[$alias]['base']);

        return $this->isFillable($generator, $alias)
            ? $this->fillGenerator($generator, $this->configs[$alias]['items'])
            : $generator;
    }

    /**
     * @param string $className
     * @return ItemGenerator
     */
    private function getInstance(string $className): ItemGenerator
    {
        return $this->container->resolve($className);
    }

    /**
     * @param ItemGenerator $generator
     * @param string $alias
     * @return bool
     */
    private function isFillable(ItemGenerator $generator, string $alias): bool
    {
        return $generator instanceof Composite && isset($this->configs[$alias]['items']);
    }

    /**
     * @param Composite $generator
     * @param array $items
     * @return ItemGenerator
     */
    private function fillGenerator(Composite $generator, array $items): ItemGenerator
    {
        foreach ($items as $item) {
            $generator->addItem($this->getInstance($item));
        }

        return $generator;
    }
}
