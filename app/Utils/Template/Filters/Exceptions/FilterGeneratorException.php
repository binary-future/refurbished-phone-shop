<?php


namespace App\Utils\Template\Filters\Exceptions;

use App\Presentation\Services\Filters\Contracts\Scenario;

/**
 * Class FilterGeneratorException
 * @package App\Utils\Template\Filters\Exceptions
 */
final class FilterGeneratorException extends \Exception
{
    /**
     * @param Scenario $scenario
     * @param string $message
     * @return FilterGeneratorException
     */
    public static function cannotGenerateFiltersByScenario(Scenario $scenario, string $message)
    {
        return new self(
            sprintf(
                'Cannot generate filters by given scenario - %s. %s',
                get_class($scenario),
                $message
            )
        );
    }

    /**
     * @param string $error
     * @return FilterGeneratorException
     */
    public static function cannotBuildGenerator(string $error)
    {
        return new self($error);
    }

    /**
     * @param string $error
     * @return FilterGeneratorException
     */
    public static function cannotGenerateFilter(string $error)
    {
        return new self(sprintf('Cannot generate filter for given data. Reason - %s', $error));
    }
}
