<?php


namespace App\Utils\Template\Filters\Contracts;

use App\Utils\Template\Filters\Exceptions\FilterGeneratorException;

/**
 * Interface ItemGenerator
 * @package App\Presentation\Services\Filters\Contracts
 */
interface ItemGenerator
{
    /**
     * @param $data
     * @param array $options
     * @throws FilterGeneratorException
     */
    public function generate($data, array $options = []);
}
