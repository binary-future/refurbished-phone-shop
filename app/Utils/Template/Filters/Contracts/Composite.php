<?php

namespace App\Utils\Template\Filters\Contracts;

/**
 * Interface Composite
 * @package App\Presentation\Services\Filters\Contracts
 */
interface Composite extends ItemGenerator
{
    /**
     * @param ItemGenerator $generator
     * @return mixed
     */
    public function addItem(ItemGenerator $generator);
}