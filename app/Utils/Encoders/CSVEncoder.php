<?php


namespace App\Utils\Encoders;

use App\Utils\Encoders\Contracts\CSVEncoder as Contract;

/**
 * Class CSVEncoder
 * @package App\Utils\Encoders
 */
final class CSVEncoder implements Contract
{
    /**
     * @var
     */
    private $file;

    /**
     * @param string $path
     * @return
     * @throws \Exception
     */
    public function decode(string $path): iterable
    {
        $this->file = fopen($path, 'r');

        return $this->parse();
    }

    /**
     * @return \Generator|void
     */
    private function parse()
    {
        $headers = array_map('trim', fgetcsv($this->file, 4096));
        while (!feof($this->file)) {
            $row = array_map('trim', (array)fgetcsv($this->file, 4096));
            if (count($headers) !== count($row)) {
                continue;
            }
            $row = array_combine($headers, $row);
            yield $row;
        }
        return;
    }

    public function __destruct()
    {
        if ($this->file) {
            fclose($this->file);
        }
    }
}
