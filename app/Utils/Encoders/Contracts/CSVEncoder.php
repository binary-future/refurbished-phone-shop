<?php


namespace App\Utils\Encoders\Contracts;


interface CSVEncoder
{
    /**
     * @param string $path
     * @return iterable
     */
    public function decode(string $path): iterable;
}
