<?php


namespace App\Utils\Sitemaps\Contracts;


use Carbon\Carbon;

/**
 * Interface Sitemaps
 * @package App\Utils\Sitemaps\Contracts
 */
interface Sitemaps
{
    /**
     * @return mixed
     */
    public function render();

    /**
     * @param string $route
     * @param Carbon $date
     * @param string $period
     * @param string $priority
     * @return mixed
     */
    public function addTag(string $route, Carbon $date, string $period, string $priority);
}
