<?php


namespace App\Utils\Sitemaps;

use App\Utils\Sitemaps\Contracts\Sitemaps as Contract;
use Carbon\Carbon;
use Watson\Sitemap\Facades\Sitemap;

/**
 * Class Sitemaps
 * @package App\Utils\Sitemaps
 */
final class Sitemaps implements Contract
{
    /**
     * @return mixed
     */
    public function render()
    {
        return Sitemap::render();
    }

    /**
     * @param string $route
     * @param Carbon $date
     * @param string $period
     * @param string $priority
     * @return mixed
     */
    public function addTag(string $route, Carbon $date, string $period, string $priority)
    {
        return Sitemap::addTag($route, $date, $period, $priority);
    }
}
