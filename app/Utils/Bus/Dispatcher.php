<?php

namespace App\Utils\Bus;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Bus\Exceptions\BusDispatcherException;

/**
 * Class Dispatcher
 * @package App\Utils\Bus
 */
class Dispatcher
{
    private const HANDLER_SUFIX = 'Handler';

    /**
     * @var Container
     */
    private $container;

    /**
     * Dispatcher constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param object $object
     * @return object|null
     * @throws BusDispatcherException
     */
    public function dispatch(object $object): ?object
    {
        try {
            return $this->proceed($object);
        } catch (\Throwable $exception) {
            throw BusDispatcherException::cannotDispatchObject($object, $exception);
        }
    }

    /**
     * @param object $object
     * @return mixed
     */
    private function proceed(object $object)
    {
        $handlerName = $this->generateHandlerName($object);
        $handler = $this->getInstance($handlerName);

        return $handler->handle($object);
    }

    /**
     * @param object $object
     * @return string
     */
    private function generateHandlerName(object $object)
    {
        return sprintf('%s%s', get_class($object), self::HANDLER_SUFIX);
    }

    /**
     * @param string $className
     * @return Handler
     */
    private function getInstance(string $className): Handler
    {
        return $this->container->resolve($className);
    }
}
