<?php

namespace App\Utils\Bus;

use App\Application\Services\Bus\QueryBus as Contract;
use App\Utils\Bus\Exceptions\BusDispatcherException;

final class QueryBus implements Contract
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * QueryBus constructor.
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param object $query
     * @return object
     * @throws BusDispatcherException
     */
    public function dispatch(object $query): object
    {
        $response = $this->dispatcher->dispatch($query);
        if (! is_object($response)) {
            throw BusDispatcherException::shouldObjectReturn();
        }

        return $response;
    }
}
