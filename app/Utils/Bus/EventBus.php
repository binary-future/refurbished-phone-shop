<?php

namespace App\Utils\Bus;

use App\Utils\Adapters\Event\Contracts\Event;
use App\Application\Services\Bus\EventBus as Bus;
use App\Utils\Bus\Exceptions\BusDispatcherException;

/**
 * Class EventBus
 * @package App\Utils\Adapters\Event
 */
final class EventBus implements Event, Bus
{
    /**
     * @param object $event
     * @return mixed|void
     */
    public function dispatch(object $event)
    {
        try {
            event($event);
        } catch (\Throwable $exception) {
            BusDispatcherException::cannotDispatchObject($event, $exception->getMessage());
        }
    }
}
