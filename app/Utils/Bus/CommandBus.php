<?php

namespace App\Utils\Bus;

use App\Application\Services\Bus\CommandBus as Contract;

final class CommandBus implements Contract
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * CommandBus constructor.
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param object $command
     * @return object|null
     */
    public function dispatch(object $command): ?object
    {
        return $this->dispatcher->dispatch($command);
    }
}
