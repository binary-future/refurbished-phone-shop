<?php


namespace App\Utils\File\Download\Exception;

/**
 * Class FileDownloaderException
 * @package App\Utils\File\Download\Exception
 */
final class FileDownloaderException extends \Exception
{
    /**
     * @param string $link
     * @param string $message
     * @return FileDownloaderException
     */
    public static function cannotDownloadFile(string $message, string $link = null)
    {
        return new self(
            sprintf(
                'Cannot download file %s. %s',
                $message,
                $link ? 'by given link - ' . $link : ''
            )
        );
    }
}
