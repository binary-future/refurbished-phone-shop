<?php


namespace App\Utils\File\Download\Contract;


/**
 * Interface FileDownloader
 * @package App\Utils\File\Download\Contract
 */
interface FileDownloader
{
    /**
     * @param string $link
     * @param string $path
     * @return bool
     */
    public function download(string $link, string $path): bool;
}
