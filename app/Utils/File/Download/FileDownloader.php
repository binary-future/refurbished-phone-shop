<?php


namespace App\Utils\File\Download;

use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\File\Download\Contract\FileDownloader as Contract;
use App\Utils\File\Download\Exception\FileDownloaderException;
use App\Utils\Http\Contracts\ContentGetter;

final class FileDownloader implements Contract
{
    /**
     * @var ContentGetter
     */
    private $contentGetter;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * FileDownloader constructor.
     * @param ContentGetter $contentGetter
     * @param Storage $storage
     */
    public function __construct(ContentGetter $contentGetter, Storage $storage)
    {
        $this->contentGetter = $contentGetter;
        $this->storage = $storage;
    }

    /**
     * @param string $link
     * @param string $path
     * @return bool
     * @throws FileDownloaderException
     */
    public function download(string $link, string $path): bool
    {
        try {
            return $this->proceed($link, $path);
        } catch (\Throwable $exception) {
            throw FileDownloaderException::cannotDownloadFile($exception->getMessage(), $link);
        }
    }

    private function proceed(string $link, string $path): bool
    {
        $content = $this->contentGetter->getContent($link);
        if (! $this->isValidContent($content)) {
            throw new \Exception('Invalid file content');
        }

        return (bool) $this->storage->put($path, $content);
    }

    private function isValidContent(string $content): bool
    {
        return $content && stripos($content, 'DOCTYPE html') === false;
    }
}
