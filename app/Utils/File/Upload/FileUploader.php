<?php

namespace App\Utils\File\Upload;

use App\Utils\File\Upload\Contract\FileUploader as Contract;
use App\Utils\File\Upload\Exception\FileUploadException;
use Illuminate\Http\UploadedFile;

/**
 * Class FileUploader
 * @package App\Utils\File\Upload
 */
final class FileUploader implements Contract
{
    /**
     * @param UploadedFile $file
     * @param string $path
     * @param string $name
     * @return string
     * @throws FileUploadException
     */
    public function store(UploadedFile $file, string $path, string $name): string
    {
        try {
            return $file->storeAs($path, $name);
        } catch (\Throwable $exception) {
            throw FileUploadException::cannotUploadFile($exception->getMessage());
        }
    }
}
