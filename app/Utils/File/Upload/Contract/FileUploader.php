<?php


namespace App\Utils\File\Upload\Contract;

use Illuminate\Http\UploadedFile;

interface FileUploader
{
    /**
     * @param UploadedFile $file
     * @param string $path
     * @param string $name
     * @return string
     */
    public function store(UploadedFile $file, string $path, string $name): string;
}