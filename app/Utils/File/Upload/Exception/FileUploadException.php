<?php


namespace App\Utils\File\Upload\Exception;


/**
 * Class FileUploadException
 * @package App\Utils\File\Upload\Exception
 */
final class FileUploadException extends \Exception
{
    /**
     * @param string $error
     * @return FileUploadException
     */
    public static function cannotUploadFile(string $error)
    {
        return new self(sprintf('Cannot upload file. %s', $error));
    }
}
