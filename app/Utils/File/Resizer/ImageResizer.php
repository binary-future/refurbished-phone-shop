<?php

namespace App\Utils\File\Resizer;

use App\Utils\File\Resizer\Contract\ImageResizer as Contract;
use App\Utils\File\Resizer\Exception\ImageResizerException;
use Intervention\Image\Facades\Image as Facade;
use Intervention\Image\Image;


final class ImageResizer implements Contract
{
    public function resize(Resize $request): string
    {
        $image = $this->getImage($request->getPath());
        $width = $this->shouldRecalculateWidth($image, $request)
            ? $this->calculateWidth($image, $request)
            : $request->getWidth();
        $height = $request->getHeigth();
        $path = $this->generatePath($request->getPath(), $height, $width);
        $image->resize($width, $height)->save($path);

        return $path;
    }

    private function getImage(string $path)
    {
        try {
            return Facade::make($path);
        } catch (\Throwable $exception) {
            throw ImageResizerException::imageIsAbsent($path);
        }
    }

    private function shouldRecalculateWidth(Image $image, Resize $resize): bool
    {
        return ! $resize->isForce()
            && $image->getWidth() / $image->getHeight() !== $resize->getWidth() / $resize->getHeigth();
    }

    private function calculateWidth(Image $image, Resize $resize): int
    {
        $width = (int) round($resize->getHeigth() * $image->getWidth() / $image->getHeight());

        return $width;
    }

    private function generatePath(string $path, int $height, int $width): string
    {
        $imageFullName = basename($path);
        $nameParts = explode('.', $imageFullName);
        $name = array_shift($nameParts);
        $name = sprintf('%s_%sx%s', $name, $height, $width);
        $novelFullName = implode('.', array_merge([$name], $nameParts));

        return str_ireplace($imageFullName, $novelFullName, $path);
    }
}
