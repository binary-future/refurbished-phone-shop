<?php

namespace App\Utils\File\Resizer;

/**
 * Class Resize
 * @package App\Utils\File\Resizer
 */
final class Resize
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var int
     */
    private $heigth;

    /**
     * @var int
     */
    private $width;

    /**
     * @var bool
     */
    private $force = false;

    /**
     * Resize constructor.
     * @param string $path
     * @param int $heigth
     * @param int $width
     * @param bool $force
     */
    public function __construct(string $path, int $heigth, int $width, bool $force = false)
    {
        $this->path = $path;
        $this->heigth = $heigth;
        $this->width = $width;
        $this->force = $force;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getHeigth(): int
    {
        return $this->heigth;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return bool
     */
    public function isForce(): bool
    {
        return $this->force;
    }
}
