<?php


namespace App\Utils\File\Resizer\Contract;


use App\Utils\File\Resizer\Resize;

/**
 * Interface ImageResizer
 * @package App\Utils\File\Resizer\Contract
 */
interface ImageResizer
{
    /**
     * @param Resize $request
     * @return string
     */
    public function resize(Resize $request): string;
}
