<?php


namespace App\Utils\File\Resizer\Exception;


/**
 * Class ImageResizerException
 * @package App\Utils\File\Resizer\Exception
 */
final class ImageResizerException extends \Exception
{
    /**
     * @param string $path
     * @return ImageResizerException
     */
    public static function imageIsAbsent(string $path)
    {
        return new self(sprintf('Cannot find image for resizing by given path - %s', $path));
    }
}
