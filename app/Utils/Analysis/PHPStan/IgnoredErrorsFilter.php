<?php

declare(strict_types=1);

namespace App\Utils\Analysis\PHPStan;

use PHPStan\Command\AnalysisResult;
use PHPStan\Command\ErrorFormatter\TableErrorFormatter;
use PHPStan\File\RelativePathHelper;
use Symfony\Component\Console\Style\OutputStyle;

class IgnoredErrorsFilter extends TableErrorFormatter
{
    private const MUTE_ERROR_ANNOTATION = 'phpstan:ignoreError';
    private const COMMENT_SYMBOLS = '//';

    private const NO_ERRORS = 0;

    public function __construct(
        RelativePathHelper $relativePathHelper,
        bool $showTipsOfTheDay = false,
        bool $checkThisOnly = false,
        bool $inferPrivatePropertyTypeFromConstructor = true
    ) {
        parent::__construct(
            $relativePathHelper,
            $showTipsOfTheDay,
            $checkThisOnly,
            $inferPrivatePropertyTypeFromConstructor
        );
    }

    /**
   * {@inheritdoc}
   */
    public function formatErrors(AnalysisResult $analysisResult, OutputStyle $outputStyle): int
    {
        if (!$analysisResult->hasErrors()) {
            return self::NO_ERRORS;
        }

        $fileSpecificErrorsWithoutIgnoredErrors = $this->clearIgnoredErrors(
            $analysisResult->getFileSpecificErrors()
        );

        $clearedAnalysisResult = new AnalysisResult(
            $fileSpecificErrorsWithoutIgnoredErrors,
            $analysisResult->getNotFileSpecificErrors(),
            $analysisResult->isDefaultLevelUsed(),
            $analysisResult->getCurrentDirectory(),
            $analysisResult->hasInferrablePropertyTypesFromConstructor(),
            $analysisResult->getProjectConfigFile()
        );

        return parent::formatErrors($clearedAnalysisResult, $outputStyle);
    }

    private function clearIgnoredErrors(array $fileSpecificErrors): array
    {
        foreach ($fileSpecificErrors as $index => $error) {
            $comments = $this->sanitizeSpaces(
                $this->getComments($error->getFile(), $error->getLine() - 2)
            );
            $errorMessage = $this->sanitizeSpaces($error->getMessage());

            $lineContainAnnotation = strpos($comments, self::MUTE_ERROR_ANNOTATION);
            $lineContainErrorDescription = strpos($comments, $errorMessage);

            if (false !== $lineContainAnnotation && false !== $lineContainErrorDescription) {
                unset($fileSpecificErrors[$index]);
            }
        }

        return $fileSpecificErrors;
    }

    private function sanitizeSpaces(string $string): string
    {
        return str_replace(' ', '', $string);
    }

    private function getComments(string $filename, int $errorLineNumber): string
    {
        $file = new \SplFileObject($filename);
        $lineNumber = $errorLineNumber;

        $lines = '';

        while (true) {
            $line = trim($this->getLine($file, $lineNumber));

            // if it's not a single-line comment
            if (strpos($line, self::COMMENT_SYMBOLS) !== 0) {
                break;
            }

            // append to beginning, because we go up every line
            $lines = trim($line, self::COMMENT_SYMBOLS . ' ') . $lines;
            --$lineNumber;
        }

        return $lines;
    }

    /**
     * @param \SplFileObject $file
     * @param int $lineNumber
     * @return array|false|string
     */
    private function getLine(\SplFileObject $file, int $lineNumber)
    {
        // get the line above to the line that caused the error
        $file->seek($lineNumber);

        return $file->current();
    }
}
