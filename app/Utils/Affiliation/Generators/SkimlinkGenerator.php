<?php


namespace App\Utils\Affiliation\Generators;


use App\Utils\Affiliation\Contracts\AffiliationGenerator;
use App\Utils\Affiliation\Contracts\Configurable;

final class SkimlinkGenerator implements AffiliationGenerator, Configurable
{
    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $id;

    /**
     * @param array $params
     * @return void
     */
    public function configure(array $params)
    {
        $this->domain = $params['domain'];
        $this->key = $params['key'];
        $this->id = $params['id'];
    }

    public function generate(string $link, string $referrer = null): string
    {
        return sprintf(
            "%s/?id=%s&url=%s&sref=%s&xcust=%s",
            $this->domain,
            $this->key,
            urlencode($link),
            $referrer,
            $this->id
        );
    }
}