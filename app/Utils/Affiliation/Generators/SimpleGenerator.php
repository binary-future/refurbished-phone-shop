<?php


namespace App\Utils\Affiliation\Generators;

use App\Utils\Affiliation\Contracts\AffiliationGenerator;

/**
 * Class SimpleGenerator
 * @package App\Utils\Affiliation\Generators
 */
final class SimpleGenerator implements AffiliationGenerator
{
    /**
     * @param string $link
     * @param string|null $referrer
     * @return string
     */
    public function generate(string $link, string $referrer = null): string
    {
        return $link;
    }
}
