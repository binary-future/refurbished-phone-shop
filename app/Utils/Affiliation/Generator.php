<?php

namespace App\Utils\Affiliation;

use App\Application\Services\Affiliation\Contracts\Generator as Contract;
use App\Domain\Shared\Contracts\HasLinkWithAffiliation;
use App\Utils\Affiliation\Exceptions\AffiliationException;

/**
 * Class Generator
 * @package App\Utils\Affiliation
 */
final class Generator implements Contract
{
    /**
     * @var Factory
     */
    private $factory;

    /**
     * Generator constructor.
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param HasLinkWithAffiliation $owner
     * @param string|null $referrer
     * @return string
     * @throws AffiliationException
     */
    public function generate(HasLinkWithAffiliation $owner, string $referrer = null): string
    {
        if (! $owner->getLink()) {
            throw AffiliationException::noLinkGiven($owner);
        }

        return $this->generateLink($owner, $referrer);
    }

    /**
     * @param HasLinkWithAffiliation $owner
     * @param string|null $referrer
     * @return string
     * @throws AffiliationException
     */
    private function generateLink(HasLinkWithAffiliation $owner, string $referrer = null): string
    {
        $link = $owner->getLink()->getLink();
        if (! $owner->getAffiliation()) {
            return $link;
        }
        $generator = $this->factory->buildGenerator($owner->getAffiliation());

        return $generator->generate($link, $referrer);
    }
}
