<?php


namespace App\Utils\Affiliation\Contracts;


/**
 * Interface Configurable
 * @package App\Utils\Affiliation\Contracts
 */
interface Configurable
{
    /**
     * @param $params
     * @return mixed
     */
    public function configure(array $params);
}
