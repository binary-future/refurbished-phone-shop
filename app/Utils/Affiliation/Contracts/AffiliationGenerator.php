<?php

namespace App\Utils\Affiliation\Contracts;

/**
 * Interface AffiliationGenerator
 * @package App\Utils\Affiliation\Contracts
 */
interface AffiliationGenerator
{
    /**
     * @param string $link
     * @param string|null $referrer
     * @return string
     */
    public function generate(string $link, string $referrer = null): string;
}