<?php

namespace App\Utils\Affiliation\Exceptions;

use App\Domain\Shared\Contracts\HasLinkWithAffiliation;
use App\Domain\Store\Affiliation\Affiliation;

/**
 * Class AffiliationException
 * @package App\Utils\Affiliation\Exceptions
 */
final class AffiliationException extends \Exception
{
    /**
     * @param Affiliation $affiliation
     * @param string|null $error
     * @return AffiliationException
     */
    public static function cannotBuildAffiliationGenerator(
        Affiliation $affiliation,
        string $error = null
    ): AffiliationException {
        return new self(
            trim(sprintf('Cannot build generator for %s. %s', $affiliation->getType(), $error ?: ''))
        );
    }

    public static function noLinkGiven(HasLinkWithAffiliation $owner): AffiliationException
    {
        return new self(sprintf('%s has no link for affiliation', get_class($owner)));
    }
}
