<?php

namespace App\Utils\Affiliation;

use App\Domain\Store\Affiliation\Affiliation;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Affiliation\Contracts\AffiliationGenerator;
use App\Utils\Affiliation\Contracts\Configurable;
use App\Utils\Affiliation\Exceptions\AffiliationException;

/**
 * Class Factory
 * @package App\Utils\Affiliation
 */
class Factory
{
    private const CONFIG_PATH = 'affiliation.networks';
    private const GENERATORS_CONFIGS_PATH = 'affiliation.credentials';
    public const DEFAULT_GENERATOR = 'default';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var array
     */
    private $map = [];

    /**
     * Factory constructor.
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->config = $config;
        $this->configure();
    }

    private function configure()
    {
        $this->map = $this->config->get(self::CONFIG_PATH, []);
    }


    /**
     * @param Affiliation $affiliation
     * @return AffiliationGenerator
     * @throws AffiliationException
     */
    public function buildGenerator(Affiliation $affiliation): AffiliationGenerator
    {
        try {
            return $this->proceed($affiliation);
        } catch (\Throwable $exception) {
            throw AffiliationException::cannotBuildAffiliationGenerator($affiliation, $exception->getMessage());
        }
    }

    /**
     * @param Affiliation $affiliation
     * @return AffiliationGenerator
     */
    private function proceed(Affiliation $affiliation): AffiliationGenerator
    {
        $generatorName = $this->getGeneratorName($affiliation->getType());
        $generator = $this->getGeneratorInstance($generatorName);

        return $generator instanceof Configurable
            ? $this->configureGenerator($generator)
            : $generator;
    }

    /**
     * @param string $type
     * @return string
     */
    private function getGeneratorName(string $type): string
    {
        return $this->map['networks'][$type] ?? $this->map[self::DEFAULT_GENERATOR];
    }

    /**
     * @param string $generator
     * @return AffiliationGenerator
     */
    private function getGeneratorInstance(string $generator): AffiliationGenerator
    {
        return $this->container->resolve($generator);
    }

    private function configureGenerator(Configurable $configurable)
    {
        $configurable->configure($this->getConfigs($configurable));
        /**
         * @var AffiliationGenerator $configurable
         */

        return $configurable;
    }

    private function getConfigs(Configurable $generator): array
    {
        return $this->config->get(
            sprintf('%s.%s', self::GENERATORS_CONFIGS_PATH, get_class($generator)),
            []
        );
    }
}
