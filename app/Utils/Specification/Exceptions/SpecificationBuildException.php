<?php

namespace App\Utils\Specification\Exceptions;

/**
 * Class SpecificationBuildException
 * @package App\Utils\Specification\Exceptions
 */
final class SpecificationBuildException extends \Exception
{
    /**
     * @param string $alias
     * @param null $error
     * @return SpecificationBuildException
     */
    public static function cannotBuildSpecification(string $alias, $error = null)
    {
        return new self(
            sprintf('Cannot build specification by given alias %s. %s', $alias, $error ?: '')
        );
    }
}
