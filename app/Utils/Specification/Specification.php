<?php

namespace App\Utils\Specification;

use App\Utils\Specification\Contracts\SpecificationComposite;
use App\Utils\Specification\Contracts\Specification as Rule;

/**
 * Class Specification
 * @package App\Utils\Specification
 */
final class Specification implements SpecificationComposite
{
    /**
     * @var Rule[]
     */
    private $specifications = [];

    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        if (empty($this->specifications)) {
            return false;
        }

        foreach ($this->specifications as $specification) {
            if (! $specification->isSatisfy($value, $params)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Rule $specification
     * @return mixed|void
     */
    public function addSpecification(Rule $specification)
    {
        $this->specifications[] = $specification;
    }
}
