<?php

namespace App\Utils\Specification\Contracts;

/**
 * Interface Specification
 * @package App\Utils\Specification\Contracts
 */
interface Specification
{
    /**
     * @param $value
     * @param $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool;
}
