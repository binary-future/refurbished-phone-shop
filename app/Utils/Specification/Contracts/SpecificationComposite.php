<?php

namespace App\Utils\Specification\Contracts;

/**
 * Interface SpecificationComposite
 * @package App\Utils\Specification\Contracts
 */
interface SpecificationComposite extends Specification
{
    /**
     * @param Specification $specification
     * @return mixed
     */
    public function addSpecification(Specification $specification);
}
