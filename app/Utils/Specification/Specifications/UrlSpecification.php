<?php

namespace App\Utils\Specification\Specifications;

use App\Utils\Specification\Contracts\Specification;

final class UrlSpecification implements Specification
{
    /**
     * @param $value
     * @param array $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        if (! is_string($value)) {
            return false;
        }
        $result = preg_match(
            '#\b(((\S+)?)(@|mailto\:|(news|(ht|f)tp(s?))\://)\S+)\b#',
            $value,
            $matches
        );

        return $result && ! empty($matches);
    }
}
