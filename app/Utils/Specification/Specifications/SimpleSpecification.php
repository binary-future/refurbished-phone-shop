<?php

namespace App\Utils\Specification\Specifications;

use App\Utils\Specification\Contracts\Specification;

/**
 * Class SimpleSpecification
 * @package App\Utils\Specification\Specifications
 */
final class SimpleSpecification implements Specification
{
    /**
     * @param $value
     * @param $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        return true;
    }
}
