<?php

namespace App\Utils\Specification\Specifications;

use App\Utils\Specification\Contracts\Specification;

/**
 * Class RequiredFieldsSpecification
 * @package App\Utils\Specification\Specifications
 */
final class RequiredFieldsSpecification implements Specification
{
    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        try {
            if (! $params || empty($params)) {
                return false;
            }
            return $this->proceed($value, $params);
        } catch (\Throwable $e) {
            return false;
        }
    }

    /**
     * @param $value
     * @param $params
     * @return bool
     */
    private function proceed($value, $params)
    {
        foreach ($params as $field => $param) {
            if (is_array($param)) {
                if (! $this->proceed($value[$field], $param)) {
                    return false;
                }
            } elseif (! $this->isFieldPresent($value, $param)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $value
     * @param $field
     * @return bool
     */
    private function isFieldPresent($value, $field)
    {
        $result = !is_array($field) && $value[$field] !== null;
        if (! $result) {
            return false;
        }
        if (is_array($value[$field])) {
            return ! empty($value[$field]);
        }

        return true;
    }
}
