<?php

namespace App\Utils\Specification;

use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Specification\Contracts\SpecificationComposite;
use App\Utils\Specification\Exceptions\SpecificationBuildException;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class Factory
 * @package App\Utils\Specification
 */
class Factory
{
    private const CONFIG_PATH = 'specification.schema';
    public const DEFAULT = 'default';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var []
     */
    private $schema;

    /**
     * Factory constructor.
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->config = $config;
        $this->configure();
    }

    /**
     *
     */
    private function configure()
    {
        $this->schema = $this->config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param string $alias
     * @return Specification
     * @throws SpecificationBuildException
     */
    public function buildSpecification(string $alias): Specification
    {
        try {
            return $this->proceedBuilding($alias);
        } catch (\Throwable $exception) {
            throw SpecificationBuildException::cannotBuildSpecification($alias, $exception->getMessage());
        }
    }

    /**
     * @param string $alias
     * @return Specification
     */
    private function proceedBuilding(string $alias): Specification
    {
        $schema = $this->getSchemaByAlias($alias);
        $specification = $this->getInstance($schema['base']);

        return $this->shouldBeFilled($specification, $schema)
            /**
             * @var SpecificationComposite $specification
             */
            ? $this->fillSpecification($specification, $schema['items'])
            : $specification;
    }

    /**
     * @param Specification $specification
     * @param array $schema
     * @return bool
     */
    private function shouldBeFilled(Specification $specification, array $schema)
    {
        return $specification instanceof SpecificationComposite
            && isset($schema['items']);
    }

    /**
     * @param SpecificationComposite $composite
     * @param array $items
     * @return SpecificationComposite
     */
    private function fillSpecification(SpecificationComposite $composite, array $items)
    {
        foreach ($items as $item) {
            $composite->addSpecification($this->getInstance($item));
        }

        return $composite;
    }

    /**
     * @param string $alias
     * @return array
     */
    private function getSchemaByAlias(string $alias): array
    {
        return $this->schema[$alias] ?? $this->schema[self::DEFAULT];
    }

    /**
     * @param string $className
     * @return Specification
     */
    private function getInstance(string $className): Specification
    {
        return $this->container->resolve($className);
    }
}
