<?php

namespace App\Utils\Repository\Conductor\Generators;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;

class MethodGenerator implements OperatorDictionary, \App\Utils\Repository\Conductor\Contracts\MethodGenerator
{
    public function generate(string $command, string $operator, bool $or, bool $not, bool $isNull = false): string
    {
        $segments = [];
        if ($or) {
            $segments[] = 'or';
        }

        $segments[] = strtolower($command);

        if ($not) {
            $segments[] = 'not';
        }

        if ($isNull) {
            $segments[] = 'null';
        } elseif (preg_grep("/$operator/i", [self::OD_IN, self::OD_BETWEEN])) {
            $segments[] = strtolower($operator);
        }

        $method = '';
        // concatenate in camelCase
        array_walk($segments, static function ($segment, $key) use (&$method) {
            if ($key !== 0) {
                $segment = ucfirst($segment);
            }
            $method .= $segment;
        });

        return $method;
    }
}
