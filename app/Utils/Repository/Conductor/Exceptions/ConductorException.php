<?php


namespace App\Utils\Repository\Conductor\Exceptions;


class ConductorException extends \RuntimeException
{
    public static function proceedFailed(\Throwable $exception): ConductorException
    {
        return new self(
            'Conductor process failed',
            $exception->getCode(),
            $exception
        );
    }
}
