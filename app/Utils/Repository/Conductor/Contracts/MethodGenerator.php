<?php

namespace App\Utils\Repository\Conductor\Contracts;

interface MethodGenerator
{
    /**
     * @param string $command
     * @param string $operator
     * @param bool $or
     * @param bool $not
     * @param bool $isNull
     * @return string
     */
    public function generate(string $command, string $operator, bool $or, bool $not, bool $isNull = false): string;
}
