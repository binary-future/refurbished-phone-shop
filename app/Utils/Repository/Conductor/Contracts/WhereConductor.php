<?php

namespace App\Utils\Repository\Conductor\Contracts;

use App\Utils\Repository\Contracts\CriteriaValue;
use Illuminate\Database\Eloquent\Builder;

interface WhereConductor
{
    /**
     * @param Builder $builder
     * @param string $column
     * @param CriteriaValue $value
     * @param bool $or
     * @param bool $not
     * @return Builder
     */
    public function apply(
        Builder $builder, string $column, CriteriaValue $value, bool $or = false, bool $not = false
    ): Builder;
}
