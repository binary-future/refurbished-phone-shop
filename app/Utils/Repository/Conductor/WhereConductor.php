<?php

namespace App\Utils\Repository\Conductor;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Utils\Repository\Conductor\Contracts\MethodGenerator;
use App\Utils\Repository\Conductor\Contracts\WhereConductor as Contract;
use App\Utils\Repository\Conductor\Exceptions\ConductorException;
use App\Utils\Repository\Contracts\CriteriaValue;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use App\Utils\Repository\Criteria\Common\CompareValue;
use App\Utils\Repository\Conductor\Contracts\Conductor;
use Illuminate\Database\Eloquent\Builder;

class WhereConductor implements Conductor, Contract, OperatorDictionary
{
    private const COMMAND = 'where';
    /**
     * @var MethodGenerator
     */
    private $methodGenerator;

    /**
     * WhereConductor constructor.
     * @param MethodGenerator $methodGenerator
     */
    public function __construct(MethodGenerator $methodGenerator)
    {
        $this->methodGenerator = $methodGenerator;
    }

    public function apply(
        Builder $builder,
        string $column,
        CriteriaValue $value,
        bool $or = false,
        bool $not = false
    ): Builder {
        try {
            return $this->proceed($builder, $column, $value, $or, $not);
        } catch (\Throwable $exception) {
            throw ConductorException::proceedFailed($exception);
        }
    }

    private function proceed(
        Builder $builder,
        string $column,
        CriteriaValue $value,
        bool $or,
        bool $not
    ): Builder {
        $method = $this->methodGenerator->generate(
            self::COMMAND,
            $this->getOperator($value),
            $or,
            $not,
            $this->isNull($value)
        );

        if ($value instanceof BetweenValues) {
            return $builder->{$method}($column, [$value->getFromValue(), $value->getToValue()]);
        }
        if (! $value instanceof CompareValue) {
            throw new \InvalidArgumentException('Value must be ' . CompareValue::class . ' class');
        }

        if ($this->areEqualOperators($value->getOperator(), self::OD_IN)) {
            return $builder->{$method}($column, $value->getValue());
        }

        if ($value->getValue() === null) {
            return $builder->{$method}($column);
        }

        return $builder->{$method}($column, $value->getOperator(), $value->getValue());
    }

    private function getOperator(CriteriaValue $value): string
    {
        return $value instanceof CompareValue
            ? $value->getOperator()
            : self::OD_BETWEEN;
    }

    private function isNull(CriteriaValue $value): bool
    {
        return $value instanceof CompareValue && $value->getValue() === null;
    }

    private function areEqualOperators(string $operator1, string $operator2): bool
    {
        return strtolower($operator1) === strtolower($operator2);
    }
}
