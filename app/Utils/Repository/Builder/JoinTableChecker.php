<?php

namespace App\Utils\Repository\Builder;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

class JoinTableChecker
{
    public function isTableJoined(Builder $builder, string $table): bool
    {
        foreach ($builder->getQuery()->joins ?? [] as $join) {
            /** @var $join JoinClause */
            if ($table === $join->table) {
                return true;
            }
        }
        return false;
    }
}
