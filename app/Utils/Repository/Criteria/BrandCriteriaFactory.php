<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

final class BrandCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'phone-brand';

    protected function getContext()
    {
        return self::CONTEXT;
    }
}
