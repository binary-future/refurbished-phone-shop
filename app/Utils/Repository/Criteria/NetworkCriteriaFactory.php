<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class NetworkCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class NetworkCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'contract-network';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
