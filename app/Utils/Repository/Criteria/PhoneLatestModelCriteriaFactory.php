<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class PhoneLatestModelCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class PhoneLatestModelCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'phone-latest-model';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
