<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class DealCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class DealCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'deals';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
