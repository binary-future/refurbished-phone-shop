<?php

namespace App\Utils\Repository\Criteria\Store;

use App\Domain\Store\Store;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByExternalId
 * @package App\Utils\Repository\Criteria\Store
 */
final class ByAlias implements Criteria
{
    /**
     * @var
     */
    private $alias;

    /**
     * ByExternalId constructor.
     * @param $alias
     */
    public function __construct($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Store::FIELD_ALIAS, '=', $this->alias);
    }
}
