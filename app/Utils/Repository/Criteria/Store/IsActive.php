<?php

namespace App\Utils\Repository\Criteria\Store;

use App\Domain\Store\Store;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class IsActive
 * @package App\Utils\Repository\Criteria\Store
 */
final class IsActive implements Criteria
{
    /**
     * @var bool
     */
    private $value;

    /**
     * IsActive constructor.
     * @param bool $value
     */
    public function __construct(bool $value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Store::FIELD_IS_ACTIVE, '=', $this->value);
    }
}
