<?php

namespace App\Utils\Repository\Criteria\Store;

use App\Domain\Store\Store;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BySlug
 * @package App\Utils\Repository\Criteria\Store
 */
final class BySlug implements Criteria
{
    /**
     * @var string
     */
    private $slug;

    /**
     * BySlug constructor.
     * @param string $slug
     */
    public function __construct(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Store::FIELD_SLUG, '=', $this->slug);
    }
}
