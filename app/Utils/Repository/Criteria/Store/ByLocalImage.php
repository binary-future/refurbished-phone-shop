<?php


namespace App\Utils\Repository\Criteria\Store;


use App\Domain\Shared\Image\Image;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByLocalImage
 * @package App\Utils\Repository\Criteria\Store
 */
final class ByLocalImage implements Criteria
{
    /**
     * @var bool
     */
    private $isLocal;

    /**
     * ByLocalImage constructor.
     * @param bool $isLocal
     */
    public function __construct(bool $isLocal)
    {
        $this->isLocal = $isLocal;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereHas('image', function ($builder) {
            $builder->where(Image::FIELD_IS_LOCAL, '=', $this->isLocal);
        });
    }
}
