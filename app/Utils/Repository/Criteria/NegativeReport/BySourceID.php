<?php


namespace App\Utils\Repository\Criteria\NegativeReport;


use App\Application\Services\Importer\Reports\NegativeReport;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByTarget
 * @package App\Utils\Repository\Criteria\NegativeReport
 */
final class BySourceID implements Criteria
{
    /**
     * @var
     */
    private $key;

    /**
     * ByTarget constructor.
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(NegativeReport::FIELD_SOURCE_ID, '=', $this->key);
    }
}
