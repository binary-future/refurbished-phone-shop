<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class PhoneTopModelCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class PhoneTopModelCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'phone-top-model';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
