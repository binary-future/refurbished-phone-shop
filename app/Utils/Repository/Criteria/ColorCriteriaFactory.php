<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class ColorCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class ColorCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'phone-color';

    /**
     * @return string
     */protected function getContext()
    {
        return self::CONTEXT;
    }
}
