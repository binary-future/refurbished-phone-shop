<?php


namespace App\Utils\Repository\Criteria\PlannedImport;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

final class ByImportDate implements Criteria
{
    /**
     * @var Carbon
     */
    private $date;

    /**
     * ByImportDate constructor.
     * @param CompareValue $date
     */
    public function __construct(CompareValue $date)
    {
        if (! $date->getValue() instanceof Carbon) {
            throw new \InvalidArgumentException('Value must be Carbon instance');
        }

        $this->date = $date;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(
            PlannedImport::FIELD_IMPORT_DATE,
            $this->date->getOperator(),
            $this->date->getValue()->format(PlannedImport::DATE_FORMAT)
        );
    }
}
