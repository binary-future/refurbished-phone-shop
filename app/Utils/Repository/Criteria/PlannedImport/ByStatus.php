<?php


namespace App\Utils\Repository\Criteria\PlannedImport;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByStatus
 * @package App\Utils\Repository\Criteria\PlannedImport
 */
final class ByStatus implements Criteria
{
    /**
     * @var bool
     */
    private $isComplete;

    /**
     * ByStatus constructor.
     * @param bool $isComplete
     */
    public function __construct(bool $isComplete)
    {
        $this->isComplete = $isComplete;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PlannedImport::FIELD_STATUS, '=', $this->isComplete);
    }
}
