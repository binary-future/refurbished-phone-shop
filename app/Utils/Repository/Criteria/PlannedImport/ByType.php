<?php


namespace App\Utils\Repository\Criteria\PlannedImport;


use App\Application\Services\Importer\Plan\PlannedImport;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByType
 * @package App\Utils\Repository\Criteria\PlannedImport
 */
final class ByType implements Criteria
{
    /**
     * @var string
     */
    private $type;

    /**
     * ByType constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PlannedImport::FIELD_TYPE, '=', $this->type);
    }
}
