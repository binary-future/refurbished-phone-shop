<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class RoleCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class RoleCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'roles';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}