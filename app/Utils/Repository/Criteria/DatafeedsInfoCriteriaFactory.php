<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class DatafeedsInfoCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class DatafeedsInfoCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'datafeeds-info';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}