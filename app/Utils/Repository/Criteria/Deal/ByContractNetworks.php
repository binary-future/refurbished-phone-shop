<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Builder\JoinTableChecker;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByContractNetworks implements Criteria
{
    /**
     * @var int[]
     */
    private $networks;
    /**
     * @var JoinTableChecker
     */
    private $joinTableChecker;

    /**
     * ByContractNetworks constructor.
     * @param int[] $networks
     * @param JoinTableChecker $joinTableChecker
     */
    public function __construct(array $networks, JoinTableChecker $joinTableChecker)
    {
        $this->networks = $networks;
        $this->joinTableChecker = $joinTableChecker;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        if (! $this->joinTableChecker->isTableJoined($builder, Contract::TABLE)) {
            $builder = (new JoinContracts())->apply($builder);
        }
        return $builder->whereIn(
            implode('.', [Contract::TABLE, Contract::FIELD_NETWORK_ID]),
            $this->networks
        );
    }
}
