<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Criteria\Common\Where;

/**
 * Class ByUpdatedAt
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByUpdatedAt extends Where
{
    /**
     * @return string
     */
    protected function getField(): string
    {
        return Deal::UPDATED_AT;
    }
}
