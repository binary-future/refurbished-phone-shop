<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByProduct implements Criteria
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ByProduct constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Deal::FIELD_PRODUCT_ID, '=', $this->product->getKey())
            ->where(Deal::FIELD_PRODUCT_TYPE, '=', $this->product->getType());
    }
}
