<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Builder\JoinTableChecker;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

final class ByContractConditions implements Criteria
{
    /**
     * @var string[]
     */
    private $conditions;
    /**
     * @var JoinTableChecker
     */
    private $joinTableChecker;

    /**
     * ByContractNetworks constructor.
     * @param JoinTableChecker $joinTableChecker
     * @param string[] $conditions
     */
    public function __construct(array $conditions, JoinTableChecker $joinTableChecker)
    {
        $this->conditions = $conditions;
        $this->joinTableChecker = $joinTableChecker;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        if (! $this->joinTableChecker->isTableJoined($builder, Contract::TABLE)) {
            $builder = (new JoinContracts())->apply($builder);
        }
        return $builder->whereIn(
            implode('.', [Contract::TABLE, Contract::FIELD_CONDITION]),
            $this->conditions
        );
    }
}
