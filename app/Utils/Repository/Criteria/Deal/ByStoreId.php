<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByStoreId
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByStoreId implements Criteria
{
    /**
     * @var int
     */
    private $storeId;

    /**
     * ByStoreId constructor.
     * @param int $storeId
     */
    public function __construct(int $storeId)
    {
        $this->storeId = $storeId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Deal::FIELD_STORE_ID, '=', $this->storeId);
    }
}