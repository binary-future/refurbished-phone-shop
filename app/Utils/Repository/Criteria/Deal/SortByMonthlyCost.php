<?php


namespace App\Utils\Repository\Criteria\Deal;


use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\OrderBy;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SortByMonthlyCost
 * @package App\Utils\Repository\Criteria\Deal
 */
final class SortByMonthlyCost implements Criteria
{
    /**
     * @var bool;
     */
    private $direction;

    /**
     * SortByMonthlyCost constructor.
     * @param bool $direction
     */
    public function __construct(bool $direction = true)
    {
        $this->direction = $direction;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->orderBy(Deal::FIELD_MONTHLY_COST, $this->direction ? 'asc' : 'desc');
    }
}
