<?php


namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Criteria\Common\Where;

/**
 * Class ByMonthlyCost
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByMonthlyCost extends Where
{
    protected function getField(): string
    {
        return Deal::FIELD_MONTHLY_COST;
    }
}
