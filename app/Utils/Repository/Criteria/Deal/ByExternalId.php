<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByExternalId
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByExternalId implements Criteria
{
    /**
     * @var string
     */
    private $externalId;

    /**
     * ByExternalId constructor.
     * @param string $externalId
     */
    public function __construct(string $externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Deal::FIELD_EXTERNAL_ID, '=', $this->externalId);
    }
}
