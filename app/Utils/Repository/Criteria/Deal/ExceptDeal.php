<?php


namespace App\Utils\Repository\Criteria\Deal;


use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ExceptDeal
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ExceptDeal implements Criteria
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * ExceptDeal constructor.
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where('id', '!=', $this->deal->getKey());
    }
}
