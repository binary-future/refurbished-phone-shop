<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByContractId implements Criteria
{
    /**
     * @var int|null
     */
    private $contractId;

    /**
     * ByContractId constructor.
     * @param int|null $contractId
     */
    public function __construct(?int $contractId)
    {
        $this->contractId = $contractId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Deal::FIELD_CONTRACT_ID, '=', $this->contractId);
    }
}
