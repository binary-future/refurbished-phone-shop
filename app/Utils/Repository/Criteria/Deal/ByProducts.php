<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Exceptions\CriteriaBuildException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class ByProducts
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByProducts implements Criteria
{
    /**
     * @var Collection
     */
    private $products;

    /**
     * ByProducts constructor.
     * @param Collection $products
     */
    public function __construct(Collection $products)
    {
        $this->products = $products;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     * @throws CriteriaBuildException
     */
    public function apply(Builder $builder)
    {
        $product = $this->products->first();
        if (! $product instanceof Product) {
            throw CriteriaBuildException::cannotBuildCriteria(get_class($this), 'Invalid params');
        }
        $productIds = $this->products->pluck('id')->all();

        return $builder->where(Deal::FIELD_PRODUCT_TYPE, '=', $product->getType())
            ->whereIn(Deal::FIELD_PRODUCT_ID, $productIds);
    }
}
