<?php


namespace App\Utils\Repository\Criteria\Deal;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class SelectContractColumn implements Criteria
{
    /**
     * @var string
     */
    private $column;

    /**
     * SelectContractColumn constructor.
     * @param string $column
     */
    public function __construct(string $column)
    {
        $this->column = $column;
    }

    public function apply(Builder $builder)
    {
        return $builder->select("contracts.{$this->column}")
            ->join('contracts', 'contracts.id', '=', 'deals.contract_id');
    }
}
