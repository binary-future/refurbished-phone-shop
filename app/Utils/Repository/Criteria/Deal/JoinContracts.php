<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class JoinContracts implements Criteria
{
    /**
     * @var string
     */
    private $type;

    /**
     * JoinContracts constructor.
     * @param string $type
     */
    public function __construct(string $type = 'inner')
    {
        $this->type = $type;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->join(
            Contract::TABLE,
            implode('.', [Deal::TABLE, Deal::FIELD_CONTRACT_ID]),
            OperatorDictionary::OD_EQUAL,
            implode('.', [Contract::TABLE, Contract::FIELD_ID]),
            $this->type
        );
    }
}
