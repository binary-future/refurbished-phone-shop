<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByContractNetwork implements Criteria
{
    /**
     * @var string
     */
    private $network;

    /**
     * ByContractNetwork constructor.
     * @param string $network
     */
    public function __construct(string $network)
    {
        $this->network = $network;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereHas(
            implode('.', [Deal::RELATION_CONTRACT, Contract::RELATION_NETWORK]),
            function (Builder $query) {
                $query->where(Deal::FIELD_ID, OperatorDictionary::OD_EQUAL, $this->network);
            }
        );
    }
}
