<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Payment;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByPayment implements Criteria
{
    /**
     * @var Payment
     */
    private $payment;

    /**
     * ByPayment constructor.
     * @param Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(Deal::FIELD_TOTAL_COST, OperatorDictionary::OD_EQUAL, $this->payment->getTotalCost())
            ->where(Deal::FIELD_UPFRONT_COST, OperatorDictionary::OD_EQUAL, $this->payment->getUpfrontCost())
            ->where(Deal::FIELD_MONTHLY_COST, OperatorDictionary::OD_EQUAL, $this->payment->getMonthlyCost());
    }
}
