<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByMonthlyCostRange
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByMonthlyCostRange implements Criteria
{
    /**
     * @var int
     */
    private $from;

    /**
     * @var int
     */
    private $to;

    /**
     * ByMonthlyCostRange constructor.
     * @param BetweenValues $range
     */
    public function __construct(BetweenValues $range)
    {
        $this->from = (int)$range->getFromValue();
        $this->to = (int)$range->getToValue();
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereBetween(
            implode('.', [Deal::TABLE, Deal::FIELD_MONTHLY_COST]),
            [ $this->from, $this->to ]
        );
    }
}
