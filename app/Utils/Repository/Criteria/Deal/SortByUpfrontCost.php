<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SortByUpfrontCost
 * @package App\Utils\Repository\Criteria\Deal
 */
final class SortByUpfrontCost implements Criteria
{
    /**
     * @var bool
     */
    private $direction;

    /**
     * SortByUpfrontCost constructor.
     * @param bool $direction
     */
    public function __construct(bool $direction)
    {
        $this->direction = $direction;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->orderBy(Deal::FIELD_UPFRONT_COST, $this->direction ? 'asc' : 'desc');
    }
}
