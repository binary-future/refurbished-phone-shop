<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class SelectNetworkColumn implements Criteria
{
    /**
     * @var string
     */
    private $column;

    /**
     * SelectContractColumn constructor.
     * @param string $column
     */
    public function __construct(string $column)
    {
        $this->column = $column;
    }

    public function apply(Builder $builder)
    {
        return  $builder->select(
            $this->column
        )->join(
            Contract::TABLE,
            implode('.', [Deal::TABLE, Deal::FIELD_CONTRACT_ID]),
            OperatorDictionary::OD_EQUAL,
            implode('.', [Contract::TABLE, Contract::FIELD_ID]),
        )->join(
            Network::TABLE,
            implode('.', [Contract::TABLE, Contract::FIELD_NETWORK_ID]),
            OperatorDictionary::OD_EQUAL,
            implode('.', [Network::TABLE, Network::FIELD_ID]),
        );
    }
}
