<?php

namespace App\Utils\Repository\Criteria\Deal;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use App\Utils\Repository\Criteria\Common\Where;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByTotalCost
 * @package App\Utils\Repository\Criteria\Deal
 */
final class ByTotalCost extends Where
{
    /**
     * @return string
     */
    protected function getField(): string
    {
        return Deal::FIELD_TOTAL_COST;
    }
}
