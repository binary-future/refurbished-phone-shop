<?php


namespace App\Utils\Repository\Criteria\Deal;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

final class ByDealsIds implements Criteria
{
    /**
     * @var Collection
     */
    private $ids;

    /**
     * ByDealsIds constructor.
     * @param Collection $ids
     */
    public function __construct(Collection $ids)
    {
        $this->ids = $ids;
    }

    public function apply(Builder $builder)
    {
        return $builder->whereIn('deals.id', $this->ids->all());
    }

}