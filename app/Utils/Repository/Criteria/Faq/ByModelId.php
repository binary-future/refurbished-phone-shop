<?php

namespace App\Utils\Repository\Criteria\Faq;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Phone\Faq\Contracts\Faq;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByModelId implements Criteria
{
    /**
     * @var int
     */
    private $modelId;

    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    public function apply(Builder $builder): Builder
    {
        return $builder->where(Faq::FIELD_PHONE_MODEL_ID, OperatorDictionary::OD_EQUAL, $this->modelId);
    }
}
