<?php

namespace App\Utils\Repository\Criteria\Faq;

use App\Domain\Phone\Faq\Contracts\Faq;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByIds implements Criteria
{
    /**
     * @var int[]
     */
    private $ids;

    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    public function apply(Builder $builder): Builder
    {
        return $builder->whereIn(Faq::FIELD_ID, $this->ids);
    }
}
