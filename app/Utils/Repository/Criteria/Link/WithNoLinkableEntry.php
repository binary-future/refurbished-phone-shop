<?php

namespace App\Utils\Repository\Criteria\Link;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Shared\Link\Link;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

final class WithNoLinkableEntry implements Criteria, OperatorDictionary
{
    private $type;
    /**
     * @var string
     */
    private $operator;

    /**
     * WithNoDeals constructor.
     * @param CompareValue $linkableType
     */
    public function __construct(CompareValue $linkableType)
    {
        $this->type = $linkableType->getValue();
        $this->operator = $linkableType->getOperator();
    }

    public function apply(Builder $builder)
    {
        return $builder
            ->leftJoin(
                Deal::TABLE,
                Deal::TABLE . '.' . Deal::FIELD_ID,
                self::OD_EQUAL,
                Link::TABLE . '.' . Link::FIELD_OWNER_ID
            )->where(
                Link::TABLE . '.' . Link::FIELD_OWNER_TYPE,
                $this->operator,
                $this->type
            )->whereNull(Deal::TABLE . '.' . Deal::FIELD_ID);
    }
}
