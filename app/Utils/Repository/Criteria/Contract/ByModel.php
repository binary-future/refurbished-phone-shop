<?php


namespace App\Utils\Repository\Criteria\Contract;


use App\Domain\Deals\Contract\Contract;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByModel implements Criteria
{
    /**
     * @var Contract
     */
    private $contract;

    /**
     * ByModel constructor.
     * @param $contract
     */
    public function __construct(Contract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where($this->contract->toArray());
    }
}
