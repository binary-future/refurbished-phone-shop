<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

final class FaqCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'faq';

    protected function getContext(): string
    {
        return self::CONTEXT;
    }
}
