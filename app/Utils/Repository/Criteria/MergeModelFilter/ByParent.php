<?php


namespace App\Utils\Repository\Criteria\MergeModelFilter;


use App\Application\Services\Merge\Model\Filter;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByParent implements Criteria
{
    /**
     * @var Filter|null
     */
    private $parent;

    /**
     * ByParent constructor.
     * @param Filter|null $parent
     */
    public function __construct(?Filter $parent)
    {
        $this->parent = $parent;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(
            Filter::FIELD_FILTER_ID,
            '=',
            $this->parent ? $this->parent->getKey() : null
        );
    }
}
