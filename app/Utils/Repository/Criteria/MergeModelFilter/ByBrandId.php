<?php


namespace App\Utils\Repository\Criteria\MergeModelFilter;

use App\Application\Services\Merge\Model\Filter;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByBrandId implements Criteria
{
    /**
     * @var int
     */
    private $brandId;

    /**
     * ByBrandId constructor.
     * @param int $brandId
     */
    public function __construct(int $brandId)
    {
        $this->brandId = $brandId;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(Filter::FIELD_BRAND_ID, $this->brandId);
    }
}
