<?php


namespace App\Utils\Repository\Criteria\MergeModelFilter;


use App\Application\Services\Merge\Model\Filter;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByParentId
 * @package App\Utils\Repository\Criteria\MergeModelFitler
 */
final class ByParentId implements Criteria
{
    /**
     * @var int|null
     */
    private $parentId;

    /**
     * ByParentId constructor.
     * @param int|null $parentId
     */
    public function __construct(?int $parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Filter::FIELD_FILTER_ID, '=', $this->parentId);
    }
}
