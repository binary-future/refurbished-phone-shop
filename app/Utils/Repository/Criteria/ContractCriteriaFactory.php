<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class ContractCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class ContractCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'deal-contract';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
