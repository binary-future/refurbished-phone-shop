<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class PhoneModelCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class PhoneModelCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'phone-model';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
