<?php


namespace App\Utils\Repository\Criteria\MergeColorFilter;


use App\Application\Services\Merge\Color\Filter;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByFilter implements Criteria
{
    /**
     * @var string
     */
    private $filter;

    /**
     * ByFilter constructor.
     * @param string $filter
     */
    public function __construct(string $filter)
    {
        $this->filter = $filter;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(Filter::FIELD_FILTER, '=', $this->filter);
    }
}
