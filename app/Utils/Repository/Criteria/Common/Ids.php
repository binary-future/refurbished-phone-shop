<?php

namespace App\Utils\Repository\Criteria\Common;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class Ids implements Criteria, OperatorDictionary
{
    /**
     * @var int[]|string[]
     */
    private $ids;

    /**
     * Id constructor.
     * @param int[]|string[] $ids
     */
    public function __construct(array $ids)
    {
        if (! $this->isValid($ids)) {
            throw new \InvalidArgumentException('$ids must be array integer ids');
        }

        $this->ids = $ids;
    }

    private function isValid(array $ids): bool
    {
        $isValid = true;
        array_map(static function ($item) use (&$isValid) {
            if (! is_numeric($item)) {
                $isValid = false;
            }
        }, $ids);
        return $isValid;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereIn('id', $this->ids);
    }
}
