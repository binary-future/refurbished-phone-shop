<?php

namespace App\Utils\Repository\Criteria\Common;

use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class All implements Criteria
{
    /**
     * @var array
     */
    private $relations;

    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->with($this->relations);
    }
}
