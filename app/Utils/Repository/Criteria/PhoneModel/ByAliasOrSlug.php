<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use App\Utils\Repository\Criteria\PhoneModel\Objects\AliasAndSlugValues;
use Illuminate\Database\Eloquent\Builder;

final class ByAliasOrSlug implements Criteria
{
    /**
     * @var CompareValue
     */
    private $alias;

    /**
     * @var CompareValue
     */
    private $slug;

    /**
     * ByAlias constructor.
     * @param AliasAndSlugValues $aliasAndSlugValues
     */
    public function __construct(AliasAndSlugValues $aliasAndSlugValues)
    {
        $this->alias = $aliasAndSlugValues->getAlias();
        $this->slug = $aliasAndSlugValues->getSlug();
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder
            ->where(PhoneModel::FIELD_ALIAS, $this->alias->getOperator(), $this->alias->getValue())
            ->orWhere(PhoneModel::FIELD_SLUG, $this->slug->getOperator(), $this->slug->getValue());
    }
}
