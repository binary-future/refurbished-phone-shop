<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Conductor\Contracts\WhereConductor;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ActiveOrBrandIds
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class ActiveOrBrandIds implements Criteria
{
    /**
     * @var CompareValue
     */
    private $brandId;
    /**
     * @var WhereConductor
     */
    private $whereConductor;

    /**
     * ByBrandId constructor.
     * @param CompareValue $brandId
     * @param WhereConductor $whereConductor
     */
    public function __construct(CompareValue $brandId, WhereConductor $whereConductor)
    {
        $this->brandId = $brandId;
        $this->whereConductor = $whereConductor;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(function (Builder $builder) {
            $builder->where(PhoneModel::FIELD_IS_ACTIVE, '=', true);

            $this->whereConductor->apply(
                $builder,
                PhoneModel::FIELD_BRAND_ID,
                $this->brandId,
                true
            );

            return $builder;
        });
    }
}
