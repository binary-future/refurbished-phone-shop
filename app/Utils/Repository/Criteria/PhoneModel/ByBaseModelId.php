<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByBaseModelId implements Criteria
{
    /**
     * @var int|null
     */
    private $baseModelId;

    /**
     * ByBaseModelId constructor.
     * @param int|null $baseModelId
     */
    public function __construct(?int $baseModelId)
    {
        $this->baseModelId = $baseModelId;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(PhoneModel::FIELD_PARENT_ID, $this->baseModelId);
    }
}
