<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

final class SortByCoslyDeal implements Criteria
{
    /**
     * @var bool
     */
    private $direction;

    /**
     * SortByCoslyDeal constructor.
     * @param bool $direction
     */
    public function __construct(bool $direction)
    {
        $this->direction = $direction;
    }

    public function apply(Builder $builder)
    {

         return $builder->join('phones', 'phones.model_id', '=', 'phone_models.id')
            ->leftJoin('deals', function ($join) {
                $join->on('deals.product_id', '=', 'phones.id')
                    ->where('deals.product_type', '=', 'phones');
            })
            ->select(
                'phone_models.*',
                DB::raw("MAX(deals.total_cost) as total_cost_value, COUNT(deals.id) as qnt")
            )->groupBy('phone_models.id')
            ->orderBy('total_cost_value', $this->direction ? 'asc' : 'desc')
            ->orderBy('qnt', $this->direction ? 'asc' : 'desc');
    }
}
