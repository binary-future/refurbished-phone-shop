<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class NameLike
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class NameLike implements Criteria
{
    /**
     * @var string
     */
    private $name;

    /**
     * NameLike constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        $builder->select(DB::raw('phone_models.*'))
            ->join('brands as b', 'b.id', '=', 'phone_models.brand_id')
            ->where(DB::raw('CONCAT(b.name, " ", phone_models.name)'), 'LIKE', '%' . $this->name . '%');

        return $builder;
    }
}
