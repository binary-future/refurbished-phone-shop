<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BySlug
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class BySlug implements Criteria
{
    /**
     * @var string
     */
    private $slug;

    /**
     * BySlug constructor.
     * @param string $slug
     */
    public function __construct(string $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PhoneModel::FIELD_SLUG, '=', $this->slug);
    }
}
