<?php

namespace App\Utils\Repository\Criteria\PhoneModel\Objects;

use App\Utils\Repository\Criteria\Common\CompareValue;

class AliasAndSlugValues
{
    /**
     * @var CompareValue
     */
    private $alias;
    /**
     * @var CompareValue
     */
    private $slug;

    /**
     * AliasAndSlugValues constructor.
     * @param CompareValue $alias
     * @param CompareValue $slug
     */
    public function __construct(CompareValue $alias, CompareValue $slug)
    {
        $this->alias = $alias;
        $this->slug = $slug;
    }

    /**
     * @return CompareValue
     */
    public function getAlias(): CompareValue
    {
        return $this->alias;
    }

    /**
     * @return CompareValue
     */
    public function getSlug(): CompareValue
    {
        return $this->slug;
    }
}
