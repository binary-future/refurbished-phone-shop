<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class ExceptModelsById
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class ExceptModelsById implements Criteria
{
    /**
     * @var int[] $modelsIds
     */
    private $modelsIds = [];

    /**
     * ExceptModels constructor.
     * @param Collection $modelsIds
     */
    public function __construct(Collection $modelsIds)
    {
        if($this->isValid($modelsIds)) {
            $this->modelsIds = $modelsIds->toArray();
        }
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereNotIn('phone_models.id', $this->modelsIds);
    }

    /**
     * @param Collection $modelsIds
     * @return bool
     */
    private function isValid(Collection $modelsIds): bool
    {
        foreach ($modelsIds as $modelsId) {
            if (! is_int($modelsId) || $modelsId < 1) {
                throw new \InvalidArgumentException(
                    'Model id must be integer and positive'
                );
            }
        }

        return true;
    }
}
