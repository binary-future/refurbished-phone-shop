<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class HasSynonyms implements Criteria
{
    /**
     * @var bool
     */
    private $isHas;

    /**
     * HasSynonyms constructor.
     * @param bool $isHas
     */
    public function __construct(bool $isHas)
    {
        $this->isHas = $isHas;
    }

    public function apply(Builder $builder)
    {
        return $this->isHas ? $builder->has('synonyms') : $builder->doesntHave('synonyms');
    }
}
