<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

final class BySynonym implements Criteria
{
    /**
     * @var CompareValue
     */
    private $synonym;

    /**
     * BySynonym constructor.
     * @param CompareValue $synonym
     */
    public function __construct(CompareValue $synonym)
    {
        $this->synonym = $synonym;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(
            PhoneModel::TABLE . '.' . PhoneModel::FIELD_PARENT_ID,
            $this->synonym->getOperator(),
            $this->synonym->getValue()
        );
    }
}
