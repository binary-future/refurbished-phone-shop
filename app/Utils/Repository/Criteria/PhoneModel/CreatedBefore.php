<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CreatedBefore
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class CreatedBefore implements Criteria
{
    /**
     * @var Carbon
     */
    private $date;

    /**
     * CreatedBefore constructor.
     * @param Carbon $date
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PhoneModel::FIELD_CREATED_AT, '<', $this->date);
    }
}
