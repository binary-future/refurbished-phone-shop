<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByAlias implements Criteria
{
    /**
     * @var string
     */
    private $alias;

    /**
     * ByAlias constructor.
     * @param $alias
     */
    public function __construct($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PhoneModel::FIELD_ALIAS, '=', $this->alias);
    }
}
