<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class HasDeals implements Criteria
{
    /**
     * @var bool
     */
    private $isHas;

    /**
     * HasDeals constructor.
     * @param bool $isHas
     */
    public function __construct(bool $isHas)
    {
        $this->isHas = $isHas;
    }

    public function apply(Builder $builder)
    {
        return $this->isHas
            ? $builder->whereHas('phones', function ($query) {
                $query->has('deals');
            }) : $builder->whereDoesntHave('phones', function ($query) {
                $query->has('deals');
            });
    }
}
