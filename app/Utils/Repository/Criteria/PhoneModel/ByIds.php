<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByIds
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class ByIds implements Criteria
{
    /**
     * @var array
     */
    private $ids;

    /**
     * ByIds constructor.
     * @param array $ids
     */
    public function __construct(array $ids)
    {
        $this->ids = $ids;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereIn('id', $this->ids);
    }
}
