<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ExceptModel
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class ExceptModel implements Criteria
{
    /**
     * @var PhoneModel $model
     */
    private $model;

    /**
     * ExceptModel constructor.
     * @param PhoneModel $model
     */
    public function __construct(PhoneModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where('phone_models.id', '!=', $this->model->getKey());
    }
}
