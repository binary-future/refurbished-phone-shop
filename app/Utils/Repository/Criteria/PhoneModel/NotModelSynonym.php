<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class NotModelSynonym implements Criteria
{
    /**
     * @var PhoneModel
     */
    private $model;

    /**
     * NotModelSynonym constructor.
     * @param PhoneModel $model
     */
    public function __construct(PhoneModel $model)
    {
        $this->model = $model;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(
            'phone_models.' . PhoneModel::FIELD_PARENT_ID,
            '!=',
            $this->model->getKey()
        );
    }
}
