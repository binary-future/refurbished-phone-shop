<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByBrandIds
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class OrBrandIds implements Criteria
{
    /**
     * @var CompareValue
     */
    private $brandId;

    /**
     * ByBrandId constructor.
     * @param CompareValue $brandId
     */
    public function __construct(CompareValue $brandId)
    {
        $this->brandId = $brandId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return is_array($this->brandId->getValue())
            ? $builder->orWhereIn(PhoneModel::FIELD_BRAND_ID, $this->brandId->getValue())
            : $builder->orWhere(
                PhoneModel::FIELD_BRAND_ID,
                $this->brandId->getOperator(),
                $this->brandId->getValue()
            );
    }
}
