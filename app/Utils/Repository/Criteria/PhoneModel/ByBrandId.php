<?php

namespace App\Utils\Repository\Criteria\PhoneModel;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByBrandId
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class ByBrandId implements Criteria
{
    /**
     * @var int
     */
    private $brandId;

    /**
     * ByBrandId constructor.
     * @param int $brandId
     */
    public function __construct(int $brandId)
    {
        $this->brandId = $brandId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PhoneModel::FIELD_BRAND_ID, '=', $this->brandId);
    }
}
