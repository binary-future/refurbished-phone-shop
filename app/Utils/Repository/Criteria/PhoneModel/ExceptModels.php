<?php


namespace App\Utils\Repository\Criteria\PhoneModel;


use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class ExceptModels
 * @package App\Utils\Repository\Criteria\PhoneModel
 */
final class ExceptModels implements Criteria
{
    /**
     * @var int[] $modelsIds
     */
    private $modelsIds = [];

    /**
     * ExceptModels constructor.
     * @param Collection $models
     */
    public function __construct(Collection $models)
    {
        foreach ($models as $model) {
            /**
             * @var $model PhoneModel
             */
            $this->modelsIds[] = $model->getKey();
        }
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereNotIn('phone_models.id', $this->modelsIds);
    }
}
