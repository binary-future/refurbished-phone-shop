<?php

namespace App\Utils\Repository\Criteria\TopModel;

use App\Application\Services\Model\TopModel\TopModel;
use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Conductor\WhereConductor;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BySynonyms
 * @package App\Utils\Repository\Criteria\LatestModel
 */
final class BySynonyms implements Criteria, OperatorDictionary
{
    /**
     * @var CompareValue
     */
    private $synonym;
    /**
     * @var WhereConductor
     */
    private $whereConductor;

    /**
     * BySlug constructor.
     * @param WhereConductor $whereConductor
     * @param CompareValue $synonym
     */
    public function __construct(CompareValue $synonym, WhereConductor $whereConductor)
    {
        $this->synonym = $synonym;
        $this->whereConductor = $whereConductor;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        $builder = $builder->join(
            PhoneModel::TABLE,
            PhoneModel::TABLE . '.' . PhoneModel::FIELD_ID,
            self::OD_EQUAL,
            TopModel::TABLE . '.' . TopModel::FIELD_MODEL_ID
        );
        return $this->whereConductor->apply(
            $builder,
            PhoneModel::TABLE . '.' . PhoneModel::FIELD_PARENT_ID,
            $this->synonym,
            false,
            $this->synonym->getOperator() === self::OD_IS_NOT
        );
    }
}
