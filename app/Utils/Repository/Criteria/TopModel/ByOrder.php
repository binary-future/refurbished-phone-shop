<?php

namespace App\Utils\Repository\Criteria\TopModel;

use App\Application\Services\Model\TopModel\TopModel;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByOrder
 * @package App\Utils\Repository\Criteria\TopModel
 */
final class ByOrder implements Criteria
{
    /**
     * @var CompareValue|BetweenValues
     */
    private $order;

    /**
     * BySlug constructor.
     * @param CompareValue|BetweenValues $order
     */
    public function __construct($order)
    {
        if ($this->validate($order)) {
            $this->order = $order;
        }
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $this->order instanceof CompareValue
            ? $this->queryWithCompareValue($builder, $this->order)
            : $this->queryWithBetweenValues($builder, $this->order);
    }

    /**
     * @param Builder $builder
     * @param CompareValue $order
     * @return Builder
     */
    private function queryWithCompareValue(
        Builder $builder,
        CompareValue $order
    ): Builder
    {
        return $builder->where(
            TopModel::FIELD_ORDER,
            $order->getOperator(),
            $order->getValue()
        );
    }

    /**
     * @param Builder $builder
     * @param BetweenValues $order
     * @return Builder
     */
    private function queryWithBetweenValues(
        Builder $builder,
        BetweenValues $order
    ): Builder
    {
        return $builder->where(
            TopModel::FIELD_ORDER,
            $order->getFromOperator(),
            $order->getFromValue()
        )->where(
            TopModel::FIELD_ORDER,
            $order->getToOperator(),
            $order->getToValue()
        );
    }

    /**
     * @param $value
     * @return bool
     */
    private function validate($value): bool
    {
        if (! $value instanceof CompareValue
            && ! $value instanceof BetweenValues
        ) {
            throw new \InvalidArgumentException(
                'Given value should be CompareValue or BetweenValues object'
            );
        }

        return true;
    }
}
