<?php

namespace App\Utils\Repository\Criteria\TopModel;

use App\Application\Services\Model\TopModel\TopModel;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByModelId
 * @package App\Utils\Repository\Criteria\TopModel
 */
final class ByModelId implements Criteria
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * BySlug constructor.
     * @param int $modelId
     */
    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(TopModel::FIELD_MODEL_ID, '=', $this->modelId);
    }
}
