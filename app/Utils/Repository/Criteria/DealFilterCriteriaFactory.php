<?php


namespace App\Utils\Repository\Criteria;


use App\Utils\Repository\CriteriaFactory;

final class DealFilterCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'deal-filter';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
