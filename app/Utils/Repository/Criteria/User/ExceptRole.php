<?php

namespace App\Utils\Repository\Criteria\User;

use App\Domain\User\Role;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ExceptRole implements Criteria
{
    /**
     * @var string
     */
    private $role;

    /**
     * ExceptRole constructor.
     * @param string $role
     */
    public function __construct(string $role)
    {
        $this->role = $role;
    }

    public function apply(Builder $builder)
    {
        return $builder->whereHas('role', function ($query) {
            $query->where(Role::FIELD_TITLE, '!=', $this->role);
        });
    }
}
