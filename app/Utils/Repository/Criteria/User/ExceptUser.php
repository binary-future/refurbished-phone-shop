<?php

namespace App\Utils\Repository\Criteria\User;

use App\Domain\User\User;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ExceptUser
 * @package App\Utils\Repository\Criteria\User
 */
final class ExceptUser implements Criteria
{
    /**
     * @var User
     */
    private $user;

    /**
     * ExceptUser constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where('id', '!=', $this->user->getKey());
    }
}
