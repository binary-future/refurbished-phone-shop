<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class UserCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class UserCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'users';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}