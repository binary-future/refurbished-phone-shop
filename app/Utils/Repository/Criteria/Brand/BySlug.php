<?php

namespace App\Utils\Repository\Criteria\Brand;

use App\Domain\Phone\Brand\Brand;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BySlugs
 * @package App\Utils\Repository\Criteria\Brand
 */
class BySlug implements Criteria
{
    /**
     * @var CompareValue
     */
    private $slug;

    /**
     * BySlugs constructor.
     * @param CompareValue $slug
     */
    public function __construct(CompareValue $slug)
    {
        $this->slug = $slug;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return is_array($this->slug->getValue())
            ? $builder->whereIn(
                Brand::TABLE . '.' . Brand::FIELD_SLUG,
                $this->slug->getValue()
            )
            : $builder->where(
                Brand::TABLE . '.' . Brand::FIELD_SLUG,
                $this->slug->getOperator(),
                $this->slug->getValue()
            );
    }
}
