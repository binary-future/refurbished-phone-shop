<?php

namespace App\Utils\Repository\Criteria\Brand;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByActiveModels
 * @package App\Utils\Repository\Criteria\Brand
 */
class ByActiveModels implements Criteria
{
    /**
     * @var string
     */
    private $isActive;

    /**
     * BySlug constructor.
     * @param int $isActive
     */
    public function __construct(int $isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder
            ->join(PhoneModel::TABLE, function ($join) {
                $join->on(Brand::TABLE . '.id', '=', PhoneModel::TABLE . '.' . PhoneModel::FIELD_BRAND_ID)
                    ->where(PhoneModel::TABLE . '.' . PhoneModel::FIELD_IS_ACTIVE, '=', $this->isActive);
            })->groupBy(Brand::TABLE . '.id');
    }
}
