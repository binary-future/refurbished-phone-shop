<?php


namespace App\Utils\Repository\Criteria\Brand;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ById implements Criteria
{
    /**
     * @var int
     */
    private $id;

    /**
     * ById constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function apply(Builder $builder)
    {
        return $builder->where('id', $this->id);
    }
}
