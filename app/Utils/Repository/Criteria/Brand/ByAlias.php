<?php

namespace App\Utils\Repository\Criteria\Brand;

use App\Domain\Phone\Brand\Brand;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByAlias
 * @package App\Utils\Repository\Criteria\Brand
 */
class ByAlias implements Criteria
{
    /**
     * @var string
     */
    private $alias;

    /**
     * ByAlias constructor.
     * @param $alias
     */
    public function __construct($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Brand::FIELD_ALIAS, '=', $this->alias);
    }
}
