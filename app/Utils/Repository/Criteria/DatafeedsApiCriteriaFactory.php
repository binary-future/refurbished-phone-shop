<?php


namespace App\Utils\Repository\Criteria;


use App\Utils\Repository\CriteriaFactory;

/**
 * Class DatafeedsApiCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class DatafeedsApiCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'datafeeds-api';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}