<?php


namespace App\Utils\Repository\Criteria;


use App\Utils\Repository\CriteriaFactory;

final class PositiveReportCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'positive-import-report';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}