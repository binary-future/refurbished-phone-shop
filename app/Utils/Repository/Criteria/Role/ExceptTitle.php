<?php

namespace App\Utils\Repository\Criteria\Role;

use App\Domain\User\Role;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ExceptTitle
 * @package App\Utils\Repository\Criteria\Role
 */
final class ExceptTitle implements Criteria
{
    /**
     * @var string
     */
    private $title;

    /**
     * ExceptTitle constructor.
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(Role::FIELD_TITLE, '!=', $this->title);
    }
}