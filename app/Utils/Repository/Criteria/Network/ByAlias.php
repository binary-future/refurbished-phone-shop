<?php

namespace App\Utils\Repository\Criteria\Network;

use App\Domain\Deals\Contract\Network;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByAlias implements Criteria
{
    /**
     * @var string
     */
    private $alias;

    /**
     * ByAlias constructor.
     * @param string $alias
     */
    public function __construct(string $alias)
    {
        $this->alias = $alias;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Network::FIELD_ALIAS, '=', $this->alias);
    }
}
