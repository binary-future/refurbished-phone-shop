<?php


namespace App\Utils\Repository\Criteria\Phone;


use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class IsActive
 * @package App\Utils\Repository\Criteria\Phone
 */
final class IsActive implements Criteria
{
    /**
     * @var bool
     */
    private $isActive;

    /**
     * IsActive constructor.
     * @param bool $isActive
     */
    public function __construct(bool $isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereHas('model', function ($query) {
            $query->where(PhoneModel::FIELD_IS_ACTIVE, '=', $this->isActive);
        });
    }
}
