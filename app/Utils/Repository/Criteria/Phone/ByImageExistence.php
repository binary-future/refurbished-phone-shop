<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\JoinClause;

/**
 * Class WithoutImage
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByImageExistence implements Criteria, OperatorDictionary
{
    /**
     * @var bool
     */
    private $isExist;

    /**
     * ByLocalImage constructor.
     * @param bool $isExist
     */
    public function __construct(bool $isExist = true)
    {
        $this->isExist = $isExist;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        $where = $this->isExist ? 'whereNotNull' : 'whereNull';
        return $builder->leftJoin(Image::TABLE, static function (JoinClause $join) {
            $join->on(
                Phone::TABLE . '.' . Phone::FIELD_ID,
                self::OD_EQUAL,
                Image::TABLE . '.' . Image::FIELD_OWNER_ID
            )->where(
                Image::TABLE . '.' . Image::FIELD_OWNER_TYPE,
                self::OD_EQUAL,
                Image::TYPE_PHONES
            );
        })->$where(
            Image::TABLE . '.' . Image::FIELD_ID
        );
    }
}
