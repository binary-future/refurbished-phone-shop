<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByModelId implements Criteria
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * ByModelId constructor.
     * @param int $modelId
     */
    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Phone::FIELD_MODEL_ID, '=', $this->modelId);
    }
}
