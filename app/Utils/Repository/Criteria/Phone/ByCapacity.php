<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByCapacity
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByCapacity implements Criteria
{
    /**
     * @var string|null
     */
    private $capacity;

    /**
     * ByCapacity constructor.
     * @param string|null $capacity
     */
    public function __construct(?string $capacity)
    {
        $this->capacity = $capacity;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Phone::FIELD_CAPACITY, '=', $this->capacity);
    }
}
