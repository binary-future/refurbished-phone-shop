<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByExternalId
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByExternalId implements Criteria
{
    /**
     * @var int
     */
    private $externalId;

    /**
     * ByExternalId constructor.
     * @param int $externalId
     */
    public function __construct(int $externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Phone::FIELD_EXTERNAL_ID, '=', $this->externalId);
    }
}
