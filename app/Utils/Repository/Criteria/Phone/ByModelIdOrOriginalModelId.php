<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByModelIdOrOriginalModelId
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByModelIdOrOriginalModelId implements Criteria
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * ByModelIdOrOriginalModelId constructor.
     * @param int $modelId
     */
    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(function (Builder $where) {
            $where->where(Phone::FIELD_MODEL_ID, '=', $this->modelId);
            $where->orWhere(Phone::FIELD_ORIGINAL_MODEL_ID, '=', $this->modelId);
        });
    }
}
