<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByColorId
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByColorId implements Criteria
{
    /**
     * @var int
     */
    private $colorId;

    /**
     * ByColorId constructor.
     * @param int $colorId
     */
    public function __construct(int $colorId)
    {
        $this->colorId = $colorId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Phone::FIELD_COLOR_ID, '=', $this->colorId);
    }
}
