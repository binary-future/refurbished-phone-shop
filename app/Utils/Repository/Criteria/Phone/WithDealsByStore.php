<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Deals\Deal\Deal;
use App\Domain\Store\Store;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithDealsByStore
 * @package App\Utils\Repository\Criteria\Phone
 */
final class WithDealsByStore implements Criteria
{
    /**
     * @var Store
     */
    private $store;

    /**
     * WithDealsByStore constructor.
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->whereHas('deals', function ($query) {
            $query->where(Deal::FIELD_STORE_ID, '=', $this->store->getKey());
        });
    }
}
