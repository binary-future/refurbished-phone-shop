<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByColorIdOrOriginalColorId
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByColorIdOrOriginalColorId implements Criteria
{
    /**
     * @var int
     */
    private $colorId;

    /**
     * ByColorId constructor.
     * @param int $colorId
     */
    public function __construct(int $colorId)
    {
        $this->colorId = $colorId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(function (Builder $where) {
            $where->where(Phone::FIELD_COLOR_ID, '=', $this->colorId);
            $where->orWhere(Phone::FIELD_ORIGINAL_COLOR_ID, '=', $this->colorId);
        });
    }
}
