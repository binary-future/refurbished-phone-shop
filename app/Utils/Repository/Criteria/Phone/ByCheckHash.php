<?php

namespace App\Utils\Repository\Criteria\Phone;

use App\Domain\Phone\Phone\Phone;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByCheckHash
 * @package App\Utils\Repository\Criteria\Phone
 */
final class ByCheckHash implements Criteria
{
    /**
     * @var string
     */
    private $hash;

    /**
     * ByCheckHash constructor.
     * @param string $hash
     */
    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Phone::FIELD_CHECK_HASH, '=', $this->hash);
    }
}
