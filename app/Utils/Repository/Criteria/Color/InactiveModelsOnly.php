<?php


namespace App\Utils\Repository\Criteria\Color;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class InactiveModelsOnly implements Criteria
{
    private $params;

    /**
     * InactiveModelsOnly constructor.
     * @param $params
     */
    public function __construct($params = [])
    {
        $this->params = $params;
    }

    public function apply(Builder $builder)
    {
        return $builder->whereDoesntHave('phones', function (Builder $query) {
            $query->whereHas('model', function (Builder $query) {
                $query->where(PhoneModel::FIELD_IS_ACTIVE, '=', PhoneModel::ACTIVE);
            });
        });
    }
}
