<?php


namespace App\Utils\Repository\Criteria\Color;


use App\Domain\Phone\Phone\Color;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByAlias implements Criteria
{
    /**
     * @var string
     */
    private $alias;

    /**
     * ByAlias constructor.
     * @param string $alias
     */
    public function __construct(string $alias)
    {
        $this->alias = $alias;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(Color::FIELD_ALIAS, '=', $this->alias);
    }
}
