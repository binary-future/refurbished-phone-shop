<?php


namespace App\Utils\Repository\Criteria\Color;


use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ExceptId
 * @package App\Utils\Repository\Criteria\Color
 */
final class ExceptId implements Criteria
{
    /**
     * @var int
     */
    private $colorId;

    /**
     * ExceptId constructor.
     * @param int $colorId
     */
    public function __construct(int $colorId)
    {
        $this->colorId = $colorId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where('id', '!=', $this->colorId);
    }
}
