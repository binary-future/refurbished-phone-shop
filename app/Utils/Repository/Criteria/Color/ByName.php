<?php

namespace App\Utils\Repository\Criteria\Color;

use App\Domain\Phone\Phone\Color;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class ByName implements Criteria
{
    /**
     * @var string
     */
    private $name;

    /**
     * ByName constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Color::FIELD_NAME, '=', $this->name);
    }
}
