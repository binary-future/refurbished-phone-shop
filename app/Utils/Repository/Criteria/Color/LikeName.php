<?php


namespace App\Utils\Repository\Criteria\Color;


use App\Domain\Phone\Phone\Color;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class LikeName implements Criteria
{
    /**
     * @var string
     */
    private $name;

    /**
     * LikeName constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function apply(Builder $builder)
    {
        return $builder->where(Color::FIELD_NAME, 'like', '%' . $this->name . '%');
    }
}
