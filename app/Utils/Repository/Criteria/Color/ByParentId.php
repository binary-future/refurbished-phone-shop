<?php


namespace App\Utils\Repository\Criteria\Color;


use App\Domain\Phone\Phone\Color;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByParentId
 * @package App\Utils\Repository\Criteria\Color
 */
final class ByParentId implements Criteria
{
    /**
     * @var int|null
     */
    private $parentId;

    /**
     * ByParentId constructor.
     * @param int|null $parentId
     */
    public function __construct(?int $parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(Color::FIELD_PARENT_ID, '=', $this->parentId);
    }
}
