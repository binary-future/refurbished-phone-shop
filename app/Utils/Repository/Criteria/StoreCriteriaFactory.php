<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

final class StoreCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'store';

    protected function getContext()
    {
        return self::CONTEXT;
    }
}
