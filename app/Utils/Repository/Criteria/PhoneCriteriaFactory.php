<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class PhoneCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class PhoneCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'phone-item';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
