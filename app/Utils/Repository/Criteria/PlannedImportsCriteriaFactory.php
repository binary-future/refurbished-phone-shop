<?php


namespace App\Utils\Repository\Criteria;


use App\Utils\Repository\CriteriaFactory;

/**
 * Class PlannedImportsCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class PlannedImportsCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'planned-imports';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
