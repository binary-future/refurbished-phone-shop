<?php

namespace App\Utils\Repository\Criteria\LatestModel;

use App\Application\Services\Model\LatestModel\LatestModel;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByLatest
 * @package App\Utils\Repository\Criteria\LatestModel
 */
final class ByLatest implements Criteria
{
    /**
     * BySlug constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(LatestModel::FIELD_IS_LATEST, '=', true);
    }
}
