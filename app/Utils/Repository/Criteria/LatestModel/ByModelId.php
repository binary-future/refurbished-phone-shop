<?php

namespace App\Utils\Repository\Criteria\LatestModel;

use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByModelId
 * @package App\Utils\Repository\Criteria\LatestModel
 */
final class ByModelId implements Criteria
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * BySlug constructor.
     * @param int $modelId
     */
    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where('id', '=', $this->modelId);
    }
}
