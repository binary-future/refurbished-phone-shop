<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

final class PaymentMethodCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'payment-method';

    protected function getContext(): string
    {
        return self::CONTEXT;
    }
}
