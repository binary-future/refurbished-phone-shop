<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class MergeModelFilterCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class MergeModelFilterCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'merge-model-filters';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
