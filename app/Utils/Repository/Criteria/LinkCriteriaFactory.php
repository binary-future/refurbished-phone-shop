<?php

namespace App\Utils\Repository\Criteria;

use App\Utils\Repository\CriteriaFactory;

/**
 * Class LinkCriteriaFactory
 * @package App\Utils\Repository\Criteria
 */
final class LinkCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'link';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
