<?php

namespace App\Utils\Repository\Criteria\PositiveReport;

use App\Application\Services\Importer\Reports\PositiveReport;
use App\Utils\Repository\Contracts\Criteria;
use App\Utils\Repository\Criteria\Common\Objects\DateCompareValue;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByCreatedAtDate
 * @package App\Utils\Repository\Criteria\PositiveReport
 */
final class ByCreatedAtDate implements Criteria
{
    /**
     * @var string
     */
    private $operator;
    /**
     * @var \Carbon\Carbon
     */
    private $date;

    /**
     * ByCreatedAtDate constructor.
     * @param DateCompareValue $date
     */
    public function __construct(DateCompareValue $date)
    {
        $this->date = $date->getValue();
        $this->operator = $date->getOperator();
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(
            PositiveReport::FIELD_CREATED_AT,
            $this->operator,
            $this->date
        );
    }
}
