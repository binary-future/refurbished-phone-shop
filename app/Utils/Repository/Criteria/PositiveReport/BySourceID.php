<?php


namespace App\Utils\Repository\Criteria\PositiveReport;

use App\Application\Services\Importer\Reports\PositiveReport;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

final class BySourceID implements Criteria
{
    /**
     * @var
     */
    private $key;

    /**
     * ByTarget constructor.
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PositiveReport::FIELD_SOURCE_ID, '=', $this->key);
    }
}
