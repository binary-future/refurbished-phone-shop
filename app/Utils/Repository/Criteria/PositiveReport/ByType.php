<?php


namespace App\Utils\Repository\Criteria\PositiveReport;


use App\Application\Services\Importer\Reports\PositiveReport;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByType
 * @package App\Utils\Repository\Criteria\PositiveReport
 */
final class ByType implements Criteria
{
    /**
     * @var
     */
    private $type;

    /**
     * ByType constructor.
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PositiveReport::FIELD_TARGET_TYPE, '=', $this->type);
    }
}
