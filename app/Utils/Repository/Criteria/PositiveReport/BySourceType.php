<?php


namespace App\Utils\Repository\Criteria\PositiveReport;


use App\Application\Services\Importer\Reports\PositiveReport;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BySourceType
 * @package App\Utils\Repository\Criteria\PositiveReport
 */
final class BySourceType implements Criteria
{
    /**
     * @var string
     */
    private $type;

    /**
     * BySourceType constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @param Builder $builder
     * @return Builder|mixed
     */
    public function apply(Builder $builder)
    {
        return $builder->where(PositiveReport::FIELD_SOURCE_TYPE, '=', $this->type);
    }
}
