<?php


namespace App\Utils\Repository\Criteria;


use App\Utils\Repository\CriteriaFactory;

final class NegativeReportCriteriaFactory extends CriteriaFactory
{
    public const CONTEXT = 'negative-import-report';

    /**
     * @return string
     */
    protected function getContext()
    {
        return self::CONTEXT;
    }
}
