<?php


namespace App\Utils\Repository\Criteria\DatafeedsInfo;


use App\Application\Services\Importer\Datafeeds\DatafeedApi;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ByApiName
 * @package App\Utils\Repository\Criteria\DatafeedsInfo
 */
final class ByApiName implements Criteria
{
    /**
     * @var string
     */
    private $apiName;

    /**
     * ByApiName constructor.
     * @param string $apiName
     */
    public function __construct(string $apiName)
    {
        $this->apiName = $apiName;
    }

    /**
     * @param Builder $builder
     * @return mixed|void
     */
    public function apply(Builder $builder)
    {
        return $builder->whereHas('api', function ($query) {
           $query->where(DatafeedApi::FIELD_NAME, '=', $this->apiName);
        });
    }
}
