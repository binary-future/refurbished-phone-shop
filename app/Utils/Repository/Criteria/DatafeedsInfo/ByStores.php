<?php

namespace App\Utils\Repository\Criteria\DatafeedsInfo;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Utils\Repository\Contracts\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

final class ByStores implements Criteria
{
    private $stores;

    /**
     * ByStores constructor.
     * @param $stores
     */
    public function __construct(Collection $stores)
    {
        $this->stores = $stores;
    }

    public function apply(Builder $builder)
    {
        return $builder->whereIn(DatafeedInfo::FIELD_STORE_ID, $this->stores->pluck('id')->all());
    }
}
