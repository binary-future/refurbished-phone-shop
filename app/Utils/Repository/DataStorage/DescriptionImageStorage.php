<?php

namespace App\Utils\Repository\DataStorage;

use App\Utils\Encoders\Contracts\JsonEncoder;

class DescriptionImageStorage
{
    private const FILE_PATH = 'app/index/description-images.json';

    /**
     * @var JsonEncoder
     */
    private $decoder;

    /**
     * DescriptionImageStorage constructor.
     * @param JsonEncoder $decoder
     */
    public function __construct(JsonEncoder $decoder)
    {
        $this->decoder = $decoder;
    }

    public function getContent(): array
    {
        try {
            $content = file_get_contents($this->getFilePath());
            return $content ? $this->decoder->decode($content) : [];
        } catch (\Throwable $exception) {
            return [];
        }
    }

    public function putContent(array $content)
    {
        try {
            $result = file_put_contents($this->getFilePath(), $this->decoder->encode($content));
            return is_numeric($result);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    private function getFilePath(): string
    {
        return storage_path(self::FILE_PATH);
    }
}
