<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Repository\Models;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PhoneModelsRepository
 * @package App\Utils\Repository\Repositories
 */
final class PhoneModelsRepository extends Repository implements Models
{
    protected $cascadeDelete = [
        'description', 'rating'
    ];
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof PhoneModel;
    }
}
