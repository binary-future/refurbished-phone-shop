<?php

namespace App\Utils\Repository\Repositories;

use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TopModelRepository
 * @package App\Utils\Repository\Repositories
 */
final class TopModelsRepository extends Repository implements TopModels
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof TopModel;
    }
}
