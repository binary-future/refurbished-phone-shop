<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Repository\Deals;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DealRepository
 * @package App\Utils\Repository\Repositories
 */
final class DealRepository extends Repository implements Deals
{
    protected $cascadeDelete = [
        'link',
    ];
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Deal;
    }
}