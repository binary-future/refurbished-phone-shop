<?php


namespace App\Utils\Repository\Repositories;


use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PositiveReportRepository
 * @package App\Utils\Repository\Repositories
 */
final class PositiveReportRepository extends Repository implements PositiveReports
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof PositiveReport;
    }
}
