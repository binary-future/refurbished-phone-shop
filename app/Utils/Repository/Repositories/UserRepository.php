<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\User\Repository\Users;
use App\Domain\User\User;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRepository
 * @package App\Utils\Repository\Repositories
 */
final class UserRepository extends Repository implements Users
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof User;
    }
}
