<?php


namespace App\Utils\Repository\Repositories;


use App\Application\Services\Merge\Color\Filter;
use App\Application\Services\Merge\Color\Repository\Filters;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

final class MergeColorFiltersRepository extends Repository implements Filters
{
    protected $cascadeDelete = [
        'children'
    ];

    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Filter;
    }
}
