<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

final class PaymentMethodRepository extends Repository implements PaymentMethods
{
    protected $cascadeDelete = [
        PaymentMethod::RELATION_IMAGE
    ];

    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof PaymentMethod;
    }
}
