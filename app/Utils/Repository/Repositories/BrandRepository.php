<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Repository\Brands;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BrandRepository
 * @package App\Utils\Repository\Repositories
 */
final class BrandRepository extends Repository implements Brands
{
    protected $cascadeDelete = [
        'image', 'description',
    ];
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Brand;
    }
}
