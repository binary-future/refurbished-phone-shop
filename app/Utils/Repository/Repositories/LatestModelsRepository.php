<?php

namespace App\Utils\Repository\Repositories;

use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LatestModelsRepository
 * @package App\Utils\Repository\Repositories
 */
final class LatestModelsRepository extends Repository implements LatestModels
{
    protected function refreshBuilder()
    {
        // LatestModel doesn't exist without instance of PhoneModel
        // it forces update database row instead of inserting new one
        $this->model->exists = true;
        parent::refreshBuilder();
    }

    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof LatestModel;
    }

    public function create(array $params): Model
    {
        return $this->update($this->model, $params);
    }
}
