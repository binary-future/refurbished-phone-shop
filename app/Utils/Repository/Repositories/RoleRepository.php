<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\User\Repository\Roles;
use App\Domain\User\Role;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleRepository
 * @package App\Utils\Repository\Repositories
 */
final class RoleRepository extends Repository implements Roles
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Role;
    }
}