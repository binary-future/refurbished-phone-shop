<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Phone\Faq\Faq;
use App\Domain\Phone\Faq\Repository\Faqs;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

final class FaqsRepository extends Repository implements Faqs
{
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Faq;
    }
}
