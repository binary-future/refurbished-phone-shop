<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Contract\Repository\Networks;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NetworkRepository
 * @package App\Utils\Repository\Repositories
 */
final class NetworkRepository extends Repository implements Networks
{
    protected $cascadeDelete = [
        Network::RELATION_IMAGE
    ];

    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Network;
    }
}
