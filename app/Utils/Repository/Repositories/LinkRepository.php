<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Link\Repository\Links;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LinkRepository
 * @package App\Utils\Repository\Repositories
 */
final class LinkRepository extends Repository implements Links
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Link;
    }
}
