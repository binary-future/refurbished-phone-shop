<?php

namespace App\Utils\Repository\Repositories;

use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PlannedImportsRepository
 * @package App\Utils\Repository\Repositories
 */
final class PlannedImportsRepository extends Repository implements PlannedImports
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof PlannedImport;
    }
}
