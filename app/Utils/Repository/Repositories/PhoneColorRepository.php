<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Repository\Colors;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PhoneColorRepository
 * @package App\Utils\Repository\Repositories
 */
final class PhoneColorRepository extends Repository implements Colors
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Color;
    }
}
