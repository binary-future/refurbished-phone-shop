<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Repository\Contracts;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ContractRepository
 * @package App\Utils\Repository\Repositories
 */
final class ContractRepository extends Repository implements Contracts
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Contract;
    }
}
