<?php


namespace App\Utils\Repository\Repositories;


use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

final class MergeModelFiltersRepository extends Repository implements Filters
{
    protected $cascadeDelete = [
        'children'
    ];

    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Filter;
    }
}
