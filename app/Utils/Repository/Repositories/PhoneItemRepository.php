<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Repository\Phones;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PhoneItemRepository
 * @package App\Utils\Repository\Repositories
 */
final class PhoneItemRepository extends Repository implements Phones
{
    protected $cascadeDelete = [
        'images',
    ];

    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Phone;
    }
}
