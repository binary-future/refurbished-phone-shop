<?php

namespace App\Utils\Repository\Repositories;

use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StoreRepository
 * @package App\Utils\Repository\Repositories
 */
final class StoreRepository extends Repository implements Stores
{
    protected $cascadeDelete = [
        'image', 'link', 'description'
    ];

    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof Store;
    }
}
