<?php


namespace App\Utils\Repository\Repositories;


use App\Application\Services\Importer\Datafeeds\DatafeedApi;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsAPIs;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DatafeedsApiRepository
 * @package App\Utils\Repository\Repositories
 */
final class DatafeedsApiRepository extends Repository implements DatafeedsAPIs
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof DatafeedApi;
    }
}
