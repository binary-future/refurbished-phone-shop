<?php


namespace App\Utils\Repository\Repositories;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DatafeedsInfoRepository
 * @package App\Utils\Repository\Repositories
 */
final class DatafeedsInfoRepository extends Repository implements DatafeedsInfo
{
    /**
     * @param Model $model
     * @return bool
     */
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof DatafeedInfo;
    }
}
