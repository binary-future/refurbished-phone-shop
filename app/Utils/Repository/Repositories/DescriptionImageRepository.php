<?php


namespace App\Utils\Repository\Repositories;

use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\DescriptionImage;
use App\Domain\Shared\Description\Repository\DescriptionImageRepository as Contract;
use App\Utils\Repository\DataStorage\DescriptionImageStorage;
use Illuminate\Support\Collection;

/**
 * Class DescriptionImageRepository
 * @package App\Utils\Repository\Repositories
 */
final class DescriptionImageRepository implements Contract
{
    /**
     * @var DescriptionImageStorage
     */
    private $storage;

    /**
     * DescriptionImageRepository constructor.
     * @param DescriptionImageStorage $storage
     */
    public function __construct(DescriptionImageStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param Description $description
     * @param Collection $images
     * @return bool
     */
    public function sync(Description $description, Collection $images): bool
    {
        $content = $this->storage->getContent();
        $content[$description->getKey()] = $this->translateImages($images);

        return $this->storage->putContent($content);
    }

    /**
     * @param Collection $images
     * @return array
     */
    private function translateImages(Collection $images): array
    {
        return $images->filter(function ($image) {
            return $image instanceof DescriptionImage;
        })->transform(function (DescriptionImage $image) {
            return $image->getPath();
        })->values()->all();
    }

    /**
     * @param Description $description
     * @return bool
     */
    public function remove(Description $description): bool
    {
        $content = $this->storage->getContent();
        unset($content[$description->getKey()]);

        return $this->storage->putContent($content);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        $content = $this->storage->getContent();

        return $this->buildImages($content);
    }

    /**
     * @param array $data
     * @return Collection
     */
    private function buildImages(array $data): Collection
    {
        $result = [];
        foreach ($data as $descriptionId => $paths) {
            if (! is_array($paths)) {
                continue;
            }
            foreach ($paths as $path) {
                $result[] = new DescriptionImage($path, $descriptionId);
            }
        }

        return collect($result);
    }
}
