<?php


namespace App\Utils\Repository\Repositories;


use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Utils\Repository\Repository;
use Illuminate\Database\Eloquent\Model;

final class NegativeReportRepository extends Repository implements NegativeReports
{
    protected function isSatisfy(Model $model): bool
    {
        return $model instanceof NegativeReport;
    }
}
