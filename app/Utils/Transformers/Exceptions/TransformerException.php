<?php

namespace App\Utils\Transformers\Exceptions;

/**
 * Class TransformerException
 * @package App\Utils\Transformers\Exceptions
 */
final class TransformerException extends \InvalidArgumentException
{
    /**
     * @param string $message
     * @return TransformerException
     */
    public static function cannotTransformGivenData(string $message)
    {
        return new self(sprintf('Cannot transform given data. %s', $message));
    }

    /**
     * @param string $alias
     * @param string $error
     * @return TransformerException
     */
    public static function cannotBuildTransformer(string $alias, string $error)
    {
        return new self(
            sprintf('Cannot build transformer by alias - %s. %s', $alias, $error)
        );
    }
}
