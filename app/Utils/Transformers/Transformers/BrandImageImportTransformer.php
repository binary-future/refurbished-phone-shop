<?php


namespace App\Utils\Transformers\Transformers;


use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class BrandImageImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class BrandImageImportTransformer implements Transformer
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * BrandImageImportTransformer constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        $data['owner'] = $this->getBrand($data['brand']);
        $data['image'] = $this->getImage($data['image']);

        return $data;
    }

    /**
     * @param array $params
     * @return Brand
     */
    private function getBrand(array $params): Brand
    {
        $params['slug'] = $this->getSlug($params['slug']);
        /**
         * @var Brand $brand
         */
        $brand = $this->serializer->fromArray(Brand::class, $params);

        return $brand;
    }

    /**
     * @param array $params
     * @return Image
     */
    private function getImage(array $params): Image
    {
        /**
         * @var Image $image
         */
        $image = $this->serializer->fromArray(Image::class, $params);

        return $image;
    }

    /**
     * @param string $string
     * @return string
     */
    private function getSlug(string $string): string
    {
        return array_first(explode('.', $string));
    }
}
