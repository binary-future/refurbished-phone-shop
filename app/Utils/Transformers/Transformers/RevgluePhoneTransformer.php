<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Phone\Phone\Phone;

class RevgluePhoneTransformer extends SerializingTransformer
{
    protected function proceed($data, array $options = [])
    {
        $data['product'] = $this->serialize($data['product']);

        return $data;
    }

    protected function getObject(): string
    {
        return Phone::class;
    }
}
