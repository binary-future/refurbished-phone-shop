<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class PhoneModelBrandIndependentTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class PhoneModelBrandIndependentTransformer extends SerializingTransformer
{
    /**
     * @var CheckAliasGenerator
     */
    private $aliasGenerator;

    /**
     * PhoneModelBrandIndependentTransformer constructor.
     * @param Serializer $serializer
     * @param CheckAliasGenerator $generator
     */
    public function __construct(Serializer $serializer, CheckAliasGenerator $generator)
    {
        parent::__construct($serializer);
        $this->aliasGenerator = $generator;
    }

    /**
     * @param $data
     * @param array $options
     * @return array
     */
    protected function proceed($data, array $options = [])
    {
        /**
         * @var PhoneModel $phoneModel
         */
        $phoneModel = $this->serialize($data['phone_model']);
        $phoneModel->setAlias($this->aliasGenerator->generate($phoneModel->getName()));
        $data['phone_model'] = $phoneModel;

        return $data;
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return PhoneModel::class;
    }
}
