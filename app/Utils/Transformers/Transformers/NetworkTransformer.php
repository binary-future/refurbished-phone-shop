<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Network;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;

final class NetworkTransformer extends SerializingTransformer
{
    private const GROUP_NETWORK = 'network';

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var CheckAliasGenerator
     */
    private $aliasGenerator;

    public function __construct(Serializer $serializer, Generator $generator, CheckAliasGenerator $aliasGenerator)
    {
        parent::__construct($serializer);
        $this->slugGenerator = $generator;
        $this->aliasGenerator = $aliasGenerator;
    }

    protected function proceed($data, array $options = [])
    {
        $networkData = $this->getNetworkData($data[self::GROUP_NETWORK]);
        $data[self::GROUP_NETWORK] = $this->serialize($networkData);

        return $data;
    }

    private function getNetworkData(array $data): array
    {
        $data[Network::FIELD_NAME] = ucfirst($data[Network::FIELD_NAME]);
        $data[Network::FIELD_SLUG] = $this->slugGenerator->generate($data[Network::FIELD_NAME]);
        $data[Network::FIELD_ALIAS] = $this->aliasGenerator->generate($data[Network::FIELD_NAME]);

        return $data;
    }

    protected function getObject(): string
    {
        return Network::class;
    }
}
