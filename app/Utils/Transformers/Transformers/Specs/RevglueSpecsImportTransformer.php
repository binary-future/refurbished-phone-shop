<?php


namespace App\Utils\Transformers\Transformers\Specs;


use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Specs\PhoneSpecs;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Factory;
use App\Utils\Transformers\Transformers\SerializingTransformer;

/**
 * Class RevglueSpecsImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class RevglueSpecsImportTransformer extends SerializingTransformer
{
    public const CONTEXT = 'revglue-specs-transformer';

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * RevglueSpecsImportTransformer constructor.
     * @param Serializer $serializer
     * @param Factory $factory
     */
    public function __construct(Serializer $serializer, Factory $factory)
    {
        parent::__construct($serializer);
        $this->transformer = $factory->buildTransformer(self::CONTEXT);
    }


    /**
     * @param $data
     * @param array $options
     * @return array|mixed
     */
    protected function proceed($data, array $options = [])
    {
        try {
            return $this->getSpecs($data, $options);
        } catch (\Throwable $exception) {
            $data['specs'] = null;

            return $data;
        }
    }

    /**
     * @param array $data
     * @param array $options
     * @return array|mixed
     */
    private function getSpecs(array $data, array $options)
    {
        $data = $this->transformer->transform($data);
        $data['specs'] = $this->serialize($data);

        return $data;
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return PhoneSpecs::class;
    }
}
