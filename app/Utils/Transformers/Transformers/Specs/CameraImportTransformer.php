<?php


namespace App\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Camera;

/**
 * Class CameraImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class CameraImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Camera::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'camera';
    }
}
