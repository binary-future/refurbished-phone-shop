<?php


namespace App\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Autonomy;

/**
 * Class AutonomyImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class AutonomyImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Autonomy::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'autonomy';
    }
}
