<?php


namespace App\Utils\Transformers\Transformers\Specs;

use App\Domain\Common\Contracts\Translators\Translator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Transformers\SerializingTransformer;

/**
 * Class SpecGroupTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
abstract class SpecGroupTransformer extends SerializingTransformer
{
    /**
     * @var Translator
     */
    protected $translator;

    /**
     * SpecGroupTransformer constructor.
     * @param Serializer $serializer
     * @param Translator $translator
     */
    public function __construct(Serializer $serializer, Translator $translator)
    {
        parent::__construct($serializer);
        $this->translator = $translator;
    }

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        $params = $this->translator->translate($data[$this->getDataField()] ?? []);
        $data[$this->getDataField()] = $this->serialize($params);

        return $data;
    }

    /**
     * @return string
     */
    abstract protected function getDataField(): string;
}
