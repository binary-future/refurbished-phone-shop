<?php

namespace App\Utils\Transformers\Transformers\Specs;

use App\Domain\Phone\Specs\PhoneCase;

/**
 * Class PhoneCaseImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class PhoneCaseImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return PhoneCase::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'phone_case';
    }
}
