<?php


namespace App\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\OperatingSystem;

/**
 * Class OperatingSystemImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class OperatingSystemImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return OperatingSystem::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'operating';
    }
}
