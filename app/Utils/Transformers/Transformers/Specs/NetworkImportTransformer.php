<?php


namespace App\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Network;

/**
 * Class NetworkImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class NetworkImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Network::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'network';
    }
}