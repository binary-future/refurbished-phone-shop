<?php


namespace App\Utils\Transformers\Transformers\Specs;


use App\Domain\Phone\Specs\Performance;

/**
 * Class PerformanceImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class PerformanceImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Performance::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'performance';
    }

}