<?php

namespace App\Utils\Transformers\Transformers\Specs;

use App\Domain\Phone\Specs\Connection;

/**
 * Class ConnectionImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class ConnectionImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Connection::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'connection_spec';
    }
}
