<?php


namespace App\Utils\Transformers\Transformers\Specs;

use App\Domain\Phone\Specs\Display;

/**
 * Class DisplayImportTransformer
 * @package App\Utils\Transformers\Transformers\Specs
 */
final class DisplayImportTransformer extends SpecGroupTransformer
{
    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Display::class;
    }

    /**
     * @return string
     */
    protected function getDataField(): string
    {
        return 'display';
    }
}
