<?php

namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Shared\Link\Link;
use App\Utils\Transformers\Exceptions\TransformerException;

class LinkWithNoAffiliationTransformer implements Transformer
{
    private const GROUP_LINK = 'link';

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * LinkWithNoAffiliationTransformer constructor.
     * @param Transformer $transformer
     */
    public function __construct(Transformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function transform($data, array $options = [])
    {
        try {
            return $this->proceed($data);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData('Link data is invalid');
        }
    }

    protected function proceed($data)
    {
        $data[self::GROUP_LINK][Link::FIELD_LINK] = $this->parseLink(
            $data[self::GROUP_LINK][Link::FIELD_LINK]
        );

        return $this->transformer->transform($data);
    }

    private function parseLink(string $link): string
    {
        $pattern = '/(=)(https?:\/\/.*)/';
        preg_match(
            $pattern,
            urldecode($link),
            $matches
        );

        return $this->removeRefer(end($matches) ?: $link);
    }

    private function removeRefer(string $link): string
    {
        return str_ireplace('&affiliate=!!!refer!!!', '', $link);
    }
}
