<?php


namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Transformers\Exceptions\TransformerException;

final class WebgainsAffordablePhoneModelTransformer implements Transformer
{
    /**
     * @var Transformer
     */
    private $transformer;

    private $capacity;

    /**
     * WebgainsPhoneModelTransformer constructor.
     * @param Transformer $transformer
     */
    public function __construct(Transformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function transform($data, array $options = [])
    {
        try {
            return $this->proceed($data);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData($exception->getMessage());
        }
    }

    private function proceed(array $data)
    {
        $this->capacity = $this->findCapacity($this->getModelName($data));
        if (! $this->capacity) {
            throw TransformerException::cannotTransformGivenData('No capacity found');
        }
        $data['phone_model'] = $this->preparePhoneModelData($data);
        $data['color'] = $this->prepareProductColor($data);
        $data['product'] = $this->prepareProductData();

        return $this->transformer->transform($data);
    }

    private function findCapacity(string $modelName): ?string
    {
        foreach ($this->capacityList() as $capacity) {
            if (stripos($modelName, $capacity) !== false) {
                return $capacity;
            }
        }

        return null;
    }

    private function capacityList(): array
    {
        return array_reverse(Phone::CAPACITIES);
    }

    private function getModelName(array $data): string
    {
        return $data['product']['name'];
    }

    private function preparePhoneModelData(array $data): array
    {
        $nameParts = $this->splitNameByCapacity($this->getModelName($data));
        $modelName = trim(
            str_ireplace(
                [$this->capacity, $data['brand']->getName()],
                '',
                array_first($nameParts)
            )
        );

        return [
            'name' => $modelName
        ];
    }

    private function splitNameByCapacity(string $name): array
    {
        return explode($this->capacity, $name);
    }

    private function prepareProductData(): array
    {
        return [
            Phone::FIELD_CAPACITY => $this->capacity
        ];
    }

    private function prepareProductColor(array $data)
    {
        $nameParts = $this->splitNameByCapacity($this->getModelName($data));
        $color = trim(array_last($nameParts));
        if (! $color) {
            throw TransformerException::cannotTransformGivenData('No color info found');
        }

        return [
            'name' => $color
        ];
    }
}