<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Shared\Description\Description;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class DescriptionImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class DescriptionImportTransformer extends SerializingTransformer
{
    private const GROUP_DESCRIPTION = 'description';

    /**
     * @var Specification
     */
    private $specification;

    /**
     * DescriptionImportTransformer constructor.
     * @param Serializer $serializer
     * @param Specification $specification
     */
    public function __construct(Serializer $serializer, Specification $specification)
    {
        parent::__construct($serializer);
        $this->specification = $specification;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        $description = $this->serialize($data[self::GROUP_DESCRIPTION]);
        $data[self::GROUP_DESCRIPTION] = $this->specification->isSatisfy($description)
            ? $description
            : null;

        return $data;
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Description::class;
    }
}
