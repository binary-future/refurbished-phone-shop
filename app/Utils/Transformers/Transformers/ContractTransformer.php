<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Contract;

/**
 * Class ContractTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class ContractTransformer extends SerializingTransformer
{
    private const GROUP_CONTRACT = 'contract';

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        /**
         * @var Contract $contract
         */
        $contract = $this->getContract($data[self::GROUP_CONTRACT]);
        $data[self::GROUP_CONTRACT] = $contract;

        return $data;
    }

    /**
     * @param array $contractData
     * @return Contract
     */
    private function getContract(array $contractData): Contract
    {
        $contractData[Contract::FIELD_CONDITION] = $this->prepareCondition($contractData[Contract::FIELD_CONDITION]);

        /** @var Contract $contract */
        $contract = $this->serialize($contractData);

        return $contract;
    }

    private function prepareCondition(string $condition): Condition
    {
        $formattedCondition = strtolower(trim($condition));
        return new Condition($formattedCondition);
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Contract::class;
    }
}
