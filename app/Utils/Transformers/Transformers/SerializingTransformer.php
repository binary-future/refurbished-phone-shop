<?php

namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class SerializingTransformer
 * @package App\Utils\Transformers\Transformers
 */
abstract class SerializingTransformer implements Transformer
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SerializingTransformer constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        try {
            return $this->proceed($data, $options);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData($exception->getMessage());
        }
    }

    abstract protected function proceed($data, array $options = []);

    /**
     * @return string
     */
    abstract protected function getObject(): string;

    /**
     * @param array $data
     * @return object
     */
    protected function serialize(array $data)
    {
        return $this->serializer->fromArray($this->getObject(), $data);
    }
}
