<?php

namespace App\Utils\Transformers\Transformers\Awin;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class AwinContractTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class AwinContractTransformer implements Transformer
{
    private const GROUP_CONTRACT = 'contract';

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * AwinPhoneTransformer constructor.
     * @param Transformer $transformer
     */
    public function __construct(Transformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        try {
            $data[self::GROUP_CONTRACT][Contract::FIELD_CONDITION] = $this->prepareCondition(
                $data[self::GROUP_CONTRACT][Contract::FIELD_CONDITION]
            );

            return $this->transformer->transform($data);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData($exception->getMessage());
        }
    }

    private function prepareCondition(string $condition): string
    {
        $map = $this->conditionMap();
        $sanitized = trim(str_replace('condition', '', strtolower($condition)));

        $matches = 0;
        $finalCondition = str_replace(array_keys($map), array_values($map), $sanitized, $matches);

        if ($matches === 0) {
            throw TransformerException::cannotTransformGivenData('Condition not found');
        }

        return $finalCondition;
    }

    private function conditionMap(): array
    {
        return [
            'pristine' => Condition::PRISTINE(),
            'very good' => Condition::VERY_GOOD(),
            'good' => Condition::GOOD(),
        ];
    }
}
