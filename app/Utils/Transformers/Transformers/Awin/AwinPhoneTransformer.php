<?php

namespace App\Utils\Transformers\Transformers\Awin;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class AwinPhoneTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class AwinPhoneTransformer implements Transformer
{
    private const GROUP_PHONE = 'phone';
    private const GROUP_PRODUCT = 'product';

    /**
     * @var Transformer
     */
    private $phoneTransformer;

    /**
     * AwinPhoneTransformer constructor.
     * @param Transformer $transformer
     */
    public function __construct(Transformer $transformer)
    {
        $this->phoneTransformer = $transformer;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        try {
            $data[self::GROUP_PHONE] = $data[self::GROUP_PRODUCT];
            $result = $this->phoneTransformer->transform($data);
            $result[self::GROUP_PRODUCT] = $result[self::GROUP_PHONE];

            return $result;
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData($exception->getMessage());
        }
    }
}
