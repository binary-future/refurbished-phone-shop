<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Shared\Rating\Rating;
use App\Domain\Shared\Rating\RatingValueInPercents;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class RatingImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class RatingImportTransformer extends SerializingTransformer
{
    private const GROUP_RATING = 'rating';

    protected function getObject(): string
    {
        return Rating::class;
    }

    protected function proceed($data, array $options = []): array
    {
        $data[self::GROUP_RATING] = $this->serialize(
            $this->prepareRatingData($data[self::GROUP_RATING])
        );

        return $data;
    }

    private function prepareRatingData(array $rating): array
    {
        $rating[Rating::FIELD_RATING] = new RatingValueInPercents($rating[self::GROUP_RATING]);

        return $rating;
    }
}
