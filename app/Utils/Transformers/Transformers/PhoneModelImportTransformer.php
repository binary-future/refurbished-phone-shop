<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class PhoneModelImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class PhoneModelImportTransformer extends SerializingTransformer
{
    private const GROUP_PHONE_MODEL = 'phone_model';
    private const GROUP_BRAND = 'brand';

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var CheckAliasGenerator
     */
    private $aliasGenerator;

    /**
     * @var Specification
     */
    private $specification;

    /**
     * PhoneModelImportTransformer constructor.
     * @param Serializer $serializer
     * @param Generator $generator
     * @param CheckAliasGenerator $alisGenerator
     * @param Specification $specification
     */
    public function __construct(
        Serializer $serializer,
        Generator $generator,
        CheckAliasGenerator $alisGenerator,
        Specification $specification
    ) {
        parent::__construct($serializer);
        $this->slugGenerator = $generator;
        $this->aliasGenerator = $alisGenerator;
        $this->specification = $specification;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        $model = $this->getPhoneModel($data[self::GROUP_PHONE_MODEL], $data[self::GROUP_BRAND]);
        if (! $this->specification->isSatisfy($model)) {
            throw TransformerException::cannotTransformGivenData('Invalid phone model data');
        }

        $data[self::GROUP_PHONE_MODEL] = $model;

        return $data;
    }

    private function getPhoneModel(array $phoneModelData, Brand $brand): PhoneModel
    {
        $modelName = $phoneModelData[PhoneModel::FIELD_NAME];
        $phoneModelData[PhoneModel::FIELD_SLUG] = $this->getSlug($modelName);
        $phoneModelData[PhoneModel::FIELD_ALIAS] = $this->getAlias($modelName, $brand);

        /** @var PhoneModel $phoneModel */
        $phoneModel = $this->serialize($phoneModelData);
        return $phoneModel;
    }

    /**
     * @param string $modelName
     * @return string
     */
    private function getSlug(string $modelName): string
    {
        return $this->slugGenerator->generate($modelName);
    }

    /**
     * @param string $modelName
     * @param Brand $brand
     * @return string
     */
    private function getAlias(string $modelName, Brand $brand): string
    {
        $alias = sprintf('%s %s', $brand->getName(), $modelName);

        return $this->aliasGenerator->generate($alias);
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return PhoneModel::class;
    }
}
