<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Shared\Link\Link;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;

final class LinkTransformer extends SerializingTransformer
{
    private const GROUP_LINK = 'link';

    /**
     * @var Specification
     */
    private $specification;

    /**
     * LinkTransformer constructor.
     * @param Serializer $serializer
     * @param Specification $specification
     */
    public function __construct(Serializer $serializer, Specification $specification)
    {
        parent::__construct($serializer);
        $this->specification = $specification;
    }

    protected function proceed($data, array $options = [])
    {
        $data[self::GROUP_LINK] = $this->getLink($data[self::GROUP_LINK]);

        return $data;
    }

    /**
     * @param array $linkData
     * @return object
     */
    private function getLink(array $linkData)
    {
        $link = $this->serialize($linkData);
        if (! $this->specification->isSatisfy($link)) {
            throw TransformerException::cannotTransformGivenData('Invalid link generated');
        }

        return $link;
    }

    protected function getObject(): string
    {
        return Link::class;
    }
}
