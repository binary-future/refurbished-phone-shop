<?php

namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Transformers\Exceptions\TransformerException;

final class StructureByMapTransformer implements Transformer
{
    /**
     * @var array
     */
    private $result = [];

    public function transform($data, array $options = [])
    {
        try {
            return empty($options)
                ? $data
                : $this->proceedTransforming($data, $options);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData($exception->getMessage());
        }
    }

    private function proceedTransforming(array $data, array $options)
    {
        foreach ($options as $alias => $schema) {
            isset($schema['group'])
                ? $this->result[$schema['group']][$schema['alias']] = $data[$alias] ?? ''
                : $this->result[$schema['alias']] = $data[$alias] ?? '';
        }

        return $this->result;
    }
}
