<?php


namespace App\Utils\Transformers\Transformers;


use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Transformers\Exceptions\TransformerException;

final class WebgainsPhoneModelTransformer implements Transformer
{
    /**
     * @var Transformer
     */
    private $transformer;

    private $capacity;

    /**
     * WebgainsPhoneModelTransformer constructor.
     * @param Transformer $transformer
     */
    public function __construct(Transformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function transform($data, array $options = [])
    {
        try {
            return $this->proceed($data);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotTransformGivenData($exception->getMessage());
        }
    }

    private function proceed(array $data)
    {
        $data['phone_model'] = $this->preparePhoneModelData($data);
        $data['product'] = $this->prepareProductData($data);

        return $this->transformer->transform($data);
    }

    private function preparePhoneModelData(array $data): array
    {
        $modelName = $data['product']['name'];
        $this->capacity = $this->findCapacity($modelName);
        $modelName = trim(str_ireplace([$this->capacity, $data['brand']->getName()], '', $modelName));

        return [
            'name' => $modelName
        ];
    }

    private function findCapacity(string $modelName): ?string
    {
        foreach ($this->capacityList() as $capacity) {
            if (stripos($modelName, $capacity) !== false) {
                return $capacity;
            }
        }

        return null;
    }

    private function capacityList(): array
    {
        return array_reverse(Phone::CAPACITIES);
    }

    private function prepareProductData(array $data): array
    {
        if (! $this->capacity) {
            throw TransformerException::cannotTransformGivenData('No capacity found');
        }
        return [
            Phone::FIELD_CAPACITY => $this->capacity
        ];
    }
}