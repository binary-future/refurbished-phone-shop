<?php


namespace App\Utils\Transformers\Transformers;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;

/**
 * Class DatafeedInfoTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class DatafeedInfoTransformer extends SerializingTransformer
{
    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        $data['datafeed'] = isset($data['datafeed']) ? $this->buildDatafeed($data['datafeed']) : null;

        return $data;
    }

    private function buildDatafeed(array $data): ?DatafeedInfo
    {
        try {
            /**
             * @var DatafeedInfo $datafeed
             */
            $datafeed = $this->serialize($data);
            return $datafeed->hasApiValues() ? $datafeed : null;
        } catch (\Throwable $exception) {
            return null;
        }
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return DatafeedInfo::class;
    }
}
