<?php

namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Deals\Contract\Network;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;

class NetworkImageImportTransformer implements Transformer
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * NetworkImageImportTransformer constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        $data['owner'] = $this->getOperator($data['network']);
        $data['image'] = $this->getImage($data['image']);

        return $data;
    }

    /**
     * @param array $params
     * @return Network
     */
    private function getOperator(array $params): Network
    {
        $params['slug'] = $this->getSlug($params['slug']);
        /**
         * @var Network $network
         */
        $network = $this->serializer->fromArray(Network::class, $params);

        return $network;
    }

    /**
     * @param array $params
     * @return Image
     */
    private function getImage(array $params): Image
    {
        /**
         * @var Image $image
         */
        $image = $this->serializer->fromArray(Image::class, $params);

        return $image;
    }

    /**
     * @param string $string
     * @return string
     */
    private function getSlug(string $string): string
    {
        return array_first(explode('.', $string));
    }
}
