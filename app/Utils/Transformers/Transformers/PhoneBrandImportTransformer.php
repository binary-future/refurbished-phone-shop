<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Phone\Brand\Brand;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class PhoneBrandImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class PhoneBrandImportTransformer extends SerializingTransformer
{
    private const GROUP_BRAND = 'brand';

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var Specification
     */
    private $specification;

    /**
     * PhoneBrandImportTransformer constructor.
     * @param Serializer $serializer
     * @param Generator $generator
     * @param Specification $specification
     */
    public function __construct(Serializer $serializer, Generator $generator, Specification $specification)
    {
        parent::__construct($serializer);
        $this->slugGenerator = $generator;
        $this->specification = $specification;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->getBrand($data[self::GROUP_BRAND]);

        if (! $this->specification->isSatisfy($brand)) {
            throw TransformerException::cannotTransformGivenData('Invalid brand data');
        }

        $data[self::GROUP_BRAND] = $brand;

        return $data;
    }

    /**
     * @param array $brandData
     * @return Brand
     */
    private function getBrand(array $brandData): Brand
    {
        $slug = $this->getSlug($brandData[Brand::FIELD_NAME]);
        $brandData[Brand::FIELD_SLUG] = $slug;
        $brandData[Brand::FIELD_ALIAS] = $slug;

        /** @var Brand $brand */
        $brand = $this->serialize($brandData);
        return $brand;
    }

    /**
     * @param string $name
     * @return string
     */
    private function getSlug(string $name): string
    {
        return $this->slugGenerator->generate($name);
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Brand::class;
    }
}
