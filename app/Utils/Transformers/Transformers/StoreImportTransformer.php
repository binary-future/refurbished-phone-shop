<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Store\Store;

/**
 * Class StoreImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class StoreImportTransformer extends SerializingTransformer
{
    private const GROUP_STORE = 'store';

    protected function proceed($data, array $options = [])
    {
        $storeData = $this->prepareStoreData($data[self::GROUP_STORE]);

        $data[self::GROUP_STORE] = $this->serialize($storeData);

        return $data;
    }

    private function prepareStoreData(array $storeData): array
    {
        $storeData[Store::FIELD_SLUG] = strtolower($storeData[Store::FIELD_SLUG]);
        $storeData[Store::FIELD_ALIAS] = $storeData[Store::FIELD_SLUG];
        $storeData[Store::FIELD_TERMS] = (int)$storeData[Store::FIELD_TERMS];

        return $storeData;
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Store::class;
    }
}
