<?php

namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class PhonesImageImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class PhonesImageImportTransformer implements Transformer
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * PhonesImageImportTransformer constructor.
     * @param Translator $translator
     * @param Serializer $serializer
     */
    public function __construct(Translator $translator, Serializer $serializer)
    {
        $this->translator = $translator;
        $this->serializer = $serializer;
    }

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        $data['owner'] = $this->getPhone($data['phone']);
        $data['image'] = $this->getImage($data['image']);

        return $data;
    }

    /**
     * @param array $params
     * @return Phone
     */
    private function getPhone(array $params): Phone
    {
        $translatedParams = $this->translator->translate($params);
        /**
         * @var Phone $phone
         */
        $phone = $this->serializer->fromArray(Phone::class, $translatedParams);

        return $phone;
    }

    /**
     * @param array $params
     * @return Image
     */
    private function getImage(array $params): Image
    {
        /**
         * @var Image $image
         */
        $image = $this->serializer->fromArray(Image::class, $params);
        $image->setMain($this->isMainImage($params['path']));

        return $image;
    }

    /**
     * @param string $path
     * @return bool
     */
    private function isMainImage(string $path): bool
    {
        return stripos($path, '_') === false;
    }
}
