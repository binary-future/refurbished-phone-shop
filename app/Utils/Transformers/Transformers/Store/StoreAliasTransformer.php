<?php

namespace App\Utils\Transformers\Transformers\Store;

use App\Domain\Store\Store;
use App\Utils\Transformers\Transformers\SerializingTransformer;

/**
 * Class StoreAliasTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class StoreAliasTransformer extends SerializingTransformer
{
    private const GROUP_STORE = 'store';

    protected function proceed($data, array $options = []): array
    {
        /**
         * @var Store $store
         */
        $store = $this->serialize($data[self::GROUP_STORE]);
        $store->setAlias($store->getSlug());
        $data[self::GROUP_STORE] = $store;

        return $data;
    }

    protected function getObject(): string
    {
        return Store::class;
    }
}
