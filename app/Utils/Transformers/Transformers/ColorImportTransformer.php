<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Deals\Contract\Network;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class ColorImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class ColorImportTransformer extends SerializingTransformer
{
    private const GROUP_COLOR = 'color';
    private const GROUP_IMAGE = 'image';
    private const GROUP_PRODUCT = 'product';
    private const GROUP_BRAND = 'brand';
    private const GROUP_PHONE_MODEL = 'phone_model';
    private const GROUP_NETWORK = 'network';

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var CheckAliasGenerator
     */
    private $aliasGenerator;

    /**
     * ColorImportTransformer constructor.
     * @param Serializer $serializer
     * @param Generator $slugGenerator
     * @param CheckAliasGenerator $aliasGenerator
     */
    public function __construct(Serializer $serializer, Generator $slugGenerator, CheckAliasGenerator $aliasGenerator)
    {
        parent::__construct($serializer);
        $this->slugGenerator = $slugGenerator;
        $this->aliasGenerator = $aliasGenerator;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        $colorName = $this->getColorName(
            $data[self::GROUP_IMAGE],
            $data[self::GROUP_PRODUCT]['name'],
            $data[self::GROUP_BRAND],
            $data[self::GROUP_PHONE_MODEL],
            $data[self::GROUP_NETWORK]
        );

        if (! $colorName) {
            throw TransformerException::cannotTransformGivenData('Color not found');
        }

        $data[self::GROUP_COLOR] = $this->getColor($colorName);

        return $data;
    }

    private function getColor(string $colorName): Color
    {
        /**
         * @var Color $color
         */
        $color = $this->serialize([
            Color::FIELD_NAME => $colorName,
            Color::FIELD_SLUG => $this->slugGenerator->generate($colorName),
            Color::FIELD_ALIAS => $this->aliasGenerator->generate($colorName),
        ]);

        return $color;
    }

    private function getColorName(
        array $imagesData,
        string $productName,
        Brand $brand,
        PhoneModel $phoneModel,
        Network $network
    ): string {
        $colorName = $this->getColorNameFromImagePath($imagesData[Image::FIELD_PATH]);

        if (! $colorName) {
            $colorName = $this->getColorNameFromProductName(
                $productName,
                $brand,
                $phoneModel,
                $network
            );
        }

        return $colorName;
    }

    /**
     * @param string $imagePath
     * @return bool|string
     */
    private function getColorNameFromImagePath(string $imagePath)
    {
        // image path should be in the next format
        // "https://store.musicmagpie.co.uk/spree/products/2744386/product/AI000000010478__Midnight_Black__1.jpg"
        $imagePathParts = explode('__', $imagePath);

        if (count($imagePathParts) < 3) {
            return false;
        }

        $colorNameWithUnderscore = $imagePathParts[1];

        return str_replace('_', ' ', $colorNameWithUnderscore);
    }

    private function getColorNameFromProductName(
        string $productName,
        Brand $brand,
        PhoneModel $phoneModel,
        Network $network
    ): string {
        // examples of product name:
        // OnePlus 3 Soft Gold O2 - Refurbished / Used
        // OnePlus 6 256GB Midnight Black UNLOCKED - Refurbished / Used

        // get first part of product name without 'Refurbished / Used' information
        $productTitle = current(explode('-', $productName));

        $removeValues = [
            $phoneModel->getName(),
            $brand->getName(),
            $network->getName()
        ];

        $onSanitizingProductName = str_ireplace($removeValues, '', $productTitle);

        // remove capacity if exists
        $colorName = preg_replace('/\b\d+\w+\b/', '', $onSanitizingProductName);

        return trim($colorName);
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Color::class;
    }
}
