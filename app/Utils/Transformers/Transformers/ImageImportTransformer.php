<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Shared\Image\Image;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;

final class ImageImportTransformer extends SerializingTransformer
{
    private const GROUP_IMAGE = 'image';
    /**
     * @var Specification
     */
    private $specification;

    public function __construct(Serializer $serializer, Specification $specification)
    {
        parent::__construct($serializer);
        $this->specification = $specification;
    }

    protected function proceed($data, array $options = [])
    {
        $image = $this->serialize($this->prepareParams($data[self::GROUP_IMAGE]));
        $data[self::GROUP_IMAGE] = $this->specification->isSatisfy($image)
            ? $image
            : null;

        return $data;
    }

    private function prepareParams(array $imageData): array
    {
        $imageData[Image::FIELD_PATH] = $this->transformPath($imageData[Image::FIELD_PATH] ?? '');

        return $imageData;
    }

    private function transformPath(string $path): string
    {
        if (strpos($path, '?') !== false) {
            return trim(current(explode('?', $path)));
        }

        return $path;
    }

    protected function getObject(): string
    {
        return Image::class;
    }
}
