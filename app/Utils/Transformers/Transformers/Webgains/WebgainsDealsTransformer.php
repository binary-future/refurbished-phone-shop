<?php

namespace App\Utils\Transformers\Transformers\Webgains;

use App\Domain\Deals\Deal\Deal;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Transformers\DealTransformer;
use App\Utils\Transformers\Transformers\SerializingTransformer;

final class WebgainsDealsTransformer extends SerializingTransformer
{
    private const GROUP_DEAL = 'deal';
    private const GROUP_PRODUCT = 'product';
    /** @var DealTransformer */
    private $transformer;

    /**
     * AwinDealsTransformer constructor.
     * @param Serializer $serializer
     * @param DealTransformer $transformer
     */
    public function __construct(Serializer $serializer, DealTransformer $transformer)
    {
        parent::__construct($serializer);
        $this->transformer = $transformer;
    }

    protected function proceed($data, array $options = [])
    {
        $data[self::GROUP_DEAL]['is_refurbished'] = $this->isRefurbished($data);

        return $this->transformer->transform($data, $options);
    }

    private function isRefurbished(array $data): bool
    {
        return stripos($data[self::GROUP_PRODUCT]['name'], 'refurb') !== false;
    }

    protected function getObject(): string
    {
        return Deal::class;
    }
}
