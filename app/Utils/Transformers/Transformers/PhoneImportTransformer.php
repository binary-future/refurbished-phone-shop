<?php

namespace App\Utils\Transformers\Transformers;

use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Transformers\Exceptions\TransformerException;

/**
 * Class PhoneImportTransformer
 * @package App\Utils\Transformers\Transformers
 */
final class PhoneImportTransformer extends SerializingTransformer
{
    private const GROUP_PHONE = 'phone';
    private const GROUP_PHONE_MODEL = 'phone_model';
    private const GROUP_COLOR = 'color';

    /**
     * @var Specification
     */
    private $specification;

    /**
     * @var CheckAliasGenerator
     */
    private $generator;

    /**
     * PhoneImportTransformer constructor.
     * @param Serializer $serializer
     * @param Specification $specification
     * @param CheckAliasGenerator $generator
     */
    public function __construct(Serializer $serializer, Specification $specification, CheckAliasGenerator $generator)
    {
        parent::__construct($serializer);
        $this->specification = $specification;
        $this->generator = $generator;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    protected function proceed($data, array $options = [])
    {
        $phone = $this->getPhone(
            $data[self::GROUP_PHONE],
            $data[self::GROUP_PHONE_MODEL],
            $data[self::GROUP_COLOR]
        );
        if (! $this->specification->isSatisfy($phone)) {
            throw TransformerException::cannotTransformGivenData('Invalid phone data');
        }

        $data[self::GROUP_PHONE] = $phone;

        return $data;
    }

    /**
     * @param array $phoneData
     * @param PhoneModel $phoneModel
     * @param Color $colorData
     * @return Phone
     */
    private function getPhone(array $phoneData, PhoneModel $phoneModel, Color $colorData): Phone
    {
        $phoneData = $this->setCapacityFromProductName($phoneData);

        /**
         * @var Phone $phone
         */
        $phone = $this->serialize($phoneData);
        $phone->setCheckHash($this->getCheckHash($phone, $phoneModel, $colorData));

        return $phone;
    }

    private function getCheckHash(Phone $phone, PhoneModel $model, Color $color = null): string
    {
        $nameParts = [
            $model->getAlias()
        ];

        if ($color) {
            $nameParts[] = $color->getName();
        }

        if ($phone->getCapacity()) {
            $nameParts[] = $phone->getCapacity();
        }

        return $this->generator->generateHash(implode(' ', $nameParts));
    }

    private function setCapacityFromProductName(array $phoneData): array
    {
        $capacity = $this->findCapacity($phoneData['name']);
        if ($capacity === null) {
            throw TransformerException::cannotTransformGivenData('No capacity was found');
        }

        $phoneData[Phone::FIELD_CAPACITY] = $capacity;
        unset($phoneData['name']);

        return $phoneData;
    }

    /**
     * @param string $productName
     * @return int|null
     */
    private function findCapacity(string $productName): ?int
    {
        $parts = explode(' ', $productName);
        $capacity = null;

        foreach ($parts as $part) {
            if (strpos($part, 'GB')) {
                $capacity = (int)$part;
                break;
            }
        }

        return $capacity;
    }

    /**
     * @return string
     */
    protected function getObject(): string
    {
        return Phone::class;
    }
}
