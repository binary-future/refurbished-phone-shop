<?php

namespace App\Utils\Transformers\Transformers;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Payment;

final class DealTransformer extends SerializingTransformer
{
    private const GROUP_DEAL = 'deal';
    private const GROUP_DEAL_PAYMENT = 'deal-payment';
    private const OPTION_SCENARIO_CONTENT = 'scenario_content';
    private const DEFAULT_UPFRONT_COST = 0.0;

    protected function proceed($data, array $options = [])
    {
        $data[self::GROUP_DEAL] = $this->getDeal($data, $options);

        return $data;
    }

    private function getDeal($data, $options = [])
    {
        /**
         * @var DatafeedInfo $datafeed
         */
        $datafeed = $options[self::OPTION_SCENARIO_CONTENT];
        $store = $datafeed->getStore();

        $data[self::GROUP_DEAL][Deal::OBJECT_PAYMENT] = $this->getDealPayment(
            $data[self::GROUP_DEAL_PAYMENT],
            $store->getTerms()
        );
        $data[self::GROUP_DEAL][Deal::FIELD_STORE_ID] = $datafeed->getStoreId();

        return $this->serialize($data[self::GROUP_DEAL]);
    }

    private function getDealPayment(array $paymentData, int $storeTerms): Payment
    {
        $monthlyCost = $paymentData[Deal::FIELD_TOTAL_COST] / $storeTerms;
        return new Payment(
            self::DEFAULT_UPFRONT_COST,
            (float) $monthlyCost,
            (int) round($paymentData[Deal::FIELD_TOTAL_COST])
        );
    }

    protected function getObject(): string
    {
        return Deal::class;
    }
}
