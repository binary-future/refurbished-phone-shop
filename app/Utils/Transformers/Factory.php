<?php

namespace App\Utils\Transformers;

use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Transformers\Contracts\CompositeTransformer;
use App\Utils\Transformers\Exceptions\TransformerException;

class Factory
{
    private const CONFIG_PATH = 'transformers.schema';
    public const DEFAULT = 'default';

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var []
     */
    private $schema;

    /**
     * Factory constructor.
     * @param Container $container
     * @param Config $config
     */
    public function __construct(Container $container, Config $config)
    {
        $this->container = $container;
        $this->config = $config;
        $this->configure();
    }

    /**
     *
     */
    private function configure()
    {
        $this->schema = $this->config->get(self::CONFIG_PATH, []);
    }

    /**
     * @param string $alias
     * @return Transformer
     */
    public function buildTransformer(string $alias): Transformer
    {
        try {
            return $this->proceedBuilding($alias);
        } catch (\Throwable $exception) {
            throw TransformerException::cannotBuildTransformer($alias, $exception->getMessage());
        }
    }

    /**
     * @param string $alias
     * @return Transformer
     */
    private function proceedBuilding(string $alias): Transformer
    {
        $schema = $this->getSchemaByAlias($alias);
        $transformer = $this->getInstance($schema['base']);

        return $this->shouldBeFilled($transformer, $schema)
            /**
             * @var CompositeTransformer $transformer
             */
            ? $this->fillTransformers($transformer, $schema['items'])
            : $transformer;
    }

    /**
     * @param Transformer $transformer
     * @param array $schema
     * @return bool
     */
    private function shouldBeFilled(Transformer $transformer, array $schema)
    {
        return $transformer instanceof CompositeTransformer
            && isset($schema['items']);
    }

    /**
     * @param CompositeTransformer $composite
     * @param array $items
     * @return CompositeTransformer
     */
    private function fillTransformers(CompositeTransformer $composite, array $items)
    {
        foreach ($items as $item) {
            $composite->addTransformer($this->getInstance($item));
        }

        return $composite;
    }

    /**
     * @param string $alias
     * @return array
     */
    private function getSchemaByAlias(string $alias): array
    {
        return $this->schema[$alias] ?? $this->schema[self::DEFAULT];
    }

    /**
     * @param string $className
     * @return Transformer
     */
    private function getInstance(string $className): Transformer
    {
        return $this->container->resolve($className);
    }
}
