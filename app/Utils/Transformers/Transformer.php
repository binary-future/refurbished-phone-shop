<?php

namespace App\Utils\Transformers;

use App\Utils\Transformers\Contracts\CompositeTransformer;
use App\Application\Services\Importer\Transformers\Contracts\Transformer as Item;

/**
 * Class Transformer
 * @package App\Utils\Transformers
 */
final class Transformer implements CompositeTransformer
{
    /**
     * @var Item[]
     */
    private $transformers = [];

    /**
     * @param Item $transformer
     * @return mixed|void
     */
    public function addTransformer(Item $transformer)
    {
        $this->transformers[] = $transformer;
    }

    /**
     * @param $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = [])
    {
        foreach ($this->transformers as $transformer) {
            $data = $transformer->transform($data, $options);
        }

        return $data;
    }
}
