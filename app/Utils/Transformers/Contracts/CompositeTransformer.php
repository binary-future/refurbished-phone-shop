<?php

namespace App\Utils\Transformers\Contracts;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;

/**
 * Interface CompositeTransformer
 * @package App\Utils\Transformers\Contracts
 */
interface CompositeTransformer extends Transformer
{
    /**
     * @param Transformer $transformer
     * @return mixed
     */
    public function addTransformer(Transformer $transformer);
}
