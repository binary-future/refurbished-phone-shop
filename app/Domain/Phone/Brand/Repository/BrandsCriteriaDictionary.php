<?php

namespace App\Domain\Phone\Brand\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface BrandsCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_ALIAS = 'alias';
    public const CRITERIA_BY_SLUG = 'slug';
    public const CRITERIA_BY_ACTIVE_MODELS = 'active-models';
    public const CRITERIA_BY_ID = 'id';
}
