<?php

namespace App\Domain\Phone\Brand\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface Brands
 * @package App\Domain\Phone\Brand\Repository
 */
interface Brands extends Repository
{

}
