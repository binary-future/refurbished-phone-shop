<?php


namespace App\Domain\Phone\Brand\Query;


/**
 * Class ByIdQueryHandler
 * @package App\Domain\Phone\Brand\Query
 */
final class ByIdQueryHandler extends SimpleQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByIdQuery;
    }
}
