<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary;

/**
 * Class AllQuery
 * @package App\Domain\Phone\Brand\Query
 */
final class AllQuery implements Query, BrandsCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations = [];

    /**
     * AllQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => [],
            self::WITH => $this->relations
        ];
    }
}
