<?php


namespace App\Domain\Phone\Brand\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary;

final class ByIdQuery implements Query, BrandsCriteriaDictionary
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * ByIdQuery constructor.
     * @param int $id
     * @param array $relations
     */
    public function __construct(int $id, array $relations = [])
    {
        $this->id = $id;
        $this->relations = $relations;
    }

    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::ID => $this->id,
        ];
    }
}
