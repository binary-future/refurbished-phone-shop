<?php

namespace App\Domain\Phone\Brand\Query;

/**
 * Class BySlugQueryHandler
 * @package App\Domain\Phone\Brand\Query
 */
final class BySlugActiveQueryHandler extends SimpleQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof BySlugActiveQuery;
    }
}
