<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary;
use App\Domain\Phone\Model\PhoneModel;

/**
 * Class AllActiveQuery
 * @package App\Domain\Phone\Brand\Query
 */
final class AllActiveQuery implements Query, BrandsCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations = [];

    /**
     * AllQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations,
            self::SELECT => [Brand::TABLE . '.*'],
            self::CRITERIA_BY_ACTIVE_MODELS => PhoneModel::ACTIVE
        ];
    }
}
