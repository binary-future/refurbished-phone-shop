<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Phone\Brand\Repository\Brands;

abstract class SimpleQueryHandler extends Handler
{
    /**
     * @var Brands
     */
    private $brands;

    /**
     * BySlugQueryHandler constructor.
     * @param Brands $brands
     */
    public function __construct(Brands $brands)
    {
        $this->brands = $brands;
    }

    /**
     * @param BySlugQuery $object
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function proceed($object)
    {
        return $this->brands->findBy($object);
    }
}
