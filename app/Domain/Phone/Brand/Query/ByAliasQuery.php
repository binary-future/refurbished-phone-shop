<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary;

final class ByAliasQuery implements Query, BrandsCriteriaDictionary
{
    /**
     * @var string
     */
    private $alias;

    /**
     * ByAliasQuery constructor.
     * @param string $alias
     */
    public function __construct(string $alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_ALIAS => $this->getAlias()
        ];
    }
}
