<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary;
use App\Utils\Repository\Criteria\Common\CompareValue;

final class BySlugQuery implements Query, BrandsCriteriaDictionary
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * BySlugQuery constructor.
     * @param string $slug
     * @param array $relations
     */
    public function __construct(string $slug, array $relations = [])
    {
        $this->slug = $slug;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::CRITERIA_BY_SLUG =>  new CompareValue($this->slug)
        ];
    }
}
