<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Common\Contracts\Repository\PluckableQuery;
use App\Domain\Phone\Brand\Repository\BrandsCriteriaDictionary;
use App\Utils\Repository\Criteria\Common\CompareValue;

final class BySlugsQuery implements Query, PluckableQuery, BrandsCriteriaDictionary
{
    /**
     * @var string
     */
    private $slugs;

    /**
     * @var array
     */
    private $relations = [];
    /**
     * @var string
     */
    private $column;

    /**
     * BySlugQuery constructor.
     * @param string[] $slugs
     * @param array $relations
     */
    public function __construct(array $slugs, array $relations = [])
    {
        $this->slugs = $slugs;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::CRITERIA_BY_SLUG => new CompareValue($this->slugs, 'in')
        ];
    }

    /**
     * @param string $column
     * @return mixed
     */
    public function chooseColumn(string $column)
    {
        $this->column = $column;
        return $this;
    }

    /**
     * @return string|null
     */
    public function chosenColumn(): ?string
    {
        return $this->column;
    }
}
