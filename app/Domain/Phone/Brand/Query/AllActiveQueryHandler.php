<?php

namespace App\Domain\Phone\Brand\Query;

final class AllActiveQueryHandler extends SimpleQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllActiveQuery;
    }
}
