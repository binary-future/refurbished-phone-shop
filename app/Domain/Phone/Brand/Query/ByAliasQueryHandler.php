<?php

namespace App\Domain\Phone\Brand\Query;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Phone\Brand\Repository\Brands;

/**
 * Class ByAliasQueryHandler
 * @package App\Domain\Phone\Brand\Query
 */
final class ByAliasQueryHandler extends SimpleQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByAliasQuery;
    }
}
