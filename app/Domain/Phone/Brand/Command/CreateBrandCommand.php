<?php

namespace App\Domain\Phone\Brand\Command;

use App\Domain\Phone\Brand\Brand;

class CreateBrandCommand
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var array
     */
    private $related;

    /**
     * CreateBrandCommand constructor.
     * @param Brand $brand
     * @param array $related
     */
    public function __construct(Brand $brand, array $related = [])
    {
        $this->brand = $brand;
        $this->related = $related;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
