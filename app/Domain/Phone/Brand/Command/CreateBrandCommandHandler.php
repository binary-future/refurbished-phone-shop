<?php

namespace App\Domain\Phone\Brand\Command;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Repository\Brands;

final class CreateBrandCommandHandler implements Handler
{
    /**
     * @var Brands
     */
    private $brands;

    /**
     * CreateBrandCommandHandler constructor.
     * @param Brands $brands
     */
    public function __construct(Brands $brands)
    {
        $this->brands = $brands;
    }

    public function handle(object $object)
    {
        /**
         * @var CreateBrandCommand $object
         */
        $brand = $this->saveBrand($object->getBrand(), $object->getRelated());

        return $brand;
    }

    private function saveBrand(Brand $brand, array $related)
    {
        return $this->brands->save($brand, $related);
    }
}
