<?php

namespace App\Domain\Phone\Brand\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Repository\Brands;
use App\Domain\Shared\Description\Services\DescriptionManager;

/**
 * Class UpdateBrandCommandHandler
 * @package App\Domain\Phone\Brand\Command
 */
final class UpdateBrandCommandHandler extends Handler
{
    /**
     * @var Brands
     */
    private $brands;

    /**
     * @var DescriptionManager
     */
    private $descriptionManager;

    /**
     * UpdateBrandCommandHandler constructor.
     * @param Brands $brands
     * @param DescriptionManager $descriptionManager
     */
    public function __construct(Brands $brands,  DescriptionManager $descriptionManager)
    {
        $this->brands = $brands;
        $this->descriptionManager = $descriptionManager;
    }

    /**
     * @param UpdateBrandCommand $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        $brand = $object->getBrand();
        $brand = $this->manageRelations($brand, $object);
        $brand->fill($object->getParams());
        $this->brands->push($brand);
        return $brand;
    }

    private function manageRelations(Brand $brand, UpdateBrandCommand $object)
    {
        if ($object->getImage()) {
            $brand->setRelation('image', $object->getImage());
        }
        if ($object->getDescription()) {
            $brand->setRelation(
                'description',
                $this->descriptionManager->updateDescription($brand, $object->getDescription())
            );
        }

        return $brand;
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UpdateBrandCommand;
    }
}
