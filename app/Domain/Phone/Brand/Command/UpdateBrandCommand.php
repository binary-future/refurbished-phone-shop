<?php


namespace App\Domain\Phone\Brand\Command;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;

/**
 * Class UpdateBrandCommand
 * @package App\Domain\Phone\Brand\Command
 */
final class UpdateBrandCommand
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var array
     */
    private $params;

    /**
     * @var array
     */
    private $related;

    /**
     * UpdateBrandCommand constructor.
     * @param Brand $brand
     * @param array $params
     * @param array $related
     */
    public function __construct(Brand $brand, array $params, array $related = [])
    {
        $this->brand = $brand;
        $this->params = $params;
        $this->related = $related;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->getRelated('image', Image::class);
    }

    public function getDescription(): ?Description
    {
        return $this->getRelated('description', Description::class);
    }

    private function getRelated(string $key, string $type)
    {
        $related = $this->related[$key] ?? null;

        return $related instanceof $type ? $related : null;
    }
}
