<?php

namespace App\Domain\Phone\Brand\Specifications;

use App\Domain\Phone\Brand\Brand;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class BrandSpecification
 * @package App\Domain\Phone\Brand\Specifications
 */
final class BrandSpecification implements Specification
{
    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        try {
            return $this->proceed($value, $params);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * @param Brand $brand
     * @param $params
     * @return bool
     */
    private function proceed(Brand $brand, $params): bool
    {
        return ! $this->isEmpty($brand->getName())
            && ! $this->isEmpty($brand->getSlug())
            && ! $this->isEmpty($brand->getAlias());
    }

    /**
     * @param string $param
     * @return bool
     */
    private function isEmpty(string $param): bool
    {
        return $param === '';
    }
}
