<?php

namespace App\Domain\Phone\Brand;

use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Shared\Contracts\HasDescription;
use App\Domain\Shared\Contracts\HasImage;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

class Brand extends Model implements HasImage, HasDescription
{
    public const TABLE = 'brands';

    public const OWNER_TYPE = 'brands';

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_SLUG = 'slug';
    public const FIELD_ALIAS = 'alias';
    public const FIELD_UPDATED_AT = self::UPDATED_AT;

    public const RELATION_MODELS = 'models';
    public const RELATION_DESCRIPTION = 'description';
    public const RELATION_IMAGE = 'image';

    protected $table = self::TABLE;

    protected $fillable = [
        self::FIELD_NAME, self::FIELD_SLUG, self::FIELD_ALIAS, self::FIELD_UPDATED_AT
    ];

    /*------------------------------------------------------------------------------------------------------------------
     *
     * ATTRIBUTES
     *__________________________________________________________________________________________________________________
     */
    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    public function getSlug(): string
    {
        return $this->getAttribute(self::FIELD_SLUG);
    }

    public function getAlias(): string
    {
        return $this->getAttribute(self::FIELD_ALIAS);
    }

    public function setName(string $name)
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    public function setSlug(string $slug)
    {
        return $this->setAttribute(self::FIELD_SLUG, strtolower($slug));
    }

    public function setAlias(string $alias)
    {
        return $this->setAttribute(self::FIELD_ALIAS, strtolower($alias));
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->getAttribute(self::UPDATED_AT);
    }

    /*------------------------------------------------------------------------------------------------------------------
     *
     * RELATIONS
     *__________________________________________________________________________________________________________________
     */

    public function models()
    {
        return $this->hasMany(PhoneModel::class);
    }

    /**
     * Don't added return type to function,
     * because it gets conflict with getModels() function of Illuminate\Database\Eloquent\Builder
     */
    public function getModels()
    {
        return $this->getRelationValue(self::RELATION_MODELS);
    }

    public function setModels(Collection $models)
    {
        $this->setRelation(self::RELATION_MODELS, $models);
    }

    /**
     * @return MorphOne
     */
    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, Image::RELATION_IMAGABLE);
    }

    /**
     * @return MorphOne
     */
    public function description(): MorphOne
    {
        return $this->morphOne(Description::class, Description::RELATION_DESCRIBABLE);
    }

    public function getDescription(): ?Description
    {
        return $this->getRelationValue(self::RELATION_DESCRIPTION);
    }

    /**
     * @return string
     */
    public function getOwnerName(): string
    {
        return $this->getSlug();
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->getRelationValue(self::RELATION_IMAGE);
    }

    /**
     * @return string
     */
    public function getOwnerType(): string
    {
        return self::OWNER_TYPE;
    }

    /**
     * @return int
     */
    public function getOwnerKey(): int
    {
        return $this->getKey();
    }

    public function hasActiveModels(): bool
    {
        foreach ($this->getModels() as $model) {
            if ($model->isActive()) {
                return true;
            }
        }

        return false;
    }
}
