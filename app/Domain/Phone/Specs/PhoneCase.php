<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class PhoneCase extends BaseSpecGroup
{
    public const FIELD_HEIGHT = 'height';
    public const FIELD_WIDTH = 'width';
    public const FIELD_THICKNESS = 'thickness';
    public const FIELD_WEIGHT = 'weight';
    private const GROUP_NAME = 'Case';

    /**
     * @var float
     */
    private $height = 0;

    /**
     * @var float
     */
    private $width = 0;

    /**
     * @var float
     */
    private $thickness = 0;

    /**
     * @var float
     */
    private $weight = 0;

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @param float $height
     */
    public function setHeight(float $height): void
    {
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @param float $width
     */
    public function setWidth(float $width): void
    {
        $this->width = $width;
    }

    /**
     * @return float
     */
    public function getThickness(): float
    {
        return $this->thickness;
    }

    /**
     * @param float $thickness
     */
    public function setThickness(float $thickness): void
    {
        $this->thickness = $thickness;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_HEIGHT => $this->getHeight(),
            self::FIELD_WEIGHT => $this->getWeight(),
            self::FIELD_WIDTH => $this->getWidth(),
            self::FIELD_THICKNESS => $this->getThickness(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var PhoneCase $specGroup
         */
        $this->setWidth($this->chooseParam($this->getWidth(), $specGroup->getWidth()));
        $this->setWeight($this->chooseParam($this->getWeight(), $specGroup->getWeight()));
        $this->setHeight($this->chooseParam($this->getHeight(), $specGroup->getHeight()));
        $this->setThickness($this->chooseParam($this->getThickness(), $specGroup->getThickness()));

        return $this;
    }
}
