<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;

/**
 * Class BaseSpecGroup
 * @package App\Domain\Phone\Specs
 */
abstract class BaseSpecGroup implements SpecGroup
{
    /**
     * @param SpecGroup $specGroup
     * @return SpecGroup
     */
    public function merge(SpecGroup $specGroup): SpecGroup
    {
        if (! $this->isMergable($specGroup)) {
            return $this;
        }

        return $this->proceedMerging($specGroup);
    }

    /**
     * @param SpecGroup $specGroup
     * @return bool
     */
    private function isMergable(SpecGroup $specGroup): bool
    {
        return static::class === get_class($specGroup);
    }

    /**
     * @param SpecGroup $specGroup
     * @return SpecGroup
     */
    abstract protected function proceedMerging(SpecGroup $specGroup): SpecGroup;

    /**
     * @param $initialParam
     * @param $novelParam
     * @return mixed
     */
    protected function chooseParam($initialParam, $novelParam)
    {
        return ! is_null($initialParam) ? $initialParam : $novelParam;
    }
}
