<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class Autonomy extends BaseSpecGroup
{
    public const FIELD_TALK_TIME = 'talk_time';
    public const FIELD_STANBY_TIME = 'stanby_time';
    public const FIELD_BATTERY_CAPACITY = 'battery_capacity';
    public const FIELD_BATTERY_TYPE = 'battery_type';
    private const GROUP_NAME = 'Autonomy';

    /**
     * @var int|null
     */
    private $talkTime;

    /**
     * @var int|null
     */
    private $stanbyTime;

    /**
     * @var float|null
     */
    private $batteryCapacity;

    /**
     * @var string|null
     */
    private $batteryType;

    /**
     * @return int|null
     */
    public function getTalkTime(): ?int
    {
        return $this->talkTime;
    }

    /**
     * @param int|null $talkTime
     */
    public function setTalkTime(?int $talkTime): void
    {
        $this->talkTime = $talkTime;
    }

    /**
     * @return int|null
     */
    public function getStanbyTime(): ?int
    {
        return $this->stanbyTime;
    }

    /**
     * @param int|null $stanbyTime
     */
    public function setStanbyTime(?int $stanbyTime): void
    {
        $this->stanbyTime = $stanbyTime;
    }

    /**
     * @return float|null
     */
    public function getBatteryCapacity(): ?float
    {
        return $this->batteryCapacity;
    }

    /**
     * @param float|null $batteryCapacity
     */
    public function setBatteryCapacity(?float $batteryCapacity): void
    {
        $this->batteryCapacity = $batteryCapacity;
    }

    /**
     * @return string|null
     */
    public function getBatteryType(): ?string
    {
        return $this->batteryType;
    }

    /**
     * @param string|null $batteryType
     */
    public function setBatteryType(?string $batteryType): void
    {
        $this->batteryType = $batteryType;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_TALK_TIME => $this->getTalkTime(),
            self::FIELD_STANBY_TIME => $this->getStanbyTime(),
            self::FIELD_BATTERY_CAPACITY => $this->getBatteryCapacity(),
            self::FIELD_BATTERY_TYPE => $this->getBatteryType(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var Autonomy $specGroup
         */
        $this->setTalkTime($this->chooseParam($this->getTalkTime(), $specGroup->getTalkTime()));
        $this->setStanbyTime($this->chooseParam($this->getStanbyTime(), $specGroup->getStanbyTime()));
        $this->setBatteryType($this->chooseParam($this->getBatteryType(), $specGroup->getBatteryType()));
        $this->setBatteryCapacity($this->chooseParam($this->getBatteryCapacity(), $specGroup->getBatteryCapacity()));

        return $this;
    }
}
