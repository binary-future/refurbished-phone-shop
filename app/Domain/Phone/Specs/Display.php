<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Display
 * @package App\Domain\Phone\Specs
 */
final class Display extends BaseSpecGroup
{
    public const FIELD_RESOLUTION = 'resolution';
    public const FIELD_TOUCHSCREEN = 'touchscreen';
    public const FIELD_DISPLAY_SIZE = 'display_size';
    public const FIELD_DISPLAY_TYPE = 'display_type';
    private const GROUP_NAME = 'Display';

    /**
     * @var string|null
     */
    private $resolution;

    /**
     * @var bool
     */
    private $touchscreen;

    /**
     * @var float|null
     */
    private $displaySize;

    /**
     * @var string|null
     */
    private $displayType;

    /**
     * @return string|null
     */
    public function getResolution(): ?string
    {
        return $this->resolution;
    }

    /**
     * @param string|null $resolution
     */
    public function setResolution(?string $resolution): void
    {
        $this->resolution = $resolution;
    }

    /**
     * @return bool
     */
    public function isTouchscreen(): bool
    {
        return $this->touchscreen;
    }

    /**
     * @param bool $touchscreen
     */
    public function setTouchscreen(bool $touchscreen): void
    {
        $this->touchscreen = $touchscreen;
    }

    /**
     * @return float|null
     */
    public function getDisplaySize(): ?float
    {
        return $this->displaySize;
    }

    /**
     * @param float|null $displaySize
     */
    public function setDisplaySize(?float $displaySize): void
    {
        $this->displaySize = $displaySize;
    }

    /**
     * @return string|null
     */
    public function getDisplayType(): ?string
    {
        return $this->displayType;
    }

    /**
     * @param string|null $displayType
     */
    public function setDisplayType(?string $displayType): void
    {
        $this->displayType = $displayType;
    }

    public function toArray()
    {
        return [
            self::FIELD_TOUCHSCREEN => $this->isTouchscreen(),
            self::FIELD_RESOLUTION => $this->getResolution(),
            self::FIELD_DISPLAY_SIZE => $this->getDisplaySize(),
            self::FIELD_DISPLAY_TYPE => $this->getDisplayType(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var Display $specGroup
         */
        $this->setTouchscreen($this->chooseParam($this->isTouchscreen(), $specGroup->isTouchscreen()));
        $this->setResolution($this->chooseParam($this->getResolution(), $specGroup->getResolution()));
        $this->setDisplayType($this->chooseParam($this->getDisplayType(), $specGroup->getDisplayType()));
        $this->setDisplaySize($this->chooseParam($this->getDisplaySize(), $specGroup->getDisplaySize()));

        return $this;
    }
}
