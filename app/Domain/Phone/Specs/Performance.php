<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class Performance extends BaseSpecGroup
{
    public const FIELD_RAM = 'ram';
    public const FIELD_MEMORY_CARD_SLOT = 'memory_card_slot';
    public const FIELD_MEMORY_CARD = 'memory_card';
    public const FIELD_PROCESSOR = 'processor';
    public const FIELD_CHIPSET = 'chipset';
    private const GROUP_NAME = 'Performance';
    /**
     * @var int|null
     */
    private $ram;

    /**
     * @var string|null
     */
    private $memoryCardSlot;

    /**
     * @var bool
     */
    private $memoryCard = false;

    /**
     * @var int|null
     */
    private $processor;

    /**
     * @var string|null
     */
    private $chipset;

    /**
     * @return int|null
     */
    public function getRam(): ?int
    {
        return $this->ram;
    }

    /**
     * @param int|null $ram
     */
    public function setRam(?int $ram): void
    {
        $this->ram = $ram;
    }

    /**
     * @return string|null
     */
    public function getMemoryCardSlot(): ?string
    {
        return $this->memoryCardSlot;
    }

    /**
     * @param string|null $memoryCardSlot
     */
    public function setMemoryCardSlot(?string $memoryCardSlot): void
    {
        $this->memoryCardSlot = $memoryCardSlot;
    }

    /**
     * @return bool
     */
    public function isMemoryCard(): bool
    {
        return $this->memoryCard;
    }

    /**
     * @param bool $memoryCard
     */
    public function setMemoryCard(bool $memoryCard): void
    {
        $this->memoryCard = $memoryCard;
    }

    /**
     * @return int|null
     */
    public function getProcessor(): ?int
    {
        return $this->processor;
    }

    /**
     * @param int|null $processor
     */
    public function setProcessor(?int $processor): void
    {
        $this->processor = $processor;
    }

    /**
     * @return string|null
     */
    public function getChipset(): ?string
    {
        return $this->chipset;
    }

    /**
     * @param string|null $chipset
     */
    public function setChipset(?string $chipset): void
    {
        $this->chipset = $chipset;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_MEMORY_CARD => $this->isMemoryCard(),
            self::FIELD_MEMORY_CARD_SLOT => $this->getMemoryCardSlot(),
            self::FIELD_RAM => $this->getRam(),
            self::FIELD_PROCESSOR => $this->getProcessor(),
            self::FIELD_CHIPSET => $this->getChipset(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    /**
     * @param SpecGroup $specGroup
     * @return SpecGroup
     */
    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var Performance $specGroup
         */
        $this->setRam($this->chooseParam($this->getRam(), $specGroup->getRam()));
        $this->setProcessor($this->chooseParam($this->getProcessor(), $specGroup->getProcessor()));
        $this->setMemoryCardSlot($this->chooseParam($this->getMemoryCardSlot(), $specGroup->getMemoryCardSlot()));
        $this->setMemoryCard($this->chooseParam($this->isMemoryCard(), $specGroup->isMemoryCard()));
        $this->setChipset($this->chooseParam($this->getChipset(), $specGroup->getChipset()));

        return $this;
    }
}
