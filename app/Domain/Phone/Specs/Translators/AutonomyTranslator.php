<?php

namespace App\Domain\Phone\Specs\Translators;

use App\Domain\Phone\Specs\Autonomy;
use App\Domain\Common\Contracts\Translators\Translator;

/**
 * Class AutonomyTranslator
 * @package App\Domain\Phone\Specs\Translators
 */
final class AutonomyTranslator implements Translator
{
    /**
     * @var array
     */
    private $timeFields = [
        Autonomy::FIELD_TALK_TIME,
        Autonomy::FIELD_STANBY_TIME
    ];

    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        foreach ($this->timeFields as $field) {
            $data[$field] = $this->getTimeInHours($data[$field] ?? null);
        }
        $data[Autonomy::FIELD_BATTERY_TYPE] = $data[Autonomy::FIELD_BATTERY_TYPE] ?? null;
        $data[Autonomy::FIELD_BATTERY_CAPACITY] = $this
            ->getBatteryCapacity($data[Autonomy::FIELD_BATTERY_CAPACITY] ?? null);

        return $data;
    }

    /**
     * @param null $value
     * @return |null
     */
    private function getTimeInHours($value = null)
    {
        $value = (int) $value;

        return $value ?: null;
    }

    /**
     * @param null $value
     * @return |null
     */
    private function getBatteryCapacity($value = null)
    {
        $value = (float) $value;

        return $value ?: null;
    }
}
