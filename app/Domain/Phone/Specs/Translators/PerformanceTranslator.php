<?php


namespace App\Domain\Phone\Specs\Translators;


use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Specs\Performance;

final class PerformanceTranslator implements Translator
{
    private const PROCESSOR_POWER_LIMIT_GHZ = 20;
    private const GIGA_MULTIPLIER = 1000;
    private const MEMORY_CARD_PRESENCE = 'yes';
    private const RAM_LIMIT_GB = 20;
    private const BYTES_MULTIPLIER = 1024;

    public function translate(array $data, array $options = []): array
    {
        return empty($data) ? $data : $this->proceed($data);
    }

    private function proceed($data)
    {
        $result[Performance::FIELD_PROCESSOR] = $this->getProcessor($data['processor'] ?? null);
        $result[Performance::FIELD_RAM] = $this->getRam($data['ram'] ?? null);
        $result[Performance::FIELD_MEMORY_CARD_SLOT] = $data['memory_card_slot'] ?? null;
        $result[Performance::FIELD_MEMORY_CARD] = $this->getMemoryCard($data['memory_card'] ?? false);
        $result[Performance::FIELD_CHIPSET] = $data['chipset'] ?? null;

        return $result;
    }

    private function getProcessor(?string $processor)
    {
        $processor = (float) $processor;
        if (! $processor && ! is_numeric($processor)) {
            return null;
        }

        return (int) ($processor > self::PROCESSOR_POWER_LIMIT_GHZ ? $processor : $processor * self::GIGA_MULTIPLIER);
    }

    private function getRam(?string $ram)
    {
        $ramValue = (float) $ram;

        if (! $ramValue) {
            return null;
        }
        if (stripos($ram, 'MB') !== false) {
            return (int) $ramValue ?: null;
        }

        return (int) ($ramValue > self::RAM_LIMIT_GB ? $ramValue : $ramValue * self::BYTES_MULTIPLIER);
    }

    private function getMemoryCard(?string $memoryCard): bool
    {
        if (! $memoryCard) {
            return false;
        }

        return stripos($memoryCard, self::MEMORY_CARD_PRESENCE) !== false;
    }
}
