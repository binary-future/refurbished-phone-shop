<?php


namespace App\Domain\Phone\Specs\Translators;


use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Specs\Display;

final class DisplayTranslator implements Translator
{
    private const TRUE_ANALOGUE = 'yes';

    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        $data[Display::FIELD_TOUCHSCREEN] = $this->getBoolValue($data[Display::FIELD_TOUCHSCREEN] ?? null);
        $data[Display::FIELD_RESOLUTION] = $this->getResolution($data[Display::FIELD_RESOLUTION] ?? null);
        $data[Display::FIELD_DISPLAY_TYPE] = $data[Display::FIELD_DISPLAY_TYPE] ?? null;
        $data[Display::FIELD_DISPLAY_SIZE] = $this->getDisplaySize($data[Display::FIELD_DISPLAY_SIZE] ?? null);

        return $data;
    }

    private function getBoolValue(string $value = null): bool
    {
        return $value && stripos($value, self::TRUE_ANALOGUE) !== false;
    }

    private function getResolution(string $resolution = null)
    {
        $resolutionNumber = (int) $resolution;

        return $resolutionNumber ? $resolution : null;
    }

    private function getDisplaySize($size = null)
    {
        $size = (float) $size;

        return $size ?: null;
    }
}
