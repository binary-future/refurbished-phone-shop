<?php

namespace App\Domain\Phone\Specs\Translators;

use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Specs\Network;

final class NetworkTranslator implements Translator
{
    private const TRUE_ANALOGUE = 'yes';

    private $boolParams = [
        Network::FIELD_BLUETOOTH,
        Network::FIELD_USB,
        Network::FIELD_WIFI,
        Network::FIELD_WIFI_HOTSPOT,
    ];

    private $nullableParams = [
        Network::FIELD_BLUETOOTH_TYPE,
        Network::FIELD_WIFI_TYPE,
    ];

    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        foreach ($this->boolParams as $boolParam) {
            $data[$boolParam] = $this->getBoolValue($data[$boolParam] ?? null);
        }

        foreach ($this->nullableParams as $nullableParam) {
            $data[$nullableParam] = $data[$nullableParam] ?? null;
        }

        return $data;
    }

    private function getBoolValue(string $value = null): bool
    {
        return $value && stripos($value, self::TRUE_ANALOGUE) !== false;
    }
}