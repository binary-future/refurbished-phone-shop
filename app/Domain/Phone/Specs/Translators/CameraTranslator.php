<?php


namespace App\Domain\Phone\Specs\Translators;

use App\Domain\Phone\Specs\Camera;
use App\Domain\Common\Contracts\Translators\Translator;

/**
 * Class CameraTranslator
 * @package App\Domain\Phone\Specs\Translators
 */
final class CameraTranslator implements Translator
{
    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        $data[Camera::FIELD_FRONT_CAMERA] = $this->getCameraValue($data[Camera::FIELD_FRONT_CAMERA]  ?? null);
        $data[Camera::FIELD_BACK_CAMERA] = $this->getCameraValue($data[Camera::FIELD_BACK_CAMERA] ?? null);

        return $data;
    }

    /**
     * @param null $value
     * @return |null
     */
    private function getCameraValue($value = null)
    {
        $value = (float) $value;

        return $value ?: null;
    }
}