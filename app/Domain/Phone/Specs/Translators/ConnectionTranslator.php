<?php


namespace App\Domain\Phone\Specs\Translators;


use App\Domain\Phone\Specs\Connection;
use App\Domain\Common\Contracts\Translators\Translator;

/**
 * Class ConnectionTranslator
 * @package App\Domain\Phone\Specs\Translators
 */
final class ConnectionTranslator implements Translator
{
    private const TRUE_ANALOGUE = 'yes';
    private const DEFAULT_SIM_CAPACITY = 1;

    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        $data[Connection::FIELD_CONNECTION_TYPE] = $data[Connection::FIELD_CONNECTION_TYPE] ?? null;
        $data[Connection::FIELD_GPS_TYPE] = $data[Connection::FIELD_GPS_TYPE] ?? null;
        $data[Connection::FIELD_GPS] = $this->getBoolValue($data[Connection::FIELD_GPS] ?? null);
        $data[Connection::FIELD_SIM_CAPACITY] = $this->getSim($data[Connection::FIELD_SIM_CAPACITY] ?? null);

        return $data;
    }

    /**
     * @param string|null $value
     * @return bool
     */
    private function getBoolValue(string $value = null): bool
    {
        return $value && stripos($value, self::TRUE_ANALOGUE) !== false;
    }

    /**
     * @param null $sim
     * @return int
     */
    private function getSim($sim = null)
    {
        $sim = (int) $sim;

        return $sim ?: self::DEFAULT_SIM_CAPACITY;
    }
}