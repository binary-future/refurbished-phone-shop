<?php

namespace App\Domain\Phone\Specs\Translators;

use App\Domain\Common\Contracts\Translators\Translator;

/**
 * Class PhoneCaseTranslator
 * @package App\Domain\Phone\Specs\Translators
 */
final class PhoneCaseTranslator implements Translator
{
    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        if (empty($data)) {
            return $data;
        }

        return $this->proceed($data);
    }

    /**
     * @param array $data
     * @return array
     */
    private function proceed(array $data): array
    {
        $result = [];
        foreach ($data as $key => $param) {
            $result[$key] = $this->filterMetrics($param);
        }

        return $result;
    }

    /**
     * @param string $param
     * @return string
     */
    private function filterMetrics(string $param)
    {
        return trim(str_ireplace(['Pixel', 'px', 'grams'], '', $param));
    }
}
