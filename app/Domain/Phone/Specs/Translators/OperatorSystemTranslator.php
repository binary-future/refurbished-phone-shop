<?php


namespace App\Domain\Phone\Specs\Translators;


use App\Domain\Common\Contracts\Translators\Translator;

/**
 * Class OperatorSystemTranslator
 * @package App\Domain\Phone\Specs\Translators
 */
final class OperatorSystemTranslator implements Translator
{
    /**
     * @return array
     */
    private function getPlatformMap()
    {
        return [
            'Android' => [
                'types' => [
                    'KitKat',
                    'Lollipop',
                    'Marshmallow',
                    'Nougat',
                    'Oreo',
                    'Pie',
                    'Jelly Bean',

                ]
            ],
            'BlackBerry' => [],
            'Cat' => [],
            'Doro' => [],
            'MacOS' => [],
            'iOS' => [],
            'Monqi' => [],
            'Symbian' => [],
            'Tizen' => [],
            'Nokia' => [
                'types' => [
                    'Series'
                ]
            ],
            'Windows' => [],
            'Proprietary' => [],
            'Alcatel' => [],
            'AneedA' => []
        ];
    }

    /**
     * @var string|null
     */
    private $platform = null;

    /**
     * @var string
     */
    private $operatingSystem = null;

    /**
     * @param array $data
     * @param array $options
     * @return array
     */
    public function translate(array $data, array $options = []): array
    {
        if (empty($data)) {
            return $data;
        }

        return $this->proceed($data);
    }

    /**
     * @param array $data
     * @return array
     */
    private function proceed(array $data): array
    {
        if (! isset($data['operation_system'])) {
            return $this->prepareResponse();
        }
        $this->operatingSystem = $data['operation_system'];

        $this->isPlatformValueByType();
        if (! $this->platform) {
            $this->isPlatformWithoutTypes();
        }

        return $this->prepareResponse();
    }

    /**
     * @return bool
     */
    private function isPlatformValueByType()
    {
        $map = $this->getPlatformMap();
        $platforms = $this->getPlatformsWithTypes();
        foreach ($platforms as $platformItem) {
            $this->checkTypes($map[$platformItem]['types'], $platformItem);
            if ($this->platform) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    private function getPlatformsWithTypes()
    {
        $platforms = $this->getPlatforms();
        $map = $this->getPlatformMap();
        $result = [];
        foreach ($platforms as $platform) {
            if ($this->isPlatformHasTypes($map, $platform)) {
                $result[] = $platform;
            }
        }

        return $result;
    }

    /**
     * @param array $map
     * @param string $platform
     * @return bool
     */
    private function isPlatformHasTypes(array $map, string $platform): bool
    {
        return isset($map[$platform]['types']) && is_array($map[$platform]['types']);
    }

    /**
     * @param array $types
     * @param string $platformItem
     * @return bool
     */
    private function checkTypes(array $types, string $platformItem)
    {
        foreach ($types as $type) {
            if (stripos($this->operatingSystem, $type) !== false) {
                $this->platform = $platformItem;
                if (stripos($this->operatingSystem, $this->platform) === false) {
                    $this->operatingSystem = $platformItem . ' ' . $this->operatingSystem;
                }
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    private function getPlatforms()
    {
        $map = $this->getPlatformMap();

        return array_keys($map);
    }

    /**
     * @return bool
     */
    private function isPlatformWithoutTypes()
    {
        $platforms = $this->getPlatforms();
        foreach ($platforms as $platformItem) {
            if (stripos($this->operatingSystem, $platformItem) !== false) {
                $this->platform = $platformItem;
                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    private function prepareResponse(): array
    {
        $operatingSystem = str_ireplace(['(', ')'], '', $this->operatingSystem);

        return [
            'operation_system' => $operatingSystem ?: null,
            'platform' => $this->platform ?: $operatingSystem
        ];
    }
}
