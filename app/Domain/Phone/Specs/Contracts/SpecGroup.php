<?php

namespace App\Domain\Phone\Specs\Contracts;

use Illuminate\Contracts\Support\Arrayable;

/**
 * Interface SpecGroup
 * @package App\Domain\Phone\Specs\Contracts
 */
interface SpecGroup extends Arrayable
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return bool
     */
    public function isValid(): bool;

    /**
     * @param SpecGroup $specGroup
     * @return mixed
     */
    public function merge(SpecGroup $specGroup);
}
