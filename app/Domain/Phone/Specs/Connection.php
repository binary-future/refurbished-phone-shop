<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class Connection extends BaseSpecGroup
{
    public const FIELD_CONNECTION_TYPE = 'connection_type';
    public const FIELD_SIM_CAPACITY = 'sim_capacity';
    public const FIELD_GPS = 'gps';
    public const FIELD_GPS_TYPE = 'gps_type';
    private const GROUP_NAME = 'Connection';

    /**
     * @var string|null
     */
    private $connectionType;

    /**
     * @var int
     */
    private $simCapacity;

    /**
     * @var bool
     */
    private $gps;

    /**
     * @var string|null
     */
    private $gpsType;

    /**
     * @return string|null
     */
    public function getConnectionType(): ?string
    {
        return $this->connectionType;
    }

    /**
     * @param string|null $connectionType
     */
    public function setConnectionType(?string $connectionType): void
    {
        $this->connectionType = $connectionType;
    }

    /**
     * @return int
     */
    public function getSimCapacity(): int
    {
        return $this->simCapacity;
    }

    /**
     * @param int $simCapacity
     */
    public function setSimCapacity(int $simCapacity): void
    {
        $this->simCapacity = $simCapacity;
    }

    /**
     * @return bool
     */
    public function isGps(): bool
    {
        return $this->gps;
    }

    /**
     * @param bool $gps
     */
    public function setGps(bool $gps): void
    {
        $this->gps = $gps;
    }

    /**
     * @return string|null
     */
    public function getGpsType(): ?string
    {
        return $this->gpsType;
    }

    /**
     * @param string|null $gpsType
     */
    public function setGpsType(?string $gpsType): void
    {
        $this->gpsType = $gpsType;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_CONNECTION_TYPE => $this->getConnectionType(),
            self::FIELD_SIM_CAPACITY => $this->getSimCapacity(),
            self::FIELD_GPS => $this->isGps(),
            self::FIELD_GPS_TYPE => $this->getGpsType(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var Connection $specGroup
         */
        $this->setGpsType($this->chooseParam($this->getGpsType(), $specGroup->getGpsType()));
        $this->setGps($this->chooseParam($this->isGps(), $specGroup->isGps()));
        $this->setSimCapacity($this->chooseParam($this->getSimCapacity(), $specGroup->getSimCapacity()));
        $this->setConnectionType($this->chooseParam($this->getConnectionType(), $specGroup->getConnectionType()));

        return $this;
    }
}
