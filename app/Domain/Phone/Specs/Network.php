<?php

namespace App\Domain\Phone\Specs;

use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class Network extends BaseSpecGroup
{
    public const FIELD_WIFI_TYPE = 'wifi_type';
    public const FIELD_WIFI = 'wifi';
    public const FIELD_WIFI_HOTSPOT = 'wifi_hotspot';
    public const FIELD_USB = 'usb';
    public const FIELD_BLUETOOTH = 'bluetooth';
    public const FIELD_BLUETOOTH_TYPE = 'bluetooth_type';
    private const GROUP_NAME = 'Network';


    /**
     * @var string|null
     */
    private $wifiType;

    /**
     * @var bool
     */
    private $wifi = false;

    /**
     * @var bool
     */
    private $wifiHotspot = false;

    /**
     * @var bool
     */
    private $usb = false;

    /**
     * @var bool
     */
    private $bluetooth = false;

    /**
     * @var string|null
     */
    private $bluetoothType;

    /**
     * @return string|null
     */
    public function getWifiType(): ?string
    {
        return $this->wifiType;
    }

    /**
     * @param string|null $wifiType
     */
    public function setWifiType(?string $wifiType): void
    {
        $this->wifiType = $wifiType;
    }

    /**
     * @return bool
     */
    public function isWifi(): bool
    {
        return $this->wifi;
    }

    /**
     * @param bool $wifi
     */
    public function setWifi(bool $wifi): void
    {
        $this->wifi = $wifi;
    }

    /**
     * @return bool
     */
    public function isWifiHotspot(): bool
    {
        return $this->wifiHotspot;
    }

    /**
     * @param bool $wifiHotspot
     */
    public function setWifiHotspot(bool $wifiHotspot): void
    {
        $this->wifiHotspot = $wifiHotspot;
    }

    /**
     * @return bool
     */
    public function isUsb(): bool
    {
        return $this->usb;
    }

    /**
     * @param bool $usb
     */
    public function setUsb(bool $usb): void
    {
        $this->usb = $usb;
    }

    /**
     * @return bool
     */
    public function isBluetooth(): bool
    {
        return $this->bluetooth;
    }

    /**
     * @param bool $bluetooth
     */
    public function setBluetooth(bool $bluetooth): void
    {
        $this->bluetooth = $bluetooth;
    }

    /**
     * @return string|null
     */
    public function getBluetoothType(): ?string
    {
        return $this->bluetoothType;
    }

    /**
     * @param string|null $bluetoothType
     */
    public function setBluetoothType(?string $bluetoothType): void
    {
        $this->bluetoothType = $bluetoothType;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_BLUETOOTH => $this->isBluetooth(),
            self::FIELD_BLUETOOTH_TYPE => $this->getBluetoothType(),
            self::FIELD_USB => $this->isUsb(),
            self::FIELD_WIFI => $this->isWifi(),
            self::FIELD_WIFI_HOTSPOT => $this->isWifiHotspot(),
            self::FIELD_WIFI_TYPE => $this->getWifiType(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var Network $specGroup
         */
        $this->setWifi($this->chooseParam($this->isWifi(), $specGroup->isWifi()));
        $this->setWifiHotspot($this->chooseParam($this->isWifiHotspot(), $specGroup->isWifiHotspot()));
        $this->setWifiType($this->chooseParam($this->getWifiType(), $specGroup->getWifiType()));
        $this->setUsb($this->chooseParam($this->isUsb(), $specGroup->isUsb()));
        $this->setBluetooth($this->chooseParam($this->isBluetooth(), $specGroup->isBluetooth()));
        $this->setBluetoothType($this->chooseParam($this->getBluetoothType(), $specGroup->getBluetoothType()));

        return $this;
    }
}
