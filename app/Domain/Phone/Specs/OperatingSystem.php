<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class OperatingSystem extends BaseSpecGroup
{
    public const FIELD_PLATFORM = 'platform';
    public const FIELD_OPERATION_SYSTEM = 'operation_system';
    private const GROUP_NAME = 'Operating System';

    /**
     * @var string|null
     */
    private $platform;

    /**
     * @var string|null
     */
    private $operationSystem;

    /**
     * @return string|null
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    /**
     * @param string|null $platform
     */
    public function setPlatform(?string $platform): void
    {
        $this->platform = $platform;
    }

    /**
     * @return string|null
     */
    public function getOperationSystem(): ?string
    {
        return $this->operationSystem;
    }

    /**
     * @param string|null $operationSystem
     */
    public function setOperationSystem(?string $operationSystem): void
    {
        $this->operationSystem = $operationSystem;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_PLATFORM => $this->getPlatform(),
            self::FIELD_OPERATION_SYSTEM => $this->getOperationSystem(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var OperatingSystem $specGroup
         */
        $this->setPlatform($this->chooseParam($this->getPlatform(), $specGroup->getPlatform()));
        $this->setOperationSystem($this->chooseParam($this->getOperationSystem(), $specGroup->getOperationSystem()));

        return $this;
    }
}
