<?php

namespace App\Domain\Phone\Specs;

use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PhoneSpecs extends Model
{
    public const TABLE = 'phone_specs';

    public const FIELD_ID = 'id';
    public const FIELD_MODEL_ID = 'model_id';

    public const RELATION_MODEL = 'model';

    protected $table = self::TABLE;

    protected $fillable = [
        Autonomy::FIELD_BATTERY_TYPE,
        Autonomy::FIELD_BATTERY_CAPACITY,
        Autonomy::FIELD_STANBY_TIME,
        Autonomy::FIELD_TALK_TIME,
        Camera::FIELD_BACK_CAMERA,
        Camera::FIELD_FRONT_CAMERA,
        Connection::FIELD_GPS_TYPE,
        Connection::FIELD_GPS,
        Connection::FIELD_SIM_CAPACITY,
        Connection::FIELD_CONNECTION_TYPE,
        Display::FIELD_TOUCHSCREEN,
        Display::FIELD_RESOLUTION,
        Display::FIELD_DISPLAY_SIZE,
        Display::FIELD_DISPLAY_TYPE,
        Network::FIELD_WIFI_TYPE,
        Network::FIELD_WIFI,
        Network::FIELD_WIFI_HOTSPOT,
        Network::FIELD_USB,
        Network::FIELD_BLUETOOTH_TYPE,
        Network::FIELD_BLUETOOTH,
        OperatingSystem::FIELD_OPERATION_SYSTEM,
        OperatingSystem::FIELD_PLATFORM,
        Performance::FIELD_PROCESSOR,
        Performance::FIELD_CHIPSET,
        Performance::FIELD_RAM,
        Performance::FIELD_MEMORY_CARD,
        Performance::FIELD_MEMORY_CARD_SLOT,
        PhoneCase::FIELD_WIDTH,
        PhoneCase::FIELD_HEIGHT,
        PhoneCase::FIELD_THICKNESS,
        PhoneCase::FIELD_WEIGHT,
        self::FIELD_MODEL_ID,
    ];

    public function getModelId(): int
    {
        return $this->getAttribute(self::FIELD_MODEL_ID);
    }

    public function setModelId(int $modelId)
    {
        return $this->setAttribute(self::FIELD_MODEL_ID, $modelId);
    }

    public function getAutonomy(): ?Autonomy
    {
        $autonomy = new Autonomy();
        $autonomy->setBatteryCapacity($this->getAttribute(Autonomy::FIELD_BATTERY_CAPACITY));
        $autonomy->setBatteryType($this->getAttribute(Autonomy::FIELD_BATTERY_TYPE));
        $autonomy->setStanbyTime($this->getAttribute(Autonomy::FIELD_STANBY_TIME));
        $autonomy->setTalkTime($this->getAttribute(Autonomy::FIELD_TALK_TIME));

        return $autonomy;
    }

    public function setAutonomy(Autonomy $autonomy): void
    {
        $this->setSpecGroup($autonomy);
    }

    private function setSpecGroup(SpecGroup $group)
    {
        $data = $group->toArray();
        foreach ($data as $field => $value) {
            $this->setAttribute($field, $value);
        }
    }

    public function getCamera(): ?Camera
    {
        $camera = new Camera();
        $camera->setBackCamera($this->getAttribute(Camera::FIELD_BACK_CAMERA));
        $camera->setFrontCamera($this->getAttribute(Camera::FIELD_FRONT_CAMERA));

        return $camera;
    }

    public function setCamera(Camera $camera)
    {
        $this->setSpecGroup($camera);
    }

    public function getConnectionSpec(): ?Connection
    {
        $connection = new Connection();
        $connection->setConnectionType($this->getAttribute(Connection::FIELD_CONNECTION_TYPE));
        $connection->setSimCapacity($this->getAttribute(Connection::FIELD_SIM_CAPACITY));
        $connection->setGps($this->getAttribute(Connection::FIELD_GPS));
        $connection->setGpsType($this->getAttribute(Connection::FIELD_GPS_TYPE));

        return $connection;
    }

    public function setConnectionSpec(Connection $connection)
    {
        $this->setSpecGroup($connection);
    }

    public function getDisplay(): ?Display
    {
        $display = new Display();
        $display->setDisplaySize($this->getAttribute(Display::FIELD_DISPLAY_SIZE));
        $display->setDisplayType($this->getAttribute(Display::FIELD_DISPLAY_TYPE));
        $display->setResolution($this->getAttribute(Display::FIELD_RESOLUTION));
        $display->setTouchscreen($this->getAttribute(Display::FIELD_TOUCHSCREEN));

        return $display;
    }

    public function setDisplay(Display $display)
    {
        $this->setSpecGroup($display);
    }

    public function getNetwork(): ?Network
    {
        $network = new Network();
        $network->setBluetooth($this->getAttribute(Network::FIELD_BLUETOOTH));
        $network->setBluetoothType($this->getAttribute(Network::FIELD_BLUETOOTH_TYPE));
        $network->setUsb($this->getAttribute(Network::FIELD_USB));
        $network->setWifi($this->getAttribute(Network::FIELD_WIFI));
        $network->setWifiType($this->getAttribute(Network::FIELD_WIFI_TYPE));
        $network->setWifiHotspot($this->getAttribute(Network::FIELD_WIFI_HOTSPOT));

        return $network;
    }

    public function setNetwork(Network $network)
    {
        $this->setSpecGroup($network);
    }

    public function getOperatingSystem(): ?OperatingSystem
    {
        $operatingSystem = new OperatingSystem();
        $operatingSystem->setOperationSystem($this->getAttribute(OperatingSystem::FIELD_OPERATION_SYSTEM));
        $operatingSystem->setPlatform($this->getAttribute(OperatingSystem::FIELD_PLATFORM));

        return $operatingSystem;
    }

    public function setOperating(OperatingSystem $operatingSystem)
    {
        $this->setSpecGroup($operatingSystem);
    }

    public function getPerformance(): ?Performance
    {
        $performance = new Performance();
        $performance->setMemoryCard($this->getAttribute(Performance::FIELD_MEMORY_CARD));
        $performance->setMemoryCardSlot($this->getAttribute(Performance::FIELD_MEMORY_CARD_SLOT));
        $performance->setProcessor($this->getAttribute(Performance::FIELD_PROCESSOR));
        $performance->setRam($this->getAttribute(Performance::FIELD_RAM));
        $performance->setChipset($this->getAttribute(Performance::FIELD_CHIPSET));

        return $performance;
    }

    public function setPerformance(Performance $performance)
    {
        $this->setSpecGroup($performance);
    }

    public function getPhoneCase(): ?PhoneCase
    {
        $phoneCase = new PhoneCase();
        $phoneCase->setHeight($this->getAttribute(PhoneCase::FIELD_HEIGHT));
        $phoneCase->setWidth($this->getAttribute(PhoneCase::FIELD_WIDTH));
        $phoneCase->setWeight($this->getAttribute(PhoneCase::FIELD_WEIGHT));
        $phoneCase->setThickness($this->getAttribute(PhoneCase::FIELD_THICKNESS));

        return $phoneCase;
    }

    public function setPhoneCase(PhoneCase $phoneCase)
    {
        $this->setSpecGroup($phoneCase);
    }

    /**
     * @return SpecGroup[]
     */
    public function getGroupsList(): array
    {
        return [
            $this->getPhoneCase(),
            $this->getOperatingSystem(),
            $this->getDisplay(),
            $this->getPerformance(),
            $this->getAutonomy(),
            $this->getCamera(),
            $this->getConnectionSpec(),
            $this->getNetwork(),
        ];
    }

    public function merge(PhoneSpecs $phoneSpecs)
    {
        $this->setSpecGroup($this->getPerformance()->merge($phoneSpecs->getPerformance()));
        $this->setSpecGroup($this->getPhoneCase()->merge($phoneSpecs->getPhoneCase()));
        $this->setSpecGroup($this->getAutonomy()->merge($phoneSpecs->getAutonomy()));
        $this->setSpecGroup($this->getOperatingSystem()->merge($phoneSpecs->getOperatingSystem()));
        $this->setSpecGroup($this->getNetwork()->merge($phoneSpecs->getNetwork()));
        $this->setSpecGroup($this->getDisplay()->merge($phoneSpecs->getDisplay()));
        $this->setSpecGroup($this->getConnectionSpec()->merge($phoneSpecs->getConnectionSpec()));
        $this->setSpecGroup($this->getCamera()->merge($phoneSpecs->getCamera()));

        return $this;
    }

    /* RELATIONS */
    public function model(): BelongsTo
    {
        return $this->belongsTo(PhoneModel::class, self::FIELD_MODEL_ID);
    }

    public function getModel(): PhoneModel
    {
        return $this->getRelationValue(self::RELATION_MODEL);
    }
}
