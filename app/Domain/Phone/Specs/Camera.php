<?php


namespace App\Domain\Phone\Specs;


use App\Domain\Phone\Specs\Contracts\SpecGroup;
use Illuminate\Contracts\Support\Arrayable;

final class Camera extends BaseSpecGroup
{
    public const FIELD_FRONT_CAMERA = 'front_camera';
    public const FIELD_BACK_CAMERA = 'back_camera';
    public const GROUP_NAME = 'Camera';

    /**
     * @var float|null
     */
    private $frontCamera;

    /**
     * @var float|null
     */
    private $backCamera;

    /**
     * @return float|null
     */
    public function getFrontCamera(): ?float
    {
        return $this->frontCamera;
    }

    /**
     * @param float|null $frontCamera
     */
    public function setFrontCamera(?float $frontCamera): void
    {
        $this->frontCamera = $frontCamera;
    }

    /**
     * @return float|null
     */
    public function getBackCamera(): ?float
    {
        return $this->backCamera;
    }

    /**
     * @param float|null $backCamera
     */
    public function setBackCamera(?float $backCamera): void
    {
        $this->backCamera = $backCamera;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_FRONT_CAMERA => $this->getFrontCamera(),
            self::FIELD_BACK_CAMERA => $this->getBackCamera(),
        ];
    }

    public function getName(): string
    {
        return self::GROUP_NAME;
    }

    public function isValid(): bool
    {
        return true;
    }

    protected function proceedMerging(SpecGroup $specGroup): SpecGroup
    {
        /**
         * @var Camera $specGroup
         */
        $this->setFrontCamera($this->chooseParam($this->getFrontCamera(), $specGroup->getFrontCamera()));
        $this->setBackCamera($this->chooseParam($this->getBackCamera(), $specGroup->getBackCamera()));

        return $this;
    }
}
