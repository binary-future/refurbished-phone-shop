<?php


namespace App\Domain\Deals\Deal\Events;


use App\Domain\Deals\Deal\Deal;

/**
 * Class DealUpdated
 * @package App\Domain\Deals\Deal\Events
 */
final class DealUpdated
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * DealUpdated constructor.
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }
}
