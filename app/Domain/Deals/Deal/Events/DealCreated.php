<?php


namespace App\Domain\Deals\Deal\Events;


use App\Domain\Deals\Deal\Deal;

/**
 * Class DealCreated
 * @package App\Domain\Deals\Deal\Events
 */
final class DealCreated
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * DealCreated constructor.
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }
}
