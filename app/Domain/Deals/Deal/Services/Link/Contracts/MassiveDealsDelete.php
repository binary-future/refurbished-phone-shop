<?php

namespace App\Domain\Deals\Deal\Services\Link\Contracts;

use App\Domain\Common\Contracts\Repository\Query;

interface MassiveDealsDelete
{
    /**
     * @param Query $query
     * @return array
     */
    public function execute(Query $query): array;
}
