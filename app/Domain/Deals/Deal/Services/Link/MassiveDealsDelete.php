<?php


namespace App\Domain\Deals\Deal\Services\Link;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\Deals;
use App\Domain\Deals\Deal\Services\Link\Contracts\MassiveDealsDelete as Contract;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Link\Query\WithNoLinkableEntryQuery;
use App\Domain\Shared\Link\Repository\Links;

class MassiveDealsDelete implements Contract
{
    /**
     * @var Deals
     */
    private $deals;
    /**
     * @var Links
     */
    private $links;

    /**
     * MassiveDealsDelete constructor.
     * @param Deals $deals
     * @param Links $links
     */
    public function __construct(Deals $deals, Links $links)
    {
        $this->deals = $deals;
        $this->links = $links;
    }

    /**
     * @param Query $query
     * @return array
     */
    public function execute(Query $query): array
    {
        $deletedDeals = $this->deals->deleteBy($query);

        $linkQuery = new WithNoLinkableEntryQuery(Link::TYPE_DEAL);
        $deletedLinks = $this->links->deleteBy($linkQuery);

        return [$deletedDeals, $deletedLinks];
    }
}
