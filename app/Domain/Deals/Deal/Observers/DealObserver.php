<?php


namespace App\Domain\Deals\Deal\Observers;


use App\Application\Services\Bus\EventBus;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Events\DealCreated;
use App\Domain\Deals\Deal\Events\DealDeleted;
use App\Domain\Deals\Deal\Events\DealUpdated;

/**
 * Class DealObserver
 * @package App\Domain\Deals\Deal\Observers
 */
class DealObserver
{
    /**
     * @var EventBus
     */
    private $event;

    /**
     * DealObserver constructor.
     * @param EventBus $event
     */
    public function __construct(EventBus $event)
    {
        $this->event = $event;
    }

    /**
     * @param Deal $deal
     */
    public function created(Deal $deal)
    {
        $this->event->dispatch(new DealCreated($deal));
    }

    /**
     * @param Deal $deal
     */
    public function updated(Deal $deal)
    {
        $this->event->dispatch(new DealUpdated($deal));
    }

    /**
     * Listen to the User deleting event.
     * Deletes image file from the storage place
     *
     * @param Deal $deal
     * @return void
     */
    public function deleted(Deal $deal)
    {
        $this->event->dispatch(new DealDeleted($deal));
    }
}
