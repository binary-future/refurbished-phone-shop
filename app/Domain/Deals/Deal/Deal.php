<?php

namespace App\Domain\Deals\Deal;

use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Shared\Contracts\HasLinkWithAffiliation;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\Affiliation\Affiliation;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Deal
 * @package App\Domain\Deals\Deal
 */
class Deal extends Model implements HasLinkWithAffiliation
{
    public const TABLE = 'deals';

    public const OWNER_TYPE = 'deal';

    public const FIELD_ID = 'id';
    public const FIELD_STORE_ID = 'store_id';
    public const FIELD_PRODUCT_ID = 'product_id';
    public const FIELD_DESCRIPTION_ID = 'description_id';
    public const FIELD_PRODUCT_TYPE = 'product_type';
    public const FIELD_CONTRACT_ID = 'contract_id';
    public const FIELD_UPFRONT_COST = 'upfront_cost';
    public const FIELD_MONTHLY_COST = 'monthly_cost';
    public const FIELD_TOTAL_COST = 'total_cost';
    public const FIELD_EXTERNAL_ID = 'external_id';
    public const FIELD_UPDATED_AT = self::UPDATED_AT;

    public const OBJECT_PAYMENT = 'payment';

    public const RELATION_LINK = 'link';
    public const RELATION_PRODUCT = 'product';
    public const RELATION_STORE = 'store';
    public const RELATION_CONTRACT = 'contract';
    public const RELATION_DESCRIPTION = 'description';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_STORE_ID, self::FIELD_PRODUCT_ID, self::FIELD_PRODUCT_TYPE,
        self::FIELD_CONTRACT_ID, self::FIELD_UPFRONT_COST,
        self::FIELD_MONTHLY_COST, self::FIELD_EXTERNAL_ID, self::FIELD_UPDATED_AT,
        self::FIELD_TOTAL_COST,
    ];

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return new Payment(
            $this->getAttribute(self::FIELD_UPFRONT_COST),
            $this->getAttribute(self::FIELD_MONTHLY_COST),
            $this->getAttribute(self::FIELD_TOTAL_COST)
        );
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->getAttribute(self::FIELD_STORE_ID);
    }

    /**
     * @return int
     */
    public function getDescriptionId(): int
    {
        return $this->getAttribute(self::FIELD_DESCRIPTION_ID);
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->getAttribute(self::FIELD_PRODUCT_ID);
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        return $this->getAttribute(self::FIELD_PRODUCT_TYPE);
    }

    /**
     * @return int|null
     */
    public function getContractId(): ?int
    {
        return $this->getAttribute(self::FIELD_CONTRACT_ID);
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->setAttribute(self::FIELD_UPFRONT_COST, $payment->getUpfrontCost());
        $this->setAttribute(self::FIELD_MONTHLY_COST, $payment->getMonthlyCost());
        $this->setAttribute(self::FIELD_TOTAL_COST, $payment->getTotalCost());
    }

    /**
     * @param int $storeId
     */
    public function setStoreId(int $storeId)
    {
        $this->setAttribute(self::FIELD_STORE_ID, $storeId);
    }

    /**
     * @param int $descriptionId
     */
    public function setDescriptionId(int $descriptionId)
    {
        $this->setAttribute(self::FIELD_DESCRIPTION_ID, $descriptionId);
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->setAttribute(self::FIELD_PRODUCT_TYPE, $product->getType());
        $this->setAttribute(self::FIELD_PRODUCT_ID, $product->getKey());
    }

    /**
     * @param int $contractId
     */
    public function setContractId(int $contractId)
    {
        $this->setAttribute(self::FIELD_CONTRACT_ID, $contractId);
    }

    /**
     * @return string|null
     */
    public function getExternalId(): ?string
    {
        return $this->getAttribute(self::FIELD_EXTERNAL_ID);
    }

    /**
     * @param string|null $externalId
     */
    public function setExternalId(?string $externalId)
    {
        $this->setAttribute(self::FIELD_EXTERNAL_ID, $externalId);
    }

    /**
     * @return bool
     */
    public function isPaymentChanged(): bool
    {
        $originalPayment = new Payment(
            $this->getOriginal(self::FIELD_UPFRONT_COST),
            $this->getOriginal(self::FIELD_MONTHLY_COST),
            $this->getOriginal(self::FIELD_TOTAL_COST)
        );

        return ! $originalPayment->equal($this->getPayment());
    }

    /*------------------------------------------------------------------------------------------------------------------
     *
     * RELATIONS
     *__________________________________________________________________________________________________________________
     */

    /**
     * @return MorphOne
     */
    public function description(): MorphOne
    {
        return $this->morphOne(Description::class, Description::RELATION_DESCRIBABLE);
    }

    /**
     * @return Description|null
     */
    public function getDescription(): ?Description
    {
        return $this->getRelationValue(self::RELATION_DESCRIPTION);
    }

    /**
     * @return BelongsTo
     */
    public function contract(): BelongsTo
    {
        return $this->belongsTo(Contract::class, self::FIELD_CONTRACT_ID);
    }

    /**
     * @return Contract
     */
    public function getContract(): Contract
    {
        return $this->getRelationValue(self::RELATION_CONTRACT);
    }

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class, self::FIELD_STORE_ID);
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->getRelationValue(self::RELATION_STORE);
    }

    /**
     * @return MorphTo
     */
    public function product(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->getRelationValue(self::RELATION_PRODUCT);
    }

    /**
     * @return MorphOne
     */
    public function link(): MorphOne
    {
        return $this->morphOne(Link::class, Link::RELATION_LINKABLE);
    }

    /**
     * @return Link
     */
    public function getLink(): ?Link
    {
        return $this->getRelationValue(self::RELATION_LINK);
    }

    /**
     * @return string
     */
    public function getOwnerType(): string
    {
        return self::OWNER_TYPE;
    }

    /**
     * @return int
     */
    public function getOwnerKey(): int
    {
        return $this->getKey();
    }

    public function getAffiliation(): ?Affiliation
    {
        return $this->getStore()->getAffiliation();
    }
}
