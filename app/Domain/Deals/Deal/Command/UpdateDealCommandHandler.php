<?php

namespace App\Domain\Deals\Deal\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Deal\Repository\Deals;

final class UpdateDealCommandHandler extends Handler
{
    /**
     * @var Deals
     */
    private $deals;

    /**
     * UpdateDealCommandHandler constructor.
     * @param Deals $deals
     */
    public function __construct(Deals $deals)
    {
        $this->deals = $deals;
    }

    /**
     * @param UpdateDealCommand $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        $deal = $object->getDeal()->fill($object->getParams());

        $deal = $this->deals->save($deal, $object->getRelated());

        return $this->deals->push($deal);
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UpdateDealCommand;
    }
}
