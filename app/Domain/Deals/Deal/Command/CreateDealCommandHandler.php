<?php

namespace App\Domain\Deals\Deal\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Deal\Repository\Deals;

final class CreateDealCommandHandler extends Handler
{
    /**
     * @var Deals
     */
    private $deals;

    /**
     * CreateDealCommandHandler constructor.
     * @param Deals $deals
     */
    public function __construct(Deals $deals)
    {
        $this->deals = $deals;
    }

    /**
     * @param CreateDealCommand $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        return $this->deals->save($object->getDeal(), $object->getRelated());
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof CreateDealCommand;
    }
}
