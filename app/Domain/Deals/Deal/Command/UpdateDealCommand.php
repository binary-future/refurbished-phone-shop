<?php


namespace App\Domain\Deals\Deal\Command;


use App\Domain\Deals\Deal\Deal;

final class UpdateDealCommand
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var array
     */
    private $params;

    /**
     * @var array
     */
    private $related;

    /**
     * UpdateDealCommand constructor.
     * @param Deal $deal
     * @param array $params
     * @param array $related
     */
    public function __construct(Deal $deal, array $params, array $related = [])
    {
        $this->deal = $deal;
        $this->params = $params;
        $this->related = $related;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
