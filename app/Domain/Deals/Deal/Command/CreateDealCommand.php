<?php

namespace App\Domain\Deals\Deal\Command;

use App\Domain\Deals\Deal\Deal;

final class CreateDealCommand
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var array
     */
    private $related = [];

    /**
     * CreateDealCommand constructor.
     * @param Deal $deal
     * @param array $related
     */
    public function __construct(Deal $deal, array $related = [])
    {
        $this->deal = $deal;
        $this->related = $related;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
