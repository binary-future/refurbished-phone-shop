<?php


namespace App\Domain\Deals\Deal\Command;


use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Domain\Common\Bus\Handler;
use App\Domain\Common\Repository\ChunkResponse;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\NotUpdatedDealsQuery;
use App\Domain\Deals\Deal\Repository\Deals;
use App\Domain\Store\Store;
use App\Utils\Adapters\Log\Contracts\Logger;

final class DeleteDealsByStoreAndDateCommandHandler extends Handler
{
    private const CHUNK_LIMIT = 1000;

    /**
     * @var Deals
     */
    private $deals;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * DeleteDealsByStoreAndDateCommandHandler constructor.
     * @param Deals $deals
     * @param Logger $logger
     */
    public function __construct(Deals $deals, Logger $logger)
    {
        $this->deals = $deals;
        $this->logger = $logger;
    }

    /**
     * @param DeleteDealsByStoreAndDateCommand $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        $query = new NotUpdatedDealsQuery(
            $object->getStore(),
            $object->getDate()
        );
        $result = $this->deals->chunk($query, $this->generateAlgorithm(), self::CHUNK_LIMIT);

        return $this->prepareResult($result, $object->getStore());
    }

    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DeleteDealsByStoreAndDateCommand;
    }

    /**
     * @return callable
     */
    private function generateAlgorithm(): callable
    {
        return function ($deals) {
            $succeed = 0;
            $failed = 0;
            foreach ($deals as $deal) {
                $this->logger->info(memory_get_usage() / 1048576);
                /**
                 * @var Deal $deal
                 */
                try {
                    $this->deleteDeal($deal);
                    $succeed++;
                } catch (\Throwable $exception) {
                    $failed++;
                }
            }

            ChunkResponse::addParam(['succeed' => $succeed, 'failed' => $failed]);
        };
    }

    /**
     * @param Deal $deal
     */
    private function deleteDeal(Deal $deal)
    {
        $this->deals->delete($deal);
    }

    /**
     * @param ChunkResponse $response
     * @param Store $store
     * @return SoftDealsClearResponse
     */
    private function prepareResult(ChunkResponse $response, Store $store): SoftDealsClearResponse
    {
        $succeed = 0;
        $failed = 0;
        foreach ($response->getResponse() as $param) {
            $succeed += $param['succeed'] ?? 0;
            $failed += $param['failed'] ?? 0;
        }

        return new SoftDealsClearResponse($store, $succeed, $failed);
    }
}
