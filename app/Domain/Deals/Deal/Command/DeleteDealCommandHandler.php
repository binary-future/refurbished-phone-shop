<?php

namespace App\Domain\Deals\Deal\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Deal\Repository\Deals;

final class DeleteDealCommandHandler extends Handler
{
    /**
     * @var Deals
     */
    private $deals;

    /**
     * DeleteDealCommandHandler constructor.
     * @param Deals $deals
     */
    public function __construct(Deals $deals)
    {
        $this->deals = $deals;
    }

    /**
     * @param DeleteDealCommand $object
     * @return mixed
     */
    protected function proceed($object)
    {
        $deal = $object->getDeal();
        $this->deals->delete($deal);

        return $deal;
    }

    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DeleteDealCommand;
    }
}
