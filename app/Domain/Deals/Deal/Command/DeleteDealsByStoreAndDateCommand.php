<?php


namespace App\Domain\Deals\Deal\Command;


use App\Domain\Store\Store;
use Carbon\Carbon;

/**
 * Class DeleteDealsByStoreAndDateCommand
 * @package App\Domain\Deals\Deal\Command
 */
final class DeleteDealsByStoreAndDateCommand
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var Carbon
     */
    private $date;

    /**
     * DeleteDealsByStoreAndDateCommand constructor.
     * @param Store $store
     * @param Carbon $date
     */
    public function __construct(Store $store, Carbon $date)
    {
        $this->store = $store;
        $this->date = $date;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }
}
