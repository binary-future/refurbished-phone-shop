<?php


namespace App\Domain\Deals\Deal\Command;


use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Deal\DomainResponse\DeleteDealsDomainResponse;
use App\Domain\Deals\Deal\Query\NotUpdatedDealsQuery;
use App\Domain\Deals\Deal\Services\Link\Contracts\MassiveDealsDelete;

final class DeleteDealsOlderThanDateCommandHandler extends Handler
{
    /**
     * @var MassiveDealsDelete
     */
    private $massiveDealsDelete;

    /**
     * DeleteDealsOlderThanDateCommandHandler constructor.
     * @param MassiveDealsDelete $massiveDealsDelete
     */
    public function __construct(MassiveDealsDelete $massiveDealsDelete)
    {
        $this->massiveDealsDelete = $massiveDealsDelete;
    }

    /**
     * @param DeleteDealsOlderThanDateCommand $command
     * @return DeleteDealsDomainResponse
     */
    protected function proceed($command): DeleteDealsDomainResponse
    {
        $query = new NotUpdatedDealsQuery(
            null,
            $command->getDate()
        );

        [$deletedDeals, $deletedLinks] = $this->massiveDealsDelete->execute($query);

        return new DeleteDealsDomainResponse($deletedDeals, $deletedLinks);
    }

    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DeleteDealsOlderThanDateCommand;
    }
}
