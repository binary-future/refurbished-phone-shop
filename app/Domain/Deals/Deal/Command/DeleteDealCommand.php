<?php

namespace App\Domain\Deals\Deal\Command;

use App\Domain\Deals\Deal\Deal;

/**
 * Class DeleteDealCommand
 * @package App\Domain\Deals\Deal\Command
 */
final class DeleteDealCommand
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * DeleteDealCommand constructor.
     * @param Deal $deal
     */
    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }
}
