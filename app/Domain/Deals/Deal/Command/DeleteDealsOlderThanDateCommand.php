<?php


namespace App\Domain\Deals\Deal\Command;


use App\Domain\Store\Store;
use Carbon\Carbon;

/**
 * Class DeleteDealsOlderThanDateCommand
 * @package App\Domain\Deals\Deal\Command
 */
final class DeleteDealsOlderThanDateCommand
{
    /**
     * @var Carbon
     */
    private $date;

    /**
     * DeleteDealsOlderThanDateCommand constructor.
     * @param Carbon $date
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return $this->date;
    }
}
