<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class MonthlyCostByDealsIdsQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ColumnByDealsIdsQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ColumnByDealsIdsQuery;
    }
}
