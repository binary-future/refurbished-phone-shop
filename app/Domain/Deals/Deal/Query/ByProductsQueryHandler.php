<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class ByProductsQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ByProductsQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByProductsQuery;
    }
}
