<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class NotUpdatedDealsQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class NotUpdatedDealsQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof NotUpdatedDealsQuery;
    }
}
