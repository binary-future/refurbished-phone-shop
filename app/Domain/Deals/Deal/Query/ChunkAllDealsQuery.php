<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Chunkable;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

/**
 * Class ChunkAllDealsQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class ChunkAllDealsQuery implements Chunkable, DealsCriteriaDictionary
{
    /**
     * @var callable
     */
    private $algorithm;

    /**
     * @var int
     */
    private $limit;

    /**
     * @param callable $algorithm
     * @return mixed|void
     */
    public function setAlgorithm(callable $algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @return callable|null
     */
    public function getAlgorithm(): ?callable
    {
        return $this->algorithm;
    }

    /**
     * @param int|null $limit
     * @return mixed|void
     */
    public function setLimit(?int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => [],
        ];
    }
}
