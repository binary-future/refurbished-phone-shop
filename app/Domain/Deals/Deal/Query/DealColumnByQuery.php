<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Common\Contracts\Repository\PluckableQuery;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

final class DealColumnByQuery implements Query, DealsCriteriaDictionary, PluckableQuery
{
    /**
     * @var Query
     */
    private $query;

    /**
     * @var string
     */
    private $column;

    /**
     * DealColumnByQuery constructor.
     * @param Query $query
     * @param string $column
     */
    public function __construct(Query $query, string $column)
    {
        $this->query = $query;
        $this->column = $column;
    }

    public function getCriteria(): array
    {
        $criteria = $this->query->getCriteria();
        $criteria[self::SELECT] = $this->column;
        $criteria[self::UNIQUE] = true;

        return $criteria;
    }

    public function chooseColumn(string $column)
    {
        $this->column = $column;
    }

    public function chosenColumn(): ?string
    {
        return $this->column;
    }

}
