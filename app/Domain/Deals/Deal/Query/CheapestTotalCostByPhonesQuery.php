<?php


namespace App\Domain\Deals\Deal\Query;

/**
 * Class CheapestTotalCostByPhonesQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class CheapestTotalCostByPhonesQuery extends CheapestCostByPhonesQuery
{
    /**
     * @return string
     */
    protected function getCostCriteria(): string
    {
        return self::CRITERIA_BY_TOTAL_COST;
    }

    protected function getSoringCriteria(): string
    {
        return self::CRITERIA_SORT_BY_TOTAL_COST;
    }


}
