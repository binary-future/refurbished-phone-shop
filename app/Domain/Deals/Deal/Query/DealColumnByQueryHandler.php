<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class DealColumnByQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class DealColumnByQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DealColumnByQuery;
    }
}