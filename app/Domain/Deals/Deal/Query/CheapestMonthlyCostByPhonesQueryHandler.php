<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class CheapestMonthlyCostByPhonesQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class CheapestMonthlyCostByPhonesQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof CheapestMonthlyCostByPhonesQuery;
    }
}
