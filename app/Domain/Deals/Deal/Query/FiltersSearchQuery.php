<?php

namespace App\Domain\Deals\Deal\Query;

use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Common\Contracts\Repository\PaginableQuery;
use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Illuminate\Support\Collection;

class FiltersSearchQuery implements Query, DealsCriteriaDictionary, PaginableQuery
{
    /**
     * @var Collection
     */
    private $products;

    /**
     * @var int
     */
    private $monthlyCost;
    /**
     * @var int
     */
    private $totalCost;

    /**
     * @var array
     */
    private $networks;

    /**
     * @var array
     */
    private $conditions;

    /**
     * @var array
     */
    private $monthlyCostRange;

    /**
     * @var array
     */
    private $totalCostRange;

    /**
     * @var string
     */
    private $sort;

    /**
     * @var bool
     */
    private $disableSort = false;
    /**
     * @var int
     */
    private $offset;
    /**
     * @var int
     */
    private $limit;
    /**
     * @var array
     */
    private $relations;
    /**
     * @var int|null
     */
    private $perPage;
    /**
     * @var bool
     */
    private $joinContracts;

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     */
    public function setProducts(Collection $products): void
    {
        $this->products = $products;
    }

    /**
     * @return int
     */
    public function getMonthlyCost(): int
    {
        return $this->monthlyCost;
    }

    /**
     * @param int $monthlyCost
     */
    public function setMonthlyCost(int $monthlyCost): void
    {
        $this->monthlyCost = $monthlyCost;
    }

    /**
     * @return int
     */
    public function getTotalCost(): int
    {
        return $this->totalCost;
    }

    /**
     * @param int $totalCost
     */
    public function setTotalCost(int $totalCost): void
    {
        $this->totalCost = $totalCost;
    }

    public function setNetworks(array $networks): void
    {
        $this->networks = array_map(static function ($val) {
            return (int) $val;
        }, $networks);
    }

    public function getNetworks(): ?array
    {
        return $this->networks;
    }

    public function setConditions(array $conditions): void
    {
        $this->conditions = array_map(static function ($val) {
            return (string) $val;
        }, $conditions);
    }

    public function getConditions(): ?array
    {
        return $this->conditions;
    }

    /**
     * @param array $monthlyCostRange
     */
    public function setMonthlyCostRange(array $monthlyCostRange): void
    {
        $this->monthlyCostRange = $monthlyCostRange;
    }

    /**
     * @param string|null $sort
     */
    public function setSort(?string $sort): void
    {
        $sortCriteria = [
            self::CRITERIA_SORT_BY_MONTHLY_COST,
            self::CRITERIA_SORT_BY_UPFRONT_COST,
            self::CRITERIA_SORT_BY_TOTAL_COST,
        ];

        $this->sort = in_array($sort, $sortCriteria, true) ? $sort : null;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    /**
     * @param array $totalCostRange
     */
    public function setTotalCostRange(array $totalCostRange): void
    {
        $this->totalCostRange = $totalCostRange;
    }

    /**
     * @return array|null
     */
    public function getRelations(): ?array
    {
        return $this->relations;
    }

    /**
     * @param array $relations
     */
    public function setRelations(array $relations): void
    {
        $this->relations = $relations;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }
    /**
     * @return int|null
     */
    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    /**
     * @param int|null $perPage
     */
    public function setPerPage(?int $perPage): void
    {
        $this->perPage = $perPage;
    }

    public function getCriteria(): array
    {
        $criteria = [
            self::SELECT => Deal::TABLE . '.*',
            self::CRITERIA_BY_PRODUCTS => $this->products,
            self::CRITERIA_BY_TOTAL_COST =>
                $this->getNumericCompareValue($this->totalCost, OperatorDictionary::OD_LESS_EQUAL),
            self::CRITERIA_BY_MONTHLY_COST =>
                $this->getNumericCompareValue($this->monthlyCost, OperatorDictionary::OD_LESS_EQUAL),
            self::CRITERIA_BY_CONTRACT_NETWORKS => $this->networks ?: null,
            self::CRITERIA_BY_CONTRACT_CONDITIONS => $this->conditions ?: null,
            self::CRITERIA_BY_MONTHLY_COST_RANGE => $this->getBetweenValues($this->monthlyCostRange),
            self::CRITERIA_BY_TOTAL_COST_RANGE => $this->getBetweenValues($this->totalCostRange),
            self::OFFSET => $this->offset === 0 ? null : $this->offset,
            self::LIMIT => $this->limit,
            self::WITH => $this->getRelations(),
        ];

        if ($this->joinContracts) {
            $criteria[self::CRITERIA_JOIN_CONTRACTS ] = 'inner';
        }

        if (! $this->disableSort) {
            $criteria[$this->getSort()] = true;
        }

        return array_filter($criteria, static function ($item) {
            return $item !== null;
        });
    }

    /**
     * @return string
     */
    public function getSort(): string
    {
        return $this->sort ?: self::CRITERIA_SORT_BY_MONTHLY_COST;
    }

    /**
     * @param bool $disable
     */
    public function disableSort(bool $disable = false)
    {
        $this->disableSort = $disable;
    }

    /**
     * @param bool $isJoin
     */
    public function joinContracts(bool $isJoin = false)
    {
        $this->joinContracts = $isJoin;
    }

    /**
     * @param string|int $value
     * @param string $operator
     * @return CompareValue|null
     */
    private function getNumericCompareValue($value, string $operator): ?CompareValue
    {
        return is_numeric($value)
            ? new CompareValue((int)$value, $operator)
            : null;
    }

    /**
     * @param array $range
     * @return BetweenValues
     */
    private function getBetweenValues(?array $range): ?BetweenValues
    {
        return $range ? new BetweenValues(min($range), max($range)) : null;
    }
}
