<?php

namespace App\Domain\Deals\Deal\Query;

use App\Domain\Common\Contracts\Repository\PaginableQuery;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use Illuminate\Support\Collection;

/**
 * Class ByProductsQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class ByProductsQuery implements PaginableQuery, DealsCriteriaDictionary
{
    /**
     * @var Collection
     */
    private $products;

    /**
     * @var int
     */
    private $perPage;

    /**
     * @var array
     */
    private $related;

    /**
     * @var string|null
     */
    private $sorting;

    /**
     * @var int
     */
    private $limit;

    /**
     * ByPhoneIdsQuery constructor.
     * @param Collection $products
     * @param array $related
     * @param string|null $sorting
     */
    public function __construct(Collection $products, array $related = [], string $sorting = null)
    {
        $this->products = $products;
        $this->related = $related;
        $this->sorting = $sorting;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }

    /**
     * @return int
     */
    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        $criteria = [
            self::CRITERIA_BY_PRODUCTS => $this->products,
            self::WITH => $this->related,
            $this->getSorting() => true,
        ];

        if ($this->limit) {
            $criteria[self::LIMIT] = $this->limit;
        }

        return $criteria;
    }

    private function getSorting()
    {
        $sortCriteria = [
            self::CRITERIA_SORT_BY_MONTHLY_COST,
            self::CRITERIA_SORT_BY_UPFRONT_COST,
            self::CRITERIA_SORT_BY_TOTAL_COST,
        ];

        return $this->sorting && in_array($this->sorting, $sortCriteria, true)
            ? $this->sorting
            : self::CRITERIA_SORT_BY_MONTHLY_COST;
    }
}
