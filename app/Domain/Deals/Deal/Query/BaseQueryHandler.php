<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Bus\Handler;
use App\Domain\Common\Contracts\Repository\Chunkable;
use App\Domain\Common\Contracts\Repository\PaginableQuery;
use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\Deals;

abstract class BaseQueryHandler extends Handler
{
    /**
     * @var Deals
     */
    private $deals;

    /**
     * ByStoreIdProductAndContractIdQueryHandler constructor.
     * @param Deals $deals
     */
    public function __construct(Deals $deals)
    {
        $this->deals = $deals;
    }

    /**
     * @param PaginableQuery $object
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function proceed($object)
    {
        if ($object instanceof PaginableQuery && $object->getPerPage()) {
            return $this->deals->findPaginated($object, $object->getPerPage());
        }
        if ($object instanceof Chunkable && $object->getAlgorithm()) {
            return $this->deals->chunk($object, $object->getAlgorithm(), $object->getLimit());
        }

        return $this->deals->findBy($object);
    }
}
