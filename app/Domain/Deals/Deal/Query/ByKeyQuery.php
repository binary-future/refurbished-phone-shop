<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

final class ByKeyQuery implements DealsCriteriaDictionary, Query
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $relations;

    /**
     * ByKeyQuery constructor.
     * @param int $id
     * @param array $relations
     */
    public function __construct(int $id, array $relations = [])
    {
        $this->id = $id;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ID => $this->id,
            self::WITH => $this->relations
        ];
    }
}
