<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class CheapestTotalCostByPhonesQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class CheapestTotalCostByPhonesQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof CheapestTotalCostByPhonesQuery;
    }
}
