<?php


namespace App\Domain\Deals\Deal\Query;

/**
 * Class ChunkAllDealsQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ChunkAllDealsQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ChunkAllDealsQuery;
    }
}
