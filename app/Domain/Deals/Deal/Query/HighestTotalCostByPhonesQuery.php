<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use Illuminate\Support\Collection;

final class HighestTotalCostByPhonesQuery implements Query, DealsCriteriaDictionary
{
    /**
     * @var Collection
     */
    private $phones;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var float
     */
    private $costFrom;

    /**
     * CheapestMonthlyCostByPhones constructor.
     * @param Collection $phones
     * @param int $limit
     * @param float $costFrom
     */
    public function __construct(Collection $phones, int $limit = 1, float $costFrom = 1)
    {
        $this->phones = $phones;
        $this->limit = $limit;
        $this->costFrom = $costFrom;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_PRODUCTS => $this->phones,
            self::CRITERIA_SORT_BY_TOTAL_COST => false,
            self::LIMIT => $this->limit,
        ];
    }
}
