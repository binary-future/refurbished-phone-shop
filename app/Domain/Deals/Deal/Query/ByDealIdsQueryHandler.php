<?php


namespace App\Domain\Deals\Deal\Query;


/**
 * Class ByDealIdsQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ByDealIdsQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByDealIdsQuery;
    }
}
