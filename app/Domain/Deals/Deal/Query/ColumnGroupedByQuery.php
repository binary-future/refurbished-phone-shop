<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\PluckableQuery;
use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

/**
 * Class ColumnByDealsIdsQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class ColumnGroupedByQuery implements Query, DealsCriteriaDictionary, PluckableQuery
{

    /**
     * @var string
     */
    private $column;

    /**
     * @var string
     */
    private $groupByColumn;

    /**
     * ColumnByDealsIdsQuery constructor.
     * @param string $groupByColumn
     */
    public function __construct(string $groupByColumn)
    {
        $this->groupByColumn = $groupByColumn;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::SELECT => $this->chosenColumn(),
            self::GROUP_BY => $this->groupByColumn
        ];
    }

    /**
     * @param string $column
     * @return mixed|void
     */
    public function chooseColumn(string $column)
    {
        $this->column = $column;
    }

    /**
     * @return string|null
     */
    public function chosenColumn(): ?string
    {
        return $this->column;
    }
}
