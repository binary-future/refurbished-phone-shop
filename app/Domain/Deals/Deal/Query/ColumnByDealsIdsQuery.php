<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Common\Contracts\Repository\PluckableQuery;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use Illuminate\Support\Collection;

/**
 * Class ColumnByDealsIdsQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class ColumnByDealsIdsQuery implements Query, DealsCriteriaDictionary, PluckableQuery
{
    /**
     * @var Collection
     */
    private $dealsIds;

    /**
     * @var string
     */
    private $column;

    /**
     * @var bool
     */
    private $unique;

    /**
     * ColumnByDealsIdsQuery constructor.
     * @param Collection $dealsIds
     * @param string $column
     * @param bool $unique
     */
    public function __construct(Collection $dealsIds, string $column, bool $unique = true)
    {
        $this->dealsIds = $dealsIds;
        $this->column = $column;
        $this->unique = $unique;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::SELECT => $this->chosenColumn(),
            self::UNIQUE => $this->unique,
            self::CRITERIA_DEALS_IDS => $this->dealsIds,
        ];
    }

    /**
     * @param string $column
     * @return mixed|void
     */
    public function chooseColumn(string $column)
    {
        $this->column = $column;
    }

    /**
     * @return string|null
     */
    public function chosenColumn(): ?string
    {
        return $this->column;
    }
}
