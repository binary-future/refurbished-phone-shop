<?php

namespace App\Domain\Deals\Deal\Query;

final class NetworksColumnByQueryHandler extends BaseQueryHandler
{
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof NetworksColumnByQuery;
    }
}
