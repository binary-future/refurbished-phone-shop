<?php


namespace App\Domain\Deals\Deal\Query;

/**
 * Class ByKeyQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ByKeyQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByKeyQuery;
    }
}
