<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class CheapestUpfrontCostByPhoneQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class CheapestUpfrontCostByPhoneQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof CheapestUpfrontCostByPhoneQuery;
    }
}
