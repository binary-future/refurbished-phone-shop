<?php

namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

/**
 * Class ByExternalIdQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class ByExternalIdAndStoreQuery implements Query, DealsCriteriaDictionary
{
    /**
     * @var int
     */
    private $externalId;

    /**
     * @var int
     */
    private $storeId;
    /**
     * @var array
     */
    private $relations;

    /**
     * ByExternalIdAndStoreQuery constructor.
     * @param string $externalId
     * @param int $storeId
     * @param array $relations
     */
    public function __construct(string $externalId, int $storeId, array $relations = [])
    {
        $this->externalId = $externalId;
        $this->storeId = $storeId;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::CRITERIA_BY_EXTERNAL_ID => $this->externalId,
            self::CRITERIA_BY_STORE_ID => $this->storeId,
        ];
    }
}
