<?php


namespace App\Domain\Deals\Deal\Query;


/**
 * Class ContractColumnByQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ContractColumnByQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ContractColumnByQuery;
    }
}
