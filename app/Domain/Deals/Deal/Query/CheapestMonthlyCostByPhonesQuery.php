<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class CheapestMonthlyCostByPhonesQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class CheapestMonthlyCostByPhonesQuery extends CheapestCostByPhonesQuery
{
    protected function getCostCriteria(): string
    {
        return self::CRITERIA_BY_MONTHLY_COST;
    }

    protected function getSoringCriteria(): string
    {
        return self::CRITERIA_SORT_BY_MONTHLY_COST;
    }
}
