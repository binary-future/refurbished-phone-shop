<?php


namespace App\Domain\Deals\Deal\Query;


/**
 * Class HighestTotalCostByPhonesQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class HighestTotalCostByPhonesQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof HighestTotalCostByPhonesQuery;
    }
}
