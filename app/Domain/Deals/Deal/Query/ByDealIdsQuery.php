<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use Illuminate\Support\Collection;

final class ByDealIdsQuery implements Query, DealsCriteriaDictionary
{
    /**
     * @var array
     */
    private $ids;

    /**
     * @var array
     */
    private $relations;

    /**
     * @var string
     */
    private $sort;

    /**
     * ByDealIdsQuery constructor.
     * @param Collection $ids
     * @param array $relations
     * @param string $sort
     */
    public function __construct(Collection $ids, array $relations, string $sort)
    {
        $this->ids = $ids;
        $this->relations = $relations;
        $this->sort = $sort;
    }

    public function getCriteria(): array
    {
        $criteria = [
            self::CRITERIA_DEALS_IDS => $this->ids,
            self::WITH => $this->relations,
        ];
        if ($this->sort) {
            $criteria[$this->sort] = true;
        }

        return $criteria;
    }
}
