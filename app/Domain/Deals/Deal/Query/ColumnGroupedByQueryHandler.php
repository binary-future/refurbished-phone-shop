<?php


namespace App\Domain\Deals\Deal\Query;


/**
 * Class ColumnGroupedByQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ColumnGroupedByQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ColumnGroupedByQuery;
    }
}
