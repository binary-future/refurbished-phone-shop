<?php

namespace App\Domain\Deals\Deal\Query;

use App\Domain\Common\Contracts\Repository\Chunkable;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use App\Domain\Store\Store;
use App\Utils\Repository\Criteria\Common\CompareValue;
use Carbon\Carbon;

/**
 * Class NotUpdatedDealsQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class NotUpdatedDealsQuery implements Chunkable, DealsCriteriaDictionary
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var Carbon
     */
    private $date;

    /**
     * @var callable
     */
    private $algorithm;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * NotUpdatedDealsQuery constructor.
     * @param Store|null $store
     * @param Carbon $date
     */
    public function __construct(?Store $store, Carbon $date)
    {
        $this->store = $store;
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        $criteria = [
            self::CRITERIA_UPDATED_AT => new CompareValue($this->date, '<')
        ];

        if ($this->store) {
            $criteria[self::CRITERIA_BY_STORE_ID] = $this->store->getKey();
        }

        return $criteria;
    }

    /**
     * @return callable|null
     */
    public function getAlgorithm(): ?callable
    {
        return $this->algorithm;
    }

    /**
     * @param callable $algorithm
     */
    public function setAlgorithm(callable $algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }
}
