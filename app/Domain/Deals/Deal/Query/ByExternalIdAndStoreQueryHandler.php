<?php


namespace App\Domain\Deals\Deal\Query;


/**
 * Class ByExternalIdQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class ByExternalIdAndStoreQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByExternalIdAndStoreQuery;
    }
}
