<?php


namespace App\Domain\Deals\Deal\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

/**
 * Class RelatedDealsQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class RelatedDealsQuery implements Query, DealsCriteriaDictionary
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var int|null
     */
    private $limit;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * RelatedDealsQuery constructor.
     * @param Deal $deal
     * @param int $limit
     * @param array $relations
     */
    public function __construct(Deal $deal, int $limit = null, array $relations = [])
    {
        $this->deal = $deal;
        $this->limit = $limit;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        $criteria = [
            self::CRITERIA_EXCEPT_DEAL => $this->deal,
            self::CRITERIA_BY_PRODUCT => $this->deal->getProduct(),
            self::WITH => $this->relations,
        ];
        if ($this->limit) {
            $criteria[self::LIMIT] = $this->limit;
        }

        return $criteria;
    }
}
