<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class RelatedDealsQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class RelatedDealsQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof RelatedDealsQuery;
    }
}
