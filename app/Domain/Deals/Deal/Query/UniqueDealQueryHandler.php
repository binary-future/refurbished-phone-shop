<?php

namespace App\Domain\Deals\Deal\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Deal\Repository\Deals;

final class UniqueDealQueryHandler extends Handler
{
    /**
     * @var Deals
     */
    private $deals;

    /**
     * ByStoreIdProductAndContractIdQueryHandler constructor.
     * @param Deals $deals
     */
    public function __construct(Deals $deals)
    {
        $this->deals = $deals;
    }

    /**
     * @param UniqueDealQuery $object
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function proceed($object)
    {
        return $this->deals->findBy($object);
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UniqueDealQuery;
    }
}
