<?php

namespace App\Domain\Deals\Deal\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Deals\Deal\Payment;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;

final class UniqueDealQuery implements Query, DealsCriteriaDictionary
{
    /**
     * @var int
     */
    private $storeId;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var Payment
     */
    private $payment;

    /**
     * @var int|null
     */
    private $contractId;
    /**
     * @var array
     */
    private $relations;

    /**
     * UniqueDealQuery constructor.
     * @param int $storeId
     * @param Product $product
     * @param Payment $payment
     * @param int|null $contractId
     * @param array $relations
     */
    public function __construct(
        int $storeId,
        Product $product,
        Payment $payment,
        ?int $contractId,
        array $relations = []
    ) {
        $this->storeId = $storeId;
        $this->product = $product;
        $this->payment = $payment;
        $this->contractId = $contractId;
        $this->relations = $relations;
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->storeId;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return Payment
     */
    public function getPayment(): Payment
    {
        return $this->payment;
    }

    /**
     * @return int|null
     */
    public function getContractId(): ?int
    {
        return $this->contractId;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::CRITERIA_BY_STORE_ID => $this->getStoreId(),
            self::CRITERIA_BY_PRODUCT => $this->getProduct(),
            self::CRITERIA_BY_PAYMENT => $this->getPayment(),
            self::CRITERIA_BY_CONTRACT_ID => $this->getContractId()
        ];
    }
}
