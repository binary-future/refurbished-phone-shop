<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class FiltersSearchQueryHandler
 * @package App\Domain\Deals\Deal\Query
 */
final class FiltersSearchQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof FiltersSearchQuery;
    }
}
