<?php

namespace App\Domain\Deals\Deal\Query;

/**
 * Class CheapestUpfrontCostByPhoneQuery
 * @package App\Domain\Deals\Deal\Query
 */
final class CheapestUpfrontCostByPhoneQuery extends CheapestCostByPhonesQuery
{
    /**
     * @return string
     */
    protected function getCostCriteria(): string
    {
        return self::CRITERIA_BY_UPFRONT_COST;
    }

    protected function getSoringCriteria(): string
    {
        return self::CRITERIA_SORT_BY_UPFRONT_COST;
    }


}
