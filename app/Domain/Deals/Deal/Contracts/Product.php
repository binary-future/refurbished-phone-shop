<?php

namespace App\Domain\Deals\Deal\Contracts;

interface Product
{
    public function getType();

    public function getKey();
}
