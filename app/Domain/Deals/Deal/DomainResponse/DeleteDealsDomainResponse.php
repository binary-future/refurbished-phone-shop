<?php


namespace App\Domain\Deals\Deal\DomainResponse;


final class DeleteDealsDomainResponse
{
    /**
     * @var int
     */
    private $deletedDeals;
    /**
     * @var int
     */
    private $deletedLinks;

    /**
     * DeleteDealsDomainResponse constructor.
     * @param int $deletedDeals
     * @param int $deletedLinks
     */
    public function __construct(int $deletedDeals, int $deletedLinks)
    {
        $this->deletedDeals = $deletedDeals;
        $this->deletedLinks = $deletedLinks;
    }

    /**
     * @return int
     */
    public function getDeletedDeals(): int
    {
        return $this->deletedDeals;
    }

    /**
     * @return int
     */
    public function getDeletedLinks(): int
    {
        return $this->deletedLinks;
    }
}
