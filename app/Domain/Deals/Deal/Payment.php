<?php

namespace App\Domain\Deals\Deal;

class Payment
{
    /**
     * @var float
     */
    private $upfrontCost;

    /**
     * @var float
     */
    private $monthlyCost;
    /**
     * @var int
     */
    private $totalCost;

    /**
     * Payment constructor.
     * @param float $upfrontCost
     * @param float $monthlyCost
     * @param int $totalCost
     */
    public function __construct(float $upfrontCost, float $monthlyCost, int $totalCost)
    {
        if (! $this->isValid($monthlyCost)) {
            throw new \InvalidArgumentException('Invalid payment params');
        }
        $this->upfrontCost = $upfrontCost;
        $this->monthlyCost = $monthlyCost;
        $this->totalCost = $totalCost;
    }

    private function isValid(float $monthlyCost): bool
    {
        return $monthlyCost > 0;
    }

    /**
     * @return float
     */
    public function getUpfrontCost(): float
    {
        return $this->upfrontCost;
    }

    /**
     * @return float
     */
    public function getMonthlyCost(): float
    {
        return $this->monthlyCost;
    }

    /**
     * @return float
     */
    public function getTotalCost(): float
    {
        return $this->totalCost;
    }

    /**
     * @param self $payment
     * @return bool
     */
    public function equal(self $payment): bool
    {
        return $payment->getUpfrontCost() === $this->getUpfrontCost()
            && $payment->getMonthlyCost() === $this->getMonthlyCost()
            && $payment->getTotalCost() === $this->getTotalCost();
    }
}
