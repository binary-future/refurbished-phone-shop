<?php

namespace App\Domain\Deals\Deal\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

/**
 * Interface DealsCriteriaDictionary
 * @package App\Domain\Deals\Deal\Repository
 */
interface DealsCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_STORE_ID = 'by-store-id';
    public const CRITERIA_BY_PRODUCT = 'by-product';
    public const CRITERIA_BY_PRODUCTS = 'by-products';
    public const CRITERIA_BY_CONTRACT_ID = 'by-contract-id';
    public const CRITERIA_BY_PAYMENT = 'by-payment';
    public const CRITERIA_BY_EXTERNAL_ID = 'by-external-id';
    public const CRITERIA_BY_MONTHLY_COST = 'by-monthly-cost';
    public const CRITERIA_BY_MONTHLY_COST_RANGE = 'by-monthly-cost-range';
    public const CRITERIA_BY_UPFRONT_COST = 'by-upfront-cost';
    public const CRITERIA_BY_UPFRONT_COST_RANGE = 'by-upfront-cost-range';
    public const CRITERIA_BY_TOTAL_COST = 'by-total-cost';
    public const CRITERIA_BY_TOTAL_COST_RANGE = 'by-total-cost-range';
    public const CRITERIA_BY_CONTRACT_NETWORK = 'by-contract-network';
    public const CRITERIA_BY_CONTRACT_NETWORKS = 'by-contract-networks';
    public const CRITERIA_BY_CONTRACT_CONDITIONS = 'by-contract-conditions';
    public const CRITERIA_SORT_BY_UPFRONT_COST = 'sort-by-upfront-cost';
    public const CRITERIA_SORT_BY_MONTHLY_COST = 'sort-by-monthly-cost';
    public const CRITERIA_SORT_BY_TOTAL_COST = 'sort-by-total-cost';
    public const CRITERIA_EXCEPT_DEAL = 'except-deal';
    public const CRITERIA_UPDATED_AT = 'updated-at';
    public const CRITERIA_DEALS_IDS = 'by-deals-ids';
    public const CRITERIA_SELECT_CONTRACT_COLUMN = 'select-contract-column';
    public const CRITERIA_SELECT_NETWORK_COLUMN = 'select-network-column';
    public const CRITERIA_JOIN_CONTRACTS = 'join-contracts';
}
