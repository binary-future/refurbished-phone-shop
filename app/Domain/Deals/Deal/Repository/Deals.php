<?php

namespace App\Domain\Deals\Deal\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface Deals
 * @package App\Domain\Deals\Deal\Repository
 */
interface Deals extends Repository
{

}
