<?php

namespace App\Domain\Deals\Contract;

use App\Utils\DataTypes\Enum\Enum;

/**
 * Class Condition
 * @package App\Domain\Deals\Contract
 *
 * @method static Condition GOOD()
 * @method static Condition VERY_GOOD()
 * @method static Condition PRISTINE()
 */
class Condition extends Enum
{
    // grades a, b, c
    private const PRISTINE = 'a';
    private const VERY_GOOD = 'b';
    private const GOOD = 'c';

    public function getKey()
    {
        return strtolower(str_replace('_', ' ', parent::getKey()));
    }
}
