<?php

namespace App\Domain\Deals\Contract;

use App\Domain\Deals\Deal\Deal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Contract extends Model
{
    public const TABLE = 'contracts';

    public const FIELD_ID = 'id';
    public const FIELD_NETWORK_ID = 'network_id';
    public const FIELD_CONDITION = 'condition';

    public const RELATION_NETWORK = 'network';
    public const RELATION_DEALS = 'deals';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_NETWORK_ID, self::FIELD_CONDITION
    ];

    /*------------------------------------------------------------------------------------------------------------------
    *
    * ATTRIBUTES
    *__________________________________________________________________________________________________________________
    */
    /**
     * @return int
     */
    public function getNetworkId(): int
    {
        return $this->getAttribute(self::FIELD_NETWORK_ID);
    }

    /**
     * @param int $networkId
     * @return mixed
     */
    public function setNetworkId(int $networkId)
    {
        return $this->setAttribute(self::FIELD_NETWORK_ID, $networkId);
    }

    /**
     * @return Condition
     */
    public function getCondition(): Condition
    {
        return new Condition($this->getAttribute(self::FIELD_CONDITION));
    }

    /**
     * @param Condition $condition
     */
    public function setCondition(Condition $condition): void
    {
        $this->setAttribute(self::FIELD_CONDITION, $condition->getValue());
    }


    /*------------------------------------------------------------------------------------------------------------------
     *
     * RELATIONS
     *__________________________________________________________________________________________________________________
     */

    /**
     * @return BelongsTo
     */
    public function network(): BelongsTo
    {
        return $this->belongsTo(Network::class, self::FIELD_NETWORK_ID);
    }

    /**
     * @return mixed
     */
    public function getNetwork()
    {
        return $this->getRelationValue(self::RELATION_NETWORK);
    }

    /**
     * @return HasMany
     */
    public function deals(): HasMany
    {
        return $this->hasMany(Deal::class);
    }

    /**
     * @return mixed
     */
    public function getDeals()
    {
        return $this->getRelationValue(self::RELATION_DEALS);
    }
}
