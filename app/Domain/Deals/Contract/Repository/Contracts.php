<?php

namespace App\Domain\Deals\Contract\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface Contracts
 * @package App\Domain\Deals\Contract\Repository
 */
interface Contracts extends Repository
{

}
