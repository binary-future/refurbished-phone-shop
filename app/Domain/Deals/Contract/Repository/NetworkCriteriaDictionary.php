<?php

namespace App\Domain\Deals\Contract\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

/**
 * Interface NetworkCriteriaDictionary
 * @package App\Domain\Deals\Contract\Repository
 */
interface NetworkCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_ALIAS = 'network-by-alias';
    public const CRITERIA_BY_SLUG = 'network-by-slug';
}
