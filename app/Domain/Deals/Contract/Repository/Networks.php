<?php

namespace App\Domain\Deals\Contract\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface Networks
 * @package App\Domain\Deals\Contract\Repository
 */
interface Networks extends Repository
{

}
