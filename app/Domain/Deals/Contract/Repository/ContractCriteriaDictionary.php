<?php

namespace App\Domain\Deals\Contract\Repository;

/**
 * Interface ContractCriteriaDictionary
 * @package App\Domain\Deals\Contract\Repository
 */
interface ContractCriteriaDictionary
{
    public const CRITERIA_BY_MODEL = 'by-model';
}
