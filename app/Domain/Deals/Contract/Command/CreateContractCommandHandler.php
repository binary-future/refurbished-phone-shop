<?php


namespace App\Domain\Deals\Contract\Command;


use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Deals\Contract\Repository\Contracts;

/**
 * Class CreateContractCommandHandler
 * @package App\Domain\Deals\Contract\Command
 */
final class CreateContractCommandHandler implements Handler
{
    /**
     * @var Contracts
     */
    private $contracts;

    /**
     * CreateContractCommandHandler constructor.
     * @param Contracts $contracts
     */
    public function __construct(Contracts $contracts)
    {
        $this->contracts = $contracts;
    }

    /**
     * @param object $object
     * @return mixed
     */
    public function handle(object $object)
    {
        /**
         * @var CreateContractCommand $object
         */
        return $this->contracts->save($object->getContract(), $object->getRelated());
    }
}
