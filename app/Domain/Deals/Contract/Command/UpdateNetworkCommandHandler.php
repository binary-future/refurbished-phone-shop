<?php

namespace App\Domain\Deals\Contract\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Contract\Repository\Networks;

/**
 * Class UpdateNetworkCommandHandler
 * @package App\Domain\Deals\Contract\Command
 */
final class UpdateNetworkCommandHandler extends Handler
{
    /**
     * @var Networks
     */
    private $networks;

    /**
     * UpdateNetworkCommandHandler constructor.
     * @param Networks $networks
     */
    public function __construct(Networks $networks)
    {
        $this->networks = $networks;
    }

    /**
     * @param UpdateNetworkCommand $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        $network = $object->getNetwork();
        if ($object->getImage()) {
            $network->setRelation('image', $object->getImage());
        }
        $network->fill($object->getParams());
        $this->networks->push($network);

        return $network;
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UpdateNetworkCommand;
    }
}
