<?php


namespace App\Domain\Deals\Contract\Command;


use App\Domain\Deals\Contract\Contract;

final class CreateContractCommand
{
    /**
     * @var Contract
     */
    private $contract;

    /**
     * @var array
     */
    private $related = [];

    /**
     * CreateContractCommand constructor.
     * @param Contract $contract
     * @param array $related
     */
    public function __construct(Contract $contract, array $related = [])
    {
        $this->contract = $contract;
        $this->related = $related;
    }

    /**
     * @return Contract
     */
    public function getContract(): Contract
    {
        return $this->contract;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
