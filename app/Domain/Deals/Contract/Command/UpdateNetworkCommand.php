<?php

namespace App\Domain\Deals\Contract\Command;

use App\Domain\Deals\Contract\Network;
use App\Domain\Shared\Image\Image;

final class UpdateNetworkCommand
{
    /**
     * @var Network
     */
    private $network;

    /**
     * @var array
     */
    private $params;

    /**
     * @var Image|null
     */
    private $image;

    /**
     * UpdateNetworkCommand constructor.
     * @param Network $network
     * @param array $params
     * @param Image|null $image
     */
    public function __construct(Network $network, array $params, ?Image $image = null)
    {
        $this->network = $network;
        $this->params = $params;
        $this->image = $image;
    }

    /**
     * @return Network
     */
    public function getNetwork(): Network
    {
        return $this->network;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }
}
