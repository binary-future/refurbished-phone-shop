<?php

namespace App\Domain\Deals\Contract\Command;

use App\Domain\Deals\Contract\Network;

final class CreateNetworkCommand
{
    /**
     * @var Network
     */
    private $network;

    /**
     * @var array
     */
    private $related = [];

    /**
     * CreateNetworkCommand constructor.
     * @param Network $network
     * @param array $related
     */
    public function __construct(Network $network, array $related = [])
    {
        $this->network = $network;
        $this->related = $related;
    }

    /**
     * @return Network
     */
    public function getNetwork(): Network
    {
        return $this->network;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
