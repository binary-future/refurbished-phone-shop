<?php

namespace App\Domain\Deals\Contract\Command;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Deals\Contract\Repository\Networks;

/**
 * Class CreateNetworkCommandHandler
 * @package App\Domain\Deals\Contract\Command
 */
final class CreateNetworkCommandHandler implements Handler
{
    /**
     * @var Networks
     */
    private $networks;

    /**
     * CreateNetworkCommandHandler constructor.
     * @param Networks $networks
     */
    public function __construct(Networks $networks)
    {
        $this->networks = $networks;
    }

    /**
     * @param object $object
     * @return mixed
     */
    public function handle(object $object)
    {
        /**
         * @var CreateNetworkCommand $object
         */
        return $this->networks->save($object->getNetwork(), $object->getRelated());
    }
}
