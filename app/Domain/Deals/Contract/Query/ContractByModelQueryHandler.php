<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Deals\Contract\Repository\Contracts;

/**
 * Class ContractByModelQueryHandler
 * @package App\Domain\Deals\Contract\Query
 */
final class ContractByModelQueryHandler implements Handler
{
    /**
     * @var Contracts
     */
    private $contracts;

    /**
     * ContractByModelQueryHandler constructor.
     * @param Contracts $contracts
     */
    public function __construct(Contracts $contracts)
    {
        $this->contracts = $contracts;
    }

    /**
     * @param object $object
     * @return \Illuminate\Support\Collection|mixed
     */
    public function handle(object $object)
    {
        /**
         * @var ContractByModelQuery $object
         */
        return $this->contracts->findBy($object);
    }
}
