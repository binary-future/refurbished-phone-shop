<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Deals\Contract\Repository\Contracts;

/**
 * Class AllConditionsQueryHandler
 * @package App\Domain\Deals\Contract\Query
 */
final class AllConditionsQueryHandler extends Handler
{
    /**
     * @var Contracts
     */
    private $contracts;

    /**
     * ContractByModelQueryHandler constructor.
     * @param Contracts $contracts
     */
    public function __construct(Contracts $contracts)
    {
        $this->contracts = $contracts;
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllConditionsQuery;
    }

    protected function proceed($object)
    {
        /**
         * @var ContractByModelQuery $object
         */
        return $this->contracts->findBy($object);
    }
}
