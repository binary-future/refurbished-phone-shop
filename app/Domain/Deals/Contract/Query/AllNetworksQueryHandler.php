<?php

namespace App\Domain\Deals\Contract\Query;

/**
 * Class AllNetworksQueryHandler
 * @package App\Domain\Deals\Contract\Query
 */
final class AllNetworksQueryHandler extends NetworkBaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllNetworksQuery;
    }
}
