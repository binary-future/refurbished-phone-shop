<?php

namespace App\Domain\Deals\Contract\Query;

/**
 * Class NetworkBySlugQueryHandler
 * @package App\Domain\Deals\Contract\Query
 */
final class NetworkBySlugQueryHandler extends NetworkBaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof NetworkBySlugQuery;
    }
}
