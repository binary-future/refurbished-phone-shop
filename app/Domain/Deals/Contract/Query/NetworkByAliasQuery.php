<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Contract\Repository\NetworkCriteriaDictionary;

/**
 * Class NetworkByAliasQuery
 * @package App\Domain\Deals\Contract\Query
 */
final class NetworkByAliasQuery implements Query, NetworkCriteriaDictionary
{
    /**
     * @var string
     */
    private $alias;

    /**
     * NetworkByAliasQuery constructor.
     * @param string $alias
     */
    public function __construct(string $alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_ALIAS => $this->getAlias()
        ];
    }
}
