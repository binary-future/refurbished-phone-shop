<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

final class AllNetworksQuery implements Query, CriteriaDictionary
{
    private $relations = [];

    /**
     * AllQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }


    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations
        ];
    }
}
