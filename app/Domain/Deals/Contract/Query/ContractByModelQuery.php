<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Repository\ContractCriteriaDictionary;

class ContractByModelQuery implements Query, ContractCriteriaDictionary
{
    /**
     * @var Contract
     */
    private $contract;

    /**
     * ContractByModelQuery constructor.
     * @param Contract $contract
     */
    public function __construct(Contract $contract)
    {
        $this->contract = $contract;
    }

    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_MODEL => $this->getContract()
        ];
    }

    /**
     * @return Contract
     */
    public function getContract(): Contract
    {
        return $this->contract;
    }
}
