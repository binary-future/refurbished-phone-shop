<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Deals\Contract\Repository\Networks;

/**
 * Class NetworkByAliasQueryHandler
 * @package App\Domain\Deals\Contract\Query
 */
final class NetworkByAliasQueryHandler implements Handler
{
    /**
     * @var Networks
     */
    private $networks;

    /**
     * NetworkByAliasQueryHandler constructor.
     * @param Networks $networks
     */
    public function __construct(Networks $networks)
    {
        $this->networks = $networks;
    }

    /**
     * @param object $object
     * @return \Illuminate\Support\Collection|mixed
     */
    public function handle(object $object)
    {
        /**
         * @var NetworkByAliasQuery $object
         */
        return $this->networks->findBy($object);
    }
}
