<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;
use App\Domain\Common\Contracts\Repository\PluckableQuery;
use App\Domain\Common\Contracts\Repository\Query;

final class AllConditionsQuery implements Query, PluckableQuery, CriteriaDictionary
{
    /**
     * @var string
     */
    private $column;
    /**
     * @var bool
     */
    private $isUnique;

    public function __construct(bool $isUnique = true)
    {
        $this->isUnique = $isUnique;
    }

    public function chooseColumn(string $column)
    {
        $this->column = $column;
    }

    public function chosenColumn(): ?string
    {
        return $this->column;
    }

    public function getCriteria(): array
    {
        return [
            self::UNIQUE => $this->isUnique
        ];
    }
}
