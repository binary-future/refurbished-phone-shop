<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Contract\Repository\NetworkCriteriaDictionary;

final class NetworkBySlugQuery implements Query, NetworkCriteriaDictionary
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var array
     */
    private $relations;

    /**
     * NetworkBySlugQuery constructor.
     * @param string $slug
     * @param array $relations
     */
    public function __construct(string $slug, $relations = [])
    {
        $this->slug = $slug;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_SLUG => $this->slug,
            self::WITH => $this->relations
        ];
    }
}
