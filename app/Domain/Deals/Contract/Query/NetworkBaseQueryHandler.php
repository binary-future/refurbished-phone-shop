<?php

namespace App\Domain\Deals\Contract\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Deals\Contract\Repository\Networks;

abstract class NetworkBaseQueryHandler extends Handler
{
    /**
     * @var Networks
     */
    private $networks;

    /**
     * NetworkBaseQueryHandler constructor.
     * @param Networks $networks
     */
    public function __construct(Networks $networks)
    {
        $this->networks = $networks;
    }

    protected function proceed($object)
    {
        /**
         * @var Query $object
         */
        return $this->networks->findBy($object);
    }
}
