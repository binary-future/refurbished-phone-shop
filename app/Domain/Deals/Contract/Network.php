<?php

namespace App\Domain\Deals\Contract;

use App\Domain\Shared\Contracts\HasImage;
use App\Domain\Shared\Image\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

/**
 * Class Network
 * @package App\Domain\Deals\Contract
 */
class Network extends Model implements HasImage
{
    public const TABLE = 'networks';
    public const OWNER_TYPE = 'networks';

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_SLUG = 'slug';
    public const FIELD_ALIAS = 'alias';

    public const RELATION_IMAGE = 'image';
    public const RELATION_CONTRACTS = 'contracts';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME, self::FIELD_SLUG, self::FIELD_ALIAS
    ];

    /*------------------------------------------------------------------------------------------------------------------
     *
     * ATTRIBUTES
     *__________________________________________________________________________________________________________________
     */
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->getAttribute(self::FIELD_SLUG);
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->getAttribute(self::FIELD_ALIAS);
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name)
    {
        return $this->setAttribute(self::FIELD_NAME, strtolower($name));
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function setSlug(string $slug)
    {
        return $this->setAttribute(self::FIELD_SLUG, strtolower($slug));
    }

    /**
     * @param string $alias
     * @return mixed
     */
    public function setAlias(string $alias)
    {
        return $this->setAttribute(self::FIELD_ALIAS, strtolower($alias));
    }

    /*------------------------------------------------------------------------------------------------------------------
     *
     * RELATIONS
     *__________________________________________________________________________________________________________________
     */

    /**
     * @return HasMany
     */
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    /**
     * @return Collection
     */
    public function getContracts(): Collection
    {
        return $this->getRelationValue(self::RELATION_CONTRACTS);
    }

    /**
     * @return MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, Image::RELATION_IMAGABLE);
    }

    /**
     * @return string
     */
    public function getOwnerName(): string
    {
        return $this->getSlug();
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->getRelationValue(self::RELATION_IMAGE);
    }

    /**
     * @return string
     */
    public function getOwnerType(): string
    {
        return self::OWNER_TYPE;
    }

    /**
     * @return int
     */
    public function getOwnerKey(): int
    {
        return $this->getKey();
    }
}
