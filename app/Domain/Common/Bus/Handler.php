<?php


namespace App\Domain\Common\Bus;

use App\Domain\Common\Contracts\Bus\Handler as Contract;

/**
 * Class Handler
 * @package App\Domain\Common\Bus
 */
abstract class Handler implements Contract
{
    /**
     * @param object $object
     * @return mixed
     * @throws HandleException
     */
    public function handle(object $object)
    {
        if (! $this->isSatisfy($object)) {
            throw HandleException::cannotHandleGivenObject($object);
        }

        return $this->proceed($object);
    }

    /**
     * @param object $object
     * @return mixed
     */
    abstract protected function proceed($object);

    /**
     * @param object $object
     * @return bool
     */
    abstract protected function isSatisfy(object $object): bool;
}
