<?php

namespace App\Domain\Common\Bus;

/**
 * Class HandleException
 * @package App\Domain\Common\Bus
 */
final class HandleException extends \Exception
{
    /**
     * @param object $object
     * @return HandleException
     */
    public static function cannotHandleGivenObject(object $object)
    {
        return new self(sprintf('Cannot handle given object - %s', get_class($object)));
    }
}