<?php

namespace App\Domain\Common\Contracts\Bus;

/**
 * Interface Handler
 * @package App\Domain\Common\Contracts
 */
interface Handler
{
    /**
     * @param object $object
     * @return mixed
     */
    public function handle(object $object);
}
