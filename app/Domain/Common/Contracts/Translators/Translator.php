<?php


namespace App\Domain\Common\Contracts\Translators;


interface Translator
{
    /**
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public function translate(array $data, array $options = []): array;
}