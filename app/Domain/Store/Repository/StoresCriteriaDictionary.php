<?php

namespace App\Domain\Store\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface StoresCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_ALIAS = 'alias';
    public const CRITERIA_ACTIVE = 'is_active';
    public const CRITERIA_SLUG = 'slug';
    public const HAS_LOCAL_IMAGE = 'has-local-image';
}
