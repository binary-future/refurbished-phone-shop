<?php

namespace App\Domain\Store\Affiliation;

/**
 * Interface AffiliationDictionary
 * @package App\Domain\Store\Affiliation
 */
interface AffiliationDictionary
{
    public const SKIMLINK = 'skimlink';
    public const NONE = 'none';
}
