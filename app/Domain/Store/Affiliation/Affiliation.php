<?php

namespace App\Domain\Store\Affiliation;

final class Affiliation
{
    /**
     * @var string
     */
    private $type;

    /**
     * Affiliation constructor.
     * @param string $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function isEqual(Affiliation $affiliation): bool
    {
        return $this->getType() === $affiliation->getType();
    }
}
