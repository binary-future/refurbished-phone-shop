<?php

namespace App\Domain\Store;

use App\Domain\Shared\Contracts\HasDescription;
use App\Domain\Shared\Contracts\HasImage;
use App\Domain\Shared\Contracts\HasLinkWithAffiliation;
use App\Domain\Shared\Contracts\HasRating;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Affiliation\Affiliation;
use App\Domain\Store\Affiliation\AffiliationDictionary;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

class Store extends Model implements HasImage, HasDescription, HasLinkWithAffiliation, HasRating
{
    public const TABLE = 'stores';
    public const WITH_PAYMENT_METHOD_RELATION_PIVOT = 'store_payment_method';
    public const OWNER_TYPE = 'stores';

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_SLUG = 'slug';
    public const FIELD_ALIAS = 'alias';
    public const FIELD_IS_ACTIVE = 'is_active';
    public const FIELD_LAST_PARSING_DATE = 'last_parsing_date';
    public const FIELD_AFFILIATION = 'affiliation';
    public const FIELD_TERMS = 'terms';
    public const FIELD_GUARANTEE = 'guarantee';

    public const RELATION_DESCRIPTION = 'description';
    public const RELATION_IMAGE = 'image';
    public const RELATION_LINK = 'link';
    public const RELATION_PAYMENT_METHODS = 'paymentMethods';
    public const RELATION_RATING = 'rating';

    public const STATUS_ACTIVE = 1;
    public const STATUS_INACTIVE = 0;

    protected $table = self::TABLE;

    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_SLUG,
        self::FIELD_ALIAS,
        self::FIELD_IS_ACTIVE,
        self::FIELD_LAST_PARSING_DATE,
        self::FIELD_AFFILIATION,
        self::FIELD_GUARANTEE,
        self::FIELD_TERMS,
    ];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->getAttribute(self::FIELD_SLUG);
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->getAttribute(self::FIELD_ALIAS);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool) $this->getAttribute(self::FIELD_IS_ACTIVE);
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug)
    {
        $this->setAttribute(self::FIELD_SLUG, strtolower($slug));
    }

    /**
     * @param string $alias
     */
    public function setAlias(string $alias)
    {
        $this->setAttribute(self::FIELD_ALIAS, strtolower($alias));
    }

    /**
     * @param bool $status
     */
    public function setIsActive(bool $status)
    {
        $this->setAttribute(self::FIELD_IS_ACTIVE, $status);
    }

    /**
     * @param int $terms
     */
    public function setTerms(int $terms): void
    {
        $this->setAttribute(self::FIELD_TERMS, $terms);
    }

    /**
     * @return int
     */
    public function getTerms(): int
    {
        return $this->getAttribute(self::FIELD_TERMS);
    }

    /**
     * @param int $guarantee
     */
    public function setGuarantee(int $guarantee): void
    {
        $this->setAttribute(self::FIELD_GUARANTEE, $guarantee);
    }

    public function getGuarantee(): int
    {
        return $this->getAttribute(self::FIELD_GUARANTEE);
    }

    public function getLastParsingDate()
    {
        return $this->getAttribute(self::FIELD_LAST_PARSING_DATE);
    }

    public function setLastParsingDate($date = null)
    {
        $this->setAttribute(self::FIELD_LAST_PARSING_DATE, $date);
    }

    public function getUpdatedAt(): Carbon
    {
        return $this->getAttribute(self::UPDATED_AT);
    }

    public function getAffiliation(): ?Affiliation
    {
        $affiliation = $this->getAttribute(self::FIELD_AFFILIATION);

        return $affiliation ? new Affiliation($affiliation) : null;
    }

    public function setAffiliation(?Affiliation $affiliation)
    {
        $this->setAttribute(
            self::FIELD_AFFILIATION,
            $affiliation ? $affiliation->getType() : AffiliationDictionary::SKIMLINK
        );
    }

    public function getOwnerType(): string
    {
        return static::OWNER_TYPE;
    }

    public function getOwnerKey(): int
    {
        return $this->getKey();
    }

    public function getOwnerName(): string
    {
        return $this->getSlug();
    }

    /*------------------------------------------------------------------------------------------------------------------
     * RELATIONS
     * _________________________________________________________________________________________________________________
     */
    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, Image::RELATION_IMAGABLE);
    }

    public function description(): MorphOne
    {
        return $this->morphOne(Description::class, Description::RELATION_DESCRIBABLE);
    }

    public function link(): MorphOne
    {
        return $this->morphOne(Link::class, Link::RELATION_LINKABLE);
    }

    public function rating(): MorphOne
    {
        return $this->morphOne(Rating::class, Rating::RELATION_RATABLE);
    }

    public function paymentMethods(): BelongsToMany
    {
        return $this->belongsToMany(PaymentMethod::class, self::WITH_PAYMENT_METHOD_RELATION_PIVOT);
    }

    public function getDescription(): ?Description
    {
        return $this->getRelationValue(self::RELATION_DESCRIPTION);
    }

    public function getImage(): ?Image
    {
        return $this->getRelationValue(self::RELATION_IMAGE);
    }

    public function getLink(): ?Link
    {
        return $this->getRelationValue(self::RELATION_LINK);
    }

    public function getPaymentMethods(): Collection
    {
        return $this->getRelationValue(self::RELATION_PAYMENT_METHODS);
    }

    public function getRating(): ?Rating
    {
        return $this->getRelationValue(self::RELATION_RATING);
    }
}
