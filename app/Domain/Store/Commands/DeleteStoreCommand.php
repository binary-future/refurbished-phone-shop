<?php

namespace App\Domain\Store\Commands;

use App\Domain\Store\Store;

final class DeleteStoreCommand
{
    /**
     * @var Store
     */
    private $store;

    /**
     * DeleteStoreCommand constructor.
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }
}
