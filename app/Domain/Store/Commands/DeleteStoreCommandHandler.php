<?php

namespace App\Domain\Store\Commands;

use App\Domain\Common\Bus\Handler;
use App\Domain\Store\Repository\Stores;

/**
 * Class DeleteStoreCommandHandler
 * @package App\Domain\Store\Commands
 */
final class DeleteStoreCommandHandler extends Handler
{
    /**
     * @var Stores
     */
    private $stores;

    /**
     * DeleteStoreCommandHandler constructor.
     * @param Stores $stores
     */
    public function __construct(Stores $stores)
    {
        $this->stores = $stores;
    }

    /**
     * @param DeleteStoreCommand $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        $store = $object->getStore();
        $this->stores->delete($store);

        return $store;
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DeleteStoreCommand;
    }
}
