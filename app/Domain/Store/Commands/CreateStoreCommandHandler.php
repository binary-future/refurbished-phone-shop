<?php

namespace App\Domain\Store\Commands;

use App\Domain\Common\Contracts\Bus\Handler;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;

final class CreateStoreCommandHandler implements Handler
{
    /**
     * @var Stores
     */
    private $stores;

    /**
     * CreateStoreCommandHandler constructor.
     * @param Stores $stores
     */
    public function __construct(Stores $stores)
    {
        $this->stores = $stores;
    }

    /**
     * @param CreateStoreCommand $object
     * @return mixed
     */
    public function handle(object $object)
    {
        return $this->saveStore($object->getStore(), $object->getRelated());
    }

    private function saveStore(Store $store, array $related): Store
    {
        /** @var Store $store */
        $store = $this->stores->save($store, $related);
        return $store;
    }
}
