<?php

namespace App\Domain\Store\Commands;

use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Store;
use Illuminate\Support\Collection;

final class UpdateStoreCommand
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var array
     */
    private $params;

    private $related;

    /**
     * UpdateStoreCommand constructor.
     * @param Store $store
     * @param array $params
     * @param array $related
     */
    public function __construct(Store $store, array $params, array $related = [])
    {
        $this->store = $store;
        $this->params = $params;
        $this->related = $related;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    public function getLink(): ?Link
    {
        return $this->getRelate(Store::RELATION_LINK, Link::class);
    }

    public function getImage(): ?Image
    {
        return $this->getRelate(Store::RELATION_IMAGE, Image::class);
    }

    public function getDescription(): ?Description
    {
        return $this->getRelate(Store::RELATION_DESCRIPTION, Description::class);
    }

    public function getPaymentMethods(): ?Collection
    {
        return $this->getRelate(Store::RELATION_PAYMENT_METHODS, Collection::class);
    }

    public function getRating(): ?Rating
    {
        return $this->getRelate(Store::RELATION_RATING, Rating::class);
    }

    private function getRelate(string $key, string $type)
    {
        $relate = $this->related[$key] ?? null;

        return $relate instanceof $type ? $relate : null;
    }
}
