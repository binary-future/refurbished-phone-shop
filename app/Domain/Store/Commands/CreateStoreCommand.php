<?php

namespace App\Domain\Store\Commands;

use App\Domain\Store\Store;

final class CreateStoreCommand
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var array
     */
    private $related;

    /**
     * CreateStoreCommand constructor.
     * @param Store $store
     * @param array $related
     */
    public function __construct(Store $store, array $related = [])
    {
        $this->store = $store;
        $this->related = $related;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
