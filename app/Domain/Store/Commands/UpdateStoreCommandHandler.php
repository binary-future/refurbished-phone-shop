<?php

namespace App\Domain\Store\Commands;

use App\Domain\Common\Bus\Handler;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Shared\Description\Services\DescriptionManager;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;

final class UpdateStoreCommandHandler extends Handler
{
    /**
     * @var Stores
     */
    private $stores;

    /**
     * @var DescriptionManager
     */
    private $descriptionManager;

    /**
     * UpdateStoreCommandHandler constructor.
     * @param Stores $stores
     * @param DescriptionManager $descriptionManager
     */
    public function __construct(Stores $stores, DescriptionManager $descriptionManager)
    {
        $this->stores = $stores;
        $this->descriptionManager = $descriptionManager;
    }

    /**
     * @param UpdateStoreCommand $object
     * @return mixed
     */
    protected function proceed($object)
    {
        $store = $object->getStore();
        $store = $this->manageRelations($store, $object);
        $store->fill($object->getParams());
        $this->stores->push($store);

        return $store;
    }

    private function manageRelations(Store $store, UpdateStoreCommand $command): Store
    {
        if ($command->getLink()) {
            $store->setRelation(Store::RELATION_LINK, $this->updateLink($store, $command->getLink()));
        }
        if ($command->getImage()) {
            $store->setRelation(Store::RELATION_IMAGE, $command->getImage());
        }
        if ($command->getDescription()) {
            $store->setRelation(
                Store::RELATION_DESCRIPTION,
                $this->descriptionManager->updateDescription($store, $command->getDescription())
            );
        }
        if ($command->getRating()) {
            $store->setRelation(
                Store::RELATION_RATING,
                $command->getRating()
            );
        }
        if ($command->getPaymentMethods()) {
            $store->setRelation(
                Store::RELATION_PAYMENT_METHODS,
                $command->getPaymentMethods()
            );
        }

        return $store;
    }

    /**
     * @param Store $store
     * @param Link $link
     * @return Link|null
     */
    private function updateLink(Store $store, Link $link)
    {
        $previousLink = $store->getLink();
        if (! $previousLink) {
            return $link;
        }

        if ($previousLink->equal($link)) {
            return $previousLink;
        }

        $previousLink->setLink($link->getLink());

        return $previousLink;
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UpdateStoreCommand;
    }
}
