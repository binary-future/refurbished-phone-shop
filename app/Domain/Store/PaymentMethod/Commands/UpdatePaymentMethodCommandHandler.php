<?php

namespace App\Domain\Store\PaymentMethod\Commands;

use App\Domain\Common\Bus\Handler;
use App\Domain\Shared\Description\Services\DescriptionManager;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;

final class UpdatePaymentMethodCommandHandler extends Handler
{
    /**
     * @var PaymentMethods
     */
    private $paymentMethods;

    /**
     * UpdateStoreCommandHandler constructor.
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(PaymentMethods $paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @param UpdatePaymentMethodCommand $object
     * @return PaymentMethod
     */
    protected function proceed($object): PaymentMethod
    {
        $paymentMethod = $object->getPaymentMethod();
        $paymentMethod->fill($object->getParams());
        $paymentMethod = $this->manageRelations($paymentMethod, $object);
        $this->paymentMethods->push($paymentMethod);

        return $paymentMethod;
    }

    private function manageRelations(PaymentMethod $paymentMethod, UpdatePaymentMethodCommand $object): PaymentMethod
    {
        if ($object->getImage()) {
            $paymentMethod->setRelation(PaymentMethod::RELATION_IMAGE, $object->getImage());
        }

        return $paymentMethod;
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UpdatePaymentMethodCommand;
    }
}
