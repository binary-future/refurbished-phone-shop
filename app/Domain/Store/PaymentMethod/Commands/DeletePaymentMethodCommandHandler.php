<?php

namespace App\Domain\Store\PaymentMethod\Commands;

use App\Domain\Common\Bus\Handler;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Repository\Stores;

final class DeletePaymentMethodCommandHandler extends Handler
{
    /**
     * @var PaymentMethods
     */
    private $paymentMethods;

    /**
     * DeleteStoreCommandHandler constructor.
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(PaymentMethods $paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @param DeletePaymentMethodCommand $object
     * @return PaymentMethod
     */
    protected function proceed($object): PaymentMethod
    {
        $paymentMethod = $object->getPaymentMethod();
        $this->paymentMethods->delete($paymentMethod);

        return $paymentMethod;
    }

    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DeletePaymentMethodCommand;
    }
}
