<?php

namespace App\Domain\Store\PaymentMethod\Commands;

use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\Store;

final class UpdatePaymentMethodCommand
{
    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    /**
     * @var array
     */
    private $params;

    /**
     * @var array
     */
    private $related;

    /**
     * @param PaymentMethod $paymentMethod
     * @param array $params
     * @param array $related
     */
    public function __construct(PaymentMethod $paymentMethod, array $params, array $related = [])
    {
        $this->paymentMethod = $paymentMethod;
        $this->params = $params;
        $this->related = $related;
    }

    /**
     * @return PaymentMethod
     */
    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->getRelate(PaymentMethod::RELATION_IMAGE, Image::class);
    }

    private function getRelate(string $key, string $type)
    {
        $relate = $this->related[$key] ?? null;

        return $relate instanceof $type ? $relate : null;
    }
}
