<?php

namespace App\Domain\Store\PaymentMethod\Commands;

use App\Domain\Store\PaymentMethod\PaymentMethod;

final class DeletePaymentMethodCommand
{
    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    public function __construct(PaymentMethod $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }
}
