<?php

namespace App\Domain\Store\PaymentMethod\Commands;

use App\Domain\Store\PaymentMethod\PaymentMethod;

final class CreatePaymentMethodCommand
{
    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    /**
     * @var array
     */
    private $related;

    /**
     * CreatePaymentMethodCommand constructor.
     * @param PaymentMethod $paymentMethod
     * @param array $related
     */
    public function __construct(PaymentMethod $paymentMethod, array $related = [])
    {
        $this->paymentMethod = $paymentMethod;
        $this->related = $related;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function getRelated(): array
    {
        return $this->related;
    }
}
