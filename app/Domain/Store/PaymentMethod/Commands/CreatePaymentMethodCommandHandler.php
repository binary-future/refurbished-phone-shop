<?php

namespace App\Domain\Store\PaymentMethod\Commands;

use App\Domain\Common\Bus\Handler;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;

final class CreatePaymentMethodCommandHandler extends Handler
{
    /**
     * @var PaymentMethods
     */
    private $paymentMethods;

    /**
     * CreateStoreCommandHandler constructor.
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(PaymentMethods $paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @param CreatePaymentMethodCommand $object
     * @return PaymentMethod
     */
    protected function proceed($object): PaymentMethod
    {
        return $this->savePaymentMethod($object->getPaymentMethod(), $object->getRelated());
    }

    private function savePaymentMethod(PaymentMethod $paymentMethod, array $related): PaymentMethod
    {
        /** @var PaymentMethod $paymentMethod */
        $paymentMethod = $this->paymentMethods->save($paymentMethod, $related);
        return $paymentMethod;
    }

    protected function isSatisfy(object $object): bool
    {
        return $object instanceof CreatePaymentMethodCommand;
    }
}
