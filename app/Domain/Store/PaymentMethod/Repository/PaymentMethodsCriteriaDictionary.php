<?php

namespace App\Domain\Store\PaymentMethod\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface PaymentMethodsCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_SLUG = 'slug';
}
