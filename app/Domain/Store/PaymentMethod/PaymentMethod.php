<?php

namespace App\Domain\Store\PaymentMethod;

use App\Domain\Shared\Contracts\HasImage;
use App\Domain\Shared\Image\Image;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Collection;

class PaymentMethod extends Model implements HasImage
{
    public const TABLE = 'payment_methods';
    public const OWNER_TYPE = 'payment_methods';

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_SLUG = 'slug';


    public const RELATION_IMAGE = 'image';
    public const RELATION_STORES = 'stores';

    protected $table = self::TABLE;

    public $timestamps = false;

    protected $fillable = [
        self::FIELD_NAME, self::FIELD_SLUG
    ];

    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    public function setName(string $name): void
    {
        $this->setAttribute(self::FIELD_NAME, $name);
    }

    public function getSlug(): string
    {
        return $this->getAttribute(self::FIELD_SLUG);
    }

    public function setSlug(string $slug): void
    {
        $this->setAttribute(self::FIELD_SLUG, $slug);
    }

    public function getOwnerType(): string
    {
        return static::OWNER_TYPE;
    }

    public function getOwnerKey(): int
    {
        return $this->getKey();
    }

    public function getOwnerName(): string
    {
        return $this->getSlug();
    }

    /*------------------------------------------------------------------------------------------------------------------
     * RELATIONS
     * _________________________________________________________________________________________________________________
     */
    public function image(): MorphOne
    {
        return $this->morphOne(Image::class, Image::RELATION_IMAGABLE);
    }

    public function getImage(): ?Image
    {
        return $this->getRelationValue(self::RELATION_IMAGE);
    }

    public function stores(): BelongsToMany
    {
        return $this->belongsToMany(Store::class);
    }

    public function getStores(): Collection
    {
        return $this->getRelationValue(self::RELATION_STORES);
    }
}
