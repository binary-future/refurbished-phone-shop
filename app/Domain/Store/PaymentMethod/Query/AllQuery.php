<?php

namespace App\Domain\Store\PaymentMethod\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethodsCriteriaDictionary;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

final class AllQuery implements Query, PaymentMethodsCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations;

    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }


    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations
        ];
    }
}
