<?php

namespace App\Domain\Store\PaymentMethod\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethodsCriteriaDictionary;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

final class ByIdsQuery implements Query, PaymentMethodsCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations;
    /**
     * @var int[]
     */
    private $ids;

    /**
     * ByIdsQuery constructor.
     * @param int[] $ids
     * @param string[] $relations
     */
    public function __construct(array $ids, array $relations = [])
    {
        $this->relations = $relations;
        $this->ids = array_map(static function ($item) {
            if (! is_numeric($item)) {
                throw new \InvalidArgumentException('$ids must be array integer ids');
            }
            return (int) $item;
        }, $ids);
    }


    public function getCriteria(): array
    {
        return [
            self::IDS => $this->ids,
            self::WITH => $this->relations,
        ];
    }
}
