<?php

namespace App\Domain\Store\PaymentMethod\Query;

final class ByIdsQueryHandler extends BaseQueryHandler
{
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByIdsQuery;
    }
}
