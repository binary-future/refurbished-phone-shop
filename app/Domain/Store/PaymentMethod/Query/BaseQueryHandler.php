<?php

namespace App\Domain\Store\PaymentMethod\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Repository\Stores;

abstract class BaseQueryHandler extends Handler
{
    /**
     * @var PaymentMethods
     */
    private $paymentMethods;

    /**
     * ActiveQueryHandler constructor.
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(PaymentMethods $paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    protected function proceed($object)
    {
        /**
         * @var Query $object
         */
        return $this->paymentMethods->findBy($object);
    }
}
