<?php


namespace App\Domain\Store\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

/**
 * Class HasLocalImage
 * @package App\Domain\Store\Query
 */
final class HasLocalImageQuery implements Query, StoresCriteriaDictionary
{
    /**
     * @var array
     */
    private $related = [];

    /**
     * PhonesWithNotLocalImagesQuery constructor.
     * @param array $related
     */
    public function __construct(array $related = [])
    {
        $this->related = $related;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => array_unique(array_filter(array_merge(['image'], $this->related))),
            self::HAS_LOCAL_IMAGE => false
        ];
    }
}
