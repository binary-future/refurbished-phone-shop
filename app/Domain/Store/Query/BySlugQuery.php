<?php

namespace App\Domain\Store\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

final class BySlugQuery implements Query, StoresCriteriaDictionary
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var array
     */
    private $relations;

    /**
     * BySlugQuery constructor.
     * @param string $slug
     * @param array $relations
     */
    public function __construct(string $slug, $relations = [])
    {
        $this->slug = $slug;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_SLUG => $this->slug,
            self::WITH => $this->relations
        ];
    }
}
