<?php

namespace App\Domain\Store\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

final class ActiveQuery implements StoresCriteriaDictionary, Query
{
    /**
     * @var array
     */
    private $relations;

    /**
     * @var int
     */
    private $limit;

    /**
     * ActiveQuery constructor.
     * @param array $relations
     * @param int|null $limit
     */
    public function __construct($relations = [], int $limit = null)
    {
        $this->relations = $relations;
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_ACTIVE => true,
            self::WITH => $this->relations,
            self::LIMIT => (int) $this->limit
        ];
    }
}
