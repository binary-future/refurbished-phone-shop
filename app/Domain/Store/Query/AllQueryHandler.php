<?php

namespace App\Domain\Store\Query;

/**
 * Class AllQueryHandler
 * @package App\Domain\Store\Query
 */
final class AllQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllQuery;
    }
}
