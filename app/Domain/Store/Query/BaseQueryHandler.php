<?php

namespace App\Domain\Store\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\Repository\Stores;

abstract class BaseQueryHandler extends Handler
{
    /**
     * @var Stores
     */
    private $stores;

    /**
     * ActiveQueryHandler constructor.
     * @param Stores $stores
     */
    public function __construct(Stores $stores)
    {
        $this->stores = $stores;
    }

    protected function proceed($object)
    {
        /**
         * @var Query $object
         */
        return $this->stores->findBy($object);
    }
}
