<?php


namespace App\Domain\Store\Query;


use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

final class AllQuery implements Query, StoresCriteriaDictionary
{
    private $relations = [];

    /**
     * AllQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }


    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations
        ];
    }
}
