<?php

namespace App\Domain\Store\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\Store\Repository\StoresCriteriaDictionary;

/**
 * Class ByExternalIdQuery
 * @package App\Domain\Store\Query
 */
final class ByAliasQuery implements Query, StoresCriteriaDictionary
{
    /**
     * @var string
     */
    private $alias;

    /**
     * ByAliasQuery constructor.
     * @param string $alias
     */
    public function __construct(string $alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_ALIAS => $this->getAlias()
        ];
    }
}
