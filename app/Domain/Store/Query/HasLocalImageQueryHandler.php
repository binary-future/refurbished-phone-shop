<?php


namespace App\Domain\Store\Query;


/**
 * Class HasLocalImageQueryHandler
 * @package App\Domain\Store\Query
 */
final class HasLocalImageQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof HasLocalImageQuery;
    }
}
