<?php

namespace App\Domain\Store\Query;

/**
 * Class ActiveQueryHandler
 * @package App\Domain\Store\Query
 */
final class ActiveQueryHandler extends BaseQueryHandler
{
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ActiveQuery;
    }
}
