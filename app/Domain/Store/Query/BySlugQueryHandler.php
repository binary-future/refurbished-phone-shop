<?php

namespace App\Domain\Store\Query;

/**
 * Class BySlugQueryHandler
 * @package App\Domain\Store\Query
 */
final class BySlugQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof BySlugQuery;
    }
}
