<?php

namespace App\Domain\Store\Query;

/**
 * Class ByExternalIdQueryHandler
 * @package App\Domain\Store\Query
 */
final class ByAliasQueryHandler extends BaseQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof ByAliasQuery;
    }

}
