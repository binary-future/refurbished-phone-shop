<?php


namespace App\Domain\Store\Query;


use App\Domain\Common\Bus\Handler;
use App\Domain\Store\Affiliation\Affiliation;
use App\Utils\Adapters\Config\Contracts\Config;

/**
 * Class GetAffiliationListQueryHandler
 * @package App\Domain\Store\Query
 */
final class GetAffiliationListQueryHandler extends Handler
{
    private const CONFIG_PATH = 'affiliation.networks.networks';

    /**
     * @var Config
     */
    private $config;

    /**
     * GetAffiliationListQueryHandler constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @param object $object
     * @return mixed|void
     */
    protected function proceed($object)
    {
        $affiliations = $this->getAffiliationList();
        $result = collect();
        foreach ($affiliations as $affiliation) {
            $result->push(new Affiliation($affiliation));
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getAffiliationList(): array
    {
        return array_keys($this->config->get(self::CONFIG_PATH, []));
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof GetAffiliationListQuery;
    }
}
