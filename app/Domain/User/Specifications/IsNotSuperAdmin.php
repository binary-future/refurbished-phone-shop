<?php

namespace App\Domain\User\Specifications;

use App\Domain\User\Role;
use App\Domain\User\User;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class IsNotSuperAdmin
 * @package App\Domain\User\Specifications
 */
final class IsNotSuperAdmin implements Specification
{
    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        return $value instanceof User && $this->checkRole($value);
    }

    /**
     * @param User $user
     * @return bool
     */
    private function checkRole(User $user): bool
    {
        $role = $user->getRole();

        return ! $role || $role->getTitle() !== Role::SUPER_ADMIN;
    }
}
