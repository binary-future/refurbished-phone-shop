<?php

namespace App\Domain\User\Specifications;

use App\Domain\User\User;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class IsNotHimself
 * @package App\Domain\User\Specifications
 */
final class IsNotHimself implements Specification
{
    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        $authUser = $this->getAuthUser($params);

        return $value instanceof User && $authUser && $value->getKey() !== $authUser->getKey();
    }

    /**
     * @param $params
     * @return User|null
     */
    private function getAuthUser($params): ?User
    {
        $authUser = $params['auth'] ?? null;

        return $authUser instanceof User ? $authUser : null;
    }
}
