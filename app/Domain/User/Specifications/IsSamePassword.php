<?php

namespace App\Domain\User\Specifications;

use App\Utils\Adapters\Encryptor\Contract\Encryptor;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class IsSamePassword
 * @package App\Domain\User\Specifications
 */
final class IsSamePassword implements Specification
{
    /**
     * @var Encryptor
     */
    private $encryptor;

    /**
     * IsSamePassword constructor.
     * @param Encryptor $encryptor
     */
    public function __construct(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;
    }

    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        return is_string($value)
            && isset($params['hash'])
            && is_string($params['hash'])
            && $this->encryptor->checkHash($value, $params['hash']);
    }
}
