<?php

namespace App\Domain\User\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface UsersCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_EXCEPT_ROLE = 'except-role';
    public const CRITERIA_EXCEPT_USER = 'except-user';
}