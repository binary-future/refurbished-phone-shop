<?php

namespace App\Domain\User\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface Users
 * @package App\Domain\User\Repository
 */
interface Users extends Repository
{

}