<?php

namespace App\Domain\User\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface RolesCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_EXCEPT_TITLE = 'except-title';
}