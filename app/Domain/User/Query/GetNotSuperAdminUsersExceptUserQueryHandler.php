<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\User\Repository\Users;

/**
 * Class GetNotSuperAdminUsersExceptUserQueryHandler
 * @package App\Domain\User\Query
 */
final class GetNotSuperAdminUsersExceptUserQueryHandler extends Handler
{
    /**
     * @var Users
     */
    private $users;

    /**
     * GetNotSuperAdminUsersExceptUserQueryHandler constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * @param GetNotSuperAdminUsersExceptUserQuery $object
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function proceed($object)
    {
        return $this->users->findBy($object);
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof GetNotSuperAdminUsersExceptUserQuery;
    }
}
