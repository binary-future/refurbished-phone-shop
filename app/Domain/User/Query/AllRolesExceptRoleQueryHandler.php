<?php

namespace App\Domain\User\Query;

/**
 * Class AllRolesExceptRoleQueryHandler
 * @package App\Domain\User\Query
 */
final class AllRolesExceptRoleQueryHandler extends BaseRoleQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllRolesExceptRoleQuery;
    }
}
