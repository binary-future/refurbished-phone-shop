<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\User\Repository\RolesCriteriaDictionary;

/**
 * Class AllRolesQuery
 * @package App\Domain\User\Query
 */
final class AllRolesQuery implements Query, RolesCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations = [];

    /**
     * AllRolesQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => [],
            self::WITH => $this->relations,
        ];
    }
}
