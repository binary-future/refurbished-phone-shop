<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\User\Repository\Roles;
use Illuminate\Support\Collection;

abstract class BaseRoleQueryHandler extends Handler
{
    /**
     * @var Roles
     */
    private $roles;

    /**
     * AllRolesQueryHandler constructor.
     * @param Roles $roles
     */
    public function __construct(Roles $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @param AllRolesQuery $object
     * @return mixed
     */
    protected function proceed($object)
    {
        /**
         * @var Collection $roles
         */
        $roles = $this->roles->findBy($object);

        return $roles;
    }
}
