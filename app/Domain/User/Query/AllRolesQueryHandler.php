<?php

namespace App\Domain\User\Query;

final class AllRolesQueryHandler extends BaseRoleQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllRolesQuery;
    }
}
