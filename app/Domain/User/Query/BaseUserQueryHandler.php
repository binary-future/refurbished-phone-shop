<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\User\Repository\Users;
use Illuminate\Support\Collection;

abstract class BaseUserQueryHandler extends Handler
{
    /**
     * @var Users
     */
    private $users;

    /**
     * BaseUserQueryHandler constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * @param AllRolesQuery $object
     * @return mixed
     */
    protected function proceed($object)
    {
        /**
         * @var Collection $users
         */
        $users = $this->users->findBy($object);

        return $users;
    }
}
