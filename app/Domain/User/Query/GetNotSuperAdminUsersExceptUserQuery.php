<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\User\Repository\UsersCriteriaDictionary;
use App\Domain\User\Role;
use App\Domain\User\User;

/**
 * Class GetNotSuperAdminUsersExceptUserQuery
 * @package App\Domain\User\Query
 */
final class GetNotSuperAdminUsersExceptUserQuery implements Query, UsersCriteriaDictionary
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * GetNotSuperAdminUsersExceptUserQuery constructor.
     * @param User $user
     * @param array $relations
     */
    public function __construct(User $user, $relations = [])
    {
        $this->user = $user;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_EXCEPT_USER => $this->user,
            self::CRITERIA_EXCEPT_ROLE => Role::SUPER_ADMIN,
            self::WITH => $this->relations,
        ];
    }
}
