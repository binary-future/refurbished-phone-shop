<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\User\Repository\RolesCriteriaDictionary;

final class AllRolesExceptRoleQuery implements Query, RolesCriteriaDictionary
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var array
     */
    private $related = [];

    /**
     * AllRolesExceptRoleQuery constructor.
     * @param string $title
     * @param array $related
     */
    public function __construct(string $title, array $related = [])
    {
        $this->title = $title;
        $this->related = $related;
    }

    public function getCriteria(): array
    {
        return [
            self::WITH => $this->related,
            self::CRITERIA_EXCEPT_TITLE => $this->title,
        ];
    }


}