<?php

namespace App\Domain\User\Query;

/**
 * Class GetUserByIdQueryHandler
 * @package App\Domain\User\Query
 */
final class GetUserByIdQueryHandler extends BaseUserQueryHandler
{
    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof GetUserByIdQuery;
    }
}
