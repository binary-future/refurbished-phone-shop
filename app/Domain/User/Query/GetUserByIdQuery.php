<?php

namespace App\Domain\User\Query;

use App\Domain\Common\Contracts\Repository\Query;
use App\Domain\User\Repository\UsersCriteriaDictionary;

/**
 * Class GetUserByIdQuery
 * @package App\Domain\User\Query
 */
final class GetUserByIdQuery implements Query, UsersCriteriaDictionary
{
    /**
     * @var int
     */
    private $userId;

    /**
     * @var array
     */
    private $related = [];

    /**
     * GetUserByIdQuery constructor.
     * @param int $userId
     * @param array $related
     */
    public function __construct(int $userId, array $related = [])
    {
        $this->userId = $userId;
        $this->related = $related;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->related,
            self::ID => $this->userId,
        ];
    }
}
