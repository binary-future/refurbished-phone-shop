<?php

namespace App\Domain\User;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public const TABLE = 'users';

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';
    public const FIELD_EMAIL = 'email';
    public const FIELD_PASSWORD = 'password';
    public const FIELD_REMEMBER_TOKEN = 'remember_token';
    public const FIELD_EMAIL_VERIFIED_AT = 'email_verified_at';
    public const FIELD_ROLE_ID = 'role_id';
    public const RELATION_ROLE = 'role';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
        self::FIELD_ROLE_ID,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::FIELD_PASSWORD, self::FIELD_REMEMBER_TOKEN,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_EMAIL_VERIFIED_AT => 'datetime',
    ];

    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    public function setName(string $name)
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    public function getEmail(): string
    {
        return $this->getAttribute(self::FIELD_EMAIL);
    }

    public function setEmail(string $email)
    {
        return $this->setAttribute(self::FIELD_EMAIL, $email);
    }

    public function getRoleId(): int
    {
        return $this->getAttribute(self::FIELD_ROLE_ID);
    }

    public function setRoleId(int $roleId)
    {
        return $this->setAttribute(self::FIELD_ROLE_ID, $roleId);
    }

    public function getRole(): ?Role
    {
        return $this->getRelationValue(self::RELATION_ROLE);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class, self::FIELD_ROLE_ID);
    }

    public function isAdmin(): bool
    {
        $role = $this->getRole();

        return $role && ($this->isSuperAdmin() || $role->getTitle() === Role::ADMIN);
    }

    public function isSuperAdmin(): bool
    {
        $role = $this->getRole();

        return $role && $role->getTitle() === Role::SUPER_ADMIN;
    }

    public function isEditor(): bool
    {
        $role = $this->getRole();

        return $role && $role->getTitle() === Role::EDITOR;
    }

    public function setPassword(string $password): void
    {
        $this->setAttribute(self::FIELD_PASSWORD, $password);
    }
}
