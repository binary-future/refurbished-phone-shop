<?php

namespace App\Domain\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Role extends Model
{
    public const TABLE = 'roles';

    public const SUPER_ADMIN = 'superAdmin';
    public const ADMIN = 'admin';
    public const EDITOR = 'editor';

    public const ROLES = [
        self::SUPER_ADMIN, self::ADMIN, self::EDITOR
    ];

    public const FIELD_ID = 'id';
    public const FIELD_TITLE = 'title';

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_TITLE
    ];

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->getAttribute(self::FIELD_TITLE);
    }

    /**
     * @param string $title
     * @return mixed
     */
    public function setTitle(string $title)
    {
        return $this->setAttribute(self::FIELD_TITLE, $title);
    }

    /**
     * @return HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
