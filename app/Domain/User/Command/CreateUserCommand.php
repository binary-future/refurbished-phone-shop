<?php

namespace App\Domain\User\Command;

use App\Domain\User\User;

/**
 * Class CreateUserCommand
 * @package App\Domain\User\Command
 */
final class CreateUserCommand
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $related = [];

    /**
     * CreateUserCommand constructor.
     * @param User $user
     * @param array $related
     */
    public function __construct(User $user, array $related = [])
    {
        $this->user = $user;
        $this->related = $related;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getRelated(): array
    {
        return $this->related;
    }
}
