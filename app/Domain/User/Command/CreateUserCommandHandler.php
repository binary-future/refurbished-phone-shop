<?php

namespace App\Domain\User\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\User\Repository\Users;

/**
 * Class CreateUserCommandHandler
 * @package App\Domain\User\Command
 */
final class CreateUserCommandHandler extends Handler
{
    /**
     * @var Users
     */
    private $users;

    /**
     * CreateUserCommandHandler constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * @param CreateUserCommand $object
     * @return mixed
     */
    protected function proceed($object)
    {
        return $this->users->save($object->getUser(), $object->getRelated());
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof CreateUserCommand;
    }
}
