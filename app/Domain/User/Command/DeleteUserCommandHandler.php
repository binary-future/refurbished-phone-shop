<?php

namespace App\Domain\User\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\User\Repository\Users;

/**
 * Class DeleteUserCommandHandler
 * @package App\Domain\User\Command
 */
final class DeleteUserCommandHandler extends Handler
{
    /**
     * @var Users
     */
    private $users;

    /**
     * DeleteUserCommandHandler constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * @param DeleteUserCommand $object
     * @return mixed
     */
    protected function proceed($object)
    {
        $this->users->delete($object->getUser());

        return $object->getUser();
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof DeleteUserCommand;
    }
}
