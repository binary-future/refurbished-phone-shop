<?php

namespace App\Domain\User\Command;

use App\Domain\Common\Bus\Handler;
use App\Domain\User\Repository\Users;

final class UpdateUserCommandHandler extends Handler
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UpdateUserCommandHandler constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * @param UpdateUserCommand $object
     * @return mixed
     */
    protected function proceed($object)
    {
        return $this->users->update($object->getUser(), $object->getParams());
    }

    protected function isSatisfy(object $object): bool
    {
        return $object instanceof UpdateUserCommand;
    }

}