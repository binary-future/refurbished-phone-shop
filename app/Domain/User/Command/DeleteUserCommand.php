<?php

namespace App\Domain\User\Command;

use App\Domain\User\User;

/**
 * Class DeleteUserCommand
 * @package App\Domain\User\Command
 */
final class DeleteUserCommand
{
    /**
     * @var User
     */
    private $user;

    /**
     * DeleteUserCommand constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
