<?php

namespace App\Domain\User\Command;

use App\Domain\User\User;

/**
 * Class UpdateUserCommand
 * @package App\Domain\User\Command
 */
final class UpdateUserCommand
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $params = [];

    /**
     * UpdateUserCommand constructor.
     * @param User $user
     * @param array $params
     */
    public function __construct(User $user, array $params)
    {
        $this->user = $user;
        $this->params = $params;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
