<?php

namespace App\Domain\Shared\Description;

use App\Domain\Shared\Contracts\HasDescription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Description extends Model
{
    public const TABLE = 'descriptions';

    public const FIELD_ID = 'id';
    public const FIELD_CONTENT = 'content';
    public const FIELD_OWNER_TYPE = self::RELATION_DESCRIBABLE . '_type';
    public const FIELD_OWNER_ID = self::RELATION_DESCRIBABLE . '_id';

    public const RELATION_DESCRIBABLE = 'describable';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_CONTENT, self::FIELD_OWNER_TYPE, self::FIELD_OWNER_ID
    ];

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->getAttribute(self::FIELD_CONTENT);
    }

    /**
     * @return int
     */
    public function getDescribableId(): int
    {
        return $this->getAttribute(self::FIELD_OWNER_ID);
    }

    /**
     * @return string
     */
    public function getDescribableType(): string
    {
        return $this->getAttribute(self::FIELD_OWNER_TYPE);
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->setAttribute(self::FIELD_CONTENT, $content);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function setDescribableId(int $id)
    {
        return $this->setAttribute(self::FIELD_OWNER_ID, $id);
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function setDescribableType(string $type)
    {
        return $this->setAttribute(self::FIELD_OWNER_TYPE, $type);
    }

    public function isValid(): bool
    {
        return (bool) $this->getAttribute(self::FIELD_CONTENT);
    }

    /**
     * @param HasDescription $owner
     */
    public function setOwner(HasDescription $owner): void
    {
        $this->setDescribableId($owner->getOwnerKey());
        $this->setDescribableType($owner->getOwnerType());
    }

    public function getOwner(): HasDescription
    {
        return $this->getRelationValue(self::RELATION_DESCRIBABLE);
    }

    public function describable(): MorphTo
    {
        return $this->morphTo();
    }
}
