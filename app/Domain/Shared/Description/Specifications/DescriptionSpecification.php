<?php

namespace App\Domain\Shared\Description\Specifications;

use App\Domain\Shared\Description\Description;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class DescriptionSpecification
 * @package App\Domain\Shared\Description\Specifications
 */
final class DescriptionSpecification implements Specification
{
    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        return $this->isDescription($value) && $this->hasContent($value);
    }

    /**
     * @param $value
     * @return bool
     */
    private function isDescription($value): bool
    {
        return $value instanceof Description;
    }

    /**
     * @param Description $description
     * @return bool
     */
    private function hasContent(Description $description): bool
    {
        try {
            $content = $description->getContent();

            return is_string($content) && $content !== '';
        } catch (\Throwable $exception) {
            return false;
        }
    }
}
