<?php


namespace App\Domain\Shared\Description\Observer;


use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\Repository\DescriptionImageRepository;
use App\Domain\Shared\Description\Services\Images\ImageSynchronizer;

class DescriptionObserver
{
    /**
     * @var ImageSynchronizer
     */
    private $imageSynchronizer;

    /**
     * @var DescriptionImageRepository
     */
    private $descriptionImageRepository;

    /**
     * DescriptionObserver constructor.
     * @param ImageSynchronizer $imageSynchronizer
     * @param DescriptionImageRepository $descriptionImageRepository
     */
    public function __construct(
        ImageSynchronizer $imageSynchronizer,
        DescriptionImageRepository $descriptionImageRepository
    ) {
        $this->imageSynchronizer = $imageSynchronizer;
        $this->descriptionImageRepository = $descriptionImageRepository;
    }

    public function saved(Description $description)
    {
        $this->imageSynchronizer->synchronize($description);
    }

    /**
     * Listen to the User deleting event.
     * Deletes image file from the storage place
     *
     * @param Description $description
     * @return void
     */
    public function deleting(Description $description)
    {
        $this->descriptionImageRepository->remove($description);
    }
}
