<?php


namespace App\Domain\Shared\Description\Repository;


use App\Domain\Shared\Description\Description;
use Illuminate\Support\Collection;

/**
 * Interface DescriptionImageRepository
 * @package App\Domain\Shared\Description\Repository
 */
interface DescriptionImageRepository
{
    public function all(): Collection;

    /**
     * @param Description $description
     * @param Collection $images
     * @return mixed
     */
    public function sync(Description $description, Collection $images): bool;

    /**
     * @param Description $description
     * @return bool
     */
    public function remove(Description $description): bool;
}
