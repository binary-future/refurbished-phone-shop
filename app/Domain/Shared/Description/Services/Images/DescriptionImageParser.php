<?php

namespace App\Domain\Shared\Description\Services\Images;

use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\DescriptionImage;
use App\Utils\Adapters\Config\Contracts\Config;
use Illuminate\Support\Collection;

class DescriptionImageParser
{
    private const CONFIG_PATH = 'file.upload.image.description-image';

    private $configs = [];

    public function __construct(Config $config)
    {
        $this->configs = $config->get(self::CONFIG_PATH, []);
    }

    public function parseImages(Description $description): Collection
    {
        $images = $this->filterPaths($this->proceed($description->getContent()));

        return $images->transform(function (string $path) use ($description) {
            return new DescriptionImage($path, $description->getKey());
        });
    }

    private function proceed(string $content): Collection
    {
        preg_match_all('/<img[^>]+>/i', $content, $result);
        $result = array_first($result);

        return collect(array_map(function ($tag) {
            $images = [];
            preg_match('/(src)=("[^"]*")/i', $tag, $images);
            return str_ireplace('"', '', array_last($images));
        }, $result));
    }

    private function filterPaths(Collection $paths): Collection
    {
        $localPathPattern = $this->configs['storage_suffix'] ?? '';

        return $paths->filter(function (string $path) use ($localPathPattern) {
            return $path && stripos($path, $localPathPattern) !== false;
        });
    }
}
