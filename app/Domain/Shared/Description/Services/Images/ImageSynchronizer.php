<?php


namespace App\Domain\Shared\Description\Services\Images;


use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\Repository\DescriptionImageRepository;

/**
 * Class ImageSynchronizer
 * @package App\Domain\Shared\Description\Services\Images
 */
class ImageSynchronizer
{
    /**
     * @var DescriptionImageRepository
     */
    private $repository;

    /**
     * @var DescriptionImageParser
     */
    private $imageParser;

    /**
     * ImageSynchronizer constructor.
     * @param DescriptionImageRepository $repository
     * @param DescriptionImageParser $imageParser
     */
    public function __construct(DescriptionImageRepository $repository, DescriptionImageParser $imageParser)
    {
        $this->repository = $repository;
        $this->imageParser = $imageParser;
    }

    /**
     * @param Description $description
     * @return bool
     */
    public function synchronize(Description $description): bool
    {
        $images = $this->imageParser->parseImages($description);

        return $this->repository->sync($description, $images);
    }
}
