<?php

namespace App\Domain\Shared\Description\Services;

use App\Domain\Shared\Contracts\HasDescription;
use App\Domain\Shared\Description\Description;

class DescriptionManager
{
    /**
     * @param HasDescription $owner
     * @param Description $description
     * @return Description|null
     * @throws \Exception
     */
    public function updateDescription(HasDescription $owner, Description $description): ?Description
    {
        if (! $description->isValid()) {
            $this->deletePrevious($owner);
            return null;
        }
        $previousDescription = $owner->getDescription();
        if (! $previousDescription) {
            return $description;
        }

        $previousDescription->fill($description->toArray());

        return $previousDescription;
    }

    /**
     * @param HasDescription $owner
     * @return bool|null
     * @throws \Exception
     */
    private function deletePrevious(HasDescription $owner): ?bool
    {
        $description = $owner->getDescription();

        return $description ? $description->delete() : false;
    }
}
