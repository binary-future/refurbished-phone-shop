<?php


namespace App\Domain\Shared\Description;


/**
 * Class DescriptionImage
 * @package App\Domain\Shared\Description
 */
final class DescriptionImage
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var int
     */
    private $descriptionId;

    /**
     * @var string
     */
    private $originalPath;

    /**
     * DescriptionImage constructor.
     * @param string $path
     * @param int $descriptionId
     */
    public function __construct(string $path, int $descriptionId)
    {
        $this->path = $path;
        $this->originalPath = $path;
        $this->descriptionId = $descriptionId;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return int
     */
    public function getDescriptionId(): int
    {
        return $this->descriptionId;
    }

    /**
     * @param string $path
     */
    public function changePath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getOriginalPath(): string
    {
        return $this->originalPath;
    }
}
