<?php

namespace App\Domain\Shared\Description\Query;

use App\Domain\Common\Bus\Handler;
use App\Domain\Shared\Description\Repository\DescriptionImageRepository;

final class AllImagesQueryHandler extends Handler
{
    /**
     * @var DescriptionImageRepository
     */
    private $repository;

    /**
     * AllImagesQueryHandler constructor.
     * @param DescriptionImageRepository $repository
     */
    public function __construct(DescriptionImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param object $object
     * @return \Illuminate\Support\Collection|mixed
     */
    protected function proceed($object)
    {
        return $this->repository->all();
    }

    /**
     * @param object $object
     * @return bool
     */
    protected function isSatisfy(object $object): bool
    {
        return $object instanceof AllImagesQuery;
    }

}