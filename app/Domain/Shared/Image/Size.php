<?php

namespace App\Domain\Shared\Image;

/**
 * Class Size
 * @package App\Domain\Shared\Image
 */
final class Size
{
    /**
     * @var int
     */
    private $height;

    /**
     * @var int
     */
    private $width;

    /**
     * Size constructor.
     * @param int $height
     * @param int $width
     */
    public function __construct(int $height, int $width)
    {
        $this->height = $height;
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param Size $size
     * @return bool
     */
    public function isEqual(Size $size): bool
    {
        return $this->getWidth() === $size->getWidth()
            && $this->getHeight() === $size->getHeight();
    }
}
