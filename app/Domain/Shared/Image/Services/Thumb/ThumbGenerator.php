<?php

namespace App\Domain\Shared\Image\Services\Thumb;

use App\Domain\Shared\Image\Services\Thumb\Exceptions\ThumbGenerationException;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Image\Size;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\File\Resizer\Contract\ImageResizer;
use App\Utils\File\Resizer\Resize;
use Illuminate\Support\Collection;

class ThumbGenerator
{
    private const CONFIG_PATH = 'file.thumbs.thumbs.sizes';

    /**
     * @var array
     */
    private $configs = [];

    /**
     * @var ImageResizer
     */
    private $resizer;

    /**
     * ThumbGenerator constructor.
     * @param Config $config
     * @param ImageResizer $resizer
     */
    public function __construct(Config $config, ImageResizer $resizer)
    {
        $this->resizer = $resizer;
        $this->configs = $config->get(self::CONFIG_PATH);
    }

    /**
     * @param Image $image
     * @return Collection
     * @throws ThumbGenerationException
     */
    public function generateThumb(Image $image): Collection
    {
        try {
            return $this->proceedGeneration($image);
        } catch (\Throwable $exception) {
            throw ThumbGenerationException::cannotGenerateThumbs($exception->getMessage());
        }
    }

    private function proceedGeneration(Image $image): Collection
    {
        if (! $image->isLocal() || $image->isThumb()) {
            return collect();
        }

        $result = collect();
        foreach ($this->configs as $size) {
            $result->push($this->generateThumbWIthSize($image, $size));
        }

        return $result;
    }

    private function generateThumbWithSize(Image $image, array $size): Image
    {
        $resize = new Resize(
            $this->generatePath($image->getPath()),
            $size['height'],
            $size['width']
        );
        $thumbImage = new Image();
        $thumbImage->setIsLocal(true);
        $thumbImage->setImagableId($image->getImagableId());
        $thumbImage->setImagableType($image->getImagableType());
        $thumbImage->assignParent($image);
        $thumbImage->changeSize(new Size($resize->getHeigth(), $resize->getWidth()));
        $thumbPath = str_ireplace(public_path('/'), '', $this->resizer->resize($resize));
        $thumbImage->setPath($thumbPath);

        return $thumbImage;
    }

    private function generatePath(string $path): string
    {
        return public_path($path);
    }
}
