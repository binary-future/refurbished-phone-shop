<?php


namespace App\Domain\Shared\Image\Services\Thumb\Exceptions;


/**
 * Class ThumbGenerationException
 * @package App\Application\Services\File\Image\Exceptions
 */
class ThumbGenerationException extends \Exception
{
    /**
     * @param string $message
     * @return ThumbGenerationException
     */
    public static function cannotGenerateThumbs(string $message)
    {
        return new self(sprintf(
            'Cannot generate thumbnails. Reason: %s',
            $message
        ));
    }
}
