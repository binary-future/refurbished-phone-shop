<?php

namespace App\Domain\Shared\Image;

use App\Domain\Shared\Contracts\HasImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Collection;
use InvalidArgumentException;

class Image extends Model
{
    public const TABLE = 'images';

    public const FIELD_ID = 'id';
    public const FIELD_PATH = 'path';
    public const FIELD_IS_LOCAL = 'is_local';
    public const FIELD_IS_MAIN = 'is_main';
    public const FIELD_OWNER_TYPE = self::RELATION_IMAGABLE . '_type';
    public const FIELD_OWNER_ID = self::RELATION_IMAGABLE . '_id';
    public const FIELD_PARENT_ID = 'parent_id';
    public const FIELD_HEIGHT = 'height';
    public const FIELD_WIDTH = 'width';

    public const TYPE_PHONES = 'phones';
    public const TYPE_STORES = 'stores';

    public const RELATION_IMAGABLE = 'imagable';
    public const RELATION_PARENT = 'parent';
    public const RELATION_THUMBS = 'thumbs';

    public const ALLOWED_EXTENSIONS = [ 'jpeg', 'png', 'gif', 'jpg', 'svg' ];

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_PATH, self::FIELD_IS_LOCAL, self::FIELD_OWNER_TYPE,
        self::FIELD_OWNER_ID, self::FIELD_IS_MAIN, self::FIELD_PARENT_ID,
        self::FIELD_HEIGHT, self::FIELD_WIDTH
    ];

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->getAttribute(self::FIELD_PATH);
    }

    /**
     * @return bool
     */
    public function isLocal(): bool
    {
        return (bool) $this->getAttribute(self::FIELD_IS_LOCAL);
    }

    /**
     * @return int
     */
    public function getImagableId(): int
    {
        return $this->getAttribute(self::FIELD_OWNER_ID);
    }

    /**
     * @return string
     */
    public function getImagableType(): string
    {
        return $this->getAttribute(self::FIELD_OWNER_TYPE);
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->setAttribute(self::FIELD_PATH, $path);
    }

    /**
     * @param bool $isLocal
     */
    public function setIsLocal(bool $isLocal)
    {
        $this->setAttribute(self::FIELD_IS_LOCAL, $isLocal);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function setImagableId(int $id)
    {
        return $this->setAttribute(self::FIELD_OWNER_ID, $id);
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function setImagableType(string $type)
    {
        return $this->setAttribute(self::FIELD_OWNER_TYPE, $type);
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        return (bool) $this->getAttribute(self::FIELD_IS_MAIN);
    }

    public function getName(): string
    {
        return basename($this->getPath());
    }

    /**
     * @param bool $main
     */
    public function setMain(bool $main)
    {
        $this->setAttribute(self::FIELD_IS_MAIN, $main);
    }

    /**
     * @param HasImage $owner
     */
    public function setOwner(HasImage $owner)
    {
        $this->setImagableId($owner->getOwnerKey());
        $this->setImagableType($owner->getOwnerType());
    }

    public function getSize(): ?Size
    {
        $height = $this->{self::FIELD_HEIGHT};
        $width = $this->{self::FIELD_WIDTH};

        return is_int($height) && is_int($width)
            ? new Size($height, $width)
            : null;
    }

    public function changeSize(Size $size)
    {
        $this->setAttribute(self::FIELD_WIDTH, $size->getWidth());
        $this->setAttribute(self::FIELD_HEIGHT, $size->getHeight());
    }

    public function isThumb(): bool
    {
        return (bool) $this->getAttribute(self::FIELD_PARENT_ID);
    }

    public function getParent(): ?Image
    {
        return $this->getRelationValue(self::RELATION_PARENT);
    }

    public function getThumbs(): Collection
    {
        return $this->getRelationValue(self::RELATION_THUMBS);
    }

    public function assignParent(Image $image): void
    {
        if ($image->isThumb()) {
            throw new InvalidArgumentException('Parent image cannot be thumb');
        }
        $this->setAttribute(self::FIELD_PARENT_ID, $image->getKey());
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, self::FIELD_PARENT_ID);
    }

    public function thumbs(): HasMany
    {
        return $this->hasMany(self::class, self::FIELD_PARENT_ID);
    }

    public function delete()
    {
        $thumbs = $this->getThumbs();
        foreach ($thumbs as $thumb) {
            $thumb->delete();
        }

        return parent::delete();
    }

    /**
     * @return MorphTo
     */
    public function imagable(): MorphTo
    {
        return $this->morphTo();
    }
}
