<?php


namespace App\Domain\Shared\Image\Events;


use App\Domain\Shared\Image\Image;

final class ImageDeleted
{
    /**
     * @var Image
     */
    private $image;

    /**
     * ImageSaved constructor.
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }
}
