<?php


namespace App\Domain\Shared\Image\Events;


use App\Domain\Shared\Image\Image;

/**
 * Class ImageSaved
 * @package App\Domain\Shared\Events
 */
final class ImageSaved
{
    /**
     * @var Image
     */
    private $image;

    /**
     * ImageSaved constructor.
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->image = $image;
    }

    /**
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }
}
