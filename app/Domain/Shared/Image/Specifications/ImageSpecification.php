<?php

namespace App\Domain\Shared\Image\Specifications;

use App\Domain\Shared\Image\Image;
use App\Utils\Specification\Contracts\Specification;

/**
 * Class ImageSpecification
 * @package App\Domain\Shared\Image\Specifications
 */
class ImageSpecification implements Specification
{
    private const ALLOWED_EXTENSIONS = Image::ALLOWED_EXTENSIONS;

    /**
     * @var Specification
     */
    private $urlSpecification;

    /**
     * ImageSpecification constructor.
     * @param Specification $urlSpecification
     */
    public function __construct(Specification $urlSpecification)
    {
        $this->urlSpecification = $urlSpecification;
    }

    /**
     * @param mixed $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        try {
            return $this->isImage($value) && $this->hasPath($value) && $this->hasExtension($value);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * @param mixed $value
     * @return bool
     */
    private function isImage($value): bool
    {
        return $value instanceof Image;
    }

    /**
     * @param Image $image
     * @return bool
     */
    private function hasPath(Image $image): bool
    {
        $path = $image->getPath();
        if (! $image->isLocal()) {
            return $this->urlSpecification->isSatisfy($path);
        }

        return $path !== '';
    }

    /**
     * @param Image $image
     * @return bool
     */
    private function hasExtension(Image $image): bool
    {
        $path = $image->getPath();
        $parts = explode('.', $path);
        $extension = strtolower(end($parts));

        return in_array($extension, self::ALLOWED_EXTENSIONS, true);
    }
}
