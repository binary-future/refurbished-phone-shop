<?php


namespace App\Domain\Shared\Image\Observer;

use App\Domain\Shared\Image\Events\ImageDeleted;
use App\Domain\Shared\Image\Events\ImageSaved;
use App\Utils\Adapters\Event\Contracts\Event;
use App\Domain\Shared\Image\Image;

/**
 * Class ImageObserver
 * @package App\Observers
 */
class ImageObserver
{
    /**
     * @var Event
     */
    private $event;

    /**
     * ImageObserver constructor.
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function saved(Image $image)
    {
        $this->event->dispatch(new ImageSaved($image));
    }

    /**
     * Listen to the User deleting event.
     * Deletes image file from the storage place
     *
     * @param  Image $image
     * @return void
     */
    public function deleting(Image $image)
    {
        $this->event->dispatch(new ImageDeleted($image));
    }
}
