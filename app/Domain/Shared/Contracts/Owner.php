<?php

namespace App\Domain\Shared\Contracts;

interface Owner
{
    public function getOwnerType(): string;

    public function getOwnerKey(): int;
}
