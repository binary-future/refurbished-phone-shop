<?php

namespace App\Domain\Shared\Contracts;

use App\Domain\Shared\Link\Link;
use App\Domain\Store\Affiliation\Affiliation;

/**
 * Interface HasLinkWithAffiliation
 * @package App\Domain\Shared\Contracts
 */
interface HasLinkWithAffiliation extends HasLink
{
    public function getAffiliation(): ?Affiliation;
}
