<?php

namespace App\Domain\Shared\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface HasBorrowedImages
 * @package App\Domain\Shared\Contracts
 */
interface HasBorrowedImages extends HasBorrowedImage
{
    /**
     * @return Collection
     */
    public function getImages(): Collection;
}
