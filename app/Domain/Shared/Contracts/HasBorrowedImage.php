<?php

namespace App\Domain\Shared\Contracts;

use App\Domain\Shared\Image\Image;

interface HasBorrowedImage
{
    public function getImage(): ?Image;
}
