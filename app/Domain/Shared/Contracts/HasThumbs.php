<?php

namespace App\Domain\Shared\Contracts;

use App\Domain\Shared\Image\Image;

/**
 * Interface HasThumbs
 * @package App\Domain\Shared\Contracts
 */
interface HasThumbs extends HasImages
{
    /**
     * @param int|null $height
     * @return Image|null
     */
    public function getThumb(int $height = null): ?Image;
}
