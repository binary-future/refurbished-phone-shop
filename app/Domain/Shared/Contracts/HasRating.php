<?php

namespace App\Domain\Shared\Contracts;

use App\Domain\Shared\Rating\Rating;

/**
 * Interface HasRating
 * @package App\Domain\Shared\Contracts
 */
interface HasRating extends Owner
{
    public function getRating(): ?Rating;
}
