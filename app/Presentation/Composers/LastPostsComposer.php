<?php

namespace App\Presentation\Composers;

use App\Application\UseCases\Post\Contracts\LastPostsExceptTagsCase;
use Illuminate\Contracts\View\View;

/**
 * Class LastPostsComposer
 * @package App\Presentation\Composers
 */
final class LastPostsComposer
{
    private const POSTS_QUANTITY = 3;
    private const POSTS_EXCEPT_TAGS = ['homepage'];

    /**
     * @var LastPostsExceptTagsCase
     */
    private $case;

    /**
     * LastPostsComposer constructor.
     * @param LastPostsExceptTagsCase $case
     */
    public function __construct(LastPostsExceptTagsCase $case)
    {
        $this->case = $case;
    }

    /**
     * @param View $view
     * @return View
     */
    public function compose(View $view): View
    {
        return $view->with('posts', $this->case->execute(
            self::POSTS_EXCEPT_TAGS,
            self::POSTS_QUANTITY
        )->getPosts());
    }
}
