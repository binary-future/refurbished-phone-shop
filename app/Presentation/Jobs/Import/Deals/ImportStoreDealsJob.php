<?php

namespace App\Presentation\Jobs\Import\Deals;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\UseCases\Import\Contracts\StoreDealsImportCase;
use App\Presentation\Presenters\Contracts\ImportReportPresenter;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportStoreDealsJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var DatafeedInfo
     */
    private $datafeedInfo;

    /**
     * Create a new job instance.
     *
     * @param DatafeedInfo $datafeedInfo
     */
    public function __construct(DatafeedInfo $datafeedInfo)
    {
        $this->datafeedInfo = $datafeedInfo;
    }

    /**
     * Execute the job.
     *
     * @param StoreDealsImportCase $storeDealsImportCase
     * @param ImportReportPresenter $presenter
     * @return void
     * @throws ImportHandlerBuildException
     */
    public function handle(
        StoreDealsImportCase $storeDealsImportCase,
        ImportReportPresenter $presenter
    ) {
        $response = $storeDealsImportCase->execute($this->datafeedInfo);
        $presenter->present($response);
    }
}
