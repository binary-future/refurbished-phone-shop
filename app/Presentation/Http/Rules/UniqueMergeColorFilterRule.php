<?php

namespace App\Presentation\Http\Rules;

use App\Utils\Specification\Contracts\Specification;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UniqueMergeColorFilterRule implements Rule
{
    /**
     * @var Specification
     */
    private $specification;

    /**
     * @var FormRequest
     */
    private $request;

    /**
     * Create a new rule instance.
     *
     * @param Specification $specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param FormRequest $request
     */
    public function setRequest(FormRequest $request): void
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->specification->isSatisfy($this->request ? $this->request->input() : []);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Filter name has been already taken';
    }
}
