<?php


namespace App\Presentation\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class MergeGroupsRule implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach ($value as $item) {
            if (! $this->isValidGroup($item)) {
                return false;
            }
        }

        return true;
    }

    private function isValidGroup($item): bool
    {
        return $this->checkParent($item['parent'] ?? null) && $this->checkSynonyms($item['synonyms'] ?? null);
    }

    private function checkParent($parent): bool
    {
        return is_numeric($parent);
    }

    private function checkSynonyms($synonyms): bool
    {
        $synonyms = json_decode($synonyms, true);
        if (! is_array($synonyms)) {
            return false;
        }

        foreach ($synonyms as $synonym) {
            if (! is_numeric($synonym)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid merge group';
    }
}
