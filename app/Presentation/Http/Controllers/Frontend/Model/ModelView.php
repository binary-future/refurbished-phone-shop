<?php

namespace App\Presentation\Http\Controllers\Frontend\Model;

use App\Application\UseCases\Phones\Contracts\ModelViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\ModelViewPresenter;
use App\Presentation\Services\Cookie\Contracts\CookieList;
use Illuminate\Http\Request;

final class ModelView extends Controller implements CookieList
{
    /**
     * @var ModelViewCase
     */
    private $modelCase;

    /**
     * @var ModelViewPresenter
     */
    private $modelPresenter;

    /**
     * ModelView constructor.
     * @param ModelViewCase $modelCase
     * @param ModelViewPresenter $modelPresenter
     */
    public function __construct(ModelViewCase $modelCase, ModelViewPresenter $modelPresenter)
    {
        $this->modelCase = $modelCase;
        $this->modelPresenter = $modelPresenter;
    }

    /**
     * @param Request $request
     * @param string $brand
     * @param string $model
     * @return mixed
     */
    public function __invoke(Request $request, string $brand, string $model)
    {
        $response = $this->modelCase->execute($brand, $model, $request->cookie(self::MODEL_VIEW_SORTING_COOKIE_ALIAS));

        return $this->modelPresenter->present($response);
    }
}
