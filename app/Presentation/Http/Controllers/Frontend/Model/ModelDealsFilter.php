<?php

namespace App\Presentation\Http\Controllers\Frontend\Model;

use App\Application\UseCases\Phones\Contracts\ModelDealsFilterCase;
use App\Application\UseCases\Phones\Requests\ModelDealsFilterRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\ModelDealsFilterPresenter as Presenter;
use App\Presentation\Services\Cookie\Contracts\CookieList;
use App\Presentation\Services\Request\Preprocessors\Contracts\ModelDealFilterPreprocessor;
use App\Utils\Adapters\Cookie\Contract\Cookie;
use Illuminate\Http\Request;

final class ModelDealsFilter extends Controller implements CookieList
{
    /**
     * @var ModelDealFilterPreprocessor
     */
    private $preprocessor;

    /**
     * @var ModelDealsFilterCase
     */
    private $case;

    /**
     * @var Presenter
     */
    private $presenter;

    /**
     * @var Cookie
     */
    private $cookie;

    /**
     * ModelDealsFilter constructor.
     * @param ModelDealFilterPreprocessor $preprocessor
     * @param ModelDealsFilterCase $case
     * @param Presenter $presenter
     * @param Cookie $cookie
     */
    public function __construct(
        ModelDealFilterPreprocessor $preprocessor,
        ModelDealsFilterCase $case,
        Presenter $presenter,
        Cookie $cookie
    ) {
        $this->preprocessor = $preprocessor;
        $this->case = $case;
        $this->presenter = $presenter;
        $this->cookie = $cookie;
    }

    public function __invoke(Request $request, string $brand, string $model)
    {
        if ($request->query(ModelDealsFilterRequest::FIELD_SORT)) {
            $this->cookie->queue(
                self::MODEL_VIEW_SORTING_COOKIE_ALIAS,
                $request->query(ModelDealsFilterRequest::FIELD_SORT)
            );
        }

        $request = $this->buildRequest($request->query(), $brand, $model);
        $response = $this->case->execute($request);

        return $this->presenter->present($response);
    }

    /**
     * @param array $params
     * @param string $brand
     * @param string $model
     * @return ModelDealsFilterRequest
     */
    private function buildRequest(array $params, string $brand, string $model): ModelDealsFilterRequest
    {
        try {
            return $this->preprocessor->process($params, $brand, $model);
        } catch (\Throwable $exception) {
            throw new \InvalidArgumentException('Invalid search arguments', 400);
        }
    }
}
