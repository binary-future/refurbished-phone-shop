<?php

namespace App\Presentation\Http\Controllers\Frontend\Model;

use App\Application\UseCases\Phones\Contracts\ModelColorViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\ModelColorViewPresenter;

/**
 * Class ModelColorView
 * @package App\Presentation\Http\Controllers\Frontend\Model
 */
final class ModelColorView extends Controller
{
    /**
     * @var ModelColorViewCase
     */
    private $case;

    /**
     * @var ModelColorViewPresenter
     */
    private $presenter;

    /**
     * ModelColorView constructor.
     * @param ModelColorViewCase $case
     * @param ModelColorViewPresenter $presenter
     */
    public function __construct(ModelColorViewCase $case, ModelColorViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @param string $model
     * @param string $color
     * @return mixed
     */
    public function __invoke(string $brand, string $model, string $color)
    {
        $response = $this->case->execute($brand, $model, $color);

        return $this->presenter->present($response);
    }
}
