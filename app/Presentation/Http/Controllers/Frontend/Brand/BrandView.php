<?php

namespace App\Presentation\Http\Controllers\Frontend\Brand;

use App\Application\UseCases\Phones\Contracts\BrandViewCase;
use App\Presentation\Presenters\Contracts\BrandViewPresenter;
use App\Presentation\Services\Cookie\Contracts\CookieList;
use Illuminate\Http\Request;

/**
 * Class BrandView
 * @package App\Presentation\Http\Controllers\Frontend\Brand
 */
final class BrandView implements CookieList
{
    /**
     * @var BrandViewCase
     */
    private $case;

    /**
     * @var BrandViewPresenter
     */
    private $presenter;

    /**
     * BrandView constructor.
     * @param BrandViewCase $case
     * @param BrandViewPresenter $presenter
     */
    public function __construct(BrandViewCase $case, BrandViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param Request $request
     * @param string $brand
     * @return mixed
     */
    public function __invoke(Request $request, string $brand)
    {
        $response = $this->case->execute($brand, $request->cookie(self::MODEL_VIEW_SORTING_COOKIE_ALIAS));
        return $this->presenter->present($response);
    }
}
