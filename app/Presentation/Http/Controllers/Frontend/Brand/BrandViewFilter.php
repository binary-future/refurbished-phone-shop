<?php

namespace App\Presentation\Http\Controllers\Frontend\Brand;

use App\Application\UseCases\Phones\Contracts\BrandViewDealsFilterCase;
use App\Application\UseCases\Phones\Requests\BrandDealsFilterRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Frontend\Brand\BrandViewFilterRequest;
use App\Presentation\Presenters\Contracts\BrandDealsFilterPresenter;
use App\Presentation\Services\Cookie\Contracts\CookieList;
use App\Presentation\Services\Request\Preprocessors\Contracts\BrandDealFilterPreprocessor;
use App\Utils\Adapters\Cookie\Contract\Cookie;

/**
 * Class BrandViewFilter
 * @package App\Presentation\Http\Controllers\Frontend\Brand
 */
final class BrandViewFilter extends Controller implements CookieList
{
    /**
     * @var BrandViewDealsFilterCase
     */
    private $case;

    /**
     * @var BrandDealsFilterPresenter
     */
    private $presenter;

    /**
     * @var BrandDealFilterPreprocessor
     */
    private $preprocessor;

    /**
     * @var Cookie
     */
    private $cookie;

    /**
     * BrandViewFilter constructor.
     * @param BrandViewDealsFilterCase $case
     * @param BrandDealsFilterPresenter $presenter
     * @param BrandDealFilterPreprocessor $preprocessor
     * @param Cookie $cookie
     */
    public function __construct(
        BrandViewDealsFilterCase $case,
        BrandDealsFilterPresenter $presenter,
        BrandDealFilterPreprocessor $preprocessor,
        Cookie $cookie
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->preprocessor = $preprocessor;
        $this->cookie = $cookie;
    }

    /**
     * @param BrandViewFilterRequest $request
     * @param string $brand
     * @return mixed
     */
    public function __invoke(BrandViewFilterRequest $request, string $brand)
    {
        $validated = $request->validated();
        if (isset($validated[BrandDealsFilterRequest::FIELD_SORT])) {
            $this->cookie->queue(
                self::MODEL_VIEW_SORTING_COOKIE_ALIAS,
                $validated[BrandDealsFilterRequest::FIELD_SORT]
            );
        }

        $newRequest = $this->buildRequest($validated, $brand);
        $response = $this->case->execute($newRequest);

        return $this->presenter->present($response);
    }

    /**
     * @param array $params
     * @param string $brand
     * @return BrandDealsFilterRequest
     */
    private function buildRequest(array $params, string $brand): BrandDealsFilterRequest
    {
        try {
            return $this->preprocessor->process($params, $brand);
        } catch (\Throwable $exception) {
            throw new \InvalidArgumentException('Invalid search arguments', 400);
        }
    }
}
