<?php

namespace App\Presentation\Http\Controllers\Frontend\Brand;

use App\Application\UseCases\Phones\Contracts\BrandsListCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\BrandsListPresenter;

final class BrandIndex extends Controller
{
    /**
     * @var BrandsListCase
     */
    private $useCase;

    /**
     * @var BrandsListPresenter
     */
    private $presenter;

    /**
     * BrandIndex constructor.
     * @param BrandsListCase $useCase
     * @param BrandsListPresenter $presenter
     */
    public function __construct(BrandsListCase $useCase, BrandsListPresenter $presenter)
    {
        $this->useCase = $useCase;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $brands = $this->useCase->execute();

        return $this->presenter->present($brands);
    }
}
