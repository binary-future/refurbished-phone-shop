<?php


namespace App\Presentation\Http\Controllers\Frontend;


use App\Application\UseCases\Main\Contracts\MainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\HomePagePresenter;

/**
 * Class HomePage
 * @package App\Presentation\Http\Controllers\Frontend
 */
final class HomePage extends Controller
{
    /**
     * @var MainCase
     */
    private $case;

    /**
     * @var HomePagePresenter
     */
    private $presenter;

    /**
     * HomePage constructor.
     * @param MainCase $case
     * @param HomePagePresenter $presenter
     */
    public function __construct(MainCase $case, HomePagePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
