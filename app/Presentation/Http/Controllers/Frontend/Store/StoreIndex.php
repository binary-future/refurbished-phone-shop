<?php


namespace App\Presentation\Http\Controllers\Frontend\Store;


use App\Application\UseCases\Store\Contracts\StoreMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\StoreMainPresenter;

/**
 * Class StoreIndex
 * @package App\Presentation\Http\Controllers\Frontend\Store
 */
final class StoreIndex extends Controller
{
    /**
     * @var StoreMainCase
     */
    private $case;

    /**
     * @var StoreMainPresenter
     */
    private $presenter;

    /**
     * StoreIndex constructor.
     * @param StoreMainCase $case
     * @param StoreMainPresenter $presenter
     */
    public function __construct(StoreMainCase $case, StoreMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
