<?php


namespace App\Presentation\Http\Controllers\Frontend\Store;


use App\Application\UseCases\Store\Contracts\StoreViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\StoreViewPresenter;

/**
 * Class StoreView
 * @package App\Presentation\Http\Controllers\Frontend\Store
 */
final class StoreView extends Controller
{
    /**
     * @var StoreViewCase
     */
    private $case;

    /**
     * @var StoreViewPresenter
     */
    private $presenter;

    /**
     * StoreView constructor.
     * @param StoreViewCase $case
     * @param StoreViewPresenter $presenter
     */
    public function __construct(StoreViewCase $case, StoreViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @return mixed
     */
    public function __invoke(string $store)
    {
        $response = $this->case->execute($store);

        return $this->presenter->present($response);
    }
}
