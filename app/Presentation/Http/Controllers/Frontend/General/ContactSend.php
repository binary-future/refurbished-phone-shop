<?php


namespace App\Presentation\Http\Controllers\Frontend\General;


use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Frontend\ContactFormRequest;
use App\Presentation\Presenters\Contracts\ContactSendPresenter;

/**
 * Class ContactSend
 * @package App\Presentation\Http\Controllers\Frontend\General
 */
final class ContactSend extends Controller
{
    /**
     * @var ContactSendPresenter
     */
    private $presenter;

    /**
     * ContactSend constructor.
     * @param ContactSendPresenter $presenter
     */
    public function __construct(ContactSendPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * @param ContactFormRequest $request
     * @return mixed
     */
    public function __invoke(ContactFormRequest $request)
    {
        return $this->presenter->present($request->validated());
    }
}
