<?php


namespace App\Presentation\Http\Controllers\Frontend\General;


use App\Application\UseCases\Main\Contracts\SitemapsCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\SitemapsPresenter;

/**
 * Class SitemapsRender
 * @package App\Presentation\Http\Controllers\Frontend
 */
final class SitemapsRender extends Controller
{
    /**
     * @var SitemapsCase
     */
    private $case;

    /**
     * @var SitemapsPresenter
     */
    private $presenter;

    /**
     * SitemapsRender constructor.
     * @param SitemapsCase $case
     * @param SitemapsPresenter $presenter
     */
    public function __construct(SitemapsCase $case, SitemapsPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
