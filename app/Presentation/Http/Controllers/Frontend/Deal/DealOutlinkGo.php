<?php

namespace App\Presentation\Http\Controllers\Frontend\Deal;

use App\Application\UseCases\Deal\Contracts\DealOutlinkGoCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\DealOutlinkGoPresenter;
use Illuminate\Http\Request;

final class DealOutlinkGo extends Controller
{
    /**
     * @var DealOutlinkGoCase
     */
    private $case;

    /**
     * @var DealOutlinkGoPresenter
     */
    private $presenter;

    /**
     * DealOutlink constructor.
     * @param DealOutlinkGoCase $case
     * @param DealOutlinkGoPresenter $presenter
     */
    public function __construct(
        DealOutlinkGoCase $case,
        DealOutlinkGoPresenter $presenter
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(Request $request, int $deal)
    {
        $response = $this->case->execute(
            $deal
        );

        return $this->presenter->present($response);
    }
}
