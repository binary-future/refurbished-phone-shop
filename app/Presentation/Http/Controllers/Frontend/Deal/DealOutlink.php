<?php

namespace App\Presentation\Http\Controllers\Frontend\Deal;

use App\Application\UseCases\Deal\Contracts\DealOutlinkCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\DealOutlinkPresenter;
use App\Presentation\Services\Routes\Dictionaries\DealsRouteDict;
use App\Presentation\Services\Routes\RouteMap;
use App\Utils\Adapters\UrlGenerator\Contracts\UrlGenerator;
use Illuminate\Http\Request;

final class DealOutlink extends Controller
{
    /**
     * @var DealOutlinkPresenter
     */
    private $presenter;
    /**
     * @var UrlGenerator
     */
    private $urlGenerator;
    /**
     * @var DealOutlinkCase
     */
    private $case;

    /**
     * DealOutlink constructor.
     * @param DealOutlinkPresenter $presenter
     * @param UrlGenerator $urlGenerator
     */
    public function __construct(
        DealOutlinkPresenter $presenter,
        DealOutlinkCase $case,
        UrlGenerator $urlGenerator
    ) {
        $this->presenter = $presenter;
        $this->urlGenerator = $urlGenerator;
        $this->case = $case;
    }

    public function __invoke(int $deal)
    {
        $response = $this->case->execute($deal);
        $urlToGo = $this->urlGenerator->route(DealsRouteDict::DEAL_OUTLINK_GO, [
            DealsRouteDict::DEAL_OUTLINK_GO_PARAM_DEAL => $deal
        ]);
        $response->setUrlToGo($urlToGo);

        return $this->presenter->present($response);
    }
}
