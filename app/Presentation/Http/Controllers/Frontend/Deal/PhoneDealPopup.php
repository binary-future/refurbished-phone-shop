<?php

namespace App\Presentation\Http\Controllers\Frontend\Deal;

use App\Application\UseCases\Deal\Contracts\PhoneDealCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\PhoneDealPresenter;

/**
 * Class PhoneDealPopup
 * @package App\Presentation\Http\Controllers\Frontend\Deal
 */
final class PhoneDealPopup extends Controller
{
    /**
     * @var PhoneDealCase
     */
    private $case;

    /**
     * @var PhoneDealPresenter
     */
    private $presenter;

    /**
     * PhoneDeal constructor.
     * @param PhoneDealCase $case
     * @param PhoneDealPresenter $presenter
     */
    public function __construct(PhoneDealCase $case, PhoneDealPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke($deal)
    {
        $response = $this->case->execute((int) $deal);

        return $this->presenter->present($response);
    }
}
