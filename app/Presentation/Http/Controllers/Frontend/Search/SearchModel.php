<?php


namespace App\Presentation\Http\Controllers\Frontend\Search;


use App\Application\UseCases\Phones\Contracts\ModelSearchCase;
use App\Application\UseCases\Phones\Requests\ModelSearchRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Frontend\ModelSearchFormRequest;
use App\Presentation\Presenters\Contracts\ModelSearchPresenter;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Http\Request;

/**
 * Class SearchModel
 * @package App\Presentation\Http\Controllers\Frontend\Search
 */
final class SearchModel extends Controller
{
    /**
     * @var ModelSearchCase
     */
    private $case;

    /**
     * @var ModelSearchPresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SearchModel constructor.
     * @param ModelSearchCase $case
     * @param ModelSearchPresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(ModelSearchCase $case, ModelSearchPresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param ModelSearchFormRequest $request
     * @return mixed
     */
    public function __invoke(ModelSearchFormRequest $request)
    {
        /**
         * @var ModelSearchRequest $searchRequest
         */
        $searchRequest = $this->serializer->fromArray(ModelSearchRequest::class, $request->query());
        $response = $this->case->execute($searchRequest);
        $response->getModels()->appends($request->query());

        return $this->presenter->present($response);
    }
}
