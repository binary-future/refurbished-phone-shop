<?php

namespace App\Presentation\Http\Controllers\Frontend\Search;

use App\Application\UseCases\Phones\Contracts\ModelSearchCase;
use App\Application\UseCases\Phones\Requests\ModelSearchRequest;
use App\Presentation\Http\Requests\Frontend\ModelSearchFormRequest;
use App\Presentation\Presenters\Contracts\ModelSearchPresenter;
use App\Utils\Serializer\Contracts\Serializer;

class AutocompleteModel
{
    private const LIMIT = 24;

    /**
     * @var ModelSearchCase
     */
    private $case;

    /**
     * @var ModelSearchPresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * SearchModel constructor.
     * @param ModelSearchCase $case
     * @param ModelSearchPresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(ModelSearchCase $case, ModelSearchPresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param ModelSearchFormRequest $request
     * @return mixed
     */
    public function __invoke(ModelSearchFormRequest $request)
    {
        /**
         * @var ModelSearchRequest $searchRequest
         */
        $searchRequest = $this->serializer->fromArray(
            ModelSearchRequest::class,
            array_merge($request->query(), ['limit' => self::LIMIT])
        );
        $response = $this->case->execute($searchRequest);

        return $this->presenter->present($response);
    }
}