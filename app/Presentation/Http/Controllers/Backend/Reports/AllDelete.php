<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\AllReportsDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ReportsGroupDeletePresenter;

/**
 * Class AllDelete
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class AllDelete extends Controller
{
    /**
     * @var AllReportsDeleteCase
     */
    private $case;

    /**
     * @var ReportsGroupDeletePresenter
     */
    private $presenter;

    /**
     * AllDelete constructor.
     * @param AllReportsDeleteCase $case
     * @param ReportsGroupDeletePresenter $presenter
     */
    public function __construct(AllReportsDeleteCase $case, ReportsGroupDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function __invoke(string $type)
    {
        $response = $this->case->execute($type);

        return $this->presenter->present($response);
    }
}
