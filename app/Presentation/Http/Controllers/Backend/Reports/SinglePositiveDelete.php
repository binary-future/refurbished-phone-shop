<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\SinglePositiveDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsDeletePresenter;

/**
 * Class SinglePositiveDelete
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class SinglePositiveDelete extends Controller
{
    /**
     * @var SinglePositiveDeleteCase
     */
    private $case;

    /**
     * @var PositiveReportsDeletePresenter
     */
    private $presenter;

    /**
     * SinglePositiveDelete constructor.
     * @param SinglePositiveDeleteCase $case
     * @param PositiveReportsDeletePresenter $presenter
     */
    public function __construct(SinglePositiveDeleteCase $case, PositiveReportsDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @param int $report
     * @return mixed
     */
    public function __invoke(string $store, int $report)
    {
        $response = $this->case->execute($store, $report);

        return $this->presenter->present($response);
    }
}
