<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\AllReportsCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\AllReportsPresenter;

final class AllReports extends Controller
{
    /**
     * @var AllReportsCase
     */
    private $case;

    /**
     * @var AllReportsPresenter
     */
    private $presenter;

    /**
     * AllReports constructor.
     * @param AllReportsCase $case
     * @param AllReportsPresenter $presenter
     */
    public function __construct(AllReportsCase $case, AllReportsPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $type)
    {
        $response = $this->case->execute($type);

        return $this->presenter->present($response);
    }
}