<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\NegativeViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsViewPresenter;

/**
 * Class NegativeView
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class NegativeView extends Controller
{
    /**
     * @var NegativeViewCase
     */
    private $case;

    /**
     * @var NegativeReportsViewPresenter
     */
    private $presenter;

    /**
     * NegativeView constructor.
     * @param NegativeViewCase $case
     * @param NegativeReportsViewPresenter $presenter
     */
    public function __construct(NegativeViewCase $case, NegativeReportsViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $store, string $type)
    {
        $response = $this->case->execute($store, $type);

        return $this->presenter->present($response);
    }
}
