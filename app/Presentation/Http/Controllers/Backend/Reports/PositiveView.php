<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\PositiveViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsViewPresenter;

/**
 * Class PositiveView
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class PositiveView extends Controller
{
    /**
     * @var PositiveViewCase
     */
    private $case;

    /**
     * @var PositiveReportsViewPresenter
     */
    private $presenter;

    /**
     * PositiveView constructor.
     * @param PositiveViewCase $case
     * @param PositiveReportsViewPresenter $presenter
     */
    public function __construct(PositiveViewCase $case, PositiveReportsViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @param string $type
     * @return mixed
     */
    public function __invoke(string $store, string $type)
    {
        $response = $this->case->execute($store, $type);

        return $this->presenter->present($response);
    }
}
