<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\ByStoreDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ReportsGroupDeletePresenter;

/**
 * Class ByStoreDelete
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class ByStoreDelete extends Controller
{
    /**
     * @var ByStoreDeleteCase
     */
    private $case;

    /**
     * @var ReportsGroupDeletePresenter
     */
    private $presenter;

    /**
     * ByStoreDelete constructor.
     * @param ByStoreDeleteCase $case
     * @param ReportsGroupDeletePresenter $presenter
     */
    public function __construct(ByStoreDeleteCase $case, ReportsGroupDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @param string $type
     * @return mixed
     */
    public function __invoke(string $store, string $type)
    {
        $response = $this->case->execute($store, $type);

        return $this->presenter->present($response);
    }
}
