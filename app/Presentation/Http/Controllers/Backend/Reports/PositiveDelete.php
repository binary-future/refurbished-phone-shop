<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\PositiveDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsDeletePresenter;

/**
 * Class PositiveDelete
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class PositiveDelete extends Controller
{
    /**
     * @var PositiveDeleteCase
     */
    private $case;

    /**
     * @var PositiveReportsDeletePresenter
     */
    private $presenter;

    /**
     * PositiveDelete constructor.
     * @param PositiveDeleteCase $case
     * @param PositiveReportsDeletePresenter $presenter
     */
    public function __construct(PositiveDeleteCase $case, PositiveReportsDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @param string $type
     * @return mixed
     */
    public function __invoke(string $store, string $type)
    {
        $reponse = $this->case->execute($store, $type);

        return $this->presenter->present($reponse);
    }
}
