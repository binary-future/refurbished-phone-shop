<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\SingleNegativeDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsDeletePresenter;

/**
 * Class SingleNegativeDelete
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class SingleNegativeDelete extends Controller
{
    /**
     * @var SingleNegativeDeleteCase
     */
    private $case;

    /**
     * @var NegativeReportsDeletePresenter
     */
    private $presenter;

    /**
     * SingleNegativeDelete constructor.
     * @param SingleNegativeDeleteCase $case
     * @param NegativeReportsDeletePresenter $presenter
     */
    public function __construct(SingleNegativeDeleteCase $case, NegativeReportsDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param $store
     * @param $report
     * @return mixed
     */
    public function __invoke($store, $report)
    {
        $response = $this->case->execute($store, $report);

        return $this->presenter->present($response);
    }
}
