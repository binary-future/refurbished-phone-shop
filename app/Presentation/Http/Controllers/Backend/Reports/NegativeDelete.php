<?php


namespace App\Presentation\Http\Controllers\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Contracts\NegativeDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsDeletePresenter;

/**
 * Class NegativeDelete
 * @package App\Presentation\Http\Controllers\Backend\Reports
 */
final class NegativeDelete extends Controller
{
    /**
     * @var NegativeDeleteCase
     */
    private $case;

    /**
     * @var NegativeReportsDeletePresenter
     */
    private $presenter;

    /**
     * NegativeDelete constructor.
     * @param NegativeDeleteCase $case
     * @param NegativeReportsDeletePresenter $presenter
     */
    public function __construct(NegativeDeleteCase $case, NegativeReportsDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @return mixed
     */
    public function __invoke(string $store, string $type)
    {
        $response = $this->case->execute($store, $type);

        return $this->presenter->present($response);
    }
}