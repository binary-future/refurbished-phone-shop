<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Color;


use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorSynonymsCase;
use App\Presentation\Presenters\Contracts\Backend\ColorSynonymsViewPresenter;

final class ColorSynonyms
{
    /**
     * @var ColorSynonymsCase
     */
    private $case;

    /**
     * @var ColorSynonymsViewPresenter
     */
    private $presenter;

    /**
     * ColorSynonyms constructor.
     * @param ColorSynonymsCase $case
     * @param ColorSynonymsViewPresenter $presenter
     */
    public function __construct(ColorSynonymsCase $case, ColorSynonymsViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $color)
    {
        $response = $this->case->execute($color);

        return $this->presenter->present($response);
    }
}
