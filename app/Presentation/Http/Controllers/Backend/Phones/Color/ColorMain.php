<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Color;


use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ColorMainPresenter;

final class ColorMain extends Controller
{
    /**
     * @var ColorMainCase
     */
    private $case;

    /**
     * @var ColorMainPresenter
     */
    private $presenter;

    /**
     * ColorMain constructor.
     * @param ColorMainCase $case
     * @param ColorMainPresenter $presenter
     */
    public function __construct(ColorMainCase $case, ColorMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}