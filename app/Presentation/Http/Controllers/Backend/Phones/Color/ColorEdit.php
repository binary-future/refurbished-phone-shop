<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Color;


use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ColorEditPresenter;

/**
 * Class ColorEdit
 * @package App\Presentation\Http\Controllers\Backend\Phones\Color
 */
final class ColorEdit extends Controller
{
    /**
     * @var ColorEditCase
     */
    private $case;

    /**
     * @var ColorEditPresenter
     */
    private $presenter;

    /**
     * ColorEdit constructor.
     * @param ColorEditCase $case
     * @param ColorEditPresenter $presenter
     */
    public function __construct(ColorEditCase $case, ColorEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $color
     * @return mixed
     */
    public function __invoke(string $color)
    {
        $response = $this->case->execute($color);

        return $this->presenter->present($response);
    }
}
