<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Color;


use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorRemoveSynonymCase;
use App\Presentation\Presenters\Contracts\Backend\ColorManageSynonymPresenter;
use Illuminate\Http\Request;

final class ColorRemoveSynonym
{
    /**
     * @var ColorRemoveSynonymCase
     */
    private $case;

    /**
     * @var ColorManageSynonymPresenter
     */
    private $presenter;

    /**
     * ColorRemoveSynonym constructor.
     * @param ColorRemoveSynonymCase $case
     * @param ColorManageSynonymPresenter $presenter
     */
    public function __construct(ColorRemoveSynonymCase $case, ColorManageSynonymPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(Request $request, string $color)
    {
        $response = $this->case->execute($color, $request->get('synonym'));

        return $this->presenter->present($response);
    }
}
