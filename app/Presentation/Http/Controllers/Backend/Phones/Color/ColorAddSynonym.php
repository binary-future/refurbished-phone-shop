<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Color;


use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorAddSynonymCase;
use App\Presentation\Http\Requests\Backend\ColorAddSynonymRequest;
use App\Presentation\Presenters\Contracts\Backend\ColorManageSynonymPresenter;

final class ColorAddSynonym
{
    /**
     * @var ColorAddSynonymCase
     */
    private $case;

    /**
     * @var ColorManageSynonymPresenter
     */
    private $presenter;

    /**
     * ColorAddSynonym constructor.
     * @param ColorAddSynonymCase $case
     * @param ColorManageSynonymPresenter $presenter
     */
    public function __construct(ColorAddSynonymCase $case, ColorManageSynonymPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(ColorAddSynonymRequest $request, string $color)
    {
        $response = $this->case->execute($color, $request->get('synonym'));

        return $this->presenter->present($response);
    }
}
