<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Color;


use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorUpdateCase;
use App\Application\UseCases\Backend\Phones\Color\Requests\ColorUpdateRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\ColorUpdateFormRequest;
use App\Presentation\Presenters\Contracts\Backend\ColorUpdatePresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class ColorUpdate
 * @package App\Presentation\Http\Controllers\Backend\Phones\Color
 */
final class ColorUpdate extends Controller
{
    /**
     * @var ColorUpdateCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ColorUpdatePresenter
     */
    private $presenter;

    /**
     * ColorUpdate constructor.
     * @param ColorUpdateCase $case
     * @param Serializer $serializer
     * @param ColorUpdatePresenter $presenter
     */
    public function __construct(ColorUpdateCase $case, Serializer $serializer, ColorUpdatePresenter $presenter)
    {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    /**
     * @param ColorUpdateFormRequest $request
     * @param string $color
     * @return mixed
     */
    public function __invoke(ColorUpdateFormRequest $request, string $color)
    {
        /**
         * @var ColorUpdateRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(ColorUpdateRequest::class, $request->validated());
        $response = $this->case->execute($color, $caseRequest);

        return $this->presenter->present($response);
    }
}
