<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Brand;

use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandUpdateCase;
use App\Application\UseCases\Backend\Phones\Brand\Requests\UpdateBrandRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\BrandUpdateRequest;
use App\Presentation\Presenters\Contracts\Backend\BrandUpdatePresenter;

/**
 * Class BrandUpdate
 * @package App\Presentation\Http\Controllers\Backend\Phones\Brand
 */
final class BrandUpdate extends Controller
{
    /**
     * @var BrandUpdateCase
     */
    private $case;

    /**
     * @var BrandUpdatePresenter
     */
    private $presenter;

    /**
     * BrandUpdate constructor.
     * @param BrandUpdateCase $case
     * @param BrandUpdatePresenter $presenter
     */
    public function __construct(BrandUpdateCase $case, BrandUpdatePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param BrandUpdateRequest $request
     * @param string $brand
     * @return mixed
     */
    public function __invoke(BrandUpdateRequest $request, string $brand)
    {
        $caseRequest = UpdateBrandRequest::fromArray(array_merge($request->validated(), ['slug' => $brand]));
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
