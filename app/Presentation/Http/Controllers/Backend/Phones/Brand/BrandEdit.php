<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Brand;

use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\BrandEditPresenter;

final class BrandEdit extends Controller
{
    /**
     * @var BrandEditCase
     */
    private $case;

    /**
     * @var BrandEditPresenter
     */
    private $presenter;

    /**
     * BrandEdit constructor.
     * @param BrandEditCase $case
     * @param BrandEditPresenter $presenter
     */
    public function __construct(BrandEditCase $case, BrandEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @return mixed
     */
    public function __invoke(string $brand)
    {
        $response = $this->case->execute($brand);

        return $this->presenter->present($response);
    }
}
