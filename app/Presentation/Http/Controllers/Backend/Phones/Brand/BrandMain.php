<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Brand;


use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\BrandMainPresenter;

/**
 * Class BrandMain
 * @package App\Presentation\Http\Controllers\Backend\Phones\Brand
 */
final class BrandMain extends Controller
{
    /**
     * @var BrandMainCase
     */
    private $case;

    /**
     * @var BrandMainPresenter
     */
    private $presenter;

    /**
     * BrandMain constructor.
     * @param BrandMainCase $case
     * @param BrandMainPresenter $presenter
     */
    public function __construct(BrandMainCase $case, BrandMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
