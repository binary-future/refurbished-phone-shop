<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelUpdateCase;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelUpdateRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\TopModelUpdateHttpRequest;
use App\Presentation\Presenters\Contracts\Backend\TopModelCreatePresenter;
use App\Presentation\Presenters\Contracts\Backend\TopModelUpdatePresenter;
use App\Utils\Serializer\Serializer;

/**
 * Class TopModelUpdate
 * @package App\Presentation\Http\Controllers\Backend\Phones\TopModel
 */
final class TopModelUpdate extends Controller
{
    /**
     * @var TopModelUpdateCase
     */
    private $case;

    /**
     * @var TopModelCreatePresenter
     */
    private $presenter;
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ModelIndex constructor.
     * @param TopModelUpdateCase $case
     * @param TopModelUpdatePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(
        TopModelUpdateCase $case,
        TopModelUpdatePresenter $presenter,
        Serializer $serializer
    )
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param TopModelUpdateHttpRequest $request
     * @param int $topModelId
     * @return mixed
     * @throws \App\Utils\Serializer\Exception\SerializerException
     */
    public function __invoke(TopModelUpdateHttpRequest $request, int $topModelId)
    {
        /**
         * @var $caseRequest TopModelUpdateRequest
         */
        $caseRequest = $this->serializer->fromArray(
            TopModelUpdateRequest::class,
            $request->validated()
        );
        $caseRequest->setTopModelId($topModelId);

        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
