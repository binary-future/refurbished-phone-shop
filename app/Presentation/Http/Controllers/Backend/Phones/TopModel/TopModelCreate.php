<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelCreateRequest as CreateTopModelRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelCreateCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\TopModelCreateRequest;
use App\Presentation\Presenters\Contracts\Backend\TopModelCreatePresenter;
use App\Utils\Serializer\Serializer;

/**
 * Class TopModelAdd
 * @package App\Presentation\Http\Controllers\Backend\Phones\TopModel
 */
final class TopModelCreate extends Controller
{
    /**
     * @var TopModelCreateCase
     */
    private $case;

    /**
     * @var TopModelCreatePresenter
     */
    private $presenter;
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ModelIndex constructor.
     * @param TopModelCreateCase $case
     * @param TopModelCreatePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(
        TopModelCreateCase $case,
        TopModelCreatePresenter $presenter,
        Serializer $serializer
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param TopModelCreateRequest $request
     * @return mixed
     * @throws \App\Utils\Serializer\Exception\SerializerException
     */
    public function __invoke(TopModelCreateRequest $request)
    {
        /**
         * @var $caseRequest CreateTopModelRequest
         */
        $caseRequest = $this->serializer->fromArray(CreateTopModelRequest::class, $request->validated());

        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
