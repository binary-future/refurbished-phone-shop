<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelDeleteCase;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelDeleteRequest as DeleteTopModelRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\TopModelDeletePresenter;

/**
 * Class TopModelDelete
 * @package App\Presentation\Http\Controllers\Backend\Phones\TopModel
 */
final class TopModelDelete extends Controller
{
    /**
     * @var TopModelDeleteCase
     */
    private $case;

    /**
     * @var TopModelDeletePresenter
     */
    private $presenter;

    /**
     * ModelIndex constructor.
     * @param TopModelDeleteCase $case
     * @param TopModelDeletePresenter $presenter
     */
    public function __construct(
        TopModelDeleteCase $case,
        TopModelDeletePresenter $presenter
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $topModelId
     * @return mixed
     */
    public function __invoke(int $topModelId)
    {
        $caseRequest = new DeleteTopModelRequest($topModelId);

        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
