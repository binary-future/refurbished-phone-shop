<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\TopModelMainPresenter;

/**
 * Class TopModelIndex
 * @package App\Presentation\Http\Controllers\Backend\Phones\TopModel
 */
final class TopModelIndex extends Controller
{
    /**
     * @var TopModelMainCase
     */
    private $case;

    /**
     * @var TopModelMainPresenter
     */
    private $presenter;

    /**
     * ModelIndex constructor.
     * @param TopModelMainCase $case
     * @param TopModelMainPresenter $presenter
     */
    public function __construct(TopModelMainCase $case, TopModelMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
