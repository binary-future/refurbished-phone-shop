<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ModelEditPresenter;

/**
 * Class ModelEdit
 * @package App\Presentation\Http\Controllers\Backend\Phones\Model
 */
final class ModelEdit extends Controller
{
    /**
     * @var ModelEditCase
     */
    private $case;

    /**
     * @var ModelEditPresenter
     */
    private $presenter;

    /**
     * ModelEdit constructor.
     * @param ModelEditCase $case
     * @param ModelEditPresenter $presenter
     */
    public function __construct(ModelEditCase $case, ModelEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @param string $model
     * @return mixed
     */
    public function __invoke(string $brand, string $model)
    {
        $response = $this->case->execute($brand, $model);

        return $this->presenter->present($response);
    }
}
