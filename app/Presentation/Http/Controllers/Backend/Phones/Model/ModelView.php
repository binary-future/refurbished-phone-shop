<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ModelViewPresenter;

/**
 * Class ModelView
 * @package App\Presentation\Http\Controllers\Backend\Phones\Model
 */
final class ModelView extends Controller
{
    /**
     * @var ModelViewCase
     */
    private $case;

    /**
     * @var ModelViewPresenter
     */
    private $presenter;

    /**
     * ModelView constructor.
     * @param ModelViewCase $case
     * @param ModelViewPresenter $presenter
     */
    public function __construct(ModelViewCase $case, ModelViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @param string $model
     * @return mixed
     */
    public function __invoke(string $brand, string $model)
    {
        $response = $this->case->execute($brand, $model);

        return $this->presenter->present($response);
    }
}
