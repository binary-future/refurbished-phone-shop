<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelSearchCase;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelSearchRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\PhoneModelSearchRequest;
use App\Presentation\Presenters\Contracts\Backend\ModelSearchPresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class ModelSearch
 * @package App\Presentation\Http\Controllers\Backend\Phones\Model
 */
final class ModelSearch extends Controller
{
    /**
     * @var ModelSearchCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ModelSearchPresenter
     */
    private $presenter;

    /**
     * ModelSearch constructor.
     * @param ModelSearchCase $case
     * @param Serializer $serializer
     * @param ModelSearchPresenter $presenter
     */
    public function __construct(ModelSearchCase $case, Serializer $serializer, ModelSearchPresenter $presenter)
    {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    /**
     * @param PhoneModelSearchRequest $request
     * @return mixed
     */
    public function __invoke(PhoneModelSearchRequest $request)
    {
        /**
         * @var ModelSearchRequest $searchRequest
         */
        $searchRequest = $this->serializer->fromArray(ModelSearchRequest::class, $request->validated());
        $response = $this->case->execute($searchRequest);
        $response->getModels()->appends($request->query());

        return $this->presenter->present($response);
    }
}
