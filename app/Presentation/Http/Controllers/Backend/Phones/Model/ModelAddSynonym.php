<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelAddSynonymCase;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelAddSynonymRequest;
use App\Presentation\Http\Requests\Backend\ModelAddSynonymHttpRequest;
use App\Presentation\Presenters\Contracts\Backend\ModelManageSynonymPresenter;
use Illuminate\Http\Request;

final class ModelAddSynonym
{
    /**
     * @var ModelAddSynonymCase
     */
    private $case;

    /**
     * @var ModelManageSynonymPresenter
     */
    private $presenter;

    /**
     * ModelAddSynonym constructor.
     * @param ModelAddSynonymCase $case
     * @param ModelManageSynonymPresenter $presenter
     */
    public function __construct(ModelAddSynonymCase $case, ModelManageSynonymPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(ModelAddSynonymHttpRequest $request, string $brand, string $model)
    {
        $caseRequest = new ModelAddSynonymRequest($brand, $model, $request->input('synonym'));
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
