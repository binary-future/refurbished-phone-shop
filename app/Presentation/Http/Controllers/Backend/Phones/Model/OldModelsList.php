<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\OldPhonesListCase;
use App\Presentation\Presenters\Contracts\Backend\OldPhonesListPresenter;

final class OldModelsList
{
    /**
     * @var OldPhonesListCase
     */
    private $case;

    /**
     * @var OldPhonesListPresenter
     */
    private $presenter;

    /**
     * OldModelsList constructor.
     * @param OldPhonesListCase $case
     * @param OldPhonesListPresenter $presenter
     */
    public function __construct(OldPhonesListCase $case, OldPhonesListPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
