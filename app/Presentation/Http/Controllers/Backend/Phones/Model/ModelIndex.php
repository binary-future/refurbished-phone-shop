<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ModelMainPresenter;

/**
 * Class ModelIndex
 * @package App\Presentation\Http\Controllers\Backend\Phones\Model
 */
final class ModelIndex extends Controller
{
    /**
     * @var ModelMainCase
     */
    private $case;

    /**
     * @var ModelMainPresenter
     */
    private $presenter;

    /**
     * ModelIndex constructor.
     * @param ModelMainCase $case
     * @param ModelMainPresenter $presenter
     */
    public function __construct(ModelMainCase $case, ModelMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
