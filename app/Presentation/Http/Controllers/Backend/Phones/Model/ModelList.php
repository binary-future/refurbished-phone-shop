<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelListCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ModelListPresenter;

final class ModelList extends Controller
{
    /**
     * @var ModelListCase
     */
    private $case;

    /**
     * @var ModelListPresenter
     */
    private $presenter;

    /**
     * ModelList constructor.
     * @param ModelListCase $case
     * @param ModelListPresenter $presenter
     */
    public function __construct(ModelListCase $case, ModelListPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @return mixed
     */
    public function __invoke(string $brand)
    {
        $response = $this->case->execute($brand);

        return $this->presenter->present($response);
    }
}