<?php

namespace App\Presentation\Http\Controllers\Backend\Phones\Model;

use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelAutocompleteCase;
use App\Presentation\Presenters\Contracts\Backend\ModelAutocompletePresenter;
use Illuminate\Http\Request;

final class Autocomplete
{
    /**
     * @var ModelAutocompleteCase
     */
    private $case;

    /**
     * @var ModelAutocompletePresenter
     */
    private $presenter;

    /**
     * Autocomplete constructor.
     * @param ModelAutocompleteCase $case
     * @param ModelAutocompletePresenter $presenter
     */
    public function __construct(ModelAutocompleteCase $case, ModelAutocompletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(Request $request)
    {
        $response = $this->case->execute($request->query('name'));

        return $this->presenter->present($response);
    }
}