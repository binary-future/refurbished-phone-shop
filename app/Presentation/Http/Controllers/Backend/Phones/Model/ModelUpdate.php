<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelUpdateCase;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelUpdateRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\PhoneModelUpdateRequest;
use App\Presentation\Presenters\Contracts\Backend\ModelUpdatePresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class ModelUpdate
 * @package App\Presentation\Http\Controllers\Backend\Phones\Model
 */
final class ModelUpdate extends Controller
{
    /**
     * @var ModelUpdateCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ModelUpdatePresenter
     */
    private $presenter;

    /**
     * ModelUpdate constructor.
     * @param ModelUpdateCase $case
     * @param Serializer $serializer
     * @param ModelUpdatePresenter $presenter
     */
    public function __construct(ModelUpdateCase $case, Serializer $serializer, ModelUpdatePresenter $presenter)
    {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    /**
     * @param PhoneModelUpdateRequest $request
     * @param string $brand
     * @param string $model
     * @return mixed
     */
    public function __invoke(PhoneModelUpdateRequest $request, string $brand, string $model)
    {
        /**
         * @var ModelUpdateRequest $updateRequest
         */
        $updateRequest = $this->serializer->fromArray(ModelUpdateRequest::class, $request->validated());
        $updateRequest->setModel($model);
        $updateRequest->setBrand($brand);
        $response = $this->case->execute($updateRequest);

        return $this->presenter->present($response);
    }
}
