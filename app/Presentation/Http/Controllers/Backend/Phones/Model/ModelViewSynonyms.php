<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelSynonymViewCase;
use App\Presentation\Presenters\Contracts\Backend\ModelSynonymViewPresenter;

final class ModelViewSynonyms
{
    /**
     * @var ModelSynonymViewCase
     */
    private $case;

    /**
     * @var ModelSynonymViewPresenter
     */
    private $presenter;

    /**
     * ModelViewSynonyms constructor.
     * @param ModelSynonymViewCase $case
     * @param ModelSynonymViewPresenter $presenter
     */
    public function __construct(ModelSynonymViewCase $case, ModelSynonymViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $brand, string $model)
    {
        $response = $this->case->execute($brand, $model);

        return $this->presenter->present($response);
    }
}
