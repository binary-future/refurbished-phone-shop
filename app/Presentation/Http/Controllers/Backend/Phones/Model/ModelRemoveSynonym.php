<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Model;


use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelRemoveSynonymCase;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelRemoveSynonymRequest;
use App\Presentation\Http\Requests\Backend\ModelRemoveSynonymHttpRequest;
use App\Presentation\Presenters\Contracts\Backend\ModelManageSynonymPresenter;

final class ModelRemoveSynonym
{
    /**
     * @var ModelRemoveSynonymCase
     */
    private $case;

    /**
     * @var ModelManageSynonymPresenter
     */
    private $presenter;

    /**
     * ModelRemoveSynonym constructor.
     * @param ModelRemoveSynonymCase $case
     * @param ModelManageSynonymPresenter $presenter
     */
    public function __construct(ModelRemoveSynonymCase $case, ModelManageSynonymPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(ModelRemoveSynonymHttpRequest $request, string $brand, string $model)
    {
        $caseRequest = new ModelRemoveSynonymRequest($brand, $model, $request->get('synonym'));
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
