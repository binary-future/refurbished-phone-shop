<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Phone\Gallery;


use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryDeleteImageCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryDeleteImagePresenter;

final class DeleteImage extends Controller
{
    /**
     * @var PhoneGalleryDeleteImageCase
     */
    private $case;

    /**
     * @var PhoneGalleryDeleteImagePresenter
     */
    private $presenter;

    /**
     * DeleteImage constructor.
     * @param PhoneGalleryDeleteImageCase $case
     * @param PhoneGalleryDeleteImagePresenter $presenter
     */
    public function __construct(PhoneGalleryDeleteImageCase $case, PhoneGalleryDeleteImagePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $phone, int $image)
    {
        $response = $this->case->execute($phone, $image);

        return $this->presenter->present($response);
    }
}