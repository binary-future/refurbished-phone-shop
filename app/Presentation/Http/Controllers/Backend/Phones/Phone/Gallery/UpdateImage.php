<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Phone\Gallery;


use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryUpdateImageCase;
use App\Application\UseCases\Backend\Phones\Phone\Requests\PhoneGalleryUpdateImageRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryUpdateImagePresenter;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Http\Request;

/**
 * Class UpdateImage
 * @package App\Presentation\Http\Controllers\Backend\Phones\Phone\Gallery
 */
final class UpdateImage extends Controller
{
    /**
     * @var PhoneGalleryUpdateImageCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var PhoneGalleryUpdateImagePresenter
     */
    private $presenter;

    /**
     * UpdateImage constructor.
     * @param PhoneGalleryUpdateImageCase $case
     * @param Serializer $serializer
     * @param PhoneGalleryUpdateImagePresenter $presenter
     */
    public function __construct(
        PhoneGalleryUpdateImageCase $case,
        Serializer $serializer,
        PhoneGalleryUpdateImagePresenter $presenter
    ) {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    /**
     * @param Request $request
     * @param $phone
     * @param $image
     * @return mixed
     */
    public function __invoke(Request $request, $phone, $image)
    {
        $data = $request->input();
        $data['phoneId'] = $phone;
        $data['imageId'] = $image;
        /**
         * @var PhoneGalleryUpdateImageRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(PhoneGalleryUpdateImageRequest::class, $data);
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
