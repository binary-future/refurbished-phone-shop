<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Phone\Gallery;


use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryAddImageCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\PhoneGalleryAddImageRequest;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryAddImagePresenter;

/**
 * Class AddImage
 * @package App\Presentation\Http\Controllers\Backend\Phones\Phone\Gallery
 */
final class AddImage extends Controller
{
    /**
     * @var PhoneGalleryAddImageCase
     */
    private $case;

    /**
     * @var PhoneGalleryAddImagePresenter
     */
    private $preseter;

    /**
     * AddImage constructor.
     * @param PhoneGalleryAddImageCase $case
     * @param PhoneGalleryAddImagePresenter $preseter
     */
    public function __construct(PhoneGalleryAddImageCase $case, PhoneGalleryAddImagePresenter $preseter)
    {
        $this->case = $case;
        $this->preseter = $preseter;
    }

    /**
     * @param PhoneGalleryAddImageRequest $request
     * @param $phone
     * @return mixed
     */
    public function __invoke(PhoneGalleryAddImageRequest $request, $phone)
    {
        $response = $this->case->execute($request->file('file'), $phone);

        return $this->preseter->present($response);
    }
}
