<?php


namespace App\Presentation\Http\Controllers\Backend\Phones\Phone;


use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PhoneViewPresenter;

/**
 * Class PhoneView
 * @package App\Presentation\Http\Controllers\Backend\Phones\Phone
 */
final class PhoneView extends Controller
{
    /**
     * @var PhoneViewCase
     */
    private $case;

    /**
     * @var PhoneViewPresenter
     */
    private $presenter;

    /**
     * PhoneView constructor.
     * @param PhoneViewCase $case
     * @param PhoneViewPresenter $presenter
     */
    public function __construct(PhoneViewCase $case, PhoneViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $phone
     * @return mixed
     */
    public function __invoke(int $phone)
    {
        $response = $this->case->execute($phone);

        return $this->presenter->present($response);
    }
}
