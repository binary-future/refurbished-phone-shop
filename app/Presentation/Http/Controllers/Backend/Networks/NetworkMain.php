<?php

namespace App\Presentation\Http\Controllers\Backend\Networks;

use App\Application\UseCases\Backend\Networks\Contracts\NetworksMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\NetworksMainPresenter;

/**
 * Class NetworkMain
 * @package App\Presentation\Http\Controllers\Backend\Stores
 */
final class NetworkMain extends Controller
{
    /**
     * @var NetworksMainCase
     */
    private $case;

    /**
     * @var NetworksMainPresenter
     */
    private $presenter;

    /**
     * NetworkMain constructor.
     * @param NetworksMainCase $case
     * @param NetworksMainPresenter $presenter
     */
    public function __construct(NetworksMainCase $case, NetworksMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
