<?php

namespace App\Presentation\Http\Controllers\Backend\Networks;

use App\Application\UseCases\Backend\Networks\Contracts\NetworkEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\NetworksEditPresenter;

final class NetworkEdit extends Controller
{
    /**
     * @var NetworkEditCase
     */
    private $case;

    /**
     * @var NetworksEditPresenter
     */
    private $presenter;

    /**
     * NetworkEdit constructor.
     * @param NetworkEditCase $case
     * @param NetworksEditPresenter $presenter
     */
    public function __construct(NetworkEditCase $case, NetworksEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $network)
    {
        $response = $this->case->execute($network);

        return $this->presenter->present($response);
    }
}
