<?php

namespace App\Presentation\Http\Controllers\Backend\Networks;

use App\Application\UseCases\Backend\Networks\Contracts\NetworksUpdateCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\NetworkUpdateRequest;
use App\Presentation\Presenters\Contracts\Backend\NetworksSavePresenter;

/**
 * Class NetworkUpdate
 * @package App\Presentation\Http\Controllers\Backend\Stores
 */
final class NetworkUpdate extends Controller
{
    /**
     * @var NetworksUpdateCase
     */
    private $case;

    /**
     * @var NetworksSavePresenter
     */
    private $presenter;

    /**
     * NetworkUpdate constructor.
     * @param NetworksUpdateCase $case
     * @param NetworksSavePresenter $presenter
     */
    public function __construct(NetworksUpdateCase $case, NetworksSavePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param NetworkUpdateRequest $request
     * @param string $network
     * @return mixed
     */
    public function __invoke(NetworkUpdateRequest $request, string $network)
    {
        $response = $this->case->execute($network, $request->all());

        return $this->presenter->present($response);
    }
}
