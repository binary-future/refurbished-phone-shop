<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Map\Color;


use App\Application\UseCases\Backend\Merge\Map\Color\Contracts\MapViewCase;
use App\Presentation\Presenters\Contracts\Backend\ColorMapViewPresenter;

final class View
{
    /**
     * @var MapViewCase
     */
    private $case;

    /**
     * @var ColorMapViewPresenter
     */
    private $presenter;

    /**
     * View constructor.
     * @param MapViewCase $case
     * @param ColorMapViewPresenter $presenter
     */
    public function __construct(MapViewCase $case, ColorMapViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
