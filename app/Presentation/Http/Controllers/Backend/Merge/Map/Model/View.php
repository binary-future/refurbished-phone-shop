<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Map\Model;


use App\Application\UseCases\Backend\Merge\Map\Model\Contracts\MapViewCase;
use App\Presentation\Presenters\Contracts\Backend\ModelMapViewPresenter;

final class View
{
    /**
     * @var MapViewCase
     */
    private $case;

    /**
     * @var ModelMapViewPresenter
     */
    private $presenter;

    /**
     * View constructor.
     * @param MapViewCase $case
     * @param ModelMapViewPresenter $presenter
     */
    public function __construct(MapViewCase $case, ModelMapViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
