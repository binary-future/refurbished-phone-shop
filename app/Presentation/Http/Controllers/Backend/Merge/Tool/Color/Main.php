<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Color;

use App\Application\UseCases\Backend\Merge\Tool\Color\Contracts\ToolMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorMainPresenter;

final class Main extends Controller
{
    /**
     * @var ToolMainCase
     */
    private $case;

    /**
     * @var MergeToolColorMainPresenter
     */
    private $presenter;


    /**
     * Main constructor.
     * @param ToolMainCase $case

     */
    public function __construct(ToolMainCase $case, MergeToolColorMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}