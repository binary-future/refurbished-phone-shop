<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Color\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolColorFilterDeleteAllCase;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterDeleteAllPresenter;

final class DeleteAll
{
    /**
     * @var ToolColorFilterDeleteAllCase
     */
    private $case;

    /**
     * @var MergeToolColorFilterDeleteAllPresenter
     */
    private $presenter;

    /**
     * DeleteAll constructor.
     * @param ToolColorFilterDeleteAllCase $case
     * @param MergeToolColorFilterDeleteAllPresenter $presenter
     */
    public function __construct(ToolColorFilterDeleteAllCase $case, MergeToolColorFilterDeleteAllPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
