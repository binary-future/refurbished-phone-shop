<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Color\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolColorFilterSaveCase;
use App\Presentation\Http\Requests\Backend\ColorFilterSaveRequest;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Requests\ColorFilterSaveRequest as CaseRequest;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

final class Save
{
    /**
     * @var ToolColorFilterSaveCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var MergeToolColorFilterSavePresenter
     */
    private $presenter;

    /**
     * Save constructor.
     * @param ToolColorFilterSaveCase $case
     * @param Serializer $serializer
     * @param MergeToolColorFilterSavePresenter $presenter
     */
    public function __construct(
        ToolColorFilterSaveCase $case,
        Serializer $serializer,
        MergeToolColorFilterSavePresenter $presenter
    ) {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    public function __invoke(ColorFilterSaveRequest $request)
    {
        $caseRequest = $this->generateCaseResponse($request);
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }

    private function generateCaseResponse(ColorFilterSaveRequest $request): CaseRequest
    {
        try {
            /**
             * @var CaseRequest $caseRequest
             */
            $caseRequest = $this->serializer->fromArray(CaseRequest::class, $request->validated());
            return $caseRequest;
        } catch (\Throwable $exception) {
            throw new \InvalidArgumentException($exception->getMessage());
        }
    }
}
