<?php

namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Color;

use App\Application\UseCases\Backend\Merge\Tool\Color\Contracts\UnmergeColorCase;
use App\Presentation\Presenters\Contracts\Backend\MergeToolUnmergeColorPresenter;

final class UnMergeColor
{
    /**
     * @var UnmergeColorCase
     */
    private $case;

    /**
     * @var MergeToolUnmergeColorPresenter
     */
    private $presenter;

    /**
     * UnMergeModel constructor.
     * @param UnmergeColorCase $case
     * @param MergeToolUnmergeColorPresenter $presenter
     */
    public function __construct(UnmergeColorCase $case, MergeToolUnmergeColorPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $color
     * @return mixed
     */
    public function __invoke(int $color)
    {
        $response = $this->case->execute($color);

        return $this->presenter->present($response);
    }
}
