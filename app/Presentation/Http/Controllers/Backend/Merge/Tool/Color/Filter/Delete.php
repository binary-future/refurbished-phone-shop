<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Color\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolColorFilterDeleteCase;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterDeletePresenter;

final class Delete
{
    /**
     * @var ToolColorFilterDeleteCase
     */
    private $case;

    /**
     * @var MergeToolColorFilterDeletePresenter
     */
    private $presenter;

    /**
     * Delete constructor.
     * @param ToolColorFilterDeleteCase $case
     * @param MergeToolColorFilterDeletePresenter $presenter
     */
    public function __construct(ToolColorFilterDeleteCase $case, MergeToolColorFilterDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $filterId)
    {
        $response = $this->case->execute($filterId);

        return $this->presenter->present($response);
    }
}
