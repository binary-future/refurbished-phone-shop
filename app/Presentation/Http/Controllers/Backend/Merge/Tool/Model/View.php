<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model;


use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\ToolViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelViewPresenter;

/**
 * Class View
 * @package App\Presentation\Http\Controllers\Backend\Merge\Tool\Model
 */
final class View extends Controller
{
    /**
     * @var ToolViewCase
     */
    private $case;

    /**
     * @var MergeToolModelViewPresenter
     */
    private $presenter;

    /**
     * View constructor.
     * @param ToolViewCase $case
     * @param MergeToolModelViewPresenter $presenter
     */
    public function __construct(ToolViewCase $case, MergeToolModelViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @return mixed
     */
    public function __invoke(string $brand)
    {
        $response = $this->case->execute($brand);

        return $this->presenter->present($response);
    }
}
