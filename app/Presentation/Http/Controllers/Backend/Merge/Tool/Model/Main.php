<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model;


use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\ToolMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelMainPresenter;

final class Main extends Controller
{
    /**
     * @var ToolMainCase
     */
    private $case;

    /**
     * @var MergeToolModelMainPresenter
     */
    private $presenter;

    /**
     * Main constructor.
     * @param ToolMainCase $case
     * @param MergeToolModelMainPresenter $presenter
     */
    public function __construct(ToolMainCase $case, MergeToolModelMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}