<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterDeleteAllCase;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeleteAllPresenter;

final class DeleteAll
{
    /**
     * @var ToolModelFilterDeleteAllCase
     */
    private $case;

    /**
     * @var MergeToolModelFilterDeleteAllPresenter
     */
    private $presenter;

    /**
     * DeleteAll constructor.
     * @param ToolModelFilterDeleteAllCase $case
     * @param MergeToolModelFilterDeleteAllPresenter $presenter
     */
    public function __construct(ToolModelFilterDeleteAllCase $case, MergeToolModelFilterDeleteAllPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
