<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterDeleteByBrandCase;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelFilterDeleteByBrandPresenter;

/**
 * Class DeleteByBrand
 * @package App\Presentation\Http\Controllers\Backend\Merge\Tool\Model\Filter
 */
final class DeleteByBrand
{
    /**
     * @var ToolModelFilterDeleteByBrandCase
     */
    private $case;

    /**
     * @var WebMergeToolModelFilterDeleteByBrandPresenter
     */
    private $presenter;

    /**
     * DeleteByBrand constructor.
     * @param ToolModelFilterDeleteByBrandCase $case
     * @param WebMergeToolModelFilterDeleteByBrandPresenter $presenter
     */
    public function __construct(
        ToolModelFilterDeleteByBrandCase $case,
        WebMergeToolModelFilterDeleteByBrandPresenter $presenter
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $brand
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function __invoke(string $brand)
    {
        $response = $this->case->execute($brand);

        return $this->presenter->present($response);
    }
}
