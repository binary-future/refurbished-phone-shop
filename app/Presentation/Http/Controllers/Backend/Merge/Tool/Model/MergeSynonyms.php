<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model;


use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\MergeSynonymsCase;
use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\MergeSynonymsResponse;
use App\Presentation\Http\Requests\Backend\SynonymsMergeHttpRequest;
use App\Presentation\Presenters\Contracts\Backend\MergeToolMergeSynonymsPresenter;
use App\Presentation\Services\Request\Preprocessors\Contracts\MergeSynonymsRequestPreprocessor;

final class MergeSynonyms
{
    /**
     * @var MergeSynonymsCase
     */
    private $case;

    /**
     * @var MergeSynonymsRequestPreprocessor
     */
    private $preprocessor;

    /**
     * @var MergeToolMergeSynonymsPresenter
     */
    private $presenter;

    /**
     * MergeSynonyms constructor.
     * @param MergeSynonymsCase $case
     * @param MergeSynonymsRequestPreprocessor $preprocessor
     * @param MergeToolMergeSynonymsPresenter $presenter
     */
    public function __construct(
        MergeSynonymsCase $case,
        MergeSynonymsRequestPreprocessor $preprocessor,
        MergeToolMergeSynonymsPresenter $presenter
    ) {
        $this->case = $case;
        $this->preprocessor = $preprocessor;
        $this->presenter = $presenter;
    }

    public function __invoke(SynonymsMergeHttpRequest $request)
    {
        $response = $this->makeRequest($request);

        return $this->presenter->present($response);
    }

    private function makeRequest(SynonymsMergeHttpRequest $request)
    {
        try {
            $caseRequest = $this->preprocessor->process($request->validated());
            return $this->case->execute($caseRequest);
        } catch (\Throwable $exception) {
            return new MergeSynonymsResponse(false);
        }
    }
}
