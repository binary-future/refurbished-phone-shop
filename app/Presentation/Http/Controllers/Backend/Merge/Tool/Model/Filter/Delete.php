<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterDeleteCase;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeletePresenter;

final class Delete
{
    /**
     * @var ToolModelFilterDeleteCase
     */
    private $case;

    /**
     * @var MergeToolModelFilterDeletePresenter
     */
    private $presenter;

    /**
     * Delete constructor.
     * @param ToolModelFilterDeleteCase $case
     * @param MergeToolModelFilterDeletePresenter $presenter
     */
    public function __construct(ToolModelFilterDeleteCase $case, MergeToolModelFilterDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $filterId)
    {
        $response = $this->case->execute($filterId);

        return $this->presenter->present($response);
    }
}
