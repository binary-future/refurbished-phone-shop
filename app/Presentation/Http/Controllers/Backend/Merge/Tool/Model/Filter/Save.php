<?php


namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model\Filter;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterSaveCase;
use App\Presentation\Http\Requests\Backend\ModelFilterSaveRequest;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Requests\ModelFilterSaveRequest as CaseRequest;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

final class Save
{
    /**
     * @var ToolModelFilterSaveCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var MergeToolModelFilterSavePresenter
     */
    private $presenter;

    /**
     * Save constructor.
     * @param ToolModelFilterSaveCase $case
     * @param Serializer $serializer
     * @param MergeToolModelFilterSavePresenter $presenter
     */
    public function __construct(
        ToolModelFilterSaveCase $case,
        Serializer $serializer,
        MergeToolModelFilterSavePresenter $presenter
    ) {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    public function __invoke(ModelFilterSaveRequest $request)
    {
        $caseRequest = $this->generateCaseRequest($request);
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }

    private function generateCaseRequest(ModelFilterSaveRequest $request): CaseRequest
    {
        try {
            /**
             * @var CaseRequest $caseRequest
             */
            $caseRequest = $this->serializer->fromArray(CaseRequest::class, $request->validated());
            return $caseRequest;
        } catch (\Throwable $exception) {
            throw new \InvalidArgumentException($exception->getMessage());
        }
    }
}
