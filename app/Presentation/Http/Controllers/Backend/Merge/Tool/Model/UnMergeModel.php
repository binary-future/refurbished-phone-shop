<?php

namespace App\Presentation\Http\Controllers\Backend\Merge\Tool\Model;

use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\UnmergeModelCase;
use App\Presentation\Presenters\Contracts\Backend\MergeToolUnmergeModelPresenter;

final class UnMergeModel
{
    /**
     * @var UnmergeModelCase
     */
    private $case;

    /**
     * @var MergeToolUnmergeModelPresenter
     */
    private $presenter;

    /**
     * UnMergeModel constructor.
     * @param UnmergeModelCase $case
     * @param MergeToolUnmergeModelPresenter $presenter
     */
    public function __construct(UnmergeModelCase $case, MergeToolUnmergeModelPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $model
     * @return mixed
     */
    public function __invoke(int $model)
    {
        $response = $this->case->execute($model);

        return $this->presenter->present($response);
    }
}
