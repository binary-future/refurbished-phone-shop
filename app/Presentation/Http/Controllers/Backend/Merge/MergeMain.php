<?php

namespace App\Presentation\Http\Controllers\Backend\Merge;

use App\Application\UseCases\Backend\Merge\Main\Contracts\MergeMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\MergeMainPresenter;

/**
 * Class MergeMain
 * @package App\Presentation\Http\Controllers\Backend\Merge
 */
final class MergeMain extends Controller
{
    /**
     * @var MergeMainCase
     */
    private $case;

    /**
     * @var MergeMainPresenter
     */
    private $presenter;

    /**
     * MergeMain constructor.
     * @param MergeMainCase $case
     * @param MergeMainPresenter $presenter
     */
    public function __construct(MergeMainCase $case, MergeMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
