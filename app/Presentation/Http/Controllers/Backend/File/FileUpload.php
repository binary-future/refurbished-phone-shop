<?php

namespace App\Presentation\Http\Controllers\Backend\File;

use App\Application\UseCases\Backend\Main\Contracts\FileUploadCase;
use App\Application\UseCases\Backend\Main\Request\FileUploadRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\FileUploadFormRequest;
use App\Presentation\Presenters\Contracts\Backend\FileUploadPresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class FileUpload
 * @package App\Presentation\Http\Controllers\Backend\File
 */
final class FileUpload extends Controller
{
    /**
     * @var FileUploadCase
     */
    private $case;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var FileUploadPresenter
     */
    private $presenter;

    /**
     * FileUpload constructor.
     * @param FileUploadCase $case
     * @param Serializer $serializer
     * @param FileUploadPresenter $presenter
     */
    public function __construct(FileUploadCase $case, Serializer $serializer, FileUploadPresenter $presenter)
    {
        $this->case = $case;
        $this->serializer = $serializer;
        $this->presenter = $presenter;
    }

    /**
     * @param FileUploadFormRequest $formRequest
     * @return mixed
     */
    public function __invoke(FileUploadFormRequest $formRequest)
    {
        /**
         * @var FileUploadRequest $request
         */
        $request = $this->serializer->fromArray(FileUploadRequest::class, $formRequest->validated());
        $response = $this->case->execute($request);

        return $this->presenter->present($response);
    }
}
