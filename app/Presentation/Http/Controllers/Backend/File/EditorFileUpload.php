<?php


namespace App\Presentation\Http\Controllers\Backend\File;


use App\Application\UseCases\Backend\Main\Contracts\FileUploadEditorCase;
use App\Application\UseCases\Backend\Main\Responses\FileUploadEditorResult;
use App\Presentation\Http\Requests\Backend\FileUploadEditorFormRequest;
use App\Presentation\Presenters\Contracts\Backend\FileUploadEditorPresenter;

/**
 * Class EditorFileUpload
 * @package App\Presentation\Http\Controllers\Backend\File
 */
final class EditorFileUpload
{
    /**
     * @var FileUploadEditorCase
     */
    private $case;

    /**
     * @var FileUploadEditorPresenter
     */
    private $presenter;

    /**
     * EditorFileUpload constructor.
     * @param FileUploadEditorCase $case
     * @param FileUploadEditorPresenter $presenter
     */
    public function __construct(FileUploadEditorCase $case, FileUploadEditorPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param FileUploadEditorFormRequest $request
     * @return mixed
     */
    public function __invoke(FileUploadEditorFormRequest $request)
    {
        $response = $request->file('file')
            ? $this->case->execute($request->file('file'))
            : new FileUploadEditorResult('', false);

        return $this->presenter->present($response);
    }
}