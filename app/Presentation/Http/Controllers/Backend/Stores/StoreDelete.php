<?php


namespace App\Presentation\Http\Controllers\Backend\Stores;


use App\Application\UseCases\Backend\Stores\Contracts\StoreDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\StoreDeletePresenter;

final class StoreDelete extends Controller
{
    /**
     * @var StoreDeleteCase
     */
    private $case;

    /**
     * @var StoreDeletePresenter
     */
    private $presenter;

    /**
     * StoreDelete constructor.
     * @param StoreDeleteCase $case
     * @param StoreDeletePresenter $presenter
     */
    public function __construct(StoreDeleteCase $case, StoreDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @return mixed
     */
    public function __invoke(string $store)
    {
        $response = $this->case->execute($store);

        return $this->presenter->present($response);
    }
}
