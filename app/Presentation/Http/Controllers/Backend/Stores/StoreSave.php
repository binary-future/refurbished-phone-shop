<?php

namespace App\Presentation\Http\Controllers\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Contracts\StoreSaveCase;
use App\Application\UseCases\Backend\Stores\Requests\CreateStoreRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\StoreCreateRequest;
use App\Presentation\Presenters\Contracts\Backend\StoreSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class StoreSave
 * @package App\Presentation\Http\Controllers\Backend\Stores
 */
final class StoreSave extends Controller
{
    /**
     * @var StoreSaveCase $case
     */
    private $case;

    /**
     * @var StoreSavePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * StoreSave constructor.
     * @param StoreSaveCase $case
     * @param StoreSavePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(StoreSaveCase $case, StoreSavePresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param StoreCreateRequest $request
     * @return mixed
     */
    public function __invoke(StoreCreateRequest $request)
    {
        /**
         * @var CreateStoreRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(CreateStoreRequest::class, $request->validated());
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
