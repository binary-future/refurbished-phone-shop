<?php

namespace App\Presentation\Http\Controllers\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Contracts\StoreCreateCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\StoreCreatePresenter;

final class StoreCreate extends Controller
{
    /**
     * @var StoreCreateCase
     */
    private $case;

    /**
     * @var StoreCreatePresenter
     */
    private $presenter;

    /**
     * StoreCreate constructor.
     * @param StoreCreateCase $case
     * @param StoreCreatePresenter $presenter
     */
    public function __construct(StoreCreateCase $case, StoreCreatePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }

}