<?php

namespace App\Presentation\Http\Controllers\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Contracts\StoreUpdateCase;
use App\Application\UseCases\Backend\Stores\Requests\UpdateStoreRequest;
use App\Domain\Shared\Rating\Rating;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\StoreUpdateRequest;
use App\Presentation\Presenters\Contracts\Backend\StoreSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class StoreUpdate
 * @package App\Presentation\Http\Controllers\Backend\Stores
 */
final class StoreUpdate extends Controller
{
    /**
     * @var StoreUpdateCase
     */
    private $case;

    /**
     * @var StoreSavePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * StoreUpdate constructor.
     * @param StoreUpdateCase $case
     * @param StoreSavePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(StoreUpdateCase $case, StoreSavePresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param StoreUpdateRequest $request
     * @param string $store
     * @return mixed
     */
    public function __invoke(StoreUpdateRequest $request, string $store)
    {
        /**
         * @var UpdateStoreRequest $caseRequest
         */
        $data = $this->prepareData($request->validated());
        $caseRequest = $this->serializer->fromArray(UpdateStoreRequest::class, $data);
        $response = $this->case->execute($store, $caseRequest);

        return $this->presenter->present($response);
    }

    private function prepareData(array $validated): array
    {
        $validated[Rating::TABLE][Rating::FIELD_RATING] =
            translate_rating($validated[Rating::TABLE][Rating::FIELD_RATING], false);

        return $validated;
    }
}
