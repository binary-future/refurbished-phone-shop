<?php

namespace App\Presentation\Http\Controllers\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Contracts\StoreMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\StoresMainPresenter;

/**
 * Class StoreMain
 * @package App\Presentation\Http\Controllers\Backend\Stores
 */
final class StoreMain extends Controller
{
    /**
     * @var StoreMainCase
     */
    private $case;

    /**
     * @var StoresMainPresenter
     */
    private $presenter;

    /**
     * StoreMain constructor.
     * @param StoreMainCase $case
     * @param StoresMainPresenter $presenter
     */
    public function __construct(StoreMainCase $case, StoresMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
