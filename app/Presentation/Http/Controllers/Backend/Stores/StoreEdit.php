<?php


namespace App\Presentation\Http\Controllers\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Contracts\StoreEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\StoresEditPresenter;

final class StoreEdit extends Controller
{
    /**
     * @var StoreEditCase
     */
    private $case;

    /**
     * @var StoresEditPresenter
     */
    private $presenter;

    /**
     * StoreEdit constructor.
     * @param StoreEditCase $case
     * @param StoresEditPresenter $presenter
     */
    public function __construct(StoreEditCase $case, StoresEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $slug)
    {
        $response = $this->case->execute($slug);

        return $this->presenter->present($response);
    }
}
