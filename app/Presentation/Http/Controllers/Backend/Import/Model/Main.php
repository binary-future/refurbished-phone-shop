<?php

namespace App\Presentation\Http\Controllers\Backend\Import\Model;

use App\Application\UseCases\Backend\Import\Main\Contracts\MainImportCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\MainImportPagePresenter;

/**
 * Class Main
 * @package App\Presentation\Http\Controllers\Backend\Import\Model
 */
final class Main extends Controller
{
    /**
     * @var MainImportCase
     */
    private $case;

    /**
     * @var MainImportPagePresenter
     */
    private $presenter;

    /**
     * Main constructor.
     * @param MainImportCase $case
     * @param MainImportPagePresenter $presenter
     */
    public function __construct(MainImportCase $case, MainImportPagePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
