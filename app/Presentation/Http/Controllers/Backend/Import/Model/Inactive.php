<?php


namespace App\Presentation\Http\Controllers\Backend\Import\Model;


use App\Application\UseCases\Backend\Import\Model\Contracts\InactiveListCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\InactiveModelListPresenter;

final class Inactive extends Controller
{
    /**
     * @var InactiveListCase
     */
    private $case;

    /**
     * @var InactiveModelListPresenter
     */
    private $presenter;

    /**
     * Inactive constructor.
     * @param InactiveListCase $case
     * @param InactiveModelListPresenter $presenter
     */
    public function __construct(InactiveListCase $case, InactiveModelListPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}