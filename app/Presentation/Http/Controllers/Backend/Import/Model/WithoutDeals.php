<?php

namespace App\Presentation\Http\Controllers\Backend\Import\Model;

use App\Application\UseCases\Backend\Import\Model\Contracts\WithoutDealsCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ModelsWithoutDealsPresenter;

/**
 * Class WithoutDeals
 * @package App\Presentation\Http\Controllers\Backend\Import\Model
 */
final class WithoutDeals extends Controller
{
    /**
     * @var WithoutDealsCase
     */
    private $case;

    /**
     * @var ModelsWithoutDealsPresenter
     */
    private $presenter;

    /**
     * WithoutDeals constructor.
     * @param WithoutDealsCase $case
     * @param ModelsWithoutDealsPresenter $presenter
     */
    public function __construct(WithoutDealsCase $case, ModelsWithoutDealsPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->presenter($response);
    }
}
