<?php


namespace App\Presentation\Http\Controllers\Backend\Import\PlannedImport;


use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedSaveCase;
use App\Application\UseCases\Backend\Import\Planned\Requests\CreatePlan;
use App\Presentation\Http\Requests\Backend\PlannedImportCreateRequest;
use App\Presentation\Presenters\Contracts\Backend\PlannedSavePresenter;

/**
 * Class PlanSave
 * @package App\Presentation\Http\Controllers\Backend\Import\PlannedImport
 */
final class PlanSave
{
    /**
     * @var PlannedSaveCase
     */
    private $case;

    /**
     * @var PlannedSavePresenter
     */
    private $presenter;

    /**
     * PlanSave constructor.
     * @param PlannedSaveCase $case
     * @param PlannedSavePresenter $presenter
     */
    public function __construct(PlannedSaveCase $case, PlannedSavePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param PlannedImportCreateRequest $request
     * @return mixed
     */
    public function __invoke(PlannedImportCreateRequest $request)
    {
        $response = $this->case->execute(new CreatePlan($request->input('date')));

        return $this->presenter->present($response);
    }
}
