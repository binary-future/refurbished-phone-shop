<?php


namespace App\Presentation\Http\Controllers\Backend\Import\PlannedImport;


use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedUpdateCase;
use App\Application\UseCases\Backend\Import\Planned\Requests\UpdatePlan;
use App\Presentation\Http\Requests\Backend\PlannedImportUpdateRequest;
use App\Presentation\Presenters\Contracts\Backend\PlannedSavePresenter;

/**
 * Class PlanUpdate
 * @package App\Presentation\Http\Controllers\Backend\Import\PlannedImport
 */
final class PlanUpdate
{
    /**
     * @var PlannedUpdateCase
     */
    private $case;

    /**
     * @var PlannedSavePresenter
     */
    private $presenter;

    /**
     * PlanUpdate constructor.
     * @param PlannedUpdateCase $case
     * @param PlannedSavePresenter $presenter
     */
    public function __construct(PlannedUpdateCase $case, PlannedSavePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param PlannedImportUpdateRequest $request
     * @param int $plan
     * @return mixed
     */
    public function __invoke(PlannedImportUpdateRequest $request, int $plan)
    {
        $response = $this->case->execute(new UpdatePlan($plan, $request->get('date')));

        return $this->presenter->present($response);
    }
}
