<?php


namespace App\Presentation\Http\Controllers\Backend\Import\PlannedImport;


use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedMainCase;
use App\Presentation\Presenters\Contracts\Backend\PlannedImportsMainPresenter;

final class PlanList
{
    /**
     * @var PlannedMainCase
     */
    private $case;

    /**
     * @var PlannedImportsMainPresenter
     */
    private $presenter;

    /**
     * PlanList constructor.
     * @param PlannedMainCase $case
     * @param PlannedImportsMainPresenter $presenter
     */
    public function __construct(PlannedMainCase $case, PlannedImportsMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
