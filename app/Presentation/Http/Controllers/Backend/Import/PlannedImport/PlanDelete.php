<?php


namespace App\Presentation\Http\Controllers\Backend\Import\PlannedImport;


use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedDeleteCase;
use App\Presentation\Presenters\Contracts\Backend\PlannedDeletePresenter;

/**
 * Class PlanDelete
 * @package App\Presentation\Http\Controllers\Backend\Import\PlannedImport
 */
final class PlanDelete
{
    /**
     * @var PlannedDeleteCase
     */
    private $case;

    /**
     * @var PlannedDeletePresenter
     */
    private $presenter;

    /**
     * PlanDelete constructor.
     * @param PlannedDeleteCase $case
     * @param PlannedDeletePresenter $presenter
     */
    public function __construct(PlannedDeleteCase $case, PlannedDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $plan
     * @return mixed
     */
    public function __invoke(int $plan)
    {
        $response = $this->case->execute($plan);

        return $this->presenter->present($response);
    }
}
