<?php

namespace App\Presentation\Http\Controllers\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Contracts\UpdateInfoCase;
use App\Application\UseCases\Backend\User\Profile\Contracts\UpdatePasswordCase;
use App\Application\UseCases\Backend\User\Profile\Requests\UpdateInfoRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\ProfileUpdatePasswordRequest;
use App\Presentation\Presenters\Contracts\Backend\ProfileUpdatePresenter;
use App\Utils\Serializer\Contracts\Serializer;

final class ProfileUpdatePassword extends Controller
{
    /**
     * @var UpdateInfoCase
     */
    private $case;

    /**
     * @var ProfileUpdatePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ProfileUpdateInfo constructor.
     * @param UpdatePasswordCase $case
     * @param ProfileUpdatePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(UpdatePasswordCase $case, ProfileUpdatePresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param ProfileUpdatePasswordRequest $request
     * @return mixed
     */
    public function __invoke(ProfileUpdatePasswordRequest $request)
    {
        /**
         * @var UpdateInfoRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(UpdateInfoRequest::class, $request->validated());
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}