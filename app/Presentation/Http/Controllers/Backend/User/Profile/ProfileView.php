<?php

namespace App\Presentation\Http\Controllers\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Contracts\ProfileViewCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\ProfileViewPresenter;

/**
 * Class ProfileView
 * @package App\Presentation\Http\Controllers\Backend\User\Profile
 */
final class ProfileView extends Controller
{
    /**
     * @var ProfileViewCase
     */
    private $case;

    /**
     * @var ProfileViewPresenter
     */
    private $presenter;

    /**
     * ProfileView constructor.
     * @param ProfileViewCase $case
     * @param ProfileViewPresenter $presenter
     */
    public function __construct(ProfileViewCase $case, ProfileViewPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
