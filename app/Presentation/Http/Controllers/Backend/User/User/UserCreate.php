<?php

namespace App\Presentation\Http\Controllers\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Contracts\UserCreateCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\UserCreatePresenter;

/**
 * Class UserCreate
 * @package App\Presentation\Http\Controllers\Backend\User\User
 */
final class UserCreate extends Controller
{
    /**
     * @var UserCreateCase
     */
    private $case;

    /**
     * @var UserCreatePresenter
     */
    private $presenter;

    /**
     * UserCreate constructor.
     * @param UserCreateCase $case
     * @param UserCreatePresenter $presenter
     */
    public function __construct(UserCreateCase $case, UserCreatePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
