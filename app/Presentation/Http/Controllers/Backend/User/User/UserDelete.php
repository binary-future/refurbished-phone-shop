<?php

namespace App\Presentation\Http\Controllers\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Contracts\UserDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\UserDeletePresenter;

/**
 * Class UserDelete
 * @package App\Presentation\Http\Controllers\Backend\User\User
 */
final class UserDelete extends Controller
{
    /**
     * @var UserDeleteCase
     */
    private $case;

    /**
     * @var UserDeletePresenter
     */
    private $presenter;

    /**
     * UserDelete constructor.
     * @param UserDeleteCase $case
     * @param UserDeletePresenter $presenter
     */
    public function __construct(UserDeleteCase $case, UserDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param int $user
     * @return mixed
     */
    public function __invoke(int $user)
    {
        $response = $this->case->execute($user);

        return $this->presenter->present($response);
    }
}
