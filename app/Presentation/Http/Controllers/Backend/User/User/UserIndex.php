<?php

namespace App\Presentation\Http\Controllers\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Contracts\UserListCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\UserListPresenter;
use Illuminate\Http\Request;

final class UserIndex extends Controller
{
    /**
     * @var UserListCase
     */
    private $case;

    /**
     * @var UserListPresenter
     */
    private $presenter;

    /**
     * UserIndex constructor.
     * @param UserListCase $case
     * @param UserListPresenter $presenter
     */
    public function __construct(UserListCase $case, UserListPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }

}