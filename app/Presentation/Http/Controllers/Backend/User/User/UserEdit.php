<?php

namespace App\Presentation\Http\Controllers\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Contracts\UserEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\UserEditPresenter;

final class UserEdit extends Controller
{
    /**
     * @var UserEditCase
     */
    private $case;

    /**
     * @var UserEditPresenter
     */
    private $presenter;

    /**
     * UserEdit constructor.
     * @param UserEditCase $case
     * @param UserEditPresenter $presenter
     */
    public function __construct(UserEditCase $case, UserEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }


    public function __invoke(int $user)
    {
        $response = $this->case->execute($user);

        return $this->presenter->present($response);
    }
}