<?php

namespace App\Presentation\Http\Controllers\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Contracts\UserUpdateCase;
use App\Application\UseCases\Backend\User\User\Request\UserUpdateRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\UserUpdateFormRequest;
use App\Presentation\Presenters\Contracts\Backend\UserUpdatePresenter;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class UserUpdate
 * @package App\Presentation\Http\Controllers\Backend\User\User
 */
class UserUpdate extends Controller
{
    /**
     * @var UserUpdateCase
     */
    private $case;

    /**
     * @var UserUpdatePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * UserUpdate constructor.
     * @param UserUpdateCase $case
     * @param UserUpdatePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(UserUpdateCase $case, UserUpdatePresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    /**
     * @param UserUpdateFormRequest $request
     * @param int $user
     * @return mixed
     */
    public function __invoke(UserUpdateFormRequest $request, int $user)
    {
        /**
         * @var UserUpdateRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(UserUpdateRequest::class, $request->validated());
        $caseRequest->setUserId($user);
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
