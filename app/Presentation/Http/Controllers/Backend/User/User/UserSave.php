<?php

namespace App\Presentation\Http\Controllers\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Contracts\UserSaveCase;
use App\Application\UseCases\Backend\User\User\Request\UserSaveRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\UserCreateFormRequest;
use App\Presentation\Presenters\Contracts\Backend\UserSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

final class UserSave extends Controller
{
    /**
     * @var UserSaveCase
     */
    private $case;

    /**
     * @var UserSavePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * UserSave constructor.
     * @param UserSaveCase $case
     * @param UserSavePresenter $presenter
     * @param Serializer $serializer
     */
    public function __construct(UserSaveCase $case, UserSavePresenter $presenter, Serializer $serializer)
    {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    public function __invoke(UserCreateFormRequest $request)
    {
        /**
         * @var UserSaveRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(UserSaveRequest::class, $request->validated());
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}