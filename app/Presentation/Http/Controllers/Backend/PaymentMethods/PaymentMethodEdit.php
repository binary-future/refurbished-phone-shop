<?php

namespace App\Presentation\Http\Controllers\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodEditCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodEditPresenter;

final class PaymentMethodEdit extends Controller
{
    /**
     * @var PaymentMethodEditCase
     */
    private $case;

    /**
     * @var PaymentMethodEditPresenter
     */
    private $presenter;

    public function __construct(PaymentMethodEditCase $case, PaymentMethodEditPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(string $slug)
    {
        $response = $this->case->execute($slug);

        return $this->presenter->present($response);
    }
}
