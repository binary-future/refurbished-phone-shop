<?php

namespace App\Presentation\Http\Controllers\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodSaveCase;
use App\Application\UseCases\Backend\PaymentMethods\Requests\SavePaymentMethodRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\PaymentMethods\PaymentMethodSaveRequest;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

final class PaymentMethodSave extends Controller
{
    /**
     * @var PaymentMethodSaveCase $case
     */
    private $case;

    /**
     * @var PaymentMethodSavePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        PaymentMethodSaveCase $case,
        PaymentMethodSavePresenter $presenter,
        Serializer $serializer
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    public function __invoke(PaymentMethodSaveRequest $request)
    {
        /**
         * @var SavePaymentMethodRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(SavePaymentMethodRequest::class, $request->validated());
        $response = $this->case->execute($caseRequest);

        return $this->presenter->present($response);
    }
}
