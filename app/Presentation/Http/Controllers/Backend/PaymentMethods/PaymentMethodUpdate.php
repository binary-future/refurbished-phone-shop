<?php

namespace App\Presentation\Http\Controllers\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodSaveCase;
use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodUpdateCase;
use App\Application\UseCases\Backend\PaymentMethods\Requests\SavePaymentMethodRequest;
use App\Application\UseCases\Backend\PaymentMethods\Requests\UpdatePaymentMethodRequest;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Http\Requests\Backend\PaymentMethods\PaymentMethodSaveRequest;
use App\Presentation\Http\Requests\Backend\PaymentMethods\PaymentMethodUpdateRequest;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodSavePresenter;
use App\Utils\Serializer\Contracts\Serializer;

final class PaymentMethodUpdate extends Controller
{
    /**
     * @var PaymentMethodUpdateCase $case
     */
    private $case;

    /**
     * @var PaymentMethodSavePresenter
     */
    private $presenter;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        PaymentMethodUpdateCase $case,
        PaymentMethodSavePresenter $presenter,
        Serializer $serializer
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
        $this->serializer = $serializer;
    }

    public function __invoke(PaymentMethodUpdateRequest $request, string $paymentMethod)
    {
        /**
         * @var UpdatePaymentMethodRequest $caseRequest
         */
        $caseRequest = $this->serializer->fromArray(UpdatePaymentMethodRequest::class, $request->validated());
        $response = $this->case->execute($paymentMethod, $caseRequest);

        return $this->presenter->present($response);
    }
}
