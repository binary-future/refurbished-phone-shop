<?php

namespace App\Presentation\Http\Controllers\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodsMainCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodsMainPresenter;

final class PaymentMethodsMain extends Controller
{
    /**
     * @var PaymentMethodsMainCase
     */
    private $case;

    /**
     * @var PaymentMethodsMainPresenter
     */
    private $presenter;

    public function __construct(PaymentMethodsMainCase $case, PaymentMethodsMainPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
