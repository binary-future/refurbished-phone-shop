<?php

namespace App\Presentation\Http\Controllers\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodDeleteCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\StoreDeletePresenter;

final class PaymentMethodDelete extends Controller
{
    /**
     * @var PaymentMethodDeleteCase
     */
    private $case;

    /**
     * @var PaymentMethodDeletePresenter
     */
    private $presenter;

    public function __construct(PaymentMethodDeleteCase $case, PaymentMethodDeletePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @param string $store
     * @return mixed
     */
    public function __invoke(string $store)
    {
        $response = $this->case->execute($store);

        return $this->presenter->present($response);
    }
}
