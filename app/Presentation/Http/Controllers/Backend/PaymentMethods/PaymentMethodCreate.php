<?php

namespace App\Presentation\Http\Controllers\Backend\PaymentMethods;

use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodCreatePresenter;

final class PaymentMethodCreate extends Controller
{
    /**
     * @var PaymentMethodCreatePresenter
     */
    private $presenter;

    public function __construct(PaymentMethodCreatePresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        return $this->presenter->present();
    }
}
