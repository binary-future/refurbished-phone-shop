<?php


namespace App\Presentation\Http\Controllers\Backend;

use App\Application\UseCases\Backend\Main\Contracts\MainPageCase;
use App\Presentation\Http\Controllers\Controller;
use App\Presentation\Presenters\Contracts\Backend\HomePagePresenter;

/**
 * Class MainPage
 * @package App\Presentation\Http\Controllers\Backend
 */
final class MainPage extends Controller
{
    /**
     * @var MainPageCase
     */
    private $case;

    /**
     * @var HomePagePresenter
     */
    private $presenter;

    /**
     * MainPage constructor.
     * @param MainPageCase $case
     * @param HomePagePresenter $presenter
     */
    public function __construct(MainPageCase $case, HomePagePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * @return mixed
     */
    public function __invoke()
    {
        $response = $this->case->execute();

        return $this->presenter->present($response);
    }
}
