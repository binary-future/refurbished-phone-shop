<?php

namespace App\Presentation\Http\Middleware;

use App\Utils\Adapters\Auth\Contracts\Auth;
use Closure;

final class IsAdminMiddleware
{
    /**
     * @var Auth
     */
    private $auth;

    /**
     * IsAdminMiddleware constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $this->auth->getAuthUser();
        if (! $user || ! $user->isAdmin()) {
            return redirect()->route('admin.models.main');
        }

        return $next($request);
    }
}
