<?php

namespace App\Presentation\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

final class UserUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => sprintf('required|string|email|max:255|unique:users,email,%s', $this->user),
            'role' => 'required|numeric'
        ];

        return $rules;
    }
}