<?php

namespace App\Presentation\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

final class ProfileUpdateInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => sprintf('required|string|email|max:255|unique:users,email,%s', auth()->user()->getKey()),
            'check_password' => 'required|string|min:6',
        ];

        return $rules;
    }
}