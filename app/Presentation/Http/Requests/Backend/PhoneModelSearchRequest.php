<?php

namespace App\Presentation\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class PhoneModelSearchRequest
 * @package App\Presentation\Http\Requests\Backend
 */
final class PhoneModelSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
        ];

        return $rules;
    }
}
