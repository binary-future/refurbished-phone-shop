<?php

namespace App\Presentation\Http\Requests\Backend;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\UseCases\Backend\Stores\Requests\UpdateStoreRequest;
use App\Domain\Store\Store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

final class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool)Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return auth()->user() && auth()->user()->isAdmin()
            ? [
                UpdateStoreRequest::INPUT_NAME => sprintf(
                    'required|max:50|unique:%s,%s,%s',
                    Store::TABLE,
                    Store::FIELD_NAME,
                    $this->get(DatafeedInfo::FIELD_STORE_ID)
                ),
                UpdateStoreRequest::INPUT_RATING_VALUE => 'required|numeric|min:0|max:5',
                UpdateStoreRequest::INPUT_RATING_URL => 'required|url',
                UpdateStoreRequest::INPUT_GUARANTEE => 'required|numeric|min:0|max:60',
                UpdateStoreRequest::INPUT_TERMS => 'required|numeric|min:0|max:60',
                UpdateStoreRequest::INPUT_LINK =>
                    'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                UpdateStoreRequest::INPUT_IS_ACTIVE => 'required|numeric',
                UpdateStoreRequest::INPUT_AFFILIATION => 'required|string',
                UpdateStoreRequest::INPUT_PAYMENT_METHODS . '.*' => 'nullable|numeric',
                UpdateStoreRequest::INPUT_LOGO =>
                    'mimetypes:image/jpeg,image/bmp,image/png,image/svg,image/svg+xml|max:1024',
                UpdateStoreRequest::INPUT_DATAFEED_API_ID => 'nullable|numeric',
                UpdateStoreRequest::INPUT_DATAFEED_EXTERNAL_ID => 'nullable|numeric',
                UpdateStoreRequest::INPUT_DESCRIPTION_CONTENT => 'nullable',
                UpdateStoreRequest::INPUT_STORE_ID => 'required|numeric',
            ] : [
                UpdateStoreRequest::INPUT_DESCRIPTION_CONTENT => 'nullable',
                UpdateStoreRequest::INPUT_STORE_ID => 'required|numeric',
            ];
    }
}
