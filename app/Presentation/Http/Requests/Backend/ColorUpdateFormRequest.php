<?php


namespace App\Presentation\Http\Requests\Backend;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ColorUpdateFormRequest
 * @package App\Presentation\Http\Requests\Backend
 */
final class ColorUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => sprintf('required|max:100|unique:colors,name,%s', $this->get('color_id')),
            'hex' => 'size:6',
        ];

        return $rules;
    }
}
