<?php


namespace App\Presentation\Http\Requests\Backend;


use App\Presentation\Http\Rules\MergeGroupsRule;
use Illuminate\Foundation\Http\FormRequest;

class SynonymsMergeHttpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'models' => ['required', new MergeGroupsRule()],
        ];

        return $rules;
    }
}
