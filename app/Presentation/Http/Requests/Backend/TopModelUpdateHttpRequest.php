<?php


namespace App\Presentation\Http\Requests\Backend;


use Illuminate\Foundation\Http\FormRequest;

final class TopModelUpdateHttpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order' => 'required|numeric',
        ];
    }
}
