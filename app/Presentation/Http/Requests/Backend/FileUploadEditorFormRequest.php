<?php


namespace App\Presentation\Http\Requests\Backend;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FileUploadRequest
 * @package App\Presentation\Http\Requests\Backend
 */
final class FileUploadEditorFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'file' => 'required|mimetypes:image/jpeg,image/bmp,image/png,image/svg,image/svg+xml,image/gif,image/webp|max:1024',
        ];

        return $rules;
    }
}
