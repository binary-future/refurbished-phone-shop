<?php


namespace App\Presentation\Http\Requests\Backend;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

final class BrandUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return auth()->user()->isAdmin()
            ? [
                'name' => sprintf('required|max:50|unique:brands,name,%s', $this->get('brand_id')),
                'logo' => 'mimetypes:image/jpeg,image/bmp,image/png,image/svg,image/svg+xml|max:1024',
                'description.content' => 'nullable',

            ] : [
                'description.content' => 'nullable',
            ];
    }
}
