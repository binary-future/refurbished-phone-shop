<?php

namespace App\Presentation\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

final class ProfileUpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'password' => 'required|string|min:6|max:50|confirmed',
            'check_password' => 'required|string|min:6|max:50',
        ];

        return $rules;
    }
}