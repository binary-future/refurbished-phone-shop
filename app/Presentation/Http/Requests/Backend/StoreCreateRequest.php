<?php

namespace App\Presentation\Http\Requests\Backend;

use App\Application\UseCases\Backend\Stores\Requests\CreateStoreRequest;
use App\Domain\Store\Store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

final class StoreCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool)Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            CreateStoreRequest::INPUT_NAME => 'required|max:50|unique:' . Store::TABLE . ',' . Store::FIELD_NAME,
            CreateStoreRequest::INPUT_GUARANTEE => 'required|numeric|min:0|max:60',
            CreateStoreRequest::INPUT_TERMS => 'required|numeric|min:0|max:60',
            CreateStoreRequest::INPUT_LINK =>
                'required|max:300|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
            CreateStoreRequest::INPUT_AFFILIATION => 'required|string',
            CreateStoreRequest::INPUT_PAYMENT_METHODS . '.*' => 'nullable|numeric',
            CreateStoreRequest::INPUT_IS_ACTIVE => 'required|numeric',
            CreateStoreRequest::INPUT_LOGO => 'mimetypes:image/jpeg,image/png,image/svg,image/svg+xml|max:1024',
            CreateStoreRequest::INPUT_DATAFEED_API_ID => 'nullable|numeric',
            CreateStoreRequest::INPUT_DATAFEED_EXTERNAL_ID => 'nullable|numeric',
            CreateStoreRequest::INPUT_DESCRIPTION_CONTENT => 'nullable',
        ];
    }
}
