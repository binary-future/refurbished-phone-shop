<?php


namespace App\Presentation\Http\Requests\Backend;


use App\Presentation\Http\Rules\UniqueMergeColorFilterRule;
use Illuminate\Foundation\Http\FormRequest;

final class ColorFilterSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param UniqueMergeColorFilterRule $rule
     * @return array
     */
    public function rules(UniqueMergeColorFilterRule $rule)
    {
        $rule->setRequest($this);
        $rules = [
            'name' => ['required', 'string', 'max:30', $rule],
            'filter_id' => 'nullable|numeric',
            'parent_id' => 'nullable|numeric',
        ];

        return $rules;
    }
}
