<?php


namespace App\Presentation\Http\Requests\Backend;


use App\Application\Services\Importer\Plan\PlannedImport;
use Illuminate\Foundation\Http\FormRequest;

final class PlannedImportCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user() && auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $format = PlannedImport::DATE_FORMAT;
        $rules = [
            'date' => "required|date|date_format:$format|after:now|unique:planned_imports,import_date",
        ];

        return $rules;
    }
}
