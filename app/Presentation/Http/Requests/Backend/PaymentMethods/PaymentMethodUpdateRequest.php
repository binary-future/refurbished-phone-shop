<?php

namespace App\Presentation\Http\Requests\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Requests\SavePaymentMethodRequest;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\Store;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

final class PaymentMethodUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool)Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            SavePaymentMethodRequest::FIELD_NAME => 'required|max:50',
            SavePaymentMethodRequest::FIELD_LOGO => 'mimetypes:image/jpeg,image/png,image/svg,image/svg+xml|max:1024',
        ];
    }
}
