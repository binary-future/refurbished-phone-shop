<?php

namespace App\Presentation\Http\Requests\Backend;

use App\Domain\Deals\Contract\Network;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

final class NetworkUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (bool)Auth::getUser();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => sprintf('required|max:50|unique:%s,name,%s', Network::TABLE, $this->get('network_id')),
            'logo' => 'mimetypes:image/jpeg,image/bmp,image/png,image/svg,image/svg+xml|max:1024',
        ];

        return $rules;
    }
}
