<?php

namespace App\Presentation\Http\Requests\Backend;

use App\Application\UseCases\Backend\Phones\Model\Requests\ModelUpdateRequest;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Foundation\Http\FormRequest;

final class PhoneModelUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return (bool)auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ModelUpdateRequest::INPUT_NAME => sprintf(
                '%s|max:100|unique:%s,name,%s,id,brand_id,%s',
                auth()->user()->isAdmin() ? 'required' : '',
                PhoneModel::TABLE,
                $this->get(ModelUpdateRequest::INPUT_MODEL_ID),
                $this->get(ModelUpdateRequest::INPUT_BRAND_ID)
            ),
            ModelUpdateRequest::INPUT_BRAND_ID => 'required|numeric',
            ModelUpdateRequest::INPUT_MODEL_ID => 'required|numeric',
            ModelUpdateRequest::INPUT_IS_ACTIVE => auth()->user()->isAdmin() ? 'required|numeric' : 'numeric',
            ModelUpdateRequest::INPUT_IS_LATEST => auth()->user()->isAdmin() ? 'required|numeric' : 'numeric',
            ModelUpdateRequest::INPUT_DESCRIPTION_CONTENT => 'nullable',
            ModelUpdateRequest::INPUT_FAQ_ID => 'nullable|numeric',
            ModelUpdateRequest::INPUT_FAQ_ANSWER => 'nullable|string',
            ModelUpdateRequest::INPUT_FAQ_QUESTION => 'nullable|string',
        ];
    }
}
