<?php


namespace App\Presentation\Http\Requests\Backend;


use App\Presentation\Http\Rules\UniqueMergeModelFilterRule;
use Illuminate\Foundation\Http\FormRequest;

final class ModelFilterSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param UniqueMergeModelFilterRule $rule
     * @return array
     */
    public function rules(UniqueMergeModelFilterRule $rule)
    {
        $rule->setRequest($this);
        $rules = [
            'brand_id' => 'required|numeric',
            'name' => ['required', 'string', 'max:30', $rule],
            'filter_id' => 'nullable|numeric',
            'parent_id' => 'nullable|numeric',
        ];

        return $rules;
    }
}
