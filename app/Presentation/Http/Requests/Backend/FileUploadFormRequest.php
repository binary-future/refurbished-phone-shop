<?php


namespace App\Presentation\Http\Requests\Backend;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class FileUploadRequest
 * @package App\Presentation\Http\Requests\Backend
 */
final class FileUploadFormRequest extends FormRequest
{
    /**
     * @var array
     */
    private $schema = [
        'images' => ['file' => 'required|mimes:zip'],
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'target' => 'required',
            'type' => 'required'
        ];

        return array_merge($rules, $this->schema[$this->get('type')] ?? []);
    }
}
