<?php


namespace App\Presentation\Http\Requests\Frontend;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ModelSearchFormRequest
 * @package App\Presentation\Http\Requests\Frontend
 */
final class ModelSearchFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string',
        ];

        return $rules;
    }
}
