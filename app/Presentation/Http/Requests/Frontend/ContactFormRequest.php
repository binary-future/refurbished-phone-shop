<?php


namespace App\Presentation\Http\Requests\Frontend;


use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ContactFormRequest
 * @package App\Presentation\Http\Requests\Frontend
 */
final class ContactFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'max:500',
        ];

        if (config('app.env') === 'production') {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }

        return $rules;
    }
}
