<?php

namespace App\Presentation\Http\Requests\Frontend\Brand;

use App\Application\UseCases\Phones\Requests\BrandDealsFilterRequest;
use Illuminate\Foundation\Http\FormRequest;

class BrandViewFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            BrandDealsFilterRequest::FIELD_SORT => 'nullable|alpha_dash|max:30',
            BrandDealsFilterRequest::FIELD_MODELS => 'nullable|alpha_dash|max:100',
            BrandDealsFilterRequest::FIELD_MONTHLY_COST => 'nullable|string|max:50',
            BrandDealsFilterRequest::FIELD_TOTAL_COST => 'nullable|string|max:50',
            BrandDealsFilterRequest::FIELD_NETWORKS => 'nullable|string|max:50',
            BrandDealsFilterRequest::FIELD_CONDITIONS => 'nullable|string|max:10',
            BrandDealsFilterRequest::FIELD_PAGE => 'nullable|integer',
        ];
    }
}
