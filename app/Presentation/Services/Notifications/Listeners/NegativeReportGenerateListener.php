<?php


namespace App\Presentation\Services\Notifications\Listeners;


use App\Application\Services\Importer\Events\NegativeReportGeneratedEvent;
use App\Presentation\Services\Notifications\NegativeReportGenerateNotification;
use App\Utils\Adapters\Notificator\Contracts\Notificator;

/**
 * Class NegativeReportGenerateListener
 * @package App\Presentation\Services\Notifications\Listeners
 */
final class NegativeReportGenerateListener
{
    /**
     * @var Notificator
     */
    private $notificator;

    /**
     * NegativeReportGenerateListener constructor.
     * @param Notificator $notificator
     */
    public function __construct(Notificator $notificator)
    {
        $this->notificator = $notificator;
    }

    /**
     * @param NegativeReportGeneratedEvent $event
     */
    public function handle(NegativeReportGeneratedEvent $event)
    {
        $this->notificator->notify(new NegativeReportGenerateNotification($event->getReport(), $event->getStore()));
    }
}
