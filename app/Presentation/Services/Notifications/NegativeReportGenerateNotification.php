<?php


namespace App\Presentation\Services\Notifications;


use App\Application\Services\Importer\Reports\NegativeReport;
use App\Domain\Store\Store;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

final class NegativeReportGenerateNotification extends Notification
{
    /**
     * @var NegativeReport
     */
    private $report;

    /**
     * @var Store
     */
    private $store;

    /**
     * NegativeReportGenerateNotification constructor.
     * @param NegativeReport $report
     * @param Store $store
     */
    public function __construct(NegativeReport $report, Store $store)
    {
        $this->report = $report;
        $this->store = $store;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())->markdown('frontend.components.mails.negative-report-notification', [
                'report' => $this->report,
                'store' => $this->store,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
