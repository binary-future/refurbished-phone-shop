<?php


namespace App\Presentation\Services\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

final class UserContactNotification extends Notification
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * Create a new notification instance.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->isValidData($this->data)) {
            return (new MailMessage())->markdown('frontend.components.mails.contact-form-notification', [
                'name' => $this->data['name'],
                'email' => $this->data['email'],
                'message' => $this->data['message'] ?? '',
            ]);
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isValidData(array $data): bool
    {
        return isset($data['name']) && isset($data['email']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
