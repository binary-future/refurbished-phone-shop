<?php

namespace App\Presentation\Services\Templates;

use App\Application\Services\Search\Filters\BrandDealsAdjustedFilters;
use App\Application\Services\Search\Filters\Contracts\PrimaryDealsAdjustedFilters;
use App\Application\Services\Search\Filters\ModelDealsAdjustedFilters;

/**
 * Class FiltersTemplatesRenderer
 * @package App\Presentation\Services\Templates
 */
class FiltersTemplatesRenderer
{
    private const SLIDER_SUFFIX = '_slider';
    private const RANGE_SUFFIX = '_range';

    /**
     * @var array
     */
    private $filtersTemplatesMap = [
        PrimaryDealsAdjustedFilters::FILTER_MODEL_SLUG => 'frontend.components.filters.filter-phone-models',
        PrimaryDealsAdjustedFilters::FILTER_MONTHLY_COST => 'frontend.components.filters.filter-monthly-cost',
        PrimaryDealsAdjustedFilters::FILTER_MONTHLY_COST_RANGE
            => 'frontend.components.filters.filter-monthly-cost-slider',
        PrimaryDealsAdjustedFilters::FILTER_NETWORKS => 'frontend.components.filters.filter-network',
        PrimaryDealsAdjustedFilters::FILTER_TOTAL_COST => 'frontend.components.filters.filter-upfront-cost',
        PrimaryDealsAdjustedFilters::FILTER_TOTAL_COST_RANGE
            => 'frontend.components.filters.filter-upfront-cost-slider',
        ModelDealsAdjustedFilters::FILTER_COLOR => 'frontend.components.filters.filter-phone-colors',
        ModelDealsAdjustedFilters::FILTER_CAPACITY => 'frontend.components.filters.filter-phone-capacity',
    ];


    /**
     * @param array $filters
     * @param array $selected
     * @return array
     * @throws \Throwable
     */
    public function renderFilters(array $filters, array $selected = [])
    {
        $selected = $this->prepareSelected($selected);
        $filtersToRender = $this->removeSelectedFilters($filters, $selected);

        return $this->proceedRendering($filtersToRender);
    }

    /**
     * @param array $filters
     * @param array $selected
     * @return array
     */
    private function removeSelectedFilters(array $filters, array $selected)
    {
        foreach ($selected as $key) {
            unset($filters[$key]);
        }

        return $filters;
    }

    /**
     * @param array $filters
     * @return array
     * @throws \Throwable
     */
    private function proceedRendering(array $filters)
    {
        $renderedFilters = [];
        foreach ($filters as $name => $filter) {
            if (isset($this->filtersTemplatesMap[$name])) {
                $renderedFilters[] = [
                    'html' => view($this->filtersTemplatesMap[$name], ['filters' => $filter])->render(),
                    'name' => $name,
                ];
            }
        }

        return $renderedFilters;
    }

    private function prepareSelected(array $data): array
    {
        $params = [];
        foreach (array_keys($data) as $item) {
            $item = str_ireplace(self::RANGE_SUFFIX, '', $item);
            $params[] = $item;
            if (! stripos($item, self::SLIDER_SUFFIX)) {
                $params[] = sprintf('%s%s', $item, self::SLIDER_SUFFIX);
            }
        }

        return $params;
    }
}
