<?php

namespace App\Presentation\Services\Routes\Dictionaries;

interface DealsRouteDict
{
    public const DEAL_OUTLINK = 'deal.outlink';
    public const DEAL_OUTLINK_PARAM_DEAL = 'deal';

    public const DEAL_OUTLINK_GO = 'deal.outlink.go';
    public const DEAL_OUTLINK_GO_PARAM_DEAL = 'deal';
}
