<?php

namespace App\Presentation\Services\Filters\Filters;

use App\Application\Services\Search\Filters\ModelDealsAdjustedFilters;
use App\Utils\Template\Filters\Filters\CheckboxSection;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Filters\Slider;

final class ModelViewLeftSideBarFilter
{
    /**
     * @var RadioSection|null
     */
    private $totalCost;

    /**
     * @var RadioSection|null
     */
    private $monthlyCost;

    /**
     * @var CheckboxSection|null
     */
    private $networks;

    /**
     * @var Slider|null
     */
    private $monthlyCostSlider;

    /**
     * @var Slider|null
     */
    private $totalCostSlider;
    /**
     * @var CheckboxSection|null
     */
    private $conditions;

    /**
     * @return RadioSection
     */
    public function getTotalCost(): RadioSection
    {
        return $this->totalCost;
    }

    /**
     * @return RadioSection
     */
    public function getMonthlyCost(): RadioSection
    {
        return $this->monthlyCost;
    }

    /**
     * @return CheckboxSection
     */
    public function getNetworks(): CheckboxSection
    {
        return $this->networks;
    }

    /**
     * @return CheckboxSection
     */
    public function getConditions(): CheckboxSection
    {
        return $this->conditions;
    }

    /**
     * @param RadioSection $totalCost
     */
    public function setTotalCost(RadioSection $totalCost): void
    {
        $this->totalCost = $totalCost;
    }

    /**
     * @param RadioSection $monthlyCost
     */
    public function setMonthlyCost(RadioSection $monthlyCost): void
    {
        $this->monthlyCost = $monthlyCost;
    }

    /**
     * @param CheckboxSection $networks
     */
    public function setNetworks(CheckboxSection $networks): void
    {
        $this->networks = $networks;
    }

    /**
     * @param CheckboxSection $conditions
     */
    public function setConditions(CheckboxSection $conditions): void
    {
        $this->conditions = $conditions;
    }

    public function toArray(): array
    {
        return [
            ModelDealsAdjustedFilters::FILTER_TOTAL_COST => $this->totalCost,
            ModelDealsAdjustedFilters::FILTER_MONTHLY_COST => $this->monthlyCost,
            ModelDealsAdjustedFilters::FILTER_NETWORKS => $this->networks,
            ModelDealsAdjustedFilters::FILTER_CONDITIONS => $this->conditions,
            ModelDealsAdjustedFilters::FILTER_MONTHLY_COST . '_slider' => $this->monthlyCostSlider,
            ModelDealsAdjustedFilters::FILTER_TOTAL_COST . '_slider' => $this->totalCostSlider,
        ];
    }

    public function setMonthlyCostSlider(Slider $slider)
    {
        $this->monthlyCostSlider = $slider;
    }

    public function getMonthlyCostSlider(): Slider
    {
        return $this->monthlyCostSlider;
    }

    /**
     * @return Slider
     */
    public function getTotalCostSlider(): Slider
    {
        return $this->totalCostSlider;
    }

    /**
     * @param Slider $totalCostSlider
     */
    public function setTotalCostSlider(Slider $totalCostSlider): void
    {
        $this->totalCostSlider = $totalCostSlider;
    }

    public function isValid(): bool
    {
        return $this->monthlyCostSlider
            && $this->monthlyCost
            && $this->networks
            && $this->conditions
            && $this->totalCost
            && $this->totalCostSlider;
    }
}
