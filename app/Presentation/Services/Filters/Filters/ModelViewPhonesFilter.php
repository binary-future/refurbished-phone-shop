<?php

namespace App\Presentation\Services\Filters\Filters;

use App\Utils\Template\Filters\Filters\RadioSection;

final class ModelViewPhonesFilter
{
    /**
     * @var RadioSection
     */
    private $colors;

   /**
    * @var RadioSection
    */
    private $capacity;

    /**
     * @return RadioSection
     */
    public function getColors(): RadioSection
    {
        return $this->colors;
    }

    /**
     * @param RadioSection $colors
     */
    public function setColors(RadioSection $colors): void
    {
        $this->colors = $colors;
    }

    /**
     * @return RadioSection
     */
    public function getCapacity(): RadioSection
    {
        return $this->capacity;
    }

    /**
     * @param RadioSection $capacity
     */
    public function setCapacity(RadioSection $capacity): void
    {
        $this->capacity = $capacity;
    }

    public function toArray(): array
    {
        return [
            'color' => $this->colors,
            'capacity' => $this->capacity
        ];
    }

    public function isValid(): bool
    {
        return $this->colors
            && $this->capacity;
    }
}
