<?php

namespace App\Presentation\Services\Filters\Filters;

use App\Application\Services\Search\Filters\BrandDealsAdjustedFilters;
use App\Utils\Template\Filters\Filters\CheckboxSection;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Filters\Slider;

final class BrandViewLeftSideBarFilter
{
    /**
     * @var RadioSection|null
     */
    private $totalCost;

    /**
     * @var RadioSection|null
     */
    private $monthlyCost;

    /**
     * @var CheckboxSection|null
     */
    private $networks;

    /**
     * @var RadioSection|null
     */
    private $models;

    /**
     * @var Slider|null
     */
    private $monthlyCostSlider;

    /**
     * @var Slider|null
     */
    private $totalCostSlider;
    /**
     * @var CheckboxSection
     */
    private $conditions;

    /**
     * @return RadioSection
     */
    public function getTotalCost(): RadioSection
    {
        return $this->totalCost;
    }

    /**
     * @param RadioSection $totalCost
     */
    public function setTotalCost(RadioSection $totalCost): void
    {
        $this->totalCost = $totalCost;
    }

    /**
     * @return RadioSection
     */
    public function getMonthlyCost(): RadioSection
    {
        return $this->monthlyCost;
    }

    /**
     * @param RadioSection $monthlyCost
     */
    public function setMonthlyCost(RadioSection $monthlyCost): void
    {
        $this->monthlyCost = $monthlyCost;
    }

    /**
     * @return CheckboxSection
     */
    public function getNetworks(): CheckboxSection
    {
        return $this->networks;
    }

    /**
     * @param CheckboxSection $networks
     */
    public function setNetworks(CheckboxSection $networks): void
    {
        $this->networks = $networks;
    }

    /**
     * @return CheckboxSection
     */
    public function getConditions(): CheckboxSection
    {
        return $this->conditions;
    }

    /**
     * @param CheckboxSection $conditions
     */
    public function setConditions(CheckboxSection $conditions): void
    {
        $this->conditions = $conditions;
    }

    /**
     * @return RadioSection
     */
    public function getModels(): RadioSection
    {
        return $this->models;
    }

    /**
     * @param RadioSection $models
     */
    public function setModels(RadioSection $models): void
    {
        $this->models = $models;
    }

    public function toArray(): array
    {
        return [
            BrandDealsAdjustedFilters::FILTER_MODEL_SLUG => $this->getModels(),
            BrandDealsAdjustedFilters::FILTER_NETWORKS => $this->getNetworks(),
            BrandDealsAdjustedFilters::FILTER_CONDITIONS => $this->getConditions(),
            BrandDealsAdjustedFilters::FILTER_MONTHLY_COST => $this->getMonthlyCost(),
            BrandDealsAdjustedFilters::FILTER_TOTAL_COST => $this->getTotalCost(),
            BrandDealsAdjustedFilters::FILTER_MONTHLY_COST_RANGE => $this->monthlyCostSlider,
            BrandDealsAdjustedFilters::FILTER_TOTAL_COST_RANGE => $this->totalCostSlider,
        ];
    }

    public function setMonthlyCostSlider(Slider $slider)
    {
        $this->monthlyCostSlider = $slider;
    }

    public function getMonthlyCostSlider(): ?Slider
    {
        return $this->monthlyCostSlider;
    }

    /**
     * @return Slider
     */
    public function getTotalCostSlider(): Slider
    {
        return $this->totalCostSlider;
    }

    /**
     * @param Slider $totalCostSlider
     */
    public function setTotalCostSlider(Slider $totalCostSlider): void
    {
        $this->totalCostSlider = $totalCostSlider;
    }

    public function isValid(): bool
    {
        return $this->models
            && $this->monthlyCost
            && $this->networks
            && $this->totalCost
            && $this->monthlyCostSlider
            && $this->totalCostSlider;
    }
}
