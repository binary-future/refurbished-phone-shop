<?php


namespace App\Presentation\Services\Filters\Scenario;


use App\Application\Services\Search\Filters\DealIndex;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Presentation\Services\Filters\Contracts\Scenario;
use Illuminate\Support\Collection;

final class ModelViewPhones implements Scenario
{
    public const CONTEXT = 'model-view-phones-filter';

    /**
     * @var Collection
     */
    private $phones;

    /**
     * @var DealsInfoSearchResponse
     */
    private $dealsInfo;

    /**
     * @var DealIndex
     */
    private $dealIndex;

    /**
     * ModelViewPhones constructor.
     * @param Collection $phones
     * @param DealsInfoSearchResponse $dealsInfo
     * @param DealIndex $dealIndex
     */
    public function __construct(
        Collection $phones,
        DealsInfoSearchResponse $dealsInfo,
        DealIndex $dealIndex
    ) {
        $this->phones = $phones;
        $this->dealsInfo = $dealsInfo;
        $this->dealIndex = $dealIndex;
    }

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @return DealsInfoSearchResponse
     */
    public function getDealsInfo(): DealsInfoSearchResponse
    {
        return $this->dealsInfo;
    }

    /**
     * @return DealIndex
     */
    public function getDealIndex(): DealIndex
    {
        return $this->dealIndex;
    }
}
