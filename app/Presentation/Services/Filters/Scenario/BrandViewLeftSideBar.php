<?php

namespace App\Presentation\Services\Filters\Scenario;

use App\Application\Services\Search\Filters\BrandDealsAdjustedFilters;
use App\Application\Services\Search\Filters\DealIndex;
use App\Presentation\Services\Filters\Contracts\Scenario;
use Illuminate\Support\Collection;

/**
 * Class ModelGroupViewLeftSideBar
 * @package App\Presentation\Services\Filters\Scenario
 */
final class BrandViewLeftSideBar implements Scenario
{
    public const CONTEXT = 'brand-view-left-side-bar';

    /**
     * @var Collection
     */
    private $models;

    /**
     * @var Collection
     */
    private $allNetworks;

    /**
     * @var DealIndex
     */
    private $dealIndex;
    /**
     * @var BrandDealsAdjustedFilters
     */
    private $adjustedFilters;
    /**
     * @var Collection
     */
    private $allConditions;

    /**
     * ModelGroupViewLeftSideBar constructor.
     * @param Collection $allNetworks
     * @param Collection $allConditions
     * @param Collection $models
     * @param DealIndex $dealIndex
     * @param BrandDealsAdjustedFilters $adjustedFilters
     */
    public function __construct(
        Collection $allNetworks,
        Collection $allConditions,
        Collection $models,
        DealIndex $dealIndex,
        BrandDealsAdjustedFilters $adjustedFilters
    ) {
        $this->allNetworks = $allNetworks;
        $this->models = $models;
        $this->dealIndex = $dealIndex;
        $this->adjustedFilters = $adjustedFilters;
        $this->allConditions = $allConditions;
    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return Collection
     */
    public function getAllNetworks(): Collection
    {
        return $this->allNetworks;
    }

    /**
     * @return Collection
     */
    public function getAllConditions(): Collection
    {
        return $this->allConditions;
    }

    /**
     * @return BrandDealsAdjustedFilters
     */
    public function getAdjustedFilters(): BrandDealsAdjustedFilters
    {
        return $this->adjustedFilters;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }

    /**
     * @return DealIndex
     */
    public function getDealIndex(): DealIndex
    {
        return $this->dealIndex;
    }
}
