<?php

namespace App\Presentation\Services\Filters\Scenario;

use App\Application\Services\Search\Filters\DealIndex;
use App\Application\Services\Search\Filters\ModelDealsAdjustedFilters;
use App\Presentation\Services\Filters\Contracts\Scenario;
use Illuminate\Support\Collection;

/**
 * Class ModelViewLeftSideBar
 * @package App\Presentation\Services\Filters\Scenario
 */
final class ModelViewLeftSideBar implements Scenario
{
    public const CONTEXT = 'model-view-left-side-bar';

    /**
     * @var Collection
     */
    private $networks;

    /**
     * @var DealIndex
     */
    private $dealIndex;
    /**
     * @var ModelDealsAdjustedFilters
     */
    private $adjustedFilters;
    /**
     * @var Collection
     */
    private $conditions;

    /**
     * ModelViewLeftSideBar constructor.
     * @param Collection $networks
     * @param Collection $conditions
     * @param DealIndex $dealIndex
     * @param ModelDealsAdjustedFilters $adjustedFilters
     */
    public function __construct(
        Collection $networks,
        Collection $conditions,
        DealIndex $dealIndex,
        ModelDealsAdjustedFilters $adjustedFilters
    ) {
        $this->networks = $networks;
        $this->dealIndex = $dealIndex;
        $this->adjustedFilters = $adjustedFilters;
        $this->conditions = $conditions;
    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return Collection
     */
    public function getNetworks(): Collection
    {
        return $this->networks;
    }

    /**
     * @return Collection
     */
    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    /**
     * @return DealIndex
     */
    public function getDealIndex(): DealIndex
    {
        return $this->dealIndex;
    }

    /**
     * @return ModelDealsAdjustedFilters
     */
    public function getAdjustedFilters(): ModelDealsAdjustedFilters
    {
        return $this->adjustedFilters;
    }
}
