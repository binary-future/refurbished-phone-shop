<?php

namespace App\Presentation\Services\Filters\Contracts;

interface Filter
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return bool
     */
    public function isEnabled(): bool;
}
