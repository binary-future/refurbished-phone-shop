<?php


namespace App\Presentation\Services\Filters\Contracts;


/**
 * Interface Scenario
 * @package App\Presentation\Services\Filters\Contracts
 */
interface Scenario
{
    /**
     * @return string
     */
    public function getContext(): string;
}