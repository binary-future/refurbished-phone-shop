<?php


namespace App\Presentation\Services\Filters\Contracts;


/**
 * Interface Generator
 * @package App\Presentation\Services\Filters\Contracts
 */
interface Generator
{
    /**
     * @param Scenario $scenario
     * @return mixed
     */
    public function generate(Scenario $scenario);
}