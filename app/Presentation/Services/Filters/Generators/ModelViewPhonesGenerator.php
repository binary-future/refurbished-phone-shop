<?php


namespace App\Presentation\Services\Filters\Generators;

use App\Application\Services\Search\DTO\PhonesGroupDealsInfo;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Presentation\Services\Filters\Contracts\Filter;
use App\Presentation\Services\Filters\Contracts\Scenario;
use App\Presentation\Services\Filters\Filters\ModelViewPhonesFilter;
use App\Presentation\Services\Filters\Scenario\ModelViewPhones;
use App\Utils\Template\Filters\BaseGenerator;
use Illuminate\Support\Collection;

/**
 * Class ModelViewPhonesGenerator
 * @package App\Presentation\Services\Filters\Generators
 */
final class ModelViewPhonesGenerator extends Generator
{
    /**
     * @param ModelViewPhones $scenario
     * @return array
     */
    protected function translateData(Scenario $scenario): array
    {
        return [
            'phones' => $scenario->getPhones(),
            'activePhones' => $scenario->getDealIndex()->getProducts(),
            'dealsInfo' => $scenario->getDealsInfo(),
        ];
    }

    protected function filterName(): string
    {
        return ModelViewPhonesFilter::class;
    }
}
