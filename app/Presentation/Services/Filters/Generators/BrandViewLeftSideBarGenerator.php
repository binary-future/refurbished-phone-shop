<?php

namespace App\Presentation\Services\Filters\Generators;

use App\Presentation\Services\Filters\Contracts\Scenario;
use App\Presentation\Services\Filters\Filters\BrandViewLeftSideBarFilter;
use App\Presentation\Services\Filters\Generators\Items\ConditionCheckboxGenerator;
use App\Presentation\Services\Filters\Generators\Items\NetworkCheckboxGenerator;
use App\Presentation\Services\Filters\Generators\Items\TotalCostRadioGenerator;
use App\Presentation\Services\Filters\Scenario\BrandViewLeftSideBar;

/**
 * Class ModelGroupViewLeftSideBarGenerator
 * @package App\Presentation\Services\Filters\Generators
 */
final class BrandViewLeftSideBarGenerator extends Generator
{
    /**
     * @param BrandViewLeftSideBar $scenario
     * @return array
     */
    protected function translateData(Scenario $scenario): array
    {
        return [
            NetworkCheckboxGenerator::FILTER_NETWORKS => $scenario->getAllNetworks()->all(),
            NetworkCheckboxGenerator::FILTER_CHECKED_NETWORKS => $scenario->getAdjustedFilters()->getNetworks(),
            NetworkCheckboxGenerator::FILTER_ENABLED_NETWORKS => $scenario->getDealIndex()->getNetworks()->all(),
            ConditionCheckboxGenerator::FILTER_CONDITIONS => $scenario->getAllConditions()->all(),
            ConditionCheckboxGenerator::FILTER_ENABLED_CONDITIONS => $scenario->getDealIndex()->getConditions()->all(),
            'monthly_cost' => $scenario->getDealIndex()->getMonthlyCosts()->all(),
            TotalCostRadioGenerator::FILTER_TOTAL_COST => $scenario->getDealIndex()->getTotalCosts()->all(),
            'models' => $scenario->getModels(),
            'phones' => $scenario->getDealIndex()->getProducts(),
        ];
    }

    protected function filterName(): string
    {
        return BrandViewLeftSideBarFilter::class;
    }
}
