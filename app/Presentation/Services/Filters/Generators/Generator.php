<?php


namespace App\Presentation\Services\Filters\Generators;

use App\Presentation\Services\Filters\Contracts\Generator as Contract;
use App\Presentation\Services\Filters\Contracts\Scenario;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Template\Filters\Exceptions\FilterGeneratorException;
use App\Utils\Template\Filters\ItemFactory;

abstract class Generator implements Contract
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var ItemFactory
     */
    private $factory;

    /**
     * Generator constructor.
     * @param Serializer $serializer
     * @param ItemFactory $factory
     * @param Config $config
     */
    public function __construct(Serializer $serializer, ItemFactory $factory)
    {
        $this->serializer = $serializer;
        $this->factory = $factory;
    }

    /**
     * @param Scenario $scenario
     * @return mixed|object
     * @throws FilterGeneratorException
     */
    public function generate(Scenario $scenario)
    {
        try {
            return $this->proceed($scenario);
        } catch (\Throwable $exception) {
            throw FilterGeneratorException::cannotGenerateFiltersByScenario($scenario, $exception->getMessage());
        }
    }

    /**
     * @param Scenario $scenario
     * @return object
     * @throws FilterGeneratorException
     */
    private function proceed(Scenario $scenario)
    {
        $generator = $this->factory->buildGenerator($scenario->getContext());
        $data = $this->translateData($scenario);

        return $this->serializer->fromArray($this->filterName(), $generator->generate($data));
    }

    abstract protected function translateData(Scenario $scenario): array;

    abstract protected function filterName(): string;
}
