<?php

namespace App\Presentation\Services\Filters\Generators;

use App\Presentation\Services\Filters\Contracts\Scenario;
use App\Presentation\Services\Filters\Filters\ModelViewLeftSideBarFilter;
use App\Presentation\Services\Filters\Generators\Items\ConditionCheckboxGenerator;
use App\Presentation\Services\Filters\Generators\Items\NetworkCheckboxGenerator;
use App\Presentation\Services\Filters\Generators\Items\TotalCostRadioGenerator;
use App\Presentation\Services\Filters\Scenario\ModelViewLeftSideBar;

final class ModelViewLeftSideBarGenerator extends Generator
{
    /**
     * @param ModelViewLeftSideBar $scenario
     * @return array
     */
    protected function translateData(Scenario $scenario): array
    {
        return [
            NetworkCheckboxGenerator::FILTER_NETWORKS => $scenario->getNetworks()->all(),
            NetworkCheckboxGenerator::FILTER_ENABLED_NETWORKS => $scenario->getDealIndex()->getNetworks()->all(),
            NetworkCheckboxGenerator::FILTER_CHECKED_NETWORKS => $scenario->getAdjustedFilters()->getNetworks(),
            ConditionCheckboxGenerator::FILTER_CONDITIONS => $scenario->getConditions()->all(),
            ConditionCheckboxGenerator::FILTER_ENABLED_CONDITIONS => $scenario->getDealIndex()->getConditions()->all(),
            'monthly_cost' => $scenario->getDealIndex()->getMonthlyCosts()->all(),
            TotalCostRadioGenerator::FILTER_TOTAL_COST => $scenario->getDealIndex()->getTotalCosts()->all(),
        ];
    }

    protected function filterName(): string
    {
        return ModelViewLeftSideBarFilter::class;
    }
}
