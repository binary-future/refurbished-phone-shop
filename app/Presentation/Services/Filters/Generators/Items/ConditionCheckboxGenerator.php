<?php

namespace App\Presentation\Services\Filters\Generators\Items;

use App\Presentation\Services\Filters\Contracts\Filter;
use App\Presentation\Services\Translators\Phones\TranslatedCondition;
use App\Utils\Template\Filters\Filters\Checkbox;
use App\Utils\Template\Filters\Filters\CheckboxSection;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

/**
 * Class ConditionCheckboxGenerator
 * @package App\Presentation\Services\Filters\Generators\Items
 */
final class ConditionCheckboxGenerator extends BaseItem
{
    public const FILTER_CONDITIONS = 'conditions';
    public const FILTER_ENABLED_CONDITIONS = 'enabledConditions';

    /**
     * @param mixed $data
     * @param array $options
     * @return bool
     */
    protected function isSatisfy($data, array $options): bool
    {
        return isset($data[self::FILTER_CONDITIONS], $data[self::FILTER_ENABLED_CONDITIONS]);
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return array|mixed
     */
    protected function proceed($data, array $options)
    {
        $conditions = $data[self::FILTER_CONDITIONS];
        $enabledConditions = $data[self::FILTER_ENABLED_CONDITIONS];
        $filterItems = [];
        foreach ($conditions as $condition) {
            /**
             * @var TranslatedCondition $condition
             */
            $filterItems[] = $this->generateItem($condition, $enabledConditions, []);
        }

        $filters = Collection::make($filterItems)->sortBy(static function (Filter $filterItem) {
            return ! $filterItem->isEnabled();
        })->all();

        $data[self::FILTER_CONDITIONS] = new CheckboxSection($filters, $this->filterName(), count($filters) > 0);

        return $data;
    }

    /**
     * @param TranslatedCondition $condition
     * @param array $enabled
     * @param array $checked
     * @return Filter
     */
    private function generateItem(TranslatedCondition $condition, array $enabled, array $checked): Filter
    {
        return new Checkbox(
            $this->filterName(),
            $condition,
            in_array($condition->getKey(), $enabled, true),
            in_array($condition->getKey(), $checked, true)
        );
    }

    /**
     * @return string
     */
    protected function filterName(): string
    {
        return self::FILTER_CONDITIONS;
    }
}
