<?php

namespace App\Presentation\Services\Filters\Generators\Items;

use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Filters\Contracts\Filter;
use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

/**
 * Class PhoneModelRadioGenerator
 * @package App\Presentation\Services\Filters\Generators\Items
 */
final class PhoneModelRadioGenerator extends BaseItem
{
    /**
     * @param $data
     * @param array $options
     * @return bool
     */
    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['models'])
            && $data['models'] instanceof Collection
            && isset($data['phones'])
            && $data['phones'] instanceof Collection;
    }

    /**
     * @param $data
     * @param array $options
     * @return RadioSection|mixed
     */
    protected function proceed($data, array $options)
    {
        $models = $this->getModels($data);
        $phones = $this->getPhones($data);
        $groupedPhones = $phones->groupBy('model_id');
        $result = [];
        foreach ($models as $model) {
            /**
             * @var PhoneModel $model
             */
            $result[] = new Radio('models', $model, $groupedPhones->has($model->getKey()));
        }

        $filters = array_sort($result, function (Filter $filter) {
            return ! $filter->isEnabled();
        });
        $data['models'] = new RadioSection($filters, 'models', count($filters) > 0);

        return $data;
    }

    /**
     * @param array $data
     * @return Collection
     */
    private function getModels(array $data): Collection
    {
        return $data['models']->filter(function ($item) {
            return $item instanceof PhoneModel;
        });
    }

    /**
     * @param array $data
     * @return Collection
     */
    private function getPhones(array $data): Collection
    {
        return $data['phones'];
    }

    /**
     * @return string
     */
    protected function filterName(): string
    {
        return 'models';
    }
}
