<?php

namespace App\Presentation\Services\Filters\Generators\Items;

use App\Presentation\Services\Translators\Contracts\Translator;
use App\Utils\Template\Filters\Filters\Slider;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

final class TotalCostSliderGenerator extends BaseItem
{
    public const FILTER_TOTAL_COST = TotalCostRadioGenerator::FILTER_TOTAL_COST;
    public const FILTER_TOTAL_COST_SLIDER = TotalCostRadioGenerator::FILTER_TOTAL_COST . '_slider';

    private const STEP = 10;
    private const MIN_VALUE = 0;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * TotalCostSliderGenerator constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    protected function isSatisfy($data, array $options): bool
    {
        return isset($data[self::FILTER_TOTAL_COST]);
    }

    protected function proceed($data, array $options)
    {
        $totalCost =  $this->translator->translate($this->getTotalCost($data));
        $slider = $totalCost->isEmpty() ? $this->generateEmptySlider() : $this->generateSlider($totalCost);
        $data[self::FILTER_TOTAL_COST_SLIDER] = $slider;

        return $data;
    }

    private function generateSlider(Collection $totalCost): Slider
    {
        $minValue = $totalCost->min();
        $maxValue = $totalCost->max();

        return new Slider(
            $this->filterName(),
            (string)$minValue,
            (string)self::MIN_VALUE,
            (string)$maxValue,
            (string)self::STEP,
            'Total cost',
            $minValue !== $maxValue
        );
    }

    protected function filterName(): string
    {
        return self::FILTER_TOTAL_COST_SLIDER;
    }

    private function getTotalCost($data)
    {
        $totalCost = $data[self::FILTER_TOTAL_COST];

        return is_array($totalCost) ? collect($totalCost) : $totalCost;
    }

    private function generateEmptySlider(): Slider
    {
        return new Slider(
            $this->filterName(),
            '0',
            '0',
            '0',
            (string)self::STEP,
            'Total cost',
            false
        );
    }
}
