<?php

namespace App\Presentation\Services\Filters\Generators\Items;

use App\Presentation\Services\Translators\Contracts\Translator;
use App\Utils\Template\Filters\Filters\Slider;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

final class UpfrontCostSliderGenerator extends BaseItem
{
    private const STEP = 10;
    private const MIN_VALUE = 0;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * MonthlyCostSliderGenerator constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['upfront_cost']);
    }

    protected function proceed($data, array $options)
    {
        $upfrontCost =  $this->translator->translate($this->getUpfrontCost($data));
        $slider = $upfrontCost->isEmpty() ? $this->generateEmptySlider() : $this->generateSlider($upfrontCost);
        $data['upfront_cost_slider'] = $slider;

        return $data;
    }

    private function generateSlider(Collection $upfrontCost): Slider
    {
        $minValue = $upfrontCost->min();
        $maxValue = $upfrontCost->max();

        return new Slider(
            $this->filterName(),
            $minValue,
            self::MIN_VALUE,
            $maxValue,
            self::STEP,
            'Upfront cost',
            $minValue !== $maxValue
        );
    }

    protected function filterName(): string
    {
        return 'upfront_cost_slider';
    }

    private function getUpfrontCost($data)
    {
        $monthlyCost = $data['upfront_cost'];

        return is_array($monthlyCost) ? collect($monthlyCost) : $monthlyCost;
    }

    private function generateEmptySlider(): Slider
    {
        return new Slider(
            $this->filterName(),
            '0',
            '0',
            '0',
            self::STEP,
            'Upfront cost',
            false
        );
    }
}
