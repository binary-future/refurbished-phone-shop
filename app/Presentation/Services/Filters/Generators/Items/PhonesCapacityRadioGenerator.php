<?php


namespace App\Presentation\Services\Filters\Generators\Items;

use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

final class PhonesCapacityRadioGenerator extends BaseItem
{
    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['phones']) && isset($data['activePhones']);
    }

    protected function proceed($data, array $options)
    {
        $phonesCapacity = $this->getCapacity($data['phones']);
        $activePhonesCapacity = $this->getCapacity($data['activePhones']);
        $result = [];
        foreach ($phonesCapacity as $capacity) {
            $result[] = new Radio($this->filterName(), $capacity, in_array($capacity, $activePhonesCapacity));
        }

        $data['capacity'] = new RadioSection($result, $this->filterName());

        return $data;
    }

    /**
     * @param Collection $phones
     * @return array
     */
    private function getCapacity(Collection $phones)
    {
        $capacity = $phones->pluck('capacity')->unique()->filter()->all();
        natsort($capacity);

        return $capacity;
    }

    protected function filterName(): string
    {
        return 'capacity';
    }
}
