<?php


namespace App\Presentation\Services\Filters\Generators\Items;


use App\Presentation\Services\Translators\Contracts\Translator;
use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;

final class MonthlyCostRadioGenerator extends BaseItem
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * MonthlyCostRadioGenerator constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['monthly_cost']);
    }

    protected function proceed($data, array $options)
    {
        $monthlyCost = $this->translator->translate($data['monthly_cost']);

        $filters = [];
        foreach ($monthlyCost as $value) {
            $filters[] = new Radio($this->filterName(), $value);
        }
        $data['monthly_cost'] = new RadioSection($filters, $this->filterName(), count($monthlyCost) > 0);

        return $data;
    }

    protected function filterName(): string
    {
        return 'monthly_cost';
    }
}
