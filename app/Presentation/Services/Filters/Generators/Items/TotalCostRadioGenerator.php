<?php

namespace App\Presentation\Services\Filters\Generators\Items;

use App\Presentation\Services\Translators\Contracts\Translator;
use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;

/**
 * Class UpfrontCostRadioGenerator
 * @package App\Presentation\Services\Filters\Generators\Items
 */
final class TotalCostRadioGenerator extends BaseItem
{
    public const FILTER_TOTAL_COST = 'total_cost';

    /**
     * @var Translator
     */
    private $translator;

    /**
     * UpfrontCostRadioGenerator constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return bool
     */
    protected function isSatisfy($data, array $options): bool
    {
        return isset($data[self::FILTER_TOTAL_COST]);
    }

    /**'upfront_cost'
     * @param $data
     * @param array $options
     * @return RadioSection|mixed
     */
    protected function proceed($data, array $options)
    {
        $upfrontCosts = $this->translator->translate($data[self::FILTER_TOTAL_COST]);
        $filters = [];
        foreach ($upfrontCosts as $value) {
            $filters[] = new Radio($this->filterName(), $value);
        }
        $data[self::FILTER_TOTAL_COST] = new RadioSection(
            $filters,
            $this->filterName(),
            count($upfrontCosts) > 0
        );

        return $data;
    }

    /**
     * @return string
     */
    protected function filterName(): string
    {
        return self::FILTER_TOTAL_COST;
    }
}
