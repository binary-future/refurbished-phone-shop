<?php


namespace App\Presentation\Services\Filters\Generators\Items;


use App\Presentation\Services\Translators\Contracts\Translator;
use App\Utils\Template\Filters\Filters\Slider;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

final class MonthlyCostSliderGenerator extends BaseItem
{
    private const STEP = 5;
    private const MIN_VALUE = 0;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * MonthlyCostSliderGenerator constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['monthly_cost']);
    }

    protected function proceed($data, array $options)
    {
        $monthlyCost =  $this->translator->translate($this->getMonthlyCost($data));
        $slider = $monthlyCost->isEmpty() ? $this->generateEmptySlider() : $this->generateSlider($monthlyCost);
        $data['monthly_cost_slider'] = $slider;

        return $data;
    }

    private function generateSlider(Collection $monthlyCost): Slider
    {
        $minValue = $monthlyCost->min();
        $maxValue = $monthlyCost->max();

        return new Slider(
            $this->filterName(),
            $minValue,
            self::MIN_VALUE,
            $maxValue,
            self::STEP,
            'Monthly cost',
            $minValue !== $maxValue
        );
    }

    protected function filterName(): string
    {
        return 'monthly_cost_slider';
    }

    private function getMonthlyCost($data)
    {
        $monthlyCost = $data['monthly_cost'];

        return is_array($monthlyCost) ? collect($monthlyCost) : $monthlyCost;
    }

    private function generateEmptySlider(): Slider
    {
        return new Slider(
            $this->filterName(),
            '0',
            '0',
            '0',
            self::STEP,
            'Monthly cost',
            false
        );
    }
}
