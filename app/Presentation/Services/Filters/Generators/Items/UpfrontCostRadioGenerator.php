<?php


namespace App\Presentation\Services\Filters\Generators\Items;


use App\Presentation\Services\Translators\Contracts\Translator;
use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;

/**
 * Class UpfrontCostRadioGenerator
 * @package App\Presentation\Services\Filters\Generators\Items
 */
final class UpfrontCostRadioGenerator extends BaseItem
{
    /**
     * @var Translator
     */
    private $translator;

    /**
     * UpfrontCostRadioGenerator constructor.
     * @param Translator $translator
     */
    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param $data
     * @param array $options
     * @return bool
     */
    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['upfront_cost']);
    }

    /**
     * @param $data
     * @param array $options
     * @return RadioSection|mixed
     */
    protected function proceed($data, array $options)
    {
        $upfrontCosts = $this->translator->translate($data['upfront_cost']);
        $filters = [];
        foreach ($upfrontCosts as $value) {
            $filters[] = new Radio($this->filterName(), $value);
        }
        $data['upfront_cost'] = new RadioSection($filters, $this->filterName(), count($upfrontCosts) > 0);

        return $data;
    }

    /**
     * @return string
     */
    protected function filterName(): string
    {
        return 'upfront_cost';
    }

}
