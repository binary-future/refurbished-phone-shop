<?php

namespace App\Presentation\Services\Filters\Generators\Items;

use App\Domain\Deals\Contract\Network;
use App\Presentation\Services\Filters\Contracts\Filter;
use App\Utils\Template\Filters\Filters\Checkbox;
use App\Utils\Template\Filters\Filters\CheckboxSection;
use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

/**
 * Class NetworkRadioGenerator
 * @package App\Presentation\Services\Filters\Generators\Items
 */
final class NetworkCheckboxGenerator extends BaseItem
{
    public const FILTER_NETWORKS = 'networks';
    public const FILTER_ENABLED_NETWORKS = 'enabledNetworks';
    public const FILTER_CHECKED_NETWORKS = 'checkedNetworks';

    /**
     * @param mixed $data
     * @param array $options
     * @return bool
     */
    protected function isSatisfy($data, array $options): bool
    {
        return isset($data[self::FILTER_ENABLED_NETWORKS], $data[self::FILTER_NETWORKS]);
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return array|mixed
     */
    protected function proceed($data, array $options)
    {
        $networks = $data[self::FILTER_NETWORKS];
        $enabledNetworks = $data[self::FILTER_ENABLED_NETWORKS];
        $checkedNetworks = $data[self::FILTER_CHECKED_NETWORKS] ?? [];
        $filterItems = [];
        foreach ($networks as $network) {
            /**
             * @var Network $network
             */
            $filterItems[] = $this->generateItem($network, $enabledNetworks, $checkedNetworks);
        }

        $filters = Collection::make($filterItems)->sortBy(static function (Filter $filterItem) {
            return ! $filterItem->isEnabled();
        })->all();

        $data[self::FILTER_NETWORKS] = new CheckboxSection($filters, $this->filterName(), count($filters) > 0);

        return $data;
    }

    /**
     * @param Network $network
     * @param array $enabled
     * @param array $checked
     * @return Filter
     */
    private function generateItem(Network $network, array $enabled, array $checked): Filter
    {
        return new Checkbox(
            $this->filterName(),
            $network,
            in_array($network->getKey(), $enabled, true),
            in_array($network->getKey(), $checked, true)
        );
    }

    /**
     * @return string
     */
    protected function filterName(): string
    {
        return self::FILTER_NETWORKS;
    }
}
