<?php


namespace App\Presentation\Services\Filters\Generators\Items;

use App\Application\Services\Search\DTO\PhonesGroupDealsInfo;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Presentation\Services\Filters\Contracts\Filter;
use App\Utils\Template\Filters\Filters\Radio;
use App\Utils\Template\Filters\Filters\RadioSection;
use App\Utils\Template\Filters\Items\BaseItem;
use Illuminate\Support\Collection;

final class PhoneColorRadioGenerator extends BaseItem
{
    private const LAST_COLOR_IN_ORDER = 'red';

    protected function isSatisfy($data, array $options): bool
    {
        return isset($data['phones']) && isset($data['activePhones']) && isset($data['dealsInfo']);
    }

    protected function proceed($data, array $options)
    {
        $phones = $data['phones'];
        $activePhones = $data['activePhones'];
        $dealsInfo = $data['dealsInfo'];
        $filters = $this->getColorsFilters(
            $phones,
            $dealsInfo,
            $activePhones
        );

        $data['colors'] = new RadioSection($filters, $this->filterName());

        return $data;
    }

    /**
     * @param Collection $phones
     * @param DealsInfoSearchResponse $dealsInfo
     * @param Collection $activePhones
     * @return array
     */
    private function getColorsFilters(
        Collection $phones,
        DealsInfoSearchResponse $dealsInfo,
        Collection $activePhones
    ): array {
        $colors = $this->allColors($phones);
        $activeColors = $this->activeColors($activePhones, $dealsInfo);
        $filters = [];
        foreach ($colors as $color) {
            $filters[] = new Radio($this->filterName(), $color, in_array($color, $activeColors));
        }


        return array_sort($filters, function (Filter $filter) {
            return ! $filter->isEnabled();
        });
    }

    private function allColors(Collection $phones)
    {
        return $this->orderByColor(array_filter($this->getColors($phones)));
    }


    private function orderByColor(array $colors)
    {
        return array_sort($colors, function (Color $color) {
            return $color->getAlias() === self::LAST_COLOR_IN_ORDER ? 1 : 0;
        });
    }

    /**
     * @param Collection $phones
     * @return array
     */
    private function getColors(Collection $phones)
    {
        $result = [];
        foreach ($phones as $phone) {
            /**
             * @var Phone $phone
             */
            $color = $phone->getColor();
            if ($color) {
                $result[$color->getKey()] = $color;
            }
        }

        return $result;
    }

    private function activeColors(Collection $phones, DealsInfoSearchResponse $dealsInfo)
    {
        $colors = $this->getColors($phones);

        return $this->filterActiveColors($colors, $dealsInfo->getPhoneDealsInfo());
    }

    private function filterActiveColors(array $colors, array $phoneDeals)
    {
        if (empty($phoneDeals) || empty($colors)) {
            return ['disabled' => true];
        }
        $dealsCount = $this->generateColorsIdOrderedPhoneDealsCount($phoneDeals);
        $result = [];
        foreach ($dealsCount as $colorId) {
            $result[] = $colors[$colorId] ?? null;
        }

        return array_filter($result);
    }



    /**
     * @param array $phoneDeals
     * @return Collection
     */
    private function generateColorsIdOrderedPhoneDealsCount(array $phoneDeals)
    {
        $result = collect();
        foreach ($phoneDeals as $phoneDeal) {
            /**
             * @var PhonesGroupDealsInfo $phoneDeal
             */
            $phones = $phoneDeal->getPhones();
            $result = $result->push(
                [
                    'deals_count' => $this->calculatePhoneDeals($phones),
                    'color_id' => $phones->first()->getColorId(),
                ]
            );
        }
        return $result->sortByDesc('deals_count')->pluck('color_id');
    }

    /**
     * @param Collection $phones
     * @return int
     */
    private function calculatePhoneDeals(Collection $phones): int
    {
        return $phones->map(function (Phone $phone) {
            return $phone->getDealsCount();
        })->sum();
    }

    protected function filterName(): string
    {
        return 'color';
    }
}
