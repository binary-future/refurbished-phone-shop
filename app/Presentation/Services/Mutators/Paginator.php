<?php

namespace App\Presentation\Services\Mutators;

/**
 * Class Paginator
 * @package App\Presentation\Services\Decorators
 */
final class Paginator
{
    private const EACH_SIDE_LINKS_COUNT = 2;

    /**
     * @param $collection
     * @return mixed
     */
    public function mutateEachSideLinks($collection)
    {
        if (method_exists($collection, 'onEachSide')) {
            $collection->onEachSide(self::EACH_SIDE_LINKS_COUNT);
        }

        return $collection;
    }
}

