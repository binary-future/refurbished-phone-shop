<?php

use App\Domain\Shared\Contracts\HasBorrowedThumbs;
use App\Domain\Shared\Contracts\HasThumbs;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Rating\RatingValueInPercents;

if (! function_exists('brand_name')) {
    function brand_name(string $brandName)
    {
        if ($brandName === 'Apple') {
            return 'iPhone';
        } elseif ($brandName === 'Samsung') {
            return 'Galaxy';
        }

        return $brandName;
    }
}

if (! function_exists('image_path')) {
    function image_path($image)
    {
        $path = $image instanceof Image
            ? $image->getPath()
            : 'images/no-image.jpg';

        $cdn = cdn_path();

        return $cdn ? sprintf('%s/%s', $cdn, $path) : asset($path);
    }
}

if (! function_exists('cdn_path')) {
    function cdn_path()
    {
        return env('CDN_DOMAIN');
    }
}


if (! function_exists('thumb_image')) {
    function thumb_image($owner)
    {
        if (
            ! $owner instanceof HasThumbs
            && ! $owner instanceof HasBorrowedThumbs
        ) {
            throw new InvalidArgumentException(
                sprintf(
                    'Argument should implement %s or %s interfaces',
                    HasThumbs::class,
                    HasBorrowedThumbs::class
                )
            );
        }
        $height = 125;
        $thumb = $owner->getThumb($height);

        return image_path($thumb ?: $owner->getImage());
    }
}

if (! function_exists('filter_content')) {
    function filter_content(string $content)
    {
        if (stripos($content, 'Huawei Honor') !== false) {
            $filterEntries = [
                [ 'entry' => 'Huawei', 'replace' => '' ],
                [ 'entry' => '  ', 'replace' => ' '],
            ];
            foreach ($filterEntries as $filter) {
                $content = str_ireplace($filter['entry'], $filter['replace'], $content);
            }
            $content = trim($content);
        }

        return $content;
    }
}

if (! function_exists('trailing_zeros')) {
    function trailing_zeros(float $number, int $floatNumbers = 2, bool $onlyIfHasFloats = true)
    {
        if ($onlyIfHasFloats && $number - (int)$number === 0.0) {
            return $number;
        }

        return sprintf("%.{$floatNumbers}f", $number);
    }
}

if (! function_exists('present_gigabyte')) {
    function present_gigabyte(string $value, string $dimension = 'GB')
    {
        return (float)$value . "<span class='gb-value'>$dimension</span>";
    }
}

if (! function_exists('translate_rating')) {
    /**
     * @param int|float $rating
     * @param bool $toTrustpilot
     * @return float|int
     */
    function translate_rating($rating, bool $toTrustpilot = true)
    {
        $trustpilotMax = 5;
        if ($toTrustpilot) {
            return $trustpilotMax * $rating / RatingValueInPercents::MAX_RATING;
        }

        return $rating / $trustpilotMax * RatingValueInPercents::MAX_RATING;
    }
}

if (! function_exists('rating_to_image_stars')) {
    function rating_to_image_stars(int $rating)
    {
        $trustpilotRating = translate_rating($rating, true);

        $stars = (int)($trustpilotRating / 0.5) / 2;
        return max(1, $stars);
    }
}
