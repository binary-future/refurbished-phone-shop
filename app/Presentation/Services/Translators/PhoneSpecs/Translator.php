<?php


namespace App\Presentation\Services\Translators\PhoneSpecs;


use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Specs\Autonomy;
use App\Domain\Phone\Specs\Camera;
use App\Domain\Phone\Specs\Connection;
use App\Domain\Phone\Specs\Contracts\SpecGroup;
use App\Domain\Phone\Specs\Display;
use App\Domain\Phone\Specs\Network;
use App\Domain\Phone\Specs\OperatingSystem;
use App\Domain\Phone\Specs\Performance;
use App\Domain\Phone\Specs\PhoneCase;
use App\Domain\Phone\Specs\PhoneSpecs;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Presentation\Services\Translators\Contracts\Translator as TranslatorItem;
use App\Utils\Template\Translators\Translators\PhoneSpecs\AutonomyTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\CameraTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\ConnectionTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\DisplayTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\NetworkTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\OperatingSystemTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\PerformanceTranslator;
use App\Utils\Template\Translators\Translators\PhoneSpecs\PhoneCaseTranslator;

/**
 * Class Translator
 * @package App\Presentation\Services\Translators\PhoneSpecs
 */
class Translator
{
    /**
     * @var array
     */
    private $map = [
        Autonomy::class => AutonomyTranslator::class,
        Camera::class => CameraTranslator::class,
        Connection::class => ConnectionTranslator::class,
        Display::class => DisplayTranslator::class,
        Network::class => NetworkTranslator::class,
        OperatingSystem::class => OperatingSystemTranslator::class,
        PhoneCase::class => PhoneCaseTranslator::class,
        Performance::class => PerformanceTranslator::class,
    ];

    /**
     * @var Container
     */
    private $container;

    /**
     * Translator constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param PhoneModel $model
     * @return array
     */
    public function translateSpecs(PhoneModel $model): array
    {
        $specs = $model->getSpecs();
        if (! $specs) {
            return [];
        }

        return $this->proceed($specs);
    }

    /**
     * @param PhoneSpecs $specs
     * @return array
     */
    private function proceed(PhoneSpecs $specs): array
    {
        $specGroups = $specs->getGroupsList();
        $result = [];
        foreach ($specGroups as $group) {
            $result[] = $this->translateGroup($group);
        }

        return array_filter($result);
    }

    /**
     * @param SpecGroup $group
     * @return Specs|null
     */
    private function translateGroup(SpecGroup $group): ?Specs
    {
        try {
            $translator = $this->getTranslator($this->map[get_class($group)]);

            return $translator->translate($group);
        } catch (\Throwable $exception) {
            return null;
        }
    }

    /**
     * @param string $className
     * @return TranslatorItem
     */
    private function getTranslator(string $className): TranslatorItem
    {
        return $this->container->resolve($className);
    }
}
