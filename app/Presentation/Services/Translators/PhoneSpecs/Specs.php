<?php


namespace App\Presentation\Services\Translators\PhoneSpecs;


/**
 * Class Specs
 * @package App\Presentation\Services\Translators\PhoneSpecs
 */
final class Specs
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var array
     */
    private $content = [];

    /**
     * Specs constructor.
     * @param string $name
     * @param array $content
     */
    public function __construct(string $name, array $content)
    {
        $this->name = $name;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return ! empty($this->getContent());
    }
}
