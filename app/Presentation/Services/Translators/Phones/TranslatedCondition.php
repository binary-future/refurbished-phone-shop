<?php

namespace App\Presentation\Services\Translators\Phones;

use App\Utils\DataTypes\Enum\Enum;

/**
 * Class TranslatedCondition
 * @package App\Presentation\Services\Translators\Phones
 *
 * @method static TranslatedCondition C()
 * @method static TranslatedCondition B()
 * @method static TranslatedCondition A()
 */
class TranslatedCondition extends Enum
{
    private const A = 'pristine';
    private const B = 'very good';
    private const C = 'good';

    public function getKey()
    {
        return strtolower(parent::getKey());
    }
}
