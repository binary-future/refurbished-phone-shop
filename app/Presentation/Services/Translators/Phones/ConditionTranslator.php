<?php

namespace App\Presentation\Services\Translators\Phones;

use App\Domain\Deals\Contract\Condition;
use Illuminate\Support\Collection;

class ConditionTranslator
{
    /**
     * @param Collection $conditions
     * @return Collection
     */
    public function translate(Collection $conditions): Collection
    {
        $translatedConditions = new Collection();
        foreach ($conditions as $condition) {
            $translatedCondition = $this->getConditionMap()[$condition];
            $translatedConditions->add($translatedCondition);
        }

        return $translatedConditions;
    }

    private function getConditionMap(): array
    {
        return [
            Condition::PRISTINE()->getValue() => TranslatedCondition::A(),
            Condition::VERY_GOOD()->getValue() => TranslatedCondition::B(),
            Condition::GOOD()->getValue() => TranslatedCondition::C(),
        ];
    }
}
