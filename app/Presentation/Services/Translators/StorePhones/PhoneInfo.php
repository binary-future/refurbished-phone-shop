<?php

namespace App\Presentation\Services\Translators\StorePhones;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Shared\Image\Image;
use Illuminate\Support\Collection;

/**
 * Class PhoneInfo
 * @package App\Presentation\Services\Translators\StorePhones
 */
final class PhoneInfo
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var PhoneModel
     */
    private $model;

    /**
     * @var Collection
     */
    private $colors;

    /**
     * @var Image|null
     */
    private $image;

    /**
     * PhoneInfo constructor.
     * @param Brand $brand
     * @param PhoneModel $model
     * @param Collection $colors
     * @param Image|null $image
     */
    public function __construct(Brand $brand, PhoneModel $model, Collection $colors, Image $image = null)
    {
        $this->brand = $brand;
        $this->model = $model;
        $this->colors = $colors;
        $this->image = $image;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->model;
    }

    /**
     * @return Collection
     */
    public function getColors(): Collection
    {
        return $this->colors;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }
}