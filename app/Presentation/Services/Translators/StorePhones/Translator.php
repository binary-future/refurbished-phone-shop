<?php

namespace App\Presentation\Services\Translators\StorePhones;

use App\Domain\Phone\Phone\Phone;
use Illuminate\Support\Collection;

/**
 * Class Translator
 * @package App\Presentation\Services\Translators\StorePhones
 */
class Translator
{
    /**
     * @param Collection $phones
     * @return Collection
     */
    public function translate(Collection $phones): Collection
    {
        $groupPhones = $this->groupPhonesByModel($phones);

        return $this->getPhonesInfo($groupPhones);
    }

    /**
     * @param Collection $phones
     * @return Collection
     */
    private function groupPhonesByModel(Collection $phones): Collection
    {
        return $phones->groupBy(function (Phone $phone) {
            return $phone->getModelId();
        })->sortByDesc(function (Collection $group) {
            return $group->count();
        });
    }

    /**
     * @param Collection $groupedPhones
     * @return Collection
     */
    private function getPhonesInfo(Collection $groupedPhones)
    {
        $phonesInfo = collect();
        foreach ($groupedPhones as $group) {
            $phonesInfo->push($this->generatePhoneInfo($group));
        }


        return $phonesInfo->filter();
    }

    /**
     * @param Collection $phones
     * @return PhoneInfo|null
     */
    private function generatePhoneInfo(Collection $phones): ?PhoneInfo
    {
        if ($phones->isEmpty()) {
            return null;
        }
        $phone = $this->getPhoneFromGroup($phones);

        return new PhoneInfo(
            $phone->getModel()->getBrand(),
            $phone->getModel(),
            $this->getColors($phones),
            $phone->getImage()
        );
    }

    /**
     * @param Collection $phones
     * @return Phone
     */
    private function getPhoneFromGroup(Collection $phones): Phone
    {
        $phone = $phones->first(function (Phone $phone) {
            return (bool) $phone->getImage();
        });

        return $phone ?: $phones->first();
    }

    /**
     * @param Collection $phones
     * @return Collection
     */
    private function getColors(Collection $phones): Collection
    {
        return $phones->map(function (Phone $phone) {
            return $phone->getColor();
        })->unique();
    }
}
