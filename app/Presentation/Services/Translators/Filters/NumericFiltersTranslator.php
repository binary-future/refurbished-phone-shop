<?php


namespace App\Presentation\Services\Translators\Filters;


use App\Presentation\Services\Translators\Contracts\Translator;
use Illuminate\Support\Collection;

/**
 * Class NumericFiltersTranslator
 * @package App\Presentation\Services\Translators\Filters
 */
final class NumericFiltersTranslator implements Translator
{
    private const MIN_ITEMS_COUNT = 2;
    private const TRANSFORMING_COEF = 1.2;
    private const MULTIPLIER = 5;

    /**
     * @param Collection $data
     * @return \Illuminate\Support\Collection|mixed
     */
    public function translate($data)
    {
        if (! $data instanceof Collection && ! is_array($data)) {
            return $data;
        }

        $filteredData = $this->removeInvalid($this->prepareData($data));
        if ($filteredData->count() < self::MIN_ITEMS_COUNT) {
            return $filteredData;
        }
        $minValue = $this->generateMinValue($filteredData);
        $maxValue = $this->generateMaxValue($filteredData);
        $medianValue = $this->generateMedian($maxValue, $minValue);

        return collect([$minValue, $medianValue, $maxValue])->unique()->sort();
    }

    private function prepareData($data): Collection
    {
        return is_array($data) ? collect($data): $data;
    }

    /**
     * @param Collection $data
     * @return Collection
     */
    private function removeInvalid(Collection $data): Collection
    {
        return $data->filter(function ($item) {
            return !is_null($item) && is_numeric($item);
        })->values();
    }

    /**
     * @param Collection $values
     * @return int
     */
    private function generateMinValue(Collection $values): int
    {
        $min = round($values->min() * self::TRANSFORMING_COEF);

        return $this->increaseToMultiplier($min);
    }

    /**
     * @param Collection $values
     * @return int
     */
    private function generateMaxValue(Collection $values): int
    {
        $max = round($values->max() / self::TRANSFORMING_COEF);

        return $this->decreaseToMultiplier($max);
    }

    /**
     * @param $max
     * @param $min
     * @return int
     */
    private function generateMedian($max, $min): int
    {
        $median = round(($max + $min) / 2);

        return $this->increaseToMultiplier($median);
    }

    /**
     * @param int $value
     * @return int
     */
    private function increaseToMultiplier(int $value): int
    {
        if ($this->shouldChangeValue($value)) {
            $value = $value + self::MULTIPLIER - ($value % self::MULTIPLIER);
        }

        return $value;
    }

    /**
     * @param int $value
     * @return int
     */
    private function decreaseToMultiplier(int $value): int
    {
        if ($this->shouldChangeValue($value)) {
            $value = $value - ($value % self::MULTIPLIER);
        }

        return $value;
    }

    private function shouldChangeValue(int $value): bool
    {
        return $value % self::MULTIPLIER !== 0 && $value > self::MULTIPLIER;
    }
}
