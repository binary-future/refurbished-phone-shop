<?php

namespace App\Presentation\Services\Translators\Filters;

use App\Presentation\Services\Translators\Contracts\Translator;
use Illuminate\Support\Collection;

final class CostFiltersTranslator implements Translator
{
    private const MIN_ITEMS_COUNT = 2;
    private const MAX_MULTIPLIER = 50;
    private const MIN_MULTIPLOER = 10;
    private const MAX_ROUND_COEFFICIENT = 0.25;

    public function translate($data)
    {
        if (! $data instanceof Collection && ! is_array($data)) {
            return $data;
        }

        $filteredData = $this->removeInvalid($this->prepareData($data));
        if ($filteredData->count() < self::MIN_ITEMS_COUNT) {
            return $filteredData;
        }
        $maxValue = $this->generateMaxValue($filteredData);
        $minValue = $this->generateMinValue($filteredData);
        $medianValue = $this->generateMedianValue($minValue, $maxValue);

        return collect([$minValue, $medianValue, $maxValue])->unique()->sort();
    }

    private function prepareData($data): Collection
    {
        return is_array($data) ? collect($data): $data;
    }

    /**
     * @param Collection $data
     * @return Collection
     */
    private function removeInvalid(Collection $data): Collection
    {
        return $data->filter(function ($item) {
            return !is_null($item) && is_numeric($item);
        })->values();
    }

    private function generateMaxValue(Collection $values): int
    {
        $maxValue = $values->max();
        $yield = $maxValue % self::MAX_MULTIPLIER;
        if ($yield === 0) {
            return $maxValue;
        }
        $yieldCoefficient = $yield / self::MAX_MULTIPLIER;

        $maxValue = $yieldCoefficient < self::MAX_ROUND_COEFFICIENT
            ? floor($maxValue / self::MAX_MULTIPLIER) * self::MAX_MULTIPLIER
            : ceil($maxValue / self::MAX_MULTIPLIER) * self::MAX_MULTIPLIER;

        return (int) $maxValue;
    }

    private function generateMinValue(Collection $values): int
    {
        $minValue = $values->min();
        $yield = $minValue % self::MIN_MULTIPLOER;
        if ($yield === 0) {
            return $minValue;
        }
        $minValue = ceil($minValue / self::MIN_MULTIPLOER) * self::MIN_MULTIPLOER;

        return (int) $minValue;
    }

    private function generateMedianValue(int $minValue, int $maxValue): int
    {
        return ceil(($maxValue - $minValue) / (2 * self::MAX_MULTIPLIER)) * self::MAX_MULTIPLIER;
    }
}
