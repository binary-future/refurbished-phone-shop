<?php


namespace App\Presentation\Services\Translators\Filters;


use App\Presentation\Services\Translators\Contracts\Translator;
use Illuminate\Support\Collection;

final class MonthlyCostSliderTranslator implements Translator
{
    private const MULTIPLIER = 5;

    public function translate($data)
    {
        $data = $this->prepareData($data);
        if ($data->isEmpty()) {
            return $data;
        }
        $minValue = $this->getMinValue($data);
        $maxValue = $this->getMaxValue($data);

        return collect([$minValue, $maxValue]);
    }

    private function prepareData($data): Collection
    {
        $data = $data instanceof Collection ? $data : collect($data);

        return $data->filter(function ($item) {
            return is_numeric($item) && $item >= 0;
        });
    }

    private function getMinValue(Collection $data): int
    {
        $value = (int) $data->min();

        return $value - $value % self::MULTIPLIER;
    }

    private function getMaxValue(Collection $data): int
    {
        $value = (int) $data->max();

        return $value + self::MULTIPLIER - $value % self::MULTIPLIER;
    }
}
