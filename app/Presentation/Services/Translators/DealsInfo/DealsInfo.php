<?php

namespace App\Presentation\Services\Translators\DealsInfo;

/**
 * Class DealsInfo
 * @package App\Presentation\Services\Translators\DealsInfo
 */
final class DealsInfo
{
    /**
     * @var int
     */
    private $dealsTotalCount;

    /**
     * @var float
     */
    private $dealsBestTotalPrice;

    /**
     * @var float
     */
    private $dealsHighestTotalPrice;

    /**
     * @var PhoneDeals[]
     */
    private $phoneDeals;

    /**
     * @var bool
     */
    private $isValid = true;

    /**
     * DealsInfo constructor.
     * @param int $dealsTotalCount
     * @param float $dealsBestTotalPrice
     * @param float $dealsHighestTotalPrice
     * @param PhoneDeals[] $phoneDeals
     */
    public function __construct(
        int $dealsTotalCount,
        float $dealsBestTotalPrice,
        float $dealsHighestTotalPrice,
        array $phoneDeals
    ) {
        $this->dealsTotalCount = $dealsTotalCount;
        $this->dealsBestTotalPrice = $dealsBestTotalPrice;
        $this->dealsHighestTotalPrice = $dealsHighestTotalPrice;
        $this->phoneDeals = $phoneDeals;
    }

    /**
     * @return int
     */
    public function getDealsTotalCount(): int
    {
        return $this->dealsTotalCount;
    }

    /**
     * @return float
     */
    public function getDealsBestTotalPrice()
    {
        return sprintf('%.2f', $this->dealsBestTotalPrice);
    }

    /**
     * @return float
     */
    public function getDealsHighestTotalPrice()
    {
        return sprintf('%.2f', $this->dealsHighestTotalPrice);
    }

    /**
     * @return PhoneDeals[]
     */
    public function getPhoneDeals(): array
    {
        return $this->phoneDeals;
    }

    public function getPhoneDealsCount(): int
    {
        return count(
            array_filter(
                $this->phoneDeals,
                function (PhoneDeals $deal) {
                    return $deal->getDealsCount() > 0;
                }
            )
        );
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->isValid;
    }

    /**
     * @param bool $isValid
     */
    public function setIsValid(bool $isValid): void
    {
        $this->isValid = $isValid;
    }

    public function isEqual(DealsInfo $dealsInfo): bool
    {
        return $this->getDealsBestTotalPrice() === $dealsInfo->getDealsBestTotalPrice()
            && $this->getDealsTotalCount() === $dealsInfo->getDealsTotalCount()
            && $this->checkPhoneDeals($dealsInfo->getPhoneDeals());
    }

    private function checkPhoneDeals(array $phoneDeals): bool
    {
        return $this->getCheckStrings($phoneDeals) === $this->getCheckStrings($this->phoneDeals);
    }

    private function getCheckStrings(array $phoneDeals): string
    {
        $checkStrings = array_map(function (PhoneDeals $deals) {
            return $deals->getCheckString();
        }, $phoneDeals);
        natsort($checkStrings);

        return implode(' ', $checkStrings);
    }
}
