<?php


namespace App\Presentation\Services\Translators\DealsInfo;


final class PhoneDeals
{
    /**
     * @var string
     */
    private $phoneName;

    /**
     * @var string
     */
    private $color;

    /**
     * @var int
     */
    private $dealsCount;

    /**
     * @var float|null
     */
    private $bestUpfrontCost;

    /**
     * @var float|null
     */
    private $bestMonthlyCost;

    /**
     * PhoneDeals constructor.
     * @param string $phoneName
     * @param string $color
     * @param int $dealsCount
     * @param float $bestUpfrontCost |null
     * @param float $bestMonthlyCost |null
     */
    public function __construct(
        string $phoneName,
        string $color,
        int $dealsCount,
        float $bestUpfrontCost = null,
        float $bestMonthlyCost = null
    ) {
        $this->phoneName = $phoneName;
        $this->color = $color;
        $this->dealsCount = $dealsCount;
        $this->bestUpfrontCost = $bestUpfrontCost;
        $this->bestMonthlyCost = $bestMonthlyCost;
    }

    /**
     * @return string
     */
    public function getPhoneName(): string
    {
        return $this->phoneName;
    }

    /**
     * @return int
     */
    public function getDealsCount(): int
    {
        return $this->dealsCount;
    }

    /**
     * @return float
     */
    public function getBestUpfrontCost(): ?float
    {
        return $this->bestUpfrontCost;
    }

    /**
     * @return float
     */
    public function getBestMonthlyCost(): ?float
    {
        return $this->bestMonthlyCost;
    }

    public function getCheckString(): string
    {
        return trim(
            sprintf(
                '%s %s %s %s',
                $this->phoneName,
                $this->dealsCount,
                $this->bestUpfrontCost,
                $this->bestMonthlyCost
            )
        );
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}
