<?php

namespace App\Presentation\Services\Translators\DealsInfo;

use App\Application\Services\Search\DTO\PhonesGroupDealsInfo;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Support\Collection;

class ModelViewDealsTranslator
{
    public function translate(PhoneModel $model, DealsInfoSearchResponse $dealsInfo): DealsInfo
    {
        if (! $dealsInfo->getCheapestTotalCostDeal() || ! $dealsInfo->getBestDeal()) {
            return $this->generateFailedResponse();
        }

        return new DealsInfo(
            $this->generateDealsTotalCount($dealsInfo->getPhoneDealsInfo()),
            $dealsInfo->getCheapestTotalCostDeal()->getPayment()->getTotalCost(),
            $dealsInfo->getHighestTotalCostDeal()->getPayment()->getTotalCost(),
            $this->generatePhoneDeals($model, $dealsInfo->getPhoneDealsInfo())
        );
    }


    private function generateFailedResponse(): DealsInfo
    {
        $response = new DealsInfo(0, 0, 0, []);
        $response->setIsValid(false);

        return $response;
    }

    private function generateDealsTotalCount(array $phoneDeals): int
    {
        $totalCount = 0;
        foreach ($phoneDeals as $dealInfo) {
            /**
             * @var PhonesGroupDealsInfo $dealInfo
             */
            $totalCount += $this->calculatePhonesGroupTotalDeals($dealInfo->getPhones());
        }

        return $totalCount;
    }


    private function calculatePhonesGroupTotalDeals(Collection $phones): int
    {
        return $phones->map(static function (Phone $phone) {
            return $phone->getDealsCount();
        })->sum();
    }

    private function generatePhoneDeals(PhoneModel $model, array $phoneDealsInfo): array
    {
        $dealsInfo = [];
        foreach ($phoneDealsInfo as $phoneDeal) {
            /**
             * @var PhonesGroupDealsInfo $phoneDeal
             */
            $dealsInfo[] = new PhoneDeals(
                $this->generatePhoneName($model, $phoneDeal->getPhones()),
                $this->getColorSlug($phoneDeal->getPhones()),
                $this->calculatePhonesGroupTotalDeals($phoneDeal->getPhones()),
                $phoneDeal->getBestUpfrontCostDeal()
                    ? $phoneDeal->getBestUpfrontCostDeal()->getPayment()->getUpfrontCost()
                    : null,
                $phoneDeal->getBestMonthCostDeal()
                    ? $phoneDeal->getBestMonthCostDeal()->getPayment()->getMonthlyCost()
                    : null
            );
        }

        return $dealsInfo;
    }

    private function generatePhoneName(PhoneModel $model, Collection $phones): string
    {
        /**
         * @var Phone $phone
         */
        $phone = $phones->first();

        return trim(
            sprintf(
                '%s %s %s',
                $model->getBrand()->getName(),
                $model->getName(),
                $phone->getColor() ? $phone->getColor()->getName() : ''
            )
        );
    }

    private function getColorSlug(Collection $phones): string
    {
        /**
         * @var Phone $phone
         */
        $phone = $phones->first();

        return $phone->getColor() ? $phone->getColor()->getSlug() : '';
    }
}
