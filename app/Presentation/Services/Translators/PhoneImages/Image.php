<?php


namespace App\Presentation\Services\Translators\PhoneImages;

/**
 * Class Image
 * @package App\Presentation\Services\Translators\PhoneImages
 */
final class Image
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $color;

    /**
     * Image constructor.
     * @param string $path
     * @param string $color
     */
    public function __construct(string $path, string $color)
    {
        $this->path = $path;
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}
