<?php


namespace App\Presentation\Services\Translators\PhoneImages;

use App\Domain\Phone\Phone\Phone;
use App\Presentation\Services\Translators\Contracts\Translator as Contract;
use Illuminate\Support\Collection;

final class Translator implements Contract
{
    public function translate($data)
    {
        $images = collect();
        /**
         * @var Collection $data
         */
        $groupedPhones = $this->groupPhonesByColor($data);
        foreach ($groupedPhones as $color => $group) {
            $images = $images->push($this->generateImage($group, $color));
        }

        return $images->filter();
    }

    private function groupPhonesByColor(Collection $phones): Collection
    {
        return $phones->groupBy(function (Phone $phone) {
            return $phone->getColor()->getKey();
        });
    }

    private function generateImage(Collection $phones, string $color): ?Image
    {
        $imagePath = $this->getImagePathFromGroup($phones);

        return $imagePath ? new Image($imagePath, $color) : null;
    }

    private function getImagePathFromGroup(Collection $phones): ?string
    {
        foreach ($phones as $phone) {
            if ($phone->getImage()) {
                return $phone->getImage()->getPath();
            }
        }

        return null;
    }
}
