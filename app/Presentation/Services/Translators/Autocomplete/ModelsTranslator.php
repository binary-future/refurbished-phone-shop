<?php

namespace App\Presentation\Services\Translators\Autocomplete;

use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Translators\Contracts\Translator;

/**
 * Class ModelsTranslator
 * @package App\Presentation\Services\Translators\Autocomplete
 */
final class ModelsTranslator implements Translator
{
    /**
     * @param $data
     * @return array|mixed
     */
    public function translate($data)
    {
        if (!is_iterable($data)) {
            throw new \InvalidArgumentException('Data should be iterable');
        }

        return $this->translateModels($this->filterModels($data));
    }

    /**
     * @param iterable $data
     * @return array
     */
    private function filterModels(iterable $data): array
    {
        $result = [];
        foreach ($data as $item) {
            if ($item instanceof PhoneModel) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param array $models
     * @return array
     */
    private function translateModels(array $models): array
    {
        return array_map(function (PhoneModel $model) {
            return [
                'id' => $model->getKey(),
                'name' => sprintf('%s %s', $model->getBrand()->getName(), $model->getName()),
                'image' => $model->getThumb() ? asset($model->getThumb()->getPath()) : asset('images/no-image.jpg'),
                'url' => route('models.view', ['brand' => $model->getBrand()->getSlug(), 'model' => $model->getSlug()])
            ];
        }, $models);
    }
}
