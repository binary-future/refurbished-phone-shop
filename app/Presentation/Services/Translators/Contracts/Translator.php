<?php


namespace App\Presentation\Services\Translators\Contracts;


/**
 * Interface Translator
 * @package App\Presentation\Services\Translators\Contracts
 */
interface Translator
{
    /**
     * @param $data
     * @return mixed
     */
    public function translate($data);
}
