<?php

namespace App\Presentation\Services\Request\Preprocessors;

use App\Application\Services\Search\Filters\Contracts\PrimaryDealsAdjustedFilters;
use App\Application\UseCases\Phones\Requests\ModelDealsFilterRequest;
use App\Presentation\Services\Request\Preprocessors\Contracts\ModelDealFilterPreprocessor as Contract;
use App\Presentation\Services\Request\Preprocessors\Contracts\RangedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Utils\Serializer\Contracts\Serializer;
use App\Presentation\Services\Request\Preprocessors\Contracts\CommaSeparatedParamsPreprocessor;

/**
 * Class ModelDealFilterPreprocessor
 * @package App\Presentation\Services\Request\Preprocessors
 */
final class ModelDealFilterPreprocessor implements Contract
{
    private const FILTER_BRAND = 'brand';
    private const FILTER_MODEL = 'model';
    private const FILTER_NETWORKS = PrimaryDealsAdjustedFilters::FILTER_NETWORKS;
    private const FILTER_CONDITIONS = PrimaryDealsAdjustedFilters::FILTER_CONDITIONS;
    private const FILTER_MONTHLY_COST = PrimaryDealsAdjustedFilters::FILTER_MONTHLY_COST;
    private const FILTER_MONTHLY_COST_RANGE = PrimaryDealsAdjustedFilters::FILTER_MONTHLY_COST_RANGE;
    private const FILTER_TOTAL_COST = PrimaryDealsAdjustedFilters::FILTER_TOTAL_COST;
    private const FILTER_TOTAL_COST_RANGE = PrimaryDealsAdjustedFilters::FILTER_TOTAL_COST_RANGE;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var CommaSeparatedParamsPreprocessor
     */
    private $commaSeparatedParamsPreprocessor;

    /**
     * @var array
     */
    private $rangedMap = [
        self::FILTER_MONTHLY_COST => self::FILTER_MONTHLY_COST_RANGE,
        self::FILTER_TOTAL_COST => self::FILTER_TOTAL_COST_RANGE
    ];
    /**
     * @var RangedParamsPreprocessor
     */
    private $rangedParamsPreprocessor;

    /**
     * ModelDealFilterPreprocessor constructor.
     * @param Serializer $serializer
     * @param CommaSeparatedParamsPreprocessor $commaSeparatedParamsPreprocessor
     * @param RangedParamsPreprocessor $rangedParamsPreprocessor
     */
    public function __construct(
        Serializer $serializer,
        CommaSeparatedParamsPreprocessor $commaSeparatedParamsPreprocessor,
        RangedParamsPreprocessor $rangedParamsPreprocessor
    ) {
        $this->serializer = $serializer;
        $this->commaSeparatedParamsPreprocessor = $commaSeparatedParamsPreprocessor;
        $this->rangedParamsPreprocessor = $rangedParamsPreprocessor;
    }

    /**
     * @param array $params
     * @param string $brand
     * @param string $model
     * @return ModelDealsFilterRequest
     * @throws RequestPreprocessorException
     */
    public function process(array $params, string $brand, string $model): ModelDealsFilterRequest
    {
        try {
            return $this->proceed($params, $brand, $model);
        } catch (\Throwable $exception) {
            throw RequestPreprocessorException::cannotProcessedRequest(
                ModelDealsFilterRequest::class,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param array $params
     * @param string $brand
     * @param string $model
     * @return ModelDealsFilterRequest
     * @throws RequestPreprocessorException
     */
    private function proceed(array $params, string $brand, string $model): ModelDealsFilterRequest
    {
        $params = $this->rangedParamsPreprocessor->process($params, $this->rangedMap)->getParams();
        $params[self::FILTER_BRAND] = $brand;
        $params[self::FILTER_MODEL] = $model;
        $params[self::FILTER_NETWORKS] = isset($params[self::FILTER_NETWORKS])
            ? $this->commaSeparatedParamsPreprocessor->process($params[self::FILTER_NETWORKS])
            : [];
        $params[self::FILTER_CONDITIONS] = isset($params[self::FILTER_CONDITIONS])
            ? $this->commaSeparatedParamsPreprocessor->process($params[self::FILTER_CONDITIONS])
            : [];
        /**
         * @var ModelDealsFilterRequest $request
         */
        $request = $this->serializer->fromArray(ModelDealsFilterRequest::class, $params);

        return $request;
    }
}
