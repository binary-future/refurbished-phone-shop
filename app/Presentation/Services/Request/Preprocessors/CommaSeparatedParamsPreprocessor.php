<?php

namespace App\Presentation\Services\Request\Preprocessors;

use App\Presentation\Services\Request\Preprocessors\Contracts\CommaSeparatedParamsPreprocessor as Contract;

/**
 * Class CommaSeparatedParamsPreprocessor
 * @package App\Presentation\Services\Request\Preprocessors
 */
final class CommaSeparatedParamsPreprocessor implements Contract
{
    public function process(string $param): array
    {
        return explode(',', $param);
    }
}
