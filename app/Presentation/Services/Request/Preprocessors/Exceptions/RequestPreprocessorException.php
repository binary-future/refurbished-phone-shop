<?php


namespace App\Presentation\Services\Request\Preprocessors\Exceptions;


/**
 * Class RequestPreprocessorException
 * @package App\Presentation\Services\Request\Preprocessors\Exceptions
 */
final class RequestPreprocessorException extends \Exception
{
    /**
     * @param string $targetName
     * @param string|null $error
     * @return RequestPreprocessorException
     */
    public static function cannotProcessedRequest(string $targetName, string $error = null)
    {
        return new self(
            trim(
                sprintf(
                    'Cannot process request into %s. %s',
                    $targetName,
                    $error ?: ''
                )
            )
        );
    }
}
