<?php

namespace App\Presentation\Services\Request\Preprocessors;

use App\Presentation\Services\Request\Preprocessors\Contracts\RangedParamsPreprocessor as Contract;
use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Presentation\Services\Request\Preprocessors\Request\RangedParamsRequest;

final class RangedParamsPreprocessor implements Contract
{

    /**
     * @param array $params
     * @param array $rangedMap
     * @return RangedParamsRequest
     * @throws RequestPreprocessorException
     */
    public function process(array $params, array $rangedMap): RangedParamsRequest
    {
        try {
            return $this->proceed($params, $rangedMap);
        } catch (\Throwable $exception) {
            throw RequestPreprocessorException::cannotProcessedRequest(
                RangedParamsRequest::class,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param array $params
     * @param array $rangedMap
     * @return RangedParamsRequest
     */
    private function proceed(array $params, array $rangedMap): RangedParamsRequest
    {
        $params = $this->prepareParams($params, $rangedMap);

        return new RangedParamsRequest($params);
    }

    /**
     * @param array $params
     * @param array $rangedMap
     * @return array
     */
    private function prepareParams(array $params, array $rangedMap): array
    {
        foreach ($rangedMap as $index => $rangedIndex) {
            $params = isset($params[$index]) ? $this->prepareRangedParam($params, $index, $rangedIndex) : $params;
        }

        return $params;
    }

    /**
     * @param array $params
     * @param string $index
     * @param string $rangedIndex
     * @return array
     */
    private function prepareRangedParam(array $params, string $index, string $rangedIndex): array
    {
        $param = $params[$index];
        unset($params[$index]);
        $values = explode(',', $param);
        $params[$rangedIndex] = count($values) === 1 ? [current($values), current($values)] : $values;

        return $params;
    }
}
