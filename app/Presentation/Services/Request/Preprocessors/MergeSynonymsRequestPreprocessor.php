<?php


namespace App\Presentation\Services\Request\Preprocessors;

use App\Application\Services\Merge\Model\DTO\ParentModelId;
use App\Application\Services\Merge\Model\DTO\SynonymsIds;
use App\Application\Services\Merge\Model\MergeGroup;
use App\Application\UseCases\Backend\Merge\Tool\Models\Requests\MergeSynonymsRequest;
use App\Presentation\Services\Request\Preprocessors\Contracts\MergeSynonymsRequestPreprocessor as Contract;
use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;

final class MergeSynonymsRequestPreprocessor implements Contract
{
    public function process(array $params): MergeSynonymsRequest
    {
        try {
            return $this->proceed($params);
        } catch (\Throwable $exception) {
            throw RequestPreprocessorException::cannotProcessedRequest(
                MergeSynonymsRequest::class,
                $exception->getMessage()
            );
        }
    }

    private function proceed(array $params): MergeSynonymsRequest
    {
        $result = [];
        foreach ($params['models'] as $param) {
            $result[] = $this->buildGroup($param);
        }

        return new MergeSynonymsRequest($result);
    }

    private function buildGroup(array $data): MergeGroup
    {
        return new MergeGroup(
            new ParentModelId($data['parent']),
            new SynonymsIds(json_decode($data['synonyms'], true))
        );
    }
}
