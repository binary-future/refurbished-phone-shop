<?php


namespace App\Presentation\Services\Request\Preprocessors\Request;


class RangedParamsRequest
{
    /**
     * @var array
     */
    private $params;

    /**
     * RangeParamsRequest constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
