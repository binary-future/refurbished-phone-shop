<?php

namespace App\Presentation\Services\Request\Preprocessors\Contracts;

/**
 * Interface NetworkDealFilterPreprocessor
 * @package App\Presentation\Services\Request\Preprocessors\Contracts
 */
interface CommaSeparatedParamsPreprocessor
{
    /**
     * @param string $param
     * @return array
     */
    public function process(string $param): array;
}
