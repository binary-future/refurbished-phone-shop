<?php


namespace App\Presentation\Services\Request\Preprocessors\Contracts;


use App\Application\UseCases\Phones\Requests\BrandDealsFilterRequest;

/**
 * Interface BrandDealFilterPreprocessor
 * @package App\Presentation\Services\Request\Preprocessors\Contracts
 */
interface BrandDealFilterPreprocessor
{
    /**
     * @param array $params
     * @param string $brand
     * @return BrandDealsFilterRequest
     */
    public function process(array $params, string $brand): BrandDealsFilterRequest;
}
