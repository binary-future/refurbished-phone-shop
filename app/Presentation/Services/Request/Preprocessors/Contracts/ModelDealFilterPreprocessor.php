<?php


namespace App\Presentation\Services\Request\Preprocessors\Contracts;


use App\Application\UseCases\Phones\Requests\ModelDealsFilterRequest;

interface ModelDealFilterPreprocessor
{
    public function process(array $params, string $brand, string $model): ModelDealsFilterRequest;
}