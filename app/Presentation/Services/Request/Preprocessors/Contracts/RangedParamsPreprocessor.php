<?php

namespace App\Presentation\Services\Request\Preprocessors\Contracts;

use App\Presentation\Services\Request\Preprocessors\Exceptions\RequestPreprocessorException;
use App\Presentation\Services\Request\Preprocessors\Request\RangedParamsRequest;

interface RangedParamsPreprocessor
{
    /**
     * @param array $params
     * @param array $rangedMap
     * @return RangedParamsRequest
     * @throws RequestPreprocessorException
     */
    public function process(array $params, array $rangedMap): RangedParamsRequest;
}
