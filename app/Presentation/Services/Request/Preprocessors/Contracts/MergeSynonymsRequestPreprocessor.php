<?php


namespace App\Presentation\Services\Request\Preprocessors\Contracts;


use App\Application\UseCases\Backend\Merge\Tool\Models\Requests\MergeSynonymsRequest;

interface MergeSynonymsRequestPreprocessor
{
    public function process(array $params): MergeSynonymsRequest;
}
