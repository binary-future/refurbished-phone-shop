<?php


namespace App\Presentation\Services\Sitemaps;


use App\Application\UseCases\Main\Responses\SitemapsResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Store\Store;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Sitemaps\Contracts\Sitemaps;
use Carbon\Carbon;
use Illuminate\Support\Collection;

final class Generator
{
    /**
     * @var Sitemaps
     */
    private $sitemaps;

    /**
     * Generator constructor.
     * @param Config $config
     * @param Sitemaps $sitemaps
     */
    public function __construct(Sitemaps $sitemaps)
    {
        $this->sitemaps = $sitemaps;
    }

    /**
     * @param SitemapsResponse $response
     * @return Sitemaps
     */
    public function generate(SitemapsResponse $response): Sitemaps
    {
        $this->generateMain()
            ->generateBrands($response->getBrands())
            ->generateModels($response->getModels())
            ->generateStores($response->getStores());

        return $this->sitemaps;
    }

    private function generateMain()
    {
        $this->sitemaps->addTag(
            route('home'),
            Carbon::now(),
            'daily',
            '1.0'
        );

        return $this;
    }

    private function generateBrands(Collection $brands)
    {
        foreach ($brands as $brand) {
            /**
             * @var Brand $brand
             */
            $this->sitemaps
                ->addTag(
                    route('manufacturers.view', [
                        'brand' => $brand->getSlug()
                    ]),
                    $brand->getUpdatedAt(),
                    'daily',
                    '0.8'
                );
        }

        return $this;
    }

    private function generateModels(Collection $models)
    {
        foreach ($models as $model) {
            /**
             * @var PhoneModel $model
             */
            $this->sitemaps
                ->addTag(
                    route('models.view', [
                        'brand' => $model->getBrand()->getSlug(),
                        'model' => $model->getSlug()
                    ]),
                    $model->getUpdatedAt(),
                    'daily',
                    '0.6'
                );
        }

        return $this;
    }

    private function generateStores(Collection $stores)
    {
        foreach ($stores as $store) {
            /**
             * @var Store $store
             */
            $this->sitemaps
                ->addTag(
                    route('stores.view', [
                        'store' => $store->getSlug()
                    ]),
                    $store->getUpdatedAt(),
                    'daily',
                    '0.6'
                );
        }

        return $this;
    }
}
