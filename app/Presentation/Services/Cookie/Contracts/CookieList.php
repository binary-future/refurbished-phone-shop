<?php

namespace App\Presentation\Services\Cookie\Contracts;

interface CookieList
{
    public const MODEL_VIEW_SORTING_COOKIE_ALIAS = 'ph_deal_srt';
}