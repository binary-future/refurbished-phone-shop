<?php


namespace App\Presentation\Services\Generators\Title\AssignmentScenarios;


use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\Contracts\AssignmentScenario;

class ByPhoneModelIdAssignmentScenario implements AssignmentScenario
{
    public function getKey(array $titles, PhoneModel $model)
    {
        $count = count($titles);

        return $model->getKey() % $count;
    }
}
