<?php


namespace App\Presentation\Services\Generators\Title;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\Contracts\AssignmentScenario;
use App\Presentation\Services\Generators\Title\Contracts\TitleConstructor;
use App\Presentation\Services\Generators\Title\Exceptions\TitleGeneratorException;
use App\Utils\Adapters\Config\Contracts\Config;

class TitleGenerator implements Contracts\TitleGenerator
{
    private const CONFIG = 'titles.schema.deals';

    /**
     * @var Config
     */
    private $config;
    /**
     * @var AssignmentScenario
     */
    private $scenario;

    /**
     * TitleGenerator constructor.
     * @param Config $config
     * @param AssignmentScenario $scenario
     */
    public function __construct(Config $config, AssignmentScenario $scenario)
    {
        $this->config = $config;
        $this->scenario = $scenario;
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @return string
     */
    public function generate(Brand $brand, PhoneModel $model): string
    {
        try {
            return $this->execute($brand, $model);
        } catch (\Throwable $exception) {
            throw TitleGeneratorException::generationFailed($exception);
        }
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @return string
     */
    private function execute(Brand $brand, PhoneModel $model): string
    {
        $templates = $this->getTemplates();

        $key = $this->getTemplateKey($model, $templates);

        return $this->constructTitle($brand, $model, $templates, $key);
    }

    /**
     * @return array
     */
    private function getTemplates(): array
    {
        return $this->config->get(self::CONFIG);
    }

    /**
     * @param PhoneModel $model
     * @param array $templates
     * @return mixed
     */
    private function getTemplateKey(PhoneModel $model, array $templates)
    {
        return $this->scenario->getKey($templates, $model);
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @param array $templates
     * @param $key
     * @return string
     */
    private function constructTitle(Brand $brand, PhoneModel $model, array $templates, $key): string
    {
        $template = $templates[$key]['template'];
        $class = $templates[$key]['constructor'];

        $constructor = new $class;

        if (! $constructor instanceof TitleConstructor) {
            throw new \InvalidArgumentException(
                'Constructor must be instance of TitleConstructor interface'
            );
        }

        return $constructor->execute($template, $brand, $model);
    }
}
