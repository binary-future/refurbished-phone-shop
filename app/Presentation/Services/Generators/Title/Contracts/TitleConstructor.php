<?php

namespace App\Presentation\Services\Generators\Title\Contracts;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;

interface TitleConstructor
{
    /**
     * @param string $template
     * @param Brand $brand
     * @param PhoneModel $model
     * @return string
     */
    public function execute(string $template, Brand $brand, PhoneModel $model): string ;
}
