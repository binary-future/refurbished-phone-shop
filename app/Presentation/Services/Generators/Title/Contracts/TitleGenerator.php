<?php

namespace App\Presentation\Services\Generators\Title\Contracts;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;

interface TitleGenerator
{
    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @return string
     */
    public function generate(Brand $brand, PhoneModel $model): string;
}
