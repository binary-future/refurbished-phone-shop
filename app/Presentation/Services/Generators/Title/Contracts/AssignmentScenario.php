<?php

namespace App\Presentation\Services\Generators\Title\Contracts;

use App\Domain\Phone\Model\PhoneModel;

interface AssignmentScenario
{
    public function getKey(array $titles, PhoneModel $model);
}
