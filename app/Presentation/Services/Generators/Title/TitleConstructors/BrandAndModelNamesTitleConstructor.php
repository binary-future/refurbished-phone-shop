<?php


namespace App\Presentation\Services\Generators\Title\TitleConstructors;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\Contracts\TitleConstructor;

class BrandAndModelNamesTitleConstructor implements TitleConstructor
{
    /**
     * @param string $template
     * @param Brand $brand
     * @param PhoneModel $model
     * @return string
     */
    public function execute(string $template, Brand $brand, PhoneModel $model): string
    {
        return sprintf($template, $brand->getName(), $model->getName());
    }
}
