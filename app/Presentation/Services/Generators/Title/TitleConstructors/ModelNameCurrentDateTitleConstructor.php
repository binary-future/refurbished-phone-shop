<?php


namespace App\Presentation\Services\Generators\Title\TitleConstructors;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\Generators\Title\Contracts\TitleConstructor;

class ModelNameCurrentDateTitleConstructor implements TitleConstructor
{
    /**
     * @param string $template
     * @param Brand $brand
     * @param PhoneModel $model
     * @return string
     * @throws \Exception
     */
    public function execute(string $template, Brand $brand, PhoneModel $model): string
    {
        $date = (new \DateTime())->format('M');
        return sprintf($template, $brand->getName(), $model->getName(), $date);
    }
}
