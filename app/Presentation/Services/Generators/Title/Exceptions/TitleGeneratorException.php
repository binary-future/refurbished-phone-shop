<?php


namespace App\Presentation\Services\Generators\Title\Exceptions;


class TitleGeneratorException extends \RuntimeException
{
    /**
     * @param \Throwable $throwable
     * @return TitleGeneratorException
     */
    public static function generationFailed(\Throwable $throwable): TitleGeneratorException
    {
        return new self('Title generation failed', 0, $throwable);
    }
}
