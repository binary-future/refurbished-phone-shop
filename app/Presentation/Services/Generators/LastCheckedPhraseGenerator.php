<?php

namespace App\Presentation\Services\Generators;

use Carbon\Carbon;

class LastCheckedPhraseGenerator
{
    private const HOURS_IN_DAY = 24;
    private const HOURS_IN_SIX_DAYS = 144;
    private const ONE_HOUR = 1;
    private const MAX_DAY_DIFFERENCE = 14;

    /**
     * @param Carbon $checkedDate
     * @return string
     */
    public function generatePhrase(Carbon $checkedDate): ?string
    {
        $currentDate = Carbon::now();
        if ($currentDate->diffInDays($checkedDate) > self::MAX_DAY_DIFFERENCE) {
            return null;
        }

        return $this->getPhraseByHours($currentDate->diffInHours($checkedDate), $checkedDate);
    }

    /**
     * @param Carbon $checkedDate
     * @param int $moreThan
     * @return string|null
     */
    public function generateFakePhrase(Carbon $checkedDate, int $moreThan = 0): ?string
    {
        $currentDate = Carbon::now();
        return $currentDate->diffInDays($checkedDate) >= $moreThan
            ? $this->generatePhrase($currentDate->addHour(2))
            : $this->generatePhrase($checkedDate);
    }

    /**
     * @param $hours
     * @param $date
     * @return string
     */
    private function getPhraseByHours($hours, Carbon $date)
    {
        $formattedDate = $date->toFormattedDateString();

        if ($hours < self::ONE_HOUR) {
            $formattedDate = 'a few minutes ago';
        } elseif ($hours === self::ONE_HOUR) {
            $formattedDate = 'an hour ago';
        } elseif ($hours > self::ONE_HOUR && $hours < self::HOURS_IN_DAY) {
            $formattedDate = sprintf('%d hours ago', $hours);
        } elseif ($hours >= self::HOURS_IN_DAY && $hours < self::HOURS_IN_SIX_DAYS) {
            $formattedDate = sprintf('%d days ago', $hours / self::HOURS_IN_DAY + self::ONE_HOUR);
        }

        return $formattedDate;
    }
}
