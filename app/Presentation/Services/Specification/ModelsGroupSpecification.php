<?php


namespace App\Presentation\Services\Specification;


use App\Utils\Specification\Contracts\Specification;

/**
 * Class ModelsGroupSpecification
 * @package App\Presentation\Services\Specification
 */
final class ModelsGroupSpecification implements Specification
{
    /**
     * @var array
     */
    private $groupsList = [
        [
            'brand' => 'apple',
            'group' => 'iphone'
        ],
        [
            'brand' => 'samsung',
            'group' => 'galaxy'
        ]
    ];

    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        foreach ($this->groupsList as $group) {
            if ($group['brand'] === ($value['brand'] ?? null) && $group['group'] === ($value['model'] ?? null)) {
                return true;
            }
        }

        return false;
    }
}
