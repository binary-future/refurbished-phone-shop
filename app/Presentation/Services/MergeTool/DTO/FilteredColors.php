<?php

namespace App\Presentation\Services\MergeTool\DTO;

use App\Application\Services\Merge\Color\Filter;
use App\Domain\Phone\Phone\Color;
use Illuminate\Support\Collection;

final class FilteredColors
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var Color|null
     */
    private $baseColor;

    /**
     * @var Color[]
     */
    private $synonyms = [];

    /**
     * Filtered constructor.
     * @param Filter $filter
     * @param Color|null $baseColor
     * @param Color[] $synonyms
     */
    public function __construct(Filter $filter, Color $baseColor = null, array $synonyms = [])
    {
        $this->filter = $filter;
        $this->baseColor = $baseColor;
        $this->synonyms = $synonyms;
    }

    /**
     * @return Filter
     */
    public function getFilter(): Filter
    {
        return $this->filter;
    }

    /**
     * @return Color|null
     */
    public function getBaseColor(): ?Color
    {
        return $this->baseColor;
    }

    /**
     * @return Color[]
     */
    public function getSynonyms(): array
    {
        return array_unique($this->synonyms);
    }

    /**
     * @param Color|null $baseColor
     */
    public function setBaseColor(Color $baseColor): void
    {
        $this->baseColor = $baseColor;
    }

    public function addSynonym(Color $color): void
    {
        $this->synonyms[] = $color;
    }

    public function addSynonyms(Collection $colors): void
    {
        $this->synonyms = array_merge($this->synonyms, $colors->all());
    }
}