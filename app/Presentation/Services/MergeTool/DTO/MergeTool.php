<?php


namespace App\Presentation\Services\MergeTool\DTO;


final class MergeTool
{
    /**
     * @var array
     */
    private $filtered;

    /**
     * @var NotFiltered
     */
    private $unfiltered;

    /**
     * MergeTool constructor.
     * @param array $filtered
     * @param NotFiltered $unfiltered
     */
    public function __construct(array $filtered, NotFiltered $unfiltered)
    {
        $this->filtered = $filtered;
        $this->unfiltered = $unfiltered;
    }

    /**
     * @return array
     */
    public function getFiltered(): array
    {
        return $this->filtered;
    }

    /**
     * @return NotFiltered
     */
    public function getUnfiltered(): NotFiltered
    {
        return $this->unfiltered;
    }
}
