<?php


namespace App\Presentation\Services\MergeTool\DTO;


use App\Application\Services\Merge\Model\Filter;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Support\Collection;

final class Filtered
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var PhoneModel|null
     */
    private $baseModel;

    /**
     * @var PhoneModel[]
     */
    private $synonyms = [];

    /**
     * Filtered constructor.
     * @param Filter $filter
     * @param PhoneModel|null $baseModel
     * @param PhoneModel[] $synonyms
     */
    public function __construct(Filter $filter, PhoneModel $baseModel = null, array $synonyms = [])
    {
        $this->filter = $filter;
        $this->baseModel = $baseModel;
        $this->synonyms = $synonyms;
    }

    /**
     * @return Filter
     */
    public function getFilter(): Filter
    {
        return $this->filter;
    }

    /**
     * @return PhoneModel|null
     */
    public function getBaseModel(): ?PhoneModel
    {
        return $this->baseModel;
    }

    /**
     * @return PhoneModel[]
     */
    public function getSynonyms(): array
    {
        return array_unique($this->synonyms);
    }

    /**
     * @param PhoneModel|null $baseModel
     */
    public function setBaseModel(PhoneModel $baseModel): void
    {
        $this->baseModel = $baseModel;
    }

    public function addSynonym(PhoneModel $phoneModel): void
    {
        $this->synonyms[] = $phoneModel;
    }

    public function addSynonyms(Collection $models): void
    {
        $this->synonyms = array_merge($this->synonyms, $models->all());
    }
}
