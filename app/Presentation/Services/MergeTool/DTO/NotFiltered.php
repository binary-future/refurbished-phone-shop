<?php


namespace App\Presentation\Services\MergeTool\DTO;


use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Color;
use Illuminate\Support\Collection;

final class NotFiltered
{
    /**
     * @var Collection
     */
    private $models;

    /**
     * NotFiltered constructor.
     * @param Collection $models
     */
    public function __construct(Collection $models)
    {
        if (! $this->isValid($models)) {
            throw new \InvalidArgumentException('Invalid collection content');
        }
        $this->models = $models;
    }

    private function isValid(Collection $models): bool
    {
        return $this->isPhoneModels($models) || $this->isColors($models);
    }

    private function isPhoneModels(Collection $models): bool
    {
        foreach ($models as $model) {
            if (! $model instanceof PhoneModel) {
                return false;
            }
        }

        return true;
    }

    private function isColors(Collection $models): bool
    {
        foreach ($models as $model) {
            if (! $model instanceof Color) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }
}
