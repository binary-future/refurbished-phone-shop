<?php


namespace App\Presentation\Services\MergeTool;


use App\Application\Services\Merge\Model\Filter;
use App\Domain\Phone\Model\PhoneModel;
use App\Presentation\Services\MergeTool\DTO\Filtered;
use App\Presentation\Services\MergeTool\DTO\MergeTool;
use App\Presentation\Services\MergeTool\DTO\NotFiltered;
use Illuminate\Support\Collection;

final class ModelGroupsGenerator
{
    /**
     * @var array
     */
    private $unFilteredModels = [];

    private $filtered = [];

    public function generate(Collection $models, Collection $filters): MergeTool
    {
        try {
            return $this->proceedGeneration($models, $filters);
        } catch (\Throwable $exception) {
            return new MergeTool([], new NotFiltered($models));
        }
    }

    private function proceedGeneration(Collection $models, Collection $filters): MergeTool
    {
        $this->fillFiltered($this->prepareFilters($filters));
        $this->filterModels($this->prepareModels($models));

        return new MergeTool($this->filtered, new NotFiltered(collect($this->unFilteredModels)));
    }

    private function prepareFilters(Collection $filters): Collection
    {
        return $filters->sortByDesc(function (Filter $filter) {
            return strlen($filter->getFilter());
        });
    }

    private function fillFiltered(Collection $filters)
    {
        foreach ($filters as $filter) {
            /**
             * @var Filter
             */
            $this->generateFiltered($filter);
        }
    }

    private function generateFiltered(Filter $filter): Filtered
    {
        $filtered = new Filtered($filter);
        $this->filtered[] = $filtered;

        return $filtered;
    }

    private function prepareModels(Collection $models): Collection
    {
        return $models->filter(function (PhoneModel $model) {
            return ! $model->isSynonym();
        });
    }

    private function filterModels(Collection $models)
    {
        foreach ($this->filtered as $filtered) {
            $models = $this->assignFilter($filtered, $models);
        }

        $this->unFilteredModels = $models->all();
        $this->filtered = $this->sortFiltered();
    }

    private function assignFilter(Filtered $filtered, Collection $models): Collection
    {
        $filter = $filtered->getFilter();
        $result = collect([]);
        $filteredModels = collect([]);
        foreach ($models as $model) {
            /**
             * @var PhoneModel $model
             */
            $this->shouldFilter($filter, $model)
                ? $filteredModels->push($model)
                : $result->push($model);
        }

        if ($filter->hasChildren()) {
            foreach ($filter->getChildren() as $child) {
                $filteredChild = $this->generateFiltered($child);
                $filteredModels = $this->assignFilter($filteredChild, $filteredModels);
            }
        }
        $this->prepareFiltered($filtered, $filteredModels);

        return $result;
    }

    private function prepareFiltered(Filtered $filtered, Collection $models)
    {
        $baseModel = $models->first(function (PhoneModel $model) {
            return $model->hasSynonyms();
        });
        if ($baseModel) {
            $filtered->setBaseModel($baseModel);
            $models = $models->diff(collect([$baseModel]));
        }
        $filtered->addSynonyms($models);
    }

    private function shouldFilter(Filter $filter, PhoneModel $model): bool
    {
        return stripos(strtolower($model->getName()), strtolower($filter->getFilter())) !== false;
    }

    private function sortFiltered()
    {
        return collect($this->filtered)->sortBy(function (Filtered $filtered) {
            return $filtered->getFilter()->getName();
        })->all();
    }
}
