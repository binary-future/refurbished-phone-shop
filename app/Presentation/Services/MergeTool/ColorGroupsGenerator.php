<?php


namespace App\Presentation\Services\MergeTool;


use App\Application\Services\Merge\Color\Filter;
use App\Domain\Phone\Phone\Color;
use App\Presentation\Services\MergeTool\DTO\FilteredColors;
use App\Presentation\Services\MergeTool\DTO\MergeTool;
use App\Presentation\Services\MergeTool\DTO\NotFiltered;
use Illuminate\Support\Collection;

final class ColorGroupsGenerator
{
    /**
     * @var array
     */
    private $unFilteredColors = [];

    private $filtered = [];

    /**
     * @var Collection
     */
    private $baseFilters;

    /**
     * @var Collection
     */
    private $childFilters;

    public function generate(Collection $colors, Collection $filters): MergeTool
    {
        try {
            return $this->proceedGeneration($colors, $filters);
        } catch (\Throwable $exception) {
            return new MergeTool([], new NotFiltered($colors));
        }
    }

    private function proceedGeneration(Collection $colors, Collection $filters): MergeTool
    {
        $this->prepareFilters($filters);
        $this->filterColors($this->prepareColors($colors));

        return new MergeTool($this->filtered, new NotFiltered(collect($this->unFilteredColors)));
    }

    private function prepareFilters(Collection $filters)
    {
        $this->baseFilters = $this->sortFilters($filters->filter(function (Filter $filter) {
            return $filter->getFilterId() === null;
        }));
        $this->childFilters = $filters->diff($this->baseFilters)->groupBy(Filter::FIELD_FILTER_ID);
    }

    private function sortFilters(Collection $filters): Collection
    {
        return $filters->sortByDesc(function (Filter $filter) {
            return strlen($filter->getFilter());
        });
    }

    private function prepareColors(Collection $colors): Collection
    {
        return $colors->filter(function (Color $color) {
            return ! $color->isSynonym();
        });
    }

    private function filterColors(Collection $colors)
    {
        foreach ($this->baseFilters as $filter) {
            $colors = $this->assignFilter($filter, $colors);
        }
        $this->unFilteredColors = $colors->all();
        $this->filtered = $this->sortFiltered();
    }

    private function assignFilter(Filter $filter, Collection $colors): Collection
    {
        $result = collect([]);
        $filteredColors = collect([]);
        foreach ($colors as $color) {
            /**
             * @var Color $color
             */
            $this->shouldFilter($filter, $color)
                ? $filteredColors->push($color)
                : $result->push($color);
        }
        if ($this->hasChildren($filter)) {
            $children = $this->getChildren($filter);
            foreach ($children as $child) {
                $filteredColors = $this->assignFilter($child, $filteredColors);
            }
        }
        $this->generateFiltered($filter, $filteredColors);

        return $result;
    }

    private function shouldFilter(Filter $filter, Color $color): bool
    {
        return stripos(strtolower($color->getName()), strtolower($filter->getFilter())) !== false;
    }

    private function hasChildren(Filter $filter): bool
    {
        return $this->childFilters->has($filter->getKey());
    }

    private function getChildren(Filter $filter): Collection
    {
        return $this->childFilters->get($filter->getKey());
    }

    private function generateFiltered(Filter $filter, Collection $colors)
    {
        $filtered = new FilteredColors($filter);
        $baseColor = $colors->first(function (Color $color) {
            return $color->hasSynonyms();
        });
        if ($baseColor) {
            $filtered->setBaseColor($baseColor);
            $colors = $colors->diff(collect([$baseColor]));
        }
        $filtered->addSynonyms($colors);
        $this->filtered[] = $filtered;
    }

    private function sortFiltered()
    {
        return collect($this->filtered)->sortBy(function (FilteredColors $filtered) {
            return $filtered->getFilter()->getName();
        })->all();
    }
}
