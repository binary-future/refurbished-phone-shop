<?php


namespace App\Presentation\Services\HighCharts;


use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\PositiveReport;
use Illuminate\Support\Collection;

class ReportsChartsGenerator
{
    private const STATUS_PASSED = 'passed';
    private const STATUS_FAILED = 'failed';

    /**
     * Data get from DB
     *
     * @var array
     */
    private $data = [];

    /**
     * Series, used for chart data preparation
     *
     * @var array
     */
    private $series = [
        [
            'name' => self::STATUS_PASSED,
            'data' => [],
            'color' => '#228B22'
        ],
        [
            'name' => self::STATUS_FAILED,
            'data' => [],
            'color' => '#dc143c'
        ]
    ];

    /**
     * Final charts data used for charts rendering
     *
     * @var array
     */
    private $charts = [
        'chart' => ['type' => 'line'],
        'title' => ['text' => 'Positive/Negative reports Dynamics'],
        'xAxis' => [
            'categories' => null,
        ],
        'yAxis' => [
            'title' => [
                'text' => 'Number of reports'
            ]
        ],
        'series' => null
    ];

    /**
     * @param Collection $positiveReports
     * @param Collection $negativeReports
     * @return array
     */
    public function generate(Collection $positiveReports, Collection $negativeReports)
    {
        $data = $this->getData($positiveReports, $negativeReports);
        $dates = array_keys($data);
        $series = $this->getSeries($data);

        return $this->getCharts($series, $dates);
    }

    /**
     * @param Collection $positiveReports
     * @param Collection $negativeReports
     * @return array
     */
    private function getData(Collection $positiveReports, Collection $negativeReports)
    {
        foreach ($positiveReports as $report) {
            $this->getDataFromPositiveReport($report);
        }

        foreach ($negativeReports as $report) {
            $this->getDataFromNegativeReport($report);
        }

        return $this->data;
    }

    /**
     * @param PositiveReport $report
     * @return int
     */
    private function getDataFromPositiveReport(PositiveReport $report)
    {
        return $this->setData($report->created_at->format('d-m-Y'), self::STATUS_PASSED);
    }

    /**
     * @param NegativeReport $report
     * @return int
     */
    private function getDataFromNegativeReport(NegativeReport $report)
    {
        return $this->setData($report->created_at->format('d-m-Y'), self::STATUS_FAILED);
    }

    /**
     * Set proper data into variable
     *
     * @param $date
     * @param $type
     * @return int
     */
    private function setData($date, $type)
    {
        if (isset($this->data[$date][$type])) {
            return $this->data[$date][$type]++;
        }

        return $this->data[$date][$type] = 1;
    }

    /**
     * Prepare charts from base variable and return proper charts
     *
     * @param array $series
     * @param $dates
     * @return array
     */
    private function getCharts(array $series, $dates)
    {
        $charts = $this->charts;
        $charts['xAxis']['categories'] = $dates;
        $charts['series'] = $series;

        return $charts;
    }

    /**
     * Prepare series from base variable
     *
     * @param array $data
     * @return array
     */
    private function getSeries(array $data)
    {
        $series = $this->series;

        foreach ($data as $item) {
            $series[0]['data'][] = $item[self::STATUS_PASSED] ?? 0;
            $series[1]['data'][] = $item[self::STATUS_FAILED] ?? 0;
        }

        return $series;
    }
}
