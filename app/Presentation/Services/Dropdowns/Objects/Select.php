<?php


namespace App\Presentation\Services\Dropdowns\Objects;


use Illuminate\Support\Collection;

/**
 * Class Select
 * @package App\Presentation\Services\Dropdowns\Objects
 */
final class Select
{
    /**
     * @var Collection
     */
    private $options;

    /**
     * @var string
     */
    private $name;

    /**
     * Select constructor.
     * @param Collection $options
     * @param string $name
     */
    public function __construct(Collection $options, string $name)
    {
        $this->options = $options;
        $this->name = $name;
    }

    /**
     * @return Collection
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
