<?php


namespace App\Presentation\Services\Dropdowns\Objects;


class Option
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var
     */
    private $value;

    /**
     * @var bool
     */
    private $isSelected = false;

    /**
     * Option constructor.
     * @param string $name
     * @param $value
     * @param bool $isSelected
     */
    public function __construct(string $name, $value, bool $isSelected = false)
    {
        $this->name = $name;
        $this->value = $value;
        $this->isSelected = $isSelected;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    public function isSelected(): bool
    {
        return $this->isSelected;
    }

    /**
     * @param bool $isSelected
     */
    public function setIsSelected(bool $isSelected): void
    {
        $this->isSelected = $isSelected;
    }
}
