<?php

namespace App\Presentation\Services\Dropdowns;

use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use App\Presentation\Services\Dropdowns\Objects\Option;
use App\Presentation\Services\Dropdowns\Objects\Select;
use Illuminate\Support\Collection;

/**
 * Class ModelViewSortDropdown
 * @package App\Presentation\Services\Dropdowns
 */
final class ModelViewSortDropdown
{
    /**
     * @var array
     */
    private $dropdownData = [
        'name' => 'sort',
        'options' => [
            [
                'name' => 'Monthly Cost (Low to High)',
                'value' => DealsCriteriaDictionary::CRITERIA_SORT_BY_MONTHLY_COST,
                'selected' => true,
            ],
            [
                'name' => 'Total Cost (Low to High)',
                'value' => DealsCriteriaDictionary::CRITERIA_SORT_BY_TOTAL_COST,
                'selected' => false,
            ],
            [
                'name' => 'Upfront Cost (Low to High)',
                'value' => DealsCriteriaDictionary::CRITERIA_SORT_BY_UPFRONT_COST,
                'selected' => false,
            ]
        ]
    ];

    /**
     * @param string|null $sorting
     * @return Select
     */
    public function generate(string $sorting = null): Select
    {
        return new Select($this->getOptions($sorting), $this->dropdownData['name']);
    }

    /**
     * @param string|null $sorting
     * @return Collection
     */
    private function getOptions(string $sorting = null): Collection
    {
        $options = $this->dropdownData['options'];
        $result = collect();
        foreach ($options as $option) {
            $result->push(new Option($option['name'], $option['value'], $option['selected']));
        }

        return $sorting ? $this->setSelected($result, $sorting) : $result;
    }

    private function setSelected(Collection $options, string $sorting): Collection
    {
        foreach ($options as $option) {
            /**
             * @var Option $option
             */
            $option->setIsSelected($sorting === $option->getValue());
        }

        return $options;
    }
}
