<?php

namespace App\Presentation\ViewModels\Deals;

use App\Domain\Deals\Contract\Condition;
use App\Domain\Deals\Deal\Deal;
use App\Presentation\Services\Translators\Phones\TranslatedCondition;
use Spatie\BladeX\ViewModel;

class DealConditionViewModel extends ViewModel
{
    /**
     * @var Deal
     */
    public $deal;

    public function __construct(Deal $deal)
    {
        $this->deal = $deal;
    }

    public function translateCondition(Condition $condition): TranslatedCondition
    {
        return new TranslatedCondition($condition->getKey());
    }
}
