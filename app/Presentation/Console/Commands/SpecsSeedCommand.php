<?php


namespace App\Presentation\Console\Commands;


use App\Application\UseCases\Import\Contracts\SpecsSeedCase;
use App\Presentation\Presenters\Contracts\ImportReportPresenter;
use Illuminate\Console\Command;

final class SpecsSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'specs:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed specs command';

    /**
     * @var SpecsSeedCase
     */
    private $case;

    /**
     * @var ImportReportPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param SpecsSeedCase $case
     * @param ImportReportPresenter $presenter
     */
    public function __construct(SpecsSeedCase $case, ImportReportPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start import process...');
        $response = $this->case->execute();
        foreach ($response as $item) {
            $this->presenter->present($item);
        }
    }
}
