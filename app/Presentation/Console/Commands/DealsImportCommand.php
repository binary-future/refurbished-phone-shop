<?php

namespace App\Presentation\Console\Commands;

use App\Application\UseCases\Import\Contracts\DealsImportCase;
use Illuminate\Console\Command;

final class DealsImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import deals command';

    private $case;

    /**
     * Create a new command instance.
     *
     * @param DealsImportCase $case
     */
    public function __construct(DealsImportCase $case)
    {
        parent::__construct();
        $this->case = $case;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Creating import jobs...');
        $this->case->execute();
        $this->info('Done');
    }
}
