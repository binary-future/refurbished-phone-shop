<?php

namespace App\Presentation\Console\Commands;

use App\Application\UseCases\Import\Contracts\StoreImportCase;
use App\Presentation\Presenters\Contracts\ImportReportPresenter;
use Illuminate\Console\Command;

class StoreImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import store command';

    /**
     * @var StoreImportCase
     */
    private $case;

    /**
     * @var ImportReportPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param StoreImportCase $case
     * @param ImportReportPresenter $presenter
     */
    public function __construct(StoreImportCase $case, ImportReportPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start import process...');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}
