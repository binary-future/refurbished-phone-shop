<?php

namespace App\Presentation\Console\Commands;

use App\Application\UseCases\Import\Contracts\StoresRatingCase;
use App\Presentation\Presenters\Contracts\ImportReportPresenter;
use Illuminate\Console\Command;

final class StoresRatingSeedCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rating:store:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed stores rating command';

    /**
     * @var StoresRatingCase
     */
    private $case;

    /**
     * @var ImportReportPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param StoresRatingCase $case
     * @param ImportReportPresenter $presenter
     */
    public function __construct(StoresRatingCase $case, ImportReportPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function handle(): void
    {
        $this->info('Start import process...');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}
