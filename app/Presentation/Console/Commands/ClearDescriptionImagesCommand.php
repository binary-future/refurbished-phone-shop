<?php


namespace App\Presentation\Console\Commands;


use App\Application\UseCases\Backend\Main\Contracts\ClearDescriptionImagesCase;
use App\Presentation\Presenters\Contracts\Backend\ClearDescriptionImagesPresenter;
use Illuminate\Console\Command;

final class ClearDescriptionImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:descriptions:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear deals command';

    /**
     * @var ClearDescriptionImagesCase
     */
    private $case;

    /**
     * @var ClearDescriptionImagesPresenter
     */
    private $presenter;

    /**
     * ClearDescriptionImagesCommand constructor.
     * @param ClearDescriptionImagesCase $case
     * @param ClearDescriptionImagesPresenter $presenter
     */
    public function __construct(ClearDescriptionImagesCase $case, ClearDescriptionImagesPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start clearing process...');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}