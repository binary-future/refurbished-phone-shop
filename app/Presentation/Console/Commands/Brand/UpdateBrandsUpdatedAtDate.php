<?php

namespace App\Presentation\Console\Commands\Brand;

use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateBrandsUpdatedAtDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'brands:update:date:updated_at';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update updated_at date for Brands';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting updating brands...' . PHP_EOL);
        $brandDates = DB::table(Brand::TABLE)
            ->selectRaw('brands.id, MAX(pm.updated_at) as last_update')
            ->join(PhoneModel::TABLE . ' as pm', 'brands.id', '=', 'pm.brand_id')
            ->groupBy('brands.slug')
            ->get();

        foreach ($brandDates as $brandDate) {
            /**
             * @var \stdClass $brandDate
             * @var Brand $brand
             */
            $brand = Brand::find($brandDate->id);
            $brand->setUpdatedAt($brandDate->last_update);
            $brand->save();

            $this->info("Updated {$brand->getSlug()}. New updated_at date: {$brand->getUpdatedAt()}");
        }

        $this->info(PHP_EOL . 'Complete!');
    }
}
