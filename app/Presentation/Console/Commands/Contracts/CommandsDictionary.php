<?php


namespace App\Presentation\Console\Commands\Contracts;


interface CommandsDictionary
{
    public const DEALS_FILTERS_CACHE_UPDATE = 'deals:filters:cache:update';
}
