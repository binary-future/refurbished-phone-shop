<?php

namespace App\Presentation\Console\Commands;

use App\Application\UseCases\Import\Contracts\PhonesImportCase;
use App\Presentation\Presenters\Contracts\ImportReportPresenter;
use Illuminate\Console\Command;

final class PhonesImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'phones:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import phones command';

    /**
     * @var PhonesImportCase
     */
    private $case;

    /**
     * @var ImportReportPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param PhonesImportCase $case
     * @param ImportReportPresenter $presenter
     */
    public function __construct(PhonesImportCase $case, ImportReportPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start import process...');
        $response = $this->case->execute();
        foreach ($response as $responseItem) {
            if (isset($responseItem['response'])) {
                $this->presenter->present($responseItem['response']);
            }
        }
    }
}
