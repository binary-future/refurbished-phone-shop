<?php

namespace App\Presentation\Console\Commands;

use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Shared\Link\Link;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class DeleteIncorrectDeals extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:delete:incorrect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete no updated deals command';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Deal::where('monthly_cost', '=', 0)->chunkById(100, function (Collection $deals) {
            $bar = $this->output->createProgressBar($deals->count());
            $bar->setFormat('debug');
            foreach ($deals as $deal) {
                $deal->delete();
                $bar->advance();
            }
            $bar->finish();
            $this->info(PHP_EOL);
            $this->info('success' . PHP_EOL);
        });
    }
}
