<?php


namespace App\Presentation\Console\Commands\Images\Duplicate;


use App\Application\UseCases\Phones\Contracts\DuplicateImageCase;
use App\Presentation\Presenters\Contracts\DownloadImagePresenter;
use App\Presentation\Presenters\Contracts\DuplicateImagePresenter;
use Illuminate\Console\Command;

/**
 * Class DuplicateImageByColorCommand
 * @package App\Presentation\Console\Commands
 */
class DuplicateImageByColorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:duplicate:color-model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Duplicates images by color and model';

    /**
     * @var DuplicateImageCase
     */
    private $case;

    /**
     * @var DownloadImagePresenter
     */
    private $presenter;

    /**
     * DuplicateImageByColorCommand constructor.
     * @param DuplicateImageCase $case
     * @param DuplicateImagePresenter $presenter
     */
    public function __construct(DuplicateImageCase $case, DuplicateImagePresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return $this->presenter->present(
            $this->case->execute()
        );
    }
}
