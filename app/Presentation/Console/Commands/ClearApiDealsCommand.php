<?php


namespace App\Presentation\Console\Commands;


use App\Application\UseCases\Deal\Contracts\ClearApiDealsCase;
use App\Presentation\Presenters\Contracts\SoftDeleteOldDealsPresenter;
use Illuminate\Console\Command;

final class ClearApiDealsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:api:clear {api}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear deals command';

    /**
     * @var ClearApiDealsCase
     */
    private $case;

    /**
     * @var SoftDeleteOldDealsPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param ClearApiDealsCase $case
     * @param SoftDeleteOldDealsPresenter $presenter
     */
    public function __construct(ClearApiDealsCase $case, SoftDeleteOldDealsPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start clearing process...');
        $response = $this->case->execute($this->argument('api'));
        $this->presenter->present($response);
    }
}
