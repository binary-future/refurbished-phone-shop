<?php


namespace App\Presentation\Console\Commands;


use App\Domain\Phone\Phone\Phone;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class GenerateOriginalModelsIdsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:original:model:id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate original model id';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start generating...');

        /**
         * @var Collection $images
         */
        Phone::chunkById(100, function (Collection $phones) {
            $bar = $this->output->createProgressBar($phones->count());
            $bar->setFormat('debug');
            foreach ($phones as $phone) {
                /**
                 * @var Phone $phone
                 */
                if (! $phone->getOriginalModelId()) {
                    $phone->setOriginalModelId($phone->getModelId());
                    $phone->save();
                }
                $bar->advance();
            }
            $bar->finish();
        });
    }
}
