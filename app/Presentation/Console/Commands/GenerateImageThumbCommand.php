<?php


namespace App\Presentation\Console\Commands;


use App\Domain\Shared\Image\Image;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Console\Command;

class GenerateImageThumbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:thumb:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate images thumb';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start thumbs generating...');
        
        /**
         * @var Collection $images
         */
        $images = Image::doesntHave('parent')->doesntHave('thumbs')->where('imagable_type', 'phones')->get();
        $bar = $this->output->createProgressBar($images->count());
        $bar->setFormat('debug');
        foreach ($images as $image) {
            if ($image->isLocal()) {
                try {
                    $images->updated_at = Carbon::now();
                    $image->save();
                } catch (\Throwable $exception) {
                    $this->error('Whooops');
                }
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
