<?php

namespace App\Presentation\Console\Commands\Deals\Filters;

use App\Application\UseCases\Deal\Contracts\GenerateDealsFiltersCacheCase;
use App\Presentation\Console\Commands\Contracts\CommandsDictionary;
use App\Presentation\Presenters\Console\GenerateDealsFilterCachePresenter;
use Illuminate\Console\Command;

class GenerateDealsFiltersCacheUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = CommandsDictionary::DEALS_FILTERS_CACHE_UPDATE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate filters for deals and put them to cache';

    /**
     * @var GenerateDealsFiltersCacheCase
     */
    private $case;

    /**
     * @var GenerateDealsFilterCachePresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param GenerateDealsFiltersCacheCase $case
     * @param GenerateDealsFilterCachePresenter $presenter
     */
    public function __construct(GenerateDealsFiltersCacheCase $case, GenerateDealsFilterCachePresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $iterable = $this->case->execute();

        $this->presenter->present($iterable);
    }
}
