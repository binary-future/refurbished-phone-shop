<?php

namespace App\Presentation\Console\Commands\Deals;

use App\Application\UseCases\Deal\Contracts\SoftDeleteOldDealsCase;
use App\Application\UseCases\Deal\HardDeleteOldDealsCase;
use App\Application\UseCases\Deal\Requests\HardDeleteOldDealsRequest;
use App\Presentation\Presenters\Contracts\HardDeleteOldDealsPresenter;
use App\Presentation\Presenters\Contracts\SoftDeleteOldDealsPresenter;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class HardDeleteOldDealsCommand extends Command
{
    private const OPTION_HOURS = 'hours';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:delete:old:hard
                            {--hours= : Delete deals older than hours (min 1)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete no updated all deals older than n hours';

    /**
     * @var HardDeleteOldDealsCase
     */
    private $case;

    /**
     * @var HardDeleteOldDealsPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param HardDeleteOldDealsCase $case
     * @param HardDeleteOldDealsPresenter $presenter
     */
    public function __construct(HardDeleteOldDealsCase $case, HardDeleteOldDealsPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (! $this->isValid()) {
            exit(1);
        }

        $this->info('Start cleaning process...');
        $hours = (int) $this->option(self::OPTION_HOURS);

        $response = $this->case->execute(
            new HardDeleteOldDealsRequest($hours)
        );
        $this->presenter->present($response);
    }

    private function isValid(): bool
    {
        $validator = Validator::make([
            self::OPTION_HOURS => $this->option(self::OPTION_HOURS),
        ], [
            self::OPTION_HOURS => ['numeric', 'min:1'],
        ]);

        if ($validator->fails()) {
            $this->error('Deals were not deleted. See error messages below:');

            foreach ($validator->errors()->all() as $error) {
                $this->error($error);
            }
            return false;
        }

        return true;
    }
}
