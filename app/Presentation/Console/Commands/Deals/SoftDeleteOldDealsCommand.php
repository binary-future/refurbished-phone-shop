<?php

namespace App\Presentation\Console\Commands\Deals;

use App\Application\UseCases\Deal\Contracts\SoftDeleteOldDealsCase;
use App\Presentation\Presenters\Contracts\SoftDeleteOldDealsPresenter;
use Illuminate\Console\Command;

class SoftDeleteOldDealsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:delete:old:soft';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete no updated deals based on the date of the last positive report';

    /**
     * @var SoftDeleteOldDealsCase
     */
    private $case;

    /**
     * @var SoftDeleteOldDealsPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param SoftDeleteOldDealsCase $case
     * @param SoftDeleteOldDealsPresenter $presenter
     */
    public function __construct(SoftDeleteOldDealsCase $case, SoftDeleteOldDealsPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start cleaning process...');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}
