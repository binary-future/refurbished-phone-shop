<?php


namespace App\Presentation\Console\Commands;


use App\Application\UseCases\Download\Contracts\DownloadImageCase;
use App\Presentation\Presenters\Contracts\DownloadImagePresenter;
use Illuminate\Console\Command;

/**
 * Class DownloadNotLocalImageCommand
 * @package App\Presentation\Console\Commands
 */
class DownloadNotLocalImageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download not local images';

    /**
     * @var DownloadImageCase
     */
    private $case;

    /**
     * @var DownloadImagePresenter
     */
    private $presenter;

    /**
     * DownloadNotLocalImageCommand constructor.
     * @param DownloadImageCase $case
     * @param DownloadImagePresenter $presenter
     */
    public function __construct(DownloadImageCase $case, DownloadImagePresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting download procedure');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}
