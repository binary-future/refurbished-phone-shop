<?php

namespace App\Presentation\Console\Commands;

use App\Application\UseCases\System\Contracts\DealsFiltersGenerationCase;
use App\Presentation\Presenters\Contracts\System\DealsFiltersGenerationPresenter;
use Illuminate\Console\Command;

/**
 * Class GenerateDealFiltersCommand
 * @package App\Presentation\Console\Commands
 */
final class GenerateDealFiltersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deals:filters:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deals filters generation command';

    /**
     * @var DealsFiltersGenerationCase
     */
    private $case;

    /**
     * @var DealsFiltersGenerationPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param DealsFiltersGenerationCase $case
     * @param DealsFiltersGenerationPresenter $presenter
     */
    public function __construct(DealsFiltersGenerationCase $case, DealsFiltersGenerationPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start generating process...');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}
