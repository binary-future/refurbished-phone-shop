<?php


namespace App\Presentation\Console\Commands;

use App\Application\UseCases\Import\Contracts\StartPlannedImportsCase;
use App\Presentation\Presenters\Contracts\StartPlannedImportPresenter;
use Illuminate\Console\Command;

class StartPlannedImportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planned:imports:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Starts planned imports';

    /**
     * @var StartPlannedImportsCase
     */
    private $case;

    /**
     * @var StartPlannedImportPresenter
     */
    private $presenter;

    /**
     * Create a new command instance.
     *
     * @param StartPlannedImportsCase $case
     * @param StartPlannedImportPresenter $presenter
     */
    public function __construct(StartPlannedImportsCase $case, StartPlannedImportPresenter $presenter)
    {
        parent::__construct();
        $this->case = $case;
        $this->presenter = $presenter;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start import process...');
        $response = $this->case->execute();
        $this->presenter->present($response);
    }
}
