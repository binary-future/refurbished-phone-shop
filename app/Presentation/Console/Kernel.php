<?php

namespace App\Presentation\Console;

use App\Presentation\Console\Commands\Contracts\CommandsDictionary;
use App\Presentation\Console\Commands\DealsImportCommand;
use App\Presentation\Console\Commands\DownloadNotLocalImageCommand;
use App\Presentation\Console\Commands\GenerateDealFiltersCommand;
use App\Presentation\Console\Commands\StoresRatingSeedCommand;
use App\Presentation\Console\Commands\PhonesImportCommand;
use App\Presentation\Console\Commands\SpecsSeedCommand;
use App\Presentation\Console\Commands\StoreImportCommand;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    private const TIMEZONE = 'Europe/London';

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        StoreImportCommand::class,
        PhonesImportCommand::class,
        DealsImportCommand::class,
        SpecsSeedCommand::class,
        DownloadNotLocalImageCommand::class,
        StoresRatingSeedCommand::class,
        GenerateDealFiltersCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $this->tzSchedule($schedule, 'horizon:snapshot')->everyFiveMinutes();

        // twice in month (1 and 15 days) in 3 am
        $this->tzSchedule($schedule, 'phones:import')->cron('0 3 1,15 * *');

        $this->tzSchedule($schedule, 'images:download')->dailyAt('6:00');
        // every saturday in 4 am
        $this->tzSchedule($schedule, 'clear:descriptions:images')->cron('0 4 * * 6');

        $this->tzSchedule($schedule, 'deals:import')->twiceDaily(7, 14)
            ->after($this->runDealsFiltersCacheUpdating());

        $this->tzSchedule($schedule, 'deals:delete:old:hard --hours=12')->twiceDaily(8, 15)
            ->after($this->runDealsFiltersCacheUpdating());

        $this->tzSchedule($schedule, 'planned:imports:start')->everyTenMinutes()
            ->after($this->runDealsFiltersCacheUpdating());
    }

    private function tzSchedule(Schedule $schedule, $command, array $parameters = []): Event
    {
        return $schedule->command($command, $parameters)->timezone(self::TIMEZONE);
    }

    private function runDealsFiltersCacheUpdating(): callable
    {
        return function () {
            $this->call(CommandsDictionary::DEALS_FILTERS_CACHE_UPDATE);
        };
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
