<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Phone;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneViewResponse;
use App\Presentation\Presenters\Contracts\Backend\PhoneViewPresenter;

/**
 * Class WebPhoneViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebPhoneViewPresenter implements PhoneViewPresenter
{
    /**
     * @param PhoneViewResponse $response
     * @return mixed|void
     */
    public function present(PhoneViewResponse $response)
    {
        return view('backend.phone.phone.view', [
            'phone' => $response->getPhone(),
            'model' => $response->getModel(),
            'brand' => $response->getBrand()
        ]);
    }
}
