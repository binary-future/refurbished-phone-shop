<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Phone;

use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryDeleteImageResponse;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryDeleteImagePresenter;

/**
 * Class WebPhoneGalleryDeleteImagePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone\Phone
 */
final class WebPhoneGalleryDeleteImagePresenter implements PhoneGalleryDeleteImagePresenter
{
    /**
     * @param PhoneGalleryDeleteImageResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(PhoneGalleryDeleteImageResponse $response)
    {
        $message = $response->isSuccess()
            ? 'Success! Image was deleted'
            : 'Error was occurred';

        return redirect()
            ->route('admin.phones.view', [
                'phone' => $response->getPhone()->getKey()
            ])->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
