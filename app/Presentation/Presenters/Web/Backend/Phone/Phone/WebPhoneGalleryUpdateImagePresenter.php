<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Phone;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryUpdateImageResponse;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryUpdateImagePresenter;

final class WebPhoneGalleryUpdateImagePresenter implements PhoneGalleryUpdateImagePresenter
{
    public function present(PhoneGalleryUpdateImageResponse $response)
    {
        if ($response->isSuccess()) {
            $html = view('backend.phone.components.phone-gallery', ['phone' => $response->getPhone()])
                ->render();
            return response()->json(['html' => $html]);
        }

        return response()->json(['error' => 'Error has been occurred']);
    }
}
