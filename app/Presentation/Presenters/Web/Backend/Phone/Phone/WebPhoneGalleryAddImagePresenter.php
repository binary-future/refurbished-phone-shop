<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Phone;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryAddImageResponse;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryAddImagePresenter;

/**
 * Class WebPhoneGalleryAddImagePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebPhoneGalleryAddImagePresenter implements PhoneGalleryAddImagePresenter
{
    /**
     * @param PhoneGalleryAddImageResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(PhoneGalleryAddImageResponse $response)
    {
        $message = $response->isSuccess()
            ? 'Success! Image was added'
            : 'Error was occurred';

        return redirect()
            ->route('admin.phones.view', [
                'phone' => $response->getPhone()->getKey()
            ])->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
