<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Color;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorUpdateResponse;
use App\Presentation\Presenters\Contracts\Backend\ColorUpdatePresenter;

/**
 * Class WebColorUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone\Color
 */
final class WebColorUpdatePresenter implements ColorUpdatePresenter
{
    /**
     * @param ColorUpdateResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(ColorUpdateResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was saved', $response->getColor()->getName())
            : 'Error was occurred';

        return redirect()->route('admin.colors.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
