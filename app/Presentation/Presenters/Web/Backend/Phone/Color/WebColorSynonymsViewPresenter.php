<?php

namespace App\Presentation\Presenters\Web\Backend\Phone\Color;

use App\Application\UseCases\Backend\Phones\Color\Responses\ColorSynonymsResponse;
use App\Presentation\Presenters\Contracts\Backend\ColorSynonymsViewPresenter;

/**
 * Class WebColorSynonymsViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone\Color
 */
final class WebColorSynonymsViewPresenter implements ColorSynonymsViewPresenter
{
    /**
     * @param ColorSynonymsResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ColorSynonymsResponse $response)
    {
        return view('backend.phone.color.synonyms', [
            'color' => $response->getColor(),
            'synonyms' => $response->getSynonyms(),
            'potentialSynonyms' => $response->getPotentialSynonyms()
        ]);
    }
}
