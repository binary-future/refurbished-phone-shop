<?php

namespace App\Presentation\Presenters\Web\Backend\Phone\Color;

use App\Application\UseCases\Backend\Phones\Color\Responses\ColorMainResponse;
use App\Presentation\Presenters\Contracts\Backend\ColorMainPresenter;

/**
 * Class WebColorMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone\Color
 */
final class WebColorMainPresenter implements ColorMainPresenter
{
    /**
     * @param ColorMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ColorMainResponse $response)
    {
        return view('backend.phone.color.main', [
            'colors' => $response->getColors(),
            'inactiveColorsIds' => $response->getInactiveColors()->pluck('id')
        ]);
    }
}
