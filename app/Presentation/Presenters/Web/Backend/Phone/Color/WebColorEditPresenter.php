<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Color;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorEditResponse;
use App\Presentation\Presenters\Contracts\Backend\ColorEditPresenter;

/**
 * Class WebColorEditPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone\Color
 */
final class WebColorEditPresenter implements ColorEditPresenter
{
    /**
     * @param ColorEditResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ColorEditResponse $response)
    {
        return view('backend.phone.color.edit', ['color' => $response->getColor()]);
    }
}