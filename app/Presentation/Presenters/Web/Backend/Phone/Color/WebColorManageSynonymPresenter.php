<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Color;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorManageSynonymResponse;
use App\Presentation\Presenters\Contracts\Backend\ColorManageSynonymPresenter;

/**
 * Class WebColorManageSynonymPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone\Color
 */
final class WebColorManageSynonymPresenter implements ColorManageSynonymPresenter
{
    /**
     * @param ColorManageSynonymResponse $response
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function present(ColorManageSynonymResponse $response)
    {
        $status = $response->isSucceed() ? 'success' : 'error';
        $message = $response->isSucceed() ? 'Model was saved' : 'Error has been occurred';

        return response()->json(['status' => $status, 'message' => $message]);
    }
}
