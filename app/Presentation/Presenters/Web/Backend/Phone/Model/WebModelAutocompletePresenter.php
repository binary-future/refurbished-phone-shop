<?php

namespace App\Presentation\Presenters\Web\Backend\Phone\Model;

use App\Application\UseCases\Backend\Phones\Model\Responses\ModelAutocompleteResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelAutocompletePresenter;
use App\Presentation\Services\Translators\Contracts\Translator;

final class WebModelAutocompletePresenter implements ModelAutocompletePresenter
{
    /**
     * @var Translator
     */
    private $trasnaltor;

    /**
     * WebModelAutocompletePresenter constructor.
     * @param Translator $trasnaltor
     */
    public function __construct(Translator $trasnaltor)
    {
        $this->trasnaltor = $trasnaltor;
    }

    public function present(ModelAutocompleteResponse $response)
    {
        $models = $response->getModels();

        return response()->json(['models' => $this->trasnaltor->translate($models)]);
    }
}