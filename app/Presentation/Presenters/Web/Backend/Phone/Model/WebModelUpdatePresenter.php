<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelUpdateResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelUpdatePresenter;

/**
 * Class WebModelUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebModelUpdatePresenter implements ModelUpdatePresenter
{
    /**
     * @param ModelUpdateResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(ModelUpdateResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was saved', $response->getModel()->getName())
            : 'Error was occurred';

        return redirect()->route('admin.models.edit', [
            'brand' => $response->getBrand()->getSlug(),
            'model' => $response->getModel()->getSlug()
        ])->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
