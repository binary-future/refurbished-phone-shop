<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelListResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelListPresenter;

/**
 * Class WebModelListPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebModelListPresenter implements ModelListPresenter
{
    /**
     * @param ModelListResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ModelListResponse $response)
    {
        return view('backend.phone.model.list', [
            'brand' => $response->getBrand(),
            'models' => $response->getModels()
        ]);
    }
}
