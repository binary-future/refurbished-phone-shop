<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSynonymViewResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelSynonymViewPresenter;

final class WebModelSynonymViewPresenter implements ModelSynonymViewPresenter
{
    public function present(ModelSynonymViewResponse $response)
    {
        return view('backend.phone.model.synonyms', [
            'model' => $response->getModel(),
            'brand' => $response->getBrand(),
            'synonyms' => $response->getSynonyms(),
            'potentialSynonyms' => $response->getPotentialSynonyms()
        ]);
    }
}
