<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelEditResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelEditPresenter;

/**
 * Class WebModelEditPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebModelEditPresenter implements ModelEditPresenter
{
    /**
     * @param ModelEditResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ModelEditResponse $response)
    {
        return view('backend.phone.model.edit', [
            'brand' => $response->getBrand(),
            'model' => $response->getPhoneModel(),
            'isLatestModel' => $response->isLatestModel()
        ]);
    }
}
