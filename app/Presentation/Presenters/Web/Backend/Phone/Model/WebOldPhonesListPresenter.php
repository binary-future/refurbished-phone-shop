<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\OldPhonesListResponse;
use App\Presentation\Presenters\Contracts\Backend\OldPhonesListPresenter;

final class WebOldPhonesListPresenter implements OldPhonesListPresenter
{
    public function present(OldPhonesListResponse $response)
    {
        return view('backend.phone.model.old-list', [
            'models' => $response->getModels()
        ]);
    }

}
