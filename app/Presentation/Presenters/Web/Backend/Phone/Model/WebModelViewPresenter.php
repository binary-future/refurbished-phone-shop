<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelViewResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelViewPresenter;

/**
 * Class WebModelViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebModelViewPresenter implements ModelViewPresenter
{
    /**
     * @param ModelViewResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ModelViewResponse $response)
    {
        return view('backend.phone.model.view', [
            'brand' => $response->getBrand(),
            'model' => $response->getPhoneModel(),
            'latestModel' => $response->getLatestModel(),
            'phones' => $response->getPhones(),
            'synonymsPhones' => $response->getSynonymsPhones(),
            'colors' => $response->getColors(),
        ]);
    }
}
