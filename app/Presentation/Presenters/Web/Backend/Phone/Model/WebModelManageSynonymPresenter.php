<?php

namespace App\Presentation\Presenters\Web\Backend\Phone\Model;

use App\Application\UseCases\Backend\Phones\Model\Responses\ModelManageSynonymResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelManageSynonymPresenter;

final class WebModelManageSynonymPresenter implements ModelManageSynonymPresenter
{
    public function present(ModelManageSynonymResponse $response)
    {
        $status = $response->isSucceed() ? 'success' : 'error';
        $message = $response->isSucceed() ? 'Model was saved' : 'Error has been occurred';

        return response()->json(['status' => $status, 'message' => $message]);
    }

}