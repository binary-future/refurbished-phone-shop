<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSearchResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelSearchPresenter;

/**
 * Class WebModelSearchPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebModelSearchPresenter implements ModelSearchPresenter
{
    /**
     * @param ModelSearchResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ModelSearchResponse $response)
    {
        return view('backend.phone.model.search', [
            'models' => $response->getModels()
        ]);
    }
}
