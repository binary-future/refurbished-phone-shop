<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Model;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelMainResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelMainPresenter;

/**
 * Class WebModelMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebModelMainPresenter implements ModelMainPresenter
{
    /**
     * @param ModelMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ModelMainResponse $response)
    {
        return view('backend.phone.model.main', [
            'brands' => $response->getBrands()
        ]);
    }
}
