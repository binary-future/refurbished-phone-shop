<?php

namespace App\Presentation\Presenters\Web\Backend\Phone\Brand;

use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandUpdateResponse;
use App\Presentation\Presenters\Contracts\Backend\BrandUpdatePresenter;

/**
 * Class WebBrandUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebBrandUpdatePresenter implements BrandUpdatePresenter
{
    /**
     * @param BrandUpdateResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(BrandUpdateResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was saved', $response->getBrand()->getName())
            : 'Error was occurred';

        return redirect()->route('admin.brands.edit', ['brand' => $response->getBrand()->getSlug()])
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
