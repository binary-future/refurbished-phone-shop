<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Brand;

use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandMainResponse;
use App\Presentation\Presenters\Contracts\Backend\BrandMainPresenter;

/**
 * Class WebBrandMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebBrandMainPresenter implements BrandMainPresenter
{
    /**
     * @param BrandMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(BrandMainResponse $response)
    {
        return view('backend.phone.brand.index', [
            'brands' => $response->getBrands()
        ]);
    }
}
