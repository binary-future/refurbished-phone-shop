<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\Brand;


use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandEditResponse;
use App\Presentation\Presenters\Contracts\Backend\BrandEditPresenter;

final class WebBrandEditPresenter implements BrandEditPresenter
{
    public function present(BrandEditResponse $response)
    {
        return view('backend.phone.brand.edit', [
            'brand' => $response->getBrand()
        ]);
    }
}
