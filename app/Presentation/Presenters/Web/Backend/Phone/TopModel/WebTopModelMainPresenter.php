<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelCreateResponse;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelMainResponse;
use App\Presentation\Presenters\Contracts\Backend\TopModelMainPresenter;

/**
 * Class WebModelMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebTopModelMainPresenter implements TopModelMainPresenter
{
    /**
     * @param TopModelMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(TopModelMainResponse $response)
    {
        return view('backend.phone.top-model.main', [
            'models' => $response->getModels(),
            'topModels' => $response->getTopModels()
        ]);
    }
}
