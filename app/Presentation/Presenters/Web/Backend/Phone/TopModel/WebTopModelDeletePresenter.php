<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\TopModelDeletePresenter;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class WebTopModelDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebTopModelDeletePresenter implements TopModelDeletePresenter
{
    /**
     * @param TopModelDeleteResponse $response
     * @return Factory|View|mixed
     */
    public function present(TopModelDeleteResponse $response)
    {
        return response()
            ->json(
                $response->isSuccess()
                ? 'Success! Top model was deleted'
                : 'Error was occurred',
                204
            );
    }
}
