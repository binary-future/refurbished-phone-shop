<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelCreateResponse;
use App\Presentation\Presenters\Contracts\Backend\TopModelCreatePresenter;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class WebTopModelBulkUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebTopModelCreatePresenter implements TopModelCreatePresenter
{
    /**
     * @param TopModelCreateResponse $response
     * @return Factory|View|mixed
     */
    public function present(TopModelCreateResponse $response)
    {
        if ($response->isSuccess()) {
            return response()->json(
                $response->getTopModel()
            );
        }

        return response()->json(
            'Top model not created',
            422
        );
    }
}
