<?php


namespace App\Presentation\Presenters\Web\Backend\Phone\TopModel;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelCreateResponse;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelUpdateResponse;
use App\Presentation\Presenters\Contracts\Backend\TopModelCreatePresenter;
use App\Presentation\Presenters\Contracts\Backend\TopModelUpdatePresenter;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Class WebTopModelUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Phone
 */
final class WebTopModelUpdatePresenter implements TopModelUpdatePresenter
{
    /**
     * @param TopModelUpdateResponse $response
     * @return Factory|View|mixed
     */
    public function present(TopModelUpdateResponse $response)
    {
        return $response->isSuccess()
            ? response()->json(
                $response->getTopModel()
            )
            : response()->json('Top model not updated', 422);
    }
}
