<?php

namespace App\Presentation\Presenters\Web\Backend;

use App\Application\UseCases\Backend\Main\Responses\FileUploadEditorResult;
use App\Presentation\Presenters\Contracts\Backend\FileUploadEditorPresenter;

/**
 * Class WebFileUploadEditorPresenter
 * @package App\Presentation\Presenters\Web\Backend
 */
final class WebFileUploadEditorPresenter implements FileUploadEditorPresenter
{
    /**
     * @param FileUploadEditorResult $result
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function present(FileUploadEditorResult $result)
    {
        $responseData = [
            'location' => asset($result->getLocation())
        ];
        if (! $result->isSucceed() || ! $result->getLocation()) {
            $responseData['error'] = true;
            $responseData['message'] = 'Cannot upload file';
        }

        return response()->json($responseData);
    }
}
