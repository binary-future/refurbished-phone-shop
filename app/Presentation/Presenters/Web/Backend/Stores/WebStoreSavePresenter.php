<?php

namespace App\Presentation\Presenters\Web\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Response\StoreSaveResponse;
use App\Presentation\Presenters\Contracts\Backend\StoreSavePresenter;

/**
 * Class WebStoreUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
final class WebStoreSavePresenter implements StoreSavePresenter
{
    /**
     * @param StoreSaveResponse $response
     * @return mixed|void
     */
    public function present(StoreSaveResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was saved', $response->getStore()->getName())
            : 'Error was occurred';

        return $response->isSuccess()
            ? redirect()->route(
                'admin.stores.edit',
                ['store' => $response->getStore()->getSlug()]
            )->with('success', $message)
            : redirect()->route('admin.stores.main')->with('error', $message);
    }
}
