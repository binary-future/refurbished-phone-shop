<?php

namespace App\Presentation\Presenters\Web\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Response\StoreMainResponse;
use App\Presentation\Presenters\Contracts\Backend\StoresMainPresenter;

/**
 * Class WebStoresMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
final class WebStoresMainPresenter implements StoresMainPresenter
{
    /**
     * @param StoreMainResponse $response
     * @return mixed|void
     */
    public function present(StoreMainResponse $response)
    {
        return view('backend.store.index', ['stores' => $response->getStores()]);
    }
}
