<?php

namespace App\Presentation\Presenters\Web\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Response\StoreEditResponse;
use App\Presentation\Presenters\Contracts\Backend\StoresEditPresenter;

/**
 * Class WebStoresEditPresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
class WebStoresEditPresenter implements StoresEditPresenter
{
    /**
     * @param StoreEditResponse $response
     * @return mixed|void
     */
    public function present(StoreEditResponse $response)
    {
        return view('backend.store.edit', [
            'store' => $response->getStore(),
            'affiliations' => $response->getAffiliations(),
            'apiList' => $response->getApi(),
            'paymentMethods' => $response->getPaymentMethods(),
            'datafeed' => $response->getDatafeed(),
            'datafeedDownloadLink' => $response->getDatafeedDownloadLink(),
        ]);
    }
}
