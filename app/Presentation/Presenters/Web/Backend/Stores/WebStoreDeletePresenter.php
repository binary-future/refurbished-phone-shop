<?php

namespace App\Presentation\Presenters\Web\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Response\StoreDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\StoreDeletePresenter;

/**
 * Class WebStoreDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
final class WebStoreDeletePresenter implements StoreDeletePresenter
{
    /**
     * @param StoreDeleteResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(StoreDeleteResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was deleted', $response->getStore()->getName())
            : 'Error was occurred';

        return redirect()->route('admin.stores.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
