<?php

namespace App\Presentation\Presenters\Web\Backend\Stores;

use App\Application\UseCases\Backend\Stores\Response\StoreCreateResponse;
use App\Presentation\Presenters\Contracts\Backend\StoreCreatePresenter;

/**
 * Class WebStoreCreatePresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
final class WebStoreCreatePresenter implements StoreCreatePresenter
{
    /**
     * @param StoreCreateResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(StoreCreateResponse $response)
    {
        return view('backend.store.create', [
            'affiliations' => $response->getAffiliations(),
            'apiList' => $response->getApiList(),
            'paymentMethods' => $response->getPaymentMethods(),
        ]);
    }
}
