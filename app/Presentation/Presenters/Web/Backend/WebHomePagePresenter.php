<?php

namespace App\Presentation\Presenters\Web\Backend;

use App\Application\UseCases\Backend\Main\Responses\MainPageResponse;
use App\Presentation\Presenters\Contracts\Backend\HomePagePresenter;

/**
 * Class WebHomePagePresenter
 * @package App\Presentation\Presenters\Web\Backend
 */
final class WebHomePagePresenter implements HomePagePresenter
{
    /**
     * @param MainPageResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function present(MainPageResponse $response)
    {
        return view('backend.home', [
            'stores' => $response->getStores(),
            'positiveReports' => $response->getDealsPositiveReports(),
            'negativeReports' => $response->getDealsNegativeReports(),
            'inActiveModel' => $response->getInactiveModels(),
            'withoutDealsModels' => $response->getWithoutDealsModels(),
            'phonesPositiveReports' => $response->getPhonesPositiveReports(),
            'phonesNegativeReports' => $response->getPhonesNegativeReports(),
            'plannedImports' => $response->getPlannedImports(),
            'storesCount' => $response->getStores()->count(),
        ]);
    }
}
