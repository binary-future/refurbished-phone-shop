<?php

namespace App\Presentation\Presenters\Web\Backend\Networks;

use App\Application\UseCases\Backend\Networks\Response\NetworksEditResponse;
use App\Presentation\Presenters\Contracts\Backend\NetworksEditPresenter;

/**
 * Class WebNetworksEditPresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
class WebNetworksEditPresenter implements NetworksEditPresenter
{
    /**
     * @param NetworksEditResponse $response
     * @return mixed|void
     */
    public function present(NetworksEditResponse $response)
    {
        return view('backend.networks.edit', ['network' => $response->getNetwork()]);
    }
}
