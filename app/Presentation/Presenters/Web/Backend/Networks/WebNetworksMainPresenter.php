<?php

namespace App\Presentation\Presenters\Web\Backend\Networks;

use App\Application\UseCases\Backend\Networks\Response\NetworkMainResponse;
use App\Presentation\Presenters\Contracts\Backend\NetworksMainPresenter;

/**
 * Class WebNetworksMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
final class WebNetworksMainPresenter implements NetworksMainPresenter
{
    /**
     * @param NetworkMainResponse $response
     * @return mixed|void
     */
    public function present(NetworkMainResponse $response)
    {
        return view('backend.networks.index', ['networks' => $response->getNetworks()]);
    }
}
