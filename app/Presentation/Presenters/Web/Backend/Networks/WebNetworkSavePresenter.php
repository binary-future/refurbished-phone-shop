<?php

namespace App\Presentation\Presenters\Web\Backend\Networks;

use App\Application\UseCases\Backend\Networks\Response\NetworkSaveResponse;
use App\Presentation\Presenters\Contracts\Backend\NetworksSavePresenter;

/**
 * Class WebNetworkSavePresenter
 * @package App\Presentation\Presenters\Web\Backend\Networks
 */
final class WebNetworkSavePresenter implements NetworksSavePresenter
{
    /**
     * @param NetworkSaveResponse $response
     * @return mixed|void
     */
    public function present(NetworkSaveResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was saved', $response->getNetwork()->getName())
            : 'Error was occurred';

        return redirect()->route('admin.networks.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
