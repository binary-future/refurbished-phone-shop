<?php


namespace App\Presentation\Presenters\Web\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Responses\ReportsGroupDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\ReportsGroupDeletePresenter;

/**
 * Class WebReportsGroupDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\Reports
 */
final class WebReportsGroupDeletePresenter implements ReportsGroupDeletePresenter
{
    /**
     * @param ReportsGroupDeleteResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(ReportsGroupDeleteResponse $response)
    {
        $message = $response->isDeleted() ? 'Success! Deleted' : 'Error was occurred';

        return redirect()->route('admin.home')->with($response->isDeleted() ? 'success' : 'error', $message);
    }
}
