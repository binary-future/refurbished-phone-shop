<?php


namespace App\Presentation\Presenters\Web\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Responses\PositiveViewResponse;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsViewPresenter;

/**
 * Class WebPositiveReportsViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Reports
 */
final class WebPositiveReportsViewPresenter implements PositiveReportsViewPresenter
{
    /**
     * @param PositiveViewResponse $response
     * @return mixed|void
     */
    public function present(PositiveViewResponse $response)
    {
        return view('backend.reports.positive-view', [
            'store' => $response->getStore(),
            'reports' => $response->getReports(),
            'type' => $response->getType(),
        ]);
    }
}
