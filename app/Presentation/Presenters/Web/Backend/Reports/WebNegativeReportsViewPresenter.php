<?php


namespace App\Presentation\Presenters\Web\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Responses\NegativeViewResponse;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsViewPresenter;

/**
 * Class WebNegativeReportsViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Reports
 */
final class WebNegativeReportsViewPresenter implements NegativeReportsViewPresenter
{
    /**
     * @param NegativeViewResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(NegativeViewResponse $response)
    {
        return view('backend.reports.negative-view', [
            'store' => $response->getStore(),
            'reports' => $response->getReports(),
            'type' => $response->getType(),
        ]);
    }
}
