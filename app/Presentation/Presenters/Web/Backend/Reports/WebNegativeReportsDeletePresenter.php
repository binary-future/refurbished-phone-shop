<?php


namespace App\Presentation\Presenters\Web\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsDeletePresenter;

final class WebNegativeReportsDeletePresenter implements NegativeReportsDeletePresenter
{
    public function present(NegativeDeleteResponse $response)
    {
        $message = $response->isDeleted()
            ? 'Success! Deleted'
            : 'Error was occurred';

        return redirect()->route('admin.reports.negative.view', [
                'store' => $response->getStore()->getSlug(),
                'type' => $response->getType(),
            ])->with($response->isDeleted() ? 'success' : 'error', $message);
    }
}
