<?php


namespace App\Presentation\Presenters\Web\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsDeletePresenter;

/**
 * Class WebPositiveReportsDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\Reports
 */
final class WebPositiveReportsDeletePresenter implements PositiveReportsDeletePresenter
{
    /**
     * @param PositiveDeleteResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(PositiveDeleteResponse $response)
    {
        $message = $response->isDeleted()
            ? 'Success! Deleted'
            : 'Error was occurred';

        return redirect()->route('admin.reports.positive.view', [
                'store' => $response->getStore()->getSlug(),
                'type' => $response->getType()
            ])->with($response->isDeleted() ? 'success' : 'error', $message);
    }
}
