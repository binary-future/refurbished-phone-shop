<?php


namespace App\Presentation\Presenters\Web\Backend\Reports;


use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;
use App\Presentation\Presenters\Contracts\Backend\AllReportsPresenter;
use App\Presentation\Services\HighCharts\ReportsChartsGenerator;

/**
 * Class WebAllReportsPresenter
 * @package App\Presentation\Presenters\Web\Backend\Reports
 */
final class WebAllReportsPresenter implements AllReportsPresenter
{
    /**
     * @var ReportsChartsGenerator
     */
    private $chartsGenerator;

    /**
     * WebAllReportsPresenter constructor.
     * @param ReportsChartsGenerator $chartsGenerator
     */
    public function __construct(ReportsChartsGenerator $chartsGenerator)
    {
        $this->chartsGenerator = $chartsGenerator;
    }

    /**
     * @param AllReportsResponse $response
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function present(AllReportsResponse $response)
    {
        $charts = $this->chartsGenerator->generate($response->getPositiveReports(), $response->getNegativeReports());

        return response()->json(['charts' => $charts]);
    }
}
