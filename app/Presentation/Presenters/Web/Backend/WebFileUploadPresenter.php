<?php


namespace App\Presentation\Presenters\Web\Backend;


use App\Application\UseCases\Backend\Main\Responses\FileUploadResponse;
use App\Presentation\Presenters\Contracts\Backend\FileUploadPresenter;

final class WebFileUploadPresenter implements FileUploadPresenter
{
    public function present(FileUploadResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s were saved, %s were skipped', $response->getSucceed(), $response->getFailed())
            : 'Error was occurred';

        return redirect()->route('admin.home')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
