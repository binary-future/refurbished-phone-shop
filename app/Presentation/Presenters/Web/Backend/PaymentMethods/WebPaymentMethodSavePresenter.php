<?php

namespace App\Presentation\Presenters\Web\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\SavePaymentMethodResponse;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodSavePresenter;

final class WebPaymentMethodSavePresenter implements PaymentMethodSavePresenter
{
    public function present(SavePaymentMethodResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was saved', $response->getPaymentMethod()->getName())
            : 'Error was occurred';

        return $response->isSuccess()
            ? redirect()->route(
                'admin.payment-methods.edit',
                ['payment-method' => $response->getPaymentMethod()->getSlug()]
            )->with('success', $message)
            : redirect()->route('admin.payment-methods.main')->with('error', $message);
    }
}
