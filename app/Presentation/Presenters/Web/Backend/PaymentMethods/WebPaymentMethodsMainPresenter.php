<?php

namespace App\Presentation\Presenters\Web\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodsMainResponse;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodsMainPresenter;

final class WebPaymentMethodsMainPresenter implements PaymentMethodsMainPresenter
{
    /**
     * @param PaymentMethodsMainResponse $response
     * @return mixed|void
     */
    public function present(PaymentMethodsMainResponse $response)
    {
        return view('backend.payment-methods.index', ['paymentMethods' => $response->getPaymentMethods()]);
    }
}
