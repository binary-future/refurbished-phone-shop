<?php

namespace App\Presentation\Presenters\Web\Backend\PaymentMethods;

use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodCreatePresenter;

final class WebPaymentMethodCreatePresenter implements PaymentMethodCreatePresenter
{
    public function present()
    {
        return view('backend.payment-methods.create');
    }
}
