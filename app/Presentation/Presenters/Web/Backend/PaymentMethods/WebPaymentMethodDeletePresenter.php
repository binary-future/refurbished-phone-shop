<?php

namespace App\Presentation\Presenters\Web\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\DeletePaymentMethodResponse;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodDeletePresenter;

/**
 * Class WebStoreDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\Stores
 */
final class WebPaymentMethodDeletePresenter implements PaymentMethodDeletePresenter
{
    public function present(DeletePaymentMethodResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was deleted', $response->getPaymentMethod()->getName())
            : 'Error was occurred';

        return redirect()->route('admin.payment-methods.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
