<?php

namespace App\Presentation\Presenters\Web\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodEditResponse;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodEditPresenter;

class WebPaymentMethodEditPresenter implements PaymentMethodEditPresenter
{
    public function present(PaymentMethodEditResponse $response)
    {
        return view('backend.payment-methods.edit', [
            'paymentMethod' => $response->getPaymentMethod(),
        ]);
    }
}
