<?php

namespace App\Presentation\Presenters\Web\Backend\Import\Models;

use App\Application\UseCases\Backend\Import\Main\Responses\MainImportResponse;
use App\Presentation\Presenters\Contracts\Backend\MainImportPagePresenter;

/**
 * Class WebMainImportPresenter
 * @package App\Presentation\Presenters\Web\Backend\Import\Models
 */
final class WebMainImportPresenter implements MainImportPagePresenter
{
    /**
     * @param MainImportResponse $response
     * @return mixed|void
     */
    public function present(MainImportResponse $response)
    {
        return view('backend.import.model.main', [
            'inactive' => $response->getInActiveModels(),
            'withoutDeals' => $response->getModelsWithoutDeals(),
            'plannedImport' => $response->getPlannedImports(),
        ]);
    }
}
