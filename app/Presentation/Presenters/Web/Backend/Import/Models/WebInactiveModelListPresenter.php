<?php


namespace App\Presentation\Presenters\Web\Backend\Import\Models;


use App\Application\UseCases\Backend\Import\Model\Responses\InactiveListResponse;
use App\Presentation\Presenters\Contracts\Backend\InactiveModelListPresenter;

/**
 * Class WebInactiveModelListPresenter
 * @package App\Presentation\Presenters\Web\Backend\Import\Models
 */
final class WebInactiveModelListPresenter implements InactiveModelListPresenter
{
    /**
     * @param InactiveListResponse $response
     * @return mixed|void
     */
    public function present(InactiveListResponse $response)
    {
        return view('backend.import.model.inactive', [
            'models' => $response->getModels(),
        ]);
    }
}