<?php

namespace App\Presentation\Presenters\Web\Backend\Import\Models;

use App\Application\UseCases\Backend\Import\Model\Responses\WithoutDealsResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelsWithoutDealsPresenter;

final class WebModelsWithoutDealsPresenter implements ModelsWithoutDealsPresenter
{
    /**
     * @param WithoutDealsResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function presenter(WithoutDealsResponse $response)
    {
        return view('backend.import.model.without-deals', [
            'models' => $response->getModels(),
        ]);
    }
}
