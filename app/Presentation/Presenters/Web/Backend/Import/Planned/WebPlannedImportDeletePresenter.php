<?php


namespace App\Presentation\Presenters\Web\Backend\Import\Planned;


use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanDeleted;
use App\Presentation\Presenters\Contracts\Backend\PlannedDeletePresenter;

/**
 * Class WebPlannedImportDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\Import\Planned
 */
final class WebPlannedImportDeletePresenter implements PlannedDeletePresenter
{
    /**
     * @param ImportPlanDeleted $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(ImportPlanDeleted $response)
    {
        $message = $response->isSuccess()
            ? 'Success! Planned import was deleted'
            : 'Error was occurred';

        return redirect()->route('admin.import.planned.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
