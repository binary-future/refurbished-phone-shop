<?php


namespace App\Presentation\Presenters\Web\Backend\Import\Planned;


use App\Application\UseCases\Backend\Import\Planned\Responses\PlannedMainResponse;
use App\Presentation\Presenters\Contracts\Backend\PlannedImportsMainPresenter;

/**
 * Class WebPlannedImportsMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Import\Planned
 */
final class WebPlannedImportsMainPresenter implements PlannedImportsMainPresenter
{
    /**
     * @param PlannedMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(PlannedMainResponse $response)
    {
        return view('backend.import.planned.index', [
            'imports' => $response->getPlannedImports()
        ]);
    }
}
