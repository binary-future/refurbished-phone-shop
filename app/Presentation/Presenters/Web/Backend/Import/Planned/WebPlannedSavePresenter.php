<?php


namespace App\Presentation\Presenters\Web\Backend\Import\Planned;


use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanSaved;
use App\Presentation\Presenters\Contracts\Backend\PlannedSavePresenter;

/**
 * Class WebPlannedSavePresenter
 * @package App\Presentation\Presenters\Web\Backend\Import\Planned
 */
final class WebPlannedSavePresenter implements PlannedSavePresenter
{
    /**
     * @param ImportPlanSaved $response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function present(ImportPlanSaved $response)
    {
        $message = $response->isSuccess()
            ? 'Success! Planned import was saved'
            : 'Error was occurred';

        return redirect()->route('admin.import.planned.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }
}
