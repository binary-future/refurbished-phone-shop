<?php

namespace App\Presentation\Presenters\Web\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Response\UserDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\UserDeletePresenter;

/**
 * Class WebUserDeletePresenter
 * @package App\Presentation\Presenters\Web\Backend\User\User
 */
final class WebUserDeletePresenter implements UserDeletePresenter
{
    /**
     * @param UserDeleteResponse $response
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function present(UserDeleteResponse $response)
    {
        $message = $response->isSuccess()
            ? sprintf('Success! %s was deleted', $response->getUser()->getName())
            : sprintf('Error has occurred! Cannot delete %s', $response->getUser()->getName());

        return redirect()->route('admin.users.main')
            ->with($response->isSuccess() ? 'success' : 'error', $message);
    }

}