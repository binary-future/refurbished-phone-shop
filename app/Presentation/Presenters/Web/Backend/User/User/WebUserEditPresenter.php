<?php

namespace App\Presentation\Presenters\Web\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Response\UserEditResponse;
use App\Presentation\Presenters\Contracts\Backend\UserEditPresenter;

/**
 * Class WebUserEditPresenter
 * @package App\Presentation\Presenters\Web\Backend\User\User
 */
final class WebUserEditPresenter implements UserEditPresenter
{
    /**
     * @param UserEditResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(UserEditResponse $response)
    {
        return view('backend.users.user.edit', [
            'user' => $response->getUser(),
            'roles' => $response->getRoles(),
        ]);
    }
}