<?php

namespace App\Presentation\Presenters\Web\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Response\UserSaveResponse;
use App\Presentation\Presenters\Contracts\Backend\UserSavePresenter;

final class WebUserSavePresenter implements UserSavePresenter
{
    public function present(UserSaveResponse $response)
    {
        $message = $response->isSucceed() && $response->getUser()
            ? sprintf('Success! %s was created', $response->getUser()->getName())
            : 'Cannot create user. Error has occurred!';

        return redirect()->route('admin.users.main')
            ->with($response->isSucceed() ? 'success' : 'error', $message);
    }
}
