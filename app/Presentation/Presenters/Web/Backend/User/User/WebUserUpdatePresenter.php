<?php

namespace App\Presentation\Presenters\Web\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Response\UserUpdateResponse;
use App\Presentation\Presenters\Contracts\Backend\UserUpdatePresenter;

/**
 * Class WebUserUpdatePresenter
 * @package App\Presentation\Presenters\Web\Backend\User\User
 */
final class WebUserUpdatePresenter implements UserUpdatePresenter
{
    /**
     * @param UserUpdateResponse $response
     * @return \Illuminate\Http\RedirectResponse
     */
    public function present(UserUpdateResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was deleted', $response->getUser()->getName())
            : sprintf('Error has occurred! Cannot update %s', $response->getUser()->getName());

        return redirect()->route('admin.users.main')
            ->with($response->isSucceed() ? 'success' : 'error', $message);
    }
}
