<?php

namespace App\Presentation\Presenters\Web\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Response\UserCreateResponse;
use App\Presentation\Presenters\Contracts\Backend\UserCreatePresenter;

/**
 * Class WebUserCreatePresent
 * @package App\Presentation\Presenters\Web\Backend\User\User
 */
final class WebUserCreatePresenter implements UserCreatePresenter
{
    /**
     * @param UserCreateResponse $response
     * @return mixed
     */
    public function present(UserCreateResponse $response)
    {
        return view('backend.users.user.create', [
            'roles' => $response->getRoles(),
        ]);
    }
}
