<?php

namespace App\Presentation\Presenters\Web\Backend\User\User;

use App\Application\UseCases\Backend\User\User\Response\UserListResponse;
use App\Presentation\Presenters\Contracts\Backend\UserListPresenter;

/**
 * Class WebUserListPresenter
 * @package App\Presentation\Presenters\Web\Backend\User\User
 */
final class WebUserListPresenter implements UserListPresenter
{
    /**
     * @param UserListResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(UserListResponse $response)
    {
        return view('backend.users.user.index', [
            'users' => $response->getUsers()
        ]);
    }
}
