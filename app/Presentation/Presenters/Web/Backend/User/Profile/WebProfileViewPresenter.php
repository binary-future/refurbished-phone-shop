<?php

namespace App\Presentation\Presenters\Web\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Responses\ProfileViewResponse;
use App\Presentation\Presenters\Contracts\Backend\ProfileViewPresenter;

/**
 * Class WebProfileViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\User\Profile
 */
final class WebProfileViewPresenter implements ProfileViewPresenter
{
    /**
     * @param ProfileViewResponse $response
     * @return mixed
     */
    public function present(ProfileViewResponse $response)
    {
        return view('backend.users.profile.view', [
            'user' => $response->getUser(),
        ]);
    }
}
