<?php

namespace App\Presentation\Presenters\Web\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Responses\ProfileUpdateResponse;
use App\Presentation\Presenters\Contracts\Backend\ProfileUpdatePresenter;

final class WebProfileUpdatePresenter implements ProfileUpdatePresenter
{
    public function present(ProfileUpdateResponse $response)
    {
        $message = $response->isSucceed()
            ? 'Success! Your profile was updated'
            : $response->getMessage() ?: 'Error has occurred! Cannot update your profile';

        return redirect()->route('admin.profile.view')
            ->with($response->isSucceed() ? 'success' : 'error', $message);
    }

}