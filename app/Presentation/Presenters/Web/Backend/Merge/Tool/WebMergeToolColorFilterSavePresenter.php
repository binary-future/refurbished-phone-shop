<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ColorFilterSaveResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterSavePresenter;

final class WebMergeToolColorFilterSavePresenter implements MergeToolColorFilterSavePresenter
{
    public function present(ColorFilterSaveResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was saved', $response->getFilter()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route(
            'admin.merge.tool.color.main'
        )->with($status, $message);
    }
}
