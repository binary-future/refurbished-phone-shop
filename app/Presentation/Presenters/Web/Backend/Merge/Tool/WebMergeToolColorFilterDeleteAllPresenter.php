<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;

use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ColorFilterDeleteAllResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterDeleteAllPresenter;

final class WebMergeToolColorFilterDeleteAllPresenter implements MergeToolColorFilterDeleteAllPresenter
{
    public function present(ColorFilterDeleteAllResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! All filters were deleted')
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route('admin.merge.tool.color.main')->with($status, $message);
    }
}
