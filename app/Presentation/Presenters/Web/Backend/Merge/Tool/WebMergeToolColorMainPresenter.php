<?php

namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;

use App\Application\UseCases\Backend\Merge\Tool\Color\Responses\ToolMainResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorMainPresenter;
use App\Presentation\Services\MergeTool\ColorGroupsGenerator;

final class WebMergeToolColorMainPresenter implements MergeToolColorMainPresenter
{
    /**
     * @var ColorGroupsGenerator
     */
    private $mergeToolGenerator;

    /**
     * WebMergeToolColorMainPresenter constructor.
     * @param ColorGroupsGenerator $mergeToolGenerator
     */
    public function __construct(ColorGroupsGenerator $mergeToolGenerator)
    {
        $this->mergeToolGenerator = $mergeToolGenerator;
    }

    public function present(ToolMainResponse $response)
    {
        return view('backend.merge.tool.color.main', [
            'mergeTool' => $this->mergeToolGenerator->generate($response->getColors(), $response->getFilters())
        ]);
    }

}