<?php

namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;

use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeletePresenter;

final class WebMergeToolModelFilterDeletePresenter implements MergeToolModelFilterDeletePresenter
{
    public function present(ModelFilterDeleteResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was deleted', $response->getFilter()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route(
            'admin.merge.tool.model.view',
            ['brand' => $response->getBrand()->getSlug()]
        )->with($status, $message);
    }
}
