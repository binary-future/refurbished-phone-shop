<?php

namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;

use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ColorFilterDeleteResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterDeletePresenter;

final class WebMergeToolColorFilterDeletePresenter implements MergeToolColorFilterDeletePresenter
{
    public function present(ColorFilterDeleteResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was deleted', $response->getFilter()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route('admin.merge.tool.color.main')->with($status, $message);
    }
}
