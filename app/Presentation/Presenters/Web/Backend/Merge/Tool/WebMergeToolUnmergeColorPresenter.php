<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Color\Responses\UnmergeColorResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolUnmergeColorPresenter;

final class WebMergeToolUnmergeColorPresenter implements MergeToolUnmergeColorPresenter
{
    public function present(UnmergeColorResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was unmerged', $response->getColor()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route('admin.merge.tool.color.main')->with($status, $message);
    }
}
