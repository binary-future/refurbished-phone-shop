<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\MergeSynonymsResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolMergeSynonymsPresenter;

/**
 * Class WebMergeToolMergeSynonymsPresenter
 * @package App\Presentation\Presenters\Web\Backend\Merge\Tool
 */
final class WebMergeToolMergeSynonymsPresenter implements MergeToolMergeSynonymsPresenter
{
    /**
     * @param MergeSynonymsResponse $response
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function present(MergeSynonymsResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Merging succeed!')
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return response()->json([$status => $message]);
    }
}
