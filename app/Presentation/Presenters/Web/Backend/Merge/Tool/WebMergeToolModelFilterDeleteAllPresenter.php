<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterDeleteAllResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeleteAllPresenter;

final class WebMergeToolModelFilterDeleteAllPresenter implements MergeToolModelFilterDeleteAllPresenter
{
    public function present(ModelFilterDeleteAllResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! all filters were deleted')
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route('admin.merge.tool.model.main')->with($status, $message);
    }
}
