<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterDeleteByBrandResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeleteByBrandPresenter;

final class WebMergeToolModelFilterDeleteByBrandPresenter implements MergeToolModelFilterDeleteByBrandPresenter
{
    public function present(ModelFilterDeleteByBrandResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s filters were deleted', $response->getBrand()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route(
            'admin.merge.tool.model.view',
            ['brand' => $response->getBrand()->getSlug()]
        )->with($status, $message);
    }
}
