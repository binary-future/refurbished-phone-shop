<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\UnmergeModelResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolUnmergeModelPresenter;

final class WebMergeToolUnmergeModelPresenter implements MergeToolUnmergeModelPresenter
{
    public function present(UnmergeModelResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was unmerged', $response->getModel()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route(
            'admin.merge.tool.model.view',
            ['brand' => $response->getModel()->getBrand()->getSlug()]
        )->with($status, $message);
    }
}
