<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterSaveResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterSavePresenter;

final class WebMergeToolModelFilterSavePresenter implements MergeToolModelFilterSavePresenter
{
    public function present(ModelFilterSaveResponse $response)
    {
        $message = $response->isSucceed()
            ? sprintf('Success! %s was saved', $response->getFilter()->getName())
            : 'Error was occurred';
        $status = $response->isSucceed() ? 'success' : 'error';

        return redirect()->route(
            'admin.merge.tool.model.view',
            ['brand' => $response->getBrand()->getSlug()]
        )->with($status, $message);
    }
}
