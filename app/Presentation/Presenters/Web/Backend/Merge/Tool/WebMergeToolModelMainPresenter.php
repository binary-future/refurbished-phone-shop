<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\ToolMainResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelMainPresenter;

/**
 * Class WebMergeToolModelMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Merge\Tool
 */
final class WebMergeToolModelMainPresenter implements MergeToolModelMainPresenter
{
    /**
     * @param ToolMainResponse $response
     * @return mixed
     */
    public function present(ToolMainResponse $response)
    {
        return view('backend.merge.tool.models.main', [
            'brands' => $response->getBrands(),
            'filters' => $response->getFilters()->groupBy('brand_id'),
        ]);
    }
}
