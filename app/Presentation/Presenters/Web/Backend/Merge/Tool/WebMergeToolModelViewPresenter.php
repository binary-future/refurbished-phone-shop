<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Tool;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\ToolViewResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelViewPresenter;
use App\Presentation\Services\MergeTool\ModelGroupsGenerator;

/**
 * Class WebMergeToolModelViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Merge\Tool
 */
final class WebMergeToolModelViewPresenter implements MergeToolModelViewPresenter
{
    /**
     * @var ModelGroupsGenerator
     */
    private $mergeToolGenerator;

    /**
     * WebMergeToolModelViewPresenter constructor.
     * @param ModelGroupsGenerator $mergeToolGenerator
     */
    public function __construct(ModelGroupsGenerator $mergeToolGenerator)
    {
        $this->mergeToolGenerator = $mergeToolGenerator;
    }

    /**
     * @param ToolViewResponse $response
     * @return mixed
     */
    public function present(ToolViewResponse $response)
    {
        return view('backend.merge.tool.models.view', [
            'brand' => $response->getBrand(),
            'mergeTool' => $this->mergeToolGenerator->generate($response->getModels(), $response->getFilters())
        ]);
    }
}
