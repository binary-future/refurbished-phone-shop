<?php

namespace App\Presentation\Presenters\Web\Backend\Merge\Main;

use App\Application\UseCases\Backend\Merge\Main\Responses\MergeMainResponse;
use App\Presentation\Presenters\Contracts\Backend\MergeMainPresenter;

/**
 * Class WebMergeMainPresenter
 * @package App\Presentation\Presenters\Web\Backend\Merge\Main
 */
final class WebMergeMainPresenter implements MergeMainPresenter
{
    /**
     * @param MergeMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function present(MergeMainResponse $response)
    {
        return view('backend.merge.main', [
            'models' => $response->getModels(),
            'synonymModels' => $response->getSynonymModels(),
            'colors' => $response->getColors(),
            'synonymColors' => $response->getSynonymColors(),
            'modelsFilter' => $response->getModelsFilter(),
            'colorsFilter' => $response->getColorsFilter(),
        ]);
    }
}
