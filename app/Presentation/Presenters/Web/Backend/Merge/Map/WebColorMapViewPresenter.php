<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Map;


use App\Application\UseCases\Backend\Merge\Map\Color\Responses\MapViewResponse;
use App\Presentation\Presenters\Contracts\Backend\ColorMapViewPresenter;

/**
 * Class WebModelMapViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Merge\Map
 */
final class WebColorMapViewPresenter implements ColorMapViewPresenter
{
    /**
     * @param MapViewResponse $response
     * @return mixed
     */
    public function present(MapViewResponse $response)
    {
        return view('backend.merge.map.color.view', [
            'colors' => $response->getColors()
        ]);
    }
}
