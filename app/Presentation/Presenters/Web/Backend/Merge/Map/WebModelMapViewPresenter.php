<?php


namespace App\Presentation\Presenters\Web\Backend\Merge\Map;


use App\Application\UseCases\Backend\Merge\Map\Model\Responses\MapViewResponse;
use App\Presentation\Presenters\Contracts\Backend\ModelMapViewPresenter;

/**
 * Class WebModelMapViewPresenter
 * @package App\Presentation\Presenters\Web\Backend\Merge\Map
 */
final class WebModelMapViewPresenter implements ModelMapViewPresenter
{
    /**
     * @param MapViewResponse $response
     * @return mixed|void
     */
    public function present(MapViewResponse $response)
    {
        return view('backend.merge.map.models.view', [
            'models' => $response->getModels()
        ]);
    }
}
