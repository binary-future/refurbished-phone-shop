<?php


namespace App\Presentation\Presenters\Web\Frontend\General;


use App\Presentation\Presenters\Contracts\ContactSendPresenter;
use App\Presentation\Services\Notifications\UserContactNotification;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use App\Utils\Adapters\Notificator\Contracts\Notificator;

final class WebContactSendPresenter implements ContactSendPresenter
{
    /**
     * @var Notificator
     */
    private $notificator;
    /**
     * @var ChannelLogger
     */
    private $logManager;

    /**
     * WebContactSendPresenter constructor.
     * @param Notificator $notificator
     * @param ChannelLogger $logManager
     */
    public function __construct(Notificator $notificator, ChannelLogger $logManager)
    {
        $this->notificator = $notificator;
        $this->logManager = $logManager;
    }

    public function present(array $params)
    {
        try {
            $this->notificator->notify(new UserContactNotification($params));
            return redirect()->route('contact.us')
                ->with('emailSent', 'Your message was sent. A member of the oneCompare team will get back to you soon.');
        } catch (\Throwable $exception) {
            $this->logManager->channel(ChannelLogger::CHANNEL_STACK)->error($exception);
            return redirect()->route('contact.us')->with('error', 'Sorry, an error occurred. Please email info@onecompare.com instead.');
        }
    }
}
