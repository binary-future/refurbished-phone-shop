<?php

namespace App\Presentation\Presenters\Web\Frontend\Main;

use App\Application\UseCases\Main\Responses\MainPageResponse;
use App\Presentation\Presenters\Contracts\HomePagePresenter;

/**
 * Class WebHomePagePresenter
 * @package App\Presentation\Presenters\Web\Frontend\Main
 */
final class WebHomePagePresenter implements HomePagePresenter
{
    /**
     * @param MainPageResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function present(MainPageResponse $response)
    {
        return view('frontend.home', [
            'stores' => $response->getStores(),
            'topModels' => $response->getTopModels(),
            'latestModels' => $response->getLatestModels(),
            'taggedPosts' => $response->getTaggedPosts(),
        ]);
    }
}
