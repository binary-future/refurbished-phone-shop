<?php


namespace App\Presentation\Presenters\Web\Frontend\Main;


use App\Application\UseCases\Main\Responses\SitemapsResponse;
use App\Presentation\Presenters\Contracts\SitemapsPresenter;
use App\Presentation\Services\Sitemaps\Generator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class WebSitemapsPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Main
 */
final class WebSitemapsPresenter implements SitemapsPresenter
{
    /**
     * @var Generator
     */
    private $generator;

    /**
     * WebSitemapsPresenter constructor.
     * @param Generator $generator
     */
    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
    }

    /**
     * @param SitemapsResponse $response
     * @return mixed
     */
    public function present(SitemapsResponse $response)
    {
        try {
            $sitemap = $this->generator->generate($response);

            return $sitemap->render();
        } catch (\Throwable $exception) {
            throw new NotFoundHttpException();
        }
    }
}
