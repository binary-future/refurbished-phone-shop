<?php


namespace App\Presentation\Presenters\Web\Frontend\Deals;


use App\Application\UseCases\Deal\Responses\PhoneDealResponse;
use App\Presentation\Presenters\Contracts\PhoneDealPresenter;

/**
 * Class WebPhoneDealPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Deals
 */
final class WebPhoneDealPresenter implements PhoneDealPresenter
{
    /**
     * @param PhoneDealResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(PhoneDealResponse $response)
    {
        return view('frontend.deals.deals.phone-view', [
            'deal' => $response->getDeal(),
            'phone' => $response->getPhone(),
            'relatedDeals' => $response->getRelatedDeals(),
        ]);
    }
}
