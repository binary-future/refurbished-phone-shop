<?php

namespace App\Presentation\Presenters\Web\Frontend\Deals;

use App\Application\UseCases\Deal\Responses\DealOutlinkGoResponse;
use App\Application\UseCases\Deal\Responses\DealOutlinkResponse;
use App\Presentation\Presenters\Contracts\DealOutlinkPresenter;

/**
 * Class WebDealOutlinkPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Deals
 */
final class WebDealOutlinkPresenter implements DealOutlinkPresenter
{
    /**
     * @param DealOutlinkResponse $response
     * @return mixed|void
     */
    public function present(DealOutlinkResponse $response)
    {
        return view('frontend.deals.deals.deal-outlink', [
            'urlToGo' => $response->getUrlToGo(),
            'store' => $response->getStore(),
        ]);
    }
}
