<?php


namespace App\Presentation\Presenters\Web\Frontend\Deals;


use App\Application\UseCases\Deal\Responses\PhoneDealResponse;
use App\Presentation\Presenters\Contracts\PhoneDealPresenter;

/**
 * Class WebPhoneDealPopupPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Deals
 */
final class WebPhoneDealPopupPresenter implements PhoneDealPresenter
{
    /**
     * @param PhoneDealResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     * @throws \Throwable
     */
    public function present(PhoneDealResponse $response)
    {
        $html = view('frontend.deals.components.deals.phone-view-popup', [
            'deal' => $response->getDeal(),
            'phone' => $response->getPhone()
        ])->render();

        return response()->json(['html' => $html]);
    }
}
