<?php

namespace App\Presentation\Presenters\Web\Frontend\Deals;

use App\Application\UseCases\Deal\Responses\DealOutlinkGoResponse;
use App\Presentation\Presenters\Contracts\DealOutlinkGoPresenter;

/**
 * Class WebDealOutlinkGoPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Deals
 */
final class WebDealOutlinkGoPresenter implements DealOutlinkGoPresenter
{
    /**
     * @param DealOutlinkGoResponse $response
     * @return mixed|void
     */
    public function present(DealOutlinkGoResponse $response)
    {
        return redirect($response->getLink()->getLink());
    }
}
