<?php


namespace App\Presentation\Presenters\Web\Frontend\Store;


use App\Application\UseCases\Store\Responses\StoreMainResponse;
use App\Presentation\Presenters\Contracts\StoreMainPresenter;

/**
 * Class WebStoreMainPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Store
 */
final class WebStoreMainPresenter implements StoreMainPresenter
{
    /**
     * @param StoreMainResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(StoreMainResponse $response)
    {
        return view('frontend.stores.index', [
            'stores' => $response->getStores()
        ]);
    }
}