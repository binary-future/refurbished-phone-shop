<?php


namespace App\Presentation\Presenters\Web\Frontend\Store;

use App\Application\UseCases\Store\Responses\StoreViewResponse;
use App\Presentation\Presenters\Contracts\StoreViewPresenter;
use App\Presentation\Services\Translators\StorePhones\Translator;

/**
 * Class WebStoreViewPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Store
 */
final class WebStoreViewPresenter implements StoreViewPresenter
{
    /**
     * @var Translator
     */
    private $phoneTranslator;

    /**
     * WebStoreViewPresenter constructor.
     * @param Translator $phoneTranslator
     */
    public function __construct(Translator $phoneTranslator)
    {
        $this->phoneTranslator = $phoneTranslator;
    }

    /**
     * @param StoreViewResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(StoreViewResponse $response)
    {
        return view('frontend.stores.view', [
            'store' => $response->getStore(),
            'phones' => $this->phoneTranslator->translate($response->getPhones())
        ]);
    }
}