<?php


namespace App\Presentation\Presenters\Web\Frontend\Phones;


use App\Application\UseCases\Phones\Responses\ModelSearchResponse;
use App\Presentation\Presenters\Contracts\ModelSearchPresenter;
use App\Presentation\Services\Translators\Contracts\Translator;

final class WebModelAutocompletePresenter implements ModelSearchPresenter
{
    /**
     * @var Translator
     */
    private $trasnaltor;

    /**
     * WebModelAutocompletePresenter constructor.
     * @param Translator $trasnaltor
     */
    public function __construct(Translator $trasnaltor)
    {
        $this->trasnaltor = $trasnaltor;
    }

    public function present(ModelSearchResponse $response)
    {
        $models = $response->getModels();

        return response()->json(['models' => $this->trasnaltor->translate($models)]);
    }
}