<?php


namespace App\Presentation\Presenters\Web\Frontend\Phones;


use App\Application\UseCases\Phones\Responses\ModelSearchResponse;
use App\Presentation\Presenters\Contracts\ModelSearchPresenter;

final class WebModelSearchPresenter implements ModelSearchPresenter
{
    public function present(ModelSearchResponse $response)
    {
        return view('frontend.phone.model.search', ['models' => $response->getModels()]);
    }
}