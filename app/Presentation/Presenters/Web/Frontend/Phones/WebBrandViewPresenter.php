<?php

namespace App\Presentation\Presenters\Web\Frontend\Phones;

use App\Application\UseCases\Phones\Responses\BrandViewResponse;
use App\Domain\Phone\Brand\Brand;
use App\Presentation\Presenters\Contracts\BrandViewPresenter;
use App\Presentation\Services\Dropdowns\ModelViewSortDropdown;
use App\Presentation\Services\Filters\Contracts\Generator;
use App\Presentation\Services\Filters\Scenario\BrandViewLeftSideBar;
use App\Presentation\Services\Generators\LastCheckedPhraseGenerator;
use App\Presentation\Services\Translators\Phones\ConditionTranslator;
use Illuminate\Support\Collection;

/**
 * Class WebBrandViewPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Phones
 */
final class WebBrandViewPresenter implements BrandViewPresenter
{
    private const RELATIVE_MODELS_CHUNK_SIZE = 3;
    private const FAKE_LASTUPDATE_MORE_THAN_DAYS = 5;

    /**
     * @var Generator
     */
    private $filterGenerator;

    /**
     * @var ModelViewSortDropdown
     */
    private $sortByGenerator;

    /**
     * @var LastCheckedPhraseGenerator
     */
    private $lastCheckedGenerator;
    /**
     * @var ConditionTranslator
     */
    private $conditionTranslator;

    /**
     * WebModelViewPresenter constructor.
     * @param Generator $filterGenerator
     * @param ModelViewSortDropdown $sortByGenerator
     * @param LastCheckedPhraseGenerator $lastCheckedGenerator
     * @param ConditionTranslator $conditionTranslator
     */
    public function __construct(
        Generator $filterGenerator,
        ModelViewSortDropdown $sortByGenerator,
        LastCheckedPhraseGenerator $lastCheckedGenerator,
        ConditionTranslator $conditionTranslator
    ) {
        $this->filterGenerator = $filterGenerator;
        $this->sortByGenerator = $sortByGenerator;
        $this->lastCheckedGenerator = $lastCheckedGenerator;
        $this->conditionTranslator = $conditionTranslator;
    }

    /**
     * @param BrandViewResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(BrandViewResponse $response)
    {
        return view('frontend.phone.brand.view', [
            'brand' => $response->getBrand(),
            'models' => $response->getModels(),
            'phones' => $response->getPhones(),
            'deals' => $response->getDeals(),
            'sideBarFilters' => $this->filterGenerator
                ->generate(
                    new BrandViewLeftSideBar(
                        $response->getAllNetworks(),
                        $this->conditionTranslator->translate($response->getAllConditions()),
                        $response->getModels(),
                        $response->getEnabledFilters(),
                        $response->getAdjustedFilters(),
                    )
                ),
            'sortByDropdown' => $this->sortByGenerator->generate($response->getAdjustedFilters()->getSort()),
            'chunkedModels' => $response->getModels()->chunk(self::RELATIVE_MODELS_CHUNK_SIZE),
            'lastChecked' => $this->getLastCheckedDate($response->getBrand()),
        ]);
    }

    /**
     * @param Brand $brand
     * @return string|null
     */
    private function getLastCheckedDate(Brand $brand): ?string
    {
        $generator = $this->lastCheckedGenerator;
        $updatedAt = $brand->getUpdatedAt();

        return $generator->generateFakePhrase(
            $updatedAt,
            self::FAKE_LASTUPDATE_MORE_THAN_DAYS
        );
    }
}
