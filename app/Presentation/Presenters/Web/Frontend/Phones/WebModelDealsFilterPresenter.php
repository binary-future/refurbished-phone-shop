<?php

namespace App\Presentation\Presenters\Web\Frontend\Phones;

use App\Application\UseCases\Phones\Responses\ModelDealsFilterResponse;
use App\Domain\Phone\Phone\Phone;
use App\Presentation\Presenters\Contracts\ModelDealsFilterPresenter;
use App\Presentation\Services\Filters\Contracts\Generator;
use App\Presentation\Services\Filters\Scenario\ModelViewLeftSideBar;
use App\Presentation\Services\Filters\Scenario\ModelViewPhones;
use App\Presentation\Services\Templates\FiltersTemplatesRenderer;
use App\Presentation\Services\Translators\Phones\ConditionTranslator;

final class WebModelDealsFilterPresenter implements ModelDealsFilterPresenter
{
    /**
     * @var Generator
     */
    private $filterGenerator;

    /**
     * @var FiltersTemplatesRenderer
     */
    private $filterRenderer;
    /**
     * @var ConditionTranslator
     */
    private $conditionTranslator;

    /**
     * WebModelDealsFilterPresenter constructor.
     * @param Generator $filterGenerator
     * @param ConditionTranslator $conditionTranslator
     * @param FiltersTemplatesRenderer $filtersRenderer
     */
    public function __construct(
        Generator $filterGenerator,
        ConditionTranslator $conditionTranslator,
        FiltersTemplatesRenderer $filtersRenderer
    ) {
        $this->filterGenerator = $filterGenerator;
        $this->filterRenderer = $filtersRenderer;
        $this->conditionTranslator = $conditionTranslator;
    }

    /**
     * @param ModelDealsFilterResponse $response
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function present(ModelDealsFilterResponse $response)
    {
        $sidebarFilters = $this->filterGenerator->generate(
            new ModelViewLeftSideBar(
                $response->getNetworks(),
                $this->conditionTranslator->translate($response->getConditions()),
                $response->getDealIndex(),
                $response->getAdjustedFilters(),
            )
        );
        $phonesFilter = $this->filterGenerator
            ->generate(
                new ModelViewPhones(
                    $response->getPhones(),
                    $response->getDealsInfo(),
                    $response->getDealIndex()
                )
            );

        $items = view('frontend.deals.components.deals.deal-items', [
            'deals' => $response->getDeals(),
            'phones' => $response->getPhones()->keyBy(Phone::FIELD_ID)
        ])->render();

        $pagination = view('frontend.deals.components.deals.deals-list-pagination', [
            'deals' => $response->getDeals(),
        ])->render();

        return response()->json([
            'pagination' => $pagination,
            'items' => $items,
            'filters' => $this->filterRenderer
                ->renderFilters(
                    array_merge($phonesFilter->toArray(), $sidebarFilters->toArray()),
                    $response->getAdjustedFilters()->toArray()
                )
        ]);
    }
}
