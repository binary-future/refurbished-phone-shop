<?php

namespace App\Presentation\Presenters\Web\Frontend\Phones;

use App\Application\UseCases\Phones\Responses\BrandListResponse;
use App\Presentation\Presenters\Contracts\BrandsListPresenter;

/**
 * Class WebBrandListPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Phones
 */
final class WebBrandListPresenter implements BrandsListPresenter
{
    /**
     * @param BrandListResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(BrandListResponse $response)
    {
        return view('frontend.phone.brand.index', [
            'brands' => $response->getBrands()
        ]);
    }
}
