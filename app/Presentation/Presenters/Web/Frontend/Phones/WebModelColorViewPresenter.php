<?php


namespace App\Presentation\Presenters\Web\Frontend\Phones;


use App\Application\UseCases\Phones\Responses\ModelColorViewResponse;
use App\Presentation\Presenters\Contracts\ModelColorViewPresenter;

/**
 * Class WebModelColorViewPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Phones
 */
final class WebModelColorViewPresenter implements ModelColorViewPresenter
{
    /**
     * @param ModelColorViewResponse $response
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function present(ModelColorViewResponse $response)
    {
        return view('frontend.phone.model.color-view', [
            'brand' => $response->getBrand(),
            'model' => $response->getModel(),
            'color' => $response->getColor(),
            'deals' => $response->getDeals(),
        ]);
    }
}
