<?php

namespace App\Presentation\Presenters\Web\Frontend\Phones;

use App\Application\UseCases\Phones\Responses\ModelViewResponse;
use App\Domain\Phone\Phone\Phone;
use App\Presentation\Presenters\Contracts\ModelViewPresenter;
use App\Presentation\Services\Dropdowns\ModelViewSortDropdown;
use App\Presentation\Services\Filters\Contracts\Generator;
use App\Presentation\Services\Filters\Scenario\ModelViewLeftSideBar;
use App\Presentation\Services\Filters\Scenario\ModelViewPhones;
use App\Presentation\Services\Generators\LastCheckedPhraseGenerator;
use App\Presentation\Services\Generators\Title\Contracts\TitleGenerator;
use App\Presentation\Services\Translators\DealsInfo\ModelViewDealsTranslator;
use App\Presentation\Services\Translators\PhoneImages\Translator as PhoneImagesTranslator;
use App\Presentation\Services\Translators\Phones\ConditionTranslator;
use App\Presentation\Services\Translators\PhoneSpecs\Translator as PhoneSpecTranslator;

/**
 * Class WebModelViewPresenter
 * @package App\Presentation\Presenters\Web\Frontend\Phones
 */
final class WebModelViewPresenter implements ModelViewPresenter
{
    private const FAKE_LASTUPDATE_MORE_THAN_DAYS = 5;

    /**
     * @var Generator
     */
    private $filterGenerator;

    /**
     * @var PhoneSpecTranslator
     */
    private $phoneSpecTranslator;

    /**
     * @var PhoneImagesTranslator
     */
    private $phoneImagesTranslator;

    /**
     * @var ModelViewSortDropdown
     */
    private $sortByGenerator;

    /**
     * @var LastCheckedPhraseGenerator
     */
    private $lastCheckedGenerator;

    /**
     * @var ModelViewDealsTranslator
     */
    private $dealsInfoTranslator;

    /**
     * @var TitleGenerator
     */
    private $titleGenerator;
    /**
     * @var ConditionTranslator
     */
    private $conditionTranslator;

    /**
     * WebModelViewPresenter constructor.
     * @param Generator $filterGenerator
     * @param PhoneSpecTranslator $phoneSpecTranslator
     * @param PhoneImagesTranslator $phoneImagesTranslator
     * @param ConditionTranslator $conditionTranslator
     * @param ModelViewSortDropdown $sortByGenerator
     * @param LastCheckedPhraseGenerator $lastCheckedGenerator
     * @param ModelViewDealsTranslator $dealsInfoTranslator
     * @param TitleGenerator $titleGenerator
     */
    public function __construct(
        Generator $filterGenerator,
        PhoneSpecTranslator $phoneSpecTranslator,
        PhoneImagesTranslator $phoneImagesTranslator,
        ConditionTranslator $conditionTranslator,
        ModelViewSortDropdown $sortByGenerator,
        LastCheckedPhraseGenerator $lastCheckedGenerator,
        ModelViewDealsTranslator $dealsInfoTranslator,
        TitleGenerator $titleGenerator
    ) {
        $this->filterGenerator = $filterGenerator;
        $this->phoneSpecTranslator = $phoneSpecTranslator;
        $this->phoneImagesTranslator = $phoneImagesTranslator;
        $this->sortByGenerator = $sortByGenerator;
        $this->lastCheckedGenerator = $lastCheckedGenerator;
        $this->dealsInfoTranslator = $dealsInfoTranslator;
        $this->titleGenerator = $titleGenerator;
        $this->conditionTranslator = $conditionTranslator;
    }

    /**
     * @param ModelViewResponse $response
     * @return mixed
     */
    public function present(ModelViewResponse $response)
    {
        return view('frontend.phone.model.view', [
            'pageTitle' => $this->titleGenerator->generate($response->getBrand(), $response->getModel()),
            'brand' => $response->getBrand(),
            'model' => $response->getModel(),
            'phones' => $response->getPhones()->keyBy(Phone::FIELD_ID),
            'deals' => $response->getDeals(),
            'sideBarFilters' => $this->filterGenerator
                ->generate(new ModelViewLeftSideBar(
                    $response->getNetworks(),
                    $this->conditionTranslator->translate($response->getConditions()),
                    $response->getEnabledFilters(),
                    $response->getAdjustedFilters(),
                )),
            'phonesFilter' => $this->filterGenerator
                ->generate(
                    new ModelViewPhones(
                        $response->getPhones(),
                        $response->getDealsInfo(),
                        $response->getEnabledFilters()
                    )
                ),
            'sortByDropdown' => $this->sortByGenerator->generate($response->getAdjustedFilters()->getSort()),
            'specs' => $this->phoneSpecTranslator->translateSpecs($response->getModel()),
            'lastChecked' => $this->getLastCheckedDate($response),
            'phoneImages' => $this->phoneImagesTranslator->translate($response->getPhones()),
            'bestDeal' => $response->getDealsInfo()->getBestDeal(),
            'dealsInfo' => $this->dealsInfoTranslator->translate($response->getModel(), $response->getDealsInfo()),
            'tagPosts' => $response->getPosts(),
        ]);
    }

    /**
     * @param ModelViewResponse $response
     * @return string|null
     */
    private function getLastCheckedDate(ModelViewResponse $response): ?string
    {
        $generator = $this->lastCheckedGenerator;
        $updatedAt = $response->getModel()->getUpdatedAt();

        return $response->getDeals()->isEmpty()
            ? $generator->generatePhrase($updatedAt)
            : $generator->generateFakePhrase(
                $updatedAt,
                self::FAKE_LASTUPDATE_MORE_THAN_DAYS
            );
    }
}
