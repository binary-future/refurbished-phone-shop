<?php

namespace App\Presentation\Presenters\Web\Frontend\Phones;

use App\Application\UseCases\Phones\Responses\BrandViewDealsFilterResponse;
use App\Domain\Phone\Phone\Phone;
use App\Presentation\Presenters\Contracts\BrandDealsFilterPresenter;
use App\Presentation\Services\Filters\Contracts\Generator;
use App\Presentation\Services\Filters\Scenario\BrandViewLeftSideBar;
use App\Presentation\Services\Templates\FiltersTemplatesRenderer;
use App\Presentation\Services\Translators\Phones\ConditionTranslator;

final class WebBrandDealsFilterPresenter implements BrandDealsFilterPresenter
{
    /**
     * @var Generator
     */
    private $filterGenerator;

    /**
     * @var FiltersTemplatesRenderer
     */
    private $filtersRenderer;
    /**
     * @var ConditionTranslator
     */
    private $conditionTranslator;

    /**
     * WebModelViewPresenter constructor.
     * @param Generator $filterGenerator
     * @param FiltersTemplatesRenderer $filtersRenderer
     * @param ConditionTranslator $conditionTranslator
     */
    public function __construct(
        Generator $filterGenerator,
        FiltersTemplatesRenderer $filtersRenderer,
        ConditionTranslator $conditionTranslator
    ) {
        $this->filterGenerator = $filterGenerator;
        $this->filtersRenderer = $filtersRenderer;
        $this->conditionTranslator = $conditionTranslator;
    }

    /**
     * @param BrandViewDealsFilterResponse $response
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws \Throwable
     */
    public function present(BrandViewDealsFilterResponse $response)
    {
        $filters = $this->filterGenerator->generate(
            new BrandViewLeftSideBar(
                $response->getAllNetworks(),
                $this->conditionTranslator->translate($response->getAllConditions()),
                $response->getModels(),
                $response->getDealIndex(),
                $response->getAdjustedFilters()
            )
        );

        $items = view('frontend.deals.components.deals.deal-items', [
            'deals' => $response->getDeals(),
            'phones' => $response->getPhones()->keyBy(Phone::FIELD_ID)
        ])->render();

        $pagination = view('frontend.deals.components.deals.deals-list-pagination', [
            'deals' => $response->getDeals(),
        ])->render();

        return response()->json([
            'pagination' => $pagination,
            'items' => $items,
            'filters' => $this->filtersRenderer
                ->renderFilters($filters->toArray(), $response->getAdjustedFilters()->toArray())
        ]);
    }
}
