<?php

namespace App\Presentation\Presenters\Console;

use App\Application\UseCases\Deal\Responses\HardDealsClearResponse;
use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Presentation\Presenters\Contracts\HardDeleteOldDealsPresenter as Contract;
use App\Utils\Adapters\Log\ConsoleLogger;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use Illuminate\Support\Facades\Log;

final class HardDeleteOldDealsPresenter implements Contract
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;
    /**
     * @var ChannelLogger
     */
    private $channelLogger;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     * @param ChannelLogger $channelLogger
     */
    public function __construct(ConsoleLogger $outPut, ChannelLogger $channelLogger)
    {
        $this->outPut = $outPut;
        $this->channelLogger = $channelLogger;
    }

    /**
     * @param HardDealsClearResponse $response
     * @return mixed|void
     */
    public function present(HardDealsClearResponse $response)
    {
        $response->isError()
            ? $this->logError()
            : $this->logSuccess($response);
    }

    private function logError()
    {
        $message = 'Hard clearing was failed';
        $this->outPut->error($message);
        $this->channelLogger
            ->channel(ChannelLogger::CHANNEL_DEALS_DELETE_DAILY)
            ->error($message);
    }

    private function logSuccess(HardDealsClearResponse $response)
    {
        $message = sprintf(
            'Deals hard deletion succeed:' . PHP_EOL
            . 'deals deleted - %d' . PHP_EOL
            . 'links deleted - %d',
            $response->getDeletedDeals(),
            $response->getDeletedLinks());
        $this->outPut->info($message);
        $this->channelLogger
            ->channel(ChannelLogger::CHANNEL_DEALS_DELETE_DAILY)
            ->error($message);
    }
}
