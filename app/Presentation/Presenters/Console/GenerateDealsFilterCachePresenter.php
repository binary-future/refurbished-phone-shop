<?php

namespace App\Presentation\Presenters\Console;

use App\Application\UseCases\Deal\Responses\GenerateDealsFiltersCacheResponse;
use App\Utils\Adapters\Log\ConsoleLogger;

final class GenerateDealsFilterCachePresenter implements \App\Presentation\Presenters\Contracts\GenerateDealsFilterCachePresenter
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     */
    public function __construct(ConsoleLogger $outPut)
    {
        $this->outPut = $outPut;
    }

    /**
     * @param GenerateDealsFiltersCacheResponse[] $response
     * @return mixed|void
     */
    public function present(iterable $response)
    {
        foreach ($response as $item) {
            $item->isSuccess()
                ? $this->logSuccess($item)
                : $this->logError($item);
        }
    }

    private function logError(GenerateDealsFiltersCacheResponse $response)
    {
        $this->outPut->error('Filter generation failed for brand: ' . $response->getBrand());
    }

    private function logSuccess(GenerateDealsFiltersCacheResponse $response)
    {
        $this->outPut->info('Filter cached successfully for brand: ' . $response->getBrand());
    }
}
