<?php


namespace App\Presentation\Presenters\Console;


use App\Application\UseCases\System\Results\DealsFiltersGenerationResult;
use App\Presentation\Presenters\Contracts\System\DealsFiltersGenerationPresenter;
use App\Utils\Adapters\Log\ConsoleLogger;

/**
 * Class ConsoleDealsFiltersGenerationPresenter
 * @package App\Presentation\Presenters\Console
 */
final class ConsoleDealsFiltersGenerationPresenter implements DealsFiltersGenerationPresenter
{
    /**
     * @var ConsoleLogger
     */
    private $output;

    /**
     * ConsoleDealsFiltersGenerationPresenter constructor.
     * @param ConsoleLogger $output
     */
    public function __construct(ConsoleLogger $output)
    {
        $this->output = $output;
    }

    /**
     * @param DealsFiltersGenerationResult $result
     * @return mixed|void
     */
    public function present(DealsFiltersGenerationResult $result)
    {
        $this->output->info(sprintf('%d - filters were created', $result->getSucced()));
        $this->output->error(sprintf('%d - filters were failed', $result->getFailed()));
    }
}
