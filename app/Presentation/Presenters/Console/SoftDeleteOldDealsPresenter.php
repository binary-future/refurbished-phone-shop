<?php

namespace App\Presentation\Presenters\Console;

use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Presentation\Presenters\Contracts\SoftDeleteOldDealsPresenter as Contract;
use App\Utils\Adapters\Log\ConsoleLogger;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;

final class SoftDeleteOldDealsPresenter implements Contract
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     */
    public function __construct(ConsoleLogger $outPut)
    {
        $this->outPut = $outPut;
    }

    /**
     * @param SoftDealsClearResponse[] $response
     * @return mixed|void
     */
    public function present(array $response)
    {
        foreach ($response as $item) {
            $item->isError()
                ? $this->logError($item)
                : $this->logSuccess($item);
        }
    }

    private function logError(SoftDealsClearResponse $response)
    {
        $this->outPut->error(sprintf('Clearing %s was failed', $response->getStore()->getName()));
        Log::channel('deals-delete')->info(
            sprintf('Clearing %s was failed', $response->getStore()->getName())
        );
    }

    private function logSuccess(SoftDealsClearResponse $response)
    {
        $this->outPut->line(sprintf('%s:', $response->getStore()->getName()));
        $this->outPut->info(sprintf('Deals deletion succeed - %d', $response->getSuccess()));
        $this->outPut->error(sprintf('Deals deletion failed - %d', $response->getFailed()));
        Log::channel('deals-delete')->info(
            sprintf(
                '%s: succeed - %d, failed - %d',
                $response->getStore()->getName(),
                $response->getSuccess(),
                $response->getFailed()
            )
        );
    }
}
