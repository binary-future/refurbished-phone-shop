<?php

namespace App\Presentation\Presenters\Console;

use App\Application\Services\Importer\Response\Contracts\ImportResponse;
use App\Presentation\Presenters\Contracts\ImportReportPresenter as Contract;
use App\Utils\Adapters\Log\ConsoleLogger;

final class ImportReportPresenter implements Contract
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     */
    public function __construct(ConsoleLogger $outPut)
    {
        $this->outPut = $outPut;
    }

    public function present(ImportResponse $response)
    {
        $content = $response->getContent();
        if (! $response->isSuccess()) {
            $this->outPut->error($content['error']);
            return;
        }

        $this->outPut->info('Success');
        $this->outPut->line('Report');
        foreach ($content as $item => $value) {
            $this->outPut->line(sprintf('%s - %s', $item, $value));
        }
    }
}
