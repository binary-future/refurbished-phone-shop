<?php


namespace App\Presentation\Presenters\Console;


use App\Application\UseCases\Import\Responses\PlannedImportResponse;
use App\Presentation\Presenters\Contracts\StartPlannedImportPresenter;
use App\Utils\Adapters\Log\ConsoleLogger;
use Illuminate\Support\Facades\Log;

/**
 * Class ConsoleStartPlannedImportPresenter
 * @package App\Presentation\Presenters\Console
 */
final class ConsoleStartPlannedImportPresenter implements StartPlannedImportPresenter
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     */
    public function __construct(ConsoleLogger $outPut)
    {
        $this->outPut = $outPut;
    }

    /**
     * @param PlannedImportResponse $response
     * @return mixed|void
     */
    public function present(PlannedImportResponse $response)
    {
        $response->isSuccess()
            ? $this->logSuccess()
            : $this->logError($response);
    }

    /**
     * @param PlannedImportResponse $response
     */
    private function logError(PlannedImportResponse $response)
    {
        $message = sprintf('Planned import was failed. %s', $response->getMessage() ?: '');
        $this->outPut->error($message);
        Log::channel('planned-import')->info($message);
    }

    private function logSuccess()
    {
        $message = 'Planned import passed';
        $this->outPut->info($message);
        Log::channel('planned-import')->info($message);
    }
}
