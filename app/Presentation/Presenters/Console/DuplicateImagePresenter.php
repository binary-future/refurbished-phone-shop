<?php


namespace App\Presentation\Presenters\Console;

use App\Application\UseCases\Phones\Responses\DuplicateImageResponse;
use App\Presentation\Presenters\Contracts\DuplicateImagePresenter as Contract;
use App\Utils\Adapters\Log\ConsoleLogger;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;

final class DuplicateImagePresenter implements Contract
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;
    /**
     * @var ChannelLogger
     */
    private $channelLogger;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     * @param ChannelLogger $channelLogger
     */
    public function __construct(ConsoleLogger $outPut, ChannelLogger $channelLogger)
    {
        $this->outPut = $outPut;
        $this->channelLogger = $channelLogger;
    }

    /**
     * @param DuplicateImageResponse[] $response
     * @return mixed|void
     */
    public function present(iterable $response)
    {
        $this->outPut->info('Starting duplication procedure');

        foreach ($response as $item) {
            $item->isSuccess()
                ? $this->logSuccess($item)
                : $this->logError($item);
        }

        $this->outPut->info('Duplication finished');
    }

    private function logError(DuplicateImageResponse $response)
    {
        if ($response->getPhone()) {
            $message = sprintf(
                'Duplicating of phone %s id were failed. Message: %s. Reason: %s',
                $response->getPhone()->getKey(),
                $response->getMessage(),
                $response->getException() ? $response->getException()->getMessage() : ''
            );
        } else {
            $message = sprintf(
                'Duplicating were failed. Message: %s. Reason: %s',
                $response->getMessage(),
                $response->getException() ? $response->getException()->getMessage() : ''
            );
        }

        $this->outPut->error($message);
        $this->channelLogger
            ->channel(ChannelLogger::CHANNEL_DUPLICATION_IMAGES)
            ->error($message);
    }

    private function logSuccess(DuplicateImageResponse $response)
    {
        $message = sprintf(
            'Image from %s copied to %s',
            $response->getSourcePath(),
            $response->getDuplicatePath()
        );

        $this->outPut->line($message);
        $this->channelLogger
            ->channel(ChannelLogger::CHANNEL_DUPLICATION_IMAGES)
            ->info($message);
    }
}
