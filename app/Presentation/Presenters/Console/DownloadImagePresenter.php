<?php


namespace App\Presentation\Presenters\Console;

use App\Application\UseCases\Download\Responses\DownloadImageResponse;
use App\Utils\Adapters\Log\ConsoleLogger;
use App\Presentation\Presenters\Contracts\DownloadImagePresenter as Contract;
use Illuminate\Support\Facades\Log;

final class DownloadImagePresenter implements Contract
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     */
    public function __construct(ConsoleLogger $outPut)
    {
        $this->outPut = $outPut;
    }

    /**
     * @param DownloadImageResponse[] $response
     * @return mixed|void
     */
    public function present(array $response)
    {
        foreach ($response as $item) {
            $item->isSuccess()
                ? $this->logSuccess($item)
                : $this->logError($item);
        }
    }

    private function logError(DownloadImageResponse $response)
    {
        $message = sprintf(
            'Downloading of %s images failed. Reason: %s',
            $response->getType(),
            $response->getMessage()
        );
        $this->outPut->error($message);
        Log::channel('download-images')->error($message);
    }

    private function logSuccess(DownloadImageResponse $response)
    {
        $this->outPut->info('Success');
        $message = sprintf(
            'Images for %d %s were downloaded',
            $response->getOwners()->count(),
            $response->getType()
        );
        $this->outPut->line($message);
        Log::channel('download-images')->info($message);
    }
}
