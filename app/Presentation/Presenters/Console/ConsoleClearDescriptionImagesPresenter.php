<?php

namespace App\Presentation\Presenters\Console;

use App\Application\UseCases\Backend\Main\Responses\ClearedDescriptionImages;
use App\Presentation\Presenters\Contracts\Backend\ClearDescriptionImagesPresenter;
use App\Utils\Adapters\Log\ConsoleLogger;
use Illuminate\Support\Facades\Log;

final class ConsoleClearDescriptionImagesPresenter implements ClearDescriptionImagesPresenter
{
    /**
     * @var ConsoleLogger
     */
    private $outPut;

    /**
     * ImportReportPresenter constructor.
     * @param ConsoleLogger $outPut
     */
    public function __construct(ConsoleLogger $outPut)
    {
        $this->outPut = $outPut;
    }

    /**
     * @param ClearedDescriptionImages $response
     * @return mixed|void
     */
    public function present(ClearedDescriptionImages $response)
    {
        $response->isSucceed()
            ? $this->logSuccess($response)
            : $this->logError($response);
    }

    /**
     * @param ClearedDescriptionImages $response
     */
    private function logError(ClearedDescriptionImages $response)
    {
        $message = sprintf('Images deleting was failed. %s', $response->getMessage() ?: '');
        $this->outPut->error($message);
        Log::channel('description-images')->info($message);
    }

    /**
     * @param ClearedDescriptionImages $response
     */
    private function logSuccess(ClearedDescriptionImages $response)
    {
        $message = sprintf('Description images were deleted (%d image(s))', $response->getDeleted());
        $this->outPut->info($message);
        Log::channel('description-images')->info($message);
    }
}
