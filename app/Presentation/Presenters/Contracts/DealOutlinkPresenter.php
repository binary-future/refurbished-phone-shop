<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Deal\Responses\DealOutlinkGoResponse;
use App\Application\UseCases\Deal\Responses\DealOutlinkResponse;

/**
 * Interface DealOutlinkPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface DealOutlinkPresenter
{
    /**
     * @param DealOutlinkResponse $response
     * @return mixed
     */
    public function present(DealOutlinkResponse $response);
}
