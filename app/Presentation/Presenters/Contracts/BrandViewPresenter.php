<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Phones\Responses\BrandViewResponse;

/**
 * Interface BrandViewPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface BrandViewPresenter
{
    /**
     * @param BrandViewResponse $response
     * @return mixed
     */
    public function present(BrandViewResponse $response);
}