<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Phones\Responses\ModelColorViewResponse;

/**
 * Interface ModelColorViewPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface ModelColorViewPresenter
{
    /**
     * @param ModelColorViewResponse $response
     * @return mixed
     */
    public function present(ModelColorViewResponse $response);
}
