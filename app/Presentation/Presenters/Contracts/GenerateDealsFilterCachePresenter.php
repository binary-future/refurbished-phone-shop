<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Deal\Responses\GenerateDealsFiltersCacheResponse;

interface GenerateDealsFilterCachePresenter
{
    /**
     * @param GenerateDealsFiltersCacheResponse[] $response
     * @return mixed|void
     */
    public function present(iterable $response);
}
