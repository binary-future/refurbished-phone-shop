<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Phones\Responses\DuplicateImageResponse;

/**
 * Interface DuplicateImagePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface DuplicateImagePresenter
{
    /**
     * @param DuplicateImageResponse[] $response
     * @return mixed
     */
    public function present(iterable $response);
}
