<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Phones\Responses\BrandListResponse;

/**
 * Interface BrandsListPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface BrandsListPresenter
{
    /**
     * @param BrandListResponse $response
     * @return mixed
     */
    public function present(BrandListResponse $response);
}
