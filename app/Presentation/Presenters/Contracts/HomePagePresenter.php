<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Main\Responses\MainPageResponse;

interface HomePagePresenter
{
    public function present(MainPageResponse $response);
}