<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Phones\Responses\ModelSearchResponse;

/**
 * Interface ModelSearchPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface ModelSearchPresenter
{
    /**
     * @param ModelSearchResponse $response
     * @return mixed
     */
    public function present(ModelSearchResponse $response);
}
