<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Store\Responses\StoreViewResponse;

/**
 * Interface StoreViewPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface StoreViewPresenter
{
    /**
     * @param StoreViewResponse $response
     * @return mixed
     */
    public function present(StoreViewResponse $response);
}
