<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Download\Responses\DownloadImageResponse;

/**
 * Interface DownloadImagePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface DownloadImagePresenter
{
    /**
     * @param DownloadImageResponse[] $response
     * @return mixed
     */
    public function present(array $response);
}