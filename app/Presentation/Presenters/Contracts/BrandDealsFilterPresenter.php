<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Phones\Responses\BrandViewDealsFilterResponse;

/**
 * Interface BrandDealsFilterPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface BrandDealsFilterPresenter
{
    /**
     * @param BrandViewDealsFilterResponse $response
     * @return mixed
     */
    public function present(BrandViewDealsFilterResponse $response);
}