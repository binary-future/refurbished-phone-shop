<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;

/**
 * Interface DeleteOldDealsPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface SoftDeleteOldDealsPresenter
{
    /**
     * @param SoftDealsClearResponse[] $response
     * @return mixed
     */
    public function present(array $response);
}
