<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Store\Responses\StoreMainResponse;

/**
 * Interface StoreMainPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface StoreMainPresenter
{
    /**
     * @param StoreMainResponse $response
     * @return mixed
     */
    public function present(StoreMainResponse $response);
}
