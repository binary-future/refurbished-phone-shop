<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Import\Model\Responses\WithoutDealsResponse;

/**
 * Interface ModelsWithoutDealsPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelsWithoutDealsPresenter
{
    /**
     * @param WithoutDealsResponse $response
     * @return mixed
     */
    public function presenter(WithoutDealsResponse $response);
}