<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Networks\Response\NetworksEditResponse;

/**
 * Interface StoresEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface NetworksEditPresenter
{
    /**
     * @param NetworksEditResponse $response
     * @return mixed
     */
    public function present(NetworksEditResponse $response);
}
