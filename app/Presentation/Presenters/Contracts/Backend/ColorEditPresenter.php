<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorEditResponse;

/**
 * Interface ColorEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ColorEditPresenter
{
    /**
     * @param ColorEditResponse $response
     * @return mixed
     */
    public function present(ColorEditResponse $response);
}