<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Phones\Color\Responses\ColorManageSynonymResponse;

/**
 * Interface ColorEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ColorManageSynonymPresenter
{
    /**
     * @param ColorManageSynonymResponse $response
     * @return mixed
     */
    public function present(ColorManageSynonymResponse $response);
}
