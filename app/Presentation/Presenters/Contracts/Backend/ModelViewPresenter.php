<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelViewResponse;

/**
 * Interface ModelViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelViewPresenter
{
    /**
     * @param ModelViewResponse $response
     * @return mixed
     */
    public function present(ModelViewResponse $response);
}