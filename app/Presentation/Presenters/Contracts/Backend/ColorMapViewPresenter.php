<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Map\Color\Responses\MapViewResponse;

/**
 * Interface ModelMapViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ColorMapViewPresenter
{
    /**
     * @param MapViewResponse $response
     * @return mixed
     */
    public function present(MapViewResponse $response);
}
