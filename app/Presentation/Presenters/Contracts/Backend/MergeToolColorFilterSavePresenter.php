<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ColorFilterSaveResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolColorFilterSavePresenter
{
    /**
     * @param ColorFilterSaveResponse $response
     * @return mixed
     */
    public function present(ColorFilterSaveResponse $response);
}
