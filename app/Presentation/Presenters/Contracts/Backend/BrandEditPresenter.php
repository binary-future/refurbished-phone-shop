<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandEditResponse;

/**
 * Interface BrandEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface BrandEditPresenter
{
    /**
     * @param BrandEditResponse $response
     * @return mixed
     */
    public function present(BrandEditResponse $response);
}
