<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Import\Model\Responses\InactiveListResponse;

/**
 * Interface InactiveModelListPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface InactiveModelListPresenter
{
    /**
     * @param InactiveListResponse $response
     * @return mixed
     */
    public function present(InactiveListResponse $response);
}
