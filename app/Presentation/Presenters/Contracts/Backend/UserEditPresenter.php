<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\User\Response\UserEditResponse;

/**
 * Interface UserEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface UserEditPresenter
{
    /**
     * @param UserEditResponse $response
     * @return mixed
     */
    public function present(UserEditResponse $response);
}