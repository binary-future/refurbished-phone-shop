<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelUpdateResponse;

/**
 * Interface TopModelUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface TopModelUpdatePresenter
{
    /**
     * @param TopModelUpdateResponse $response
     * @return mixed
     */
    public function present(TopModelUpdateResponse $response);
}
