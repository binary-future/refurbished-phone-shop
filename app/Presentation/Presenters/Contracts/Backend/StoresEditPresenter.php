<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Stores\Response\StoreEditResponse;

/**
 * Interface StoresEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface StoresEditPresenter
{
    /**
     * @param StoreEditResponse $response
     * @return mixed
     */
    public function present(StoreEditResponse $response);
}