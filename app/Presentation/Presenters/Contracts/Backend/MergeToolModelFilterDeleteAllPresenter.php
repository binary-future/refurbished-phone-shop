<?php


namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterDeleteAllResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolModelFilterDeleteAllPresenter
{
    /**
     * @param ModelFilterDeleteAllResponse $response
     * @return mixed
     */
    public function present(ModelFilterDeleteAllResponse $response);
}
