<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorMainResponse;

/**
 * Interface ColorMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ColorMainPresenter
{
    /**
     * @param ColorMainResponse $response
     * @return mixed
     */
    public function present(ColorMainResponse $response);
}