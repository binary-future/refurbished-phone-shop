<?php


namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;

/**
 * Interface AllReportsPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface AllReportsPresenter
{
    /**
     * @param AllReportsResponse $response
     * @return mixed
     */
    public function present(AllReportsResponse $response);
}