<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterSaveResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolModelFilterSavePresenter
{
    /**
     * @param ModelFilterSaveResponse $response
     * @return mixed
     */
    public function present(ModelFilterSaveResponse $response);
}
