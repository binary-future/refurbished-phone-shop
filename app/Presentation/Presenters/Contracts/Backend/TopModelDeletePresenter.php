<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelDeleteResponse;

/**
 * Interface TopModelBulkUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface TopModelDeletePresenter
{
    /**
     * @param TopModelDeleteResponse $response
     * @return mixed
     */
    public function present(TopModelDeleteResponse $response);
}
