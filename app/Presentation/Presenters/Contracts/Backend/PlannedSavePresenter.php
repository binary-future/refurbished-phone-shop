<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanSaved;

interface PlannedSavePresenter
{
    public function present(ImportPlanSaved $response);
}