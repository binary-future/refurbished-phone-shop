<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandUpdateResponse;

/**
 * Interface BrandUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface BrandUpdatePresenter
{
    /**
     * @param BrandUpdateResponse $response
     * @return mixed
     */
    public function present(BrandUpdateResponse $response);
}
