<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ColorFilterDeleteResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolColorFilterDeletePresenter
{
    /**
     * @param ColorFilterDeleteResponse $response
     * @return mixed
     */
    public function present(ColorFilterDeleteResponse $response);
}
