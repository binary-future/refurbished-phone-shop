<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\User\Response\UserListResponse;

/**
 * Interface UserListPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface UserListPresenter
{
    /**
     * @param UserListResponse $response
     * @return mixed
     */
    public function present(UserListResponse $response);
}
