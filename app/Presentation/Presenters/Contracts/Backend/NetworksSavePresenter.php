<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Networks\Response\NetworkSaveResponse;

/**
 * Interface StoreUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface NetworksSavePresenter
{
    /**
     * @param NetworkSaveResponse $response
     * @return mixed
     */
    public function present(NetworkSaveResponse $response);
}
