<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Stores\Response\StoreSaveResponse;

/**
 * Interface StoreUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface StoreSavePresenter
{
    /**
     * @param StoreSaveResponse $response
     * @return mixed
     */
    public function present(StoreSaveResponse $response);
}