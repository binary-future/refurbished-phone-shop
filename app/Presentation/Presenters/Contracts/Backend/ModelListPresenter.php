<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelListResponse;

/**
 * Interface ModelListPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelListPresenter
{
    /**
     * @param ModelListResponse $response
     * @return mixed
     */
    public function present(ModelListResponse $response);
}