<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterDeleteResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolModelFilterDeletePresenter
{
    /**
     * @param ModelFilterDeleteResponse $response
     * @return mixed
     */
    public function present(ModelFilterDeleteResponse $response);
}
