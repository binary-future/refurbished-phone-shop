<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneViewResponse;

/**
 * Interface PhoneViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PhoneViewPresenter
{
    /**
     * @param PhoneViewResponse $response
     * @return mixed
     */
    public function present(PhoneViewResponse $response);
}
