<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Stores\Response\StoreMainResponse;

/**
 * Interface StoresMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface StoresMainPresenter
{
    /**
     * @param StoreMainResponse $response
     * @return mixed
     */
    public function present(StoreMainResponse $response);
}
