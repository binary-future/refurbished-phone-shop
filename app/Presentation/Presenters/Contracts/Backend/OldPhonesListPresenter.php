<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\OldPhonesListResponse;

/**
 * Class OldPhonesListPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface OldPhonesListPresenter
{
    /**
     * @param OldPhonesListResponse $response
     */
    public function present(OldPhonesListResponse $response);
}
