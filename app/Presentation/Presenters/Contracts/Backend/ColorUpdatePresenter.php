<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorUpdateResponse;

/**
 * Interface ColorUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ColorUpdatePresenter
{
    /**
     * @param ColorUpdateResponse $response
     * @return mixed
     */
    public function present(ColorUpdateResponse $response);
}
