<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelEditResponse;

/**
 * Interface ModelEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelEditPresenter
{
    /**
     * @param ModelEditResponse $response
     * @return mixed
     */
    public function present(ModelEditResponse $response);
}