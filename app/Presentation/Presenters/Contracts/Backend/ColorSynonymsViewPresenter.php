<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Phones\Color\Responses\ColorSynonymsResponse;

/**
 * Interface ColorEditPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ColorSynonymsViewPresenter
{
    /**
     * @param ColorSynonymsResponse $response
     * @return mixed
     */
    public function present(ColorSynonymsResponse $response);
}
