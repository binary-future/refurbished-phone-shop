<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Import\Planned\Responses\PlannedMainResponse;

/**
 * Interface PlannedImportsMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PlannedImportsMainPresenter
{
    /**
     * @param PlannedMainResponse $response
     * @return mixed
     */
    public function present(PlannedMainResponse $response);
}
