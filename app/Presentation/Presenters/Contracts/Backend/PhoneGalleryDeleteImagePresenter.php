<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryDeleteImageResponse;

/**
 * Interface PhoneGalleryDeleteImagePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PhoneGalleryDeleteImagePresenter
{
    /**
     * @param PhoneGalleryDeleteImageResponse $response
     * @return mixed
     */
    public function present(PhoneGalleryDeleteImageResponse $response);
}