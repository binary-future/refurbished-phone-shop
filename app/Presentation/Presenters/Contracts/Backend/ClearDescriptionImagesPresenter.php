<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Main\Responses\ClearedDescriptionImages;

/**
 * Interface ClearDescriptionImagesPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ClearDescriptionImagesPresenter
{
    /**
     * @param ClearedDescriptionImages $images
     * @return mixed
     */
    public function present(ClearedDescriptionImages $images);
}
