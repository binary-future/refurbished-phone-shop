<?php

namespace App\Presentation\Presenters\Contracts\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\SavePaymentMethodResponse;

interface PaymentMethodSavePresenter
{
    public function present(SavePaymentMethodResponse $response);
}
