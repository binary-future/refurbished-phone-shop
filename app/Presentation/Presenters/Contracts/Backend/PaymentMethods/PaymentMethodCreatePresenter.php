<?php

namespace App\Presentation\Presenters\Contracts\Backend\PaymentMethods;

interface PaymentMethodCreatePresenter
{
    public function present();
}
