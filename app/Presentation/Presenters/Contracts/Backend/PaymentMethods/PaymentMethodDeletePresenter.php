<?php

namespace App\Presentation\Presenters\Contracts\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\DeletePaymentMethodResponse;

interface PaymentMethodDeletePresenter
{
    public function present(DeletePaymentMethodResponse $response);
}
