<?php

namespace App\Presentation\Presenters\Contracts\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodsMainResponse;

interface PaymentMethodsMainPresenter
{
    public function present(PaymentMethodsMainResponse $response);
}
