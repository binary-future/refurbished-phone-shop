<?php

namespace App\Presentation\Presenters\Contracts\Backend\PaymentMethods;

use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodEditResponse;

interface PaymentMethodEditPresenter
{
    public function present(PaymentMethodEditResponse $response);
}
