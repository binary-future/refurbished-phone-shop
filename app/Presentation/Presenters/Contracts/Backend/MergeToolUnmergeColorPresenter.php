<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Color\Responses\UnmergeColorResponse;

/**
 * Interface MergeToolUnmergeModelPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MergeToolUnmergeColorPresenter
{
    /**
     * @param UnmergeColorResponse $response
     * @return mixed
     */
    public function present(UnmergeColorResponse $response);
}
