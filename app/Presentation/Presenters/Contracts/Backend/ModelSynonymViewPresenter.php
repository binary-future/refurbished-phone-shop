<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSynonymViewResponse;

/**
 * Interface ModelSynonymViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelSynonymViewPresenter
{
    /**
     * @param ModelSynonymViewResponse $response
     * @return mixed
     */
    public function present(ModelSynonymViewResponse $response);
}
