<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;

/**
 * Interface PositiveReportsDeletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PositiveReportsDeletePresenter
{
    /**
     * @param PositiveDeleteResponse $response
     * @return mixed
     */
    public function present(PositiveDeleteResponse $response);
}