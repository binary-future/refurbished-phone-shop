<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\ToolViewResponse;

/**
 * Interface MergeToolModelMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MergeToolModelViewPresenter
{
    /**
     * @param ToolViewResponse $response
     * @return mixed
     */
    public function present(ToolViewResponse $response);
}
