<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Networks\Response\NetworkMainResponse;

/**
 * Interface StoresMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface NetworksMainPresenter
{
    /**
     * @param NetworkMainResponse $response
     * @return mixed
     */
    public function present(NetworkMainResponse $response);
}
