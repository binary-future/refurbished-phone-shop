<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Main\Responses\FileUploadResponse;

/**
 * Interface FileUploadPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface FileUploadPresenter
{
    /**
     * @param FileUploadResponse $response
     * @return mixed
     */
    public function present(FileUploadResponse $response);
}