<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\Profile\Responses\ProfileUpdateResponse;

/**
 * Interface ProfileUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ProfileUpdatePresenter
{
    /**
     * @param ProfileUpdateResponse $response
     * @return mixed
     */
    public function present(ProfileUpdateResponse $response);
}