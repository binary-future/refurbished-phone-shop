<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Main\Responses\MainPageResponse;

interface HomePagePresenter
{
    public function present(MainPageResponse $response);
}
