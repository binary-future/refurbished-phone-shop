<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSearchResponse;

/**
 * Interface ModelSearchPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelSearchPresenter
{
    /**
     * @param ModelSearchResponse $response
     * @return mixed
     */
    public function present(ModelSearchResponse $response);
}