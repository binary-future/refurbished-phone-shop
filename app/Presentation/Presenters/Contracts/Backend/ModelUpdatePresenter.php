<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelUpdateResponse;

/**
 * Interface ModelUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelUpdatePresenter
{
    /**
     * @param ModelUpdateResponse $response
     * @return mixed
     */
    public function present(ModelUpdateResponse $response);
}