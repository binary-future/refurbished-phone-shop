<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelMainResponse;

/**
 * Interface ModelMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelMainPresenter
{
    /**
     * @param ModelMainResponse $response
     * @return mixed
     */
    public function present(ModelMainResponse $response);
}