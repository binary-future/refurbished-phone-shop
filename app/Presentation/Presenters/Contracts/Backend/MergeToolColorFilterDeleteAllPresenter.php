<?php


namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ColorFilterDeleteAllResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolColorFilterDeleteAllPresenter
{
    /**
     * @param ColorFilterDeleteAllResponse $response
     * @return mixed
     */
    public function present(ColorFilterDeleteAllResponse $response);
}
