<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelCreateResponse;

/**
 * Interface TopModelBulkUpdatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface TopModelCreatePresenter
{
    /**
     * @param TopModelCreateResponse $response
     * @return mixed
     */
    public function present(TopModelCreateResponse $response);
}
