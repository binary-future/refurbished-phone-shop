<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\MergeSynonymsResponse;

/**
 * Interface MergeToolUnmergeModelPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MergeToolMergeSynonymsPresenter
{
    /**
     * @param MergeSynonymsResponse $response
     * @return mixed
     */
    public function present(MergeSynonymsResponse $response);
}
