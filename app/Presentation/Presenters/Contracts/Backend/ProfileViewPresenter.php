<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\Profile\Responses\ProfileViewResponse;

/**
 * Interface ProfileViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ProfileViewPresenter
{
    /**
     * @param ProfileViewResponse $response
     * @return mixed
     */
    public function present(ProfileViewResponse $response);
}