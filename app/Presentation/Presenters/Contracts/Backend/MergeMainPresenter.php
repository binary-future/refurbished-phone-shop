<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Main\Responses\MergeMainResponse;

interface MergeMainPresenter
{
    public function present(MergeMainResponse $response);
}