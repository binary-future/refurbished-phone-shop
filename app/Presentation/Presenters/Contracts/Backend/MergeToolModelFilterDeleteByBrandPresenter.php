<?php


namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Merge\Tool\Filters\Responses\ModelFilterDeleteByBrandResponse;

/**
 * Interface ToolModelFilterSavePresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface MergeToolModelFilterDeleteByBrandPresenter
{
    /**
     * @param ModelFilterDeleteByBrandResponse $response
     * @return mixed
     */
    public function present(ModelFilterDeleteByBrandResponse $response);
}
