<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryUpdateImageResponse;

/**
 * Interface PhoneGalleryUpdateImagePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PhoneGalleryUpdateImagePresenter
{
    /**
     * @param PhoneGalleryUpdateImageResponse $
     * @return mixed
     */
    public function present(PhoneGalleryUpdateImageResponse $response);
}