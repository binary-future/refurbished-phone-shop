<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Phones\Model\Responses\ModelManageSynonymResponse;

/**
 * Interface ModelManageSynonymPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelManageSynonymPresenter
{
    /**
     * @param ModelManageSynonymResponse $response
     * @return mixed
     */
    public function present(ModelManageSynonymResponse $response);
}
