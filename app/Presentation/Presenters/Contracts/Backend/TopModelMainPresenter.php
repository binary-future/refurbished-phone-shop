<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelMainResponse;

/**
 * Interface TopModelMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface TopModelMainPresenter
{
    /**
     * @param TopModelMainResponse $response
     * @return mixed
     */
    public function present(TopModelMainResponse $response);
}
