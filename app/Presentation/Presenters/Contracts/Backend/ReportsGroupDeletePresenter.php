<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Reports\Responses\ReportsGroupDeleteResponse;

/**
 * Interface ReportsGroupDeletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ReportsGroupDeletePresenter
{
    /**
     * @param ReportsGroupDeleteResponse $response
     * @return mixed
     */
    public function present(ReportsGroupDeleteResponse $response);
}