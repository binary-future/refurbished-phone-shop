<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Map\Model\Responses\MapViewResponse;

/**
 * Interface ModelMapViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelMapViewPresenter
{
    /**
     * @param MapViewResponse $response
     * @return mixed
     */
    public function present(MapViewResponse $response);
}
