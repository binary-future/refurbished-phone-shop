<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Phones\Model\Responses\ModelAutocompleteResponse;

/**
 * Interface ModelAutocompletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface ModelAutocompletePresenter
{
    /**
     * @param ModelAutocompleteResponse $response
     * @return mixed
     */
    public function present(ModelAutocompleteResponse $response);
}