<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Stores\Response\StoreDeleteResponse;

/**
 * Interface StoreDeletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface StoreDeletePresenter
{
    /**
     * @param StoreDeleteResponse $response
     * @return mixed
     */
    public function present(StoreDeleteResponse $response);
}