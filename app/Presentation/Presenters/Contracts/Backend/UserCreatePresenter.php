<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\User\Response\UserCreateResponse;

/**
 * Interface UserCreatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface UserCreatePresenter
{
    /**
     * @param UserCreateResponse $response
     * @return mixed
     */
    public function present(UserCreateResponse $response);
}