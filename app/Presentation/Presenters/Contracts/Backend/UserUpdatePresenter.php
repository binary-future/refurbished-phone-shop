<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\User\Response\UserUpdateResponse;

interface UserUpdatePresenter
{
    public function present(UserUpdateResponse $response);
}