<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Color\Responses\ToolMainResponse;

/**
 * Interface MergeToolModelMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MergeToolColorMainPresenter
{
    /**
     * @param ToolMainResponse $response
     * @return mixed
     */
    public function present(ToolMainResponse $response);
}