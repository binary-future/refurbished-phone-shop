<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Main\Responses\FileUploadEditorResult;

/**
 * Interface FileUploadEditorPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface FileUploadEditorPresenter
{
    /**
     * @param FileUploadEditorResult $result
     * @return mixed
     */
    public function present(FileUploadEditorResult $result);
}