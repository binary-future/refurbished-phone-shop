<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Reports\Responses\NegativeViewResponse;

/**
 * Interface NegativeReportsViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface NegativeReportsViewPresenter
{
    /**
     * @param NegativeViewResponse $response
     * @return mixed
     */
    public function present(NegativeViewResponse $response);
}
