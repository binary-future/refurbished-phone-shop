<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;

/**
 * Interface NegativeReportsDeletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface NegativeReportsDeletePresenter
{
    /**
     * @param NegativeDeleteResponse $response
     * @return mixed
     */
    public function present(NegativeDeleteResponse $response);
}