<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\UnmergeModelResponse;

/**
 * Interface MergeToolUnmergeModelPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MergeToolUnmergeModelPresenter
{
    /**
     * @param UnmergeModelResponse $response
     * @return mixed
     */
    public function present(UnmergeModelResponse $response);
}
