<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Import\Main\Responses\MainImportResponse;

/**
 * Interface MainImportPagePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MainImportPagePresenter
{
    /**
     * @param MainImportResponse $response
     * @return mixed
     */
    public function present(MainImportResponse $response);
}
