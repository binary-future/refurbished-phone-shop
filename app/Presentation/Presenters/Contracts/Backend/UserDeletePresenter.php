<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\User\Response\UserDeleteResponse;

/**
 * Interface UserDeletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface UserDeletePresenter
{
    /**
     * @param UserDeleteResponse $response
     * @return mixed
     */
    public function present(UserDeleteResponse $response);
}