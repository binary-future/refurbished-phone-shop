<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Reports\Responses\PositiveViewResponse;

/**
 * Interface PositiveReportsViewPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PositiveReportsViewPresenter
{
    /**
     * @param PositiveViewResponse $response
     * @return mixed
     */
    public function present(PositiveViewResponse $response);
}