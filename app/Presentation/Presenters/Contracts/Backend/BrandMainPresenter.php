<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandMainResponse;

/**
 * Interface BrandMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface BrandMainPresenter
{
    /**
     * @param BrandMainResponse $response
     * @return mixed
     */
    public function present(BrandMainResponse $response);
}
