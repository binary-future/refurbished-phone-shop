<?php

namespace App\Presentation\Presenters\Contracts\Backend;

use App\Application\UseCases\Backend\User\User\Response\UserSaveResponse;

/**
 * Interface UserSavePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface UserSavePresenter
{
    /**
     * @param UserSaveResponse $response
     * @return mixed
     */
    public function present(UserSaveResponse $response);
}