<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryAddImageResponse;

/**
 * Interface PhoneGalleryAddImagePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PhoneGalleryAddImagePresenter
{
    /**
     * @param PhoneGalleryAddImageResponse $response
     * @return mixed
     */
    public function present(PhoneGalleryAddImageResponse $response);
}