<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Merge\Tool\Models\Responses\ToolMainResponse;

/**
 * Interface MergeToolModelMainPresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface MergeToolModelMainPresenter
{
    /**
     * @param ToolMainResponse $response
     * @return mixed
     */
    public function present(ToolMainResponse $response);
}