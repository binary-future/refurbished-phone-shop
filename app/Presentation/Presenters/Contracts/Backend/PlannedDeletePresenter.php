<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanDeleted;

/**
 * Interface PlannedDeletePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface PlannedDeletePresenter
{
    /**
     * @param ImportPlanDeleted $response
     * @return mixed
     */
    public function present(ImportPlanDeleted $response);
}
