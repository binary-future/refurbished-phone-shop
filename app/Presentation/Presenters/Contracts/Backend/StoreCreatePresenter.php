<?php


namespace App\Presentation\Presenters\Contracts\Backend;


use App\Application\UseCases\Backend\Stores\Response\StoreCreateResponse;

/**
 * Interface StoreCreatePresenter
 * @package App\Presentation\Presenters\Contracts\Backend
 */
interface StoreCreatePresenter
{
    /**
     * @param StoreCreateResponse $response
     * @return mixed
     */
    public function present(StoreCreateResponse $response);
}