<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Deal\Responses\HardDealsClearResponse;

/**
 * Interface HardDeleteOldDealsPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface HardDeleteOldDealsPresenter
{
    /**
     * @param HardDealsClearResponse $response
     * @return mixed
     */
    public function present(HardDealsClearResponse $response);
}
