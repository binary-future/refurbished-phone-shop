<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\Services\Importer\Response\Contracts\ImportResponse;

/**
 * Class ImportReportPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface ImportReportPresenter
{
    /**
     * @param ImportResponse $response
     */
    public function present(ImportResponse $response);
}
