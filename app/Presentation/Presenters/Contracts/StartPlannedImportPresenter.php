<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Import\Responses\PlannedImportResponse;

/**
 * Interface StartPlannedImportPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface StartPlannedImportPresenter
{
    /**
     * @param PlannedImportResponse $response
     * @return mixed
     */
    public function present(PlannedImportResponse $response);
}
