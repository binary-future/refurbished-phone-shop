<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Phones\Responses\ModelDealsFilterResponse;

/**
 * Interface ModelDealsFilterPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface ModelDealsFilterPresenter
{
    public function present(ModelDealsFilterResponse $response);
}
