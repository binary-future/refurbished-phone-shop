<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Deal\Responses\DealOutlinkGoResponse;

/**
 * Interface DealOutlinkGoPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface DealOutlinkGoPresenter
{
    /**
     * @param DealOutlinkGoResponse $response
     * @return mixed
     */
    public function present(DealOutlinkGoResponse $response);
}
