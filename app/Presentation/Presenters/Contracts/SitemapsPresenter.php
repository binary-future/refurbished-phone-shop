<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Main\Responses\SitemapsResponse;

/**
 * Interface SitemapsPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface SitemapsPresenter
{
    /**
     * @param SitemapsResponse $response
     * @return mixed
     */
    public function present(SitemapsResponse $response);
}
