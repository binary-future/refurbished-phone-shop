<?php

namespace App\Presentation\Presenters\Contracts;

use App\Application\UseCases\Phones\Responses\ModelViewResponse;

/**
 * Interface ModelViewPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface ModelViewPresenter
{
    /**
     * @param ModelViewResponse $response
     * @return mixed
     */
    public function present(ModelViewResponse $response);
}
