<?php


namespace App\Presentation\Presenters\Contracts\System;


use App\Application\UseCases\System\Results\DealsFiltersGenerationResult;

/**
 * Interface DealsFiltersGenerationPresenter
 * @package App\Presentation\Presenters\Contracts\System
 */
interface DealsFiltersGenerationPresenter
{
    /**
     * @param DealsFiltersGenerationResult $result
     * @return mixed
     */
    public function present(DealsFiltersGenerationResult $result);
}