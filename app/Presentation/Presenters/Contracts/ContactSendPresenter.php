<?php

namespace App\Presentation\Presenters\Contracts;

/**
 * Interface ContactSendPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface ContactSendPresenter
{
    public function present(array $params);
}
