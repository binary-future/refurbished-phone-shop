<?php


namespace App\Presentation\Presenters\Contracts;


use App\Application\UseCases\Deal\Responses\PhoneDealResponse;

/**
 * Interface PhoneDealPresenter
 * @package App\Presentation\Presenters\Contracts
 */
interface PhoneDealPresenter
{
    /**
     * @param PhoneDealResponse $response
     * @return mixed
     */
    public function present(PhoneDealResponse $response);
}