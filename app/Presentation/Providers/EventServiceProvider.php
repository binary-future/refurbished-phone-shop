<?php

namespace App\Presentation\Providers;

use App\Application\Services\File\Image\PhysicalImageDeleter;
use App\Application\Services\File\Image\ThumbnailsGenerator;
use App\Application\Services\Importer\Events\NegativeReportGeneratedEvent;
use App\Domain\Deals\Deal\Events\DealCreated;
use App\Domain\Deals\Deal\Events\DealDeleted;
use App\Domain\Deals\Deal\Events\DealUpdated;
use App\Domain\Shared\Image\Events\ImageDeleted;
use App\Domain\Shared\Image\Events\ImageSaved;
use App\Presentation\Services\Notifications\Listeners\NegativeReportGenerateListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NegativeReportGeneratedEvent::class => [
            NegativeReportGenerateListener::class
        ],
        DealUpdated::class => [
        ],
        DealDeleted::class => [
        ],
        DealCreated::class => [
        ],
        ImageDeleted::class => [
            PhysicalImageDeleter::class
        ],
        ImageSaved::class => [
            ThumbnailsGenerator::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
