<?php

namespace App\Presentation\Providers;

use App\Domain\Deals\Deal\Services\Link\Contracts\MassiveDealsDelete;
use App\Domain\Deals\Deal\Services\Link\MassiveDealsDelete as MassiveDealsDeleteImpl;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(MassiveDealsDelete::class, MassiveDealsDeleteImpl::class);
    }
}
