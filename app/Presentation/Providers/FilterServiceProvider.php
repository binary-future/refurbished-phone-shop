<?php

namespace App\Presentation\Providers;

use App\Application\Services\Search\Filters\Contracts\FilterInitializer;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\DealFiltersSearcherCached;
use App\Application\Services\Search\Filters\Defaults\PrimaryFilterInitializer
    as DefaultFilterValuesInitializerImpl;
use App\Application\Services\Search\Filters\FiltersSearcherByDeals;
use Illuminate\Support\ServiceProvider;

class FilterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            FilterInitializer::class,
            DefaultFilterValuesInitializerImpl::class
        );

        $this->app->when(DealFiltersSearcherCached::class)
            ->needs(FiltersSearcher::class)
            ->give(FiltersSearcherByDeals::class);
    }
}
