<?php

namespace App\Presentation\Providers\Frontend;

use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\DealFiltersSearcherCached;
use App\Application\Services\Search\Translators\ModelNameTranslator;
use App\Application\UseCases\Deal\ClearApiDealsCase as ClearApiDealsCaseImpl;
use App\Application\UseCases\Deal\Contracts\ClearApiDealsCase;
use App\Application\UseCases\Deal\Contracts\DealOutlinkCase;
use App\Application\UseCases\Deal\Contracts\DealOutlinkGoCase;
use App\Application\UseCases\Deal\Contracts\GenerateDealsFiltersCacheCase;
use App\Application\UseCases\Deal\Contracts\HardDeleteOldDealsCase;
use App\Application\UseCases\Deal\Contracts\PhoneDealCase;
use App\Application\UseCases\Deal\Contracts\SoftDeleteOldDealsCase;
use App\Application\UseCases\Deal\DealOutlinkCase as DealOutlinkCaseImpl;
use App\Application\UseCases\Deal\DealOutlinkGoCase as DealOutlinkGoCaseImpl;
use App\Application\UseCases\Deal\GenerateDealsFiltersCacheCase as GenerateDealsFiltersCacheCaseImpl;
use App\Application\UseCases\Deal\HardDeleteOldDealsCase as HardDeleteOldDealsCaseImpl;
use App\Application\UseCases\Deal\PhoneDealCase as PhoneDealCaseImpl;
use App\Application\UseCases\Deal\SoftDeleteOldDealsCase as DeleteOldDealsCaseImpl;
use App\Application\UseCases\Download\Contracts\DownloadImageCase;
use App\Application\UseCases\Download\DownloadImageCase as DownloadImageCaseImpl;
use App\Application\UseCases\Download\PhonesDownloadImageCase;
use App\Application\UseCases\Download\StoresDownloadImageCase;
use App\Application\UseCases\Import\Contracts\DealsImportCase;
use App\Application\UseCases\Import\Contracts\StoresRatingCase;
use App\Application\UseCases\Import\Contracts\PhonesImportCase;
use App\Application\UseCases\Import\Contracts\SpecsSeedCase;
use App\Application\UseCases\Import\Contracts\StartPlannedImportsCase;
use App\Application\UseCases\Import\Contracts\StoreImportCase;
use App\Application\UseCases\Import\DealsImportCase as DealsImportCaseImpl;
use App\Application\UseCases\Import\StoresRatingCase as ModelsRatingCaseImpl;
use App\Application\UseCases\Import\PhonesImportCase as PhonesImportCaseImpl;
use App\Application\UseCases\Import\SpecsSeedCase as SpecsSeedCaseImpl;
use App\Application\UseCases\Import\StartPlannedImportsCase as StartPlannedImportCaseImpl;
use App\Application\UseCases\Import\StoreImportCase as StoreImportCaseImpl;
use App\Application\UseCases\Main\Contracts\MainCase;
use App\Application\UseCases\Main\Contracts\SitemapsCase;
use App\Application\UseCases\Main\MainCase as MainCaseImpl;
use App\Application\UseCases\Main\SitemapsCase as SitemapsCaseImpl;
use App\Application\UseCases\Phones\BrandsListCase as BrandsListCaseImpl;
use App\Application\UseCases\Phones\BrandViewCase as BrandViewCaseImpl;
use App\Application\UseCases\Phones\BrandViewDealsFilterCase as BrandViewDealsFilterCaseImpl;
use App\Application\UseCases\Phones\Contracts\BrandsListCase;
use App\Application\UseCases\Phones\Contracts\BrandViewCase;
use App\Application\UseCases\Phones\Contracts\BrandViewDealsFilterCase;
use App\Application\UseCases\Phones\Contracts\DuplicateImageCase;
use App\Application\UseCases\Phones\Contracts\ModelColorViewCase;
use App\Application\UseCases\Phones\Contracts\ModelDealsFilterCase;
use App\Application\UseCases\Phones\Contracts\ModelSearchCase;
use App\Application\UseCases\Phones\Contracts\ModelViewCase;
use App\Application\UseCases\Phones\DuplicateImageCase as DuplicateImageCaseImpl;
use App\Application\UseCases\Phones\ModelColorViewCase as ModelColorViewCaseImpl;
use App\Application\UseCases\Phones\ModelDealsFilterCase as ModelDealsFilterCaseImpl;
use App\Application\UseCases\Phones\ModelSearchCase as ModelSearchCaseImpl;
use App\Application\UseCases\Phones\ModelViewCase as ModelViewCaseImpl;
use App\Application\UseCases\Post\Contracts\LastPostsCase;
use App\Application\UseCases\Post\Contracts\LastPostsExceptTagsCase;
use App\Application\UseCases\Post\Contracts\TagPostsCase;
use App\Application\UseCases\Post\LastPostsCase as LastPostCaseImpl;
use App\Application\UseCases\Post\LastPostsExceptTagsCase as LastPostsExceptTagsCaseImpl;
use App\Application\UseCases\Post\TagPostsCase as TagPostsCaseImpl;
use App\Application\UseCases\Store\Contracts\StoreMainCase;
use App\Application\UseCases\Store\Contracts\StoreViewCase;
use App\Application\UseCases\Store\StoreMainCase as StoreMainCaseImpl;
use App\Application\UseCases\Store\StoreViewCase as StoreViewCaseImpl;
use App\Domain\Common\Contracts\Translators\Translator;
use Illuminate\Support\ServiceProvider;

class UseCaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* MAIN */
        $this->app->bind(MainCase::class, MainCaseImpl::class);

        /* IMPORT */
        $this->app->bind(StoreImportCase::class, StoreImportCaseImpl::class);
        $this->app->bind(PhonesImportCase::class, PhonesImportCaseImpl::class);
        $this->app->bind(DealsImportCase::class, DealsImportCaseImpl::class);
        $this->app->bind(SpecsSeedCase::class, SpecsSeedCaseImpl::class);
        $this->app->bind(StoresRatingCase::class, ModelsRatingCaseImpl::class);
        $this->app->bind(StartPlannedImportsCase::class, StartPlannedImportCaseImpl::class);

        /* BLOG */
        $this->app->bind(LastPostsCase::class, LastPostCaseImpl::class);
        $this->app->bind(TagPostsCase::class, TagPostsCaseImpl::class);
        $this->app->bind(LastPostsExceptTagsCase::class, LastPostsExceptTagsCaseImpl::class);

        /* IMAGES */
        $this->app->bind(DownloadImageCase::class, DownloadImageCaseImpl::class);
        $this->app->bind(DownloadImageCaseImpl::class, function () {
            return new DownloadImageCaseImpl(
                $this->app->make(StoresDownloadImageCase::class),
                $this->app->make(PhonesDownloadImageCase::class)
            );
        });
        $this->app->bind(DuplicateImageCase::class, DuplicateImageCaseImpl::class);

        /* PHONES */
        $this->app->bind(BrandsListCase::class, BrandsListCaseImpl::class);
        $this->app->bind(BrandViewCase::class, BrandViewCaseImpl::class);
        $this->app->when(BrandViewCaseImpl::class)
            ->needs(FiltersSearcher::class)
            ->give(DealFiltersSearcherCached::class);
        $this->app->bind(BrandViewDealsFilterCase::class, BrandViewDealsFilterCaseImpl::class);
        $this->app->when(BrandViewDealsFilterCaseImpl::class)
            ->needs(FiltersSearcher::class)
            ->give(DealFiltersSearcherCached::class);
        $this->app->bind(ModelViewCase::class, ModelViewCaseImpl::class);
        $this->app->bind(ModelColorViewCase::class, ModelColorViewCaseImpl::class);
        $this->app->bind(ModelDealsFilterCase::class, ModelDealsFilterCaseImpl::class);
        $this->app->bind(ModelSearchCase::class, ModelSearchCaseImpl::class);
        $this->app->when(ModelSearchCaseImpl::class)
            ->needs(Translator::class)
            ->give(ModelNameTranslator::class);

        /* DEALS */
        $this->app->bind(PhoneDealCase::class, PhoneDealCaseImpl::class);
        $this->app->bind(DealOutlinkCase::class, DealOutlinkCaseImpl::class);
        $this->app->bind(DealOutlinkGoCase::class, DealOutlinkGoCaseImpl::class);
        $this->app->bind(SoftDeleteOldDealsCase::class, DeleteOldDealsCaseImpl::class);
        $this->app->bind(HardDeleteOldDealsCase::class, HardDeleteOldDealsCaseImpl::class);
        $this->app->bind(GenerateDealsFiltersCacheCase::class, GenerateDealsFiltersCacheCaseImpl::class);
        $this->app->bind(ClearApiDealsCase::class, ClearApiDealsCaseImpl::class);

        /* STORES */
        $this->app->bind(StoreViewCase::class, StoreViewCaseImpl::class);
        $this->app->bind(StoreMainCase::class, StoreMainCaseImpl::class);

        $this->app->bind(SitemapsCase::class, SitemapsCaseImpl::class);
    }
}
