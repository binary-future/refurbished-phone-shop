<?php


namespace App\Presentation\Providers\Frontend;

use App\Presentation\Composers\LastPostsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function boot()
    {
        View::composer(
            'frontend.components.footer.posts',
            LastPostsComposer::class
        );
    }
}