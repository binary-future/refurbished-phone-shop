<?php

namespace App\Presentation\Providers\Frontend;

use App\Presentation\Http\Controllers\Frontend\Brand\BrandViewFilter;
use App\Presentation\Http\Controllers\Frontend\Deal\PhoneDealPopup;
use App\Presentation\Http\Controllers\Frontend\Model\ModelDealsFilter;
use App\Presentation\Http\Controllers\Frontend\Search\AutocompleteModel;
use App\Presentation\Presenters\Contracts\ModelSearchPresenter;
use App\Presentation\Presenters\Contracts\PhoneDealPresenter;
use App\Presentation\Presenters\Web\Frontend\Deals\WebPhoneDealPopupPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebModelAutocompletePresenter;
use App\Presentation\Services\Request\Preprocessors\Contracts\BrandDealFilterPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Contracts\ModelDealFilterPreprocessor;
use App\Presentation\Services\Request\Preprocessors\Contracts\CommaSeparatedParamsPreprocessor;
use Illuminate\Support\ServiceProvider;

class ControllerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(ModelDealsFilter::class)
            ->needs(ModelDealFilterPreprocessor::class)
            ->give(\App\Presentation\Services\Request\Preprocessors\ModelDealFilterPreprocessor::class);

        $this->app->when(BrandViewFilter::class)
            ->needs(BrandDealFilterPreprocessor::class)
            ->give(\App\Presentation\Services\Request\Preprocessors\BrandDealFilterPreprocessor::class);

        $this->app->when(AutocompleteModel::class)
            ->needs(ModelSearchPresenter::class)
            ->give(WebModelAutocompletePresenter::class);

        $this->app->bind(
            CommaSeparatedParamsPreprocessor::class,
            \App\Presentation\Services\Request\Preprocessors\CommaSeparatedParamsPreprocessor::class
        );

        $this->app->when(PhoneDealPopup::class)
            ->needs(PhoneDealPresenter::class)
            ->give(WebPhoneDealPopupPresenter::class);
    }
}
