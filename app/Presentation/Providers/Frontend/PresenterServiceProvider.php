<?php

namespace App\Presentation\Providers\Frontend;

use App\Presentation\Http\Controllers\Frontend\Brand\BrandIndex;
use App\Presentation\Http\Controllers\Frontend\Brand\BrandView;
use App\Presentation\Http\Controllers\Frontend\Model\ModelView;
use App\Presentation\Presenters\Console\ConsoleStartPlannedImportPresenter;
use App\Presentation\Presenters\Console\DuplicateImagePresenter as DuplicateImagePresenterImpl;
use App\Presentation\Presenters\Console\GenerateDealsFilterCachePresenter as GenerateDealsFilterCachePresenterImpl;
use App\Presentation\Presenters\Console\HardDeleteOldDealsPresenter as HardDeleteOldDealsPresenterImpl;
use App\Presentation\Presenters\Contracts\BrandDealsFilterPresenter;
use App\Presentation\Presenters\Contracts\BrandsListPresenter;
use App\Presentation\Presenters\Contracts\BrandViewPresenter;
use App\Presentation\Presenters\Contracts\ContactSendPresenter;
use App\Presentation\Presenters\Contracts\DealOutlinkGoPresenter;
use App\Presentation\Presenters\Contracts\DealOutlinkPresenter;
use App\Presentation\Presenters\Contracts\DuplicateImagePresenter;
use App\Presentation\Presenters\Contracts\GenerateDealsFilterCachePresenter;
use App\Presentation\Presenters\Contracts\HardDeleteOldDealsPresenter;
use App\Presentation\Presenters\Contracts\SoftDeleteOldDealsPresenter;
use App\Presentation\Presenters\Contracts\DownloadImagePresenter;
use App\Presentation\Presenters\Contracts\HomePagePresenter;
use App\Presentation\Presenters\Contracts\ImportReportPresenter;
use App\Presentation\Presenters\Console\ImportReportPresenter as ImportReportPresenterImpl;
use App\Presentation\Presenters\Contracts\ModelColorViewPresenter;
use App\Presentation\Presenters\Contracts\ModelDealsFilterPresenter;
use App\Presentation\Presenters\Contracts\ModelSearchPresenter;
use App\Presentation\Presenters\Contracts\ModelViewPresenter;
use App\Presentation\Presenters\Contracts\PhoneDealPresenter;
use App\Presentation\Presenters\Contracts\SitemapsPresenter;
use App\Presentation\Presenters\Contracts\StartPlannedImportPresenter;
use App\Presentation\Presenters\Contracts\StoreMainPresenter;
use App\Presentation\Presenters\Contracts\StoreViewPresenter;
use App\Presentation\Presenters\Web\Frontend\Deals\WebDealOutlinkGoPresenter;
use App\Presentation\Presenters\Web\Frontend\Deals\WebDealOutlinkPresenter;
use App\Presentation\Presenters\Web\Frontend\Deals\WebPhoneDealPresenter;
use App\Presentation\Presenters\Web\Frontend\General\WebContactSendPresenter;
use App\Presentation\Presenters\Web\Frontend\Main\WebHomePagePresenter;
use App\Presentation\Presenters\Web\Frontend\Main\WebSitemapsPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebBrandDealsFilterPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebBrandListPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebBrandViewPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebModelAutocompletePresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebModelColorViewPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebModelDealsFilterPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebModelSearchPresenter;
use App\Presentation\Presenters\Web\Frontend\Phones\WebModelViewPresenter;
use App\Presentation\Presenters\Web\Frontend\Store\WebStoreMainPresenter;
use App\Presentation\Presenters\Web\Frontend\Store\WebStoreViewPresenter;
use App\Presentation\Services\Translators\Autocomplete\ModelsTranslator;
use App\Presentation\Services\Translators\Contracts\Translator;
use Illuminate\Support\ServiceProvider;
use App\Presentation\Presenters\Console\DownloadImagePresenter as DownloadImagePresenterImpl;
use App\Presentation\Presenters\Console\SoftDeleteOldDealsPresenter as DeleteOldDealsPresenterImpl;

class PresenterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* MAIN */
        $this->app->bind(HomePagePresenter::class, WebHomePagePresenter::class);
        $this->app->bind(SitemapsPresenter::class, WebSitemapsPresenter::class);
        $this->app->bind(ContactSendPresenter::class, WebContactSendPresenter::class);

        /* IMPORT */
        $this->app->bind(ImportReportPresenter::class, ImportReportPresenterImpl::class);
        $this->app->bind(StartPlannedImportPresenter::class, ConsoleStartPlannedImportPresenter::class);

        /* Images */
        $this->app->bind(DownloadImagePresenter::class, DownloadImagePresenterImpl::class);
        $this->app->bind(DuplicateImagePresenter::class, DuplicateImagePresenterImpl::class);

        /* PHONES */
        $this->app->when(BrandIndex::class)
            ->needs(BrandsListPresenter::class)
            ->give(WebBrandListPresenter::class);
        $this->app->when(BrandView::class)
            ->needs(BrandViewPresenter::class)
            ->give(WebBrandViewPresenter::class);
        $this->app->when(ModelView::class)
            ->needs(ModelViewPresenter::class)
            ->give(WebModelViewPresenter::class);
        $this->app->bind(BrandDealsFilterPresenter::class, WebBrandDealsFilterPresenter::class);
        $this->app->bind(ModelDealsFilterPresenter::class, WebModelDealsFilterPresenter::class);
        $this->app->bind(ModelSearchPresenter::class, WebModelSearchPresenter::class);

        $this->app->when(WebModelAutocompletePresenter::class)
            ->needs(Translator::class)
            ->give(ModelsTranslator::class);

        $this->app->bind(ModelColorViewPresenter::class, WebModelColorViewPresenter::class);

        /* DEALS */
        $this->app->bind(PhoneDealPresenter::class, WebPhoneDealPresenter::class);
        $this->app->bind(DealOutlinkGoPresenter::class, WebDealOutlinkGoPresenter::class);
        $this->app->bind(DealOutlinkPresenter::class, WebDealOutlinkPresenter::class);
        $this->app->bind(SoftDeleteOldDealsPresenter::class, DeleteOldDealsPresenterImpl::class);
        $this->app->bind(HardDeleteOldDealsPresenter::class, HardDeleteOldDealsPresenterImpl::class);
        $this->app->bind(GenerateDealsFilterCachePresenter::class, GenerateDealsFilterCachePresenterImpl::class);

        /* STORE */
        $this->app->bind(StoreMainPresenter::class, WebStoreMainPresenter::class);
        $this->app->bind(StoreViewPresenter::class, WebStoreViewPresenter::class);
    }
}
