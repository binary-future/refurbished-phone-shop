<?php

namespace App\Presentation\Providers;

use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Observer\FilterObserver;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Observers\DealObserver;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Description\Observer\DescriptionObserver;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Image\Observer\ImageObserver;
use Illuminate\Support\ServiceProvider;
use App\Application\Services\Merge\Color\Filter as ColorFilter;
use App\Application\Services\Merge\Color\Observer\FilterObserver as ColorFilterObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Image::observe(ImageObserver::class);
        Description::observe(DescriptionObserver::class);
        Deal::observe(DealObserver::class);
        Filter::observe(FilterObserver::class);
        ColorFilter::observe(ColorFilterObserver::class);
    }
}
