<?php

namespace App\Presentation\Providers\System;


use App\Presentation\Presenters\Console\ConsoleDealsFiltersGenerationPresenter;
use App\Presentation\Presenters\Contracts\System\DealsFiltersGenerationPresenter;
use Illuminate\Support\ServiceProvider;

class PresenterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(DealsFiltersGenerationPresenter::class, ConsoleDealsFiltersGenerationPresenter::class);
    }
}
