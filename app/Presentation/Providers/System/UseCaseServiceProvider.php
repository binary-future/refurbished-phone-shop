<?php

namespace App\Presentation\Providers\System;

use App\Application\UseCases\System\Contracts\DealsFiltersGenerationCase;
use App\Application\UseCases\System\DealsFiltersGenerationCase as DealsFiltersGenerationCaseImpl;
use Illuminate\Support\ServiceProvider;

class UseCaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(DealsFiltersGenerationCase::class, DealsFiltersGenerationCaseImpl::class);
    }
}
