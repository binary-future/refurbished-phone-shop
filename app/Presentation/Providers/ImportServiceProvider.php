<?php

namespace App\Presentation\Providers;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\HandlerFactory;
use App\Application\Services\Importer\Importer\ContractImporter;
use App\Application\Services\Importer\Importer\Contracts\Importer;
use App\Application\Services\Importer\Importer\DealBrandImporter;
use App\Application\Services\Importer\Importer\DealImporter;
use App\Application\Services\Importer\Importer\NetworkImporter;
use App\Application\Services\Importer\Importer\PhoneBrandImporter;
use App\Application\Services\Importer\Importer\PhoneColorImporter;
use App\Application\Services\Importer\Importer\PhoneModelImporter;
use App\Application\Services\Importer\Importer\PhonesImporter;
use App\Application\Services\Importer\Importer\ProductImporter;
use App\Application\Services\Importer\Importer\Proxies\Logging\MemoryTimeLoggingImporter;
use App\Application\Services\Importer\Importer\Proxies\Transactions\TransactionImporter;
use App\Utils\Adapters\Log\Contracts\Logger;
use App\Utils\Adapters\Transaction\Contracts\Transaction;
use App\Utils\Importer\Handlers\Factory;
use App\Utils\Importer\Source\Providers\Revglue\Request\Contracts\RequestLinkGenerator;
use App\Utils\Importer\Source\Providers\Revglue\Request\RequestGenerator;
use App\Utils\Importer\Source\Providers\Revglue\Response\Contracts\RevglueResponseHandler;
use App\Utils\Importer\Source\Providers\Revglue\Response\ResponseHandler;
use Illuminate\Support\ServiceProvider;

class ImportServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(HandlerFactory::class, Factory::class);

        /* Revglue api source */
        $this->app->bind(RequestLinkGenerator::class, RequestGenerator::class);
        $this->app->bind(RevglueResponseHandler::class, ResponseHandler::class);

        /* Importers */
        $this->app->when(PhoneModelImporter::class)
            ->needs(Importer::class)
            ->give(PhoneBrandImporter::class);

        $this->app->when(ContractImporter::class)
            ->needs(Importer::class)
            ->give(NetworkImporter::class);

        $this->app->bind(PhonesImporter::class, function () {
            return new PhonesImporter(
                $this->app->make(QueryBus::class),
                $this->app->make(CommandBus::class),
                $this->app->make(Logger::class),
                $this->app->make(PhoneColorImporter::class),
                $this->app->make(PhoneModelImporter::class)
            );
        });

        $this->app->bind(DealImporter::class, function () {
            $dealImporter = new DealImporter(
                $this->app->make(QueryBus::class),
                $this->app->make(CommandBus::class),
                $this->app->make(DealBrandImporter::class),
                $this->app->make(ProductImporter::class),
                $this->app->make(ContractImporter::class)
            );

            $transactionImporter = new TransactionImporter(
                $this->app->make(Transaction::class),
                $dealImporter
            );

            return new MemoryTimeLoggingImporter(
                $this->app->make(Logger::class),
                $transactionImporter
            );
        });
    }
}
