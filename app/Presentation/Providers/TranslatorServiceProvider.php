<?php

namespace App\Presentation\Providers;

use App\Presentation\Services\Filters\Generators\Items\MonthlyCostRadioGenerator;
use App\Presentation\Services\Filters\Generators\Items\MonthlyCostSliderGenerator;
use App\Presentation\Services\Filters\Generators\Items\TotalCostRadioGenerator;
use App\Presentation\Services\Filters\Generators\Items\TotalCostSliderGenerator;
use App\Presentation\Services\Filters\Generators\Items\UpfrontCostRadioGenerator;
use App\Presentation\Services\Filters\Generators\Items\UpfrontCostSliderGenerator;
use App\Presentation\Services\Filters\Generators\ModelViewLeftSideBarGenerator;
use App\Presentation\Services\Translators\Contracts\Translator;
use App\Presentation\Services\Translators\Filters\CostFiltersTranslator;
use App\Presentation\Services\Translators\Filters\MonthlyCostSliderTranslator;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Template\Filters\ItemFactory;
use Illuminate\Support\ServiceProvider;

class TranslatorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(MonthlyCostRadioGenerator::class)
            ->needs(Translator::class)
            ->give(CostFiltersTranslator::class);

        $this->app->when(MonthlyCostSliderGenerator::class)
            ->needs(Translator::class)
            ->give(MonthlyCostSliderTranslator::class);

        $this->app->when(UpfrontCostRadioGenerator::class)
            ->needs(Translator::class)
            ->give(CostFiltersTranslator::class);

        $this->app->when(UpfrontCostSliderGenerator::class)
            ->needs(Translator::class)
            ->give(CostFiltersTranslator::class);

        $this->app->when(TotalCostRadioGenerator::class)
            ->needs(Translator::class)
            ->give(CostFiltersTranslator::class);

        $this->app->when(TotalCostSliderGenerator::class)
            ->needs(Translator::class)
            ->give(CostFiltersTranslator::class);

        $this->app->bind(ModelViewLeftSideBarGenerator::class, function ($app) {
            return new ModelViewLeftSideBarGenerator(
                $this->app->make(Serializer::class),
                $this->app->make(ItemFactory::class)
            );
        });
    }
}
