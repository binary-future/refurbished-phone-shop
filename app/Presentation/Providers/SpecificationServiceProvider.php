<?php

namespace App\Presentation\Providers;

use App\Application\Services\Importer\Specifications\Awin\AwinDealsImportSpecification;
use App\Application\Services\Importer\Specifications\Awin\AwinPhonesImportSpecification;
use App\Application\Services\Importer\Specifications\BrandsImagesImportSpecification;
use App\Application\Services\Importer\Specifications\StoresRatingSeedSpecification;
use App\Application\Services\Importer\Specifications\NetworksImagesImportSpecification;
use App\Application\Services\Importer\Specifications\PhonesImagesImportSpecification;
use App\Application\Services\Importer\Specifications\PhonesImportSpecification;
use App\Application\Services\Importer\Specifications\RevglueDealsImportSpecification;
use App\Application\Services\Importer\Specifications\SpecsSeedSpecification;
use App\Application\Services\Importer\Specifications\StoreImportSpecification;
use App\Application\Services\Importer\Specifications\WebgainsDealsImportSpecification;
use App\Application\Services\Merge\Color\Specifications\UniqueFilterSpecification as UniqueColorFilterSpecification;
use App\Application\Services\Merge\Model\Specifications\UniqueFilterSpecification;
use App\Domain\Shared\Image\Specifications\ImageSpecification;
use App\Domain\Shared\Link\Specifications\LinkSpecification;
use App\Presentation\Http\Rules\UniqueMergeColorFilterRule;
use App\Presentation\Http\Rules\UniqueMergeModelFilterRule;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Specification\Specifications\RequiredFieldsSpecification;
use App\Utils\Specification\Specifications\UrlSpecification;
use Illuminate\Support\ServiceProvider;

class SpecificationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(ImageSpecification::class)
            ->needs(Specification::class)
            ->give(UrlSpecification::class);

        $this->app->when(StoreImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(PhonesImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(RevglueDealsImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(AwinDealsImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(WebgainsDealsImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(SpecsSeedSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(PhonesImagesImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(BrandsImagesImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(NetworksImagesImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(LinkSpecification::class)
            ->needs(Specification::class)
            ->give(UrlSpecification::class);

        $this->app->when(StoresRatingSeedSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(AwinPhonesImportSpecification::class)
            ->needs(Specification::class)
            ->give(RequiredFieldsSpecification::class);

        $this->app->when(UniqueMergeModelFilterRule::class)
            ->needs(Specification::class)
            ->give(UniqueFilterSpecification::class);

        $this->app->when(UniqueMergeColorFilterRule::class)
            ->needs(Specification::class)
            ->give(UniqueColorFilterSpecification::class);
    }
}
