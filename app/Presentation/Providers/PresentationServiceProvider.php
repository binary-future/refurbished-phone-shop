<?php

namespace App\Presentation\Providers;

use App\Presentation\Services\Request\Preprocessors\Contracts\RangedParamsPreprocessor;
use App\Presentation\Services\Request\Preprocessors\RangedParamsPreprocessor as RangedParamsPreprocessorImpl;
use App\Presentation\ViewModels\Deals\DealConditionViewModel;
use Illuminate\Support\ServiceProvider;
use Spatie\BladeX\Facades\BladeX;

class PresentationServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     * @throws \Spatie\BladeX\Exceptions\CouldNotRegisterComponent
     */
    public function boot()
    {
        $this->app->bind(RangedParamsPreprocessor::class, RangedParamsPreprocessorImpl::class);

        BladeX::components(['frontend.deals.components.deals.bladex.*']);
        BladeX::component('frontend.deals.components.deals.bladex.deal-condition')
            ->viewModel(DealConditionViewModel::class);
    }
}
