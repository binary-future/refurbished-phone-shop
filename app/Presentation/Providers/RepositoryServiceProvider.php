<?php

namespace App\Presentation\Providers;

use App\Application\Services\Importer\Datafeeds\DatafeedApi;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsAPIs;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\Services\Merge\Color\Filter as ColorFilter;
use App\Application\Services\Merge\Color\Repository\Filters as ColorMergeFilters;
use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters as ModelMergeFilters;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Contract\Repository\Contracts;
use App\Domain\Deals\Contract\Repository\Networks;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Repository\Deals;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Repository\Brands;
use App\Domain\Phone\Faq\Faq;
use App\Domain\Phone\Faq\Repository\Faqs;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Repository\Models;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Repository\Colors;
use App\Domain\Phone\Phone\Repository\Phones;
use App\Domain\Shared\Description\Repository\DescriptionImageRepository;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Link\Repository\Links;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Repository\Stores;
use App\Domain\Store\Store;
use App\Domain\User\Repository\Roles;
use App\Domain\User\Repository\Users;
use App\Domain\User\Role;
use App\Domain\User\User;
use App\Utils\Repository\Conductor\Contracts\MethodGenerator;
use App\Utils\Repository\Conductor\Contracts\WhereConductor;
use App\Utils\Repository\Conductor\Generators\MethodGenerator as MethodGeneratorImpl;
use App\Utils\Repository\Conductor\WhereConductor as WhereConductorImpl;
use App\Utils\Repository\Contracts\CriteriaFactory;
use App\Utils\Repository\Criteria\BrandCriteriaFactory;
use App\Utils\Repository\Criteria\ColorCriteriaFactory;
use App\Utils\Repository\Criteria\ContractCriteriaFactory;
use App\Utils\Repository\Criteria\DatafeedsApiCriteriaFactory;
use App\Utils\Repository\Criteria\DatafeedsInfoCriteriaFactory;
use App\Utils\Repository\Criteria\DealCriteriaFactory;
use App\Utils\Repository\Criteria\FaqCriteriaFactory;
use App\Utils\Repository\Criteria\LinkCriteriaFactory;
use App\Utils\Repository\Criteria\MergeColorFilterCriteriaFactory;
use App\Utils\Repository\Criteria\MergeModelFilterCriteriaFactory;
use App\Utils\Repository\Criteria\NegativeReportCriteriaFactory;
use App\Utils\Repository\Criteria\NetworkCriteriaFactory;
use App\Utils\Repository\Criteria\PaymentMethodCriteriaFactory;
use App\Utils\Repository\Criteria\PhoneCriteriaFactory;
use App\Utils\Repository\Criteria\PhoneLatestModelCriteriaFactory;
use App\Utils\Repository\Criteria\PhoneModelCriteriaFactory;
use App\Utils\Repository\Criteria\PhoneTopModelCriteriaFactory;
use App\Utils\Repository\Criteria\PlannedImportsCriteriaFactory;
use App\Utils\Repository\Criteria\PositiveReportCriteriaFactory;
use App\Utils\Repository\Criteria\RoleCriteriaFactory;
use App\Utils\Repository\Criteria\StoreCriteriaFactory;
use App\Utils\Repository\Criteria\UserCriteriaFactory;
use App\Utils\Repository\Repositories\BrandRepository;
use App\Utils\Repository\Repositories\ContractRepository;
use App\Utils\Repository\Repositories\DatafeedsApiRepository;
use App\Utils\Repository\Repositories\DatafeedsInfoRepository;
use App\Utils\Repository\Repositories\DealRepository;
use App\Utils\Repository\Repositories\DescriptionImageRepository as DescriptionImageRepositoryImpl;
use App\Utils\Repository\Repositories\FaqsRepository;
use App\Utils\Repository\Repositories\LatestModelsRepository;
use App\Utils\Repository\Repositories\LinkRepository;
use App\Utils\Repository\Repositories\MergeColorFiltersRepository;
use App\Utils\Repository\Repositories\MergeModelFiltersRepository;
use App\Utils\Repository\Repositories\NegativeReportRepository;
use App\Utils\Repository\Repositories\NetworkRepository;
use App\Utils\Repository\Repositories\PaymentMethodRepository;
use App\Utils\Repository\Repositories\PhoneColorRepository;
use App\Utils\Repository\Repositories\PhoneItemRepository;
use App\Utils\Repository\Repositories\PhoneModelsRepository;
use App\Utils\Repository\Repositories\PlannedImportsRepository;
use App\Utils\Repository\Repositories\PositiveReportRepository;
use App\Utils\Repository\Repositories\RoleRepository;
use App\Utils\Repository\Repositories\StoreRepository;
use App\Utils\Repository\Repositories\TopModelsRepository;
use App\Utils\Repository\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        //
    }

    public function boot(): void
    {
        /* FAQS */
        $this->app->bind(Faqs::class, FaqsRepository::class);
        $this->app->when(FaqsRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(FaqCriteriaFactory::class);
        $this->app->when(FaqsRepository::class)
            ->needs(Model::class)
            ->give(Faq::class);

        /* STORES */
        $this->app->bind(Stores::class, StoreRepository::class);
        $this->app->when(StoreRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(StoreCriteriaFactory::class);
        $this->app->when(StoreRepository::class)
            ->needs(Model::class)
            ->give(Store::class);

        /* PAYMENT METHODS */
        $this->app->bind(PaymentMethods::class, PaymentMethodRepository::class);
        $this->app->when(PaymentMethodRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PaymentMethodCriteriaFactory::class);
        $this->app->when(PaymentMethodRepository::class)
            ->needs(Model::class)
            ->give(PaymentMethod::class);

        /* BRANDS */
        $this->app->bind(Brands::class, BrandRepository::class);
        $this->app->when(BrandRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(BrandCriteriaFactory::class);
        $this->app->when(BrandRepository::class)
            ->needs(Model::class)
            ->give(Brand::class);

        /* MODELS */
        $this->app->bind(Models::class, PhoneModelsRepository::class);
        $this->app->when(PhoneModelsRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PhoneModelCriteriaFactory::class);
        $this->app->when(PhoneModelsRepository::class)
            ->needs(Model::class)
            ->give(PhoneModel::class);

        /* TOP MODELS */
        $this->app->bind(TopModels::class, TopModelsRepository::class);
        $this->app->when(TopModelsRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PhoneTopModelCriteriaFactory::class);
        $this->app->when(TopModelsRepository::class)
            ->needs(Model::class)
            ->give(TopModel::class);

        /* LATEST MODELS */
        $this->app->bind(LatestModels::class, LatestModelsRepository::class);
        $this->app->when(LatestModelsRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PhoneLatestModelCriteriaFactory::class);
        $this->app->when(LatestModelsRepository::class)
            ->needs(Model::class)
            ->give(LatestModel::class);

        /* COLORS */
        $this->app->bind(Colors::class, PhoneColorRepository::class);
        $this->app->when(PhoneColorRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(ColorCriteriaFactory::class);
        $this->app->when(PhoneColorRepository::class)
            ->needs(Model::class)
            ->give(Color::class);

        /* PHONES */
        $this->app->bind(Phones::class, PhoneItemRepository::class);
        $this->app->when(PhoneItemRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PhoneCriteriaFactory::class);
        $this->app->when(PhoneItemRepository::class)
            ->needs(Model::class)
            ->give(Phone::class);

        /* NETWORKS */
        $this->app->bind(Networks::class, NetworkRepository::class);
        $this->app->when(NetworkRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(NetworkCriteriaFactory::class);
        $this->app->when(NetworkRepository::class)
            ->needs(Model::class)
            ->give(Network::class);

        /* CONTRACTS */
        $this->app->bind(Contracts::class, ContractRepository::class);
        $this->app->when(ContractRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(ContractCriteriaFactory::class);
        $this->app->when(ContractRepository::class)
            ->needs(Model::class)
            ->give(Contract::class);

        /* DEALS */
        $this->app->bind(Deals::class, DealRepository::class);
        $this->app->when(DealRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(DealCriteriaFactory::class);
        $this->app->when(DealRepository::class)
            ->needs(Model::class)
            ->give(Deal::class);

        /* LINKS */
        $this->app->bind(Links::class, LinkRepository::class);
        $this->app->when(LinkRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(LinkCriteriaFactory::class);
        $this->app->when(LinkRepository::class)
            ->needs(Model::class)
            ->give(Link::class);

        /* NEGATIVE REPORTS */
        $this->app->bind(NegativeReports::class, NegativeReportRepository::class);
        $this->app->when(NegativeReportRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(NegativeReportCriteriaFactory::class);
        $this->app->when(NegativeReportRepository::class)
            ->needs(Model::class)
            ->give(NegativeReport::class);

        /* POSITIVE REPORTS */
        $this->app->bind(PositiveReports::class, PositiveReportRepository::class);
        $this->app->when(PositiveReportRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PositiveReportCriteriaFactory::class);
        $this->app->when(PositiveReportRepository::class)
            ->needs(Model::class)
            ->give(PositiveReport::class);

        /* DATAFEEDS API */
        $this->app->bind(DatafeedsAPIs::class, DatafeedsApiRepository::class);
        $this->app->when(DatafeedsApiRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(DatafeedsApiCriteriaFactory::class);
        $this->app->when(DatafeedsApiRepository::class)
            ->needs(Model::class)
            ->give(DatafeedApi::class);

        /* DATAFEEDS INFO */
        $this->app->bind(DatafeedsInfo::class, DatafeedsInfoRepository::class);
        $this->app->when(DatafeedsInfoRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(DatafeedsInfoCriteriaFactory::class);
        $this->app->when(DatafeedsInfoRepository::class)
            ->needs(Model::class)
            ->give(DatafeedInfo::class);

        /* USERS */
        $this->app->bind(Users::class, UserRepository::class);
        $this->app->when(UserRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(UserCriteriaFactory::class);
        $this->app->when(UserRepository::class)
            ->needs(Model::class)
            ->give(User::class);

        /* ROLES */
        $this->app->bind(Roles::class, RoleRepository::class);
        $this->app->when(RoleRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(RoleCriteriaFactory::class);
        $this->app->when(RoleRepository::class)
            ->needs(Model::class)
            ->give(Role::class);

        /* PLANNED IMPORTS */
        $this->app->bind(PlannedImports::class, PlannedImportsRepository::class);
        $this->app->when(PlannedImportsRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(PlannedImportsCriteriaFactory::class);
        $this->app->when(PlannedImportsRepository::class)
            ->needs(Model::class)
            ->give(PlannedImport::class);

        $this->app->bind(DescriptionImageRepository::class, DescriptionImageRepositoryImpl::class);

        $this->app->bind(ModelMergeFilters::class, MergeModelFiltersRepository::class);
        $this->app->when(MergeModelFiltersRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(MergeModelFilterCriteriaFactory::class);
        $this->app->when(MergeModelFiltersRepository::class)
            ->needs(Model::class)
            ->give(Filter::class);

        $this->app->bind(ColorMergeFilters::class, MergeColorFiltersRepository::class);
        $this->app->when(MergeColorFiltersRepository::class)
            ->needs(CriteriaFactory::class)
            ->give(MergeColorFilterCriteriaFactory::class);
        $this->app->when(MergeColorFiltersRepository::class)
            ->needs(Model::class)
            ->give(ColorFilter::class);

        /* CONDUCTORS */
        $this->app->bind(WhereConductor::class, WhereConductorImpl::class);
        $this->app->bind(MethodGenerator::class, MethodGeneratorImpl::class);
    }
}
