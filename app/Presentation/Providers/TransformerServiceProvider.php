<?php

namespace App\Presentation\Providers;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Application\Services\Importer\Transformers\SerializeTransformer;
use App\Application\Services\Importer\Transformers\StructureTransformer;
use App\Domain\Common\Contracts\Translators\Translator;
use App\Domain\Phone\Brand\Specifications\BrandSpecification;
use App\Domain\Phone\Model\Specification\PhoneModelSpecification;
use App\Domain\Phone\Phone\Specifications\PhoneSpecification;
use App\Domain\Phone\Phone\Translator\CheckHashFromStringTranslator;
use App\Domain\Phone\Specs\Translators\AutonomyTranslator;
use App\Domain\Phone\Specs\Translators\CameraTranslator;
use App\Domain\Phone\Specs\Translators\ConnectionTranslator;
use App\Domain\Phone\Specs\Translators\DisplayTranslator;
use App\Domain\Phone\Specs\Translators\NetworkTranslator;
use App\Domain\Phone\Specs\Translators\OperatorSystemTranslator;
use App\Domain\Phone\Specs\Translators\PerformanceTranslator;
use App\Domain\Phone\Specs\Translators\PhoneCaseTranslator;
use App\Domain\Shared\Description\Specifications\DescriptionSpecification;
use App\Domain\Shared\Image\Specifications\ImageSpecification;
use App\Domain\Shared\Link\Specifications\LinkSpecification;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Specification\Factory as SpecificationFactory;
use App\Utils\Transformers\Transformers\Awin\AwinContractTransformer;
use App\Utils\Transformers\Transformers\Awin\AwinPhoneTransformer;
use App\Utils\Transformers\Transformers\AwinPhoneModelTransformer;
use App\Utils\Transformers\Transformers\ContractTransformer;
use App\Utils\Transformers\Transformers\DescriptionImportTransformer;
use App\Utils\Transformers\Transformers\ImageImportTransformer;
use App\Utils\Transformers\Transformers\LinkTransformer;
use App\Utils\Transformers\Transformers\LinkWithNoAffiliationTransformer;
use App\Utils\Transformers\Transformers\PhoneBrandImportTransformer;
use App\Utils\Transformers\Transformers\PhoneImportTransformer;
use App\Utils\Transformers\Transformers\PhoneModelImportTransformer;
use App\Utils\Transformers\Transformers\PhonesImageImportTransformer;
use App\Utils\Transformers\Transformers\Specs\AutonomyImportTransformer;
use App\Utils\Transformers\Transformers\Specs\CameraImportTransformer;
use App\Utils\Transformers\Transformers\Specs\ConnectionImportTransformer;
use App\Utils\Transformers\Transformers\Specs\DisplayImportTransformer;
use App\Utils\Transformers\Transformers\Specs\NetworkImportTransformer;
use App\Utils\Transformers\Transformers\Specs\OperatingSystemImportTransformer;
use App\Utils\Transformers\Transformers\Specs\PerformanceImportTransformer;
use App\Utils\Transformers\Transformers\Specs\PhoneCaseImportTransformer;
use App\Utils\Transformers\Transformers\StructureByMapTransformer;
use App\Utils\Transformers\Transformers\WebgainsAffordablePhoneModelTransformer;
use App\Utils\Transformers\Transformers\WebgainsPhoneModelTransformer;
use Illuminate\Support\ServiceProvider;

class TransformerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(StructureTransformer::class, function ($app) {
            return new StructureTransformer(
                $app->make(Config::class),
                $app->make(StructureByMapTransformer::class),
                $app->make(SerializeTransformer::class),
                $app->make(SpecificationFactory::class)
            );
        });

        $this->app->when(DescriptionImportTransformer::class)
            ->needs(Specification::class)
            ->give(DescriptionSpecification::class);

        $this->app->when(ImageImportTransformer::class)
            ->needs(Specification::class)
            ->give(ImageSpecification::class);

        $this->app->when(PhoneBrandImportTransformer::class)
            ->needs(Specification::class)
            ->give(BrandSpecification::class);

        $this->app->when(PhoneModelImportTransformer::class)
            ->needs(Specification::class)
            ->give(PhoneModelSpecification::class);

        $this->app->when(PhoneImportTransformer::class)
            ->needs(Specification::class)
            ->give(PhoneSpecification::class);

        $this->app->when(LinkTransformer::class)
            ->needs(Specification::class)
            ->give(LinkSpecification::class);

        $this->app->when(PhonesImageImportTransformer::class)
            ->needs(Translator::class)
            ->give(CheckHashFromStringTranslator::class);

        $this->app->when(LinkWithNoAffiliationTransformer::class)
            ->needs(Transformer::class)
            ->give(LinkTransformer::class);

        /* AWIN API */
        $this->app->when(AwinPhoneTransformer::class)
            ->needs(Transformer::class)
            ->give(PhoneImportTransformer::class);
        $this->app->when(AwinContractTransformer::class)
            ->needs(Transformer::class)
            ->give(ContractTransformer::class);

        /* WEBGAINS API */
        $this->app->when(WebgainsPhoneModelTransformer::class)
            ->needs(Transformer::class)
            ->give(PhoneModelImportTransformer::class);
        $this->app->when(WebgainsAffordablePhoneModelTransformer::class)
            ->needs(Transformer::class)
            ->give(PhoneModelImportTransformer::class);

        /* Specs */
        $this->app->when(AutonomyImportTransformer::class)
            ->needs(Translator::class)
            ->give(AutonomyTranslator::class);
        $this->app->when(CameraImportTransformer::class)
            ->needs(Translator::class)
            ->give(CameraTranslator::class);
        $this->app->when(ConnectionImportTransformer::class)
            ->needs(Translator::class)
            ->give(ConnectionTranslator::class);
        $this->app->when(DisplayImportTransformer::class)
            ->needs(Translator::class)
            ->give(DisplayTranslator::class);
        $this->app->when(NetworkImportTransformer::class)
            ->needs(Translator::class)
            ->give(NetworkTranslator::class);
        $this->app->when(OperatingSystemImportTransformer::class)
            ->needs(Translator::class)
            ->give(OperatorSystemTranslator::class);
        $this->app->when(PerformanceImportTransformer::class)
            ->needs(Translator::class)
            ->give(PerformanceTranslator::class);
        $this->app->when(PhoneCaseImportTransformer::class)
            ->needs(Translator::class)
            ->give(PhoneCaseTranslator::class);
    }
}
