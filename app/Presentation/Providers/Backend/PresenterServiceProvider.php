<?php

namespace App\Presentation\Providers\Backend;

use App\Presentation\Http\Controllers\Backend\MainPage;
use App\Presentation\Http\Controllers\Backend\Stores\StoreMain;
use App\Presentation\Presenters\Console\ConsoleClearDescriptionImagesPresenter;
use App\Presentation\Presenters\Contracts\Backend\AllReportsPresenter;
use App\Presentation\Presenters\Contracts\Backend\BrandEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\BrandMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\BrandUpdatePresenter;
use App\Presentation\Presenters\Contracts\Backend\ClearDescriptionImagesPresenter;
use App\Presentation\Presenters\Contracts\Backend\ColorEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\ColorMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\ColorManageSynonymPresenter;
use App\Presentation\Presenters\Contracts\Backend\ColorMapViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\ColorSynonymsViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\ColorUpdatePresenter;
use App\Presentation\Presenters\Contracts\Backend\FileUploadEditorPresenter;
use App\Presentation\Presenters\Contracts\Backend\FileUploadPresenter;
use App\Presentation\Presenters\Contracts\Backend\HomePagePresenter;
use App\Presentation\Presenters\Contracts\Backend\InactiveModelListPresenter;
use App\Presentation\Presenters\Contracts\Backend\MainImportPagePresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterDeleteAllPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorFilterSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolColorMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolMergeSynonymsPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeleteAllPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeleteByBrandPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelFilterSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolModelViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolUnmergeColorPresenter;
use App\Presentation\Presenters\Contracts\Backend\MergeToolUnmergeModelPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelAutocompletePresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelListPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelManageSynonymPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelMapViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelSearchPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelsWithoutDealsPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelSynonymViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelUpdatePresenter;
use App\Presentation\Presenters\Contracts\Backend\ModelViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\NegativeReportsViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\NetworksEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\NetworksMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\NetworksSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\OldPhonesListPresenter;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodCreatePresenter;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\PaymentMethods\PaymentMethodsMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryAddImagePresenter;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryDeleteImagePresenter;
use App\Presentation\Presenters\Contracts\Backend\PhoneGalleryUpdateImagePresenter;
use App\Presentation\Presenters\Contracts\Backend\PhoneViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\PlannedDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\PlannedImportsMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\PlannedSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\PositiveReportsViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\ProfileUpdatePresenter;
use App\Presentation\Presenters\Contracts\Backend\ProfileViewPresenter;
use App\Presentation\Presenters\Contracts\Backend\ReportsGroupDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\StoreCreatePresenter;
use App\Presentation\Presenters\Contracts\Backend\StoreDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\StoreSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\StoresEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\StoresMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\TopModelCreatePresenter;
use App\Presentation\Presenters\Contracts\Backend\TopModelDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\TopModelMainPresenter;
use App\Presentation\Presenters\Contracts\Backend\TopModelUpdatePresenter;
use App\Presentation\Presenters\Contracts\Backend\UserCreatePresenter;
use App\Presentation\Presenters\Contracts\Backend\UserDeletePresenter;
use App\Presentation\Presenters\Contracts\Backend\UserEditPresenter;
use App\Presentation\Presenters\Contracts\Backend\UserListPresenter;
use App\Presentation\Presenters\Contracts\Backend\UserSavePresenter;
use App\Presentation\Presenters\Contracts\Backend\UserUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\Import\Models\WebInactiveModelListPresenter;
use App\Presentation\Presenters\Web\Backend\Import\Models\WebMainImportPresenter;
use App\Presentation\Presenters\Web\Backend\Import\Models\WebModelsWithoutDealsPresenter;
use App\Presentation\Presenters\Web\Backend\Import\Planned\WebPlannedImportDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Import\Planned\WebPlannedImportsMainPresenter;
use App\Presentation\Presenters\Web\Backend\Import\Planned\WebPlannedSavePresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Main\WebMergeMainPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Map\WebColorMapViewPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Map\WebModelMapViewPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolColorFilterDeleteAllPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolColorFilterDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolColorFilterSavePresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolColorMainPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolMergeSynonymsPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelFilterDeleteAllPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelFilterDeleteByBrandPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelFilterDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelFilterSavePresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelMainPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolModelViewPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolUnmergeColorPresenter;
use App\Presentation\Presenters\Web\Backend\Merge\Tool\WebMergeToolUnmergeModelPresenter;
use App\Presentation\Presenters\Web\Backend\Networks\WebNetworkSavePresenter;
use App\Presentation\Presenters\Web\Backend\Networks\WebNetworksEditPresenter;
use App\Presentation\Presenters\Web\Backend\Networks\WebNetworksMainPresenter;
use App\Presentation\Presenters\Web\Backend\PaymentMethods\WebPaymentMethodCreatePresenter;
use App\Presentation\Presenters\Web\Backend\PaymentMethods\WebPaymentMethodDeletePresenter;
use App\Presentation\Presenters\Web\Backend\PaymentMethods\WebPaymentMethodEditPresenter;
use App\Presentation\Presenters\Web\Backend\PaymentMethods\WebPaymentMethodSavePresenter;
use App\Presentation\Presenters\Web\Backend\PaymentMethods\WebPaymentMethodsMainPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Brand\WebBrandEditPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Brand\WebBrandMainPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Brand\WebBrandUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Color\WebColorEditPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Color\WebColorMainPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Color\WebColorManageSynonymPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Color\WebColorSynonymsViewPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Color\WebColorUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelAutocompletePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelEditPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelListPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelMainPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelManageSynonymPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelSearchPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelSynonymViewPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebModelViewPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Model\WebOldPhonesListPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Phone\WebPhoneGalleryAddImagePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Phone\WebPhoneGalleryDeleteImagePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Phone\WebPhoneGalleryUpdateImagePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\Phone\WebPhoneViewPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\TopModel\WebTopModelCreatePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\TopModel\WebTopModelDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Phone\TopModel\WebTopModelMainPresenter;
use App\Presentation\Presenters\Web\Backend\Phone\TopModel\WebTopModelUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\Reports\WebAllReportsPresenter;
use App\Presentation\Presenters\Web\Backend\Reports\WebNegativeReportsDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Reports\WebNegativeReportsViewPresenter;
use App\Presentation\Presenters\Web\Backend\Reports\WebPositiveReportsDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Reports\WebPositiveReportsViewPresenter;
use App\Presentation\Presenters\Web\Backend\Reports\WebReportsGroupDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Stores\WebStoreCreatePresenter;
use App\Presentation\Presenters\Web\Backend\Stores\WebStoreDeletePresenter;
use App\Presentation\Presenters\Web\Backend\Stores\WebStoreSavePresenter;
use App\Presentation\Presenters\Web\Backend\Stores\WebStoresEditPresenter;
use App\Presentation\Presenters\Web\Backend\Stores\WebStoresMainPresenter;
use App\Presentation\Presenters\Web\Backend\User\Profile\WebProfileUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\User\Profile\WebProfileViewPresenter;
use App\Presentation\Presenters\Web\Backend\User\User\WebUserCreatePresenter;
use App\Presentation\Presenters\Web\Backend\User\User\WebUserDeletePresenter;
use App\Presentation\Presenters\Web\Backend\User\User\WebUserEditPresenter;
use App\Presentation\Presenters\Web\Backend\User\User\WebUserListPresenter;
use App\Presentation\Presenters\Web\Backend\User\User\WebUserSavePresenter;
use App\Presentation\Presenters\Web\Backend\User\User\WebUserUpdatePresenter;
use App\Presentation\Presenters\Web\Backend\WebFileUploadEditorPresenter;
use App\Presentation\Presenters\Web\Backend\WebFileUploadPresenter;
use App\Presentation\Presenters\Web\Backend\WebHomePagePresenter;
use App\Presentation\Services\Translators\Autocomplete\ModelsTranslator;
use App\Presentation\Services\Translators\Contracts\Translator;
use Illuminate\Support\ServiceProvider;

class PresenterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* BACKEND */
        $this->app->when(MainPage::class)
            ->needs(HomePagePresenter::class)
            ->give(WebHomePagePresenter::class);
        $this->app->bind(FileUploadPresenter::class, WebFileUploadPresenter::class);
        $this->app->bind(FileUploadEditorPresenter::class, WebFileUploadEditorPresenter::class);
        $this->app->bind(ClearDescriptionImagesPresenter::class, ConsoleClearDescriptionImagesPresenter::class);

        /* STORES */
        $this->app->when(StoreMain::class)
            ->needs(StoresMainPresenter::class)
            ->give(WebStoresMainPresenter::class);
        $this->app->bind(StoresEditPresenter::class, WebStoresEditPresenter::class);
        $this->app->bind(StoreSavePresenter::class, WebStoreSavePresenter::class);
        $this->app->bind(StoreDeletePresenter::class, WebStoreDeletePresenter::class);
        $this->app->bind(StoreCreatePresenter::class, WebStoreCreatePresenter::class);

        /* PAYMENT METHODS */
        $this->app->bind(PaymentMethodsMainPresenter::class, WebPaymentMethodsMainPresenter::class);
        $this->app->bind(PaymentMethodCreatePresenter::class, WebPaymentMethodCreatePresenter::class);
        $this->app->bind(PaymentMethodSavePresenter::class, WebPaymentMethodSavePresenter::class);
        $this->app->bind(PaymentMethodEditPresenter::class, WebPaymentMethodEditPresenter::class);
        $this->app->bind(PaymentMethodDeletePresenter::class, WebPaymentMethodDeletePresenter::class);

        /* NETWORKS */
        $this->app->bind(NetworksMainPresenter::class, WebNetworksMainPresenter::class);
        $this->app->bind(NetworksEditPresenter::class, WebNetworksEditPresenter::class);
        $this->app->bind(NetworksSavePresenter::class, WebNetworkSavePresenter::class);

        /* BRAND */
        $this->app->bind(BrandMainPresenter::class, WebBrandMainPresenter::class);
        $this->app->bind(BrandEditPresenter::class, WebBrandEditPresenter::class);
        $this->app->bind(BrandUpdatePresenter::class, WebBrandUpdatePresenter::class);

        /* MODELS */
        $this->app->bind(ModelMainPresenter::class, WebModelMainPresenter::class);
        $this->app->bind(ModelListPresenter::class, WebModelListPresenter::class);
        $this->app->bind(ModelViewPresenter::class, WebModelViewPresenter::class);
        $this->app->bind(ModelEditPresenter::class, WebModelEditPresenter::class);
        $this->app->bind(ModelSearchPresenter::class, WebModelSearchPresenter::class);
        $this->app->Bind(ModelUpdatePresenter::class, WebModelUpdatePresenter::class);
        $this->app->bind(ModelSynonymViewPresenter::class, WebModelSynonymViewPresenter::class);
        $this->app->bind(ModelManageSynonymPresenter::class, WebModelManageSynonymPresenter::class);
        $this->app->bind(ModelAutocompletePresenter::class, WebModelAutocompletePresenter::class);
        $this->app->bind(OldPhonesListPresenter::class, WebOldPhonesListPresenter::class);

        $this->app->when(WebModelAutocompletePresenter::class)
            ->needs(Translator::class)
            ->give(ModelsTranslator::class);

        /* TOP MODELS */
        $this->app->bind(TopModelMainPresenter::class, WebTopModelMainPresenter::class);
        $this->app->bind(TopModelCreatePresenter::class, WebTopModelCreatePresenter::class);
        $this->app->bind(TopModelDeletePresenter::class, WebTopModelDeletePresenter::class);
        $this->app->bind(TopModelUpdatePresenter::class, WebTopModelUpdatePresenter::class);

        /* NEGATIVE REPORTS */
        $this->app->bind(NegativeReportsViewPresenter::class, WebNegativeReportsViewPresenter::class);
        $this->app->bind(NegativeReportsDeletePresenter::class, WebNegativeReportsDeletePresenter::class);

        /* POSITIVE REPORTS */
        $this->app->bind(PositiveReportsViewPresenter::class, WebPositiveReportsViewPresenter::class);
        $this->app->bind(PositiveReportsDeletePresenter::class, WebPositiveReportsDeletePresenter::class);

        $this->app->bind(AllReportsPresenter::class, WebAllReportsPresenter::class);
        $this->app->bind(ReportsGroupDeletePresenter::class, WebReportsGroupDeletePresenter::class);

        /* PHONES */
        $this->app->bind(PhoneViewPresenter::class, WebPhoneViewPresenter::class);
        $this->app->bind(PhoneGalleryAddImagePresenter::class, WebPhoneGalleryAddImagePresenter::class);
        $this->app->bind(PhoneGalleryDeleteImagePresenter::class, WebPhoneGalleryDeleteImagePresenter::class);
        $this->app->bind(PhoneGalleryUpdateImagePresenter::class, WebPhoneGalleryUpdateImagePresenter::class);

        /* COLORS */
        $this->app->bind(ColorMainPresenter::class, WebColorMainPresenter::class);
        $this->app->bind(ColorEditPresenter::class, WebColorEditPresenter::class);
        $this->app->bind(ColorUpdatePresenter::class, WebColorUpdatePresenter::class);
        $this->app->bind(ColorSynonymsViewPresenter::class, WebColorSynonymsViewPresenter::class);
        $this->app->bind(ColorManageSynonymPresenter::class, WebColorManageSynonymPresenter::class);

        /* USERS */
        $this->app->bind(UserListPresenter::class, WebUserListPresenter::class);
        $this->app->bind(UserCreatePresenter::class, WebUserCreatePresenter::class);
        $this->app->bind(UserEditPresenter::class, WebUserEditPresenter::class);
        $this->app->bind(UserDeletePresenter::class, WebUserDeletePresenter::class);
        $this->app->bind(UserSavePresenter::class, WebUserSavePresenter::class);
        $this->app->bind(UserUpdatePresenter::class, WebUserUpdatePresenter::class);

        /* PROFILE */
        $this->app->bind(ProfileViewPresenter::class, WebProfileViewPresenter::class);
        $this->app->bind(ProfileUpdatePresenter::class, WebProfileUpdatePresenter::class);

        /* IMPORT */
        $this->app->bind(MainImportPagePresenter::class, WebMainImportPresenter::class);
        $this->app->bind(InactiveModelListPresenter::class, WebInactiveModelListPresenter::class);
        $this->app->bind(ModelsWithoutDealsPresenter::class, WebModelsWithoutDealsPresenter::class);

        /* PLANNED */
        $this->app->bind(PlannedImportsMainPresenter::class, WebPlannedImportsMainPresenter::class);
        $this->app->bind(PlannedSavePresenter::class, WebPlannedSavePresenter::class);
        $this->app->bind(PlannedDeletePresenter::class, WebPlannedImportDeletePresenter::class);

        /* MERGE */
        $this->app->bind(MergeMainPresenter::class, WebMergeMainPresenter::class);
        $this->app->bind(MergeToolModelMainPresenter::class, WebMergeToolModelMainPresenter::class);
        $this->app->bind(MergeToolModelViewPresenter::class, WebMergeToolModelViewPresenter::class);
        $this->app->bind(MergeToolUnmergeModelPresenter::class, WebMergeToolUnmergeModelPresenter::class);
        $this->app->bind(MergeToolMergeSynonymsPresenter::class, WebMergeToolMergeSynonymsPresenter::class);
        $this->app->bind(MergeToolModelFilterSavePresenter::class, WebMergeToolModelFilterSavePresenter::class);
        $this->app->bind(MergeToolColorFilterSavePresenter::class, WebMergeToolColorFilterSavePresenter::class);
        $this->app->bind(MergeToolModelFilterDeletePresenter::class, WebMergeToolModelFilterDeletePresenter::class);
        $this->app->bind(
            MergeToolModelFilterDeleteByBrandPresenter::class,
            WebMergeToolModelFilterDeleteByBrandPresenter::class
        );
        $this->app->bind(
            MergeToolModelFilterDeleteAllPresenter::class,
            WebMergeToolModelFilterDeleteAllPresenter::class
        );
        $this->app->bind(MergeToolUnmergeColorPresenter::class, WebMergeToolUnmergeColorPresenter::class);
        $this->app->bind(MergeToolColorMainPresenter::class, WebMergeToolColorMainPresenter::class);
        $this->app->bind(MergeToolColorFilterDeletePresenter::class, WebMergeToolColorFilterDeletePresenter::class);
        $this->app->bind(
            MergeToolColorFilterDeleteAllPresenter::class,
            WebMergeToolColorFilterDeleteAllPresenter::class
        );

        /* MERGE MAP */
        $this->app->bind(ModelMapViewPresenter::class, WebModelMapViewPresenter::class);
        $this->app->bind(ColorMapViewPresenter::class, WebColorMapViewPresenter::class);
    }
}
