<?php


namespace App\Presentation\Providers\Backend;

use App\Presentation\Http\Controllers\Backend\Merge\Tool\Model\MergeSynonyms;
use App\Presentation\Services\Request\Preprocessors\Contracts\MergeSynonymsRequestPreprocessor;
use Illuminate\Support\ServiceProvider;
use App\Presentation\Http\Controllers\Backend\Merge\Tool\Color\MergeSynonyms as ColorMergeSynonyms;

class ControllerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(MergeSynonyms::class)
            ->needs(MergeSynonymsRequestPreprocessor::class)
            ->give(\App\Presentation\Services\Request\Preprocessors\MergeSynonymsRequestPreprocessor::class);

        $this->app->when(ColorMergeSynonyms::class)
            ->needs(MergeSynonymsRequestPreprocessor::class)
            ->give(\App\Presentation\Services\Request\Preprocessors\MergeSynonymsRequestPreprocessor::class);
    }
}
