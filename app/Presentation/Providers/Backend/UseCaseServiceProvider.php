<?php

namespace App\Presentation\Providers\Backend;

use App\Application\Services\Specifications\UserManageSpecification;
use App\Application\UseCases\Backend\Import\Main\Contracts\MainImportCase;
use App\Application\UseCases\Backend\Import\Main\MainImportCase as MainImportCaseImpl;
use App\Application\UseCases\Backend\Import\Model\Contracts\InactiveListCase;
use App\Application\UseCases\Backend\Import\Model\Contracts\WithoutDealsCase;
use App\Application\UseCases\Backend\Import\Model\InactiveListCase as InactiveListCaseImpl;
use App\Application\UseCases\Backend\Import\Model\WithoutDealsCase as WithoutDealsCaseImpl;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedDeleteCase;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedMainCase;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedSaveCase;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedUpdateCase;
use App\Application\UseCases\Backend\Import\Planned\PlannedDeleteCase as PlannedDeleteCaseImpl;
use App\Application\UseCases\Backend\Import\Planned\PlannedMainCase as PlannedMainCaseImpl;
use App\Application\UseCases\Backend\Import\Planned\PlannedSaveCase as PlannedSaveCaseImpl;
use App\Application\UseCases\Backend\Import\Planned\PlannedUpdateCase as PlannedUpdateCaseImpl;
use App\Application\UseCases\Backend\Main\ClearDescriptionImagesCase as ClearDescriptionImagesCaseImpl;
use App\Application\UseCases\Backend\Main\Contracts\ClearDescriptionImagesCase;
use App\Application\UseCases\Backend\Main\Contracts\FileUploadCase;
use App\Application\UseCases\Backend\Main\Contracts\FileUploadEditorCase;
use App\Application\UseCases\Backend\Main\Contracts\MainPageCase;
use App\Application\UseCases\Backend\Main\FileUploadCase as FileUploadCaseImpl;
use App\Application\UseCases\Backend\Main\FileUploadEditorCase as FileUploadEditorCaseImpl;
use App\Application\UseCases\Backend\Main\MainPageCase as MainPageCaseImpl;
use App\Application\UseCases\Backend\Merge\Main\Contracts\MergeMainCase;
use App\Application\UseCases\Backend\Merge\Main\MergeMainCase as MergeMainCaseImpl;
use App\Application\UseCases\Backend\Merge\Map\Color\Contracts\MapViewCase as ColorMapViewCase;
use App\Application\UseCases\Backend\Merge\Map\Color\MapViewCase as ColorMapViewCaseImpl;
use App\Application\UseCases\Backend\Merge\Map\Model\Contracts\MapViewCase as ModelMapViewCase;
use App\Application\UseCases\Backend\Merge\Map\Model\MapViewCase as ModelMapViewCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Color\Contracts\MergeSynonymsCase as ColorMergeSynonymsCase;
use App\Application\UseCases\Backend\Merge\Tool\Color\Contracts\ToolMainCase as ColorToolMainCase;
use App\Application\UseCases\Backend\Merge\Tool\Color\Contracts\UnmergeColorCase;
use App\Application\UseCases\Backend\Merge\Tool\Color\MergeSynonymsCase as ColorMergeSynonymsCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Color\ToolMainCase as ColorToolMainCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Color\UnmergeColorCase as UnmergeColorCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolColorFilterDeleteAllCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolColorFilterDeleteCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolColorFilterSaveCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterDeleteAllCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterDeleteByBrandCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterDeleteCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\Contracts\ToolModelFilterSaveCase;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolColorFilterDeleteAllCase
    as ToolColorFilterDeleteAllCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolColorFilterDeleteCase as ToolColorFilterDeleteCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolColorFilterSaveCase as ToolColorFilterSaveCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteAllCase
    as ToolModelFilterDeleteAllCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteByBrandCase
    as ToolModelFilterDeleteByBrandCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterDeleteCase as ToolModelFilterDeleteCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Filters\ToolModelFilterSaveCase as ToolModelFilterSaveCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\MergeSynonymsCase;
use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\ToolMainCase as ModelToolMainCase;
use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\ToolViewCase as ToolModelViewCase;
use App\Application\UseCases\Backend\Merge\Tool\Models\Contracts\UnmergeModelCase;
use App\Application\UseCases\Backend\Merge\Tool\Models\MergeSynonymsCase as MergeSynonymsCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Models\ToolMainCase as ModelToolMainCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Models\ToolViewCase as ToolModelViewCaseImpl;
use App\Application\UseCases\Backend\Merge\Tool\Models\UnmergeModelCase as UnmergeModelCaseImpl;
use App\Application\UseCases\Backend\Networks\Contracts\NetworkEditCase;
use App\Application\UseCases\Backend\Networks\Contracts\NetworksMainCase;
use App\Application\UseCases\Backend\Networks\Contracts\NetworksUpdateCase;
use App\Application\UseCases\Backend\Networks\NetworkEditCase as NetworkEditCaseImpl;
use App\Application\UseCases\Backend\Networks\NetworksMainCase as NetworksMainCaseImpl;
use App\Application\UseCases\Backend\Networks\NetworksUpdateCase as NetworksUpdateCaseImpl;
use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodDeleteCase;
use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodEditCase;
use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodSaveCase;
use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodsMainCase;
use App\Application\UseCases\Backend\PaymentMethods\Contracts\PaymentMethodUpdateCase;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodDeleteCase as PaymentMethodDeleteCaseImpl;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodEditCase as PaymentMethodEditCaseImpl;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodSaveCase as PaymentMethodSaveCaseImpl;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodsMainCase as PaymentMethodsMainCaseImpl;
use App\Application\UseCases\Backend\PaymentMethods\PaymentMethodUpdateCase as PaymentMethodUpdateCaseImpl;
use App\Application\UseCases\Backend\Phones\Brand\BrandEditCase as BrandEditCaseImpl;
use App\Application\UseCases\Backend\Phones\Brand\BrandMainCase as BrandMainCaseImpl;
use App\Application\UseCases\Backend\Phones\Brand\BrandUpdateCase as BrandUpdateCaseImpl;
use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandEditCase;
use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandMainCase;
use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandUpdateCase;
use App\Application\UseCases\Backend\Phones\Color\ColorAddSynonymCase as ColorAddSynonymCaseImpl;
use App\Application\UseCases\Backend\Phones\Color\ColorEditCase as ColorEditCaseImpl;
use App\Application\UseCases\Backend\Phones\Color\ColorMainCase as ColorMainCaseImpl;
use App\Application\UseCases\Backend\Phones\Color\ColorRemoveSynonymCase as ColorRemoveSynonymCaseImpl;
use App\Application\UseCases\Backend\Phones\Color\ColorSynonymsCase as ColorSynonymsCaseImpl;
use App\Application\UseCases\Backend\Phones\Color\ColorUpdateCase as ColorUpdateCaseImpl;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorAddSynonymCase;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorEditCase;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorMainCase;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorRemoveSynonymCase;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorSynonymsCase;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorUpdateCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelAddSynonymCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelAutocompleteCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelEditCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelListCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelMainCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelRemoveSynonymCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelSearchCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelSynonymViewCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelUpdateCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelViewCase;
use App\Application\UseCases\Backend\Phones\Model\Contracts\OldPhonesListCase;
use App\Application\UseCases\Backend\Phones\Model\ModelAddSynonymCase as ModelAddSynonymCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelAutocompleteCase as ModelAutocompleteCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelEditCase as ModelEditCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelListCase as ModelListCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelMainCase as ModelMainCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelRemoveSynonymCase as ModelRemoveSynonymCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelSearchCase as ModelSearchCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelSynonymViewCase as ModelSynonymViewCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelUpdateCase as ModelUpdateCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\ModelViewCase as ModelVIewCaseImpl;
use App\Application\UseCases\Backend\Phones\Model\OldPhonesListCase as OldPhonesListCaseImpl;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryAddImageCase;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryDeleteImageCase;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryUpdateImageCase;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneViewCase;
use App\Application\UseCases\Backend\Phones\Phone\PhoneGalleryAddImageCase as PhoneGalleryAddImageCaseImpl;
use App\Application\UseCases\Backend\Phones\Phone\PhoneGalleryDeleteImageCase as PhoneGalleryDeleteImageCaseImpl;
use App\Application\UseCases\Backend\Phones\Phone\PhoneGalleryUpdateImageCase as PhoneGalleryUpdateImageCaseImpl;
use App\Application\UseCases\Backend\Phones\Phone\PhoneViewCase as PhoneViewCaseImpl;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelCreateCase;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelDeleteCase;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelMainCase;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelUpdateCase;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelCreateCase as TopModelCreateCaseImpl;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelDeleteCase as TopModelDeleteCaseImpl;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelMainCase as TopModelMainCaseImpl;
use App\Application\UseCases\Backend\Phones\TopModel\TopModelUpdateCase as TopModelUpdateCaseImpl;
use App\Application\UseCases\Backend\Reports\AllReportsCase as AllReportsCaseImpl;
use App\Application\UseCases\Backend\Reports\AllReportsDeleteCase as AllReportsDeleteCaseImpl;
use App\Application\UseCases\Backend\Reports\ByStoreDeleteCase as ByStoreDeleteCaseImpl;
use App\Application\UseCases\Backend\Reports\Contracts\AllReportsCase;
use App\Application\UseCases\Backend\Reports\Contracts\AllReportsDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\ByStoreDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\NegativeDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\NegativeViewCase;
use App\Application\UseCases\Backend\Reports\Contracts\PositiveDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\PositiveViewCase;
use App\Application\UseCases\Backend\Reports\Contracts\SingleNegativeDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\SinglePositiveDeleteCase;
use App\Application\UseCases\Backend\Reports\NegativeDeleteCase as NegativeDeleteCaseImpl;
use App\Application\UseCases\Backend\Reports\NegativeViewCase as NegativeViewCaseImpl;
use App\Application\UseCases\Backend\Reports\PositiveDeleteCase as PositiveDeleteCaseImpl;
use App\Application\UseCases\Backend\Reports\PositiveViewCase as PositiveViewCaseImpl;
use App\Application\UseCases\Backend\Reports\SingleNegativeDeleteCase as SingleNegativeDeleteCaseImpl;
use App\Application\UseCases\Backend\Reports\SinglePositiveDeleteCase as SinglePositiveDeleteCaseImpl;
use App\Application\UseCases\Backend\Stores\Contracts\StoreCreateCase;
use App\Application\UseCases\Backend\Stores\Contracts\StoreDeleteCase;
use App\Application\UseCases\Backend\Stores\Contracts\StoreEditCase;
use App\Application\UseCases\Backend\Stores\Contracts\StoreMainCase;
use App\Application\UseCases\Backend\Stores\Contracts\StoreSaveCase;
use App\Application\UseCases\Backend\Stores\Contracts\StoreUpdateCase;
use App\Application\UseCases\Backend\Stores\StoreCreateCase as StoreCreateCaseImpl;
use App\Application\UseCases\Backend\Stores\StoreDeleteCase as StoreDeleteCaseImpl;
use App\Application\UseCases\Backend\Stores\StoreEditCase as StoreEditCaseImpl;
use App\Application\UseCases\Backend\Stores\StoreMainCase as StoreMainCaseImpl;
use App\Application\UseCases\Backend\Stores\StoreSaveCase as StoreSaveCaseImpl;
use App\Application\UseCases\Backend\Stores\StoreUpdateCase as StoreUpdateCaseImpl;
use App\Application\UseCases\Backend\User\Profile\Contracts\ProfileViewCase;
use App\Application\UseCases\Backend\User\Profile\Contracts\UpdateInfoCase;
use App\Application\UseCases\Backend\User\Profile\Contracts\UpdatePasswordCase;
use App\Application\UseCases\Backend\User\Profile\ProfileViewCase as ProfileViewCaseImpl;
use App\Application\UseCases\Backend\User\Profile\UpdateInfoCase as UpdateInfoCaseImpl;
use App\Application\UseCases\Backend\User\Profile\UpdatePasswordCase as UpdatePasswordCaseImpl;
use App\Application\UseCases\Backend\User\User\Contracts\UserCreateCase;
use App\Application\UseCases\Backend\User\User\Contracts\UserDeleteCase;
use App\Application\UseCases\Backend\User\User\Contracts\UserEditCase;
use App\Application\UseCases\Backend\User\User\Contracts\UserListCase;
use App\Application\UseCases\Backend\User\User\Contracts\UserSaveCase;
use App\Application\UseCases\Backend\User\User\Contracts\UserUpdateCase;
use App\Application\UseCases\Backend\User\User\UserCreateCase as UserCreateCaseImpl;
use App\Application\UseCases\Backend\User\User\UserDeleteCase as UserDeleteCaseImpl;
use App\Application\UseCases\Backend\User\User\UserEditCase as UserEditCaseImpl;
use App\Application\UseCases\Backend\User\User\UserListCase as UserListCaseImpl;
use App\Application\UseCases\Backend\User\User\UserSaveCase as UserSaveCaseImpl;
use App\Application\UseCases\Backend\User\User\UserUpdateCase as UserUpdateCaseImpl;
use App\Domain\User\Specifications\IsSamePassword;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Support\ServiceProvider;

class UseCaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(MainPageCase::class, MainPageCaseImpl::class);
        $this->app->bind(FileUploadCase::class, FileUploadCaseImpl::class);
        $this->app->bind(FileUploadEditorCase::class, FileUploadEditorCaseImpl::class);
        $this->app->bind(ClearDescriptionImagesCase::class, ClearDescriptionImagesCaseImpl::class);

        /* STORES */
        $this->app->bind(StoreMainCase::class, StoreMainCaseImpl::class);
        $this->app->bind(StoreEditCase::class, StoreEditCaseImpl::class);
        $this->app->bind(StoreUpdateCase::class, StoreUpdateCaseImpl::class);
        $this->app->bind(StoreSaveCase::class, StoreSaveCaseImpl::class);
        $this->app->bind(StoreDeleteCase::class, StoreDeleteCaseImpl::class);
        $this->app->bind(StoreCreateCase::class, StoreCreateCaseImpl::class);

        /* PAYMENT METHODS */
        $this->app->bind(PaymentMethodsMainCase::class, PaymentMethodsMainCaseImpl::class);
        $this->app->bind(PaymentMethodSaveCase::class, PaymentMethodSaveCaseImpl::class);
        $this->app->bind(PaymentMethodEditCase::class, PaymentMethodEditCaseImpl::class);
        $this->app->bind(PaymentMethodUpdateCase::class, PaymentMethodUpdateCaseImpl::class);
        $this->app->bind(PaymentMethodDeleteCase::class, PaymentMethodDeleteCaseImpl::class);

        /* NETWORKS */
        $this->app->bind(NetworksMainCase::class, NetworksMainCaseImpl::class);
        $this->app->bind(NetworkEditCase::class, NetworkEditCaseImpl::class);
        $this->app->bind(NetworksUpdateCase::class, NetworksUpdateCaseImpl::class);

        $this->app->bind(BrandMainCase::class, BrandMainCaseImpl::class);
        $this->app->bind(BrandEditCase::class, BrandEditCaseImpl::class);
        $this->app->bind(BrandUpdateCase::class, BrandUpdateCaseImpl::class);

        /* MODEL */
        $this->app->bind(ModelMainCase::class, ModelMainCaseImpl::class);
        $this->app->bind(ModelListCase::class, ModelListCaseImpl::class);
        $this->app->bind(ModelViewCase::class, ModelVIewCaseImpl::class);
        $this->app->bind(ModelEditCase::class, ModelEditCaseImpl::class);
        $this->app->bind(ModelSearchCase::class, ModelSearchCaseImpl::class);
        $this->app->bind(ModelUpdateCase::class, ModelUpdateCaseImpl::class);
        $this->app->bind(ModelSynonymViewCase::class, ModelSynonymViewCaseImpl::class);
        $this->app->bind(ModelAddSynonymCase::class, ModelAddSynonymCaseImpl::class);
        $this->app->bind(ModelRemoveSynonymCase::class, ModelRemoveSynonymCaseImpl::class);
        $this->app->bind(ModelAutocompleteCase::class, ModelAutocompleteCaseImpl::class);
        $this->app->bind(OldPhonesListCase::class, OldPhonesListCaseImpl::class);

        /* TOP MODELS */
        $this->app->bind(TopModelMainCase::class, TopModelMainCaseImpl::class);
        $this->app->bind(TopModelCreateCase::class, TopModelCreateCaseImpl::class);
        $this->app->bind(TopModelDeleteCase::class, TopModelDeleteCaseImpl::class);
        $this->app->bind(TopModelUpdateCase::class, TopModelUpdateCaseImpl::class);

        $this->app->bind(NegativeViewCase::class, NegativeViewCaseImpl::class);
        $this->app->bind(NegativeDeleteCase::class, NegativeDeleteCaseImpl::class);
        $this->app->bind(SingleNegativeDeleteCase::class, SingleNegativeDeleteCaseImpl::class);

        $this->app->bind(PositiveViewCase::class, PositiveViewCaseImpl::class);
        $this->app->bind(PositiveDeleteCase::class, PositiveDeleteCaseImpl::class);
        $this->app->bind(SinglePositiveDeleteCase::class, SinglePositiveDeleteCaseImpl::class);

        $this->app->bind(AllReportsCase::class, AllReportsCaseImpl::class);
        $this->app->bind(AllReportsDeleteCase::class, AllReportsDeleteCaseImpl::class);
        $this->app->bind(ByStoreDeleteCase::class, ByStoreDeleteCaseImpl::class);

        $this->app->bind(PhoneViewCase::class, PhoneViewCaseImpl::class);
        $this->app->bind(PhoneGalleryAddImageCase::class, PhoneGalleryAddImageCaseImpl::class);
        $this->app->bind(PhoneGalleryDeleteImageCase::class, PhoneGalleryDeleteImageCaseImpl::class);
        $this->app->bind(PhoneGalleryUpdateImageCase::class, PhoneGalleryUpdateImageCaseImpl::class);

        $this->app->bind(ColorMainCase::class, ColorMainCaseImpl::class);
        $this->app->bind(ColorEditCase::class, ColorEditCaseImpl::class);
        $this->app->bind(ColorUpdateCase::class, ColorUpdateCaseImpl::class);
        $this->app->bind(ColorSynonymsCase::class, ColorSynonymsCaseImpl::class);
        $this->app->bind(ColorAddSynonymCase::class, ColorAddSynonymCaseImpl::class);
        $this->app->bind(ColorRemoveSynonymCase::class, ColorRemoveSynonymCaseImpl::class);

        $this->app->bind(UserListCase::class, UserListCaseImpl::class);
        $this->app->bind(UserCreateCase::class, UserCreateCaseImpl::class);
        $this->app->bind(UserEditCase::class, UserEditCaseImpl::class);
        $this->app->when(UserEditCaseImpl::class)
            ->needs(Specification::class)
            ->give(UserManageSpecification::class);
        $this->app->bind(UserDeleteCase::class, UserDeleteCaseImpl::class);
        $this->app->when(UserDeleteCaseImpl::class)
            ->needs(Specification::class)
            ->give(UserManageSpecification::class);
        $this->app->bind(UserSaveCase::class, UserSaveCaseImpl::class);
        $this->app->bind(UserUpdateCase::class, UserUpdateCaseImpl::class);

        $this->app->bind(ProfileViewCase::class, ProfileViewCaseImpl::class);
        $this->app->bind(UpdateInfoCase::class, UpdateInfoCaseImpl::class);
        $this->app->when(UpdateInfoCaseImpl::class)
            ->needs(Specification::class)
            ->give(IsSamePassword::class);
        $this->app->bind(UpdatePasswordCase::class, UpdatePasswordCaseImpl::class);

        $this->app->bind(MainImportCase::class, MainImportCaseImpl::class);
        $this->app->bind(InactiveListCase::class, InactiveListCaseImpl::class);
        $this->app->bind(WithoutDealsCase::class, WithoutDealsCaseImpl::class);

        $this->app->bind(PlannedMainCase::class, PlannedMainCaseImpl::class);
        $this->app->bind(PlannedSaveCase::class, PlannedSaveCaseImpl::class);
        $this->app->bind(PlannedUpdateCase::class, PlannedUpdateCaseImpl::class);
        $this->app->bind(PlannedDeleteCase::class, PlannedDeleteCaseImpl::class);

        /*  MERGE TOOL */
        $this->app->bind(MergeMainCase::class, MergeMainCaseImpl::class);
        $this->app->bind(ModelToolMainCase::class, ModelToolMainCaseImpl::class);
        $this->app->bind(ToolModelViewCase::class, ToolModelViewCaseImpl::class);
        $this->app->bind(MergeSynonymsCase::class, MergeSynonymsCaseImpl::class);
        $this->app->bind(ColorMergeSynonymsCase::class, ColorMergeSynonymsCaseImpl::class);
        $this->app->bind(UnmergeModelCase::class, UnmergeModelCaseImpl::class);
        $this->app->bind(UnmergeColorCase::class, UnmergeColorCaseImpl::class);

        $this->app->bind(ToolModelFilterSaveCase::class, ToolModelFilterSaveCaseImpl::class);
        $this->app->bind(ToolModelFilterDeleteCase::class, ToolModelFilterDeleteCaseImpl::class);
        $this->app->bind(ToolModelFilterDeleteByBrandCase::class, ToolModelFilterDeleteByBrandCaseImpl::class);
        $this->app->bind(ToolModelFilterDeleteAllCase::class, ToolModelFilterDeleteAllCaseImpl::class);
        $this->app->bind(ToolColorFilterSaveCase::class, ToolColorFilterSaveCaseImpl::class);
        $this->app->bind(ToolColorFilterDeleteCase::class, ToolColorFilterDeleteCaseImpl::class);
        $this->app->bind(ToolColorFilterDeleteAllCase::class, ToolColorFilterDeleteAllCaseImpl::class);


        $this->app->bind(ColorToolMainCase::class, ColorToolMainCaseImpl::class);

        /* MERGE MAP */
        $this->app->bind(ModelMapViewCase::class, ModelMapViewCaseImpl::class);
        $this->app->bind(ColorMapViewCase::class, ColorMapViewCaseImpl::class);
    }
}
