<?php

namespace App\Presentation\Providers;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\EventBus;
use App\Application\Services\Bus\QueryBus;
use App\Utils\Adapters\Event\Contracts\Event;
use Illuminate\Support\ServiceProvider;
use App\Utils\Bus\CommandBus as CommandBusImpl;
use App\Utils\Bus\QueryBus as QueryBusImpl;
use App\Utils\Bus\EventBus as EventBusImpl;

class BusServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(EventBus::class, EventBusImpl::class);
        $this->app->bind(Event::class, EventBusImpl::class);
        $this->app->bind(CommandBus::class, CommandBusImpl::class);
        $this->app->bind(QueryBus::class, QueryBusImpl::class);
    }
}
