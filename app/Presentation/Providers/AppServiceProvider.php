<?php

namespace App\Presentation\Providers;

use App\Application\Services\Affiliation\Contracts\Generator as AffiliationGenerator;
use App\Application\Services\File\Image\Contracts\PathHelper;
use App\Application\Services\File\Image\Helpers\PathHelper as PathHelperImpl;
use App\Application\Services\Importer\Datafeeds\LinkGenerator\Contracts\LinkGenerator;
use App\Application\Services\Importer\Datafeeds\LinkGenerator\LinkGenerator as LinkGeneratorImpl;
use App\Application\Services\Importer\Datafeeds\Services\Contracts\Datafeeds;
use App\Application\Services\Importer\Datafeeds\Services\Datafeeds as DatafeedsImpl;
use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Orderer\Orderer as OrdererImpl;
use App\Application\Services\Search\Contracts\DealsSearcher;
use App\Application\Services\Search\DealsSearcher as DealsSearcherImpl;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\FiltersSearcherByDeals as FiltersImpl;
use App\Application\Services\Store\Rating\Contracts\StoreRatingManager;
use App\Application\Services\Store\Rating\StoreRatingManager as StoreRatingManagerImpl;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\Store;
use App\Presentation\Services\Filters\Contracts\Generator as FilterGenerator;
use App\Presentation\Services\Generators\Title\AssignmentScenarios\ByPhoneModelIdAssignmentScenario;
use App\Presentation\Services\Generators\Title\Contracts\AssignmentScenario;
use App\Presentation\Services\Generators\Title\Contracts\TitleGenerator;
use App\Presentation\Services\Generators\Title\TitleGenerator as TitleGeneratorImpl;
use App\Utils\Adapters\Auth\Auth as AuthImpl;
use App\Utils\Adapters\Auth\Contracts\Auth;
use App\Utils\Adapters\Blog\Contracts\PostService;
use App\Utils\Adapters\Blog\PostService as PostServiceImpl;
use App\Utils\Adapters\Cache\CacheLaravel;
use App\Utils\Adapters\Cache\Contracts\Cache;
use App\Utils\Adapters\Config\Config as ConfigImpl;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Container\Contracts\Container;
use App\Utils\Adapters\Container\LaravelContainer;
use App\Utils\Adapters\Cookie\Contract\Cookie;
use App\Utils\Adapters\Cookie\Cookie as CookieImpl;
use App\Utils\Adapters\Encryptor\Contract\Encryptor;
use App\Utils\Adapters\Encryptor\Encryptor as EncryptorImpl;
use App\Utils\Adapters\Filesystem\Contracts\Filesystem;
use App\Utils\Adapters\Filesystem\Filesystem as FilesystemImpl;
use App\Utils\Adapters\JobDispatcher\Contracts\JobDispatcher;
use App\Utils\Adapters\JobDispatcher\JobDispatcher as JobDispatcherImpl;
use App\Utils\Adapters\Log\ChannelLogger as ChannelLoggerImpl;
use App\Utils\Adapters\Log\ConsoleLogger;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use App\Utils\Adapters\Log\Contracts\ExceptionLogger;
use App\Utils\Adapters\Log\Contracts\Logger;
use App\Utils\Adapters\Log\SentryExceptionLogger;
use App\Utils\Adapters\Notificator\Contracts\Notificator;
use App\Utils\Adapters\Notificator\Notificator as NotificatorImpl;
use App\Utils\Adapters\Slug\CheckAliasGenerator as CheckAliasGeneratorImpl;
use App\Utils\Adapters\Slug\Contracts\CheckAliasGenerator;
use App\Utils\Adapters\Slug\Contracts\Generator as SlugGenerator;
use App\Utils\Adapters\Slug\Generator as SlugGeneratorImpl;
use App\Utils\Adapters\Storage\Contracts\Storage;
use App\Utils\Adapters\Storage\Storage as StorageImpl;
use App\Utils\Adapters\Transaction\Contracts\Transaction;
use App\Utils\Adapters\Transaction\LaravelTransaction;
use App\Utils\Adapters\UrlGenerator\Contracts\UrlGenerator;
use App\Utils\Adapters\UrlGenerator\LaravelUrlGenerator;
use App\Utils\Adapters\Zipper\Contracts\Zipper;
use App\Utils\Adapters\Zipper\Zipper as ZipperImpl;
use App\Utils\Affiliation\Generator as AffiliationGeneratorImpl;
use App\Utils\Encoders\Contracts\CSVEncoder;
use App\Utils\Encoders\Contracts\JsonEncoder;
use App\Utils\Encoders\CSVEncoder as CSVEncoderImpl;
use App\Utils\Encoders\JsonEncoder as JsonEncodeImpl;
use App\Utils\File\Download\Contract\FileDownloader;
use App\Utils\File\Download\FileDownloader as FileDownloaderImpl;
use App\Utils\File\Resizer\Contract\ImageResizer;
use App\Utils\File\Resizer\ImageResizer as ImageResizerImpl;
use App\Utils\File\Upload\Contract\FileUploader;
use App\Utils\File\Upload\FileUploader as FileUploaderImpl;
use App\Utils\Http\Contracts\ContentGetter;
use App\Utils\Http\HttpContentGetter;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;
use App\Utils\Importer\Source\LinkGenerator\LinkGeneratorFactory as LinkGeneratorFactoryImpl;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Serializer\Serializer as SerializerImpl;
use App\Utils\Sitemaps\Contracts\Sitemaps;
use App\Utils\Sitemaps\Sitemaps as SitemapsImpl;
use App\Utils\Template\Filters\Generator as FilterGeneratorImpl;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            Brand::OWNER_TYPE => Brand::class,
            Phone::OWNER_TYPE => Phone::class,
            Store::OWNER_TYPE => Store::class,
            PaymentMethod::OWNER_TYPE => PaymentMethod::class,
            Deal::OWNER_TYPE => Deal::class,
            Network::OWNER_TYPE => Network::class,
            PhoneModel::OWNER_TYPE => PhoneModel::class,
            Rating::OWNER_TYPE => Rating::class,
        ]);
        $this->app->bind(Container::class, LaravelContainer::class);
        $this->app->bind(Config::class, ConfigImpl::class);
        $this->app->bind(Cache::class, CacheLaravel::class);
        $this->app->bind(Transaction::class, LaravelTransaction::class);
        $this->app->bind(JsonEncoder::class, JsonEncodeImpl::class);
        $this->app->bind(JobDispatcher::class, JobDispatcherImpl::class);
        $this->app->bind(CSVEncoder::class, CSVEncoderImpl::class);
        $this->app->bind(Serializer::class, SerializerImpl::class);
        $this->app->bind(Logger::class, ConsoleLogger::class);
        $this->app->bind(ContentGetter::class, HttpContentGetter::class);
        $this->app->bind(SlugGenerator::class, SlugGeneratorImpl::class);
        $this->app->bind(CheckAliasGenerator::class, CheckAliasGeneratorImpl::class);
        $this->app->bind(Storage::class, StorageImpl::class);
        $this->app->bind(Filesystem::class, FilesystemImpl::class);
        $this->app->bind(FileUploader::class, FileUploaderImpl::class);
        $this->app->bind(FileDownloader::class, FileDownloaderImpl::class);
        $this->app->bind(FilterGenerator::class, FilterGeneratorImpl::class);
        $this->app->bind(TitleGenerator::class, TitleGeneratorImpl::class);
        $this->app->bind(AssignmentScenario::class, ByPhoneModelIdAssignmentScenario::class);
        $this->app->bind(Zipper::class, ZipperImpl::class);
        $this->app->bind(Sitemaps::class, SitemapsImpl::class);
        $this->app->bind(Notificator::class, NotificatorImpl::class);
        $this->app->bind(AffiliationGenerator::class, AffiliationGeneratorImpl::class);
        $this->app->bind(Cookie::class, CookieImpl::class);
        $this->app->bind(Datafeeds::class, DatafeedsImpl::class);
        $this->app->bind(Auth::class, AuthImpl::class);
        $this->app->bind(Encryptor::class, EncryptorImpl::class);
        $this->app->bind(FiltersSearcher::class, FiltersImpl::class);
        $this->app->bind(DealsSearcher::class, DealsSearcherImpl::class);
        $this->app->bind(PostService::class, PostServiceImpl::class);
        $this->app->bind(ImageResizer::class, ImageResizerImpl::class);
        $this->app->bind(Orderer::class, OrdererImpl::class);
        $this->app->bind(ChannelLogger::class, ChannelLoggerImpl::class);
        $this->app->bind(ExceptionLogger::class, SentryExceptionLogger::class);
        $this->app->bind(PathHelper::class, PathHelperImpl::class);
        $this->app->bind(LinkGeneratorFactory::class, LinkGeneratorFactoryImpl::class);
        $this->app->bind(UrlGenerator::class, LaravelUrlGenerator::class);

        /**
         * Application Layer
         */
        $this->app->bind(LinkGenerator::class, LinkGeneratorImpl::class);
        $this->app->bind(StoreRatingManager::class, StoreRatingManagerImpl::class);
    }
}
