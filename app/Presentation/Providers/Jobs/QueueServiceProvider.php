<?php

namespace App\Presentation\Providers\Jobs;

use App\Presentation\Console\Commands\Contracts\CommandsDictionary;
use App\Presentation\Jobs\Import\Deals\ImportStoreDealsJob;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class QueueServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::before(static function (JobProcessing $event) {
            // $event->connectionName
            // $event->job
            // $event->job->payload()
        });

        Queue::after(function (JobProcessed $event) {
            // TODO: create handling with factory
            if (is_a($event->job->resolveName(), ImportStoreDealsJob::class, true)) {
                /** @var Kernel $kernel */
                $kernel = $this->app->make(Kernel::class);
                $kernel->call(CommandsDictionary::DEALS_FILTERS_CACHE_UPDATE);
            }
        });
    }
}
