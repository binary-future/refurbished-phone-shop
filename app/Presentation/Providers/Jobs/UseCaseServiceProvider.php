<?php

namespace App\Presentation\Providers\Jobs;

use App\Application\UseCases\Import\Contracts\StoreDealsImportCase;
use App\Application\UseCases\Import\StoreDealsImportCase as StoreDealsImportCaseImpl;
use Illuminate\Support\ServiceProvider;

class UseCaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(StoreDealsImportCase::class, StoreDealsImportCaseImpl::class);
    }
}
