<?php

namespace App\Presentation\Providers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Presentation\Http\Controllers\Frontend';

    protected $adminNamespace = 'App\Presentation\Http\Controllers\Backend';

    private $routeFilesWithoutNamespace = [
        'networks.php',
        'payment-methods.php',
    ];

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapAdminRoutes();
        $this->mapWebRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        foreach (File::allFiles(base_path('routes/site')) as $route) {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group($route->getPathname());
        }
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

    /**
     *
     */
    protected function mapAdminRoutes()
    {
        foreach (File::allFiles(base_path('routes/admin')) as $route) {
            if (! in_array($route->getFilename(), $this->routeFilesWithoutNamespace)) {
                Route::middleware('web')
                    ->prefix('admin')
                    ->namespace($this->adminNamespace)
                    ->group($route->getPathname());
            } else {
                Route::middleware('web')
                    ->prefix('admin')
                    ->group($route->getPathname());
            }
        }
    }
}
