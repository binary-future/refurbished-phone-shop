<?php


namespace App\Application\UseCases\Store;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Store\Contracts\StoreViewCase as Contract;
use App\Application\UseCases\Store\Responses\StoreViewResponse;
use App\Domain\Phone\Phone\Query\PhonesWithDealsByStoreQuery;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class StoreViewCase
 * @package App\Application\UseCases\Store
 */
final class StoreViewCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreViewCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param string $storeSlug
     * @return StoreViewResponse
     */
    public function execute(string $storeSlug): StoreViewResponse
    {
        $store = $this->getStore($storeSlug);
        $phones = $this->getPhones($store);

        return new StoreViewResponse($store, $phones);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        /**
         * @var Store $store
         */
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug, ['image']))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    private function getPhones(Store $store): Collection
    {
        /**
         * @var Collection $phones
         */
        $phones = $this->queryBus->dispatch(
            new PhonesWithDealsByStoreQuery(
                $store, ['color', 'model', 'model.brand', 'images']
            )
        );

        return $phones;
    }
}
