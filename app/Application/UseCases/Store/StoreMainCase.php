<?php


namespace App\Application\UseCases\Store;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Store\Contracts\StoreMainCase as Contract;
use App\Application\UseCases\Store\Responses\StoreMainResponse;
use App\Domain\Store\Query\ActiveQuery;
use Illuminate\Support\Collection;

/**
 * Class StoreMainCase
 * @package App\Application\UseCases\Store
 */
final class StoreMainCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreMainCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return StoreMainResponse
     */
    public function execute(): StoreMainResponse
    {
        return new StoreMainResponse($this->getStores());
    }

    /**
     * @return Collection
     */
    private function getStores(): Collection
    {
        /**
         * @var Collection $stores
         */
        $stores = $this->queryBus->dispatch(new ActiveQuery(['image', 'link']));

        return $stores;
    }
}
