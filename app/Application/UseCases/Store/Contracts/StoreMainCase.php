<?php


namespace App\Application\UseCases\Store\Contracts;


use App\Application\UseCases\Store\Responses\StoreMainResponse;

/**
 * Interface StoreMainCase
 * @package App\Application\UseCases\Store\Contracts
 */
interface StoreMainCase
{
    /**
     * @return StoreMainResponse
     */
    public function execute(): StoreMainResponse;
}