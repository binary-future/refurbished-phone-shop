<?php


namespace App\Application\UseCases\Store\Contracts;


use App\Application\UseCases\Store\Responses\StoreViewResponse;

/**
 * Interface StoreViewCase
 * @package App\Application\UseCases\Store\Contracts
 */
interface StoreViewCase
{
    /**
     * @param string $storeSlug
     * @return StoreViewResponse
     */
    public function execute(string $storeSlug): StoreViewResponse;
}