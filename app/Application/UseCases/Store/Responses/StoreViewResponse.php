<?php


namespace App\Application\UseCases\Store\Responses;


use App\Domain\Store\Store;
use Illuminate\Support\Collection;

/**
 * Class StoreViewResponse
 * @package App\Application\UseCases\Store\Responses
 */
final class StoreViewResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var Collection
     */
    private $phones;

    /**
     * StoreViewResponse constructor.
     * @param Store $store
     * @param Collection $phones
     */
    public function __construct(Store $store, Collection $phones)
    {
        $this->store = $store;
        $this->phones = $phones;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }
}
