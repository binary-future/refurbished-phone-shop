<?php


namespace App\Application\UseCases\Store\Responses;


use Illuminate\Support\Collection;

/**
 * Class StoreMainResponse
 * @package App\Application\UseCases\Store\Responses
 */
final class StoreMainResponse
{
    /**
     * @var Collection
     */
    private $stores;

    /**
     * StoreMainResponse constructor.
     * @param Collection $stores
     */
    public function __construct(Collection $stores)
    {
        $this->stores = $stores;
    }

    /**
     * @return Collection
     */
    public function getStores(): Collection
    {
        return $this->stores;
    }
}
