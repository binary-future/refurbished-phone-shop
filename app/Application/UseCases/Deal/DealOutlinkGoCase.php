<?php

namespace App\Application\UseCases\Deal;

use App\Application\Services\Affiliation\Contracts\Generator;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Deal\Contracts\DealOutlinkGoCase as Contract;
use App\Application\UseCases\Deal\Responses\DealOutlinkGoResponse;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\ByKeyQuery;
use App\Domain\Shared\Link\Link;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class DealOutlinkCase
 * @package App\Application\UseCases\Deal
 */
final class DealOutlinkGoCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Generator
     */
    private $affiliationGenerator;

    /**
     * DealOutlinkCase constructor.
     * @param QueryBus $queryBus
     * @param Generator $generator
     */
    public function __construct(QueryBus $queryBus, Generator $generator)
    {
        $this->queryBus = $queryBus;
        $this->affiliationGenerator = $generator;
    }

    /**
     * @param int $dealKey
     * @param string $referer
     * @return DealOutlinkGoResponse
     */
    public function execute(int $dealKey, ?string $referer = null): DealOutlinkGoResponse
    {
        $deal = $this->getDeal($dealKey);
        $link = $this->getLink($deal, $referer);

        return new DealOutlinkGoResponse($deal, $link);
    }

    /**
     * @param int $dealKey
     * @return Deal
     */
    private function getDeal(int $dealKey): Deal
    {
        $deal = $this->queryBus->dispatch(new ByKeyQuery($dealKey))->first();
        if (! $deal) {
            throw (new ModelNotFoundException())
                ->setModel(Deal::class);
        }

        return $deal;
    }

    /**
     * @param Deal $deal
     * @param string $referrer
     * @return Link
     */
    private function getLink(Deal $deal, ?string $referrer = null): Link
    {
        $link = $deal->getLink();
        if (! $link) {
            throw (new ModelNotFoundException())
                ->setModel(Link::class);
        }

        $link->setLink($this->affiliationGenerator->generate($deal, $referrer));

        return $link;
    }
}
