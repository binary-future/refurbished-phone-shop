<?php

namespace App\Application\UseCases\Deal\Requests;

final class HardDeleteOldDealsRequest
{
    /**
     * @var int
     */
    private $hours;

    /**
     * HardDeleteOldDealsRequest constructor.
     * @param int $hours
     */
    public function __construct(int $hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return int
     */
    public function getHours(): int
    {
        return $this->hours;
    }
}
