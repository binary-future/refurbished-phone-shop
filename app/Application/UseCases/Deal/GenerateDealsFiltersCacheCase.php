<?php

namespace App\Application\UseCases\Deal;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Filters\Deals\QueryAssemblers\FiltersSearchQueryAssembler;
use App\Application\UseCases\Deal\Contracts\GenerateDealsFiltersCacheCase as ClassContract;
use App\Application\UseCases\Deal\Responses\GenerateDealsFiltersCacheResponse;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\DealColumnByQuery;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\AllQuery;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\ByBrandIdWithDealsOrderedCostlyDealQuery;
use App\Utils\Adapters\Cache\Contracts\Cache;
use Illuminate\Support\Collection;

final class GenerateDealsFiltersCacheCase implements ClassContract
{
    private const CACHE_PREFIX = 'deal:filters:';
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var Cache
     */
    private $cache;
    /**
     * @var FiltersSearchQueryAssembler
     */
    private $assembler;

    private $filterFields = [
        Contract::FIELD_NETWORK_ID,
        Contract::FIELD_CONDITION,
        Deal::FIELD_MONTHLY_COST,
        Deal::FIELD_TOTAL_COST
    ];

    /**
     * HardDeleteOldDealsCase constructor.
     * @param QueryBus $queryBus
     * @param Cache $cache
     * @param FiltersSearchQueryAssembler $assembler
     */
    public function __construct(
        QueryBus $queryBus,
        Cache $cache,
        FiltersSearchQueryAssembler $assembler
    ) {
        $this->queryBus = $queryBus;
        $this->cache = $cache;
        $this->assembler = $assembler;
    }

    /**
     * @return GenerateDealsFiltersCacheResponse[]
     */
    public function execute(): iterable
    {
        try {
            foreach ($this->proceedBrands() as $item) {
                yield $item;
            }
        } catch (\Throwable $exception) {
            yield $this->response(false, '', $exception);
        }
    }

    private function getBrands(): Collection
    {
        /** @var Collection $collection */
        $collection = $this->queryBus->dispatch(new AllQuery());

        return $collection;
    }

    private function proceedBrands(): iterable
    {
        foreach ($this->getBrands() as $brand) {
            $success = true;

            /** @var $brand Brand */
            try {
                $this->proceedBrand($brand);
            } catch (\Throwable $exception) {
                $success = false;
                yield $this->response(false, $brand->getSlug(), $exception);
            }

            if ($success) {
                yield $this->response(true, $brand->getSlug());
            }
        }
    }

    private function proceedBrand(Brand $brand)
    {
        $filters = $this->generateFilters($brand);

        $key = self::CACHE_PREFIX . $brand->getSlug();
        $this->cache->forever($key, $filters);
    }

    /**
     * Copied and refactored code from DealFiltersSearcher.
     * Ideally needs to be refactored and moved to another component
     * @param Brand $brand
     * @return array
     */
    private function generateFilters(Brand $brand): array
    {
        $products = $this->getProducts($brand);
        $query = $this->assembler->buildQuery(['products' => $products]);

        $filters = [];

        foreach ($this->filterFields as $field) {
            $filters[$field] = $this->dealsValues(clone $query, $field);
        }

        return $filters;
    }

    private function getProducts(Brand $brand): Collection
    {
        $models = $this->getModels($brand->getKey());
        return $this->getPhones($models);
    }

    private function response(
        bool $success,
        string $brand,
        \Throwable $throwable = null
    ): GenerateDealsFiltersCacheResponse {
        return new GenerateDealsFiltersCacheResponse($success, $brand, $throwable);
    }

    /**
     * @param int $brandId
     * @return Collection
     */
    private function getModels(int $brandId): Collection
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch(
            new ByBrandIdWithDealsOrderedCostlyDealQuery($brandId, [
                PhoneModel::RELATION_PHONES
            ])
        );

        return $models;
    }

    /**
     * @param Collection $models
     * @return Collection
     */
    private function getPhones(Collection $models): Collection
    {
        $phones = collect();
        foreach ($models as $model) {
            /**
             * @var PhoneModel $model
             */
            $phones = $phones->merge($model->getPhones());
        }

        return $phones;
    }

    /**
     * @param FiltersSearchQuery $query
     * @param string $value
     * @return array
     */
    private function dealsValues(FiltersSearchQuery $query, string $value): array
    {
        /**
         * @var Collection $deals
         */
        $dealsFilters = $this->queryBus->dispatch(new DealColumnByQuery($query, $value));

        return $dealsFilters->toArray();
    }
}
