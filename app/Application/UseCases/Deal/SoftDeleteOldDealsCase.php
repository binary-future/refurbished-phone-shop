<?php

namespace App\Application\UseCases\Deal;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Query\LastOwnerPositiveReportQuery;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Deal\Contracts\SoftDeleteOldDealsCase as Contract;
use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Domain\Deals\Deal\Command\DeleteDealsByStoreAndDateCommand;
use App\Domain\Store\Query\AllQuery;
use App\Domain\Store\Store;

final class SoftDeleteOldDealsCase implements Contract
{
    private const MAX_NOT_UPDATED_PERIOD_IN_DAYS = 1;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var PositiveReports
     */
    private $positiveReports;

    /**
     * SoftDeleteOldDealsCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     * @param PositiveReports $positiveReports
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus, PositiveReports $positiveReports)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->positiveReports = $positiveReports;
    }

    /**\
     * @return SoftDealsClearResponse[]
     */
    public function execute(): array
    {
        $stores = $this->getStores();
        $result = [];
        foreach ($stores as $store) {
            /**
             * @var Store $store
             */
            $result[] = $this->removeStoreDeals($store);
        }

        return $result;
    }

    private function getStores()
    {
        return $this->queryBus->dispatch(new AllQuery());
    }

    private function removeStoreDeals(Store $store): SoftDealsClearResponse
    {
        $lastSucceedReport = $this->getLastSucceedReport($store);
        if (! $lastSucceedReport) {
            return $this->generateErrorResponse($store);
        }

        return $this->removeDeals($store, $lastSucceedReport);
    }

    private function getLastSucceedReport(Store $store): ?PositiveReport
    {
        return $this->positiveReports->findBy(
            new LastOwnerPositiveReportQuery(
                $store->getOwnerKey(),
                $store->getOwnerType()
            )
        )->first();
    }

    private function removeDeals(Store $store, PositiveReport $report): SoftDealsClearResponse
    {
        try {
            /**
             * @var SoftDealsClearResponse $response
             */
            $response = $this->commandBus->dispatch(
                new DeleteDealsByStoreAndDateCommand(
                    $store,
                    $report->getCreatedAt()->subDays(self::MAX_NOT_UPDATED_PERIOD_IN_DAYS)
                )
            );
            return $response;
        } catch (\Throwable $exception) {
            return $this->generateErrorResponse($store);
        }
    }

    private function generateErrorResponse(Store $store): SoftDealsClearResponse
    {
        $response = new SoftDealsClearResponse($store);
        $response->setError(true);

        return $response;
    }
}
