<?php


namespace App\Application\UseCases\Deal;


use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Deal\Contracts\PhoneDealCase as Contract;
use App\Application\UseCases\Deal\Responses\PhoneDealResponse;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\ByKeyQuery;
use App\Domain\Deals\Deal\Query\RelatedDealsQuery;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class PhoneDealCase
 * @package App\Application\UseCases\Deal
 */
final class PhoneDealCase implements Contract
{
    private const RELATED_DEALS_LIMIT = 10;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * PhoneDealCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param int $dealId
     * @return PhoneDealResponse
     */
    public function execute(int $dealId): PhoneDealResponse
    {
        $deal = $this->getDeal($dealId);
        $phone = $this->getPhone($deal);
        $relatedDeals = $this->getRelatedDeals($deal);

        return new PhoneDealResponse($deal, $phone, $relatedDeals);
    }

    /**
     * @param int $dealId
     * @return Deal
     */
    private function getDeal(int $dealId): Deal
    {
        $deal = $this->queryBus->dispatch(new ByKeyQuery($dealId))->first();
        if (! $deal) {
            throw new ModelNotFoundException();
        }

        return $deal;
    }

    /**
     * @param Deal $deal
     * @return \App\Domain\Deals\Deal\Contracts\Product|mixed
     */
    private function getPhone(Deal $deal)
    {
        $phone = $deal->getProduct();
        if (! $phone instanceof Phone || ! $phone->isActive()) {
            throw new ModelNotFoundException();
        }

        return $phone;
    }

    /**
     * @param Deal $deal
     * @return Collection
     */
    private function getRelatedDeals(Deal $deal): Collection
    {
        /**
         * @var Collection $deals
         */
        $deals = $this->queryBus->dispatch(new RelatedDealsQuery($deal, self::RELATED_DEALS_LIMIT));

        return $deals;
    }
}
