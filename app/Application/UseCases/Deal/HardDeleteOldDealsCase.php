<?php

namespace App\Application\UseCases\Deal;

use App\Application\Services\Bus\CommandBus;
use App\Application\UseCases\Deal\Contracts\HardDeleteOldDealsCase as Contract;
use App\Application\UseCases\Deal\Requests\HardDeleteOldDealsRequest;
use App\Application\UseCases\Deal\Responses\HardDealsClearResponse;
use App\Domain\Deals\Deal\Command\DeleteDealsOlderThanDateCommand;
use App\Domain\Deals\Deal\DomainResponse\DeleteDealsDomainResponse;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use Carbon\Carbon;

final class HardDeleteOldDealsCase implements Contract
{
    /**
     * @var CommandBus
     */
    private $commandBus;
    /**
     * @var ChannelLogger
     */
    private $channelLogger;

    /**
     * HardDeleteOldDealsCase constructor.
     * @param CommandBus $commandBus
     * @param ChannelLogger $channelLogger
     */
    public function __construct(CommandBus $commandBus, ChannelLogger $channelLogger)
    {
        $this->commandBus = $commandBus;
        $this->channelLogger = $channelLogger;
    }

    /**
     * @param HardDeleteOldDealsRequest $request
     * @return HardDealsClearResponse
     */
    public function execute(HardDeleteOldDealsRequest $request): HardDealsClearResponse
    {
        return $this->removeDeals($request->getHours());
    }

    private function removeDeals(int $hours): HardDealsClearResponse
    {
        try {
            /**
             * @var DeleteDealsDomainResponse $response
             */
            $response = $this->commandBus->dispatch(
                new DeleteDealsOlderThanDateCommand(
                    Carbon::now()->subHours($hours)
                )
            );
            return new HardDealsClearResponse(
                $response->getDeletedDeals(),
                $response->getDeletedLinks()
            );
        } catch (\Throwable $exception) {
            $this->log($exception);
            return $this->generateErrorResponse();
        }
    }

    private function log(\Throwable $exception)
    {
        $this->channelLogger
            ->channel(ChannelLogger::CHANNEL_DEALS_DELETE)
            ->error($exception);
    }

    private function generateErrorResponse(): HardDealsClearResponse
    {
        $response = new HardDealsClearResponse();
        $response->setError(true);

        return $response;
    }
}
