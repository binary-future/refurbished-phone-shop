<?php

namespace App\Application\UseCases\Deal;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Query\GetInfoByApiNameQuery;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\UseCases\Deal\Contracts\ClearApiDealsCase as Contract;
use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;
use App\Domain\Deals\Deal\Command\DeleteDealsByStoreAndDateCommand;
use App\Domain\Store\Store;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class ClearApiDealsCase
 * @package App\Application\UseCases\Deal
 */
final class ClearApiDealsCase implements Contract
{
    /**
     * @var DatafeedsInfo
     */
    private $datafeeds;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ClearApiDealsCase constructor.
     * @param DatafeedsInfo $datafeeds
     * @param CommandBus $commandBus
     */
    public function __construct(DatafeedsInfo $datafeeds, CommandBus $commandBus)
    {
        $this->datafeeds = $datafeeds;
        $this->commandBus = $commandBus;
    }

    /**
     * @param string $api
     * @return SoftDealsClearResponse[]
     */
    public function execute(string $api): array
    {
        $datafeeds = $this->getDatafeeds($api);
        $result = [];
        foreach ($datafeeds as $datafeed) {
            /**
             * @var DatafeedInfo $datafeed
             */
            $result[] = $this->removeDealsByStore($datafeed->getStore());
        }

        return $result;
    }

    private function getDatafeeds(string $api): Collection
    {
        return $this->datafeeds->findBy(new GetInfoByApiNameQuery($api, ['store']));
    }

    private function removeDealsByStore(Store $store): SoftDealsClearResponse
    {
        try {
            /**
             * @var SoftDealsClearResponse $response
             */
            $response = $this->commandBus->dispatch(
                new DeleteDealsByStoreAndDateCommand(
                    $store,
                    Carbon::now()
                )
            );
            return $response;
        } catch (\Throwable $exception) {
            $response = new SoftDealsClearResponse($store);
            $response->setError(true);
            return $response;
        }
    }
}
