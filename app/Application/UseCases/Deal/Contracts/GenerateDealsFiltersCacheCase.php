<?php

namespace App\Application\UseCases\Deal\Contracts;


use App\Application\UseCases\Deal\Responses\GenerateDealsFiltersCacheResponse;

interface GenerateDealsFiltersCacheCase
{
    /**
     * @return GenerateDealsFiltersCacheResponse[]
     */
    public function execute(): iterable;
}
