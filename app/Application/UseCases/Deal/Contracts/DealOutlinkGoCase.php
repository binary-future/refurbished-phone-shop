<?php

namespace App\Application\UseCases\Deal\Contracts;

use App\Application\UseCases\Deal\Responses\DealOutlinkGoResponse;

/**
 * Interface DealOutliinkCase
 * @package App\Application\UseCases\Deal\Contracts
 */
interface DealOutlinkGoCase
{
    /**
     * @param int $dealKey
     * @param string $referer
     * @return DealOutlinkGoResponse
     */
    public function execute(int $dealKey, string $referer = null): DealOutlinkGoResponse;
}
