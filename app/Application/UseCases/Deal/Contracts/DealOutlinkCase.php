<?php

namespace App\Application\UseCases\Deal\Contracts;

use App\Application\UseCases\Deal\Responses\DealOutlinkResponse;

/**
 * Class DealOutlinkCase
 * @package App\Application\UseCases\Deal
 */
interface DealOutlinkCase
{
    /**
     * @param int $dealKey
     * @return DealOutlinkResponse
     */
    public function execute(int $dealKey): DealOutlinkResponse;
}
