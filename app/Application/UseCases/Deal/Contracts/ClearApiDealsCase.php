<?php


namespace App\Application\UseCases\Deal\Contracts;

use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;

/**
 * Interface ClearApiDealsCase
 * @package App\Application\UseCases\Deal\Contracts
 */
interface ClearApiDealsCase
{
    /**
     * @param string $api
     * @return SoftDealsClearResponse[]
     */
    public function execute(string $api): array;
}
