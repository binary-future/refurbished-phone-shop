<?php

namespace App\Application\UseCases\Deal\Contracts;

use App\Application\UseCases\Deal\Responses\SoftDealsClearResponse;

/**
 * Interface SoftDeleteOldDealsCase
 * @package App\Application\UseCases\Deal\Contracts
 */
interface SoftDeleteOldDealsCase
{
    /**
     * @return SoftDealsClearResponse[]
     */
    public function execute(): array;
}
