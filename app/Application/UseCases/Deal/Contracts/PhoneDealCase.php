<?php

namespace App\Application\UseCases\Deal\Contracts;

use App\Application\UseCases\Deal\Responses\PhoneDealResponse;

/**
 * Interface PhoneDealCase
 * @package App\Application\UseCases\Deal\Contracts
 */
interface PhoneDealCase
{
    public function execute(int $dealId): PhoneDealResponse;
}