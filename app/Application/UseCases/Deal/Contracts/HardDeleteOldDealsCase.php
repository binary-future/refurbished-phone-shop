<?php

namespace App\Application\UseCases\Deal\Contracts;

use App\Application\UseCases\Deal\Requests\HardDeleteOldDealsRequest;
use App\Application\UseCases\Deal\Responses\HardDealsClearResponse;

/**
 * Interface HardDeleteOldDealsCase
 * @package App\Application\UseCases\Deal\Contracts
 */
interface HardDeleteOldDealsCase
{
    /**
     * @param HardDeleteOldDealsRequest $request
     * @return HardDealsClearResponse
     */
    public function execute(HardDeleteOldDealsRequest $request): HardDealsClearResponse;
}
