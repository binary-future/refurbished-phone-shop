<?php

namespace App\Application\UseCases\Deal;

use App\Application\Services\Affiliation\Contracts\Generator;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Deal\Contracts\DealOutlinkCase as Contract;
use App\Application\UseCases\Deal\Responses\DealOutlinkResponse;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\ByKeyQuery;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class DealOutlinkCase
 * @package App\Application\UseCases\Deal
 */
final class DealOutlinkCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * DealOutlinkCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param int $dealKey
     * @return DealOutlinkResponse
     */
    public function execute(int $dealKey): DealOutlinkResponse
    {
        $store = $this->getStore($dealKey);

        return new DealOutlinkResponse($store);
    }

    /**
     * @param int $dealKey
     * @return Store
     */
    private function getStore(int $dealKey): Store
    {
        /** @var Deal|null $deal */
        $deal = $this->queryBus->dispatch(new ByKeyQuery($dealKey, ['store']))->first();
        if (! $deal) {
            throw (new ModelNotFoundException())
                ->setModel(Deal::class);
        }

        return $deal->getStore();
    }
}
