<?php

namespace App\Application\UseCases\Deal\Responses;

final class HardDealsClearResponse
{
    /**
     * @var int
     */
    private $deletedDeals;

    /**
     * @var bool
     */
    private $error = false;
    /**
     * @var int
     */
    private $deletedLinks;

    /**
     * HardDealsClearResponse constructor.
     * @param int $deletedDeals
     * @param int $deletedLinks
     */
    public function __construct(int $deletedDeals = 0, int $deletedLinks = 0)
    {
        $this->deletedDeals = $deletedDeals;
        $this->deletedLinks = $deletedLinks;
    }

    /**
     * @return int
     */
    public function getDeletedLinks(): int
    {
        return $this->deletedLinks;
    }

    /**
     * @return int
     */
    public function getDeletedDeals(): int
    {
        return $this->deletedDeals;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error;
    }

    /**
     * @param bool $error
     */
    public function setError(bool $error): void
    {
        $this->error = $error;
    }
}
