<?php

namespace App\Application\UseCases\Deal\Responses;

use App\Domain\Deals\Deal\Deal;
use App\Domain\Shared\Link\Link;

/**
 * Class DealOutlinkGoResponse
 * @package App\Application\UseCases\Deal\Responses
 */
final class DealOutlinkGoResponse
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var Link
     */
    private $link;

    /**
     * DealOutlinkResponse constructor.
     * @param Deal $deal
     * @param Link $link
     */
    public function __construct(Deal $deal, Link $link)
    {
        $this->deal = $deal;
        $this->link = $link;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @return Link
     */
    public function getLink(): Link
    {
        return $this->link;
    }
}
