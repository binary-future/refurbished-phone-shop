<?php


namespace App\Application\UseCases\Deal\Responses;


final class GenerateDealsFiltersCacheResponse
{
    /**
     * @var bool
     */
    private $success;
    /**
     * @var string
     */
    private $brand;
    /**
     * @var \Throwable|null
     */
    private $throwable;

    /**
     * GenerateDealsFiltersCacheResponse constructor.
     * @param bool $success
     * @param string $brand
     * @param \Throwable|null $throwable
     */
    public function __construct(bool $success, string $brand, \Throwable $throwable = null)
    {
        $this->success = $success;
        $this->brand = $brand;
        $this->throwable = $throwable;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return \Throwable|null
     */
    public function getThrowable(): ?\Throwable
    {
        return $this->throwable;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}
