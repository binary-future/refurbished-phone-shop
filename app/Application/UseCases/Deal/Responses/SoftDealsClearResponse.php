<?php

namespace App\Application\UseCases\Deal\Responses;

use App\Domain\Store\Store;

final class SoftDealsClearResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var int
     */
    private $success;

    /**
     * @var int
     */
    private $failed;

    /**
     * @var bool
     */
    private $error = false;

    /**
     * DeleteOldDealsResponse constructor.
     * @param Store $store
     * @param int $success
     * @param int $failed
     */
    public function __construct(Store $store, int $success = 0, int $failed = 0)
    {
        $this->store = $store;
        $this->success = $success;
        $this->failed = $failed;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return int
     */
    public function getSuccess(): int
    {
        return $this->success;
    }

    /**
     * @return int
     */
    public function getFailed(): int
    {
        return $this->failed;
    }

    /**
     * @return bool
     */
    public function isError(): bool
    {
        return $this->error;
    }

    /**
     * @param bool $error
     */
    public function setError(bool $error): void
    {
        $this->error = $error;
    }
}
