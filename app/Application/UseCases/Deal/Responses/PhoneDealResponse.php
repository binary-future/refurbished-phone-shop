<?php


namespace App\Application\UseCases\Deal\Responses;


use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Support\Collection;

/**
 * Class PhoneDealResponse
 * @package App\Application\UseCases\Deal\Responses
 */
final class PhoneDealResponse
{
    /**
     * @var Deal
     */
    private $deal;

    /**
     * @var Phone
     */
    private $phone;

    /**
     * @var Collection
     */
    private $relatedDeals;

    /**
     * PhoneDealResponse constructor.
     * @param Deal $deal
     * @param Phone $phone
     * @param Collection $relatedDeals
     */
    public function __construct(Deal $deal, Phone $phone, Collection $relatedDeals)
    {
        $this->deal = $deal;
        $this->phone = $phone;
        $this->relatedDeals = $relatedDeals;
    }

    /**
     * @return Deal
     */
    public function getDeal(): Deal
    {
        return $this->deal;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @return Collection
     */
    public function getRelatedDeals(): Collection
    {
        return $this->relatedDeals;
    }
}
