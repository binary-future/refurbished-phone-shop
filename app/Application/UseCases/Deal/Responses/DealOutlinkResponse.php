<?php

namespace App\Application\UseCases\Deal\Responses;

use App\Domain\Store\Store;

/**
 * Class DealOutlinkResponse
 * @package App\Application\UseCases\Deal\Responses
 */
final class DealOutlinkResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var string
     */
    private $urlToGo;

    /**
     * DealOutlinkResponse constructor.
     * @param Store $store
     */
    public function __construct(Store $store)
    {
        $this->store = $store;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @param string|null $urlToGo
     * @return DealOutlinkResponse
     */
    public function setUrlToGo(?string $urlToGo): DealOutlinkResponse
    {
        $this->urlToGo = $urlToGo;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUrlToGo(): ?string
    {
        return $this->urlToGo;
    }
}
