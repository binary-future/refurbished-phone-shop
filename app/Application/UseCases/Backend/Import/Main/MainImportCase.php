<?php


namespace App\Application\UseCases\Backend\Import\Main;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Plan\Query\LastPlannedImportsQuery;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Main\Contracts\MainImportCase as Contract;
use App\Application\UseCases\Backend\Import\Main\Responses\MainImportResponse;
use App\Domain\Phone\Model\Query\AllActiveWithoutDealsQuery;
use App\Domain\Phone\Model\Query\AllInActiveQuery;
use Illuminate\Support\Collection;

/**
 * Class MainImportCase
 * @package App\Application\UseCases\Backend\Import\Main
 */
final class MainImportCase implements Contract
{
    private const MODELS_LIMIT = 10;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var PlannedImports
     */
    private $plannedImports;

    /**
     * MainImportCase constructor.
     * @param QueryBus $queryBus
     * @param PlannedImports $plannedImports
     */
    public function __construct(QueryBus $queryBus, PlannedImports $plannedImports)
    {
        $this->queryBus = $queryBus;
        $this->plannedImports = $plannedImports;
    }

    /**
     * @return MainImportResponse
     */
    public function execute(): MainImportResponse
    {
        return new MainImportResponse(
            $this->getInactiveModels(),
            $this->getModelsWithoutDeals(),
            $this->getPlannedImport()
        );
    }

    /**
     * @return Collection
     */
    private function getInactiveModels(): Collection
    {
        $query = new AllInActiveQuery(['brand']);
        $query->setLimit(self::MODELS_LIMIT);
        /**
         * @var Collection $inactiveModels
         */
        $inactiveModels =  $this->queryBus->dispatch($query);

        return $inactiveModels;
    }

    /**
     * @return Collection
     */
    private function getModelsWithoutDeals(): Collection
    {
        $query = new AllActiveWithoutDealsQuery(['brand']);
        $query->setLimit(self::MODELS_LIMIT);
        /**
         * @var Collection $inactiveModels
         */
        $inactiveModels =  $this->queryBus->dispatch($query);

        return $inactiveModels;
    }


    /**
     * @return Collection
     */
    private function getPlannedImport(): Collection
    {
        return $this->plannedImports->findBy(new LastPlannedImportsQuery(self::MODELS_LIMIT));
    }
}
