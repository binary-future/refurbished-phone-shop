<?php


namespace App\Application\UseCases\Backend\Import\Main\Contracts;


use App\Application\UseCases\Backend\Import\Main\Responses\MainImportResponse;

/**
 * Interface MainImportCase
 * @package App\Application\UseCases\Backend\Import\Main\Contracts
 */
interface MainImportCase
{
    /**
     * @return MainImportResponse
     */
    public function execute(): MainImportResponse;
}
