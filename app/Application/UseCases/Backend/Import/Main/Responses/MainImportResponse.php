<?php

namespace App\Application\UseCases\Backend\Import\Main\Responses;

use Illuminate\Support\Collection;

/**
 * Class MainImportResponse
 * @package App\Application\UseCases\Backend\Import\Main\Responses
 */
final class MainImportResponse
{
    /**
     * @var Collection
     */
    private $inActiveModels;

    /**
     * @var Collection
     */
    private $modelsWithoutDeals;

    /**
     * @var Collection
     */
    private $plannedImports;

    /**
     * MainImportResponse constructor.
     * @param Collection $inActiveModels
     * @param Collection $modelsWithoutDeals
     * @param Collection $plannedImports
     */
    public function __construct(Collection $inActiveModels, Collection $modelsWithoutDeals, Collection $plannedImports)
    {
        $this->inActiveModels = $inActiveModels;
        $this->modelsWithoutDeals = $modelsWithoutDeals;
        $this->plannedImports = $plannedImports;
    }

    /**
     * @return Collection
     */
    public function getInActiveModels(): Collection
    {
        return $this->inActiveModels;
    }

    /**
     * @return Collection
     */
    public function getModelsWithoutDeals(): Collection
    {
        return $this->modelsWithoutDeals;
    }

    /**
     * @return Collection
     */
    public function getPlannedImports(): Collection
    {
        return $this->plannedImports;
    }
}
