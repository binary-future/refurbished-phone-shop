<?php


namespace App\Application\UseCases\Backend\Import\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Import\Model\Contracts\WithoutDealsCase as Contract;
use App\Application\UseCases\Backend\Import\Model\Responses\WithoutDealsResponse;
use App\Domain\Phone\Model\Query\AllActiveWithoutDealsQuery;
use Illuminate\Support\Collection;

/**
 * Class WithoutDealsCase
 * @package App\Application\UseCases\Backend\Import\Model
 */
final class WithoutDealsCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * WithoutDealsCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return WithoutDealsResponse
     */
    public function execute(): WithoutDealsResponse
    {
        return new WithoutDealsResponse(
            $this->getModels()
        );
    }

    /**
     * @return Collection
     */
    private function getModels(): Collection
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch(new AllActiveWithoutDealsQuery(['brand', 'phones', 'phones.images']));

        return $models;
    }
}
