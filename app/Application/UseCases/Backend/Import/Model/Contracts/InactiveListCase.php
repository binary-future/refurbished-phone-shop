<?php

namespace App\Application\UseCases\Backend\Import\Model\Contracts;

use App\Application\UseCases\Backend\Import\Model\Responses\InactiveListResponse;

/**
 * Interface InactiveListCase
 * @package App\Application\UseCases\Backend\Import\Model\Contracts
 */
interface InactiveListCase
{
    /**
     * @return InactiveListResponse
     */
    public function execute(): InactiveListResponse;
}