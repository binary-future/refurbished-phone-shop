<?php


namespace App\Application\UseCases\Backend\Import\Model\Contracts;


use App\Application\UseCases\Backend\Import\Model\Responses\WithoutDealsResponse;

/**
 * Interface WithoutDealsCase
 * @package App\Application\UseCases\Backend\Import\Model\Contracts
 */
interface WithoutDealsCase
{
    /**
     * @return WithoutDealsResponse
     */
    public function execute(): WithoutDealsResponse;
}