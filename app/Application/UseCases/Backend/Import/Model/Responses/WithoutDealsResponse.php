<?php


namespace App\Application\UseCases\Backend\Import\Model\Responses;

use Illuminate\Support\Collection;

/**
 * Class WithoutDealsResponse
 * @package App\Application\UseCases\Backend\Import\Model\Responses
 */
final class WithoutDealsResponse
{
    /**
     * @var Collection
     */
    private $models;

    /**
     * InactiveListResponse constructor.
     * @param Collection $models
     */
    public function __construct(Collection $models)
    {
        $this->models = $models;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }
}