<?php


namespace App\Application\UseCases\Backend\Import\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Import\Model\Contracts\InactiveListCase as Contract;
use App\Application\UseCases\Backend\Import\Model\Responses\InactiveListResponse;
use App\Domain\Phone\Model\Query\AllInActiveQuery;
use Illuminate\Support\Collection;

/**
 * Class InactiveListCase
 * @package App\Application\UseCases\Backend\Import\Model
 */
final class InactiveListCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * InactiveListCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return InactiveListResponse
     */
    public function execute(): InactiveListResponse
    {
        return new InactiveListResponse(
            $this->getModels()
        );
    }

    /**
     * @return Collection
     */
    private function getModels(): Collection
    {
        $query = new AllInActiveQuery(['brand', 'phones', 'phones.images']);
        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch($query);

        return $models;
    }
}
