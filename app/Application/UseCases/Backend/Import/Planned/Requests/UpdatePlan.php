<?php


namespace App\Application\UseCases\Backend\Import\Planned\Requests;


use App\Application\Services\Importer\Plan\PlannedImport;

/**
 * Class UpdatePlan
 * @package App\Application\UseCases\Backend\Import\Planned\Requests
 */
final class UpdatePlan
{
    /**
     * @var int
     */
    private $plan;

    /**
     * @var string
     */
    private $date;

    /**
     * UpdatePlan constructor.
     * @param int $plan
     * @param string $date
     */
    public function __construct(int $plan, string $date)
    {
        $this->plan = $plan;
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getPlan(): int
    {
        return $this->plan;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            PlannedImport::FIELD_IMPORT_DATE => $this->date
        ];
    }
}
