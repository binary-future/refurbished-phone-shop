<?php

namespace App\Application\UseCases\Backend\Import\Planned\Requests;

use App\Application\Services\Importer\Plan\PlannedImport;

/**
 * Class CreatePlan
 * @package App\Application\UseCases\Backend\Import\Planned\Requests
 */
final class CreatePlan
{
    /**
     * @var string $date
     */
    private $date;

    /**
     * CreatePlan constructor.
     * @param string $date
     */
    public function __construct(string $date)
    {
        $this->date = $date;
    }

    public function getParams(): array
    {
        return [
            PlannedImport::FIELD_IMPORT_DATE => $this->date
        ];
    }
}
