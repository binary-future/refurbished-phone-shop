<?php


namespace App\Application\UseCases\Backend\Import\Planned\Responses;


use App\Application\Services\Importer\Plan\PlannedImport;

/**
 * Class ImportPlanDeleted
 * @package App\Application\UseCases\Backend\Import\Planned\Responses
 */
final class ImportPlanDeleted
{
    /**
     * @var PlannedImport
     */
    private $plannedImport;

    /**
     * @var bool
     */
    private $success;

    /**
     * ImportPlanDeleted constructor.
     * @param PlannedImport $plannedImport
     * @param bool $success
     */
    public function __construct(PlannedImport $plannedImport, bool $success)
    {
        $this->plannedImport = $plannedImport;
        $this->success = $success;
    }

    /**
     * @return PlannedImport
     */
    public function getPlannedImport(): PlannedImport
    {
        return $this->plannedImport;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}
