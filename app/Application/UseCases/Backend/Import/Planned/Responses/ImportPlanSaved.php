<?php


namespace App\Application\UseCases\Backend\Import\Planned\Responses;


use App\Application\Services\Importer\Plan\PlannedImport;

/**
 * Class ImportPlanSaved
 * @package App\Application\UseCases\Backend\Import\Planned\Responses
 */
final class ImportPlanSaved
{
    /**
     * @var PlannedImport|null
     */
    private $plannedImport;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * ImportPlanSaved constructor.
     * @param PlannedImport|null $plannedImport
     * @param bool $success
     */
    public function __construct(PlannedImport $plannedImport = null, bool $success = true)
    {
        $this->plannedImport = $plannedImport;
        $this->success = $success;
    }

    /**
     * @return PlannedImport
     */
    public function getPlannedImport(): ?PlannedImport
    {
        return $this->plannedImport;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}
