<?php


namespace App\Application\UseCases\Backend\Import\Planned\Responses;


use Illuminate\Support\Collection;

/**
 * Class PlannedMainResponse
 * @package App\Application\UseCases\Backend\Import\Planned\Requests
 */
final class PlannedMainResponse
{
    /**
     * @var Collection
     */
    private $plannedImports;

    /**
     * PlannedMainResponse constructor.
     * @param Collection $plannedImports
     */
    public function __construct(Collection $plannedImports)
    {
        $this->plannedImports = $plannedImports;
    }

    /**
     * @return Collection
     */
    public function getPlannedImports(): Collection
    {
        return $this->plannedImports;
    }
}
