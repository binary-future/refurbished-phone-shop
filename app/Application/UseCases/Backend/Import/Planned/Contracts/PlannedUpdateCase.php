<?php


namespace App\Application\UseCases\Backend\Import\Planned\Contracts;


use App\Application\UseCases\Backend\Import\Planned\Requests\UpdatePlan;
use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanSaved;

interface PlannedUpdateCase
{
    public function execute(UpdatePlan $request): ImportPlanSaved;
}
