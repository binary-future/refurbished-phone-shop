<?php


namespace App\Application\UseCases\Backend\Import\Planned\Contracts;


use App\Application\UseCases\Backend\Import\Planned\Responses\PlannedMainResponse;

/**
 * Interface PlannedMainCase
 * @package App\Application\UseCases\Backend\Import\Planned\Contracts
 */
interface PlannedMainCase
{
    /**
     * @return PlannedMainResponse
     */
    public function execute(): PlannedMainResponse;
}