<?php


namespace App\Application\UseCases\Backend\Import\Planned\Contracts;


use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanDeleted;

/**
 * Interface PlannedDeleteCase
 * @package App\Application\UseCases\Backend\Import\Planned\Contracts
 */
interface PlannedDeleteCase
{
    /**
     * @param int $plan
     * @return ImportPlanDeleted
     */
    public function execute(int $plan): ImportPlanDeleted;
}
