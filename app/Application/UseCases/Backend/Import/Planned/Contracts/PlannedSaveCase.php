<?php


namespace App\Application\UseCases\Backend\Import\Planned\Contracts;


use App\Application\UseCases\Backend\Import\Planned\Requests\CreatePlan;
use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanSaved;

/**
 * Interface PlannedSaveCase
 * @package App\Application\UseCases\Backend\Import\Planned\Contracts
 */
interface PlannedSaveCase
{
    /**
     * @param CreatePlan $request
     * @return ImportPlanSaved
     */
    public function execute(CreatePlan $request): ImportPlanSaved;
}
