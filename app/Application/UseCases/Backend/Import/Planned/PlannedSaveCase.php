<?php

namespace App\Application\UseCases\Backend\Import\Planned;

use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedSaveCase as Contract;
use App\Application\UseCases\Backend\Import\Planned\Requests\CreatePlan;
use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanSaved;
use App\Utils\Serializer\Contracts\Serializer;

/**
 * Class PlannedSaveCase
 * @package App\Application\UseCases\Backend\Import\Planned
 */
final class PlannedSaveCase implements Contract
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var PlannedImports
     */
    private $plannedImports;

    /**
     * PlannedSaveCase constructor.
     * @param Serializer $serializer
     * @param PlannedImports $plannedImports
     */
    public function __construct(Serializer $serializer, PlannedImports $plannedImports)
    {
        $this->serializer = $serializer;
        $this->plannedImports = $plannedImports;
    }

    /**
     * @param CreatePlan $request
     * @return ImportPlanSaved
     */
    public function execute(CreatePlan $request): ImportPlanSaved
    {
        try {
            return $this->proceed($request);
        } catch (\Throwable $exception) {
            return $this->generateFailedResponse();
        }
    }

    /**
     * @param CreatePlan $request
     * @return ImportPlanSaved
     */
    private function proceed(CreatePlan $request): ImportPlanSaved
    {
        /**
         * @var PlannedImport $plannedImport
         */
        $plannedImport = $this->serializer->fromArray(PlannedImport::class, $request->getParams());
        $plannedImport->changeType(PlannedImport::TYPE_PHONES);

        return new ImportPlanSaved($this->plannedImports->save($plannedImport));
    }

    /**
     * @param PlannedImport|null $plannedImport
     * @return ImportPlanSaved
     */
    private function generateFailedResponse(PlannedImport $plannedImport = null): ImportPlanSaved
    {
        return new ImportPlanSaved($plannedImport, false);
    }
}
