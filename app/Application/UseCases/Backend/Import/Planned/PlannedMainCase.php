<?php

namespace App\Application\UseCases\Backend\Import\Planned;

use App\Application\Services\Importer\Plan\Query\AllOrderByDateQuery;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedMainCase as Contract;
use App\Application\UseCases\Backend\Import\Planned\Responses\PlannedMainResponse;

/**
 * Class PlannedMainCase
 * @package App\Application\UseCases\Backend\Import\Planned
 */
final class PlannedMainCase implements Contract
{
    /**
     * @var PlannedImports
     */
    private $plannedImports;

    /**
     * PlannedMainCase constructor.
     * @param PlannedImports $plannedImports
     */
    public function __construct(PlannedImports $plannedImports)
    {
        $this->plannedImports = $plannedImports;
    }

    /**
     * @return PlannedMainResponse
     */
    public function execute(): PlannedMainResponse
    {
        return new PlannedMainResponse($this->plannedImports->findBy(new AllOrderByDateQuery()));
    }
}
