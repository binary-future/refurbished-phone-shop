<?php

namespace App\Application\UseCases\Backend\Import\Planned;

use App\Application\Services\Importer\Plan\PlannedImport;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\UseCases\Backend\Import\Planned\Contracts\PlannedUpdateCase as Contract;
use App\Application\UseCases\Backend\Import\Planned\Requests\UpdatePlan;
use App\Application\UseCases\Backend\Import\Planned\Responses\ImportPlanSaved;
use App\Domain\Deals\Deal\Query\ByKeyQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class PlannedUpdateCase implements Contract
{
    /**
     * @var PlannedImports
     */
    private $imports;

    /**
     * PlannedUpdateCase constructor.
     * @param PlannedImports $imports
     */
    public function __construct(PlannedImports $imports)
    {
        $this->imports = $imports;
    }

    public function execute(UpdatePlan $request): ImportPlanSaved
    {
        $plannedImport = $this->getPlannedImport($request->getPlan());

        try {
            return $this->proceed($plannedImport, $request);
        } catch (\Throwable $exception) {
            return $this->generateFailedResponse($plannedImport);
        }
    }

    private function getPlannedImport(int $planId): PlannedImport
    {
        /**
         * @var PlannedImport $plannedImport
         */
        $plannedImport = $this->imports->findBy(new ByKeyQuery($planId))->first();
        if (! $plannedImport) {
            throw new ModelNotFoundException();
        }

        return $plannedImport;
    }

    /**
     * @param PlannedImport $plannedImport
     * @param UpdatePlan $request
     * @return ImportPlanSaved
     */
    private function proceed(PlannedImport $plannedImport, UpdatePlan $request): ImportPlanSaved
    {
        /**
         * @var PlannedImport $updatePlannedImport
         */
        $updatePlannedImport = $this->imports->update($plannedImport, $request->getParams());

        return new ImportPlanSaved($updatePlannedImport, true);
    }

    private function generateFailedResponse(PlannedImport $plannedImport): ImportPlanSaved
    {
        return new ImportPlanSaved($plannedImport, false);
    }
}
