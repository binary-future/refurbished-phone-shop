<?php

namespace App\Application\UseCases\Backend\Stores\Response;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Domain\Store\Store;
use Illuminate\Support\Collection;

final class StoreEditResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var Collection
     */
    private $affiliations;

    /**
     * @var Collection
     */
    private $api;

    /**
     * @var DatafeedInfo|null
     */
    private $datafeed;
    /**
     * @var string|null
     */
    private $datafeedDownloadLink;
    /**
     * @var Collection
     */
    private $paymentMethods;

    /**
     * StoreEditResponse constructor.
     * @param Store $store
     * @param Collection $affiliations
     * @param Collection $api
     * @param Collection $paymentMethods
     * @param DatafeedInfo|null $datafeed
     * @param string $datafeedDownloadLink
     */
    public function __construct(
        Store $store,
        Collection $affiliations,
        Collection $api,
        Collection $paymentMethods,
        DatafeedInfo $datafeed = null,
        ?string $datafeedDownloadLink = null
    ) {
        $this->store = $store;
        $this->affiliations = $affiliations;
        $this->api = $api;
        $this->datafeed = $datafeed;
        $this->datafeedDownloadLink = $datafeedDownloadLink;
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @return string
     */
    public function getDatafeedDownloadLink(): ?string
    {
        return $this->datafeedDownloadLink;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return Collection
     */
    public function getAffiliations(): Collection
    {
        return $this->affiliations;
    }

    /**
     * @return Collection
     */
    public function getApi(): Collection
    {
        return $this->api;
    }

    /**
     * @return Collection
     */
    public function getPaymentMethods(): Collection
    {
        return $this->paymentMethods;
    }

    /**
     * @return DatafeedInfo|null
     */
    public function getDatafeed(): ?DatafeedInfo
    {
        return $this->datafeed;
    }
}
