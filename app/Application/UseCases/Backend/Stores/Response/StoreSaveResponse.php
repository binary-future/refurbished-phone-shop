<?php


namespace App\Application\UseCases\Backend\Stores\Response;


use App\Domain\Store\Store;

/**
 * Class StoreSaveResponse
 * @package App\Application\UseCases\Backend\Stores\Response
 */
final class StoreSaveResponse
{
    /**
     * @var Store|null
     */
    private $store;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * StoreSaveResponse constructor.
     * @param Store|null $store
     */
    public function __construct(Store $store = null)
    {
        $this->store = $store;
    }

    /**
     * @return Store|null
     */
    public function getStore(): ?Store
    {
        return $this->store;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
