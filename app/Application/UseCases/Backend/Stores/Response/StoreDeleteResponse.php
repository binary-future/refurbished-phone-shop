<?php

namespace App\Application\UseCases\Backend\Stores\Response;

use App\Domain\Store\Store;

final class StoreDeleteResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * StoreDeleteResponse constructor.
     * @param Store $store
     */
    public function __construct(?Store $store)
    {
        $this->store = $store;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
