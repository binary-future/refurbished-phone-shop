<?php

namespace App\Application\UseCases\Backend\Stores\Response;

use Illuminate\Support\Collection;

/**
 * Class StoreCreateResponse
 * @package App\Application\UseCases\Backend\Stores\Response
 */
final class StoreCreateResponse
{
    /**
     * @var Collection
     */
    private $affiliations;

    /**
     * @var Collection
     */
    private $apiList;
    /**
     * @var Collection
     */
    private $paymentMethods;

    /**
     * StoreCreateResponse constructor.
     * @param Collection $affiliations
     * @param Collection $apiList
     * @param Collection $paymentMethods
     */
    public function __construct(Collection $affiliations, Collection $apiList, Collection $paymentMethods)
    {
        $this->affiliations = $affiliations;
        $this->apiList = $apiList;
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @return Collection
     */
    public function getAffiliations(): Collection
    {
        return $this->affiliations;
    }

    /**
     * @return Collection
     */
    public function getApiList(): Collection
    {
        return $this->apiList;
    }

    /**
     * @return Collection
     */
    public function getPaymentMethods(): Collection
    {
        return $this->paymentMethods;
    }
}
