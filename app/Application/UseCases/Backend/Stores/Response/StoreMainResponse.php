<?php

namespace App\Application\UseCases\Backend\Stores\Response;

use Illuminate\Support\Collection;

final class StoreMainResponse
{
    /**
     * @var Collection
     */
    private $stores;

    /**
     * StoreMainResponse constructor.
     * @param Collection $stores
     */
    public function __construct(Collection $stores)
    {
        $this->stores = $stores;
    }

    /**
     * @return Collection
     */
    public function getStores(): Collection
    {
        return $this->stores;
    }
}
