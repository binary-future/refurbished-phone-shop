<?php


namespace App\Application\UseCases\Backend\Stores\Contracts;


use App\Application\UseCases\Backend\Stores\Response\StoreCreateResponse;

/**
 * Interface StoreCreateCase
 * @package App\Application\UseCases\Backend\Stores\Contracts
 */
interface StoreCreateCase
{
    /**
     * @return StoreCreateResponse
     */
    public function execute(): StoreCreateResponse;
}
