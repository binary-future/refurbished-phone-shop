<?php


namespace App\Application\UseCases\Backend\Stores\Contracts;


use App\Application\UseCases\Backend\Stores\Response\StoreDeleteResponse;

interface StoreDeleteCase
{
    public function execute(string $storeSlug): StoreDeleteResponse;
}