<?php

namespace App\Application\UseCases\Backend\Stores\Contracts;

use App\Application\UseCases\Backend\Stores\Requests\CreateStoreRequest;
use App\Application\UseCases\Backend\Stores\Response\StoreSaveResponse;

/**
 * Interface StoreCreateCase
 * @package App\Application\UseCases\Backend\Stores\Contracts
 */
interface StoreSaveCase
{
    /**
     * @param CreateStoreRequest $request
     * @return mixed
     */
    public function execute(CreateStoreRequest $request): StoreSaveResponse;
}
