<?php

namespace App\Application\UseCases\Backend\Stores\Contracts;

use App\Application\UseCases\Backend\Stores\Response\StoreMainResponse;

/**
 * Interface StoreMainCase
 * @package App\Application\UseCases\Backend\Stores\Contracts
 */
interface StoreMainCase
{
    /**
     * @return mixed
     */
    public function execute(): StoreMainResponse;
}
