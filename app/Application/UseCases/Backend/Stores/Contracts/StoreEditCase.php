<?php

namespace App\Application\UseCases\Backend\Stores\Contracts;

use App\Application\UseCases\Backend\Stores\Response\StoreEditResponse;

/**
 * Interface StoreEditCase
 * @package App\Application\UseCases\Backend\Stores\Contracts
 */
interface StoreEditCase
{
    /**
     * @param string $storeSlug
     * @return StoreEditResponse
     */
    public function execute(string $storeSlug): StoreEditResponse;
}
