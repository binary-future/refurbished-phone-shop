<?php


namespace App\Application\UseCases\Backend\Stores\Contracts;


use App\Application\UseCases\Backend\Stores\Requests\UpdateStoreRequest;
use App\Application\UseCases\Backend\Stores\Response\StoreSaveResponse;

/**
 * Interface StoreUpdateCase
 * @package App\Application\UseCases\Backend\Stores\Contracts
 */
interface StoreUpdateCase
{
    /**
     * @param string $storeSlug
     * @param UpdateStoreRequest $request
     * @return StoreSaveResponse
     */
    public function execute(string $storeSlug, UpdateStoreRequest $request): StoreSaveResponse;
}