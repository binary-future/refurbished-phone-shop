<?php

namespace App\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\Services\Importer\Datafeeds\Services\Contracts\Datafeeds;
use App\Application\Services\Transformers\DescriptionTransformer;
use App\Application\UseCases\Backend\Stores\Contracts\StoreSaveCase as Contract;
use App\Application\UseCases\Backend\Stores\Requests\CreateStoreRequest;
use App\Application\UseCases\Backend\Stores\Response\StoreSaveResponse;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\Affiliation\Affiliation;
use App\Domain\Store\Commands\CreateStoreCommand;
use App\Domain\Store\PaymentMethod\Query\ByIdsQuery;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Store;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Bus\QueryBus;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

/**
 * Class StoreCreateCase
 * @package App\Application\UseCases\Backend\Stores
 */
class StoreSaveCase implements Contract
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var Datafeeds
     */
    private $datafeeds;

    /**
     * StoreSaveCase constructor.
     * @param CommandBus $commandBus
     * @param Serializer $serializer
     * @param Uploader $uploader
     * @param Generator $slugGenerator
     * @param Datafeeds $datafeeds
     */
    public function __construct(
        CommandBus $commandBus,
        Serializer $serializer,
        Uploader $uploader,
        Generator $slugGenerator,
        Datafeeds $datafeeds
    ) {
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
        $this->uploader = $uploader;
        $this->slugGenerator = $slugGenerator;
        $this->datafeeds = $datafeeds;
    }

    public function execute(CreateStoreRequest $request): StoreSaveResponse
    {
        try {
            return $this->proceedCreating($request);
        } catch (\Throwable $exception) {
            $response = new StoreSaveResponse();
            $response->setSuccess(false);

            return $response;
        }
    }

    private function proceedCreating(CreateStoreRequest $request)
    {
        $params = $this->prepareParams($request->getParams());
        $store = $this->getStore($params);
        $related = $this->getRelated($store, $request);
        /**
         * @var Store $storeEntity
         */
        $storeEntity = $this->commandBus->dispatch(new CreateStoreCommand($store, $related));
        if ($request->getDatafeed()) {
            $this->datafeeds->store($store, $request->getDatafeed());
        }

        return new StoreSaveResponse($storeEntity);
    }

    private function getRelated(Store $store, CreateStoreRequest $request)
    {
        $related = [];
        if ($request->getLogo()) {
            $related[Store::RELATION_IMAGE] = $this->uploader->storeImage($request->getLogo(), $store);
        }
        $related[Store::RELATION_LINK] = $this->getLink($request->getLink());
        $related[Store::RELATION_DESCRIPTION] = $this->getDescription($request->getDescription());
        $related[Store::RELATION_PAYMENT_METHODS] = $request->getPaymentMethods();

        return $related;
    }

    private function prepareParams(array $params): array
    {
        $params[Store::FIELD_SLUG] = $this->slugGenerator->generate($params[CreateStoreRequest::INPUT_NAME]);
        $params[Store::FIELD_ALIAS] = $params[Store::FIELD_SLUG];
        $params[Store::FIELD_AFFILIATION] = new Affiliation($params[CreateStoreRequest::INPUT_AFFILIATION]);

        return $params;
    }

    private function getStore(array $params): Store
    {
        /**
         * @var Store $store
         */
        $store = $this->serializer->fromArray(Store::class, $params);

        return $store;
    }

    /**
     * @param string $link
     * @return Link
     */
    private function getLink(string $link): Link
    {
        /**
         * @var Link $link
         */
        $link = $this->serializer->fromArray(Link::class, [Link::FIELD_LINK => $link]);

        return $link;
    }

    private function getDescription(array $description): ?Description
    {
        try {
            /**
             * @var Description $description
             */
            $description = $this->serializer->fromArray(Description::class, $description);
            return $description->isValid() ? $description : null;
        } catch (\Throwable $exception) {
            return null;
        }
    }
}
