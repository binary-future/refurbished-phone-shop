<?php

namespace App\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Stores\Contracts\StoreDeleteCase as Contract;
use App\Application\UseCases\Backend\Stores\Response\StoreDeleteResponse;
use App\Domain\Store\Commands\DeleteStoreCommand;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StoreDeleteCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * StoreDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(string $storeSlug): StoreDeleteResponse
    {
        $store = $this->getStore($storeSlug);
        try {
            return $this->proceed($store);
        } catch (\Throwable $exception) {
            $response = new StoreDeleteResponse($store);
            $response->setSuccess(false);

            return $response;
        }
    }

    private function getStore(string $storeSlug): Store
    {
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    private function proceed(Store $store)
    {
        $this->commandBus->dispatch(new DeleteStoreCommand($store));
        $response = new StoreDeleteResponse($store);

        return $response;
    }
}
