<?php

namespace App\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Datafeeds\LinkGenerator\Contracts\LinkGenerator;
use App\Application\Services\Importer\Datafeeds\Query\GetAllApiQuery;
use App\Application\Services\Importer\Datafeeds\Query\GetInfoByStoresQuery;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsAPIs;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\Services\Importer\Datafeeds\Selectors\ImportScenarioSelector;
use App\Application\UseCases\Backend\Stores\Contracts\StoreEditCase as Contract;
use App\Application\UseCases\Backend\Stores\Response\StoreEditResponse;
use App\Domain\Store\PaymentMethod\Query\AllQuery;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Query\GetAffiliationListQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class StoreEditCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var DatafeedsInfo
     */
    private $datafeeds;

    /**
     * @var DatafeedsAPIs
     */
    private $apiRepo;
    /**
     * @var LinkGenerator
     */
    private $linkGenerator;
    /**
     * @var PaymentMethods
     */
    private $paymentMethodsRepo;

    /**
     * StoreEditCase constructor.
     * @param QueryBus $queryBus
     * @param DatafeedsInfo $datafeeds
     * @param DatafeedsAPIs $api
     * @param LinkGenerator $linkGenerator
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(
        QueryBus $queryBus,
        DatafeedsInfo $datafeeds,
        DatafeedsAPIs $api,
        LinkGenerator $linkGenerator,
        PaymentMethods $paymentMethods
    ) {
        $this->queryBus = $queryBus;
        $this->datafeeds = $datafeeds;
        $this->apiRepo = $api;
        $this->linkGenerator = $linkGenerator;
        $this->paymentMethodsRepo = $paymentMethods;
    }

    /**
     * @param string $storeSlug
     * @return StoreEditResponse
     */
    public function execute(string $storeSlug): StoreEditResponse
    {
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw (new ModelNotFoundException())->setModel(Store::class);
        }
        /**
         * @var Collection $affiliations
         */
        $affiliations = $this->queryBus->dispatch(new GetAffiliationListQuery());
        $datafeed = $this->datafeeds->findBy(new GetInfoByStoresQuery(collect([$store])))->first();
        $api = $this->apiRepo->findBy(new GetAllApiQuery());
        $paymentMethods = $this->paymentMethodsRepo->findBy(new AllQuery());

        try {
            $link = $this->linkGenerator->getLink($datafeed, ImportScenarioSelector::DEALS);
        } catch (\Throwable $exception) {
            $link = null;
        }

        return new StoreEditResponse($store, $affiliations, $api, $paymentMethods, $datafeed, $link);
    }
}
