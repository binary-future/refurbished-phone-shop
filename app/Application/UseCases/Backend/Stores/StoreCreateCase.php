<?php

namespace App\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Datafeeds\Query\GetAllApiQuery;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsAPIs;
use App\Application\UseCases\Backend\Stores\Contracts\StoreCreateCase as Contract;
use App\Application\UseCases\Backend\Stores\Response\StoreCreateResponse;
use App\Domain\Store\PaymentMethod\Query\AllQuery;
use App\Domain\Store\PaymentMethod\Repository\PaymentMethods;
use App\Domain\Store\Query\GetAffiliationListQuery;
use Illuminate\Support\Collection;

/**
 * Class StoreCreateCase
 * @package App\Application\UseCases\Backend\Stores
 */
final class StoreCreateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var DatafeedsAPIs
     */
    private $api;
    /**
     * @var PaymentMethods
     */
    private $paymentMethodsRepo;

    /**
     * StoreCreateCase constructor.
     * @param QueryBus $queryBus
     * @param DatafeedsAPIs $api
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(QueryBus $queryBus, DatafeedsAPIs $api, PaymentMethods $paymentMethods)
    {
        $this->queryBus = $queryBus;
        $this->api = $api;
        $this->paymentMethodsRepo = $paymentMethods;
    }

    public function execute(): StoreCreateResponse
    {
        /**
         * @var Collection $affiliations
         */
        $affiliations = $this->queryBus->dispatch(new GetAffiliationListQuery());
        $apiList = $this->api->findBy(new GetAllApiQuery());
        $paymentMethods = $this->paymentMethodsRepo->findBy(new AllQuery());

        return new StoreCreateResponse($affiliations, $apiList, $paymentMethods);
    }
}
