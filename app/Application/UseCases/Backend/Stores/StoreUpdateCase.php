<?php

namespace App\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\Services\Importer\Datafeeds\Services\Contracts\Datafeeds;
use App\Application\Services\Store\Rating\Contracts\StoreRatingManager;
use App\Application\UseCases\Backend\Stores\Contracts\StoreUpdateCase as Contract;
use App\Application\UseCases\Backend\Stores\Requests\UpdateStoreRequest;
use App\Application\UseCases\Backend\Stores\Response\StoreSaveResponse;
use App\Domain\Shared\Description\Description;
use App\Application\Services\Transformers\DescriptionTransformer;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Shared\Rating\RatingValueInPercents;
use App\Domain\Store\Commands\UpdateStoreCommand;
use App\Domain\Store\PaymentMethod\Query\ByIdsQuery;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class StoreUpdateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var Datafeeds
     */
    private $datafeeds;

    /**
     * @var DescriptionTransformer
     */
    private $descriptionTransformer;
    /**
     * @var StoreRatingManager
     */
    private $storeRatingManager;

    /**
     * StoreUpdateCase constructor.
     * @param QueryBus $queryBus
     * @param Uploader $uploader
     * @param CommandBus $commandBus
     * @param Serializer $serializer
     * @param Generator $generator
     * @param Datafeeds $datafeeds
     * @param DescriptionTransformer $descriptionTransformer
     * @param StoreRatingManager $storeRatingManager
     */
    public function __construct(
        QueryBus $queryBus,
        Uploader $uploader,
        CommandBus $commandBus,
        Serializer $serializer,
        Generator $generator,
        Datafeeds $datafeeds,
        DescriptionTransformer $descriptionTransformer,
        StoreRatingManager $storeRatingManager
    ) {
        $this->queryBus = $queryBus;
        $this->uploader = $uploader;
        $this->commandBus = $commandBus;
        $this->serializer = $serializer;
        $this->slugGenerator = $generator;
        $this->datafeeds = $datafeeds;
        $this->descriptionTransformer = $descriptionTransformer;
        $this->storeRatingManager = $storeRatingManager;
    }

    /**
     * @param string $storeSlug
     * @param UpdateStoreRequest $request
     * @return StoreSaveResponse
     */
    public function execute(string $storeSlug, UpdateStoreRequest $request): StoreSaveResponse
    {
        $store = $this->getStore($storeSlug);
        try {
            return $this->proceedUpdating($store, $request);
        } catch (\Throwable $exception) {
            $response = new StoreSaveResponse($store);
            $response->setSuccess(false);

            return $response;
        }
    }

    private function proceedUpdating(Store $store, UpdateStoreRequest $request): StoreSaveResponse
    {
        $params = $this->prepareParams($request->getParams());
        $related = $this->getRelated($store, $request);
        $this->commandBus->dispatch(new UpdateStoreCommand($store, $params, $related));
        if ($request->getDatafeed()) {
            $this->datafeeds->store($store, $request->getDatafeed());
        }

        return new StoreSaveResponse($store);
    }

    private function prepareParams(array $params): array
    {
        if (isset($params[UpdateStoreRequest::INPUT_NAME])) {
            $params[Store::FIELD_SLUG] = $this->slugGenerator->generate($params[UpdateStoreRequest::INPUT_NAME]);
        }

        return $params;
    }

    /**
     * @param string $slug
     * @return Store
     */
    private function getStore(string $slug): Store
    {
        $store = $this->queryBus->dispatch(new BySlugQuery($slug))->first();
        if (! $store) {
            throw (new ModelNotFoundException())->setModel(Store::class);
        }

        return $store;
    }

    private function getRelated(Store $store, UpdateStoreRequest $request): array
    {
        $related = [];
        if ($request->getLogo()) {
            $image = $this->uploader->storeImage($request->getLogo(), $store);
            $image->setOwner($store);
            $related[Store::RELATION_IMAGE] = $image;
        }
        if ($request->getLink()) {
            $related[Store::RELATION_LINK] = $this->getLink($request->getLink(), $store);
        }

        $related[Store::RELATION_DESCRIPTION] = $this->getDescription($request->getDescription(), $store);
        $related[Store::RELATION_PAYMENT_METHODS] = Collection::make($request->getPaymentMethods());
        $related[Store::RELATION_RATING] = $this->getRating(
            $request->getRatingValue(),
            $request->getRatingLink(),
            $store
        );

        return $related;
    }

    /**
     * @param RatingValueInPercents $ratingValue
     * @param Link $ratingLink
     * @param Store $store
     * @return Rating
     */
    private function getRating(RatingValueInPercents $ratingValue, Link $ratingLink, Store $store): Rating
    {
        /**
         * @var Rating $rating
         */
        $rating = $this->serializer->fromArray(Rating::class, [
            Rating::FIELD_RATING => $ratingValue
        ]);

        return $this->storeRatingManager->prepareRating($store, $rating, $ratingLink);
    }

    /**
     * @param string $link
     * @param Store $store
     * @return Link
     */
    private function getLink(string $link, Store $store): Link
    {
        /**
         * @var Link $linkEntity
         */
        $linkEntity = $this->serializer->fromArray(Link::class, [
            Link::FIELD_LINK => $link,
            Link::RELATION_OWNER => $store
        ]);

        return $linkEntity;
    }

    private function getDescription(array $description, Store $store): ?Description
    {
        return $this->descriptionTransformer->transform($description, $store);
    }

    private function getPaymentMethods(array $paymentMethods): Collection
    {
        /** @var Collection $collection */
        $collection = $this->queryBus->dispatch(new ByIdsQuery($paymentMethods));
        return $collection;
    }
}
