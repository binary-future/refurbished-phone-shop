<?php

namespace App\Application\UseCases\Backend\Stores;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Stores\Contracts\StoreMainCase as Contract;
use App\Application\UseCases\Backend\Stores\Response\StoreMainResponse;
use App\Domain\Store\Query\AllQuery;
use Illuminate\Support\Collection;

final class StoreMainCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreMainCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return StoreMainResponse
     */
    public function execute(): StoreMainResponse
    {
        /**
         * @var Collection $stores
         */
        $stores = $this->queryBus->dispatch(new AllQuery([
            'image'
        ]));

        return new StoreMainResponse($stores);
    }
}
