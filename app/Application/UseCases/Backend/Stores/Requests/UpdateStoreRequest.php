<?php

namespace App\Application\UseCases\Backend\Stores\Requests;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Shared\Rating\RatingValueInPercents;
use App\Domain\Store\Store;
use Illuminate\Http\UploadedFile;

/**
 * Class UpdateStoreRequest
 * @package App\Application\UseCases\Backend\Stores\Requests
 */
final class UpdateStoreRequest
{
    public const INPUT_NAME = Store::FIELD_NAME;
    public const INPUT_LINK = Link::FIELD_LINK;
    public const INPUT_IS_ACTIVE = Store::FIELD_IS_ACTIVE;
    public const INPUT_AFFILIATION = Store::FIELD_AFFILIATION;
    public const INPUT_PAYMENT_METHODS = Store::RELATION_PAYMENT_METHODS;
    public const INPUT_GUARANTEE = Store::FIELD_GUARANTEE;
    public const INPUT_TERMS = Store::FIELD_TERMS;
    public const INPUT_RATING_VALUE = Rating::TABLE . '.' . Rating::FIELD_RATING;
    public const INPUT_RATING_URL = Rating::TABLE . '.' . Link::FIELD_LINK;
    public const INPUT_LOGO = 'logo';
    public const INPUT_DATAFEED_API_ID = 'datafeed.' . DatafeedInfo::FIELD_API_ID;
    public const INPUT_DATAFEED_EXTERNAL_ID = 'datafeed.' . DatafeedInfo::FIELD_EXTERNAL_ID;
    public const INPUT_DESCRIPTION_CONTENT = 'description.' . Description::FIELD_CONTENT;
    public const INPUT_STORE_ID = DatafeedInfo::FIELD_STORE_ID;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $link;

    /**
     * @var int
     */
    private $isActive;

    /**
     * @var string
     */
    private $affiliation;

    /**
     * @var array
     */
    private $paymentMethods = [];

    /**
     * @var array
     */
    private $datafeed = [];

    /**
     * @var array
     */
    private $description = [];

    /**
     * @var UploadedFile|null
     */
    private $logo;
    /**
     * @var int
     */
    private $guarantee;
    /**
     * @var int
     */
    private $terms;
    /**
     * @var RatingValueInPercents
     */
    private $ratingValue;
    /**
     * @var Link
     */
    private $ratingUrl;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getisActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getAffiliation(): string
    {
        return $this->affiliation;
    }

    /**
     * @param string $affiliation
     */
    public function setAffiliation(string $affiliation): void
    {
        $this->affiliation = $affiliation;
    }

    /**
     * @return array
     */
    public function getPaymentMethods(): array
    {
        return array_filter($this->paymentMethods, static function ($item) {
            return $item !== null;
        });
    }

    /**
     * @param array $paymentMethods
     */
    public function setPaymentMethods(array $paymentMethods): void
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @return array
     */
    public function getDatafeed(): array
    {
        return $this->datafeed;
    }

    /**
     * @param array $datafeed
     */
    public function setDatafeed(array $datafeed): void
    {
        $this->datafeed = $datafeed;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @param array $description
     */
    public function setDescription(array $description): void
    {
        $this->description = $description;
    }

    public function setGuarantee(int $guarantee): void
    {
        $this->guarantee = $guarantee;
    }

    public function getGuarantee(): int
    {
        return $this->guarantee;
    }

    /**
     * @return UploadedFile|null
     */
    public function getLogo(): ?UploadedFile
    {
        return $this->logo;
    }

    /**
     * @param UploadedFile|null $logo
     */
    public function setLogo(?UploadedFile $logo): void
    {
        $this->logo = $logo;
    }

    public function setTerms(int $terms): void
    {
        $this->terms = $terms;
    }

    public function getTerms(): int
    {
        return $this->terms;
    }

    public function setRating(array $rating): void
    {
        $this->ratingValue = new RatingValueInPercents($rating[Rating::FIELD_RATING]);
        $this->ratingUrl = new Link([
            Link::FIELD_LINK => $rating[Link::FIELD_LINK]
        ]);
    }

    public function getRatingValue(): RatingValueInPercents
    {
        return $this->ratingValue;
    }

    public function getRatingLink(): Link
    {
        return $this->ratingUrl;
    }

    public function getParams(): array
    {
        $params = [
            self::INPUT_NAME => $this->name,
            self::INPUT_IS_ACTIVE => $this->isActive,
            self::INPUT_AFFILIATION => $this->affiliation,
            self::INPUT_GUARANTEE => $this->guarantee,
            self::INPUT_TERMS => $this->getTerms()
        ];

        return array_filter($params, static function ($item) {
            return $item !== null;
        });
    }
}
