<?php

namespace App\Application\UseCases\Backend\Stores\Requests;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Domain\Shared\Description\Description;
use App\Domain\Shared\Link\Link;
use App\Domain\Store\Store;
use Illuminate\Http\UploadedFile;

class CreateStoreRequest
{
    public const INPUT_NAME = Store::FIELD_NAME;
    public const INPUT_LINK = Link::FIELD_LINK;
    public const INPUT_IS_ACTIVE = Store::FIELD_IS_ACTIVE;
    public const INPUT_AFFILIATION = Store::FIELD_AFFILIATION;
    public const INPUT_PAYMENT_METHODS = Store::RELATION_PAYMENT_METHODS;
    public const INPUT_GUARANTEE = Store::FIELD_GUARANTEE;
    public const INPUT_TERMS = Store::FIELD_TERMS;
    public const INPUT_LOGO = 'logo';
    public const INPUT_DATAFEED_API_ID = 'datafeed.' . DatafeedInfo::FIELD_API_ID;
    public const INPUT_DATAFEED_EXTERNAL_ID = 'datafeed.' . DatafeedInfo::FIELD_EXTERNAL_ID;
    public const INPUT_DESCRIPTION_CONTENT = 'description.' . Description::FIELD_CONTENT;
    public const INPUT_STORE_ID = DatafeedInfo::FIELD_STORE_ID;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $link;

    /**
     * @var int
     */
    private $isActive;

    /**
     * @var string
     */
    private $affiliation;

    /**
     * @var array
     */
    private $datafeed = [];

    /**
     * @var array
     */
    private $description = [];

    /**
     * @var UploadedFile|null
     */
    private $logo;
    private $paymentMethods;
    /**
     * @var int
     */
    private $guarantee;
    /**
     * @var int
     */
    private $terms;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getisActive(): int
    {
        return $this->isActive;
    }

    /**
     * @param int $isActive
     */
    public function setIsActive(int $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getAffiliation(): string
    {
        return $this->affiliation;
    }

    /**
     * @param string $affiliation
     */
    public function setAffiliation(string $affiliation): void
    {
        $this->affiliation = $affiliation;
    }

    /**
     * @return array
     */
    public function getDatafeed(): array
    {
        return $this->datafeed;
    }

    /**
     * @param array $datafeed
     */
    public function setDatafeed(array $datafeed): void
    {
        $this->datafeed = $datafeed;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    /**
     * @param array $description
     */
    public function setDescription(array $description): void
    {
        $this->description = $description;
    }

    /**
     * @return UploadedFile|null
     */
    public function getLogo(): ?UploadedFile
    {
        return $this->logo;
    }

    /**
     * @param UploadedFile|null $logo
     */
    public function setLogo(?UploadedFile $logo): void
    {
        $this->logo = $logo;
    }

    public function getPaymentMethods(): array
    {
        return array_filter($this->paymentMethods ?? [], static function ($item) {
            return $item !== null;
        });
    }

    public function setPaymentMethods(array $paymentMethods): void
    {
        $this->paymentMethods = $paymentMethods;
    }

    public function setTerms(int $terms): void
    {
        $this->terms = $terms;
    }

    public function getTerms(): int
    {
        return $this->terms;
    }

    public function setGuarantee(int $guarantee): void
    {
        $this->guarantee = $guarantee;
    }

    public function getGuarantee(): int
    {
        return $this->guarantee;
    }

    public function getParams(): array
    {
        return [
            self::INPUT_NAME => $this->getName(),
            self::INPUT_IS_ACTIVE => $this->getisActive(),
            self::INPUT_AFFILIATION => $this->getAffiliation(),
            self::INPUT_GUARANTEE => $this->getGuarantee(),
            self::INPUT_TERMS => $this->getTerms()
        ];
    }
}
