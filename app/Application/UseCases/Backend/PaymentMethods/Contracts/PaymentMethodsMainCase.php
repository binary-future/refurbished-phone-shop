<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Contracts;

use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodsMainResponse;

interface PaymentMethodsMainCase
{
    public function execute(): PaymentMethodsMainResponse;
}
