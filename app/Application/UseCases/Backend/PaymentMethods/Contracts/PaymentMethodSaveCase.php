<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Contracts;

use App\Application\UseCases\Backend\PaymentMethods\Requests\SavePaymentMethodRequest;
use App\Application\UseCases\Backend\PaymentMethods\Response\SavePaymentMethodResponse;

interface PaymentMethodSaveCase
{
    public function execute(SavePaymentMethodRequest $request): SavePaymentMethodResponse;
}
