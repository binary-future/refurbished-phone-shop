<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Contracts;

use App\Application\UseCases\Backend\PaymentMethods\Requests\UpdatePaymentMethodRequest;
use App\Application\UseCases\Backend\PaymentMethods\Response\SavePaymentMethodResponse;
use App\Application\UseCases\Backend\PaymentMethods\Response\UpdatePaymentMethodResponse;

interface PaymentMethodUpdateCase
{
    public function execute(string $paymentMethodSlug, UpdatePaymentMethodRequest $request): SavePaymentMethodResponse;
}
