<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Contracts;

use App\Application\UseCases\Backend\PaymentMethods\Response\DeletePaymentMethodResponse;

interface PaymentMethodDeleteCase
{
    public function execute(string $paymentMethodSlug): DeletePaymentMethodResponse;
}
