<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Contracts;

use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodEditResponse;

interface PaymentMethodEditCase
{
    public function execute(string $paymentMethodSlug): PaymentMethodEditResponse;
}
