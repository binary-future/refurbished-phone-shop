<?php

namespace App\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\UseCases\Backend\PaymentMethods\Requests\UpdatePaymentMethodRequest;
use App\Application\UseCases\Backend\PaymentMethods\Response\SavePaymentMethodResponse;
use App\Domain\Store\PaymentMethod\Commands\UpdatePaymentMethodCommand;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Query\BySlugQuery;
use App\Utils\Adapters\Slug\Contracts\Generator;
use App\Utils\File\Upload\Exception\FileUploadException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Throwable;

class PaymentMethodUpdateCase implements Contracts\PaymentMethodUpdateCase
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var Generator
     */
    private $slugGenerator;
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreSaveCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     * @param Uploader $uploader
     * @param Generator $slugGenerator
     */
    public function __construct(
        QueryBus $queryBus,
        CommandBus $commandBus,
        Uploader $uploader,
        Generator $slugGenerator
    ) {
        $this->commandBus = $commandBus;
        $this->uploader = $uploader;
        $this->slugGenerator = $slugGenerator;
        $this->queryBus = $queryBus;
    }

    public function execute(string $paymentMethodSlug, UpdatePaymentMethodRequest $request): SavePaymentMethodResponse
    {
        $paymentMethod = $this->getPaymentMethod($paymentMethodSlug);
        try {
            return $this->proceedCreating($paymentMethod, $request);
        } catch (Throwable $exception) {
            $response = new SavePaymentMethodResponse();
            $response->setSuccess(false);

            return $response;
        }
    }

    /**
     * @param string $slug
     * @return PaymentMethod
     */
    private function getPaymentMethod(string $slug): PaymentMethod
    {
        $paymentMethod = $this->queryBus->dispatch(new BySlugQuery($slug))->first();
        if (! $paymentMethod) {
            throw (new ModelNotFoundException())->setModel(PaymentMethod::class);
        }

        return $paymentMethod;
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @param UpdatePaymentMethodRequest $request
     * @return SavePaymentMethodResponse
     * @throws FileUploadException
     */
    private function proceedCreating(
        PaymentMethod $paymentMethod,
        UpdatePaymentMethodRequest $request
    ): SavePaymentMethodResponse {
        $params = $this->prepareParams($request->getParams());
        $related = $this->getRelated($paymentMethod, $request);
        /**
         * @var PaymentMethod $paymentMethodEntity
         */
        $paymentMethodEntity = $this->commandBus->dispatch(
            new UpdatePaymentMethodCommand($paymentMethod, $params, $related)
        );

        return new SavePaymentMethodResponse($paymentMethodEntity);
    }

    /**
     * @param PaymentMethod $paymentMethod
     * @param UpdatePaymentMethodRequest $request
     * @return array
     * @throws FileUploadException
     */
    private function getRelated(PaymentMethod $paymentMethod, UpdatePaymentMethodRequest $request): array
    {
        $related = [];
        if ($request->getLogo()) {
            $image = $this->uploader->storeImage($request->getLogo(), $paymentMethod);
            $image->setOwner($paymentMethod);
            $related[PaymentMethod::RELATION_IMAGE] = $image;
        }

        return $related;
    }

    private function prepareParams(array $params): array
    {
        $params[PaymentMethod::FIELD_SLUG] = $this->slugGenerator->generate($params[PaymentMethod::FIELD_NAME]);

        return $params;
    }
}
