<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Response;

use Illuminate\Support\Collection;

final class PaymentMethodsMainResponse
{
    /**
     * @var Collection
     */
    private $paymentMethods;

    /**
     * StoreMainResponse constructor.
     * @param Collection $paymentMethods
     */
    public function __construct(Collection $paymentMethods)
    {
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @return Collection
     */
    public function getPaymentMethods(): Collection
    {
        return $this->paymentMethods;
    }
}
