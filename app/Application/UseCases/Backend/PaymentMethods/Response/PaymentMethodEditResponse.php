<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Response;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\Store;
use Illuminate\Support\Collection;

final class PaymentMethodEditResponse
{
    /**
     * @var PaymentMethod
     */
    private $paymentMethod;

    public function __construct(
        PaymentMethod $paymentMethod
    ) {
        $this->paymentMethod = $paymentMethod;
    }

    public function getPaymentMethod(): PaymentMethod
    {
        return $this->paymentMethod;
    }
}
