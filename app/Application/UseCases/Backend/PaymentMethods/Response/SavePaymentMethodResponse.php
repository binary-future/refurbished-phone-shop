<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Response;

use App\Domain\Store\PaymentMethod\PaymentMethod;

final class SavePaymentMethodResponse
{
    /**
     * @var PaymentMethod|null
     */
    private $paymentMethod;

    /**
     * @var bool
     */
    private $success = true;

    public function __construct(PaymentMethod $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;
    }

    public function getPaymentMethod(): ?PaymentMethod
    {
        return $this->paymentMethod;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
