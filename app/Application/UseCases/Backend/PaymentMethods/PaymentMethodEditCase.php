<?php

namespace App\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\PaymentMethods\Response\PaymentMethodEditResponse;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Query\BySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class PaymentMethodEditCase implements Contracts\PaymentMethodEditCase
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreEditCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(
        QueryBus $queryBus
    ) {
        $this->queryBus = $queryBus;
    }

    public function execute(string $paymentMethodSlug): PaymentMethodEditResponse
    {
        $paymentMethod = $this->queryBus->dispatch(new BySlugQuery($paymentMethodSlug))->first();
        if (! $paymentMethod) {
            throw (new ModelNotFoundException())->setModel(PaymentMethod::class);
        }

        return new PaymentMethodEditResponse($paymentMethod);
    }
}
