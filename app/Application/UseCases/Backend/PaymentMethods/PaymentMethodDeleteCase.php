<?php

namespace App\Application\UseCases\Backend\PaymentMethods;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\PaymentMethods\Response\DeletePaymentMethodResponse;
use App\Domain\Store\PaymentMethod\Commands\DeletePaymentMethodCommand;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\PaymentMethod\Query\BySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Throwable;

class PaymentMethodDeleteCase implements Contracts\PaymentMethodDeleteCase
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * StoreDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(string $paymentMethodSlug): DeletePaymentMethodResponse
    {
        $paymentMethod = $this->getPaymentMethod($paymentMethodSlug);
        try {
            return $this->proceed($paymentMethod);
        } catch (Throwable $exception) {
            $response = new DeletePaymentMethodResponse($paymentMethod);
            $response->setSuccess(false);

            return $response;
        }
    }

    private function getPaymentMethod(string $paymentMethodSlug): PaymentMethod
    {
        $paymentMethod = $this->queryBus->dispatch(new BySlugQuery($paymentMethodSlug))->first();
        if (! $paymentMethod) {
            throw (new ModelNotFoundException())->setModel(PaymentMethod::class);
        }

        return $paymentMethod;
    }

    private function proceed(PaymentMethod $paymentMethod): DeletePaymentMethodResponse
    {
        $this->commandBus->dispatch(new DeletePaymentMethodCommand($paymentMethod));

        return new DeletePaymentMethodResponse($paymentMethod);
    }
}
