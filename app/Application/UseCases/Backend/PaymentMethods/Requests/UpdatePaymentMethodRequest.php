<?php

namespace App\Application\UseCases\Backend\PaymentMethods\Requests;

use Illuminate\Http\UploadedFile;

class UpdatePaymentMethodRequest
{
    public const FIELD_NAME = 'name';
    public const FIELD_LOGO = 'logo';

    /**
     * @var string
     */
    private $name;

    /**
     * @var UploadedFile|null
     */
    private $logo;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return UploadedFile|null
     */
    public function getLogo(): ?UploadedFile
    {
        return $this->logo;
    }

    /**
     * @param UploadedFile|null $logo
     */
    public function setLogo(?UploadedFile $logo): void
    {
        $this->logo = $logo;
    }

    public function getParams(): array
    {
        return [
            self::FIELD_NAME => $this->getName(),
        ];
    }
}
