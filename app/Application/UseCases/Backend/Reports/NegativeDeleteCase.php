<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\Query\NegativeBySourceAndTypeQuery;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\UseCases\Backend\Reports\Contracts\NegativeDeleteCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class NegativeDeleteCase
 * @package App\Application\UseCases\Backend\Reports
 */
final class NegativeDeleteCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var NegativeReports
     */
    private $reports;

    /**
     * NegativeDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param NegativeReports $reports
     */
    public function __construct(QueryBus $queryBus, NegativeReports $reports)
    {
        $this->queryBus = $queryBus;
        $this->reports = $reports;
    }

    /**
     * @param string $store
     * @param string $type
     * @return NegativeDeleteResponse
     */
    public function execute(string $store, string $type): NegativeDeleteResponse
    {
        $store = $this->getStore($store);
        $reports = $this->getReports($store, $type);
        $success = $this->deleteReports($reports);

        return new NegativeDeleteResponse($store, $type, $success);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        /**
         * @var Store $store
         */
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    /**
     * @param Store $store
     * @param string $type
     * @return Collection
     */
    private function getReports(Store $store, string $type): Collection
    {
        return $this->reports->findBy(
            new NegativeBySourceAndTypeQuery($store->getKey(), Store::OWNER_TYPE, $type)
        );
    }

    /**
     * @param Collection $reports
     * @return bool
     */
    private function deleteReports(Collection $reports): bool
    {
        try {
            $this->reports->deleteSeveral($reports);
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }
}
