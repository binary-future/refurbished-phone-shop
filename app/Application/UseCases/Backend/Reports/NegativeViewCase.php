<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\Query\NegativeBySourceAndTypeQuery;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\UseCases\Backend\Reports\Contracts\NegativeViewCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\NegativeViewResponse;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class NegativeViewCase
 * @package App\Application\UseCases\Backend\Reports
 */
final class NegativeViewCase implements Contract
{
    private const PER_PAGE = 100;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var NegativeReports
     */
    private $reports;

    /**
     * NegativeViewCase constructor.
     * @param QueryBus $queryBus
     * @param NegativeReports $reports
     */
    public function __construct(QueryBus $queryBus, NegativeReports $reports)
    {
        $this->queryBus = $queryBus;
        $this->reports = $reports;
    }

    /**
     * @param string $store
     * @param string $type
     * @return NegativeViewResponse
     */
    public function execute(string $store, string $type): NegativeViewResponse
    {
        $storeEntity = $this->getStore($store);
        $reports = $this->getReports($storeEntity, $type);

        return new NegativeViewResponse($storeEntity, $reports, $type);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    /**
     * @param Store $store
     * @param string $type
     * @return LengthAwarePaginator
     */
    private function getReports(Store $store, string $type): LengthAwarePaginator
    {
        return $this->reports
            ->findPaginated(
                new NegativeBySourceAndTypeQuery($store->getKey(), Store::OWNER_TYPE, $type),
                self::PER_PAGE
            );
    }
}
