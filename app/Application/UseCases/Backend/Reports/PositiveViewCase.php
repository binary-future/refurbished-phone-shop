<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\Query\PositiveBySourceAndTargetTypeQuery;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\Contracts\PositiveViewCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\PositiveViewResponse;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class PositiveViewCase implements Contract
{
    private const PER_PAGE = 100;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var PositiveReports
     */
    private $reports;

    /**
     * PositiveViewCase constructor.
     * @param QueryBus $queryBus
     * @param PositiveReports $reports
     */
    public function __construct(QueryBus $queryBus, PositiveReports $reports)
    {
        $this->queryBus = $queryBus;
        $this->reports = $reports;
    }

    /**
     * @param string $storeSlug
     * @param string $type
     * @return PositiveViewResponse
     */
    public function execute(string $storeSlug, string $type): PositiveViewResponse
    {
        $storeEntity = $this->getStore($storeSlug);
        $reports = $this->getReports($storeEntity, $type);

        return new PositiveViewResponse($storeEntity, $reports, $type);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    /**
     * @param Store $store
     * @param string $type
     * @return LengthAwarePaginator
     */
    private function getReports(Store $store, string $type): LengthAwarePaginator
    {
        return $this->reports
            ->findPaginated(
                new PositiveBySourceAndTargetTypeQuery($store->getKey(), Store::OWNER_TYPE, $type),
                self::PER_PAGE
            );
    }
}
