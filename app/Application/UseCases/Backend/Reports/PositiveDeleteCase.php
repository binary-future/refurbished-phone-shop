<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\Query\PositiveBySourceAndTargetTypeQuery;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\Contracts\PositiveDeleteCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class PositiveDeleteCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var PositiveReports
     */
    private $reports;

    /**
     * NegativeDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param PositiveReports $reports
     */
    public function __construct(QueryBus $queryBus, PositiveReports $reports)
    {
        $this->queryBus = $queryBus;
        $this->reports = $reports;
    }

    /**
     * @param string $storeSlug
     * @param string $type
     * @return PositiveDeleteResponse
     */
    public function execute(string $storeSlug, string $type): PositiveDeleteResponse
    {
        $store = $this->getStore($storeSlug);
        $reports = $this->getReports($store, $type);
        $success = $this->deleteReports($reports);

        return new PositiveDeleteResponse($store, $type, $success);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        /**
         * @var Store $store
         */
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    /**
     * @param Store $store
     * @param string $type
     * @return Collection
     */
    private function getReports(Store $store, string $type): Collection
    {
        return $this->reports
            ->findBy(
                new PositiveBySourceAndTargetTypeQuery($store->getKey(), Store::OWNER_TYPE, $type)
            );
    }

    /**
     * @param Collection $reports
     * @return bool
     */
    private function deleteReports(Collection $reports): bool
    {
        try {
            $this->reports->deleteSeveral($reports);
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }
}
