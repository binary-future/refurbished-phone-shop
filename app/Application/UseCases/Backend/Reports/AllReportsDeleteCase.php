<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\Contracts\AllReportsDeleteCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;
use App\Application\UseCases\Backend\Reports\Responses\ReportsGroupDeleteResponse;
use App\Application\UseCases\Backend\Reports\Contracts\AllReportsCase;
use Illuminate\Support\Collection;

/**
 * Class AllReportsDeleteCase
 * @package App\Application\UseCases\Backend\Reports
 */
final class AllReportsDeleteCase implements Contract
{
    /**
     * @var AllReportsCase
     */
    private $allReportsCase;

    /**
     * @var NegativeReports
     */
    private $negativeReports;

    /**
     * @var PositiveReports
     */
    private $positiveReports;

    /**
     * AllReportsDeleteCase constructor.
     * @param AllReportsCase $allReportsCase
     * @param NegativeReports $negativeReports
     * @param PositiveReports $positiveReports
     */
    public function __construct(
        AllReportsCase $allReportsCase,
        NegativeReports $negativeReports,
        PositiveReports $positiveReports
    ) {
        $this->allReportsCase = $allReportsCase;
        $this->negativeReports = $negativeReports;
        $this->positiveReports = $positiveReports;
    }

    /**
     * @param string $type
     * @return ReportsGroupDeleteResponse
     */
    public function execute(string $type): ReportsGroupDeleteResponse
    {
        $reports = $this->getReports($type);
        $negativeDeleted = $this->deleteNegative($reports->getNegativeReports());
        $positiveDeleted = $this->deletePositive($reports->getPositiveReports());

        return new ReportsGroupDeleteResponse($negativeDeleted && $positiveDeleted);
    }

    /**
     * @param string $type
     * @return AllReportsResponse
     */
    private function getReports(string $type): AllReportsResponse
    {
        return $this->allReportsCase->execute($type);
    }

    /**
     * @param Collection $negativeReports
     * @return bool
     */
    private function deleteNegative(Collection $negativeReports)
    {
        try {
            $this->negativeReports->deleteSeveral($negativeReports);
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * @param Collection $positiveReports
     * @return bool
     */
    private function deletePositive(Collection $positiveReports)
    {
        try {
            $this->positiveReports->deleteSeveral($positiveReports);
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }
}
