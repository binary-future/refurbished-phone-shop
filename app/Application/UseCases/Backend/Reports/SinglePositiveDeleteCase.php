<?php


namespace App\Application\UseCases\Backend\Reports;


use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Query\PositiveByKeyQuery;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Application\UseCases\Backend\Reports\Contracts\SinglePositiveDeleteCase as Contract;

class SinglePositiveDeleteCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var PositiveReports
     */
    private $reports;

    /**
     * SinglePositiveDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param PositiveReports $reports
     */
    public function __construct(QueryBus $queryBus, PositiveReports $reports)
    {
        $this->queryBus = $queryBus;
        $this->reports = $reports;
    }

    /**
     * @param string $storeSlug
     * @param int $reportKey
     * @return PositiveDeleteResponse
     */
    public function execute(string $storeSlug, int $reportKey): PositiveDeleteResponse
    {
        $store = $this->getStore($storeSlug);
        $report = $this->getReport($reportKey);
        $success = $this->deleteReport($report);

        return new PositiveDeleteResponse($store, $report->getTargetType(), $success);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        /**
         * @var Store $store
         */
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    /**
     * @param int $reportKey
     * @return PositiveReport
     */
    private function getReport(int $reportKey): PositiveReport
    {
        $report = $this->reports->findBy(new PositiveByKeyQuery($reportKey))->first();
        if (! $report) {
            throw new ModelNotFoundException();
        }

        return $report;
    }

    /**
     * @param PositiveReport $report
     * @return bool
     */
    private function deleteReport(PositiveReport $report): bool
    {
        try {
            $this->reports->delete($report);
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }
}
