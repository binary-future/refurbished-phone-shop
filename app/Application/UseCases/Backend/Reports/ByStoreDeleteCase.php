<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\UseCases\Backend\Reports\Contracts\PositiveDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\NegativeDeleteCase;
use App\Application\UseCases\Backend\Reports\Contracts\ByStoreDeleteCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;
use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;
use App\Application\UseCases\Backend\Reports\Responses\ReportsGroupDeleteResponse;

/**
 * Class ByStoreDeleteCase
 * @package App\Application\UseCases\Backend\Reports
 */
final class ByStoreDeleteCase implements Contract
{
    /**
     * @var PositiveDeleteCase
     */
    private $positiveDeleteCase;

    /**
     * @var NegativeDeleteCase
     */
    private $negativeDeleteCase;

    /**
     * ByStoreDeleteCase constructor.
     * @param PositiveDeleteCase $positiveDeleteCase
     * @param NegativeDeleteCase $negativeDeleteCase
     */
    public function __construct(PositiveDeleteCase $positiveDeleteCase, NegativeDeleteCase $negativeDeleteCase)
    {
        $this->positiveDeleteCase = $positiveDeleteCase;
        $this->negativeDeleteCase = $negativeDeleteCase;
    }

    /**
     * @param string $storeSlug
     * @param string $type
     * @return ReportsGroupDeleteResponse
     */
    public function execute(string $storeSlug, string $type): ReportsGroupDeleteResponse
    {
        $positiveResponse = $this->positiveDeleteCase->execute($storeSlug, $type);
        $negativeResposnse = $this->negativeDeleteCase->execute($storeSlug, $type);

        return new ReportsGroupDeleteResponse($this->isNoErrors($positiveResponse, $negativeResposnse));
    }

    /**
     * @param PositiveDeleteResponse $positiveResponse
     * @param NegativeDeleteResponse $negativeResponse
     * @return bool
     */
    private function isNoErrors(
        PositiveDeleteResponse $positiveResponse,
        NegativeDeleteResponse $negativeResponse
    ): bool {
        return $positiveResponse->isDeleted() && $negativeResponse->isDeleted();
    }
}
