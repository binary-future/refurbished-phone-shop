<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;


use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;

/**
 * Interface PositiveDeleteCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface PositiveDeleteCase
{
    /**
     * @param string $storeSlug
     * @param string $type
     * @return PositiveDeleteResponse
     */
    public function execute(string $storeSlug, string $type): PositiveDeleteResponse;
}
