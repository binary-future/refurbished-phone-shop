<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;

use App\Application\UseCases\Backend\Reports\Responses\NegativeViewResponse;

/**
 * Interface NegativeViewCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface NegativeViewCase
{
    /**
     * @param string $store
     * @param string $type
     * @return NegativeViewResponse
     */
    public function execute(string $store, string $type): NegativeViewResponse;
}