<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;


use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;

/**
 * Interface AllReportsCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface AllReportsCase
{
    /**
     * @param string $type
     * @return AllReportsResponse
     */
    public function execute(string $type): AllReportsResponse;
}
