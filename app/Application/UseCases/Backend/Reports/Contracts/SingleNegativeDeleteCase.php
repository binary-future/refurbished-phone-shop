<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;


use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;

/**
 * Interface SingleNegativeDeleteCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface SingleNegativeDeleteCase
{
    /**
     * @param string $storeSlug
     * @param int $reportKey
     * @return NegativeDeleteResponse
     */
    public function execute(string $storeSlug, int $reportKey): NegativeDeleteResponse;
}