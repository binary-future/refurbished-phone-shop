<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;

use App\Application\UseCases\Backend\Reports\Responses\ReportsGroupDeleteResponse;

interface AllReportsDeleteCase
{
    public function execute(string $type): ReportsGroupDeleteResponse;
}