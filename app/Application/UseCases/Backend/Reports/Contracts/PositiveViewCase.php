<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;


use App\Application\UseCases\Backend\Reports\Responses\PositiveViewResponse;

/**
 * Interface PositiveViewCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface PositiveViewCase
{
    /**
     * @param string $storeSlug
     * @param string $type
     * @return PositiveViewResponse
     */
    public function execute(string $storeSlug, string $type): PositiveViewResponse;
}