<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;


use App\Application\UseCases\Backend\Reports\Responses\ReportsGroupDeleteResponse;

interface ByStoreDeleteCase
{
    public function execute(string $storeSlug, string $type): ReportsGroupDeleteResponse;
}