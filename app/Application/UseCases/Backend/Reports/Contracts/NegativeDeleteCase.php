<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;

use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;

/**
 * Interface NegativeDeleteCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface NegativeDeleteCase
{
    /**
     * @param string $store
     * @param string $type
     * @return mixed
     */
    public function execute(string $store, string $type): NegativeDeleteResponse;
}
