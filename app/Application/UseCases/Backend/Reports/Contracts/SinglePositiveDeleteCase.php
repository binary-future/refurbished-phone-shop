<?php


namespace App\Application\UseCases\Backend\Reports\Contracts;


use App\Application\UseCases\Backend\Reports\Responses\PositiveDeleteResponse;

/**
 * Interface SinglePositiveDeleteCase
 * @package App\Application\UseCases\Backend\Reports\Contracts
 */
interface SinglePositiveDeleteCase
{
    /**
     * @param string $storeSlug
     * @param int $reportKey
     * @return PositiveDeleteResponse
     */
    public function execute(string $storeSlug, int $reportKey): PositiveDeleteResponse;
}
