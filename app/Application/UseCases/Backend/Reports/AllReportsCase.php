<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Importer\Reports\Query\NegativeReportBySourceTypeAndTargetTypeQuery;
use App\Application\Services\Importer\Reports\Query\PositiveReportBySourceTypeAndTargetTypeQuery;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Reports\Contracts\AllReportsCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\AllReportsResponse;
use App\Domain\Store\Store;

/**
 * Class AllReportsCase
 * @package App\Application\UseCases\Backend\Reports
 */
final class AllReportsCase implements Contract
{
    /**
     * @var PositiveReports
     */
    private $positiveReports;

    /**
     * @var NegativeReports
     */
    private $negativeReports;

    /**
     * AllReportsCase constructor.
     * @param PositiveReports $positiveReports
     * @param NegativeReports $negativeReports
     */
    public function __construct(PositiveReports $positiveReports, NegativeReports $negativeReports)
    {
        $this->positiveReports = $positiveReports;
        $this->negativeReports = $negativeReports;
    }

    /**
     * @param string $type
     * @return AllReportsResponse
     */
    public function execute(string $type): AllReportsResponse
    {
        $positiveReports = $this->positiveReports
            ->findBy(new PositiveReportBySourceTypeAndTargetTypeQuery(Store::OWNER_TYPE, $type));
        $negativeReports = $this->negativeReports
            ->findBy(new NegativeReportBySourceTypeAndTargetTypeQuery(Store::OWNER_TYPE, $type));

        return new AllReportsResponse($positiveReports, $negativeReports);
    }
}
