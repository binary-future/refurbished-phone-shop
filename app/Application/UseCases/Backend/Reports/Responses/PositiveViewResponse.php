<?php

namespace App\Application\UseCases\Backend\Reports\Responses;

use App\Domain\Store\Store;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class PositiveViewResponse
 * @package App\Application\UseCases\Backend\Reports\Responses
 */
final class PositiveViewResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var LengthAwarePaginator
     */
    private $reports;

    /**
     * @var string
     */
    private $type;

    /**
     * PositiveViewResponse constructor.
     * @param Store $store
     * @param LengthAwarePaginator $reports
     * @param string $type
     */
    public function __construct(Store $store, LengthAwarePaginator $reports, string $type)
    {
        $this->store = $store;
        $this->reports = $reports;
        $this->type = $type;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getReports(): LengthAwarePaginator
    {
        return $this->reports;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
