<?php

namespace App\Application\UseCases\Backend\Reports\Responses;

/**
 * Class ReportsGroupDeleteResponse
 * @package App\Application\UseCases\Backend\Reports\Responses
 */
final class ReportsGroupDeleteResponse
{
    /**
     * @var bool
     */
    private $isDeleted;

    /**
     * ReportsGroupDeleteResponse constructor.
     * @param bool $isDeleted
     */
    public function __construct(bool $isDeleted = true)
    {
        $this->isDeleted = $isDeleted;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }
}
