<?php


namespace App\Application\UseCases\Backend\Reports\Responses;


use App\Domain\Store\Store;

/**
 * Class NegativeDeleteResponse
 * @package App\Application\UseCases\Backend\Reports\Responses
 */
final class NegativeDeleteResponse
{
    /**
     * @var Store
     */
    private $store;

    /**
     * @var bool
     */
    private $deleted;

    /**
     * @var string
     */
    private $type;

    /**
     * NegativeDeleteResponse constructor.
     * @param Store $store
     * @param string $type
     * @param bool $deleted
     */
    public function __construct(Store $store, string $type, $deleted = false)
    {
        $this->store = $store;
        $this->type = $type;
        $this->deleted = $deleted;
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->store;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}
