<?php


namespace App\Application\UseCases\Backend\Reports\Responses;


use Illuminate\Support\Collection;

/**
 * Class AllReportsResponse
 * @package App\Application\UseCases\Backend\Reports\Responses
 */
final class AllReportsResponse
{
    /**
     * @var Collection
     */
    private $negativeReports;

    /**
     * @var Collection
     */
    private $positiveReports;

    /**
     * AllReportsResponse constructor.
     * @param Collection $negativeReports
     * @param Collection $positiveReports
     */
    public function __construct(Collection $positiveReports, Collection $negativeReports)
    {
        $this->negativeReports = $negativeReports;
        $this->positiveReports = $positiveReports;
    }

    /**
     * @return Collection
     */
    public function getNegativeReports(): Collection
    {
        return $this->negativeReports;
    }

    /**
     * @return Collection
     */
    public function getPositiveReports(): Collection
    {
        return $this->positiveReports;
    }
}
