<?php


namespace App\Application\UseCases\Backend\Reports;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\Query\NegativeByKeyQuery;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\UseCases\Backend\Reports\Contracts\SingleNegativeDeleteCase as Contract;
use App\Application\UseCases\Backend\Reports\Responses\NegativeDeleteResponse;
use App\Domain\Store\Query\BySlugQuery;
use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class SingleNegativeDeleteCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var NegativeReports
     */
    private $reports;

    /**
     * SingleNegativeDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param NegativeReports $reports
     */
    public function __construct(QueryBus $queryBus, NegativeReports $reports)
    {
        $this->queryBus = $queryBus;
        $this->reports = $reports;
    }

    /**
     * @param string $storeSlug
     * @param int $reportKey
     * @return NegativeDeleteResponse
     */
    public function execute(string $storeSlug, int $reportKey): NegativeDeleteResponse
    {
        $store = $this->getStore($storeSlug);
        $report = $this->getReport($reportKey);
        $success = $this->deleteReport($report);

        return new NegativeDeleteResponse($store, $report->getTargetType(), $success);
    }

    /**
     * @param string $storeSlug
     * @return Store
     */
    private function getStore(string $storeSlug): Store
    {
        /**
         * @var Store $store
         */
        $store = $this->queryBus->dispatch(new BySlugQuery($storeSlug))->first();
        if (! $store) {
            throw new ModelNotFoundException();
        }

        return $store;
    }

    /**
     * @param int $reportKey
     * @return NegativeReport
     */
    private function getReport(int $reportKey): NegativeReport
    {
        $report = $this->reports->findBy(new NegativeByKeyQuery($reportKey))->first();
        if (! $report) {
            throw new ModelNotFoundException();
        }

        return $report;
    }

    /**
     * @param NegativeReport $report
     * @return bool
     */
    private function deleteReport(NegativeReport $report): bool
    {
        try {
            $this->reports->delete($report);
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }
}
