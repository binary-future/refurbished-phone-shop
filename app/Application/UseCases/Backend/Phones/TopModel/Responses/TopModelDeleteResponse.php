<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Responses;


/**
 * Class TopModelDeleteResponse
 * @package App\Application\UseCases\Backend\Phones\TopModel\Responses
 */
final class TopModelDeleteResponse
{
    /**
     * @var bool
     */
    private $isSuccess;

    /**
     * ModelMainResponse constructor.
     * @param bool $isSuccess
     */
    public function __construct(bool $isSuccess)
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }
}
