<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Responses;


use App\Application\Services\Model\TopModel\TopModel;

/**
 * Class TopModelCreateResponse
 * @package App\Application\UseCases\Backend\Phones\TopModel\Responses
 */
final class TopModelCreateResponse
{
    /**
     * @var TopModel|null
     */
    private $topModel;

    /**
     * @var bool
     */
    private $isSuccess;

    /**
     * ModelMainResponse constructor.
     * @param TopModel|null $topModel
     * @param bool $isSuccess
     */
    public function __construct(?TopModel $topModel, bool $isSuccess)
    {
        $this->topModel = $topModel;
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return TopModel|null
     */
    public function getTopModel(): ?TopModel
    {
        return $this->topModel;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }
}
