<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Responses;


use App\Application\Services\Model\TopModel\TopModel;

/**
 * Class TopModelUpdateResponse
 * @package App\Application\UseCases\Backend\Phones\TopModel\Responses
 */
final class TopModelUpdateResponse
{
    /**
     * @var TopModel
     */
    private $topModel;
    /** @var bool */
    private $isSuccess;

    /**
     * ModelMainResponse constructor.
     * @param TopModel $topModel
     * @param bool $isSuccess
     */
    public function __construct(TopModel $topModel, bool $isSuccess)
    {
        $this->topModel = $topModel;
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return TopModel
     */
    public function getTopModel(): TopModel
    {
        return $this->topModel;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }
}
