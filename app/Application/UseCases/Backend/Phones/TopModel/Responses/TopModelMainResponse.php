<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Responses;


use Illuminate\Support\Collection;

/**
 * Class TopModelMainResponse
 * @package App\Application\UseCases\Backend\Phones\TopModel\Responses
 */
final class TopModelMainResponse
{
    /**
     * @var Collection
     */
    private $models;
    /**
     * @var Collection
     */
    private $topModels;

    /**
     * ModelMainResponse constructor.
     * @param Collection $models
     * @param Collection $topModels
     */
    public function __construct(Collection $models, Collection $topModels)
    {
        $this->models = $models;
        $this->topModels = $topModels;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }

    /**
     * @return Collection
     */
    public function getTopModels(): Collection
    {
        return $this->topModels;
    }
}
