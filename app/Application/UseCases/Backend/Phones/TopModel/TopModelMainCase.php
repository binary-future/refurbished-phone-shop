<?php


namespace App\Application\UseCases\Backend\Phones\TopModel;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\TopModel\Query\AllOrderedQuery;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelMainCase as Contract;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelMainResponse;
use App\Domain\Phone\Model\Query\AllActiveButExcludeWithoutSynonymsQuery;
use Illuminate\Support\Collection;

final class TopModelMainCase implements Contract
{
    /**
     * @var TopModels
     */
    private $repository;
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * TopModelMainCase constructor.
     * @param TopModels $repository
     * @param QueryBus $queryBus
     */
    public function __construct(TopModels $repository, QueryBus $queryBus)
    {
        $this->repository = $repository;
        $this->queryBus = $queryBus;
    }

    /**
     * @return TopModelMainResponse
     */
    public function execute(): TopModelMainResponse
    {
        $topModels = $this->getTopModels();
        $models = $this->getModels($topModels);

        return new TopModelMainResponse($models, $topModels);
    }

    private function getTopModels(): Collection
    {
        /**
         * @var Collection $topModels
         */
        $topModels = $this->repository->findBy(new AllOrderedQuery(['model', 'model.phones', 'model.brand']));

        return $topModels;
    }

    private function getModels(Collection $topModels): Collection
    {
        $phoneModelsIds = $topModels->map(static function ($topModel) {
            /** @var $topModel TopModel */
            return $topModel->getModel()->getKey();
        });

        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch(
            new AllActiveButExcludeWithoutSynonymsQuery($phoneModelsIds, ['brand', 'phones'])
        );

        return $models;
    }


}
