<?php


namespace App\Application\UseCases\Backend\Phones\TopModel;

use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Query\TopModelByIdQuery;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelUpdateCase as Contract;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelUpdateRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelUpdateResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class TopModelUpdateCase implements Contract
{
    /**
     * @var TopModels
     */
    private $repository;
    /**
     * @var Orderer
     */
    private $orderer;

    /**
     * TopModelAddCase constructor.
     * @param TopModels $repository
     * @param Orderer $orderer
     */
    public function __construct(TopModels $repository, Orderer $orderer)
    {
        $this->repository = $repository;
        $this->orderer = $orderer;
    }

    /**
     * @param TopModelUpdateRequest $request
     * @return TopModelUpdateResponse
     */
    public function execute(TopModelUpdateRequest $request): TopModelUpdateResponse
    {
        $topModel = $this->getTopModel($request->getTopModelId());
        $newOrder = $request->getOrder();
        $oldOrder = $topModel->getOrder();
        $success = false;

        if ($oldOrder !== $newOrder) {
            try {
                $topModel = $this->proceed($oldOrder, $newOrder, $topModel);
                $success = true;
            } catch (\Throwable $exception) {}
        }

        return new TopModelUpdateResponse($topModel, $success);
    }

    /**
     * @param int $topModelId
     * @return TopModel
     */
    private function getTopModel(int $topModelId): TopModel
    {
        /**
         * @var TopModel $topModel
         */
        $topModel = $this->repository->findBy(
            new TopModelByIdQuery($topModelId)
        )->first();

        if ($topModel === null) {
            throw new ModelNotFoundException('Top model not found');
        }

        return $topModel;
    }

    /**
     * @param TopModel $topModel
     * @param int $newOrder
     * @return mixed
     */
    private function updateTopModelOrder(TopModel $topModel, int $newOrder)
    {
        $topModel->setOrder($newOrder);
        return $this->repository->save($topModel);
    }

    /**
     * @param int $oldOrder
     * @param int $newOrder
     * @param TopModel $topModel
     * @return TopModel|mixed
     */
    private function proceed(int $oldOrder, int $newOrder, TopModel $topModel)
    {
        $intermediateTopModels = $this->orderer->orderBetween($oldOrder, $newOrder);
        foreach ($intermediateTopModels as $iTopModel) {
            $this->repository->save($iTopModel);
        }

        $topModel = $this->updateTopModelOrder($topModel, $newOrder);
        return $topModel;
    }

}
