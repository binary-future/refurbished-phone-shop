<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Contracts;



use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelMainResponse;

interface TopModelMainCase
{
    public function execute(): TopModelMainResponse;
}
