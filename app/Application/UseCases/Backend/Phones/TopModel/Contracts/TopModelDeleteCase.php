<?php

namespace App\Application\UseCases\Backend\Phones\TopModel\Contracts;

use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelDeleteRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelDeleteResponse;

interface TopModelDeleteCase
{
    /**
     * @param TopModelDeleteRequest $request
     * @return TopModelDeleteResponse
     */
    public function execute(TopModelDeleteRequest $request): TopModelDeleteResponse;
}
