<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Contracts;


use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelUpdateRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelUpdateResponse;

interface TopModelUpdateCase
{
    public function execute(TopModelUpdateRequest $request): TopModelUpdateResponse;
}
