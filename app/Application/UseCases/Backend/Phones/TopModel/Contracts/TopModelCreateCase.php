<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Contracts;


use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelCreateRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelCreateResponse;

interface TopModelCreateCase
{
    public function execute(TopModelCreateRequest $request): TopModelCreateResponse;
}
