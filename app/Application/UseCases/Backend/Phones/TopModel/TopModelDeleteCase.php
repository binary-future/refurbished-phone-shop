<?php


namespace App\Application\UseCases\Backend\Phones\TopModel;

use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Query\TopModelByIdQuery;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelDeleteRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelDeleteResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class TopModelDeleteCase implements Contracts\TopModelDeleteCase
{
    /**
     * @var TopModels
     */
    private $repository;
    /**
     * @var Orderer
     */
    private $orderer;

    /**
     * TopModelAddCase constructor.
     * @param TopModels $repository
     * @param Orderer $orderer
     */
    public function __construct(TopModels $repository, Orderer $orderer)
    {
        $this->repository = $repository;
        $this->orderer = $orderer;
    }

    /**
     * @param TopModelDeleteRequest $request
     * @return TopModelDeleteResponse
     */
    public function execute(TopModelDeleteRequest $request): TopModelDeleteResponse
    {
        $isSuccess = $this->deleteTopModel($request);

        return new TopModelDeleteResponse($isSuccess);
    }

    private function deleteTopModel(TopModelDeleteRequest $request): bool
    {
        $topModelId = $request->getTopModelId();

        $topModel = $this->getTopModel($topModelId);

        $orderedTopModels = $this->orderer->deletedFrom($topModel->getOrder());
        foreach ($orderedTopModels as $orderedTopModel) {
            $this->repository->save($orderedTopModel);
        }

        return $this->repository->delete($topModel);
    }

    /**
     * @param int $topModelId
     * @return TopModel
     */
    private function getTopModel(int $topModelId): TopModel
    {
        $topModel = $this->repository->findBy(
            new TopModelByIdQuery($topModelId)
        )->first();

        if ($topModel === null) {
            throw new ModelNotFoundException('Top model not found');
        }

        return $topModel;
    }


}
