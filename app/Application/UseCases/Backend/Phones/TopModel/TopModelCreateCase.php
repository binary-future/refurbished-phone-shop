<?php


namespace App\Application\UseCases\Backend\Phones\TopModel;

use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer;
use App\Application\Services\Model\TopModel\Query\TopModelByModelIdQuery;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use App\Application\UseCases\Backend\Phones\TopModel\Contracts\TopModelCreateCase as Contract;
use App\Application\UseCases\Backend\Phones\TopModel\Requests\TopModelCreateRequest;
use App\Application\UseCases\Backend\Phones\TopModel\Responses\TopModelCreateResponse;

final class TopModelCreateCase implements Contract
{
    /**
     * @var TopModels
     */
    private $repository;
    /**
     * @var Orderer
     */
    private $orderer;

    /**
     * TopModelAddCase constructor.
     * @param TopModels $repository
     * @param Orderer $orderer
     */
    public function __construct(TopModels $repository, Orderer $orderer)
    {
        $this->repository = $repository;
        $this->orderer = $orderer;
    }

    /**
     * @param TopModelCreateRequest $request
     * @return TopModelCreateResponse
     */
    public function execute(TopModelCreateRequest $request): TopModelCreateResponse
    {
        $topModel = null;

        if (! $this->isTopModelAlreadyExists($request->getModelId())) {
            try {
                $topModel = $this->createTopModel($request);
            } catch (\Throwable $exception) {}
        }

        $success = $topModel !== null;

        return new TopModelCreateResponse($topModel, $success);
    }

    /**
     * @param int $modelId
     * @return bool
     */
    private function isTopModelAlreadyExists(int $modelId): bool
    {
        try {
            $topModel = $this->repository->findBy(
                new TopModelByModelIdQuery($modelId)
            )->first();
        } catch (\Throwable $exception) {
            return false;
        }

        return $topModel instanceof TopModel;
    }

    private function createTopModel(TopModelCreateRequest $request): TopModel
    {
        $intermediateTopModels = $this->orderer->addedTo($request->getOrder());
        foreach ($intermediateTopModels as $iTopModel) {
            $this->repository->save($iTopModel);
        }

        /**
         * @var TopModel $topModel
         */
        $topModel = $this->repository->create(
            $request->getParams()
        );

        return $topModel;
    }


}
