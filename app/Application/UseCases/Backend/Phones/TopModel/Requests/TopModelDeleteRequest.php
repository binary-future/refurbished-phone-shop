<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Requests;


/**
 * Class TopModelDeleteRequest
 * @package App\Application\UseCases\Backend\Phones\TopModel\Requests
 */
final class TopModelDeleteRequest
{
    /**
     * @var int
     */
    private $topModelId;

    /**
     * TopModelDeleteRequest constructor.
     * @param int $topModelId
     */
    public function __construct(int $topModelId)
    {
        $this->topModelId = $topModelId;
    }

    /**
     * @return int
     */
    public function getTopModelId(): int
    {
        return $this->topModelId;
    }
}
