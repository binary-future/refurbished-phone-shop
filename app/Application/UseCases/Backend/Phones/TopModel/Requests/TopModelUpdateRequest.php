<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Requests;


use App\Application\Services\Model\TopModel\TopModel;

/**
 * Class TopModelUpdateRequest
 * @package App\Application\UseCases\Backend\Phones\TopModel\Requests
 */
final class TopModelUpdateRequest
{
    /**
     * @var int
     */
    private $topModelId;

    /**
     * @var int
     */
    private $order;

    /**
     * @return int
     */
    public function getTopModelId(): int
    {
        return $this->topModelId;
    }

    /**
     * @param int $topModelId
     */
    public function setTopModelId(int $topModelId): void
    {
        $this->topModelId = $topModelId;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }
}
