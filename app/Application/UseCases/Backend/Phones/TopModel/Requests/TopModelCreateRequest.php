<?php


namespace App\Application\UseCases\Backend\Phones\TopModel\Requests;


use App\Application\Services\Model\TopModel\TopModel;

/**
 * Class TopModelCreateRequest
 * @package App\Application\UseCases\Backend\Phones\TopModel\Requests
 */
final class TopModelCreateRequest
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * @var int
     */
    private $order;

    /**
     * @return int
     */
    public function getModelId(): int
    {
        return $this->modelId;
    }

    /**
     * @param int $modelId
     */
    public function setModelId(int $modelId): void
    {
        $this->modelId = $modelId;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return [
            TopModel::FIELD_MODEL_ID => $this->getModelId(),
            TopModel::FIELD_ORDER => $this->getOrder(),
        ];
    }
}
