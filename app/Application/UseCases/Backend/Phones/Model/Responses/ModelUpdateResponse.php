<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Application\Services\Model\LatestModel\LatestModel;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;

final class ModelUpdateResponse
{
    /**
     * @var PhoneModel
     */
    private $model;

    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var bool
     */
    private $success = true;
    /**
     * @var LatestModel
     */
    private $latestModel;

    /**
     * ModelUpdateResponse constructor.
     * @param PhoneModel $model
     * @param Brand $brand
     * @param LatestModel|null $latestModel
     */
    public function __construct(PhoneModel $model, Brand $brand, ?LatestModel $latestModel)
    {
        $this->model = $model;
        $this->brand = $brand;
        $this->latestModel = $latestModel;
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->model;
    }

    /**
     * @return LatestModel
     */
    public function getLatestModel(): LatestModel
    {
        return $this->latestModel;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
