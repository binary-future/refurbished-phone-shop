<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;

/**
 * Class ModelEditResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class ModelEditResponse
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var PhoneModel
     */
    private $phoneModel;
    /**
     * @var bool
     */
    private $isLatestModel;

    /**
     * ModelEditResponse constructor.
     * @param Brand $brand
     * @param PhoneModel $phoneModel
     * @param bool $isLatestModel
     */
    public function __construct(Brand $brand, PhoneModel $phoneModel, bool $isLatestModel)
    {
        $this->brand = $brand;
        $this->phoneModel = $phoneModel;
        $this->isLatestModel = $isLatestModel;
    }

    /**
     * @return bool
     */
    public function isLatestModel(): bool
    {
        return $this->isLatestModel;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return PhoneModel
     */
    public function getPhoneModel(): PhoneModel
    {
        return $this->phoneModel;
    }
}
