<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Application\Services\Model\LatestModel\LatestModel;
use App\Domain\Phone\Model\PhoneModel;

/**
 * Class ModelsResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class ModelsResponse
{
    /**
     * @var PhoneModel
     */
    private $phoneModel;
    /**
     * @var LatestModel|null
     */
    private $latestModel;

    /**
     * ModelListResponse constructor.
     * @param PhoneModel $phoneModel
     * @param LatestModel|null $latestModel
     */
    public function __construct(PhoneModel $phoneModel, ?LatestModel $latestModel)
    {
        $this->phoneModel = $phoneModel;
        $this->latestModel = $latestModel;
    }

    /**
     * @return PhoneModel
     */
    public function getPhoneModel(): PhoneModel
    {
        return $this->phoneModel;
    }

    /**
     * @return LatestModel|null
     */
    public function getLatestModel(): ?LatestModel
    {
        return $this->latestModel;
    }

}
