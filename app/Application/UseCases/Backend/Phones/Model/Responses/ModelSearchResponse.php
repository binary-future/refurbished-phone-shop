<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class ModelSearchResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class ModelSearchResponse
{
    /**
     * @var LengthAwarePaginator|Collection
     */
    private $models;

    /**
     * ModelSearchResponse constructor.
     * @param Collection|LengthAwarePaginator $models
     */
    public function __construct($models)
    {
        $this->models = $models;
    }

    /**
     * @return Collection|LengthAwarePaginator
     */
    public function getModels()
    {
        return $this->models;
    }
}
