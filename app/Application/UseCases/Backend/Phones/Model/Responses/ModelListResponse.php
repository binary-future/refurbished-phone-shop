<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Domain\Phone\Brand\Brand;
use Illuminate\Support\Collection;

/**
 * Class ModelListResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class ModelListResponse
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var Collection
     */
    private $models;

    /**
     * ModelListResponse constructor.
     * @param Brand $brand
     * @param Collection $models
     */
    public function __construct(Brand $brand, Collection $models)
    {
        $this->brand = $brand;

        $models->map(static function ($modelsResponse) {
            if (! $modelsResponse instanceof ModelsResponse) {
                throw new \InvalidArgumentException('Model item must be instance of ModelsResponse');
            }

            return $modelsResponse;
        });
        $this->models = $models;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }
}
