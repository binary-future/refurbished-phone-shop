<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Support\Collection;

final class ModelSynonymViewResponse
{
    /**
     * @var Brand $brand
     */
    private $brand;

    /**
     * @var PhoneModel $model
     */
    private $model;

    /**
     * @var Collection
     */
    private $synonyms;

    /**
     * @var Collection
     */
    private $potentialSynonyms;

    /**
     * ModelSynonymViewResponse constructor.
     * @param Brand $brand
     * @param PhoneModel $model
     * @param Collection $synonyms
     * @param Collection $potentialSynonyms
     */
    public function __construct(Brand $brand, PhoneModel $model, Collection $synonyms, Collection $potentialSynonyms)
    {
        $this->brand = $brand;
        $this->model = $model;
        $this->synonyms = $synonyms;
        $this->potentialSynonyms = $potentialSynonyms;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->model;
    }

    /**
     * @return Collection
     */
    public function getSynonyms(): Collection
    {
        return $this->synonyms;
    }

    /**
     * @return Collection
     */
    public function getPotentialSynonyms(): Collection
    {
        return $this->potentialSynonyms;
    }
}
