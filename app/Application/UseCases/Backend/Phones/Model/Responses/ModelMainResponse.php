<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use Illuminate\Support\Collection;

/**
 * Class ModelMainResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class ModelMainResponse
{
    /**
     * @var Collection
     */
    private $brands;

    /**
     * ModelMainResponse constructor.
     * @param Collection $brands
     */
    public function __construct(Collection $brands)
    {
        $this->brands = $brands;
    }

    /**
     * @return Collection
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }
}
