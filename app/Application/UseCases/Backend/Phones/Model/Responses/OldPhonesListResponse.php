<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use Illuminate\Support\Collection;

final class OldPhonesListResponse
{
    /**
     * @var Collection
     */
    private $models;

    /**
     * OldPhonesListResponse constructor.
     * @param Collection $models
     */
    public function __construct(Collection $models)
    {
        $this->models = $models;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }
}
