<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Application\Services\Model\LatestModel\LatestModel;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Support\Collection;

/**
 * Class ModelViewResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class ModelViewResponse
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var PhoneModel
     */
    private $phoneModel;

    /**
     * @var Collection
     */
    private $phones;

    /**
     * @var Collection
     */
    private $synonymsPhones;

    /**
     * @var Collection
     */
    private $colors;
    /**
     * @var LatestModel
     */
    private $latestModel;

    /**
     * ModelViewResponse constructor.
     * @param Brand $brand
     * @param PhoneModel $phoneModel
     * @param LatestModel|null $latestModel
     * @param Collection $phones
     * @param Collection $synonymsPhones
     * @param Collection $colors
     */
    public function __construct(
        Brand $brand,
        PhoneModel $phoneModel,
        ?LatestModel $latestModel,
        Collection $phones,
        Collection $synonymsPhones,
        Collection $colors
    ) {
        $this->brand = $brand;
        $this->phoneModel = $phoneModel;
        $this->phones = $phones;
        $this->synonymsPhones = $synonymsPhones;
        $this->colors = $colors;
        $this->latestModel = $latestModel;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return PhoneModel
     */
    public function getPhoneModel(): PhoneModel
    {
        return $this->phoneModel;
    }

    /**
     * @return LatestModel|null
     */
    public function getLatestModel(): ?LatestModel
    {
        return $this->latestModel;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @return Collection
     */
    public function getColors(): Collection
    {
        return $this->colors;
    }

    /**
     * @return Collection
     */
    public function getSynonymsPhones(): Collection
    {
        return $this->synonymsPhones;
    }
}
