<?php


namespace App\Application\UseCases\Backend\Phones\Model\Responses;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;

final class ModelManageSynonymResponse
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var PhoneModel
     */
    private $model;

    /**
     * @var bool
     */
    private $succeed;

    /**
     * ModelManageSynonymResponse constructor.
     * @param Brand $brand
     * @param PhoneModel $model
     * @param bool $succeed
     */
    public function __construct(Brand $brand, PhoneModel $model, bool $succeed)
    {
        $this->brand = $brand;
        $this->model = $model;
        $this->succeed = $succeed;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->model;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }
}
