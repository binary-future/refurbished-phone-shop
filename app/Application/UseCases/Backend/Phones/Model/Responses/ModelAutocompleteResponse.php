<?php

namespace App\Application\UseCases\Backend\Phones\Model\Responses;

use Illuminate\Support\Collection;

final class ModelAutocompleteResponse
{
    /**
     * @var Collection
     */
    private $models;

    /**
     * ModelAutocompleteResponse constructor.
     * @param Collection $models
     */
    public function __construct(Collection $models)
    {
        $this->models = $models;
    }

    /**
     * @return Collection
     */
    public function getModels(): Collection
    {
        return $this->models;
    }
}