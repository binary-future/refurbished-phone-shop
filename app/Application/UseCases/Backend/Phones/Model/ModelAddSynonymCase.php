<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelAddSynonymCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelAddSynonymRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelManageSynonymResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Domain\Phone\Model\Command\UpdateModelCommand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\ByIdQuery;
use App\Domain\Phone\Model\Query\BySlugAndBrandIdQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class ModelAddSynonymCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ModelAddSynonymCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(ModelAddSynonymRequest $request): ModelManageSynonymResponse
    {
        $brand = $this->getBrand($request->getBrand());
        $model = $this->getModel($request->getModel(), $brand);
        $synonym = $this->getSynonym($request->getSynonym());
        if (! $this->isValid($model, $synonym)) {
            return $this->generateNegativeResponse($brand, $model);
        }

        return $this->addSynonym($brand, $model, $synonym);
    }

    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    private function getModel(string $modelSlug, Brand $brand): PhoneModel
    {
        /**
         * @var PhoneModel $model
         */
        $model = $this->queryBus->dispatch(new BySlugAndBrandIdQuery($modelSlug, $brand->getKey()))->first();
        if (! $model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    private function getSynonym(int $modelId): PhoneModel
    {
        /**
         * @var PhoneModel $synonym
         */
        $synonym = $this->queryBus->dispatch(
            new ByIdQuery(
                $modelId,
                ['originalPhones', 'synonyms', 'synonyms.originalPhones']
            )
        )->first();
        if (! $synonym) {
            throw new ModelNotFoundException();
        }

        return $synonym;
    }

    private function isValid(PhoneModel $model, PhoneModel $synonym): bool
    {
        return ! $model->isSynonym() && $model->getKey() !== $synonym->getKey();
    }

    private function generateNegativeResponse(Brand $brand, PhoneModel $model): ModelManageSynonymResponse
    {
        return new ModelManageSynonymResponse($brand, $model, false);
    }

    private function addSynonym(Brand $brand, PhoneModel $model, PhoneModel $synonym): ModelManageSynonymResponse
    {
        try {
            $this->assignBaseModel($model, $synonym);
            return new ModelManageSynonymResponse($brand, $model, true);
        } catch (\Throwable $exception) {
            return $this->generateNegativeResponse($brand, $model);
        }
    }

    private function assignBaseModel(PhoneModel $model, PhoneModel $synonym): PhoneModel
    {
        $synonyms = collect([$synonym]);
        $synonyms = $synonyms->merge($synonym->getSynonyms());
        foreach ($synonyms as $synonym) {
            /**
             * @var PhoneModel $synonym
             */
            $synonym->assignBaseModel($model);
            $this->saveModel($synonym);
        }

        return $model;
    }

    private function saveModel(PhoneModel $model): PhoneModel
    {
        $this->commandBus->dispatch(new UpdateModelCommand($model, []));

        return $model;
    }
}
