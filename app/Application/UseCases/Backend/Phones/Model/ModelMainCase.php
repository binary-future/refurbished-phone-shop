<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelMainCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelMainResponse;
use App\Domain\Phone\Brand\Query\AllQuery;
use Illuminate\Support\Collection;

final class ModelMainCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * ModelMainCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return ModelMainResponse
     */
    public function execute(): ModelMainResponse
    {
        $brands = $this->getBrands();

        return new ModelMainResponse($brands);
    }

    private function getBrands(): Collection
    {
        /**
         * @var Collection $brands
         */
        $brands = $this->queryBus->dispatch(new AllQuery(['models']));

        return $brands;
    }
}
