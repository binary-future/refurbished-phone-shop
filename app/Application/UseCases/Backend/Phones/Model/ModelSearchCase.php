<?php

namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelSearchCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelSearchRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSearchResponse;
use App\Domain\Phone\Model\Query\SearchQuery;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

final class ModelSearchCase implements Contract
{
    private const PER_PAGE = 24;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * ModelSearchCase constructor.
     * @param QueryBus $queryBus
     * @param Serializer $serializer
     */
    public function __construct(QueryBus $queryBus, Serializer $serializer)
    {
        $this->queryBus = $queryBus;
        $this->serializer = $serializer;
    }

    /**
     * @param ModelSearchRequest $request
     * @return ModelSearchResponse
     */
    public function execute(ModelSearchRequest $request): ModelSearchResponse
    {
        try {
            return $this->proceed($request);
        } catch (\Throwable $exception) {
            return $this->getDefaultResponse();
        }
    }

    /**
     * @param ModelSearchRequest $request
     * @return ModelSearchResponse
     */
    private function proceed(ModelSearchRequest $request): ModelSearchResponse
    {
        if (empty($request->getParams())) {
            return $this->getDefaultResponse();
        }
        /**
         * @var SearchQuery $query
         * @var LengthAwarePaginator $models
         */
        $query = $this->serializer->fromArray(SearchQuery::class, $request->getParams());
        $query->setPerPage(self::PER_PAGE);
        $query->setOnlyBaseModel(false);
        $query->setShouldSort(false);
        $query->setRelations(['brand', 'phones', 'phones.images']);
        $models = $this->queryBus->dispatch($query);

        return new ModelSearchResponse($models);
    }

    private function getDefaultResponse(): ModelSearchResponse
    {
        return new ModelSearchResponse(new \Illuminate\Pagination\LengthAwarePaginator(collect(), 0, self::PER_PAGE));
    }
}
