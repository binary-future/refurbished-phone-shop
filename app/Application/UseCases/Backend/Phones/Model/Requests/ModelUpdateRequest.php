<?php

namespace App\Application\UseCases\Backend\Phones\Model\Requests;

use App\Application\Services\Model\LatestModel\LatestModel;
use App\Domain\Phone\Faq\Faq;
use App\Domain\Phone\Faq\FaqAnswer;
use App\Domain\Phone\Faq\FaqQuestion;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Shared\Description\Description;

/**
 * Class ModelUpdateRequest
 * @package App\Application\UseCases\Backend\Phones\Requests
 */
final class ModelUpdateRequest
{
    public const INPUT_NAME = 'name';
    public const INPUT_BRAND_ID = 'brand_id';
    public const INPUT_MODEL_ID = 'model_id';
    public const INPUT_IS_ACTIVE = 'is_active';
    public const INPUT_IS_LATEST = 'is_latest';
    public const INPUT_DESCRIPTION_CONTENT = 'description.' . Description::FIELD_CONTENT;
    private const INPUT_FAQS = 'faqs';
    public const INPUT_FAQ_ID = self::INPUT_FAQS . '.*.' . Faq::FIELD_ID;
    public const INPUT_FAQ_QUESTION = self::INPUT_FAQS . '.*.' . Faq::FIELD_QUESTION;
    public const INPUT_FAQ_ANSWER = self::INPUT_FAQS . '.*.' . Faq::FIELD_ANSWER;

    /**
     * @var string
     */
    private $brand;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string|null
     */
    private $slug;

    /**
     * @var bool
     */
    private $isActive;

    /**
     * @var array
     */
    private $description = [];
    /**
     * @var bool
     */
    private $isLatest;
    /**
     * @var array
     */
    private $faqs;

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @param string $model
     */
    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }

    /**
     * @param array|null $description
     */
    public function setDescription(?array $description): void
    {
        $this->description = $description;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @param bool $isLatest
     */
    public function setIsLatest(bool $isLatest): void
    {
        $this->isLatest = $isLatest;
    }

    /**
     * @return bool
     */
    public function isLatest(): bool
    {
        return $this->isLatest;
    }

    public function setFaqs(array $faqs): void
    {
        $this->faqs = [];
        foreach ($faqs as $faq) {
            // remove faqs with empty answers and questions
            if (! $faq[Faq::FIELD_QUESTION] && ! $faq[Faq::FIELD_ANSWER]) {
                continue;
            }

            $transformedFaq = [
                Faq::FIELD_QUESTION => new FaqQuestion($faq[Faq::FIELD_QUESTION] ?? ''),
                Faq::FIELD_ANSWER => new FaqAnswer($faq[Faq::FIELD_ANSWER] ?? ''),
            ];
            if (isset($faq[Faq::FIELD_ID])) {
                $transformedFaq[Faq::FIELD_ID] = (int)$faq[Faq::FIELD_ID];
            }

            $this->faqs[] = $transformedFaq;
        }
    }

    public function getFaqs(): array
    {
        return $this->faqs;
    }

    public function getParams(): array
    {
        $params = array_filter([
            PhoneModel::FIELD_NAME => $this->getName(),
            PhoneModel::FIELD_SLUG => $this->getSlug(),
        ]);
        if ($this->isActive !== null) {
            $params[PhoneModel::FIELD_IS_ACTIVE] = $this->isActive;
        }
        if ($this->isLatest !== null) {
            $params[LatestModel::FIELD_IS_LATEST] = $this->isLatest;
        }

        return $params;
    }
}
