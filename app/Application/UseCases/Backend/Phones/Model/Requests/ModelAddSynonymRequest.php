<?php


namespace App\Application\UseCases\Backend\Phones\Model\Requests;


/**
 * Class ModelAddSynonymRequest
 * @package App\Application\UseCases\Backend\Phones\Model\Requests
 */
final class ModelAddSynonymRequest
{
    /**
     * @var string
     */
    private $brand;

    /**
     * @var string
     */
    private $model;

    /**
     * @var int
     */
    private $synonym;

    /**
     * ModelAddSynonymRequest constructor.
     * @param string $brand
     * @param string $model
     * @param int $synonym
     */
    public function __construct(string $brand, string $model, int $synonym)
    {
        $this->brand = $brand;
        $this->model = $model;
        $this->synonym = $synonym;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return string
     */
    public function getModel(): string
    {
        return $this->model;
    }

    /**
     * @return int
     */
    public function getSynonym(): int
    {
        return $this->synonym;
    }
}
