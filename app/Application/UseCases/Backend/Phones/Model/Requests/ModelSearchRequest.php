<?php


namespace App\Application\UseCases\Backend\Phones\Model\Requests;


use App\Domain\Phone\Model\PhoneModel;

/**
 * Class ModelSearchRequest
 * @package App\Application\UseCases\Backend\Phones\Requests
 */
final class ModelSearchRequest
{
    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [
            PhoneModel::FIELD_NAME => $this->name
        ];

        return array_filter($params);
    }
}
