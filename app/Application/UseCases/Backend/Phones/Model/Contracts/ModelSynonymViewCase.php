<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSynonymViewResponse;

interface ModelSynonymViewCase
{
    public function execute(string $brand, string $model): ModelSynonymViewResponse;
}
