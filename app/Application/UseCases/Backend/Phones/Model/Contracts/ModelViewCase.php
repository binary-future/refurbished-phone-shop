<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelViewResponse;

/**
 * Interface ModelViewCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface ModelViewCase
{
    /**
     * @param string $brand
     * @param string $model
     * @return ModelViewResponse
     */
    public function execute(string $brand, string $model): ModelViewResponse;
}
