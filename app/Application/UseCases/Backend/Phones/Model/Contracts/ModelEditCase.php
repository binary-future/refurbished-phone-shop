<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelEditResponse;

/**
 * Interface ModelEditCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface ModelEditCase
{
    /**
     * @param string $brand
     * @param string $model
     * @return ModelEditResponse
     */
    public function execute(string $brand, string $model): ModelEditResponse;
}