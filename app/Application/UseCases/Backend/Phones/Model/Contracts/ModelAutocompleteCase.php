<?php

namespace App\Application\UseCases\Backend\Phones\Model\Contracts;

use App\Application\UseCases\Backend\Phones\Model\Responses\ModelAutocompleteResponse;

interface ModelAutocompleteCase
{
    public function execute(?string $name): ModelAutocompleteResponse;
}