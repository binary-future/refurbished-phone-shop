<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Requests\ModelSearchRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSearchResponse;

interface ModelSearchCase
{
    public function execute(ModelSearchRequest $request): ModelSearchResponse;
}