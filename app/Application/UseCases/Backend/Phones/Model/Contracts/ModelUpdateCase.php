<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Requests\ModelUpdateRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelUpdateResponse;

/**
 * Interface ModelUpdateCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface ModelUpdateCase
{
    /**
     * @param ModelUpdateRequest $request
     * @return ModelUpdateResponse
     */
    public function execute(ModelUpdateRequest $request): ModelUpdateResponse;
}
