<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Requests\ModelRemoveSynonymRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelManageSynonymResponse;

/**
 * Interface ModelRemoveSynonymCase
 * @package App\Application\UseCases\Backend\Phones\Model\Contracts
 */
interface ModelRemoveSynonymCase
{
    /**
     * @param ModelRemoveSynonymRequest $request
     * @return ModelManageSynonymResponse
     */
    public function execute(ModelRemoveSynonymRequest $request): ModelManageSynonymResponse;
}
