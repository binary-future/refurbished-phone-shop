<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Responses\OldPhonesListResponse;

/**
 * Interface OldPhonesListCase
 * @package App\Application\UseCases\Backend\Phones\Model\Contracts
 */
interface OldPhonesListCase
{
    public function execute(): OldPhonesListResponse;
}
