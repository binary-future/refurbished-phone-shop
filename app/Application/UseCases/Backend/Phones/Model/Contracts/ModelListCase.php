<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelListResponse;

/**
 * Interface ModelListCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface ModelListCase
{
    /**
     * @param string $brand
     * @return ModelListResponse
     */
    public function execute(string $brand): ModelListResponse;
}