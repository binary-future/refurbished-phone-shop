<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Requests\ModelAddSynonymRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelManageSynonymResponse;

/**
 * Interface ModelAddSynonymCase
 * @package App\Application\UseCases\Backend\Phones\Model\Contracts
 */
interface ModelAddSynonymCase
{
    /**
     * @param ModelAddSynonymRequest $request
     * @return ModelManageSynonymResponse
     */
    public function execute(ModelAddSynonymRequest $request): ModelManageSynonymResponse;
}
