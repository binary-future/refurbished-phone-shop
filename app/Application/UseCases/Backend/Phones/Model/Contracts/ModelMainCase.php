<?php


namespace App\Application\UseCases\Backend\Phones\Model\Contracts;


use App\Application\UseCases\Backend\Phones\Model\Responses\ModelMainResponse;

interface ModelMainCase
{
    public function execute(): ModelMainResponse;
}