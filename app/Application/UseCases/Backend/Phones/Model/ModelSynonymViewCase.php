<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelSynonymViewCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelSynonymViewResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery as BrandBySlugQuery;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\BySlugAndBrandIdQuery;
use App\Domain\Phone\Model\Query\PotentialSynonymsQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class ModelSynonymViewCase implements Contract
{
    private const PERCENT = 85;
    private const MIN_NAME_LENGTH = 3;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * ModelSynonymViewCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(string $brand, string $model): ModelSynonymViewResponse
    {
        $brand = $this->getBrand($brand);
        $model = $this->getModel($model, $brand->getKey());
        $synonyms = $model->getSynonyms();
        $potentialSynonyms = $this->getPotentialSynonyms($model);

        return new ModelSynonymViewResponse($brand, $model, $synonyms, $potentialSynonyms);
    }

    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BrandBySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    private function getModel(string $modelSlug, int $brandId): PhoneModel
    {
        /**
         * @var PhoneModel $model
         */
        $model = $this->queryBus->dispatch(
            new BySlugAndBrandIdQuery(
                $modelSlug,
                $brandId,
                ['synonyms', 'brand', 'synonyms.brand']
            )
        )->first();

        if (! $model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    private function getPotentialSynonyms(PhoneModel $model): Collection
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus
            ->dispatch(new PotentialSynonymsQuery(
                $model,
                $this->generateSearchName($model),
                ['brand'])
            );

        return $models->filter(function (PhoneModel $potentialSynonym) use ($model) {
            return $potentialSynonym->getParentId() !== $model->getKey();
        });
    }

    private function generateSearchName(PhoneModel $model): string
    {
        $name = $model->getName();
        $name = strlen($model->getName()) <= self::MIN_NAME_LENGTH
            ? $name
            : trim(substr($name, 0, round(strlen($name) * static::PERCENT / 100)));

        return sprintf('%s %s', $model->getBrand()->getName(), $name);
    }
}
