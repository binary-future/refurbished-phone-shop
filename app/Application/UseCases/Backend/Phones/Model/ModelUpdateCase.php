<?php

namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Query\LatestModelByModelIdQuery;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelUpdateCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelUpdateRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelUpdateResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Domain\Phone\Faq\Command\DeleteFaqsByCommand;
use App\Domain\Phone\Faq\Contracts\Faq;
use App\Domain\Phone\Faq\FaqModelId;
use App\Domain\Phone\Faq\Query\ByIdsQuery;
use App\Domain\Phone\Faq\Query\ByModelIdQuery;
use App\Domain\Phone\Model\Command\UpdateModelCommand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\BySlugAndBrandIdQuery;
use App\Domain\Shared\Description\Description;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class ModelUpdateCase
 * @package App\Application\UseCases\Backend\Phones
 */
final class ModelUpdateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Generator
     */
    private $slugGenerator;
    /**
     * @var LatestModels
     */
    private $latestModels;

    /**
     * ModelUpdateCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     * @param Generator $slugGenerator
     * @param LatestModels $latestModels
     */
    public function __construct(
        QueryBus $queryBus,
        CommandBus $commandBus,
        Generator $slugGenerator,
        LatestModels $latestModels
    ) {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->slugGenerator = $slugGenerator;
        $this->latestModels = $latestModels;
    }

    /**
     * @param ModelUpdateRequest $request
     * @return ModelUpdateResponse
     */
    public function execute(ModelUpdateRequest $request): ModelUpdateResponse
    {
        $brand = $this->getBrand($request->getBrand());
        $model = $this->getModel($request->getModel(), $brand->getKey());

        return $this->proceedUpdating($brand, $model, $request);
    }

    /**
     * @param string $brandSlug
     * @return Brand
     */
    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand|null $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw (new ModelNotFoundException())->setModel(Brand::class);
        }

        return $brand;
    }

    /**
     * @param string $modelSlug
     * @param int $brandId
     * @return PhoneModel
     */
    private function getModel(string $modelSlug, int $brandId): PhoneModel
    {
        /**
         * @var PhoneModel|null $model
         */
        $model = $this->queryBus
            ->dispatch(new BySlugAndBrandIdQuery($modelSlug, $brandId))
            ->first();
        if (! $model) {
            throw (new ModelNotFoundException())->setModel(PhoneModel::class);
        }

        return $model;
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @param ModelUpdateRequest $request
     * @return ModelUpdateResponse
     */
    private function proceedUpdating(Brand $brand, PhoneModel $model, ModelUpdateRequest $request): ModelUpdateResponse
    {
        try {
            return $this->updateModel($brand, $model, $request);
        } catch (\Throwable $exception) {
            $response = new ModelUpdateResponse($model, $brand, null);
            $response->setSuccess(false);

            return $response;
        }
    }

    /**
     * @param Brand $brand
     * @param PhoneModel $model
     * @param ModelUpdateRequest $request
     * @return ModelUpdateResponse
     */
    private function updateModel(Brand $brand, PhoneModel $model, ModelUpdateRequest $request): ModelUpdateResponse
    {
        if ($request->getName()) {
            $request->setSlug($this->slugGenerator->generate($request->getName()));
        }
        $related = $this->getRelated($model, $request);
        /**
         * @var PhoneModel $updatedModel
         */
        $updatedModel = $this->commandBus->dispatch(new UpdateModelCommand($model, $request->getParams(), $related));

        $latestModel = $this->handleLatestModel($request->isLatest(), $model->getKey());

        return new ModelUpdateResponse($updatedModel, $brand, $latestModel);
    }

    private function handleLatestModel(bool $isLatest, int $modelId): ?LatestModel
    {
        if ($isLatest) {
            /** @var LatestModel $latestModel */
            $latestModel = $this->latestModels->create([
                LatestModel::FIELD_MODEL_ID => $modelId
            ]);

            return $latestModel;
        }

        $lModels = $this->latestModels->findBy(
            new LatestModelByModelIdQuery($modelId)
        );

        if ($lModels->isNotEmpty()) {
            /** @var LatestModel $lModel */
            $lModel = $lModels->first();
            // TODO: modify to $this->latestModels->delete
            $lModel->delete();
        }

        return null;
    }

    private function getRelated(PhoneModel $model, ModelUpdateRequest $request): array
    {
        return [
            PhoneModel::RELATION_DESCRIPTION => $this->getDescription($model, $request->getDescription()),
            PhoneModel::RELATION_FAQS => $this->getFaqs($model, $request->getFaqs()),
        ];
    }

    private function getDescription(PhoneModel $model, array $params): Description
    {
        $description = new Description();
        $description->fill($params);
        $description->setOwner($model);

        return $description;
    }

    private function getFaqs(PhoneModel $model, array $requestFaqs): array
    {
        $requestFaqs = new Collection($requestFaqs);
        $faqIdsForUpdate = $requestFaqs->pluck(Faq::FIELD_ID)->filter();

        /** @var Collection|Faq[] $existingFaqs */
        $existingFaqs = $this->queryBus->dispatch(new ByModelIdQuery($model->getKey()));

        // delete faqs which are not in request
        $faqIdsForDelete = $existingFaqs->pluck(Faq::FIELD_ID)->filter()
            ->diff($faqIdsForUpdate)->all();

        $this->commandBus->dispatch(new DeleteFaqsByCommand(new ByIdsQuery($faqIdsForDelete)));

        $resultFaqs = [];
        foreach ($requestFaqs as $faqParams) {
            if (isset($faqParams[Faq::FIELD_ID])) {
                $faq = $existingFaqs->firstWhere(Faq::FIELD_ID, $faqParams[Faq::FIELD_ID]);
            } else {
                $faq = new \App\Domain\Phone\Faq\Faq();
            }

            if (! $faq) {
                // someone hacked faq id
                throw (new ModelNotFoundException())->setModel(Faq::class);
            }

            $faq->setAnswer($faqParams[Faq::FIELD_ANSWER]);
            $faq->setQuestion($faqParams[Faq::FIELD_QUESTION]);
            $faq->setPhoneModelId(new FaqModelId($model->getKey()));

            $resultFaqs[] = $faq;
        }

        return $resultFaqs;
    }
}
