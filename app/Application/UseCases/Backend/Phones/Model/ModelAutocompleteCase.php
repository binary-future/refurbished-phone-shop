<?php

namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelAutocompleteCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelAutocompleteResponse;
use App\Domain\Phone\Model\Query\SearchQuery;
use Illuminate\Support\Collection;

final class ModelAutocompleteCase implements Contract
{
    private const LIMIT = 10;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * ModelAutocompleteCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(?string $name): ModelAutocompleteResponse
    {
        try {
            return $this->proceed($name);
        } catch (\Throwable $exception) {
            return $this->getDefaultResponse();
        }
    }

    /**
     * @param string|null $name
     * @return ModelAutocompleteResponse
     */
    private function proceed(?string $name): ModelAutocompleteResponse
    {
        if (! $name) {
            return $this->getDefaultResponse();
        }
        /**
         * @var SearchQuery $query
         * @var Collection $models
         */
        $query = new SearchQuery();
        $query->setName($name);
        $query->setLimit(self::LIMIT);
        $query->setShouldSort(false);
        $query->setRelations(['brand', 'phones', 'phones.images']);
        $models = $this->queryBus->dispatch($query);

        return new ModelAutocompleteResponse($models);
    }

    private function getDefaultResponse(): ModelAutocompleteResponse
    {
        return new ModelAutocompleteResponse(collect());
    }
}
