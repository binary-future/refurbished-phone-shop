<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\OldPhonesListCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\OldPhonesListResponse;
use App\Domain\Phone\Model\Query\CreatedBeforeDateQuery;
use Carbon\Carbon;
use Illuminate\Support\Collection;


final class OldPhonesListCase implements Contract
{
    private const MAX_DATE = '15-05-2019';

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * OldPhonesListCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(): OldPhonesListResponse
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus
            ->dispatch(new CreatedBeforeDateQuery(Carbon::parse(self::MAX_DATE), ['phones', 'brand', 'phones.images']));

        return new OldPhonesListResponse($models);
    }
}
