<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Query\LatestModelByModelIdQuery;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelEditCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelEditResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\BySlugAndBrandIdQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class ModelEditCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var LatestModels
     */
    private $latestModels;

    /**
     * ModelViewCase constructor.
     * @param QueryBus $queryBus
     * @param LatestModels $latestModels
     */
    public function __construct(QueryBus $queryBus, LatestModels $latestModels)
    {
        $this->queryBus = $queryBus;
        $this->latestModels = $latestModels;
    }

    public function execute(string $brand, string $model): ModelEditResponse
    {
        $brandEntity = $this->getBrand($brand);
        $modelEntity = $this->getModel($model, $brandEntity->getKey());
        $isLatestModel = $this->isLatest($modelEntity->getKey());

        return new ModelEditResponse($brandEntity, $modelEntity, $isLatestModel);
    }

    /**
     * @param string $brandSlug
     * @return Brand
     */
    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    /**
     * @param string $modelSlug
     * @param int $brandId
     * @return PhoneModel
     */
    private function getModel(string $modelSlug, int $brandId): PhoneModel
    {
        /**
         * @var PhoneModel $model
         */
        $model = $this->queryBus
            ->dispatch(new BySlugAndBrandIdQuery($modelSlug, $brandId, ['phones', 'phones.images', 'phones.color']))
            ->first();
        if (! $model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    /**
     * @param int $modelId
     * @return bool
     */
    private function isLatest(int $modelId): bool
    {
        /**
         * @var LatestModel $model
         */
        $model = $this->latestModels
            ->findBy(new LatestModelByModelIdQuery($modelId))
            ->first();

        return (bool) $model;
    }
}
