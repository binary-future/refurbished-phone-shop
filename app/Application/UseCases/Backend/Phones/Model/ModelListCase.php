<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Query\LatestModelByBrandIdQuery;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelListCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelListResponse;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelsResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\ByBrandIdQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class ModelListCase
 * @package App\Application\UseCases\Backend\Phones
 */
final class ModelListCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var LatestModels
     */
    private $latestModels;

    /**
     * ModelListCase constructor.
     * @param QueryBus $queryBus
     * @param LatestModels $latestModels
     */
    public function __construct(QueryBus $queryBus, LatestModels $latestModels)
    {
        $this->queryBus = $queryBus;
        $this->latestModels = $latestModels;
    }

    /**
     * @param string $brand
     * @return ModelListResponse
     */
    public function execute(string $brand): ModelListResponse
    {
        $brandEntity = $this->getBrand($brand);
        $phoneModels = $this->getPhoneModels($brandEntity->getKey());
        $latestModels = $this->getLatestModels($brandEntity);
        $models = $this->combineModels($phoneModels, $latestModels);

        return new ModelListResponse($brandEntity, $models);
    }

    /**
     * @param string $brandSlug
     * @return Brand
     */
    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    /**
     * @param int $brandId
     * @return Collection
     */
    private function getPhoneModels(int $brandId): Collection
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch(new ByBrandIdQuery($brandId, ['phones', 'originalPhones']));

        return $models;
    }

    /**
     * @param Brand $brandEntity
     * @return Collection
     */
    private function getLatestModels(Brand $brandEntity): Collection
    {
        return $this->latestModels->findBy(
            new LatestModelByBrandIdQuery($brandEntity->getKey(), ['model'])
        );
    }

    /**
     * @param Collection $phoneModels
     * @param Collection $latestModels
     * @return Collection
     */
    private function combineModels(Collection $phoneModels, Collection $latestModels): Collection
    {
        return $phoneModels->map(function (PhoneModel $phoneModel) use ($latestModels) {
            $latestModel = $this->findLatestModel($phoneModel, $latestModels);

            return new ModelsResponse($phoneModel, $latestModel);
        });
    }

    /**
     * @param PhoneModel $phoneModel
     * @param Collection $latestModels
     * @return LatestModel|null
     */
    private function findLatestModel(PhoneModel $phoneModel, Collection $latestModels): ?LatestModel
    {
        return $latestModels->first(static function (
            LatestModel $latestModel
        ) use ($phoneModel) {
            return $latestModel->getModel()->getKey() === $phoneModel->getKey();
        });
    }
}
