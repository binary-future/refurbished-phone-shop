<?php

namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelRemoveSynonymCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Requests\ModelRemoveSynonymRequest;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelManageSynonymResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Domain\Phone\Model\Command\UpdateModelCommand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\ByIdQuery;
use App\Domain\Phone\Model\Query\BySlugAndBrandIdQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class ModelRemoveSynonymCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ModelRemoveSynonymCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(ModelRemoveSynonymRequest $request): ModelManageSynonymResponse
    {
        $brand = $this->getBrand($request->getBrand());
        $model = $this->getModel($request->getModel(), $brand);
        $synonym = $this->getSynonym($request->getSynonym());

        return $this->removeSynonym($brand, $model, $synonym);
    }

    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    private function getModel(string $modelSlug, Brand $brand): PhoneModel
    {
        /**
         * @var PhoneModel $model
         */
        $model = $this->queryBus->dispatch(new BySlugAndBrandIdQuery($modelSlug, $brand->getKey()))->first();
        if (! $model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    private function getSynonym(int $modelId): PhoneModel
    {
        /**
         * @var PhoneModel $synonym
         */
        $synonym = $this->queryBus->dispatch(
            new ByIdQuery(
                $modelId,
                ['originalPhones']
            )
        )->first();
        if (! $synonym) {
            throw new ModelNotFoundException();
        }

        return $synonym;
    }

    private function generateNegativeResponse(Brand $brand, PhoneModel $model): ModelManageSynonymResponse
    {
        return new ModelManageSynonymResponse($brand, $model, false);
    }

    private function removeSynonym(Brand $brand, PhoneModel $model, PhoneModel $synonym): ModelManageSynonymResponse
    {
        try {
            $synonym->refreshBaseModel();
            $this->saveModel($synonym);
            return new ModelManageSynonymResponse($brand, $model, true);
        } catch (\Throwable $exception) {
            return $this->generateNegativeResponse($brand, $model);
        }
    }

    private function saveModel(PhoneModel $model): PhoneModel
    {
        $this->commandBus->dispatch(new UpdateModelCommand($model, []));

        return $model;
    }

}
