<?php


namespace App\Application\UseCases\Backend\Phones\Model;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Query\LatestModelByModelIdQuery;
use App\Application\Services\Model\LatestModel\Repository\LatestModels;
use App\Application\UseCases\Backend\Phones\Model\Contracts\ModelViewCase as Contract;
use App\Application\UseCases\Backend\Phones\Model\Responses\ModelViewResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\BySlugAndBrandIdQuery;
use App\Domain\Phone\Phone\Phone;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class ModelViewCase
 * @package App\Application\UseCases\Backend\Phones
 */
final class ModelViewCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;
    /**
     * @var LatestModels
     */
    private $latestModels;

    /**
     * ModelViewCase constructor.
     * @param QueryBus $queryBus
     * @param LatestModels $latestModels
     */
    public function __construct(QueryBus $queryBus, LatestModels $latestModels)
    {
        $this->queryBus = $queryBus;
        $this->latestModels = $latestModels;
    }

    /**
     * @param string $brand
     * @param string $model
     * @return ModelViewResponse
     */
    public function execute(string $brand, string $model): ModelViewResponse
    {
        $brandEntity = $this->getBrand($brand);
        $modelEntity = $this->getModel($model, $brandEntity->getKey());
        $originalPhones = $this->getOriginalPhones($modelEntity);
        $synonymsPhones = $this->getPhones($modelEntity, $originalPhones);
        $colors = $this->getColors($modelEntity->getPhones());
        $latestModel = $this->getLatestModel($modelEntity->getKey());

        return new ModelViewResponse(
            $brandEntity,
            $modelEntity,
            $latestModel,
            $originalPhones,
            $synonymsPhones,
            $colors
        );
    }

    /**
     * @param string $brandSlug
     * @return Brand
     */
    private function getBrand(string $brandSlug): Brand
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    /**
     * @param string $modelSlug
     * @param int $brandId
     * @return PhoneModel
     */
    private function getModel(string $modelSlug, int $brandId): PhoneModel
    {
        /**
         * @var PhoneModel $model
         */
        $model = $this->queryBus
            ->dispatch(new BySlugAndBrandIdQuery($modelSlug, $brandId, ['phones', 'phones.images', 'phones.color']))
            ->first();
        if (! $model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    private function getColors(Collection $phones): Collection
    {
        return $phones->map(function (Phone $phone) {
            return $phone->getColor();
        })->unique();
    }

    private function getOriginalPhones(PhoneModel $model): Collection
    {
        return $model->getOriginalPhones();
    }

    /**
     * @param PhoneModel $model
     * @param Collection $phones
     * @return Collection
     */
    private function getPhones(PhoneModel $model, Collection $phones): Collection
    {
        $allPhones = $model->getPhones();

        return $allPhones->diff($phones);
    }

    /**
     * @param int $modelId
     * @return LatestModel|null
     */
    private function getLatestModel(int $modelId): ?LatestModel
    {
        return $this->latestModels
            ->findBy(new LatestModelByModelIdQuery($modelId))
            ->first();
    }
}
