<?php


namespace App\Application\UseCases\Backend\Phones\Brand;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\Services\Transformers\DescriptionTransformer;
use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandUpdateCase as Contract;
use App\Application\UseCases\Backend\Phones\Brand\Requests\UpdateBrandRequest;
use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandUpdateResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Command\UpdateBrandCommand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class BrandUpdateCase
 * @package App\Application\UseCases\Backend\Phones\Brand
 */
final class BrandUpdateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * @var DescriptionTransformer
     */
    private $descriptionTransformer;

    /**
     * StoreUpdateCase constructor.
     * @param QueryBus $queryBus
     * @param Uploader $uploader
     * @param CommandBus $commandBus
     * @param Generator $generator
     * @param DescriptionTransformer $descriptionTransformer
     */
    public function __construct(
        QueryBus $queryBus,
        Uploader $uploader,
        CommandBus $commandBus,
        Generator $generator,
        DescriptionTransformer $descriptionTransformer
    ) {
        $this->queryBus = $queryBus;
        $this->uploader = $uploader;
        $this->commandBus = $commandBus;
        $this->slugGenerator = $generator;
        $this->descriptionTransformer = $descriptionTransformer;
    }

    /**
     * @param UpdateBrandRequest $request
     * @return BrandUpdateResponse
     */
    public function execute(UpdateBrandRequest $request): BrandUpdateResponse
    {
        $brand = $this->getBrand($request->getSlug());
        try {
            return $this->proceedUpdating($brand, $request);
        } catch (\Throwable $exception) {
            $response = new BrandUpdateResponse($brand);
            $response->setSuccess(false);

            return $response;
        }
    }

    /**
     * @param string $slug
     * @return Brand
     */
    private function getBrand(string $slug): Brand
    {
        $brand = $this->queryBus->dispatch(new BySlugQuery($slug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return $brand;
    }

    /**
     * @param Brand $brand
     * @param UpdateBrandRequest $request
     * @return BrandUpdateResponse
     * @throws \App\Utils\File\Upload\Exception\FileUploadException
     */
    private function proceedUpdating(Brand $brand, UpdateBrandRequest $request)
    {
        $params = $this->prepareParams($request->getParams());
        $related = $this->getRelated($brand, $request);
        $this->commandBus->dispatch(new UpdateBrandCommand($brand, $params, $related));

        return new BrandUpdateResponse($brand);
    }

    /**
     * @param array $params
     * @return array
     */
    private function prepareParams(array $params)
    {
        if (isset($params['name'])) {
            $params['slug'] = $this->slugGenerator->generate($params['name']);
        }

        return $params;
    }

    /**
     * @param Brand $brand
     * @param UpdateBrandRequest $request
     * @return array
     * @throws \App\Utils\File\Upload\Exception\FileUploadException
     */
    private function getRelated(Brand $brand, UpdateBrandRequest $request): array
    {
        $related = [];
        if ($request->getLogo()) {
            $image = $this->uploader->storeImage($request->getLogo(), $brand);
            $image->setOwner($brand);
            $related['image'] = $image;
        }

        $related['description'] = $this->descriptionTransformer->transform($request->getDescription(), $brand);

        return $related;
    }
}
