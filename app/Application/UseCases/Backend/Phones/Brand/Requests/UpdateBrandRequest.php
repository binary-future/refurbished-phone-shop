<?php

namespace App\Application\UseCases\Backend\Phones\Brand\Requests;

use App\Domain\Phone\Brand\Brand;
use Illuminate\Http\UploadedFile;

final class UpdateBrandRequest
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var UploadedFile|null
     */
    private $logo;

    /**
     * @var array
     */
    private $description;

    /**
     * UpdateBrandRequest constructor.
     * @param string $slug
     * @param string $name
     * @param UploadedFile|null $logo
     * @param array $description
     */
    public function __construct(string $slug, ?string $name, ?UploadedFile $logo, array $description)
    {
        $this->slug = $slug;
        $this->name = $name;
        $this->logo = $logo;
        $this->description = $description;
    }


    public static function fromArray(array $params): self
    {
        return new self(
            $params['slug'],
            $params['name'] ?? null,
            $params['logo'] ?? null,
            $params['description'] ?? []
        );
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return UploadedFile|null
     */
    public function getLogo(): ?UploadedFile
    {
        return $this->logo;
    }

    /**
     * @return array
     */
    public function getDescription(): array
    {
        return $this->description;
    }

    public function getParams(): array
    {
        $params = [
            Brand::FIELD_NAME => $this->name,
            Brand::FIELD_SLUG => $this->slug,
        ];

        return array_filter($params);
    }
}
