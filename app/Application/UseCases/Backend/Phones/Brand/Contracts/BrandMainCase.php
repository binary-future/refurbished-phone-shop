<?php

namespace App\Application\UseCases\Backend\Phones\Brand\Contracts;

use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandMainResponse;

/**
 * Interface BrandMainCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface BrandMainCase
{
    /**
     * @return BrandMainResponse
     */
    public function execute(): BrandMainResponse;
}
