<?php


namespace App\Application\UseCases\Backend\Phones\Brand\Contracts;


use App\Application\UseCases\Backend\Phones\Brand\Requests\UpdateBrandRequest;
use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandUpdateResponse;

interface BrandUpdateCase
{
    public function execute(UpdateBrandRequest $request): BrandUpdateResponse;
}