<?php

namespace App\Application\UseCases\Backend\Phones\Brand\Contracts;

use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandEditResponse;

/**
 * Interface BrandEditCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface BrandEditCase
{
    /**
     * @param string $brandSlug
     * @return BrandEditResponse
     */
    public function execute(string $brandSlug): BrandEditResponse;
}
