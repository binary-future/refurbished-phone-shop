<?php

namespace App\Application\UseCases\Backend\Phones\Brand;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandEditCase as Contract;
use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandEditResponse;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Brand\Query\BySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class BrandEditCase
 * @package App\Application\UseCases\Backend\Phones
 */
final class BrandEditCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * BrandEditCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param string $brandSlug
     * @return BrandEditResponse
     */
    public function execute(string $brandSlug): BrandEditResponse
    {
        /**
         * @var Brand $brand
         */
        $brand = $this->queryBus->dispatch(new BySlugQuery($brandSlug))->first();
        if (! $brand) {
            throw new ModelNotFoundException();
        }

        return new BrandEditResponse($brand);
    }
}
