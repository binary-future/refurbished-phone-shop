<?php

namespace App\Application\UseCases\Backend\Phones\Brand;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Brand\Contracts\BrandMainCase as Contract;
use App\Application\UseCases\Backend\Phones\Brand\Responses\BrandMainResponse;
use App\Domain\Phone\Brand\Query\AllQuery;
use Illuminate\Support\Collection;

final class BrandMainCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * BrandMainCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return BrandMainResponse
     */
    public function execute(): BrandMainResponse
    {
        $brands = $this->getBrands();

        return new BrandMainResponse($brands);
    }

    /**
     * @return Collection
     */
    private function getBrands(): Collection
    {
        /**
         * @var Collection $brands
         */
        $brands = $this->queryBus->dispatch(new AllQuery(['image']));

        return $brands;
    }
}
