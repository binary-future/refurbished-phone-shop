<?php

namespace App\Application\UseCases\Backend\Phones\Brand\Responses;

use Illuminate\Support\Collection;

final class BrandMainResponse
{
    /**
     * @var Collection
     */
    private $brands;

    /**
     * BrandMainResponse constructor.
     * @param Collection $brands
     */
    public function __construct(Collection $brands)
    {
        $this->brands = $brands;
    }

    /**
     * @return Collection
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }
}
