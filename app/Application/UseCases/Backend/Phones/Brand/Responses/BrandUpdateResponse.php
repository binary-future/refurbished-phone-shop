<?php


namespace App\Application\UseCases\Backend\Phones\Brand\Responses;


use App\Domain\Phone\Brand\Brand;

final class BrandUpdateResponse
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * BrandUpdateResponse constructor.
     * @param Brand $brand
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
