<?php


namespace App\Application\UseCases\Backend\Phones\Brand\Responses;


use App\Domain\Phone\Brand\Brand;

/**
 * Class BrandEditResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class BrandEditResponse
{
    /**
     * @var Brand
     */
    private $brand;

    /**
     * BrandEditResponse constructor.
     * @param Brand $brand
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }
}
