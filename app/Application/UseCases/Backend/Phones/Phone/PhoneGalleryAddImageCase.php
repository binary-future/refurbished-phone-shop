<?php


namespace App\Application\UseCases\Backend\Phones\Phone;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryAddImageCase as Contract;
use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryAddImageResponse;
use App\Domain\Phone\Phone\Command\UpdatePhoneCommand;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Query\PhoneByIdQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;

/**
 * Class PhoneGalleryAddImageCase
 * @package App\Application\UseCases\Backend\Phones
 */
final class PhoneGalleryAddImageCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * PhoneGalleryAddImageCase constructor.
     * @param QueryBus $queryBus
     * @param Uploader $uploader
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, Uploader $uploader, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->uploader = $uploader;
        $this->commandBus = $commandBus;
    }

    /**
     * @param UploadedFile $file
     * @param int $phone
     * @return PhoneGalleryAddImageResponse
     */
    public function execute(UploadedFile $file, int $phone): PhoneGalleryAddImageResponse
    {
        $phoneEntity = $this->getPhone($phone);

        return $this->proceed($file, $phoneEntity);
    }

    /**
     * @param int $phoneKey
     * @return Phone
     */
    private function getPhone(int $phoneKey): Phone
    {
        /**
         * @var Phone $phone
         */
        $phone = $this->queryBus->dispatch(new PhoneByIdQuery($phoneKey))->first();
        if (! $phone) {
            throw new ModelNotFoundException();
        }

        return $phone;
    }

    /**
     * @param UploadedFile $file
     * @param Phone $phone
     * @return PhoneGalleryAddImageResponse
     */
    private function proceed(UploadedFile $file, Phone $phone): PhoneGalleryAddImageResponse
    {
        try {
            return $this->storeImage($file, $phone);
        } catch (\Throwable $exception) {
            return new PhoneGalleryAddImageResponse($phone, false);
        }
    }

    /**
     * @param UploadedFile $file
     * @param Phone $phone
     * @return PhoneGalleryAddImageResponse
     * @throws \App\Utils\File\Upload\Exception\FileUploadException
     */
    private function storeImage(UploadedFile $file, Phone $phone)
    {
        $image = $this->uploader->storeImage($file, $phone);
        $image->setOwner($phone);
        $image->setMain(false);
        $this->commandBus->dispatch(new UpdatePhoneCommand($phone, [], ['images' => [$image]]));

        return new PhoneGalleryAddImageResponse($phone, true);
    }
}
