<?php


namespace App\Application\UseCases\Backend\Phones\Phone;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneViewCase as Contract;
use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneViewResponse;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Query\PhoneByIdQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class PhoneViewCase
 * @package App\Application\UseCases\Backend\Phones
 */
final class PhoneViewCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * PhoneViewCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param int $phoneId
     * @return PhoneViewResponse
     */
    public function execute(int $phoneId): PhoneViewResponse
    {
        $phone = $this->getPhone($phoneId);

        return new PhoneViewResponse($phone, $phone->getModel(), $phone->getModel()->getBrand());
    }

    /**
     * @param int $phoneId
     * @return Phone
     */
    private function getPhone(int $phoneId): Phone
    {
        /**
         * @var Phone $phone
         */
        $phone = $this->queryBus->dispatch(new PhoneByIdQuery($phoneId))->first();
        if (! $phone) {
            throw new ModelNotFoundException();
        }

        return $phone;
    }
}
