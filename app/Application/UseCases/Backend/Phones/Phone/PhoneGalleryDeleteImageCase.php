<?php


namespace App\Application\UseCases\Backend\Phones\Phone;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryDeleteImageCase as Contract;
use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryDeleteImageResponse;
use App\Domain\Phone\Phone\Command\DeletePhoneImageCommand;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Query\PhoneByIdQuery;
use App\Domain\Shared\Image\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class PhoneGalleryDeleteImageCase
 * @package App\Application\UseCases\Backend\Phones\Phone
 */
final class PhoneGalleryDeleteImageCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * PhoneGalleryDeleteImageCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    /**
     * @param int $phoneId
     * @param int $imageId
     * @return PhoneGalleryDeleteImageResponse
     */
    public function execute(int $phoneId, int $imageId): PhoneGalleryDeleteImageResponse
    {
        $phone = $this->getPhone($phoneId);
        $image = $this->getImage($phone, $imageId);

        return $this->proceed($phone, $image);
    }

    /**
     * @param int $phoneId
     * @return Phone
     */
    private function getPhone(int $phoneId): Phone
    {
        /**
         * @var Phone $phone
         */
        $phone = $this->queryBus->dispatch(new PhoneByIdQuery($phoneId))->first();
        if (! $phone) {
            throw new ModelNotFoundException();
        }

        return $phone;
    }

    /**
     * @param Phone $phone
     * @param int $imageId
     * @return Image
     */
    private function getImage(Phone $phone, int $imageId): Image
    {
        $images = $phone->getImages();

        /**
         * @var Image $image
         */
        $image = $images->filter(function (Image $image) use ($imageId) {
            return $image->getKey() === $imageId;
        })->first();
        if (! $image) {
            throw new ModelNotFoundException();
        }

        return $image;
    }

    /**
     * @param Phone $phone
     * @param Image $image
     * @return PhoneGalleryDeleteImageResponse
     */
    private function proceed(Phone $phone, Image $image)
    {
        try {
            return $this->deleteImage($phone, $image);
        } catch (\Throwable $exception) {
            return new PhoneGalleryDeleteImageResponse($phone, false);
        }
    }

    /**
     * @param Phone $phone
     * @param Image $image
     * @return PhoneGalleryDeleteImageResponse
     */
    private function deleteImage(Phone $phone, Image $image): PhoneGalleryDeleteImageResponse
    {
        $this->commandBus->dispatch(new DeletePhoneImageCommand($phone, $image));

        return new PhoneGalleryDeleteImageResponse($phone, true);
    }
}