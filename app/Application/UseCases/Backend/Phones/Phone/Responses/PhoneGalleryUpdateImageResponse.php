<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Responses;


use App\Domain\Phone\Phone\Phone;
use App\Domain\Shared\Image\Image;

/**
 * Class PhoneGalleryUpdateImageResponse
 * @package App\Application\UseCases\Backend\Phones\Phone\Responses
 */
class PhoneGalleryUpdateImageResponse
{
    /**
     * @var Phone
     */
    private $phone;

    /**
     * @var Image
     */
    private $image;

    /**
     * @var bool
     */
    private $success;

    /**
     * PhoneGalleryUpdateImageResponse constructor.
     * @param Phone $phone
     * @param Image $image
     * @param bool $success
     */
    public function __construct(Phone $phone, Image $image, bool $success)
    {
        $this->phone = $phone;
        $this->image = $image;
        $this->success = $success;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}
