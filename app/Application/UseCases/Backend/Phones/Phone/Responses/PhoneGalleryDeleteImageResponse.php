<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Responses;


use App\Domain\Phone\Phone\Phone;

/**
 * Class PhoneGalleryDeleteImageResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class PhoneGalleryDeleteImageResponse
{
    /**
     * @var Phone
     */
    private $phone;

    /**
     * @var bool
     */
    private $success;

    /**
     * PhoneGalleryDeleteImageResponse constructor.
     * @param Phone $phone
     * @param bool $success
     */
    public function __construct(Phone $phone, bool $success = true)
    {
        $this->phone = $phone;
        $this->success = $success;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}
