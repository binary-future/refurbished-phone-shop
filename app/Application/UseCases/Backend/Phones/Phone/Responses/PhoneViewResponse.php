<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Responses;


use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Phone\Phone;

/**
 * Class PhoneViewResponse
 * @package App\Application\UseCases\Backend\Phones\Responses
 */
final class PhoneViewResponse
{
    /**
     * @var Phone
     */
    private $phone;

    /**
     * @var PhoneModel
     */
    private $model;

    /**
     * @var Brand
     */
    private $brand;

    /**
     * PhoneViewResponse constructor.
     * @param Phone $phone
     * @param PhoneModel $model
     * @param Brand $brand
     */
    public function __construct(Phone $phone, PhoneModel $model, Brand $brand)
    {
        $this->phone = $phone;
        $this->model = $model;
        $this->brand = $brand;
    }

    /**
     * @return Phone
     */
    public function getPhone(): Phone
    {
        return $this->phone;
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->model;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }
}
