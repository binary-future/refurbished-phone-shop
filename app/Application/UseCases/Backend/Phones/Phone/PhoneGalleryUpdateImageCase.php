<?php


namespace App\Application\UseCases\Backend\Phones\Phone;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Phone\Contracts\PhoneGalleryUpdateImageCase as Contract;
use App\Application\UseCases\Backend\Phones\Phone\Requests\PhoneGalleryUpdateImageRequest;
use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryUpdateImageResponse;
use App\Domain\Phone\Phone\Command\UpdatePhoneCommand;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Query\PhoneByIdQuery;
use App\Domain\Shared\Image\Image;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class PhoneGalleryUpdateImageCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * PhoneGalleryDeleteImageCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(PhoneGalleryUpdateImageRequest $request): PhoneGalleryUpdateImageResponse
    {
        $phone = $this->getPhone($request->getPhoneId());
        $image = $this->getImage($phone, $request->getImageId());

        return $this->proceed($phone, $image, $request->getParams());
    }

    /**
     * @param int $phoneId
     * @return Phone
     */
    private function getPhone(int $phoneId): Phone
    {
        /**
         * @var Phone $phone
         */
        $phone = $this->queryBus->dispatch(new PhoneByIdQuery($phoneId))->first();
        if (! $phone) {
            throw new ModelNotFoundException();
        }

        return $phone;
    }

    /**
     * @param Phone $phone
     * @param int $imageId
     * @return Image
     */
    private function getImage(Phone $phone, int $imageId): Image
    {
        $images = $phone->getImages();

        /**
         * @var Image $image
         */
        $image = $images->filter(function (Image $image) use ($imageId) {
            return $image->getKey() === $imageId;
        })->first();
        if (! $image) {
            throw new ModelNotFoundException();
        }

        return $image;
    }

    /**
     * @param Phone $phone
     * @param Image $image
     * @param array $params
     * @return PhoneGalleryUpdateImageResponse
     */
    private function proceed(Phone $phone, Image $image, array $params): PhoneGalleryUpdateImageResponse
    {
        try {
            return $this->deleteImage($phone, $image, $params);
        } catch (\Throwable $exception) {
            return new PhoneGalleryUpdateImageResponse($phone, $image, false);
        }
    }

    /**
     * @param Phone $phone
     * @param Image $image
     * @return PhoneGalleryUpdateImageResponse
     */
    private function deleteImage(Phone $phone, Image $image, array $params): PhoneGalleryUpdateImageResponse
    {
        $image->fill($params);
        $command = new UpdatePhoneCommand($phone);
        $command->setUpdatedImages(collect($image));
        $this->commandBus->dispatch($command);

        return new PhoneGalleryUpdateImageResponse($phone, $image, true);
    }

}