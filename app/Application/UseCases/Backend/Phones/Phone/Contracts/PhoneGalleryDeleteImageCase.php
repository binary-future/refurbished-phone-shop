<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Contracts;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryDeleteImageResponse;

/**
 * Interface PhoneGalleryDeleteImageCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface PhoneGalleryDeleteImageCase
{
    /**
     * @param int $phoneId
     * @param int $imageId
     * @return PhoneGalleryDeleteImageResponse
     */
    public function execute(int $phoneId, int $imageId): PhoneGalleryDeleteImageResponse;
}