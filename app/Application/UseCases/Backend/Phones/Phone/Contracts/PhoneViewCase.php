<?php

namespace App\Application\UseCases\Backend\Phones\Phone\Contracts;

use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneViewResponse;

/**
 * Interface PhoneViewCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface PhoneViewCase
{
    /**
     * @param int $phoneId
     * @return PhoneViewResponse
     */
    public function execute(int $phoneId): PhoneViewResponse;
}
