<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Contracts;


use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryAddImageResponse;
use Illuminate\Http\UploadedFile;

/**
 * Interface PhoneGalleryAddImageCase
 * @package App\Application\UseCases\Backend\Phones\Contracts
 */
interface PhoneGalleryAddImageCase
{
    /**
     * @param UploadedFile $file
     * @param int $phone
     * @return mixed
     */
    public function execute(UploadedFile $file, int $phone): PhoneGalleryAddImageResponse;
}
