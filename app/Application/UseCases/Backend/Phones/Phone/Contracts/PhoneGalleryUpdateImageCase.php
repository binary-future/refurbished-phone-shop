<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Contracts;


use App\Application\UseCases\Backend\Phones\Phone\Requests\PhoneGalleryUpdateImageRequest;
use App\Application\UseCases\Backend\Phones\Phone\Responses\PhoneGalleryUpdateImageResponse;

/**
 * Interface PhoneGalleryUpdateImageCase
 * @package App\Application\UseCases\Backend\Phones\Phone\Contracts
 */
interface PhoneGalleryUpdateImageCase
{
    /**
     * @param PhoneGalleryUpdateImageRequest $request
     * @return PhoneGalleryUpdateImageResponse
     */
    public function execute(PhoneGalleryUpdateImageRequest $request): PhoneGalleryUpdateImageResponse;
}
