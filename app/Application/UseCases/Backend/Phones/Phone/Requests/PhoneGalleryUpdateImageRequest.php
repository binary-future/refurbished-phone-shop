<?php


namespace App\Application\UseCases\Backend\Phones\Phone\Requests;


use App\Domain\Shared\Image\Image;

/**
 * Class PhoneGalleryUpdateImageRequest
 * @package App\Application\UseCases\Backend\Phones\Phone\Requests
 */
final class PhoneGalleryUpdateImageRequest
{
    /**
     * @var
     */
    private $phoneId;

    /**
     * @var
     */
    private $imageId;

    /**
     * @var
     */
    private $isMain;

    /**
     * @return mixed
     */
    public function getPhoneId()
    {
        return $this->phoneId;
    }

    /**
     * @param mixed $phoneId
     */
    public function setPhoneId($phoneId): void
    {
        $this->phoneId = $phoneId;
    }

    /**
     * @return mixed
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param mixed $imageId
     */
    public function setImageId($imageId): void
    {
        $this->imageId = $imageId;
    }

    /**
     * @return mixed
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * @param mixed $isMain
     */
    public function setIsMain($isMain): void
    {
        $this->isMain = $isMain;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [];
        if (!is_null($this->isMain)) {
            $params[Image::FIELD_IS_MAIN] = (bool) $this->isMain;
        }

        return $params;
    }
}
