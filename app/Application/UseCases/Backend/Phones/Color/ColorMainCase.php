<?php


namespace App\Application\UseCases\Backend\Phones\Color;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorMainCase as Contract;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorMainResponse;
use App\Domain\Phone\Phone\Query\AllColorsQuery;
use App\Domain\Phone\Phone\Query\GetColorsOfInactiveModelsQuery;
use Illuminate\Support\Collection;

/**
 * Class ColorMainCase
 * @package App\Application\UseCases\Backend\Phones\Color
 */
final class ColorMainCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * ColorMainCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return ColorMainResponse
     */
    public function execute(): ColorMainResponse
    {
        /**
         * @var Collection $colors
         * @var Collection $inactiveColors
         */
        $colors = $this->queryBus->dispatch(new AllColorsQuery());
        $inactiveColors = $this->queryBus->dispatch(new GetColorsOfInactiveModelsQuery());

        return new ColorMainResponse($colors, $inactiveColors);
    }
}
