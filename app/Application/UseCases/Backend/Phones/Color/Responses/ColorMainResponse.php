<?php


namespace App\Application\UseCases\Backend\Phones\Color\Responses;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class ColorMainResponse
 * @package App\Application\UseCases\Backend\Phones\Color\Responses
 */
final class ColorMainResponse
{
    /**
     * @var Collection
     */
    private $colors;

    /**
     * @var Collection
     */
    private $inactiveColors;

    /**
     * ColorMainResponse constructor.
     * @param Collection|LengthAwarePaginator $colors
     * @param Collection $inactiveColors
     */
    public function __construct(Collection $colors, Collection $inactiveColors)
    {
        $this->colors = $colors;
        $this->inactiveColors = $inactiveColors;
    }

    /**
     * @return Collection|LengthAwarePaginator
     */
    public function getColors()
    {
        return $this->colors;
    }

    /**
     * @return Collection
     */
    public function getInactiveColors(): Collection
    {
        return $this->inactiveColors;
    }
}
