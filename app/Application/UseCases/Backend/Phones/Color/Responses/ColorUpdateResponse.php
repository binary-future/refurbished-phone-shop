<?php


namespace App\Application\UseCases\Backend\Phones\Color\Responses;


use App\Domain\Phone\Phone\Color;

final class ColorUpdateResponse
{
    /**
     * @var Color
     */
    private $color;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * ColorUpdateResponse constructor.
     * @param Color $color
     */
    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
