<?php

namespace App\Application\UseCases\Backend\Phones\Color\Responses;

use App\Domain\Phone\Phone\Color;
use Illuminate\Support\Collection;

final class ColorSynonymsResponse
{
    /**
     * @var Color
     */
    private $color;

    /**
     * @var Collection
     */
    private $synonyms;

    /**
     * @var Collection
     */
    private $potentialSynonyms;

    /**
     * ColorSynonymsResponse constructor.
     * @param Color $color
     * @param Collection $synonyms
     * @param Collection $potentialSynonyms
     */
    public function __construct(Color $color, Collection $synonyms, Collection $potentialSynonyms)
    {
        $this->color = $color;
        $this->synonyms = $synonyms;
        $this->potentialSynonyms = $potentialSynonyms;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return Collection
     */
    public function getSynonyms(): Collection
    {
        return $this->synonyms;
    }

    /**
     * @return Collection
     */
    public function getPotentialSynonyms(): Collection
    {
        return $this->potentialSynonyms;
    }
}
