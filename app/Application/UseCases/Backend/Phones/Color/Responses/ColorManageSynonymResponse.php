<?php


namespace App\Application\UseCases\Backend\Phones\Color\Responses;


use App\Domain\Phone\Phone\Color;

final class ColorManageSynonymResponse
{
    /**
     * @var Color
     */
    private $color;

    /**
     * @var bool
     */
    private $succeed;

    /**
     * ColorManageSynonymResponse constructor.
     * @param Color $color
     * @param bool $succeed
     */
    public function __construct(Color $color, bool $succeed)
    {
        $this->color = $color;
        $this->succeed = $succeed;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }
}
