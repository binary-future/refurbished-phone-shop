<?php

namespace App\Application\UseCases\Backend\Phones\Color\Responses;

use App\Domain\Phone\Phone\Color;

/**
 * Class ColorEditResponse
 * @package App\Application\UseCases\Backend\Phones\Color\Responses
 */
final class ColorEditResponse
{
    /**
     * @var Color
     */
    private $color;

    /**
     * ColorEditResponse constructor.
     * @param Color $color
     */
    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    /**
     * @return Color
     */
    public function getColor(): Color
    {
        return $this->color;
    }
}
