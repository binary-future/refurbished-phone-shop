<?php


namespace App\Application\UseCases\Backend\Phones\Color\Contracts;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorMainResponse;

interface ColorMainCase
{
    public function execute(): ColorMainResponse;
}