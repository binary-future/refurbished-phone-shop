<?php


namespace App\Application\UseCases\Backend\Phones\Color\Contracts;


use App\Application\UseCases\Backend\Phones\Color\Responses\ColorManageSynonymResponse;

/**
 * Interface ColorAddSynonymCase
 * @package App\Application\UseCases\Backend\Phones\Color\Contracts
 */
interface ColorAddSynonymCase
{
    /**
     * @param string $color
     * @param int $synonymId
     * @return ColorManageSynonymResponse
     */
    public function execute(string $color, int $synonymId): ColorManageSynonymResponse;
}
