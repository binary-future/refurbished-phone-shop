<?php


namespace App\Application\UseCases\Backend\Phones\Color\Contracts;


use App\Application\UseCases\Backend\Phones\Color\Requests\ColorUpdateRequest;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorUpdateResponse;

/**
 * Interface ColorUpdateCase
 * @package App\Application\UseCases\Backend\Phones\Color\Contracts
 */
interface ColorUpdateCase
{
    /**
     * @param string $colorSlug
     * @param ColorUpdateRequest $request
     * @return ColorUpdateResponse
     */
    public function execute(string  $colorSlug, ColorUpdateRequest $request): ColorUpdateResponse;
}
