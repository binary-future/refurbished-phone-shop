<?php

namespace App\Application\UseCases\Backend\Phones\Color\Contracts;

use App\Application\UseCases\Backend\Phones\Color\Responses\ColorSynonymsResponse;

/**
 * Interface ColorSynonymsCase
 * @package App\Application\UseCases\Backend\Phones\Color\Contracts
 */
interface ColorSynonymsCase
{
    /**
     * @param string $color
     * @return mixed
     */
    public function execute(string $color): ColorSynonymsResponse;
}
