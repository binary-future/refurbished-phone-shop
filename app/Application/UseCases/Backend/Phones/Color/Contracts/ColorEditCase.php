<?php

namespace App\Application\UseCases\Backend\Phones\Color\Contracts;

use App\Application\UseCases\Backend\Phones\Color\Responses\ColorEditResponse;

/**
 * Interface ColorEditCase
 * @package App\Application\UseCases\Backend\Phones\Color\Contracts
 */
interface ColorEditCase
{
    /**
     * @param string $color
     * @return mixed
     */
    public function execute(string $color): ColorEditResponse;
}