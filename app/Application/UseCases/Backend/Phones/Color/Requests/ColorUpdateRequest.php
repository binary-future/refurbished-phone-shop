<?php


namespace App\Application\UseCases\Backend\Phones\Color\Requests;


use App\Domain\Phone\Phone\Color;

final class ColorUpdateRequest
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string|null
     */
    private $hex;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getHex(): ?string
    {
        return $this->hex;
    }

    /**
     * @param string|null $hex
     */
    public function setHex(?string $hex): void
    {
        $this->hex = $hex;
    }

    public function getParams(): array
    {
        $params = [Color::FIELD_NAME => $this->getName()];
        if ($this->getHex()) {
            $params[Color::FIELD_HEX] = $this->getHex();
        }

        return $params;
    }
}
