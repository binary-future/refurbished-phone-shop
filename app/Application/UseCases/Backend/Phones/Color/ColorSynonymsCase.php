<?php

namespace App\Application\UseCases\Backend\Phones\Color;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorSynonymsCase as Contract;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorSynonymsResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Query\AllColorsQuery;
use App\Domain\Phone\Phone\Query\ColorPotentialSynonymsQuery;
use App\Domain\Phone\Phone\Query\FindColorBySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class ColorSynonymsCase implements Contract
{
    private const PERCENT = 80;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * ColorSynonymsCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(string $color): ColorSynonymsResponse
    {
        $color = $this->getColor($color);

        return new ColorSynonymsResponse(
            $color,
            $color->getSynonyms(),
            $this->getPotentialSynonyms($color)
        );
    }

    private function getColor(string $color): Color
    {
        /**
         * @var Color $color
         */
        $color = $this->queryBus->dispatch(new FindColorBySlugQuery($color, ['synonyms']))->first();
        if (!$color || $color->isSynonym()) {
            throw new ModelNotFoundException();
        }

        return $color;
    }

    private function getPotentialSynonyms(Color $color): Collection
    {
        /**
         * @var Collection $colors
         */
        $colors = $this->queryBus
            ->dispatch(new ColorPotentialSynonymsQuery($color->getKey(), $this->generateColorName($color)));

        return $colors->filter(function (Color $synonym) use ($color) {
            return $color->getKey() !== $synonym->getParentId();
        });
    }

    private function generateColorName(Color $color): string
    {
        return trim(substr($color->getName(), 0, round(strlen($color->getName()) * static::PERCENT / 100)));
    }
}
