<?php


namespace App\Application\UseCases\Backend\Phones\Color;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorRemoveSynonymCase as Contract;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorManageSynonymResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Command\UpdateColorCommand;
use App\Domain\Phone\Phone\Query\ColorByIdQuery;
use App\Domain\Phone\Phone\Query\FindColorBySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class ColorRemoveSynonymCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ColorRemoveSynonymCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(string $color, int $synonymId): ColorManageSynonymResponse
    {
        $color = $this->getColor($color);
        $synonym = $this->getSynonym($synonymId);
        if (! $this->isValid($color, $synonym)) {
            return new ColorManageSynonymResponse($color, false);
        }

        return $this->removeSynonym($color, $synonym);
    }

    private function getColor(string $colorSlug): Color
    {
        /**
         * @var Color $color
         */
        $color = $this->queryBus->dispatch(new FindColorBySlugQuery($colorSlug))->first();
        if (! $color) {
            throw new ModelNotFoundException();
        }

        return $color;
    }

    private function getSynonym(int $synonymId): Color
    {
        /**
         * @var Color $synonym
         */
        $synonym = $this->queryBus
            ->dispatch(new ColorByIdQuery($synonymId, ['phones']))
            ->first();
        if (! $synonym) {
            throw new ModelNotFoundException();
        }

        return $synonym;
    }

    private function isValid(Color $color, Color $synonym): bool
    {
        return $color->getKey() === $synonym->getParentId();
    }

    private function removeSynonym(Color $color, Color $synonym): ColorManageSynonymResponse
    {
        try {
            $synonym->refreshBaseColor();
            $this->saveColor($synonym);
            return new ColorManageSynonymResponse($color, true);
        } catch (\Throwable $exception) {
            return new ColorManageSynonymResponse($color, false);
        }
    }

    private function saveColor(Color $color)
    {
        $this->commandBus->dispatch(new UpdateColorCommand($color, []));
    }
}
