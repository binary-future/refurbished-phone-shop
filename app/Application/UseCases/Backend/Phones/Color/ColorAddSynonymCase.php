<?php


namespace App\Application\UseCases\Backend\Phones\Color;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorAddSynonymCase as Contract;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorManageSynonymResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Command\UpdateColorCommand;
use App\Domain\Phone\Phone\Query\ColorByIdQuery;
use App\Domain\Phone\Phone\Query\FindColorBySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class ColorAddSynonymCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * ColorAddSynonymCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(string $colorName, int $synonymId): ColorManageSynonymResponse
    {
        $color = $this->getColor($colorName);
        $synonym = $this->getSynonym($synonymId);
        if (! $this->isValid($color, $synonym)) {
            return new ColorManageSynonymResponse($color, false);
        }

        return $this->addSynonym($color, $synonym);
    }

    /**
     * @param string $colorSlug
     * @return Color
     */
    private function getColor(string $colorSlug): Color
    {
        /**
         * @var Color $color
         */
        $color = $this->queryBus->dispatch(new FindColorBySlugQuery($colorSlug))->first();
        if (! $color) {
            throw new ModelNotFoundException();
        }

        return $color;
    }

    private function getSynonym(int $synonymId): Color
    {
        /**
         * @var Color $synonym
         */
        $synonym = $this->queryBus
            ->dispatch(new ColorByIdQuery($synonymId, ['phones', 'synonyms', 'synonyms.phones']))
            ->first();
        if (! $synonym) {
            throw new ModelNotFoundException();
        }

        return $synonym;
    }

    private function isValid(Color $color, Color $synonym): bool
    {
        return $color->getKey() !== $synonym->getKey() && ! $color->isSynonym();
    }

    private function addSynonym(Color $color, Color $synonym): ColorManageSynonymResponse
    {
        try {
            return $this->proceedMerging($color, $synonym);
        } catch (\Throwable $exception) {
            return new ColorManageSynonymResponse($color, false);
        }
    }

    private function proceedMerging(Color $color, Color $synonymColor): ColorManageSynonymResponse
    {
        $synonyms = $synonymColor->getSynonyms()->merge(collect([$synonymColor]));
        foreach ($synonyms as $synonym) {
            /** @var Color $synonym */
            $synonym->assignBaseColor($color);
            $this->saveColor($synonym);
        }

        return new ColorManageSynonymResponse($color, true);
    }

    private function saveColor(Color $color)
    {
        $this->commandBus->dispatch(new UpdateColorCommand($color, []));
    }
}
