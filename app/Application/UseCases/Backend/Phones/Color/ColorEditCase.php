<?php


namespace App\Application\UseCases\Backend\Phones\Color;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorEditCase as Contract;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorEditResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Query\FindColorBySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class ColorEditCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * ColorEditCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    public function execute(string $color): ColorEditResponse
    {
        return new ColorEditResponse($this->getColor($color));
    }

    private function getColor(string $colorSlug): Color
    {
        /**
         * @var Color $color
         */
        $color = $this->queryBus->dispatch(new FindColorBySlugQuery($colorSlug))->first();
        if (!$color) {
            throw new ModelNotFoundException();
        }

        return $color;
    }
}
