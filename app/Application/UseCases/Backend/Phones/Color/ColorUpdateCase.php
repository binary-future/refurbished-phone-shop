<?php


namespace App\Application\UseCases\Backend\Phones\Color;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Phones\Color\Contracts\ColorUpdateCase as Contract;
use App\Application\UseCases\Backend\Phones\Color\Requests\ColorUpdateRequest;
use App\Application\UseCases\Backend\Phones\Color\Responses\ColorUpdateResponse;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Command\UpdateColorCommand;
use App\Domain\Phone\Phone\Query\FindColorBySlugQuery;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ColorUpdateCase
 * @package App\Application\UseCases\Backend\Phones\Color
 */
final class ColorUpdateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * ColorUpdateCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     * @param Generator $slugGeneratorl
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus, Generator $slugGenerator)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->slugGenerator = $slugGenerator;
    }

    /**
     * @param string $colorSlug
     * @param ColorUpdateRequest $request
     * @return ColorUpdateResponse
     */
    public function execute(string $colorSlug, ColorUpdateRequest $request): ColorUpdateResponse
    {
        $color = $this->getColor($colorSlug);
        try {
            return $this->updateColor($color, $request);
        } catch (\Throwable $exception) {
            $response = new ColorUpdateResponse($color);
            $response->setSuccess(false);

            return $response;
        }
    }

    /**
     * @param string $colorSlug
     * @return Color
     */
    private function getColor(string $colorSlug): Color
    {
        /**
         * @var Color $color
         */
        $color = $this->queryBus->dispatch(new FindColorBySlugQuery($colorSlug))->first();
        if (!$color) {
            throw new ModelNotFoundException();
        }

        return $color;
    }

    /**
     * @param Color $color
     * @param ColorUpdateRequest $request
     * @return ColorUpdateResponse
     */
    private function updateColor(Color $color, ColorUpdateRequest $request): ColorUpdateResponse
    {
        $params = $this->preperaParams($request);
        /**
         * @var Color $updatedColor
         */
        $updatedColor = $this->commandBus->dispatch(new UpdateColorCommand($color, $params));

        return new ColorUpdateResponse($updatedColor);
    }

    /**
     * @param ColorUpdateRequest $request
     * @return array
     */
    private function preperaParams(ColorUpdateRequest $request): array
    {
        $params = $request->getParams();
        $params['slug'] = $this->slugGenerator->generate($request->getName());

        return $params;
    }
}
