<?php

namespace App\Application\UseCases\Backend\Networks;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Networks\Contracts\NetworkEditCase as Contract;
use App\Application\UseCases\Backend\Networks\Response\NetworksEditResponse;
use App\Domain\Deals\Contract\Query\NetworkBySlugQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class NetworkEditCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreEditCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param string $networkSlug
     * @return NetworksEditResponse
     */
    public function execute(string $networkSlug): NetworksEditResponse
    {
        $network = $this->queryBus->dispatch(new NetworkBySlugQuery($networkSlug))->first();
        if (! $network) {
            throw new ModelNotFoundException();
        }

        return new NetworksEditResponse($network);
    }
}
