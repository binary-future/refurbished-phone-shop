<?php

namespace App\Application\UseCases\Backend\Networks;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Uploader;
use App\Application\UseCases\Backend\Networks\Contracts\NetworksUpdateCase as Contract;
use App\Application\UseCases\Backend\Networks\Response\NetworkSaveResponse;
use App\Domain\Deals\Contract\Command\UpdateNetworkCommand;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Contract\Query\NetworkBySlugQuery;
use App\Utils\Adapters\Slug\Contracts\Generator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;

final class NetworksUpdateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Uploader
     */
    private $uploader;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Generator
     */
    private $slugGenerator;

    /**
     * StoreUpdateCase constructor.
     * @param QueryBus $queryBus
     * @param Uploader $uploader
     * @param CommandBus $commandBus
     * @param Generator $generator
     */
    public function __construct(
        QueryBus $queryBus,
        Uploader $uploader,
        CommandBus $commandBus,
        Generator $generator
    ) {
        $this->queryBus = $queryBus;
        $this->uploader = $uploader;
        $this->commandBus = $commandBus;
        $this->slugGenerator = $generator;
    }

    /**
     * @param string $networkSlug
     * @param array $params
     * @return NetworkSaveResponse
     */
    public function execute(string $networkSlug, array $params): NetworkSaveResponse
    {
        $network = $this->getNetwork($networkSlug);
        try {
            return $this->proceedUpdating($network, $params);
        } catch (\Throwable $exception) {
            $response = new NetworkSaveResponse($network);
            $response->setSuccess(false);

            return $response;
        }
    }

    private function proceedUpdating(Network $network, $params)
    {
        $params = $this->prepareParams($params);
        if (isset($params['logo']) && $params['logo'] instanceof UploadedFile) {
            $image = $this->uploader->storeImage($params['logo'], $network);
            $image->setOwner($network);
        }
        $this->commandBus->dispatch(new UpdateNetworkCommand($network, $params, $image ?? null));

        return new NetworkSaveResponse($network);
    }

    private function prepareParams(array $params)
    {
        $params['slug'] = $this->slugGenerator->generate($params['name']);

        return $params;
    }

    /**
     * @param string $slug
     * @return Network
     */
    private function getNetwork(string $slug): Network
    {
        $network = $this->queryBus->dispatch(new NetworkBySlugQuery($slug))->first();
        if (! $network) {
            throw new ModelNotFoundException();
        }

        return $network;
    }
}
