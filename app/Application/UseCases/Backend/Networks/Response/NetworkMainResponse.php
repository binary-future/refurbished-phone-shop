<?php

namespace App\Application\UseCases\Backend\Networks\Response;

use Illuminate\Support\Collection;

final class NetworkMainResponse
{
    /**
     * @var Collection
     */
    private $networks;

    /**
     * NetworkMainResponse constructor.
     * @param Collection $networks
     */
    public function __construct(Collection $networks)
    {
        $this->networks = $networks;
    }

    /**
     * @return Collection
     */
    public function getNetworks(): Collection
    {
        return $this->networks;
    }
}
