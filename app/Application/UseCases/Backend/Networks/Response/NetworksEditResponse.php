<?php

namespace App\Application\UseCases\Backend\Networks\Response;

use App\Domain\Deals\Contract\Network;

final class NetworksEditResponse
{
    /**
     * @var Network
     */
    private $network;

    /**
     * NetworksEditResponse constructor.
     * @param Network $network
     */
    public function __construct(Network $network)
    {
        $this->network = $network;
    }

    /**
     * @return Network
     */
    public function getNetwork(): Network
    {
        return $this->network;
    }
}
