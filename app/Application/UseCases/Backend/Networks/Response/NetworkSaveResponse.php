<?php

namespace App\Application\UseCases\Backend\Networks\Response;

use App\Domain\Deals\Contract\Network;

/**
 * Class StoreSaveResponse
 * @package App\Application\UseCases\Backend\Stores\Response
 */
final class NetworkSaveResponse
{
    /**
     * @var Network
     */
    private $network;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * StoreSaveResponse constructor.
     * @param Network $network
     */
    public function __construct(Network $network)
    {
        $this->network = $network;
    }

    /**
     * @return Network
     */
    public function getNetwork(): Network
    {
        return $this->network;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}
