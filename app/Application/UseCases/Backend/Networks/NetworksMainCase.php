<?php

namespace App\Application\UseCases\Backend\Networks;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Networks\Contracts\NetworksMainCase as Contract;
use App\Application\UseCases\Backend\Networks\Response\NetworkMainResponse;
use App\Domain\Deals\Contract\Query\AllNetworksQuery;
use App\Domain\Store\Query\AllQuery;
use Illuminate\Support\Collection;

final class NetworksMainCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * StoreMainCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return NetworkMainResponse
     */
    public function execute(): NetworkMainResponse
    {
        /**
         * @var Collection $networks
         */
        $networks = $this->queryBus->dispatch(new AllNetworksQuery(
            ['image']
        ));

        return new NetworkMainResponse($networks);
    }
}
