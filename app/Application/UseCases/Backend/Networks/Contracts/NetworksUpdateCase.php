<?php

namespace App\Application\UseCases\Backend\Networks\Contracts;

use App\Application\UseCases\Backend\Networks\Response\NetworkSaveResponse;

/**
 * Interface NetworksUpdateCase
 * @package App\Application\UseCases\Backend\Networks\Contracts
 */
interface NetworksUpdateCase
{
    /**
     * @param string $networkSlug
     * @param array $params
     * @return NetworkSaveResponse
     */
    public function execute(string $networkSlug, array $params): NetworkSaveResponse;
}
