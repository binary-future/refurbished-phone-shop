<?php

namespace App\Application\UseCases\Backend\Networks\Contracts;

use App\Application\UseCases\Backend\Networks\Response\NetworksEditResponse;

/**
 * Interface NetworkEditCase
 * @package App\Application\UseCases\Backend\Networks\Contracts
 */
interface NetworkEditCase
{
    /**
     * @param string $networkSlug
     * @return NetworksEditResponse
     */
    public function execute(string $networkSlug): NetworksEditResponse;
}
