<?php

namespace App\Application\UseCases\Backend\Networks\Contracts;

use App\Application\UseCases\Backend\Networks\Response\NetworkMainResponse;

/**
 * Interface NetworksMainCase
 * @package App\Application\UseCases\Backend\Stores\Contracts
 */
interface NetworksMainCase
{
    /**
     * @return NetworkMainResponse
     */
    public function execute(): NetworkMainResponse;
}
