<?php


namespace App\Application\UseCases\Backend\Main\Contracts;

use App\Application\UseCases\Backend\Main\Responses\MainPageResponse;

/**
 * Interface MainPageCase
 * @package App\Application\UseCases\Backend\Main\Contracts
 */
interface MainPageCase
{
    /**
     * @return MainPageResponse
     */
    public function execute(): MainPageResponse;
}