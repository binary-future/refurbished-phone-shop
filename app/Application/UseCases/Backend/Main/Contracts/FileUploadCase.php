<?php


namespace App\Application\UseCases\Backend\Main\Contracts;


use App\Application\UseCases\Backend\Main\Request\FileUploadRequest;
use App\Application\UseCases\Backend\Main\Responses\FileUploadResponse;

interface FileUploadCase
{
    public function execute(FileUploadRequest $request): FileUploadResponse;
}