<?php


namespace App\Application\UseCases\Backend\Main\Contracts;


use App\Application\UseCases\Backend\Main\Responses\ClearedDescriptionImages;

/**
 * Interface ClearDescriptionImagesCase
 * @package App\Application\UseCases\Backend\Main\Contracts
 */
interface ClearDescriptionImagesCase
{
    /**
     * @return ClearedDescriptionImages
     */
    public function execute(): ClearedDescriptionImages;
}