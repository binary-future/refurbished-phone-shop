<?php


namespace App\Application\UseCases\Backend\Main\Contracts;

use App\Application\UseCases\Backend\Main\Responses\FileUploadEditorResult;
use Illuminate\Http\UploadedFile;

/**
 * Interface FileUploadEditorCase
 * @package App\Application\UseCases\Backend\Main\Contracts
 */
interface FileUploadEditorCase
{
    /**
     * @param UploadedFile $uploadedFile
     * @return FileUploadEditorResult
     */
    public function execute(UploadedFile $uploadedFile): FileUploadEditorResult;
}