<?php


namespace App\Application\UseCases\Backend\Main\Request;


use Illuminate\Http\UploadedFile;

/**
 * Class FileUploadRequest
 * @package App\Application\UseCases\Backend\Main\Request
 */
final class FileUploadRequest
{
    /**
     * @var string
     */
    private $target;

    /**
     * @var string
     */
    private $type;

    /**
     * @var UploadedFile|null
     */
    private $file;

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget(string $target): void
    {
        $this->target = $target;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @param UploadedFile|null $file
     */
    public function setFile(?UploadedFile $file): void
    {
        $this->file = $file;
    }
}
