<?php

namespace App\Application\UseCases\Backend\Main;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Importer\Plan\Query\LastPlannedImportsQuery;
use App\Application\Services\Importer\Plan\Repository\PlannedImports;
use App\Application\Services\Importer\Reports\Contracts\ReportsTypes;
use App\Application\Services\Importer\Reports\NegativeReport;
use App\Application\Services\Importer\Reports\PositiveReport;
use App\Application\Services\Importer\Reports\Query\NegativeReportBySourceTypeTargetTypeAndSinceDateQuery;
use App\Application\Services\Importer\Reports\Query\PositiveReportBySourceTypeTargetTypeAndSinceDateQuery;
use App\Application\Services\Importer\Reports\Repository\NegativeReports;
use App\Application\Services\Importer\Reports\Repository\PositiveReports;
use App\Application\UseCases\Backend\Main\Contracts\MainPageCase as Contract;
use App\Application\UseCases\Backend\Main\Responses\MainPageResponse;
use App\Domain\Phone\Model\Query\AllActiveWithoutDealsQuery;
use App\Domain\Phone\Model\Query\AllInActiveQuery;
use App\Domain\Store\Query\AllQuery as AllStoresQuery;
use App\Domain\Store\Store;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class MainPageCase
 * @package App\Application\UseCases\Backend\Main
 */
final class MainPageCase implements Contract, ReportsTypes
{
    public const MODELS_LIMIT = 5;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var PositiveReports
     */
    private $positiveReports;

    /**
     * @var NegativeReports
     */
    private $negativeReports;

    /**
     * @var PlannedImports
     */
    private $plannedImports;

    /**
     * MainPageCase constructor.
     * @param QueryBus $queryBus
     * @param PositiveReports $positiveReports
     * @param NegativeReports $negativeReports
     * @param PlannedImports $plannedImports
     */
    public function __construct(
        QueryBus $queryBus,
        PositiveReports $positiveReports,
        NegativeReports $negativeReports,
        PlannedImports $plannedImports
    ) {
        $this->queryBus = $queryBus;
        $this->positiveReports = $positiveReports;
        $this->negativeReports = $negativeReports;
        $this->plannedImports = $plannedImports;
    }


    /**
     * @return MainPageResponse
     * @throws \Exception
     */
    public function execute(): MainPageResponse
    {
        return new MainPageResponse(
            $this->getStores(),
            $this->getPositiveReports(self::DEALS),
            $this->getNegativeReports(self::DEALS),
            $this->getInactiveModels(),
            $this->getPositiveReports(self::PHONES),
            $this->getNegativeReports(self::PHONES),
            $this->getModelsWithoutDeals(),
            $this->getPlannedImports()
        );
    }

    /**
     * @return Collection
     */
    private function getStores(): Collection
    {
        /**
         * @var Collection $stores
         */
        $stores = $this->queryBus->dispatch(new AllStoresQuery());

        return $stores;
    }

    private function getInactiveModels(): Collection
    {
        $query = new AllInActiveQuery(['brand']);
        $query->setLimit(self::MODELS_LIMIT);
        /**
         * @var Collection $inactiveModels
         */
        $inactiveModels =  $this->queryBus->dispatch($query);

        return $inactiveModels;
    }

    private function getModelsWithoutDeals(): Collection
    {
        $query = new AllActiveWithoutDealsQuery(['brand']);
        $query->setLimit(self::MODELS_LIMIT);
        /**
         * @var Collection $inactiveModels
         */
        $inactiveModels =  $this->queryBus->dispatch($query);

        return $inactiveModels;
    }

    /**
     * @param string $type
     * @return Collection
     * @throws \Exception
     */
    private function getPositiveReports(string $type): Collection
    {
        /**
         * @var Collection $reports
         */
        $reports = $this->positiveReports
            ->findBy(new PositiveReportBySourceTypeTargetTypeAndSinceDateQuery(
                Store::OWNER_TYPE,
                (new Carbon())->subDay(),
                $type
            ));

        return $reports->groupBy(PositiveReport::FIELD_SOURCE_ID);
    }

    /**
     * @param string $type
     * @return Collection
     * @throws \Exception
     */
    private function getNegativeReports(string $type): Collection
    {
        /**
         * @var Collection $reports
         */
        $reports = $this->negativeReports
            ->findBy(new NegativeReportBySourceTypeTargetTypeAndSinceDateQuery(
                Store::OWNER_TYPE,
                (new Carbon())->subDay(),
                $type
            ));

        return $reports->groupBy(NegativeReport::FIELD_SOURCE_ID);
    }

    /**
     * @return Collection
     */
    private function getPlannedImports(): Collection
    {
        return $this->plannedImports->findBy(new LastPlannedImportsQuery(self::MODELS_LIMIT));
    }
}
