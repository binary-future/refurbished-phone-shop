<?php


namespace App\Application\UseCases\Backend\Main;

use App\Application\Services\Importer\Handler;
use App\Application\Services\Importer\Loggable;
use App\Application\Services\Importer\Response\Contracts\ImportResponse;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\UseCases\Backend\Main\Contracts\FileUploadCase as Contract;
use App\Application\UseCases\Backend\Main\Request\FileUploadRequest;
use App\Application\UseCases\Backend\Main\Responses\FileUploadResponse;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Importer\Handlers\Factory;

/**
 * Class FileUploadCase
 * @package App\Application\UseCases\Backend\Main
 */
final class FileUploadCase implements Contract
{
    private const CONFIG_PATH = 'file.upload.import.scenarios';

    /**
     * @var Factory
     */
    private $importHandlerFactory;

    /**
     * @var array
     */
    private $schema = [];

    /**
     * FileUploadCase constructor.
     * @param Config $config
     * @param Factory $importHandlerFactory
     */
    public function __construct(Config $config, Factory $importHandlerFactory)
    {
        $this->schema = $config->get(self::CONFIG_PATH, []);
        $this->importHandlerFactory = $importHandlerFactory;
    }


    /**
     * @param FileUploadRequest $request
     * @return FileUploadResponse
     */
    public function execute(FileUploadRequest $request): FileUploadResponse
    {
        if (!$request->getFile()) {
            return $this->getFailedResponse();
        }
        return $this->proceed($request);
    }

    private function getFailedResponse(): FileUploadResponse
    {
        $response = new FileUploadResponse();
        $response->setIsSuccess(false);

        return $response;
    }

    private function proceed(FileUploadRequest $request)
    {
        try {
            return $this->importFile($request);
        } catch (\Throwable $exception) {
            dd($exception->getMessage());
            return $this->getFailedResponse();
        }
    }

    private function importFile(FileUploadRequest $request): FileUploadResponse
    {
        $scenario = $this->getScenario($request);
        $handler = $this->getHandler($scenario);
        if ($handler instanceof Loggable) {
            $handler->setLogging(false);
        }
        $response = $handler->handleImport($scenario);

        return $this->preperaResponse($response);
    }

    /**
     * @param Scenario $scenario
     * @return Handler
     * @throws \App\Utils\Importer\Exceptions\ImportHandlerBuildException
     */
    private function getHandler(Scenario $scenario): Handler
    {
        return $this->importHandlerFactory->buildHandler($scenario);
    }

    private function preperaResponse(ImportResponse $response): FileUploadResponse
    {
        if (! $response->isSuccess()) {
            return $this->getFailedResponse();
        }
        $uploadResponse = new FileUploadResponse();
        $uploadResponse->setSucceed((int) $response->get('success'));
        $uploadResponse->setFailed((int) $response->get('skipped'));

        return $uploadResponse;
    }

    /**
     * @param FileUploadRequest $request
     * @return Scenario
     */
    private function getScenario(FileUploadRequest $request): Scenario
    {
        $scenario = $this->schema[$request->getTarget()][$request->getType()][$request->getFile()->getMimeType()];

        return new $scenario($request->getFile());
    }
}
