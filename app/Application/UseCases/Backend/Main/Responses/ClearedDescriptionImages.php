<?php

namespace App\Application\UseCases\Backend\Main\Responses;

/**
 * Class ClearedDescriptionImages
 * @package App\Application\UseCases\Backend\Main\Responses
 */
final class ClearedDescriptionImages
{
    /**
     * @var bool
     */
    private $succeed;

    /**
     * @var int
     */
    private $deleted = 0;

    /**
     * @var string|null
     */
    private $message;

    /**
     * ClearedDescriptionImages constructor.
     * @param bool $succeed
     * @param int $deleted
     * @param string|null $message
     */
    public function __construct(bool $succeed = true, int $deleted = 0, string $message = null)
    {
        $this->succeed = $succeed;
        $this->deleted = $deleted;
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }

    /**
     * @return int
     */
    public function getDeleted(): int
    {
        return $this->deleted;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }
}
