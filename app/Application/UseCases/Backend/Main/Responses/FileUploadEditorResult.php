<?php


namespace App\Application\UseCases\Backend\Main\Responses;


final class FileUploadEditorResult
{
    /**
     * @var string|null
     */
    private $location;

    /**
     * @var bool
     */
    private $succeed;

    /**
     * FileUploadEditorResult constructor.
     * @param string|null $location
     * @param bool $succeed
     */
    public function __construct(string $location = null, bool $succeed = true)
    {
        $this->location = $location;
        $this->succeed = $succeed;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }
}
