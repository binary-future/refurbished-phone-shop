<?php


namespace App\Application\UseCases\Backend\Main\Responses;


/**
 * Class FileUploadResponse
 * @package App\Application\UseCases\Backend\Main\Responses
 */
class FileUploadResponse
{
    /**
     * @var int
     */
    private $succeed = 0;

    /**
     * @var int
     */
    private $failed = 0;

    /**
     * @var bool
     */
    private $isSuccess = true;

    /**
     * @return int
     */
    public function getSucceed(): int
    {
        return $this->succeed;
    }

    /**
     * @param int $succeed
     */
    public function setSucceed(int $succeed): void
    {
        $this->succeed = $succeed;
    }

    /**
     * @return int
     */
    public function getFailed(): int
    {
        return $this->failed;
    }

    /**
     * @param int $failed
     */
    public function setFailed(int $failed): void
    {
        $this->failed = $failed;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * @param bool $isSuccess
     */
    public function setIsSuccess(bool $isSuccess): void
    {
        $this->isSuccess = $isSuccess;
    }
}
