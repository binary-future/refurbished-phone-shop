<?php

namespace App\Application\UseCases\Backend\Main\Responses;

use Illuminate\Support\Collection;

final class MainPageResponse
{
    /**
     * @var Collection
     */
    private $stores;

    /**
     * @var Collection
     */
    private $dealsPositiveReports;

    /**
     * @var Collection
     */
    private $dealsNegativeReports;

    /**
     * @var Collection
     */
    private $inactiveModels;

    /**
     * @var Collection
     */
    private $phonesPositiveReports;

    /**
     * @var Collection
     */
    private $phonesNegativeReports;

    /**
     * @var Collection
     */
    private $withoutDealsModels;

    /**
     * @var Collection
     */
    private $plannedImports;

    /**
     * MainPageResponse constructor.
     * @param Collection $stores
     * @param Collection $dealsPositiveReports
     * @param Collection $dealsNegativeReports
     * @param Collection $inactiveModels
     * @param Collection $phonesPositiveReports
     * @param Collection $phonesNegativeReports
     * @param Collection $withoutDealsModels
     * @param Collection $plannedImports
     */
    public function __construct(
        Collection $stores,
        Collection $dealsPositiveReports,
        Collection $dealsNegativeReports,
        Collection $inactiveModels,
        Collection $phonesPositiveReports,
        Collection $phonesNegativeReports,
        Collection $withoutDealsModels,
        Collection $plannedImports
    ) {
        $this->stores = $stores;
        $this->dealsPositiveReports = $dealsPositiveReports;
        $this->dealsNegativeReports = $dealsNegativeReports;
        $this->inactiveModels = $inactiveModels;
        $this->phonesPositiveReports = $phonesPositiveReports;
        $this->phonesNegativeReports = $phonesNegativeReports;
        $this->withoutDealsModels = $withoutDealsModels;
        $this->plannedImports = $plannedImports;
    }


    /**
     * @return Collection
     */
    public function getStores(): Collection
    {
        return $this->stores;
    }

    /**
     * @param Collection $stores
     */
    public function setStores(Collection $stores): void
    {
        $this->stores = $stores;
    }

    /**
     * @return Collection
     */
    public function getDealsPositiveReports(): Collection
    {
        return $this->dealsPositiveReports;
    }

    /**
     * @return Collection
     */
    public function getDealsNegativeReports(): Collection
    {
        return $this->dealsNegativeReports;
    }

    /**
     * @return Collection
     */
    public function getPhonesPositiveReports(): Collection
    {
        return $this->phonesPositiveReports;
    }

    /**
     * @return Collection
     */
    public function getPhonesNegativeReports(): Collection
    {
        return $this->phonesNegativeReports;
    }

    /**
     * @return Collection
     */
    public function getInactiveModels(): Collection
    {
        return $this->inactiveModels;
    }

    /**
     * @return Collection
     */
    public function getWithoutDealsModels(): Collection
    {
        return $this->withoutDealsModels;
    }

    /**
     * @return Collection
     */
    public function getPlannedImports(): Collection
    {
        return $this->plannedImports;
    }
}
