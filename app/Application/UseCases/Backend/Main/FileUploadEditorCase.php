<?php


namespace App\Application\UseCases\Backend\Main;

use App\Application\UseCases\Backend\Main\Contracts\FileUploadEditorCase as Contract;
use App\Application\UseCases\Backend\Main\Responses\FileUploadEditorResult;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\File\Upload\Contract\FileUploader;
use Illuminate\Http\UploadedFile;

final class FileUploadEditorCase implements Contract
{
    private const CONFIG_PATH = 'file.upload.image.description-image';
    private const PATH = 'base_upload_path';
    private const DEFAULT_EXTENSION = 'base_extension';

    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @var array
     */
    private $configs = [];

    /**
     * FileUploadEditorCase constructor.
     * @param Config $config
     * @param FileUploader $fileUploader
     */
    public function __construct(Config $config, FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
        $this->configs = $config->get(self::CONFIG_PATH, []);
    }

    public function execute(UploadedFile $uploadedFile): FileUploadEditorResult
    {
        try {
            return $this->proceed($uploadedFile);
        } catch (\Throwable $exception) {
            return new FileUploadEditorResult('', false);
        }
    }

    /**
     * @param UploadedFile $file
     * @return FileUploadEditorResult
     */
    private function proceed(UploadedFile $file): FileUploadEditorResult
    {
        $storedPath = $this->fileUploader->store($file, $this->getPath(), $this->generateFileName($file));

        return new FileUploadEditorResult($this->transformPath($storedPath));
    }

    /**
     * @return mixed
     */
    private function getPath()
    {
        return $this->configs[self::PATH];
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function generateFileName(UploadedFile $file)
    {
        return sprintf(
            '%s.%s',
            str_random(32),
            $file->getClientOriginalExtension() ?: $this->configs[self::DEFAULT_EXTENSION]
        );
    }

    /**
     * Prepare path for entering to DB
     *
     * @param $path
     * @return mixed
     */
    private function transformPath(string $path)
    {
        return str_ireplace('public', 'storage', $path);
    }
}
