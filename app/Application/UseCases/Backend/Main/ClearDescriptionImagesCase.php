<?php

namespace App\Application\UseCases\Backend\Main;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\Main\Contracts\ClearDescriptionImagesCase as Contract;
use App\Application\UseCases\Backend\Main\Responses\ClearedDescriptionImages;
use App\Domain\Shared\Description\DescriptionImage;
use App\Domain\Shared\Description\Query\AllImagesQuery;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Adapters\Storage\Contracts\Storage;
use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Class ClearDescriptionImagesCase
 * @package App\Application\UseCases\Backend\Main
 */
final class ClearDescriptionImagesCase implements Contract
{
    private const STORE_PATH = 'public/upload/descriptions';
    private const MIN_ALLOWED_HOURS = 12;

    /**
     * @var Config
     */
    private $configs;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * ClearDescriptionImagesCase constructor.
     * @param Config $configs
     * @param Storage $storage
     */
    public function __construct(Config $configs, QueryBus $queryBus, Storage $storage)
    {
        $this->configs = $configs;
        $this->queryBus = $queryBus;
        $this->storage = $storage;
    }

    /**
     * @return ClearedDescriptionImages
     */
    public function execute(): ClearedDescriptionImages
    {
        try {
            return $this->proceed();
        } catch (\Throwable $exception) {
            return new ClearedDescriptionImages(false, 0, $exception->getMessage());
        }
    }

    private function proceed(): ClearedDescriptionImages
    {
        $images = $this->getImages();
        $storedImages = $this->getStoredImages();
        if ($images->isEmpty() || $storedImages->isEmpty()) {
            return new ClearedDescriptionImages();
        }

        return $this->deleteImages($this->generateDeleteImagesList($images, $storedImages));
    }

    private function getImages(): Collection
    {
        /**
         * @var Collection $images
         */
        $images = $this->queryBus->dispatch(new AllImagesQuery());

        return $images->map(function (DescriptionImage $image) {
            return basename($image->getPath());
        });
    }

    private function getStoredImages(): Collection
    {
        $storedImages = collect($this->storage->files(self::STORE_PATH));

        return $storedImages->map(function (string $path) {
            return basename($path);
        });
    }

    private function generateDeleteImagesList(Collection $images, Collection $storedImages): Collection
    {
        return $storedImages->diff($images);
    }

    private function deleteImages(Collection $images): ClearedDescriptionImages
    {
        $count = 0;
        foreach ($images as $image) {
            $imagePath = sprintf('%s/%s', self::STORE_PATH, $image);
            if ($this->shouldBeDeleted($imagePath)) {
                $this->storage->delete($imagePath);
                $count++;
            }
        }

        return new ClearedDescriptionImages(true, $count);
    }

    private function shouldBeDeleted(string $path): bool
    {
        $lastModified = Carbon::createFromTimestamp($this->storage->lastModified($path));
        $today = Carbon::now();

        return $today->diffInHours($lastModified) >= self::MIN_ALLOWED_HOURS;
    }
}
