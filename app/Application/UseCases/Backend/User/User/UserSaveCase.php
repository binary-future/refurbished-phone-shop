<?php

namespace App\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\CommandBus;
use App\Application\UseCases\Backend\User\User\Contracts\UserSaveCase as Contract;
use App\Application\UseCases\Backend\User\User\Request\UserSaveRequest;
use App\Application\UseCases\Backend\User\User\Response\UserSaveResponse;
use App\Domain\User\Command\CreateUserCommand;
use App\Domain\User\User;
use App\Utils\Adapters\Encryptor\Contract\Encryptor;
use App\Utils\Serializer\Contracts\Serializer;

final class UserSaveCase implements Contract
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Encryptor
     */
    private $encryptor;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * UserSaveCase constructor.
     * @param CommandBus $commandBus
     * @param Encryptor $encryptor
     * @param Serializer $serializer
     */
    public function __construct(CommandBus $commandBus, Encryptor $encryptor, Serializer $serializer)
    {
        $this->commandBus = $commandBus;
        $this->encryptor = $encryptor;
        $this->serializer = $serializer;
    }

    public function execute(UserSaveRequest $request): UserSaveResponse
    {
        try {
            return $this->proceed($request);
        } catch (\Throwable $exception) {
            $response = new UserSaveResponse();
            $response->setSucceed(false);
            return $response;
        }
    }

    private function proceed(UserSaveRequest $request): UserSaveResponse
    {
        $request->setPassword($this->encryptor->encrypt($request->getPassword()));
        /**
         * @var User $user
         */
        $user = $this->serializer->fromArray(User::class, $request->getParams());
        $savedUser = $this->commandBus->dispatch(new CreateUserCommand($user));

        return new UserSaveResponse($savedUser);
    }
}
