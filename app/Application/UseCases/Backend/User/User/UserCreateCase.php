<?php

namespace App\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\Contracts\UserCreateCase as Contract;
use App\Application\UseCases\Backend\User\User\Response\UserCreateResponse;
use App\Domain\User\Query\AllRolesExceptRoleQuery;
use App\Domain\User\Role;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class UserCreateCase
 * @package App\Application\UseCases\Backend\User\User
 */
final class UserCreateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * UserCreateCase constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @return UserCreateResponse
     */
    public function execute(): UserCreateResponse
    {
        /**
         * @var Collection $roles
         */
        $roles = $this->queryBus->dispatch(new AllRolesExceptRoleQuery(Role::SUPER_ADMIN));
        if ($roles->isEmpty()) {
            throw new ModelNotFoundException();
        }

        return new UserCreateResponse($roles);
    }
}
