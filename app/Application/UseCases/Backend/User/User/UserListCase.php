<?php

namespace App\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\Contracts\UserListCase as Contract;
use App\Application\UseCases\Backend\User\User\Response\UserListResponse;
use App\Domain\User\Query\GetNotSuperAdminUsersExceptUserQuery;
use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class UserListCase
 * @package App\Application\UseCases\Backend\User\User
 */
final class UserListCase implements Contract
{
    /**
     * @var Auth
     */
    private $authService;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * UserListCase constructor.
     * @param Auth $authService
     * @param QueryBus $queryBus
     */
    public function __construct(Auth $authService, QueryBus $queryBus)
    {
        $this->authService = $authService;
        $this->queryBus = $queryBus;
    }

    /**
     * @return UserListResponse
     */
    public function execute(): UserListResponse
    {
        $user = $this->authService->getAuthUser();
        if (! $user) {
            throw new ModelNotFoundException();
        }

        return new UserListResponse($user, $this->getUsers($user));
    }

    /**
     * @param User $user
     * @return Collection
     */
    private function getUsers(User $user): Collection
    {
        /**
         * @var Collection $users
         */
        $users = $this->queryBus->dispatch(new GetNotSuperAdminUsersExceptUserQuery($user, ['role']));

        return $users;
    }
}
