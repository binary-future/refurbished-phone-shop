<?php

namespace App\Application\UseCases\Backend\User\User\Response;

use App\Domain\User\User;

final class UserDeleteResponse
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var bool
     */
    private $success = true;

    /**
     * UserDeleteResponse constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }
}