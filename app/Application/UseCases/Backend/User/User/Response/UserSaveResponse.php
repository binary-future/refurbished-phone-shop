<?php

namespace App\Application\UseCases\Backend\User\User\Response;

use App\Domain\User\User;

final class UserSaveResponse
{
    /**
     * @var User|null
     */
    private $user;

    /**
     * @var bool
     */
    private $succeed = true;

    /**
     * UserSaveResponse constructor.
     * @param User|null $user
     */
    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }

    /**
     * @param bool $succeed
     */
    public function setSucceed(bool $succeed): void
    {
        $this->succeed = $succeed;
    }
}
