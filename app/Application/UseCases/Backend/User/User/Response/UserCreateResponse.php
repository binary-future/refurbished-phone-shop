<?php

namespace App\Application\UseCases\Backend\User\User\Response;

use Illuminate\Support\Collection;

/**
 * Class UserCreateResponse
 * @package App\Application\UseCases\Backend\User\User\Response
 */
final class UserCreateResponse
{
    /**
     * @var Collection
     */
    private $roles;

    /**
     * UserCreateResponse constructor.
     * @param Collection $roles
     */
    public function __construct(Collection $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }
}
