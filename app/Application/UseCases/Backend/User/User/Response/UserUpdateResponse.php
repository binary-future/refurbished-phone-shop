<?php

namespace App\Application\UseCases\Backend\User\User\Response;

use App\Domain\User\User;

/**
 * Class UserUpdateResponse
 * @package App\Application\UseCases\Backend\User\User\Response
 */
final class UserUpdateResponse
{
    /**
     * @var User $user
     */
    private $user;

    /**
     * @var bool
     */
    private $succeed = true;

    /**
     * UserUpdateResponse constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }

    /**
     * @param bool $succeed
     */
    public function setSucceed(bool $succeed): void
    {
        $this->succeed = $succeed;
    }
}
