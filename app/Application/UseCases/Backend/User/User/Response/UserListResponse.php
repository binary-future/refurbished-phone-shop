<?php

namespace App\Application\UseCases\Backend\User\User\Response;

use App\Domain\User\User;
use Illuminate\Support\Collection;

/**
 * Class UserListResponse
 * @package App\Application\UseCases\Backend\User\User\Response
 */
final class UserListResponse
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Collection
     */
    private $users;

    /**
     * UserListResponse constructor.
     * @param User $user
     * @param Collection $users
     */
    public function __construct(User $user, Collection $users)
    {
        $this->user = $user;
        $this->users = $users;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }
}
