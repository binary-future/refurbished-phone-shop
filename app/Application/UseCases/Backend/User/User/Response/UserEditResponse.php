<?php

namespace App\Application\UseCases\Backend\User\User\Response;

use App\Domain\User\User;
use Illuminate\Support\Collection;

final class UserEditResponse
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Collection
     */
    private $roles;

    /**
     * UserEditResponse constructor.
     * @param User $user
     * @param Collection $roles
     */
    public function __construct(User $user, Collection $roles)
    {
        $this->user = $user;
        $this->roles = $roles;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }
}
