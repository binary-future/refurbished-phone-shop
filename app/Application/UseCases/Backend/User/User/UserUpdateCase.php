<?php

namespace App\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\Contracts\UserUpdateCase as Contract;
use App\Application\UseCases\Backend\User\User\Request\UserUpdateRequest;
use App\Application\UseCases\Backend\User\User\Response\UserUpdateResponse;
use App\Domain\User\Command\UpdateUserCommand;
use App\Domain\User\Query\GetUserByIdQuery;
use App\Domain\User\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class UserUpdateCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * UserUpdateCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function execute(UserUpdateRequest $request): UserUpdateResponse
    {
        $user = $this->getUser($request->getUserId());

        return $this->updateUser($user, $request);
    }

    private function getUser(int $userId): User
    {
        /**
         * @var User $user
         */
        $user = $this->queryBus->dispatch(new GetUserByIdQuery($userId))->first();
        if (! $user) {
            throw new ModelNotFoundException();
        }

        return $user;
    }

    private function updateUser(User $user, UserUpdateRequest $request): UserUpdateResponse
    {
        try {
            /**
             * @var User $updatedUser
             */
            $updatedUser = $this->commandBus->dispatch(new UpdateUserCommand($user, $request->getParams()));
            return new UserUpdateResponse($updatedUser);
        } catch (\Throwable $exception) {
            $response = new UserUpdateResponse($user);
            $response->setSucceed(false);
            return $response;
        }
    }
}