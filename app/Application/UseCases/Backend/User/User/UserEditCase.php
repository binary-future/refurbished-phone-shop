<?php

namespace App\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\Contracts\UserEditCase as Contract;
use App\Application\UseCases\Backend\User\User\Response\UserEditResponse;
use App\Domain\User\Query\AllRolesExceptRoleQuery;
use App\Domain\User\Query\GetUserByIdQuery;
use App\Domain\User\Role;
use App\Domain\User\User;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

/**
 * Class UserEditCase
 * @package App\Application\UseCases\Backend\User\User
 */
final class UserEditCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var Specification
     */
    private $specification;

    /**
     * UserEditCase constructor.
     * @param QueryBus $queryBus
     * @param Specification $specification
     */
    public function __construct(QueryBus $queryBus, Specification $specification)
    {
        $this->queryBus = $queryBus;
        $this->specification = $specification;
    }

    /**
     * @param int $userId
     * @return UserEditResponse
     */
    public function execute(int $userId): UserEditResponse
    {
        $user = $this->getUser($userId);
        if (! $this->specification->isSatisfy($user)) {
            throw new ModelNotFoundException();
        }
        $roles = $this->getRoles();

        return new UserEditResponse($user, $roles);
    }

    /**
     * @param int $userId
     * @return User
     */
    private function getUser(int $userId): User
    {
        /**
         * @var User $user
         */
        $user = $this->queryBus->dispatch(new GetUserByIdQuery($userId))->first();
        if (! $user) {
            throw new ModelNotFoundException();
        }

        return $user;
    }

    /**
     * @return Collection
     */
    private function getRoles(): Collection
    {
        /**
         * @var Collection $roles
         */
        $roles = $this->queryBus->dispatch(new AllRolesExceptRoleQuery(Role::SUPER_ADMIN));
        if ($roles->isEmpty()) {
            throw new ModelNotFoundException();
        }

        return $roles;
    }
}
