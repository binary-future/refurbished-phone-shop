<?php

namespace App\Application\UseCases\Backend\User\User\Contracts;

use App\Application\UseCases\Backend\User\User\Request\UserSaveRequest;
use App\Application\UseCases\Backend\User\User\Response\UserSaveResponse;

/**
 * Interface UserSaveCase
 * @package App\Application\UseCases\Backend\User\User\Contracts
 */
interface UserSaveCase
{
    /**
     * @param UserSaveRequest $request
     * @return UserSaveResponse
     */
    public function execute(UserSaveRequest $request): UserSaveResponse;
}