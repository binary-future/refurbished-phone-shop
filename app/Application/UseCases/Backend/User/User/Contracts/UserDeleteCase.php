<?php

namespace App\Application\UseCases\Backend\User\User\Contracts;

use App\Application\UseCases\Backend\User\User\Response\UserDeleteResponse;

/**
 * Interface UserDeleteCase
 * @package App\Application\UseCases\Backend\User\User\Contracts
 */
interface UserDeleteCase
{
    /**
     * @param int $userId
     * @return UserDeleteResponse
     */
    public function execute(int $userId): UserDeleteResponse;
}