<?php

namespace App\Application\UseCases\Backend\User\User\Contracts;

use App\Application\UseCases\Backend\User\User\Response\UserEditResponse;

/**
 * Interface UserEditCase
 * @package App\Application\UseCases\Backend\User\User\Contracts
 */
interface UserEditCase
{
    /**
     * @param int $userId
     * @return UserEditResponse
     */
    public function execute(int $userId): UserEditResponse;
}