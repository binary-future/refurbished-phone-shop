<?php

namespace App\Application\UseCases\Backend\User\User\Contracts;

use App\Application\UseCases\Backend\User\User\Request\UserUpdateRequest;
use App\Application\UseCases\Backend\User\User\Response\UserUpdateResponse;

/**
 * Interface UserUpdateCase
 * @package App\Application\UseCases\Backend\User\User\Contracts
 */
interface UserUpdateCase
{
    /**
     * @param UserUpdateRequest $request
     * @return UserUpdateResponse
     */
    public function execute(UserUpdateRequest $request): UserUpdateResponse;
}