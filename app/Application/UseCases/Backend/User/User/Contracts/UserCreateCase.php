<?php

namespace App\Application\UseCases\Backend\User\User\Contracts;

use App\Application\UseCases\Backend\User\User\Response\UserCreateResponse;

/**
 * Interface UserCreateCase
 * @package App\Application\UseCases\Backend\User\User\Contracts
 */
interface UserCreateCase
{
    /**
     * @return mixed
     */
    public function execute(): UserCreateResponse;
}
