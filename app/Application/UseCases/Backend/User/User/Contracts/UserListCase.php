<?php

namespace App\Application\UseCases\Backend\User\User\Contracts;

use App\Application\UseCases\Backend\User\User\Response\UserListResponse;

/**
 * Interface UserListCase
 * @package App\Application\UseCases\Backend\User\User\Contracts
 */
interface UserListCase
{
    /**
     * @return UserListResponse
     */
    public function execute(): UserListResponse;
}