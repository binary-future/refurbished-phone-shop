<?php

namespace App\Application\UseCases\Backend\User\User;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\UseCases\Backend\User\User\Contracts\UserDeleteCase as Contract;
use App\Application\UseCases\Backend\User\User\Response\UserDeleteResponse;
use App\Domain\User\Command\DeleteUserCommand;
use App\Domain\User\Query\GetUserByIdQuery;
use App\Domain\User\User;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class UserDeleteCase implements Contract
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var Specification
     */
    private $specification;

    /**
     * UserDeleteCase constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     * @param Specification $specification
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus, Specification $specification)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
        $this->specification = $specification;
    }


    public function execute(int $userId): UserDeleteResponse
    {
        $user = $this->getUser($userId);
        if (!$this->specification->isSatisfy($user)) {
            throw new ModelNotFoundException();
        }

        return $this->deleteUser($user);
    }

    /**
     * @param int $userId
     * @return User
     */
    private function getUser(int $userId): User
    {
        /**
         * @var User $user
         */
        $user = $this->queryBus->dispatch(new GetUserByIdQuery($userId))->first();
        if (!$user) {
            throw new ModelNotFoundException();
        }

        return $user;
    }

    private function deleteUser(User $user): UserDeleteResponse
    {
        $response = new UserDeleteResponse($user);

        try {
            $this->commandBus->dispatch(new DeleteUserCommand($user));
        } catch (\Throwable $exception) {
            $response->setSuccess(false);
        }

        return $response;
    }
}