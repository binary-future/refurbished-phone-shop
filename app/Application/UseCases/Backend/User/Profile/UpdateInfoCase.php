<?php

namespace App\Application\UseCases\Backend\User\Profile;

use App\Application\Services\Bus\CommandBus;
use App\Application\UseCases\Backend\User\Profile\Contracts\UpdateInfoCase as Contract;
use App\Application\UseCases\Backend\User\Profile\Requests\UpdateInfoRequest;
use App\Application\UseCases\Backend\User\Profile\Responses\ProfileUpdateResponse;
use App\Domain\User\Command\UpdateUserCommand;
use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use App\Utils\Specification\Contracts\Specification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class UpdateInfoCase implements Contract
{
    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var Specification
     */
    private $specification;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * UpdateInfoCase constructor.
     * @param Auth $auth
     * @param Specification $specification
     * @param CommandBus $commandBus
     */
    public function __construct(Auth $auth, Specification $specification, CommandBus $commandBus)
    {
        $this->auth = $auth;
        $this->specification = $specification;
        $this->commandBus = $commandBus;
    }

    public function execute(UpdateInfoRequest $request): ProfileUpdateResponse
    {
        $user = $this->auth->getAuthUser();
        if (! $user) {
            throw new ModelNotFoundException();
        }

        return $this->proceed($request, $user);
    }

    private function proceed(UpdateInfoRequest $request, User $user): ProfileUpdateResponse
    {
        try {
            return $this->checkPassword($request->getCheckPassword(), $user->getAuthPassword())
                ? $this->updateUser($user, $request)
                : new ProfileUpdateResponse($user, false, 'Invalid password');
        } catch (\Throwable $exception) {
            return new ProfileUpdateResponse($user, false);
        }
    }

    private function checkPassword(string $checkPassword, string $originalHash): bool
    {
        return $this->specification->isSatisfy($checkPassword, ['hash' => $originalHash]);
    }

    private function updateUser(User $user, UpdateInfoRequest $request): ProfileUpdateResponse
    {
        /**
         * @var User $updatedUser
         */
        $updatedUser = $this->commandBus->dispatch(new UpdateUserCommand($user, $request->getParams()));

        return new ProfileUpdateResponse($updatedUser);
    }
}
