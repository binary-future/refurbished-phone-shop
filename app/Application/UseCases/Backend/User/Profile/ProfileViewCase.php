<?php

namespace App\Application\UseCases\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Contracts\ProfileViewCase as Contract;
use App\Application\UseCases\Backend\User\Profile\Responses\ProfileViewResponse;
use App\Utils\Adapters\Auth\Contracts\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ProfileViewCase
 * @package App\Application\UseCases\Backend\User\Profile
 */
final class ProfileViewCase implements Contract
{
    /**
     * @var Auth
     */
    private $auth;

    /**
     * ProfileViewCase constructor.
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @return ProfileViewResponse
     */
    public function execute(): ProfileViewResponse
    {
        $user = $this->auth->getAuthUser();
        if (! $user) {
            throw new ModelNotFoundException();
        }

        return new ProfileViewResponse($user);
    }
}
