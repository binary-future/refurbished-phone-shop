<?php

namespace App\Application\UseCases\Backend\User\Profile\Contracts;

use App\Application\UseCases\Backend\User\Profile\Responses\ProfileViewResponse;

/**
 * Interface ProfileViewCase
 * @package App\Application\UseCases\Backend\User\Profile\Contracts
 */
interface ProfileViewCase
{
    /**
     * @return ProfileViewResponse
     */
    public function execute(): ProfileViewResponse;
}