<?php

namespace App\Application\UseCases\Backend\User\Profile\Contracts;

use App\Application\UseCases\Backend\User\Profile\Requests\UpdateInfoRequest;
use App\Application\UseCases\Backend\User\Profile\Responses\ProfileUpdateResponse;

/**
 * Interface UpdateInfoCase
 * @package App\Application\UseCases\Backend\User\Profile\Contracts
 */
interface UpdateInfoCase
{
    /**
     * @param UpdateInfoRequest $request
     * @return ProfileUpdateResponse
     */
    public function execute(UpdateInfoRequest $request): ProfileUpdateResponse;
}
