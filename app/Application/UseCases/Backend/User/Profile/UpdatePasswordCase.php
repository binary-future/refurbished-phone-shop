<?php

namespace App\Application\UseCases\Backend\User\Profile;

use App\Application\UseCases\Backend\User\Profile\Contracts\UpdatePasswordCase as Contract;
use App\Application\UseCases\Backend\User\Profile\Requests\UpdateInfoRequest;
use App\Application\UseCases\Backend\User\Profile\Responses\ProfileUpdateResponse;
use App\Application\UseCases\Backend\User\Profile\Contracts\UpdateInfoCase;
use App\Utils\Adapters\Encryptor\Contract\Encryptor;

/**
 * Class UpdatePasswordCase
 * @package App\Application\UseCases\Backend\User\Profile
 */
final class UpdatePasswordCase implements Contract
{
    /**
     * @var UpdateInfoCase
     */
    private $case;

    /**
     * @var Encryptor
     */
    private $encryptor;

    /**
     * UpdatePasswordCase constructor.
     * @param UpdateInfoCase $case
     * @param Encryptor $encryptor
     */
    public function __construct(UpdateInfoCase $case, Encryptor $encryptor)
    {
        $this->case = $case;
        $this->encryptor = $encryptor;
    }

    /**
     * @param UpdateInfoRequest $request
     * @return ProfileUpdateResponse
     */
    public function execute(UpdateInfoRequest $request): ProfileUpdateResponse
    {
        $request->setPassword($this->encryptor->encrypt($request->getPassword()));

        return $this->case->execute($request);
    }
}
