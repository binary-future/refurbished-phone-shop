<?php

namespace App\Application\UseCases\Backend\User\Profile\Responses;

use App\Domain\User\User;

/**
 * Class ProfileViewResponse
 * @package App\Application\UseCases\Backend\User\Profile\Responses
 */
final class ProfileViewResponse
{
    /**
     * @var User
     */
    private $user;

    /**
     * ProfileViewResponse constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}