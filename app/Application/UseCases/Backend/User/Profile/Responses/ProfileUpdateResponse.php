<?php

namespace App\Application\UseCases\Backend\User\Profile\Responses;

use App\Domain\User\User;

/**
 * Class ProfileUpdateResponse
 * @package App\Application\UseCases\Backend\User\Profile\Responses
 */
final class ProfileUpdateResponse
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var bool
     */
    private $succeed = true;

    /**
     * @var string|null
     */
    private $message;

    /**
     * ProfileUpdateResponse constructor.
     * @param User $user
     * @param bool $succeed
     * @param string|null $message
     */
    public function __construct(User $user, bool $succeed = true, string $message = null)
    {
        $this->user = $user;
        $this->succeed = $succeed;
        $this->message = $message;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isSucceed(): bool
    {
        return $this->succeed;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param null|string $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
