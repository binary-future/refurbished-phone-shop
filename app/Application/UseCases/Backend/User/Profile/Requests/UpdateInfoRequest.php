<?php

namespace App\Application\UseCases\Backend\User\Profile\Requests;

/**
 * Class UpdateInfoRequest
 * @package App\Application\UseCases\Backend\User\Profile\Requests
 */
final class UpdateInfoRequest
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $checkPassword;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCheckPassword(): string
    {
        return $this->checkPassword;
    }

    /**
     * @param string $checkPassword
     */
    public function setCheckPassword(string $checkPassword): void
    {
        $this->checkPassword = $checkPassword;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        $params = [
            'email' => $this->email,
            'name' => $this->name,
            'password' => $this->password,
        ];

        return array_filter($params);
    }
}
