<?php


namespace App\Application\UseCases\Download;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Downloader;
use App\Application\UseCases\Download\Contracts\DownloadImageCase as Contract;
use App\Application\UseCases\Download\Responses\DownloadImageResponse;
use App\Domain\Phone\Phone\Command\UpdatePhoneCommand;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Query\PhonesWithNotLocalImagesQuery;
use App\Utils\Adapters\Log\Contracts\Logger;
use Illuminate\Support\Collection;
use App\Application\UseCases\Download\Contracts\OwnerDownloadImageCase;

/**
 * Class DownloadImageCase
 * @package App\Application\UseCases\Download
 */
final class DownloadImageCase implements Contract
{
    /**
     * @var OwnerDownloadImageCase
     */
    private $storeImageCase;

    /**
     * @var OwnerDownloadImageCase
     */
    private $phoneImageCase;
    /**
     * DownloadImageCase constructor.
     * @param OwnerDownloadImageCase $storeImageCase
     * @param OwnerDownloadImageCase $phoneImageCase
     */
    public function __construct(OwnerDownloadImageCase $storeImageCase, OwnerDownloadImageCase $phoneImageCase)
    {
        $this->storeImageCase = $storeImageCase;
        $this->phoneImageCase = $phoneImageCase;
    }

    /**
     * @return DownloadImageResponse[]
     */
    public function execute(): array
    {
        return [
            $this->storeImageCase->execute(),
            $this->phoneImageCase->execute()
        ];
    }
}
