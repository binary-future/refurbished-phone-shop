<?php


namespace App\Application\UseCases\Download\Responses;


use Illuminate\Support\Collection;

/**
 * Class DownloadImageResponse
 * @package App\Application\UseCases\Download\Responses
 */
final class DownloadImageResponse
{
    /**
     * @var Collection
     */
    private $owners;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $isSuccess = true;

    /**
     * @var string|null
     */
    private $message;

    /**
     * DownloadImageResponse constructor.
     * @param Collection $owners
     * @param string $type
     */
    public function __construct(Collection $owners, string $type)
    {
        $this->owners = $owners;
        $this->type = $type;
    }

    /**
     * @return Collection
     */
    public function getOwners(): Collection
    {
        return $this->owners;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    /**
     * @param bool $isSuccess
     */
    public function setSuccess(bool $isSuccess): void
    {
        $this->isSuccess = $isSuccess;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
