<?php

namespace App\Application\UseCases\Download;

use App\Domain\Shared\Contracts\HasImage;
use App\Domain\Store\Commands\UpdateStoreCommand;
use App\Domain\Store\Query\HasLocalImageQuery;
use App\Domain\Store\Store;
use Illuminate\Support\Collection;

/**
 * Class StoresDownloadImageCase
 * @package App\Application\UseCases\Download
 */
final class StoresDownloadImageCase extends OwnerDownloadImageCase
{
    /**
     * @return Collection
     */
    protected function getOwners(): Collection
    {
        /**
         * @var Collection $stores;
         */
        $stores = $this->queryBus->dispatch(new HasLocalImageQuery());

        return $stores;
    }

    /**
     * @param HasImage $owner
     * @param Collection $images
     * @return HasImage
     */
    protected function updateOwner(HasImage $owner, Collection $images): HasImage
    {
        /**
         * @var Store $owner
         * @var Store $updatedStore
         */
        $updatedStore = $this->commandBus->dispatch(new UpdateStoreCommand($owner, [], ['image' => $images->first()]));

        return $updatedStore;
    }

    /**
     * @return string
     */
    protected function getType(): string
    {
        return 'stores';
    }
}
