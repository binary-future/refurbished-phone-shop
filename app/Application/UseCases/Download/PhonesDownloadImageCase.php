<?php


namespace App\Application\UseCases\Download;


use App\Domain\Phone\Phone\Command\UpdatePhoneCommand;
use App\Domain\Phone\Phone\Phone;
use App\Domain\Phone\Phone\Query\PhonesWithNotLocalImagesQuery;
use App\Domain\Shared\Contracts\HasImage;
use Illuminate\Support\Collection;

/**
 * Class PhonesDownloadImageCase
 * @package App\Application\UseCases\Download
 */
final class PhonesDownloadImageCase extends OwnerDownloadImageCase
{
    /**
     * @return Collection
     */
    protected function getOwners(): Collection
    {
        /**
         * @var Collection $phones
         */
        $phones = $this->queryBus->dispatch(new PhonesWithNotLocalImagesQuery(['model', 'model.brand']));

        return $phones;
    }

    /**
     * @param HasImage $owner
     * @param Collection $images
     * @return HasImage
     */
    protected function updateOwner(HasImage $owner, Collection $images): HasImage
    {
        /**
         * @var Phone $owner
         * @var Phone $phone
         */
        $command = new UpdatePhoneCommand($owner);
        $command->setUpdatedImages($images);
        $phone = $this->commandBus->dispatch($command);

        return $phone;
    }

    /**
     * @return string
     */
    protected function getType(): string
    {
        return 'phones';
    }
}
