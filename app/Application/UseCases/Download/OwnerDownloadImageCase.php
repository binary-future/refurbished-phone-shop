<?php

namespace App\Application\UseCases\Download;

use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\File\Image\Downloader;
use App\Application\UseCases\Download\Responses\DownloadImageResponse;
use App\Domain\Shared\Contracts\HasImage;
use App\Utils\Adapters\Log\Contracts\Logger;
use Illuminate\Support\Collection;
use App\Application\UseCases\Download\Contracts\OwnerDownloadImageCase as Contract;

abstract class OwnerDownloadImageCase implements Contract
{
    /**
     * @var QueryBus
     */
    protected $queryBus;

    /**
     * @var Downloader
     */
    private $downloader;

    /**
     * @var CommandBus
     */
    protected $commandBus;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * DownloadImageCase constructor.
     * @param QueryBus $queryBus
     * @param Downloader $downloader
     * @param CommandBus $commandBus
     * @param Logger $logger
     */
    public function __construct(
        QueryBus $queryBus,
        Downloader $downloader,
        CommandBus $commandBus,
        Logger $logger
    ) {
        $this->queryBus = $queryBus;
        $this->downloader = $downloader;
        $this->commandBus = $commandBus;
        $this->logger = $logger;
    }

    /**
     * @return DownloadImageResponse
     */
    public function execute(): DownloadImageResponse
    {
        try {
            return $this->proceed();
        } catch (\Throwable $exception) {
            $response = new DownloadImageResponse(collect(), $this->getType());
            $response->setSuccess(false);
            $response->setMessage($exception->getMessage());

            return $response;
        }
    }

    /**
     * @return DownloadImageResponse
     * @throws \App\Utils\File\Download\Exception\FileDownloaderException
     */
    private function proceed(): DownloadImageResponse
    {
        $owners = $this->getOwners();
        $result = collect();
        foreach ($owners as $owner) {
            /**
             * @var HasImage $owner
             */
            $this->logger->line(
                sprintf(
                    'Downloading images for %s',
                    $owner->getOwnerName()
                )
            );
            $result->push($this->downloadImages($owner));
        }

        return new DownloadImageResponse($result, $this->getType());
    }

    abstract protected function getOwners(): Collection;

    /**
     * @param HasImage $owner
     * @return HasImage
     * @throws \App\Utils\File\Download\Exception\FileDownloaderException
     */
    private function downloadImages(HasImage $owner)
    {
        $images = $this->downloader->download($owner);

        return $this->updateOwner($owner, $images);
    }

    abstract protected function updateOwner(HasImage $owner, Collection $images): HasImage;

    /**
     * @return string
     */
    abstract protected function getType(): string;
}
