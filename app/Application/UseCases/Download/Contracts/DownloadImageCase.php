<?php


namespace App\Application\UseCases\Download\Contracts;


use App\Application\UseCases\Download\Responses\DownloadImageResponse;

/**
 * Interface DownloadImageCase
 * @package App\Application\UseCases\Download\Contracts
 */
interface DownloadImageCase
{
    /**
     * @return DownloadImageResponse[]
     */
    public function execute(): array;
}
