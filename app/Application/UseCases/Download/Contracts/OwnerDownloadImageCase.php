<?php


namespace App\Application\UseCases\Download\Contracts;


use App\Application\UseCases\Download\Responses\DownloadImageResponse;

/**
 * Interface OwnerDownloadImageCase
 * @package App\Application\UseCases\Download\Contracts
 */
interface OwnerDownloadImageCase
{
    /**
     * @return DownloadImageResponse
     */
    public function execute(): DownloadImageResponse;
}
