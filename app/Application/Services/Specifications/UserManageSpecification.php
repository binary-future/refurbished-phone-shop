<?php

namespace App\Application\Services\Specifications;


use App\Domain\User\User;
use App\Utils\Adapters\Auth\Contracts\Auth;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Specification\Factory;

final class UserManageSpecification implements Specification
{
    public const SPECIFICATION_ALIAS = 'user-manage';

    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var Factory
     */
    private $factory;

    /**
     * UserManageSpecification constructor.
     * @param Auth $auth
     * @param Factory $factory
     */
    public function __construct(Auth $auth, Factory $factory)
    {
        $this->auth = $auth;
        $this->factory = $factory;
    }

    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        if (! $value instanceof User) {
            return false;
        }

        $authUser = $this->auth->getAuthUser();

        return $authUser ? $this->proceedChecking($value, $authUser) : false;
    }

    /**
     * @param User $user
     * @param User $authUser
     * @return bool
     */
    private function proceedChecking(User $user, User $authUser): bool
    {
        try {
            $specification = $this->getSpecification();
            return $specification->isSatisfy($user, ['auth' => $authUser]);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * @return Specification
     * @throws \App\Utils\Specification\Exceptions\SpecificationBuildException
     */
    private function getSpecification(): Specification
    {
        return $this->factory->buildSpecification(self::SPECIFICATION_ALIAS);
    }
}
