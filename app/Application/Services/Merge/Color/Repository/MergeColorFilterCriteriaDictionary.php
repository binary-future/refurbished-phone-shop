<?php


namespace App\Application\Services\Merge\Color\Repository;


use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface MergeColorFilterCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_FILTER = 'by-filter';
    public const CRITERIA_BY_PARENT_ID = 'by-parent-id';
}
