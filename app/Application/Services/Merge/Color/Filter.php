<?php

namespace App\Application\Services\Merge\Color;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

final class Filter extends Model
{
    public const FIELD_FILTER_ID = 'filter_id';
    public const FIELD_NAME = 'name';
    public const FIELD_FILTER = 'filter';

    public const RELATION_PARENT = 'parent';
    public const RELATION_CHILDREN = 'children';

    protected $table = 'color_synonyms_filters';

    protected $fillable = [
        self::FIELD_FILTER_ID, self::FIELD_FILTER, self::FIELD_NAME
    ];

    public function getFilterId(): ?int
    {
        return $this->getAttribute(self::FIELD_FILTER_ID);
    }

    public function setFilterId(int $filterId): void
    {
        $this->setAttribute(self::FIELD_FILTER_ID, $filterId);
    }

    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    public function setName(string $name): void
    {
        if ($this->getParent()) {
            $name = sprintf('%s-%s', $this->getParent()->getName(), $name);
        }
        $this->setAttribute(self::FIELD_NAME, $name);
    }

    public function getFilter(): string
    {
        return $this->getAttribute(self::FIELD_FILTER);
    }

    public function setFilter(string $filter): void
    {
        $this->setAttribute(self::FIELD_FILTER, $filter);
    }

    public function getParent(): ?self
    {
        return $this->getRelationValue(self::RELATION_PARENT);
    }

    public function getChildren(): Collection
    {
        return $this->getRelationValue(self::RELATION_CHILDREN);
    }

    public function hasChildren(): bool
    {
        return $this->getChildren()->isNotEmpty();
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, self::FIELD_FILTER_ID);
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, self::FIELD_FILTER_ID);
    }
}
