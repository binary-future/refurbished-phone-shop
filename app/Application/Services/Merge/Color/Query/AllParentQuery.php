<?php

namespace App\Application\Services\Merge\Color\Query;

use App\Application\Services\Merge\Color\Repository\MergeColorFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

final class AllParentQuery implements Query, MergeColorFilterCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations;

    /**
     * AllQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }


    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_PARENT_ID => null,
            self::WITH => $this->relations
        ];
    }
}
