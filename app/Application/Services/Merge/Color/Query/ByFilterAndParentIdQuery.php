<?php

namespace App\Application\Services\Merge\Color\Query;

use App\Application\Services\Merge\Color\Repository\MergeColorFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class ByFilterAndParentIdQuery
 * @package App\Application\Services\Merge\Color\Query
 */
final class ByFilterAndParentIdQuery implements Query, MergeColorFilterCriteriaDictionary
{
    /**
     * @var string
     */
    private $filter;

    /**
     * @var int|null
     */
    private $parentId;

    /**
     * ByFilterAndParentIdQuery constructor.
     * @param string $filter
     * @param int|null $parentId
     */
    public function __construct(string $filter, ?int $parentId)
    {
        $this->filter = $filter;
        $this->parentId = $parentId;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_FILTER => $this->filter,
            self::CRITERIA_BY_PARENT_ID => $this->parentId
        ];
    }
}