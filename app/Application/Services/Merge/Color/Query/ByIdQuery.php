<?php

namespace App\Application\Services\Merge\Color\Query;

use App\Application\Services\Merge\Color\Repository\MergeColorFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class ByIdQuery
 * @package App\Application\Services\Merge\Color\Query
 */
final class ByIdQuery implements Query, MergeColorFilterCriteriaDictionary
{
    /**
     * @var int
     */
    private $filterId;

    /**
     * @var array
     */
    private $relations;

    /**
     * ByIdQuery constructor.
     * @param int $filterId
     * @param array $relations
     */
    public function __construct(int $filterId, array $relations = [])
    {
        $this->filterId = $filterId;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ID => $this->filterId,
            self::WITH => $this->relations
        ];
    }
}
