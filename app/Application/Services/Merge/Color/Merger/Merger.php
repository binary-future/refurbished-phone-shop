<?php


namespace App\Application\Services\Merge\Color\Merger;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\DTO\ParentModelId;
use App\Application\Services\Merge\Model\DTO\SynonymsIds;
use App\Application\Services\Merge\Model\MergeGroup;
use App\Domain\Phone\Model\Command\UpdateModelCommand;
use App\Domain\Phone\Phone\Command\UpdateColorCommand;
use App\Domain\Phone\Phone\Query\ColorByIdQuery;
use App\Domain\Phone\Phone\Query\ColorByIdsQuery;
use App\Domain\Phone\Phone\Color;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class Merger
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Merger constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function merge(MergeGroup $group)
    {
        $parent = $this->getParent($group->getParentId());
        $synonyms = $this->getSynonyms($group->getSynonymIds());
        if ($parent->isSynonym()) {
            $parent->refreshBaseColor();
            $this->saveColor($parent);
        }
        $this->assignParentToSynonyms($parent, $synonyms);
    }

    private function getParent(ParentModelId $parentId): ?Color
    {
        /**
         * @var Color $color
         */
        $color = $this->queryBus->dispatch(new ColorByIdQuery($parentId->getParentId(), ['originalPhones']))->first();
        if (! $color) {
            throw new ModelNotFoundException();
        }

        return $color;
    }

    private function getSynonyms(SynonymsIds $synonymsIds): Collection
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch(new ColorByIdsQuery($synonymsIds->getIds(), ['synonyms', 'phones']));

        return $this->combineWithSynonyms($models);
    }

    private function combineWithSynonyms(Collection $colors): Collection
    {
        if ($colors->isEmpty()) {
            return $colors;
        }

        $synonyms = collect();
        foreach ($colors as $color) {
            /**
             * @var Color $color
             */
            $synonyms->push($color);
            $synonyms = $synonyms->merge($color->getSynonyms());
        }
        return $synonyms;
    }

    private function saveColor(Color $color)
    {
        $this->commandBus->dispatch(new UpdateColorCommand($color, []));
    }

    private function assignParentToSynonyms(Color $parent, Collection $synonyms)
    {
        foreach ($synonyms as $synonym) {
            /**
             * @var Color $synonym
             */
            $synonym->assignBaseColor($parent);
            $this->saveColor($synonym);
        }
    }
}
