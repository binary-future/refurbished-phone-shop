<?php

namespace App\Application\Services\Merge\Color\Merger;

use App\Application\Services\Bus\CommandBus;
use App\Domain\Phone\Phone\Color;
use App\Domain\Phone\Phone\Command\UpdateColorCommand;

class Unmerger
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Unmerger constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function unmerge(Color $color): Color
    {
        $color->refreshBaseColor();
        $synonyms = $color->getSynonyms();
        foreach ($synonyms as $synonym) {
            /**
             * @var Color $synonym
             */
            $synonym->refreshBaseColor();
        }

        return $this->saveColor($color);
    }

    private function saveColor(Color $color): Color
    {
        $this->commandBus->dispatch(new UpdateColorCommand($color, []));

        return $color;
    }
}
