<?php


namespace App\Application\Services\Merge\Model\Observer;

use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Repository\Filters;

final class FilterObserver
{
    /**
     * @var Filters
     */
    private $filters;

    /**
     * FilterObserver constructor.
     * @param Filters $filter
     */
    public function __construct(Filters $filter)
    {
        $this->filters = $filter;
    }

    /**
     * @param Filter $filter
     */
    public function updated(Filter $filter)
    {
        if ($filter->hasChildren() && $filter->getOriginal('name') !== $filter->getName()) {
            $children = $filter->getChildren();
            foreach ($children as $child) {
                $child->setName($child->getFilter());
                $this->filters->update($child, [Filter::FIELD_NAME => $child->getName()]);
            }
        };
    }

    /**
     * @param Filter $filter
     */
    public function deleted(Filter $filter)
    {
        if ($filter->hasChildren()) {
            $children = $filter->getChildren();
            foreach ($children as $child) {
                $this->filters->delete($child);
            }
        }
    }
}
