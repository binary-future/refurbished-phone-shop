<?php


namespace App\Application\Services\Merge\Model\Merger;


use App\Application\Services\Bus\CommandBus;
use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Merge\Model\DTO\ParentModelId;
use App\Application\Services\Merge\Model\DTO\SynonymsIds;
use App\Application\Services\Merge\Model\MergeGroup;
use App\Domain\Phone\Model\Command\UpdateModelCommand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Phone\Model\Query\ByIdQuery;
use App\Domain\Phone\Model\Query\ByIdsQuery;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

final class Merger
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Merger constructor.
     * @param QueryBus $queryBus
     * @param CommandBus $commandBus
     */
    public function __construct(QueryBus $queryBus, CommandBus $commandBus)
    {
        $this->queryBus = $queryBus;
        $this->commandBus = $commandBus;
    }

    public function merge(MergeGroup $group)
    {
        $parent = $this->getParent($group->getParentId());
        $synonyms = $this->getSynonyms($group->getSynonymIds());
        if ($parent->isSynonym()) {
            $parent->refreshBaseModel();
            $this->saveModel($parent);
        }
        $this->assignParentToSynonyms($parent, $synonyms);
    }

    private function getParent(ParentModelId $parentId): ?PhoneModel
    {
        /**
         * @var PhoneModel $model
         */
        $model = $this->queryBus->dispatch(new ByIdQuery($parentId->getParentId()))->first();
        if (! $model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    private function getSynonyms(SynonymsIds $synonymsIds): Collection
    {
        /**
         * @var Collection $models
         */
        $models = $this->queryBus->dispatch(new ByIdsQuery($synonymsIds->getIds(), ['synonyms', 'phones']));

        return $this->combineWithSynonyms($models);
    }

    private function combineWithSynonyms(Collection $models): Collection
    {
        if ($models->isEmpty()) {
            return $models;
        }

        $synonyms = collect();
        foreach ($models as $model) {
            /**
             * @var PhoneModel $model
             */
            $synonyms->push($model);
            $synonyms = $synonyms->merge($model->getSynonyms());
        }
        return $synonyms;
    }

    private function saveModel(PhoneModel $model)
    {
        $this->commandBus->dispatch(new UpdateModelCommand($model, []));
    }

    private function assignParentToSynonyms(PhoneModel $parent, Collection $synonyms)
    {
        foreach ($synonyms as $synonym) {
            /**
             * @var PhoneModel $synonym
             */
            $synonym->assignBaseModel($parent);
            $this->saveModel($synonym);
        }
    }
}
