<?php

namespace App\Application\Services\Merge\Model\Merger;

use App\Application\Services\Bus\CommandBus;
use App\Domain\Phone\Model\Command\UpdateModelCommand;
use App\Domain\Phone\Model\PhoneModel;

class Unmerger
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * Unmerger constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function unmerge(PhoneModel $model): PhoneModel
    {
        $model->refreshBaseModel();
        $synonyms = $model->getSynonyms();
        foreach ($synonyms as $synonym) {
            /**
             * @var PhoneModel $synonym
             */
            $synonym->refreshBaseModel();
        }

        return $this->saveModel($model);
    }

    private function saveModel(PhoneModel $model): PhoneModel
    {
        $this->commandBus->dispatch(new UpdateModelCommand($model, []));

        return $model;
    }
}
