<?php


namespace App\Application\Services\Merge\Model\DTO;


final class ParentModelId
{
    /**
     * @var int
     */
    private $parentId;

    /**
     * ParentModelId constructor.
     * @param int $parentId
     */
    public function __construct(int $parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getParentId(): int
    {
        return $this->parentId;
    }
}
