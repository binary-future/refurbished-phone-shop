<?php


namespace App\Application\Services\Merge\Model\DTO;


final class SynonymsIds
{
    /**
     * @var array
     */
    private $ids;

    /**
     * SynonymsIds constructor.
     * @param array $ids
     */
    public function __construct(array $ids)
    {
        if (! $this->isValid($ids)) {
            throw new \InvalidArgumentException();
        }
        $this->ids = $ids;
    }

    private function isValid(array $ids)
    {
        foreach ($ids as $id) {
            if (! is_numeric($id)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return array
     */
    public function getIds(): array
    {
        return $this->ids;
    }
}
