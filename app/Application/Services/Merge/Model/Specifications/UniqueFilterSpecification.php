<?php


namespace App\Application\Services\Merge\Model\Specifications;


use App\Application\Services\Merge\Model\Filter;
use App\Application\Services\Merge\Model\Query\ByBrandIdAndFilterAndParentIdQuery;
use App\Application\Services\Merge\Model\Repository\Filters;
use App\Utils\Specification\Contracts\Specification;

final class UniqueFilterSpecification implements Specification
{
    /**
     * @var Filters
     */
    private $filters;

    /**
     * UniqueFilterSpecification constructor.
     * @param Filters $filters
     */
    public function __construct(Filters $filters)
    {
        $this->filters = $filters;
    }

    public function isSatisfy($value, $params = null): bool
    {
        try {
            return $this->proceed($value);
        } catch (\Throwable $exception) {
            return false;
        }
    }

    private function proceed(array $params): bool
    {
        $filter = $this->getFilter($params);
        $filterId = $params['filter_id'] ?? null;
        if (! $filter) {
            return true;
        }

        return $filterId && $filter->getKey() === $filterId;
    }

    private function getFilter(array $params): ?Filter
    {
        /**
         * @var Filter $filter
         */
        $filter = $this->filters->findBy(new ByBrandIdAndFilterAndParentIdQuery(
            $params['brand_id'],
            $params['name'],
            $params['parent_id'] ?? null
        ))->first();

        return $filter;
    }
}
