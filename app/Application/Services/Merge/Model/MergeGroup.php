<?php

namespace App\Application\Services\Merge\Model;

use App\Application\Services\Merge\Model\DTO\ParentModelId;
use App\Application\Services\Merge\Model\DTO\SynonymsIds;

final class MergeGroup
{
    /**
     * @var ParentModelId
     */
    private $parentId;

    /**
     * @var SynonymsIds
     */
    private $synonymIds;

    /**
     * MergeGroup constructor.
     * @param ParentModelId $parentId
     * @param SynonymsIds $synonymIds
     */
    public function __construct(ParentModelId $parentId, SynonymsIds $synonymIds)
    {
        $this->parentId = $parentId;
        $this->synonymIds = $synonymIds;
    }

    /**
     * @return ParentModelId
     */
    public function getParentId(): ParentModelId
    {
        return $this->parentId;
    }

    /**
     * @return SynonymsIds
     */
    public function getSynonymIds(): SynonymsIds
    {
        return $this->synonymIds;
    }
}
