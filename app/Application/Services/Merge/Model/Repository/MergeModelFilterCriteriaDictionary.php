<?php


namespace App\Application\Services\Merge\Model\Repository;


use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

interface MergeModelFilterCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_BRAND_ID = 'by-brand-id';
    public const CRITERIA_BY_PARENT = 'by-filter';
    public const CRITERIA_BY_PARENT_ID = 'by-filter-id';
    public const CRITERIA_BY_FILTER = 'by-filter-value';
}
