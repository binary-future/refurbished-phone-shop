<?php


namespace App\Application\Services\Merge\Model\Query;


use App\Application\Services\Merge\Model\Repository\MergeModelFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class ByBrandIdAndFilterAndParentIdQuery
 * @package App\Application\Services\Merge\Model\Query
 */
final class ByBrandIdAndFilterAndParentIdQuery implements Query, MergeModelFilterCriteriaDictionary
{
    /**
     * @var int
     */
    private $brandId;

    /**
     * @var string
     */
    private $filter;

    /**
     * @var int|null
     */
    private $parentId;

    /**
     * ByBrandIdAndFilterNameAndParentId constructor.
     * @param int $brandId
     * @param string $filter
     * @param int|null $parentId
     */
    public function __construct(int $brandId, string $filter, ?int $parentId)
    {
        $this->brandId = $brandId;
        $this->filter = $filter;
        $this->parentId = $parentId;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_PARENT_ID => $this->parentId,
            self::CRITERIA_BY_FILTER => $this->filter,
            self::CRITERIA_BY_BRAND_ID => $this->brandId
        ];
    }
}
