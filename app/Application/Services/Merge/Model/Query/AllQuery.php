<?php

namespace App\Application\Services\Merge\Model\Query;

use App\Application\Services\Merge\Model\Repository\MergeModelFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class AllQuery
 * @package App\Application\Services\Merge\Model\Query
 */
final class AllQuery implements Query, MergeModelFilterCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations = [];

    /**
     * AllQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
         return [
             self::ALL => [],
             self::WITH => $this->relations
         ];
    }
}
