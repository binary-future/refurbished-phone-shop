<?php


namespace App\Application\Services\Merge\Model\Query;


use App\Application\Services\Merge\Model\Repository\MergeModelFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

final class AllParentQuery implements Query, MergeModelFilterCriteriaDictionary
{
    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_PARENT_ID => null,
        ];
    }
}
