<?php


namespace App\Application\Services\Merge\Model\Query;


use App\Application\Services\Merge\Model\Repository\MergeModelFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class ByIdQuery
 * @package App\Application\Services\Merge\Model\Query
 */
final class ByIdQuery implements Query, MergeModelFilterCriteriaDictionary
{
    /**
     * @var int
     */
    private $filterId;

    /**
     * ByIdQuery constructor.
     * @param int $filterId
     */
    public function __construct(int $filterId)
    {
        $this->filterId = $filterId;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ID => $this->filterId
        ];
    }
}

