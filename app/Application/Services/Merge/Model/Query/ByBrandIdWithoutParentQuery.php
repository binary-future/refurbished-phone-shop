<?php


namespace App\Application\Services\Merge\Model\Query;


use App\Application\Services\Merge\Model\Repository\MergeModelFilterCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

final class ByBrandIdWithoutParentQuery implements Query, MergeModelFilterCriteriaDictionary
{
    /**
     * @var int
     */
    private $brandId;

    /**
     * @var array
     */
    private $relations;

    /**
     * ByBrandIdQuery constructor.
     * @param int $brandId
     * @param array $relations
     */
    public function __construct(int $brandId, $relations = [])
    {
        $this->brandId = $brandId;
        $this->relations = $relations;
    }

    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_BRAND_ID => $this->brandId,
            self::WITH => $this->relations,
            self::CRITERIA_BY_PARENT => null,
        ];
    }
}
