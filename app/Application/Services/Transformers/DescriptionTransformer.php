<?php

namespace App\Application\Services\Transformers;

use App\Domain\Shared\Contracts\HasDescription;
use App\Domain\Shared\Description\Description;

/**
 * Class DescriptionTransformer
 * @package App\Domain\Shared\Description\Services
 */
class DescriptionTransformer
{
    /**
     * @param array $description
     * @param HasDescription $owner
     * @return Description|null
     */
    public function transform(array $description, HasDescription $owner): ?Description
    {
        if (empty($description)) {
            return null;
        }
        $descriptionEntity = new Description();
        $descriptionEntity->fill($description);
        $descriptionEntity->setOwner($owner);

        return $descriptionEntity;
    }
}
