<?php

namespace App\Application\Services\Blog;

use App\Utils\Adapters\Blog\Contracts\PostService as PostRepo;
use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Support\Collection;

class PostService
{
    private const POST_QUANTITY = 3;
    private const EXCEPT_TAGS = ['homepage'];

    /**
     * @var PostRepo
     */
    private $postRepo;

    /**
     * PostService constructor.
     * @param PostRepo $postRepo
     */
    public function __construct(PostRepo $postRepo)
    {
        $this->postRepo = $postRepo;
    }

    /**
     * @param PhoneModel $model
     * @return Collection
     */
    public function searchByModel(PhoneModel $model): Collection
    {
        $tags = $this->generateTags($model);
        $posts = $this->getPostsByTags($tags);

        return $this->isEnoughPosts($posts) ? $posts : $this->addLastPublishedPosts($posts);
    }

    private function generateTags(PhoneModel $model): array
    {
        return [
            strtolower($model->getName()),
            strtolower($model->getBrand()->getName()),
        ];
    }

    private function getPostsByTags(array $tags)
    {
        try {
            return $this->postRepo->byTags($tags, self::POST_QUANTITY);
        } catch (\Throwable $exception) {
            return collect();
        }
    }

    private function isEnoughPosts(Collection $posts): bool
    {
        return $posts->count() >= self::POST_QUANTITY;
    }

    private function addLastPublishedPosts(Collection $posts): Collection
    {
        try {
            $lastPosts = $this->postRepo->lastPostsExceptTags(
                self::EXCEPT_TAGS,
                self::POST_QUANTITY - $posts->count()
            );
            return $posts->merge($lastPosts);
        } catch (\Throwable $exception) {
            return $posts;
        }
    }
}
