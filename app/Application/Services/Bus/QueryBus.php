<?php

namespace App\Application\Services\Bus;

/**
 * Interface QueryBus
 * @package App\Application\Services\Bus
 */
interface QueryBus
{
    /**
     * @param object $query
     * @return object
     */
    public function dispatch(object $query): object;
}
