<?php

namespace App\Application\Services\Bus;

/**
 * Interface EventBus
 * @package App\Application\Services\Bus
 */
interface EventBus
{
    /**
     * @param object $event
     * @return mixed
     */
    public function dispatch(object $event);
}
