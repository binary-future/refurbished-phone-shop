<?php

namespace App\Application\Services\Bus;

/**
 * Interface CommandBus
 * @package App\Application\Services\Bus
 */
interface CommandBus
{
    /**
     * @param object $command
     * @return object|null
     */
    public function dispatch(object $command): ?object;
}
