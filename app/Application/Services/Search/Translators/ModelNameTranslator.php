<?php

namespace App\Application\Services\Search\Translators;

use App\Domain\Common\Contracts\Translators\Translator;

final class ModelNameTranslator implements Translator
{
    private $filters = [
        0 => [
            'needle' => ['ihone', 'ipone', 'iphne', 'iphoe', 'pihone', 'ihpone', 'ipohne', 'iphnoe', 'iphoen'],
            'replace' => 'iphone',
        ],
        1 => [
            'needle' => ['aple', 'appel', 'apel'],
            'replace' => 'apple',
        ],
        2 => [
            'needle' => ['smsung', 'samasung', 'samsugn'],
            'replace' => 'samsung'
        ],
        3 => [
            'needle' => ['glaxy', 'galxy', 'gaxaly'],
            'replace' => 'galaxy'
        ],
    ];

    public function translate(array $data, array $options = []): array
    {
        $name = $data['name'] ?? null;
        if (! $name) {
            return $data;
        }

        $data['name'] = $this->prepareName($name);

        return $data;
    }

    private function prepareName(string $name): string
    {
        foreach ($this->filters as $filter) {
            $name = str_ireplace($filter['needle'], $filter['replace'], $name);
        }

        return $name;
    }
}
