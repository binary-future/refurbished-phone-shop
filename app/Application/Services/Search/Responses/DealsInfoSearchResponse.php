<?php


namespace App\Application\Services\Search\Responses;


use App\Application\Services\Search\DTO\PhonesGroupDealsInfo;
use App\Domain\Deals\Deal\Deal;

final class DealsInfoSearchResponse
{
    /**
     * @var Deal|null
     */
    private $bestDeal;

    /**
     * @var Deal|null
     */
    private $cheapestTotalCostDeal;

    /**
     * @var Deal|null
     */
    private $highestTotalCostDeal;

    /**
     * @var PhonesGroupDealsInfo[]
     */
    private $phoneDealsInfo;

    /**
     * DealsInfoSearchResponse constructor.
     * @param Deal|null $bestDeal
     * @param Deal|null $cheapestTotalCostDeal
     * @param Deal|null $highestTotalCostDeal
     * @param PhonesGroupDealsInfo[] $phoneDealsInfo
     */
    public function __construct(
        Deal $bestDeal = null,
        Deal $cheapestTotalCostDeal = null,
        Deal $highestTotalCostDeal = null,
        array $phoneDealsInfo = []
    ) {
        $this->bestDeal = $bestDeal;
        $this->cheapestTotalCostDeal = $cheapestTotalCostDeal;
        $this->highestTotalCostDeal = $highestTotalCostDeal;
        $this->phoneDealsInfo = $phoneDealsInfo;
    }

    /**
     * @return Deal|null
     */
    public function getBestDeal(): ?Deal
    {
        return $this->bestDeal;
    }

    /**
     * @return Deal|null
     */
    public function getCheapestTotalCostDeal(): ?Deal
    {
        return $this->cheapestTotalCostDeal;
    }

    /**
     * @return Deal|null
     */
    public function getHighestTotalCostDeal(): ?Deal
    {
        return $this->highestTotalCostDeal;
    }

    /**
     * @return PhonesGroupDealsInfo[]
     */
    public function getPhoneDealsInfo(): array
    {
        return $this->phoneDealsInfo;
    }
}
