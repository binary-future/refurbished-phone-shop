<?php

namespace App\Application\Services\Search\DTO;

use App\Domain\Deals\Deal\Deal;
use Illuminate\Support\Collection;

final class PhonesGroupDealsInfo
{
    /**
     * @var Collection
     */
    private $phones;

    /**
     * @var Deal|null
     */
    private $bestMonthCostDeal;

    /**
     * @var Deal|null
     */
    private $bestUpfrontCostDeal;

    /**
     * PhonesGroupDealsInfo constructor.
     * @param Collection $phones
     * @param Deal|null $bestMonthCostDeal
     * @param Deal|null $bestUpfrontCostDeal
     */
    public function __construct(Collection $phones, ?Deal $bestMonthCostDeal, ?Deal $bestUpfrontCostDeal)
    {
        $this->phones = $phones;
        $this->bestMonthCostDeal = $bestMonthCostDeal;
        $this->bestUpfrontCostDeal = $bestUpfrontCostDeal;
    }

    /**
     * @return Collection
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /**
     * @return Deal|null
     */
    public function getBestMonthCostDeal(): ?Deal
    {
        return $this->bestMonthCostDeal;
    }

    /**
     * @return Deal|null
     */
    public function getBestUpfrontCostDeal(): ?Deal
    {
        return $this->bestUpfrontCostDeal;
    }
}
