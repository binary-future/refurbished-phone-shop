<?php

namespace App\Application\Services\Search;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\DTO\PhonesGroupDealsInfo;
use App\Application\Services\Search\Responses\DealsInfoSearchResponse;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\CheapestMonthlyCostByPhonesQuery;
use App\Domain\Deals\Deal\Query\CheapestTotalCostByPhonesQuery;
use App\Domain\Deals\Deal\Query\CheapestUpfrontCostByPhoneQuery;
use App\Domain\Deals\Deal\Query\HighestTotalCostByPhonesQuery;
use Illuminate\Support\Collection;

/**
 * Class DealsInfoSearcher
 * @package App\Application\Services\Searchers
 */
class DealsInfoSearcher
{
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * DealsInfoSearcher constructor.
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @param Collection $phones
     * @return DealsInfoSearchResponse
     */
    public function searchDealsInfo(Collection $phones): DealsInfoSearchResponse
    {
        if ($phones->isEmpty()) {
            return new DealsInfoSearchResponse();
        }

        return new DealsInfoSearchResponse(
            $this->getCheapestMonthCostDeal($phones),
            $this->getCheapestTotalCostDeal($phones),
            $this->getHighestTotalCostDeal($phones),
            $this->getDealsInfo($phones)
        );
    }

    /**
     * @param Collection $phones
     * @return array
     */
    private function getDealsInfo(Collection $phones)
    {
        $groupedPhones = $phones->groupBy('color_id');
        $dealsGroups = [];
        foreach ($groupedPhones as $group) {
            $dealsGroups[] = new PhonesGroupDealsInfo(
                $group,
                $this->getCheapestMonthCostDeal($group),
                $this->getCheapestUpfrontCostDeal($group)
            );
        }

        return $dealsGroups;
    }

    /**
     * @param Collection $phones
     * @return Deal|null
     */
    private function getCheapestMonthCostDeal(Collection $phones): ?Deal
    {
        /**
         * @var Collection $deals
         */
        $deals = $this->queryBus->dispatch(new CheapestMonthlyCostByPhonesQuery($phones));

        return $deals->first();
    }

    /**
     * @param Collection $phones
     * @return Deal|null
     */
    private function getCheapestTotalCostDeal(Collection $phones): ?Deal
    {
        /**
         * @var Collection $deals
         */
        $deals = $this->queryBus->dispatch(new CheapestTotalCostByPhonesQuery($phones));

        return $deals->first();
    }

    /**
     * @param Collection $phones
     * @return Deal|null
     */
    private function getCheapestUpfrontCostDeal(Collection $phones): ?Deal
    {
        /**
         * @var Collection $deals
         */
        $deals = $this->queryBus->dispatch(new CheapestUpfrontCostByPhoneQuery($phones, 1, 0));

        return $deals->first();
    }

    /**
     * @param Collection $phones
     * @return Deal|null
     */
    private function getHighestTotalCostDeal(Collection $phones): ?Deal
    {
        /**
         * @var Collection $deals
         */
        $deals = $this->queryBus->dispatch(new HighestTotalCostByPhonesQuery($phones, 1));

        return $deals->first();
    }
}
