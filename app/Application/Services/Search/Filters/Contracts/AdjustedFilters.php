<?php

namespace App\Application\Services\Search\Filters\Contracts;

interface AdjustedFilters
{
    public function toArray(): array;
}
