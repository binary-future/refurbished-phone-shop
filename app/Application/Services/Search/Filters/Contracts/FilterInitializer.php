<?php

namespace App\Application\Services\Search\Filters\Contracts;

use Illuminate\Support\Collection;

interface FilterInitializer
{
    public function init(
        Collection $networks,
        PrimaryDealsAdjustedFilters $adjustedFilters
    ): PrimaryDealsAdjustedFilters;
}
