<?php

namespace App\Application\Services\Search\Filters\Contracts;

interface PrimaryDealsAdjustedFilters extends AdjustedFilters
{
    public const FILTER_MODEL_SLUG = 'models';
    public const FILTER_NETWORKS = 'networks';
    public const FILTER_CONDITIONS = 'conditions';
    public const FILTER_TOTAL_COST = 'total_cost';
    public const FILTER_TOTAL_COST_RANGE = 'total_cost_range';
    public const FILTER_MONTHLY_COST = 'monthly_cost';
    public const FILTER_MONTHLY_COST_RANGE = 'monthly_cost_range';
    public const FILTER_SORT = 'sort';
    public const FILTER_PAGE = 'page';

    public function getModelSlug(): ?string;

    public function setModelSlug(?string $modelSlug): self;

    public function getNetworks(): array;

    public function setNetworks(array $networks): self;

    public function getConditions(): array;

    public function setConditions(array $conditions): self;

    public function setTotalCostRange(array $totalCostRange): self;

    public function getTotalCostRange(): array;

    public function setMonthlyCostRange(array $monthlyCostRange): self;

    public function getMonthlyCostRange(): array;

    public function setSort(?string $sorting): self;

    public function getSort(): ?string;

    public function getPage(): int;

    public function setPage(?int $page): self;
}
