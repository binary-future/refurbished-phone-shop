<?php

namespace App\Application\Services\Search\Filters\Contracts;

use App\Application\Services\Search\Filters\DealIndex;
use App\Domain\Phone\Brand\Brand;
use Illuminate\Support\Collection;

interface FiltersSearcher
{
    /**
     * @param Brand $brand
     * @param Collection $products
     * @param AdjustedFilters $adjustedFilters
     * @return DealIndex
     */
    public function generate(Brand $brand, Collection $products, AdjustedFilters $adjustedFilters): DealIndex;
}
