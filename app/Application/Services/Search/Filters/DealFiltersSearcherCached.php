<?php

namespace App\Application\Services\Search\Filters;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher;
use App\Application\Services\Search\Filters\Exceptions\DealFiltersSearcherException;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\DealColumnByQuery;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Phone\Brand\Brand;
use App\Utils\Adapters\Cache\Contracts\Cache;
use App\Utils\Adapters\Cache\Exceptions\ConnectionException as CacheConnectionException;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Support\Collection;

final class DealFiltersSearcherCached implements FiltersSearcher
{
    private const CACHE_PREFIX_DEAL_FILTERS = 'deal:filters:';

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var FiltersSearcher
     */
    private $degradatedFilters;
    /**
     * @var Cache
     */
    private $cache;
    /**
     * @var ChannelLogger
     */
    private $channelLogger;
    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * DealFiltersSearcherCached constructor.
     * @param Serializer $serializer
     * @param Cache $cache
     * @param ChannelLogger $channelLogger
     * @param QueryBus $queryBus
     * @param FiltersSearcher $degradatedFilters
     */
    public function __construct(
        Serializer $serializer,
        Cache $cache,
        ChannelLogger $channelLogger,
        QueryBus $queryBus,
        FiltersSearcher $degradatedFilters
    ) {
        $this->serializer = $serializer;
        $this->degradatedFilters = $degradatedFilters;
        $this->cache = $cache;
        $this->channelLogger = $channelLogger;
        $this->queryBus = $queryBus;
    }

    /**
     * @param Brand $brand
     * @param Collection $products
     * @param AdjustedFilters $adjustedFilters
     * @return DealIndex
     */
    public function generate(Brand $brand, Collection $products, AdjustedFilters $adjustedFilters): DealIndex
    {
        $dealIndex = null;

        if (! $products->isEmpty()) {
            try {
                $dealIndex = $this->proceed($brand, $products, $adjustedFilters);
            } catch (CacheConnectionException | DealFiltersSearcherException $exception) {
                $this->log($exception);
                $dealIndex = $this->degradatedFilters->generate($brand, $products, $adjustedFilters);
            } catch (\Throwable $exception) {
                $this->log($exception);
                // generate default response
            }
        }

        return $dealIndex ?? $this->generateDefaultResponse($products);
    }

    private function log($message)
    {
        $this->channelLogger->channel(ChannelLogger::CHANNEL_STACK)
            ->error($message);
    }

    /**
     * @param Collection $products
     * @return DealIndex
     */
    private function generateDefaultResponse(Collection $products): DealIndex
    {
        return new DealIndex($products, collect(), collect(), collect(), collect());
    }

    /**
     * @param Brand $brand
     * @param Collection $products
     * @param AdjustedFilters $adjustedFilters
     * @return DealIndex
     */
    private function proceed(Brand $brand, Collection $products, AdjustedFilters $adjustedFilters): DealIndex
    {
        return new DealIndex(
            $this->getFilteredProducts($products, $adjustedFilters),
            ...$this->getMainFilterColumns($brand),
        );
    }

    /**
     * @param Collection $products
     * @param AdjustedFilters $adjustedFilters
     * @return FiltersSearchQuery
     */
    private function generateQuery(Collection $products, AdjustedFilters $adjustedFilters): FiltersSearchQuery
    {
        /**
         * @var FiltersSearchQuery $query
         */
        $query = $this->serializer->fromArray(FiltersSearchQuery::class, $adjustedFilters->toArray());
        $query->setProducts($products);
        $query->disableSort(true);

        return $query;
    }

    private function getMainFilterColumns(Brand $brand): array
    {
        $cacheKey = self::CACHE_PREFIX_DEAL_FILTERS . $brand->getSlug();
        $filters = $this->readFilters($cacheKey);

        if ($filters->isEmpty()) {
            throw DealFiltersSearcherException::emptyCache($cacheKey);
        }

        $fields = [
            Contract::FIELD_NETWORK_ID,
            Contract::FIELD_CONDITION,
            Deal::FIELD_MONTHLY_COST,
            Deal::FIELD_TOTAL_COST,
        ];

        $collections = [];

        foreach ($fields as $field) {
            $collections[] = collect($filters->get($field))->unique();
        }

        return $collections;
    }

    private function readFilters(string $key): Collection
    {
        return collect($this->cache->get($key));
    }

    private function getFilteredProducts(Collection $products, AdjustedFilters $adjustedFilters): Collection
    {
        if (! $adjustedFilters->toArray()) {
            return $products;
        }

        $query = $this->generateQuery($products, $adjustedFilters);

        $result = $this->dealsValues($query, Deal::FIELD_PRODUCT_ID)->flip();

        return $products->filter(static function (Product $product) use ($result) {
            return $result->has($product->getKey());
        });
    }

    /**
     * @param FiltersSearchQuery $query
     * @param string $value
     * @return Collection
     */
    private function dealsValues(FiltersSearchQuery $query, string $value): Collection
    {
        /**
         * @var Collection $filters
         */
        $filters = $this->queryBus->dispatch(new DealColumnByQuery($query, $value));

        return $filters;
    }
}
