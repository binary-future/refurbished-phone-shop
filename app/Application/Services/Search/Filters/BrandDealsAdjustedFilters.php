<?php

namespace App\Application\Services\Search\Filters;

use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use App\Application\Services\Search\Filters\Contracts\PrimaryDealsAdjustedFilters;

final class BrandDealsAdjustedFilters implements AdjustedFilters, PrimaryDealsAdjustedFilters
{
    /**
     * @var array|null
     */
    private $networks;
    /**
     * @var array|null
     */
    private $totalCostRange;
    /**
     * @var array|null
     */
    private $monthlyCostRange;
    /**
     * @var string|null
     */
    private $sort;
    /**
     * @var int|null
     */
    private $page;
    /**
     * @var string|null
     */
    private $modelSlug;
    /**
     * @var array
     */
    private $conditions;

    public function getModelSlug(): ?string
    {
        return $this->modelSlug;
    }

    public function setModelSlug(?string $modelSlug): PrimaryDealsAdjustedFilters
    {
        $this->modelSlug = $modelSlug;
        return $this;
    }

    public function getNetworks(): array
    {
        return $this->networks ?? [];
    }

    /**
     * @param array $networks
     * @return $this|PrimaryDealsAdjustedFilters
     */
    public function setNetworks(array $networks): PrimaryDealsAdjustedFilters
    {
        $this->networks = $networks;
        return $this;
    }

    /**
     * @param array $totalCostRange
     * @return $this|PrimaryDealsAdjustedFilters
     */
    public function setTotalCostRange(array $totalCostRange): PrimaryDealsAdjustedFilters
    {
        $this->totalCostRange = $totalCostRange ? [min($totalCostRange), max($totalCostRange)] : [];
        return $this;
    }

    public function getTotalCostRange(): array
    {
        return $this->totalCostRange ?? [];
    }

    /**
     * @param array $monthlyCostRange
     * @return $this|PrimaryDealsAdjustedFilters
     */
    public function setMonthlyCostRange(array $monthlyCostRange): PrimaryDealsAdjustedFilters
    {
        $this->monthlyCostRange = $monthlyCostRange ? [min($monthlyCostRange), max($monthlyCostRange)] : [];
        return $this;
    }

    public function getMonthlyCostRange(): array
    {
        return $this->monthlyCostRange ?? [];
    }

    /**
     * @param string|null $sorting
     * @return $this|PrimaryDealsAdjustedFilters
     */
    public function setSort(?string $sorting): PrimaryDealsAdjustedFilters
    {
        $this->sort = $sorting;

        return $this;
    }

    public function getSort(): ?string
    {
        return $this->sort;
    }

    public function getPage(): int
    {
        return $this->page ?? 1;
    }

    /**
     * @param int|null $page
     * @return $this|PrimaryDealsAdjustedFilters
     */
    public function setPage(?int $page): PrimaryDealsAdjustedFilters
    {
        $this->page = $page;
        return $this;
    }

    public function getConditions(): array
    {
        return $this->conditions ?? [];
    }

    public function setConditions(array $conditions): PrimaryDealsAdjustedFilters
    {
        $this->conditions = $conditions;
        return $this;
    }

    public function toArray(): array
    {
        $params = [
            self::FILTER_NETWORKS => $this->getNetworks(),
            self::FILTER_CONDITIONS => $this->getConditions(),
            self::FILTER_MODEL_SLUG => $this->getModelSlug(),
            self::FILTER_TOTAL_COST_RANGE => $this->getTotalCostRange(),
            self::FILTER_MONTHLY_COST_RANGE => $this->getMonthlyCostRange(),
            self::FILTER_PAGE => $this->getPage(),
            self::FILTER_SORT => $this->getSort(),
        ];

        return array_filter($params, static function ($item) {
            return $item !== null && ! empty($item);
        });
    }
}
