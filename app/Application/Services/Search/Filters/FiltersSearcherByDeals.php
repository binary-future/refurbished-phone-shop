<?php

namespace App\Application\Services\Search\Filters;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use App\Application\Services\Search\Filters\Contracts\FiltersSearcher as FiltersContract;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Deal\Contracts\Product;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\ContractColumnByQuery;
use App\Domain\Deals\Deal\Query\DealColumnByQuery;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Deals\Deal\Query\NetworksColumnByQuery;
use App\Domain\Phone\Brand\Brand;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Support\Collection;

/**
 * Class Filters
 * @package App\Application\Services\Search\Filters
 */
final class FiltersSearcherByDeals implements FiltersContract
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * DealsSearcher constructor.
     * @param Serializer $serializer
     * @param QueryBus $queryBus
     */
    public function __construct(Serializer $serializer, QueryBus $queryBus)
    {
        $this->serializer = $serializer;
        $this->queryBus = $queryBus;
    }

    /**
     * @param Brand $brand
     * @param Collection $products
     * @param AdjustedFilters $adjustedFilters
     * @return DealIndex
     */
    public function generate(Brand $brand, Collection $products, AdjustedFilters $adjustedFilters): DealIndex
    {
        if ($products->isEmpty()) {
            return $this->generateDefaultResponse($products);
        }
        try {
            return $this->proceed($products, $adjustedFilters);
        } catch (\Throwable $exception) {
            return $this->generateDefaultResponse($products);
        }
    }

    /**
     * @param Collection $products
     * @return DealIndex
     */
    private function generateDefaultResponse(Collection $products): DealIndex
    {
        return new DealIndex($products, collect(), collect(), collect(), collect());
    }

    private function proceed(Collection $products, AdjustedFilters $adjustedFilters): DealIndex
    {
        $query = $this->generateQuery($products, $adjustedFilters);

        return new DealIndex(
            $this->generateActiveProducts(clone $query, $products),
            $this->contractValues(clone $query, Contract::FIELD_NETWORK_ID),
            $this->contractValues(clone $query, Contract::FIELD_CONDITION),
            $this->dealsValues(clone $query, Deal::FIELD_MONTHLY_COST),
            $this->dealsValues(clone $query, Deal::FIELD_TOTAL_COST),
        );
    }

    private function generateQuery(Collection $products, AdjustedFilters $adjustedFilters): FiltersSearchQuery
    {
        /**
         * @var FiltersSearchQuery $query
         */
        $query = $this->serializer->fromArray(FiltersSearchQuery::class, $adjustedFilters->toArray());
        $query->setProducts($products);

        return $query;
    }

    private function generateActiveProducts(
        FiltersSearchQuery $query,
        Collection $products
    ): Collection {
        $result = $this->dealsValues(clone $query, Deal::FIELD_PRODUCT_ID);

        return $products->filter(static function (Product $product) use ($result) {
            return in_array($product->getKey(), $result->all(), true);
        });
    }

    private function dealsValues(FiltersSearchQuery $query, string $value): Collection
    {
        /**
         * @var Collection $deals
         */
        $deals = $this->queryBus->dispatch(new DealColumnByQuery($query, $value));

        return $deals;
    }

    private function contractValues(FiltersSearchQuery $query, string $value): Collection
    {
        /**
         * @var Collection $contractValues
         */
        if (! $query->getConditions()) {
            $contractValues = $this->queryBus->dispatch(new ContractColumnByQuery($query, $value));
        } else {
            $contractValues = $this->dealsValues($query, $value);
        }

        return $contractValues;
    }
}
