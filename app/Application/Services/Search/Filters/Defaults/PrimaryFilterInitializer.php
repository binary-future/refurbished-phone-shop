<?php

namespace App\Application\Services\Search\Filters\Defaults;

use App\Application\Services\Search\Filters\Contracts\PrimaryDealsAdjustedFilters;
use App\Application\Services\Search\Filters\Contracts\FilterInitializer
    as FilterInitializerContract;
use App\Domain\Deals\Contract\Network;
use App\Utils\Adapters\Log\Contracts\ChannelLogger;
use Illuminate\Support\Collection;

class PrimaryFilterInitializer implements FilterInitializerContract
{
    private const DEFAULT_NETWORKS_SLUG = [''];

    /**
     * @var ChannelLogger
     */
    private $channelLogger;

    /**
     * DefaultFilterValuesInitializer constructor.
     * @param ChannelLogger $channelLogger
     */
    public function __construct(ChannelLogger $channelLogger)
    {
        $this->channelLogger = $channelLogger;
    }

    public function init(
        Collection $networks,
        PrimaryDealsAdjustedFilters $adjustedFilters
    ): PrimaryDealsAdjustedFilters {
        $adjustedNetworks = $this->getNetworks(
            $networks,
            $adjustedFilters->getNetworks()
        );

        $adjustedFilters->setNetworks($adjustedNetworks);

        return $adjustedFilters;
    }

    private function getNetworks(Collection $networks, array $adjustedNetworks): array
    {
        if (! empty($adjustedNetworks)) {
            return $adjustedNetworks;
        }

        $defaultNetworks = $networks
            ->whereIn(Network::FIELD_SLUG, self::DEFAULT_NETWORKS_SLUG);

        if ($defaultNetworks->count() !== count(self::DEFAULT_NETWORKS_SLUG)) {
            $this->logError(
                'Default networks not found: ' . implode(', ', self::DEFAULT_NETWORKS_SLUG)
            );
        }

        return $defaultNetworks->pluck(Network::FIELD_ID)->all();
    }

    private function logError(string $message): void
    {
        $this->channelLogger->error($message);
    }
}
