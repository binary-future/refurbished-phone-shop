<?php

namespace App\Application\Services\Search\Filters;

use Illuminate\Support\Collection;

final class DealIndex
{
    /**
     * @var Collection
     */
    private $products;

    /**
     * @var Collection
     */
    private $networks;

    /**
     * @var Collection
     */
    private $monthlyCosts;

    /**
     * @var Collection
     */
    private $totalCosts;
    /**
     * @var Collection
     */
    private $conditions;

    /**
     * DealIndex constructor.
     * @param Collection $products
     * @param Collection $networks
     * @param Collection $conditions
     * @param Collection $monthlyCosts
     * @param Collection $totalCosts
     */
    public function __construct(
        Collection $products,
        Collection $networks,
        Collection $conditions,
        Collection $monthlyCosts,
        Collection $totalCosts
    ) {
        $this->products = $products;
        $this->networks = $networks;
        $this->monthlyCosts = $monthlyCosts;
        $this->totalCosts = $totalCosts;
        $this->conditions = $conditions;
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    /**
     * @return Collection
     */
    public function getNetworks(): Collection
    {
        return $this->networks;
    }

    /**
     * @return Collection
     */
    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    /**
     * @return Collection
     */
    public function getMonthlyCosts(): Collection
    {
        return $this->monthlyCosts;
    }

    /**
     * @return Collection
     */
    public function getTotalCosts(): Collection
    {
        return $this->totalCosts;
    }
}
