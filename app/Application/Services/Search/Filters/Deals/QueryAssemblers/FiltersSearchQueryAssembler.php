<?php

namespace App\Application\Services\Search\Filters\Deals\QueryAssemblers;

use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Deals\Deal\Repository\DealsCriteriaDictionary;
use App\Utils\Serializer\Contracts\Serializer;

class FiltersSearchQueryAssembler implements DealsCriteriaDictionary
{
    private const FILTER_SORT = 'sort';

    // TODO: every default filter value should be moved to config or separated class-modifier
    private const DEFAULT_FILTERS = [
        self::FILTER_SORT => self::CRITERIA_SORT_BY_MONTHLY_COST,
    ];

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * DealsFilterSearchQueryAssembler constructor.
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    public function buildQuery(array $params): FiltersSearchQuery
    {
        return $this->generateQuery($this->prepareParams($params));
    }

    private function prepareParams(array $params): array
    {
        return $this->generateSorting($params);
    }

    private function generateSorting(array $params): array
    {
        $sort = $params[self::FILTER_SORT] ?? self::DEFAULT_FILTERS[self::FILTER_SORT];

        $sortCriteria = [
            self::CRITERIA_SORT_BY_MONTHLY_COST,
            self::CRITERIA_SORT_BY_UPFRONT_COST,
            self::CRITERIA_SORT_BY_TOTAL_COST,
        ];

        $params[self::FILTER_SORT] = in_array($sort, $sortCriteria, true)
            ? $sort
            : self::CRITERIA_SORT_BY_MONTHLY_COST;

        return $params;
    }

    private function generateQuery(array $params): FiltersSearchQuery
    {
        /**
         * @var FiltersSearchQuery $query
         */
        $query = $this->serializer->fromArray(FiltersSearchQuery::class, $params);
        $query->joinContracts(true);

        return $query;
    }
}
