<?php


namespace App\Application\Services\Search\Filters\Exceptions;


class DealFiltersSearcherException extends \RuntimeException
{
    public static function emptyCache(string $cacheKey): DealFiltersSearcherException
    {
        return new self("Filter cache is empty. Key: $cacheKey");
    }
}
