<?php

namespace App\Application\Services\Search;

use App\Application\Services\Bus\QueryBus;
use App\Application\Services\Search\Contracts\DealsSearcher as ContractDealsSearcher;
use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Deals\Deal\Query\FiltersSearchQuery;
use App\Domain\Store\PaymentMethod\PaymentMethod;
use App\Domain\Store\Store;
use App\Utils\Serializer\Contracts\Serializer;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

class DealsSearcher implements ContractDealsSearcher
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var QueryBus
     */
    private $queryBus;

    /**
     * DealsSearcher constructor.
     * @param Serializer $serializer
     * @param QueryBus $queryBus
     */
    public function __construct(Serializer $serializer, QueryBus $queryBus)
    {
        $this->serializer = $serializer;
        $this->queryBus = $queryBus;
    }

    public function searchPaginated(
        AdjustedFilters $adjustedFilters,
        Collection $phones,
        int $perPage,
        int $page
    ): Paginator {

        if ($phones->isEmpty()) {
            return new \Illuminate\Pagination\Paginator(collect(), $perPage);
        }
        /**
         * @var FiltersSearchQuery $query
         * @var Paginator $deals
         */
        $query = $this->serializer->fromArray(FiltersSearchQuery::class, $adjustedFilters->toArray());
        $query->setProducts($phones);
        $query->setPerPage($perPage);
        $query->setRelations([
            Deal::RELATION_DESCRIPTION,
            Deal::RELATION_CONTRACT,
            implode('.', [Deal::RELATION_CONTRACT, Contract::RELATION_NETWORK]),
            implode('.', [Deal::RELATION_CONTRACT, Contract::RELATION_NETWORK, Network::RELATION_IMAGE]),
            Deal::RELATION_STORE,
            implode('.', [Deal::RELATION_STORE, Store::RELATION_IMAGE]),
            implode('.', [Deal::RELATION_STORE, Store::RELATION_PAYMENT_METHODS]),
            implode('.', [Deal::RELATION_STORE, Store::RELATION_PAYMENT_METHODS, PaymentMethod::RELATION_IMAGE]),
        ]);
        $deals = $this->queryBus->dispatch($query);

        return $deals;
    }
}
