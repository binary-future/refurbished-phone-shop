<?php

namespace App\Application\Services\Search\Contracts;

use App\Application\Services\Search\Filters\Contracts\AdjustedFilters;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Interface DealsSearcher
 * @package App\Application\Services\Search\Filters\Contracts
 */
interface DealsSearcher
{
    /**
     * @param AdjustedFilters $adjustedFilters
     * @param Collection $phones
     * @param int $perPage
     * @param int $page
     * @return Paginator
     */
    public function searchPaginated(
        AdjustedFilters $adjustedFilters,
        Collection $phones,
        int $perPage,
        int $page
    ): Paginator;
}
