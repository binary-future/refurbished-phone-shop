<?php

namespace App\Application\Services\Affiliation\Contracts;

use App\Domain\Shared\Contracts\HasLinkWithAffiliation;

/**
 * Interface Generator
 * @package App\Application\Services\Affiliation\Contracts
 */
interface Generator
{
    /**
     * @param HasLinkWithAffiliation $owner
     * @param string|null $referrer
     * @return string
     */
    public function generate(HasLinkWithAffiliation $owner, string $referrer = null): string;
}
