<?php


namespace App\Application\Services\Model\LatestModel\Query;


use App\Application\Services\Model\LatestModel\Repository\LatestModelCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class LatestModelByBrandIdQuery
 * @package App\Application\Services\Model\LatestModel\Query
 */
final class LatestModelByBrandIdQuery implements Query, LatestModelCriteriaDictionary
{
    /**
     * @var int
     */
    private $brandId;
    /**
     * @var array
     */
    private $relations;

    /**
     * LatestModelByModelIdQuery constructor.
     * @param int $brandId
     * @param array $relations
     */
    public function __construct(int $brandId, array $relations = [])
    {
        $this->brandId = $brandId;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::CRITERIA_BY_IS_LATEST => true,
            self::CRITERIA_BY_BRAND_ID => $this->brandId
        ];
    }
}
