<?php


namespace App\Application\Services\Model\LatestModel\Query;


use App\Application\Services\Model\LatestModel\LatestModel;
use App\Application\Services\Model\LatestModel\Repository\LatestModelCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class AllQuery
 * @package App\Application\Services\Model\LatestModel\Query
 */
final class AllQuery implements Query, LatestModelCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations;
    /**
     * @var int
     */
    private $limit;

    /**
     * AllQuery constructor.
     * @param array $relations
     * @param int|null $limit
     */
    public function __construct(array $relations = [], int $limit = 0)
    {
        $this->relations = $relations;
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations,
            self::LIMIT => $this->limit,
            self::CRITERIA_BY_IS_LATEST => true,
            self::ORDER_BY_DESC => LatestModel::UPDATED_AT,
        ];
    }
}
