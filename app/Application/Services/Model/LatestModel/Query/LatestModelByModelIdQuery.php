<?php


namespace App\Application\Services\Model\LatestModel\Query;


use App\Application\Services\Model\LatestModel\Repository\LatestModelCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class LatestModelByModelIdQuery
 * @package App\Application\Services\Model\LatestModel\Query
 */
final class LatestModelByModelIdQuery implements Query, LatestModelCriteriaDictionary
{
    /**
     * @var int
     */
    private $modelId;
    /**
     * @var array
     */
    private $relations;

    /**
     * LatestModelByModelIdQuery constructor.
     * @param int $modelId
     * @param array $relations
     */
    public function __construct(int $modelId, array $relations = [])
    {
        $this->modelId = $modelId;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::WITH => $this->relations,
            self::CRITERIA_BY_IS_LATEST => true,
            self::CRITERIA_BY_MODEL_ID => $this->modelId
        ];
    }
}
