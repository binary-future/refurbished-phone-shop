<?php

namespace App\Application\Services\Model\LatestModel;

use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use InvalidArgumentException;

/**
 * Class LatestModel
 * @package App\Application\Services\Model\LatestModel
 */
class LatestModel extends Model
{
    public const TABLE = 'phone_models';
    public const FIELD_IS_LATEST = 'is_latest';
    public const FIELD_MODEL_ID = 'model_id';

    public const RELATION_MODEL = 'model';

    public const LATEST = 1;
    public const NOT_LATEST = 0;

    protected $table = self::TABLE;

    public $timestamps = false;

    protected $fillable = [
        self::FIELD_MODEL_ID
    ];

    /**
     * LatestModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $attributes = $this->initPrimaryKey($attributes);

        parent::__construct($attributes);

        $this->setAttribute(self::FIELD_IS_LATEST, true);
    }

    /**
     * @return BelongsTo
     */
    public function model(): BelongsTo
    {
        return $this->belongsTo(PhoneModel::class, $this->getKeyName());
    }

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes)
    {
        $attributes = $this->initPrimaryKey($attributes);

        return parent::fill($attributes);
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->getRelationValue(self::RELATION_MODEL);
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        // set is_latest field false and don't remove instance
        // because LatestModel is not independence entry
        $this->setAttribute(self::FIELD_IS_LATEST, false);
        return $this->save();
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        if ($this->getKey() === null) {
            throw new InvalidArgumentException('Model id is required attribute');
        }

        return parent::save($options);
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function initPrimaryKey(array $attributes): array
    {
        // set 'model_id' field to primary key, because it's the same
        // created it for giving easy way for migrating on new table in future
        $modelId = $attributes[self::FIELD_MODEL_ID] ?? null;

        $this->setAttribute($this->getKeyName(), $modelId);
        unset($attributes[self::FIELD_MODEL_ID]);

        return $attributes;
    }
}
