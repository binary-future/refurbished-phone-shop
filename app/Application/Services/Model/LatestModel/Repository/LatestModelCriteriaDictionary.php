<?php


namespace App\Application\Services\Model\LatestModel\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

/**
 * Interface LatestModelCriteriaDictionary
 * @package App\Application\Services\Model\LatestModel\Repository
 */
interface LatestModelCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_MODEL_ID = 'by-model-id';
    public const CRITERIA_BY_IS_LATEST = 'by-is-latest';
    public const CRITERIA_BY_BRAND_ID = 'by-brand-id';
}
