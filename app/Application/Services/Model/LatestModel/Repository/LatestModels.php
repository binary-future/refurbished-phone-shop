<?php

namespace App\Application\Services\Model\LatestModel\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface LatestModels
 * @package App\Application\Services\Model\LatestModel\Repository
 */
interface LatestModels extends Repository
{

}
