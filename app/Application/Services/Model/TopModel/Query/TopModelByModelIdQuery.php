<?php


namespace App\Application\Services\Model\TopModel\Query;


use App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class TopModelByModelIdQuery
 * @package App\Application\Services\Model\TopModel\Query
 */
final class TopModelByModelIdQuery implements Query, TopModelCriteriaDictionary
{
    /**
     * @var int
     */
    private $modelId;

    /**
     * AllOrderedQuery constructor.
     * @param int $modelId
     */
    public function __construct(int $modelId)
    {
        $this->modelId = $modelId;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_MODEL_ID => $this->modelId
        ];
    }
}
