<?php


namespace App\Application\Services\Model\TopModel\Query;


use App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;
use App\Utils\Repository\Criteria\Common\BetweenValues;
use App\Utils\Repository\Criteria\Common\CompareValue;

/**
 * Class TopModelsFromOrderQuery
 * @package App\Application\Services\Model\TopModel\Query
 */
final class TopModelsInOrderDiapasonQuery implements Query, TopModelCriteriaDictionary
{
    /**
     * @var int
     */
    private $fromOrder;
    /**
     * @var int
     */
    private $toOrder;

    /**
     * AllOrderedQuery constructor.
     * @param int $fromOrder
     * @param int $toOrder
     */
    public function __construct(int $fromOrder, ?int $toOrder)
    {
        $this->fromOrder = $fromOrder;
        $this->toOrder = $toOrder;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_ORDER => $this->getByOrderCriteriaValue(),
            self::ORDER_BY => 'order'
        ];
    }

    /**
     * @return BetweenValues|CompareValue
     */
    private function getByOrderCriteriaValue()
    {
        if ($this->toOrder !== null) {
            return new BetweenValues(
                new CompareValue($this->fromOrder, '>='),
                new CompareValue($this->toOrder, '<=')
            );
        }

        return new CompareValue($this->fromOrder, '>=');
    }
}
