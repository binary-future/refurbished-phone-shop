<?php

namespace App\Application\Services\Model\TopModel\Query;

use App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary;
use App\Application\Services\Model\TopModel\TopModel;
use App\Domain\Common\Contracts\Repository\OperatorDictionary;
use App\Domain\Common\Contracts\Repository\Query;
use App\Utils\Repository\Criteria\Common\CompareValue;

/**
 * Class AllOrderedWithoutSynonymsQuery
 * @package App\Application\Services\Model\TopModel\Query
 */
final class AllOrderedWithoutSynonymsQuery implements Query, TopModelCriteriaDictionary, OperatorDictionary
{
    /**
     * @var array
     */
    private $relations;
    /**
     * @var int
     */
    private $limit;

    /**
     * AllOrderedWithoutSynonymsQuery constructor.
     * @param array $relations
     * @param int $limit
     */
    public function __construct(array $relations = [], int $limit = 0)
    {
        $this->relations = $relations;
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations,
            self::ORDER_BY => TopModel::FIELD_ORDER,
            self::CRITERIA_BY_SYNONYMS => new CompareValue(null, self::OD_IS),
            self::LIMIT => $this->limit
        ];
    }
}
