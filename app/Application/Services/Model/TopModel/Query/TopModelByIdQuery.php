<?php


namespace App\Application\Services\Model\TopModel\Query;


use App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class TopModelByIdQuery
 * @package App\Application\Services\Model\TopModel\Query
 */
final class TopModelByIdQuery implements Query, TopModelCriteriaDictionary
{
    /**
     * @var int
     */
    private $id;

    /**
     * AllOrderedQuery constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ID => $this->id
        ];
    }
}
