<?php


namespace App\Application\Services\Model\TopModel\Query;


use App\Application\Services\Model\TopModel\Repository\TopModelCriteriaDictionary;
use App\Application\Services\Model\TopModel\TopModel;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class AllOrderedQuery
 * @package App\Application\Services\Model\TopModel\Query
 */
final class AllOrderedQuery implements Query, TopModelCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations;
    /**
     * @var int
     */
    private $limit;

    /**
     * AllOrderedQuery constructor.
     * @param array $relations
     * @param int|null $limit
     */
    public function __construct(array $relations = [], int $limit = 0)
    {
        $this->relations = $relations;
        $this->limit = $limit;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => $this->relations,
            self::ORDER_BY => TopModel::FIELD_ORDER,
            self::LIMIT => $this->limit
        ];
    }
}
