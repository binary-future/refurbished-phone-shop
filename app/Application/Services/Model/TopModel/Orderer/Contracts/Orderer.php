<?php

namespace App\Application\Services\Model\TopModel\Orderer\Contracts;

use Illuminate\Support\Collection;

interface Orderer
{
    public function orderBetween(int $oldOrder, int $newOrder): Collection;

    public function addedTo(int $order): Collection;

    public function deletedFrom(int $order): Collection;
}
