<?php


namespace App\Application\Services\Model\TopModel\Orderer;


use App\Application\Services\Model\TopModel\Orderer\Contracts\Orderer as Contract;
use App\Application\Services\Model\TopModel\Query\TopModelsInOrderDiapasonQuery;
use App\Application\Services\Model\TopModel\Repository\TopModels;
use App\Application\Services\Model\TopModel\TopModel;
use Illuminate\Support\Collection;

final class Orderer implements Contract
{
    /**
     * @var TopModels
     */
    private $repository;

    public function __construct(TopModels $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $oldOrder
     * @param int $newOrder
     * @return Collection
     */
    public function orderBetween(int $oldOrder, int $newOrder): Collection
    {
        $orderShift = $this->getOrderShift($oldOrder, $newOrder);
        $fromOrder = $this->getFromOrder($oldOrder, $newOrder);
        $toOrder = $this->getToOrder($oldOrder, $newOrder);

        return $this->updateOrders($fromOrder, $toOrder, $orderShift);
    }

    /**
     * @param int $order
     * @return Collection
     */
    public function addedTo(int $order): Collection
    {
        return $this->updateOrders($order, null, 1);
    }

    /**
     * @param int $order
     * @return Collection
     */
    public function deletedFrom(int $order): Collection
    {
        return $this->updateOrders($order + 1, null, -1);
    }

    /**
     * @param int|null $fromOrder
     * @param int|null $toOrder
     * @param int $orderShift
     * @return Collection
     */
    private function updateOrders(?int $fromOrder, ?int $toOrder, int $orderShift): Collection
    {
        $intermediateTopModels = $this->repository->findBy(
            new TopModelsInOrderDiapasonQuery($fromOrder, $toOrder)
        );

        return $this->updateIntermediateTopModelsOrders(
            $intermediateTopModels,
            $fromOrder,
            $orderShift
        );
    }

    /**
     * @param Collection $topModels
     * @param int $fromOrder
     * @param int $orderShift
     * @return Collection
     */
    private function updateIntermediateTopModelsOrders(
        Collection $topModels,
        int $fromOrder,
        int $orderShift
    ): Collection
    {
        foreach ($topModels as $ind => $topModel) {
            /**
             * @var $topModel TopModel
             */
            $newTopModelOrder = $fromOrder + $orderShift + $ind;
            $topModel->setOrder($newTopModelOrder);
        }

        return $topModels;
    }

    /**
     * @param int $oldOrder
     * @param int $newOrder
     * @return int
     */
    private function getOrderShift(?int $oldOrder, int $newOrder): int
    {
        return $oldOrder !== null && $newOrder > $oldOrder
            ? -1
            : 1;
    }

    /**
     * @param int $oldOrder
     * @param int $newOrder
     * @return int
     */
    private function getFromOrder(?int $oldOrder, int $newOrder): int
    {
        return $oldOrder === null || $oldOrder > $newOrder
            ? $newOrder
            : $oldOrder + 1;
    }

    /**
     * @param int $oldOrder
     * @param int $newOrder
     * @return int
     */
    private function getToOrder(int $oldOrder, int $newOrder): int
    {
        return $oldOrder > $newOrder
            ? $oldOrder - 1
            : $newOrder;
    }
}
