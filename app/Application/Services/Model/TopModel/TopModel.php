<?php

namespace App\Application\Services\Model\TopModel;

use App\Domain\Phone\Model\PhoneModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class TopModel
 * @package App\Application\Services\Model\TopModel
 */
class TopModel extends Model
{
    public const TABLE = 'top_models';

    public const FIELD_ID = 'id';
    public const FIELD_MODEL_ID = 'model_id';
    public const FIELD_ORDER = 'order';

    public const RELATION_MODEL = 'model';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_MODEL_ID, self::FIELD_ORDER
    ];

    /**
     * @return int
     */
    public function getModelId(): int
    {
        return $this->getAttribute(self::FIELD_MODEL_ID);
    }

    /**
     * @param int $modelId
     */
    public function setModelId(int $modelId): void
    {
        $this->setAttribute(self::FIELD_MODEL_ID, $modelId);
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return (int) $this->getAttribute(self::FIELD_ORDER);
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->setAttribute(self::FIELD_ORDER, $order);
    }

    /**
     * @return BelongsTo
     */
    public function model(): BelongsTo
    {
        return $this->belongsTo(PhoneModel::class, self::FIELD_MODEL_ID);
    }

    /**
     * @return PhoneModel
     */
    public function getModel(): PhoneModel
    {
        return $this->getRelationValue(self::RELATION_MODEL);
    }
}
