<?php


namespace App\Application\Services\Model\TopModel\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

/**
 * Interface TopModelCriteriaDictionary
 * @package App\Application\Services\Model\TopModel\Repository
 */
interface TopModelCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_ORDER = 'by-order';
    public const CRITERIA_BY_MODEL_ID = 'by-model-id';
    public const CRITERIA_BY_SYNONYMS = 'by-synonyms';
}
