<?php

namespace App\Application\Services\Model\TopModel\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface TopModel
 * @package App\Application\Services\Model\TopModel\Repository
 */
interface TopModels extends Repository
{

}
