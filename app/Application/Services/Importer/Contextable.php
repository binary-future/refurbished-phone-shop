<?php

namespace App\Application\Services\Importer;

/**
 * Interface Contextable
 * @package App\Application\Services\Importer
 */
interface Contextable
{
    /**
     * @param $context
     * @return mixed
     */
    public function setContext($context);
}
