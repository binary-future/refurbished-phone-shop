<?php


namespace App\Application\Services\Importer;

/**
 * Interface Loggable
 * @package App\Application\Services\Importer
 */
interface Loggable
{
    /**
     * @param bool $logging
     * @return mixed
     */
    public function setLogging(bool $logging);
}