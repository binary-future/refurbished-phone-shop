<?php

namespace App\Application\Services\Importer\Datafeeds\Selectors;

use App\Application\Services\Importer\Datafeeds\ApiDictionary;
use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\AwinDealsImport;
use App\Application\Services\Importer\Scenarios\AwinPhonesImport;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use App\Application\Services\Importer\Scenarios\EcrawlerDealsImport;

/**
 * Class ImportScenarioSelector
 * @package App\Application\Services\Importer\Datafeeds\Selectors
 */
class ImportScenarioSelector
{
    public const DEALS = 'deals';
    public const PHONES = 'phones';

    /**
     * @var array
     */
    private $scenario = [
        self::DEALS => [
            ApiDictionary::AWIN => AwinDealsImport::class,
            ApiDictionary::ECRAWLER => EcrawlerDealsImport::class,
//            ApiDictionary::REVGLUE => RevglueDealsImport::class,
//            ApiDictionary::WEBGAINS => [
//                'default' => WebgainsDealsImport::class,
//                'affordable-mobiles' => WebgainsAffordableDealsImport::class,
//            ]
        ],
        self::PHONES => [
            ApiDictionary::AWIN => AwinPhonesImport::class,
        ]
    ];

    /**
     * @param DatafeedInfo $datafeed
     * @param string $context
     * @return Scenario|null
     */
    public function select(DatafeedInfo $datafeed, string $context): ?Scenario
    {
        $scenarioName = $this->scenario[$context][$datafeed->getApi()->getName()] ?? null;
        if (is_array($scenarioName)) {
            $scenarioName = $scenarioName[$datafeed->getStore()->getAlias()] ?? $scenarioName['default'] ?? null;
        }

        return $scenarioName ? new $scenarioName($datafeed) : null;
    }
}
