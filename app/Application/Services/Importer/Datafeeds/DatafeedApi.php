<?php

namespace App\Application\Services\Importer\Datafeeds;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DatafeedApi
 * @package App\Application\Services\Importer\Datafeeds
 */
class DatafeedApi extends Model
{
    public const TABLE = 'datafeeds_api';

    public const FIELD_ID = 'id';
    public const FIELD_NAME = 'name';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME
    ];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function datafeeds()
    {
        return $this->hasMany(DatafeedInfo::class, DatafeedInfo::FIELD_API_ID);
    }

    /**
     * @return mixed
     */
    public function getDatafeeds()
    {
        return $this->datafeeds;
    }
}
