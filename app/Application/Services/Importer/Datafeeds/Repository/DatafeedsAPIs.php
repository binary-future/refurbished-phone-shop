<?php


namespace App\Application\Services\Importer\Datafeeds\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface DatafeedsAPIs
 * @package App\Application\Services\Importer\Datafeeds\Repository
 */
interface DatafeedsAPIs extends Repository
{

}