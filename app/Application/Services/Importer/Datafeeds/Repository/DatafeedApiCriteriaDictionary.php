<?php


namespace App\Application\Services\Importer\Datafeeds\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

/**
 * Interface DatafeedApiCriteriaDictionary
 * @package App\Application\Services\Importer\Datafeeds\Repository
 */
interface DatafeedApiCriteriaDictionary extends CriteriaDictionary
{

}