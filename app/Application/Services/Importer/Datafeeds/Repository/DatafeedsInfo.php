<?php

namespace App\Application\Services\Importer\Datafeeds\Repository;

use App\Domain\Common\Contracts\Repository\Repository;

/**
 * Interface DatafeedsInfo
 * @package App\Application\Services\Importer\Datafeeds\Repository
 */
interface DatafeedsInfo extends Repository
{

}
