<?php


namespace App\Application\Services\Importer\Datafeeds\Repository;

use App\Domain\Common\Contracts\Repository\CriteriaDictionary;

/**
 * Interface DatafeedInfoCriteriaDictionary
 * @package App\Application\Services\Importer\Datafeeds\Repository
 */
interface DatafeedInfoCriteriaDictionary extends CriteriaDictionary
{
    public const CRITERIA_BY_STORES = 'by-stores';
    public const CRITERIA_BY_API_NAME = 'by-api-name';
}