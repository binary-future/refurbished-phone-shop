<?php

namespace App\Application\Services\Importer\Datafeeds;

/**
 * Interface ApiDictionary
 * @package App\Application\Services\Importer\Datafeeds
 */
interface ApiDictionary
{
    public const AWIN = 'awin';
    public const ECRAWLER = 'ecrawler';

    public const NAMES = [
        self::AWIN, self::ECRAWLER
    ];
}
