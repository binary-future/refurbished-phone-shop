<?php


namespace App\Application\Services\Importer\Datafeeds\Query;


use App\Application\Services\Importer\Datafeeds\Repository\DatafeedInfoCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

final class GetInfoByApiNameQuery implements Query, DatafeedInfoCriteriaDictionary
{
    /**
     * @var string
     */
    private $apiName;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * GetInfoByStoresQuery constructor.
     * @param string $api
     * @param array $relations
     */
    public function __construct(string $api, array $relations = [])
    {
        $this->apiName = $api;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_API_NAME => $this->apiName,
            self::WITH => $this->relations
        ];
    }
}