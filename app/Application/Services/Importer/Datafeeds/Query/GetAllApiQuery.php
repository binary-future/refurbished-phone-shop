<?php


namespace App\Application\Services\Importer\Datafeeds\Query;


use App\Application\Services\Importer\Datafeeds\Repository\DatafeedApiCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;

/**
 * Class GetAllApiQuery
 * @package App\Application\Services\Importer\Datafeeds\Query
 */
final class GetAllApiQuery implements Query, DatafeedApiCriteriaDictionary
{
    /**
     * @var array
     */
    private $relations = [];

    /**
     * GetAllApiQuery constructor.
     * @param array $relations
     */
    public function __construct(array $relations = [])
    {
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::ALL => [],
            self::WITH => $this->relations
        ];
    }
}
