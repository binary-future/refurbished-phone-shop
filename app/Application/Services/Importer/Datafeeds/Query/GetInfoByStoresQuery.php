<?php


namespace App\Application\Services\Importer\Datafeeds\Query;


use App\Application\Services\Importer\Datafeeds\Repository\DatafeedInfoCriteriaDictionary;
use App\Domain\Common\Contracts\Repository\Query;
use Illuminate\Support\Collection;

/**
 * Class GetInfoByStoresQuery
 * @package App\Application\Services\Importer\Datafeeds\Query
 */
final class GetInfoByStoresQuery implements Query, DatafeedInfoCriteriaDictionary
{
    /**
     * @var Collection
     */
    private $stores;

    /**
     * @var array
     */
    private $relations = [];

    /**
     * GetInfoByStoresQuery constructor.
     * @param $stores
     * @param $relations
     */
    public function __construct(Collection $stores, array $relations = [])
    {
        $this->stores = $stores;
        $this->relations = $relations;
    }

    /**
     * @return array
     */
    public function getCriteria(): array
    {
        return [
            self::CRITERIA_BY_STORES => $this->stores,
            self::WITH => $this->relations
        ];
    }
}
