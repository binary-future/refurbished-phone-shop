<?php

namespace App\Application\Services\Importer\Datafeeds;

use App\Domain\Store\Store;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Throwable;

/**
 * Class DatafeedInfo
 * @package App\Application\Services\Importer\Datafeeds
 */
class DatafeedInfo extends Model
{
    public const TABLE = 'datafeeds_info';

    public const FIELD_ID = 'id';
    public const FIELD_API_ID = 'api_id';
    public const FIELD_EXTERNAL_ID = 'external_id';
    public const FIELD_STORE_ID = 'store_id';

    public const RELATION_API = 'api';
    public const RELATION_STORE = 'store';

    /**
     * @var string
     */
    protected $table = self::TABLE;

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        self::FIELD_API_ID, self::FIELD_EXTERNAL_ID, self::FIELD_STORE_ID
    ];

    /**
     * @return int
     */
    public function getApiId(): int
    {
        return $this->getAttribute(self::FIELD_API_ID);
    }

    /**
     * @param int $apiId
     */
    public function setApiId(int $apiId)
    {
        $this->setAttribute(self::FIELD_API_ID, $apiId);
    }

    /**
     * @return int
     */
    public function getExternalId(): int
    {
        return $this->getAttribute(self::FIELD_EXTERNAL_ID);
    }

    /**
     * @param int $externalId
     */
    public function setExternalId(int $externalId)
    {
        $this->setAttribute(self::FIELD_EXTERNAL_ID, $externalId);
    }

    /**
     * @return int
     */
    public function getStoreId(): int
    {
        return $this->getAttribute(self::FIELD_STORE_ID);
    }

    /**
     * @param int $storeId
     */
    public function setStoreId(int $storeId): void
    {
        $this->setAttribute(self::FIELD_STORE_ID, $storeId);
    }

    /**
     * @return BelongsTo
     */
    public function api(): BelongsTo
    {
        return $this->belongsTo(DatafeedApi::class, self::FIELD_API_ID);
    }

    /**
     * @return DatafeedApi
     */
    public function getApi(): DatafeedApi
    {
        return $this->getRelationValue(self::RELATION_API);
    }

    /**
     * @return BelongsTo
     */
    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class, self::FIELD_STORE_ID);
    }

    /**
     * @return Store
     */
    public function getStore(): Store
    {
        return $this->getRelationValue(self::RELATION_STORE);
    }

    public function hasApiValues(): bool
    {
        return ($this->getAttribute(self::FIELD_API_ID)
            && $this->getAttribute(self::FIELD_EXTERNAL_ID));
    }

    public function isValid(): bool
    {
        try {
            return ($this->getApiId() && $this->getExternalId() && $this->getStoreId());
        } catch (Throwable $exception) {
            return false;
        }
    }

    public function isEqual(DatafeedInfo $datafeedInfo): bool
    {
        return $this->getApiId() === $datafeedInfo->getApiId()
            && $this->getExternalId() === $datafeedInfo->getExternalId()
            && $this->getStoreId() === $datafeedInfo->getStoreId();
    }
}
