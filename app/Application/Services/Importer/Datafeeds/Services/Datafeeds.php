<?php

namespace App\Application\Services\Importer\Datafeeds\Services;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Query\GetInfoByStoresQuery;
use App\Application\Services\Importer\Datafeeds\Repository\DatafeedsInfo;
use App\Application\Services\Importer\Datafeeds\Services\Contracts\Datafeeds as Contract;
use App\Domain\Store\Store;
use App\Utils\Serializer\Contracts\Serializer;

final class Datafeeds implements Contract
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var DatafeedsInfo
     */
    private $datafeedsInfo;

    /**
     * Datafeeds constructor.
     * @param Serializer $serializer
     * @param DatafeedsInfo $datafeedsInfo
     */
    public function __construct(Serializer $serializer, DatafeedsInfo $datafeedsInfo)
    {
        $this->serializer = $serializer;
        $this->datafeedsInfo = $datafeedsInfo;
    }

    public function store(Store $store, array $params)
    {
        $datafeed = $this->buildDatafeed(array_filter($params), $store);
        $previousDataFeed = $this->getPreviousDatafeed($store);
        if ($previousDataFeed) {
            $this->remove($previousDataFeed);
        }

        return $datafeed->isValid() ? $this->datafeedsInfo->save($datafeed) : false;
    }

    private function buildDatafeed(array $values, Store $store): DatafeedInfo
    {
        /**
         * @var DatafeedInfo $datafeed
         */
        $datafeed = $this->serializer->fromArray(DatafeedInfo::class, $values);
        $datafeed->setStoreId($store->getKey());

        return $datafeed;
    }

    private function getPreviousDatafeed(Store $store): ?DatafeedInfo
    {
        return $this->datafeedsInfo->findBy(new GetInfoByStoresQuery(collect([$store])))->first();
    }

    public function remove(DatafeedInfo $datafeed)
    {
        return $this->datafeedsInfo->delete($datafeed);
    }
}
