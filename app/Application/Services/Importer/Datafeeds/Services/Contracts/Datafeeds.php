<?php


namespace App\Application\Services\Importer\Datafeeds\Services\Contracts;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Domain\Store\Store;

interface Datafeeds
{
    public function store(Store $store, array $params);

    public function remove(DatafeedInfo $datafeed);
}