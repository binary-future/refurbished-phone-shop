<?php

namespace App\Application\Services\Importer\Datafeeds\LinkGenerator\Contracts;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;

interface LinkGenerator
{
    /**
     * @param DatafeedInfo $datafeed
     * @param string $context
     * @return string
     * @throws ImportHandlerBuildException
     * @throws \App\Utils\Importer\Exceptions\SourceException
     */
    public function getLink(DatafeedInfo $datafeed, string $context): string;
}
