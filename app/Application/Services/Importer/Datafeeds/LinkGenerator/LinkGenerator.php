<?php


namespace App\Application\Services\Importer\Datafeeds\LinkGenerator;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Datafeeds\Selectors\ImportScenarioSelector;
use App\Utils\Importer\Exceptions\ImportHandlerBuildException;
use App\Utils\Importer\Source\Contracts\LinkGeneratorFactory;

class LinkGenerator implements \App\Application\Services\Importer\Datafeeds\LinkGenerator\Contracts\LinkGenerator
{
    /**
     * @var ImportScenarioSelector
     */
    private $scenarioSelector;
    /**
     * @var LinkGeneratorFactory
     */
    private $linkGeneratorFactory;

    /**
     * LinkGenerator constructor.
     * @param ImportScenarioSelector $scenarioSelector
     * @param LinkGeneratorFactory $linkGeneratorFactory
     */
    public function __construct(
        ImportScenarioSelector $scenarioSelector,
        LinkGeneratorFactory $linkGeneratorFactory
    ) {
        $this->scenarioSelector = $scenarioSelector;
        $this->linkGeneratorFactory = $linkGeneratorFactory;
    }

    /**
     * @param DatafeedInfo $datafeed
     * @param string $context
     * @return string
     * @throws ImportHandlerBuildException
     * @throws \App\Utils\Importer\Exceptions\SourceException
     */
    public function getLink(DatafeedInfo $datafeed, string $context): string
    {
        $scenario = $this->scenarioSelector->select($datafeed, $context);
        if (! $scenario) {
            throw ImportHandlerBuildException::cannotFindScenario();
        }

        $requestGenerator = $this->linkGeneratorFactory->getRequestGenerator(
            $datafeed->getApi()->getName()
        );

        return $requestGenerator->generateLink($scenario);
    }

}
