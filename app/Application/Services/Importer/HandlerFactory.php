<?php

namespace App\Application\Services\Importer;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Interface HandlerFactory
 * @package App\Application\Services\Importer
 */
interface HandlerFactory
{
    /**
     * @param Scenario $scenario
     * @return Handler
     */
    public function buildHandler(Scenario $scenario): Handler;
}
