<?php

namespace App\Application\Services\Importer\Transformers\Preprocessors;

use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Domain\Phone\Model\PhoneModel;

final class AwinPreprocessor implements Transformer
{
    private const GROUP_PHONE_MODEL = 'phone_model';

    private $modelsSynonymsList = [
        'Note9' => 'Note 9',
    ];

    public function transform($data, array $options = [])
    {
        $data[self::GROUP_PHONE_MODEL][PhoneModel::FIELD_NAME] = $this->prepareModelName(
            $data[self::GROUP_PHONE_MODEL][PhoneModel::FIELD_NAME]
        );

        return $data;
    }

    private function prepareModelName(string $name): string
    {
        foreach ($this->modelsSynonymsList as $search => $replace) {
            $name = str_ireplace($search, $replace, $name);
        }

        return $this->removeSpaces($name);
    }

    private function removeSpaces(string $string): string
    {
        return trim(implode(' ', array_filter(explode(' ', $string))));
    }
}
