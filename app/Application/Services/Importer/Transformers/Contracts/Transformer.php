<?php

namespace App\Application\Services\Importer\Transformers\Contracts;

/**
 * Interface Transformer
 * @package App\Application\Services\Importer\Transformers\Contracts
 */
interface Transformer
{
    /**
     * @param mixed $data
     * @param array $options
     * @return mixed
     */
    public function transform($data, array $options = []);
}
