<?php

namespace App\Application\Services\Importer\Transformers;

use App\Application\Services\Importer\Contextable;
use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Specification\Contracts\Specification;
use App\Utils\Specification\Exceptions\SpecificationBuildException;
use App\Utils\Specification\Factory;
use Throwable;

/**
 * Class StructureTransformer
 * @package App\Application\Services\Importer\Transformers
 */
final class StructureTransformer implements Transformer, Contextable
{
    private const STRUCTURE_MAP = 'transformers.structure-map';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Transformer
     */
    private $structureTransformer;

    /**
     * @var Transformer
     */
    private $modelTransformer;

    /**
     * @var Factory
     */
    private $specificationFactory;

    /**
     * @var Specification
     */
    private $specification;

    /**
     * @var array
     */
    private $map = [];

    /**
     * StructureTransformer constructor.
     * @param Config $config
     * @param Transformer $structureTransformer
     * @param Transformer $modelTransformer
     * @param Factory $specificationFactory
     */
    public function __construct(
        Config $config,
        Transformer $structureTransformer,
        Transformer $modelTransformer,
        Factory $specificationFactory
    ) {
        $this->config = $config;
        $this->structureTransformer = $structureTransformer;
        $this->modelTransformer = $modelTransformer;
        $this->specificationFactory = $specificationFactory;
    }

    /**
     * @param mixed $context
     * @return mixed|void
     * @throws SpecificationBuildException
     */
    public function setContext($context)
    {
        $this->setMap($context);
        $this->setSpecification($context);
        if ($this->modelTransformer instanceof Contextable) {
            $this->modelTransformer->setContext($context);
        }
    }

    /**
     * @param string $context
     */
    private function setMap(string $context)
    {
        $this->map = $this->config->get(
            sprintf('%s.%s', self::STRUCTURE_MAP, $context),
            []
        );
    }

    /**
     * @param string $context
     * @throws SpecificationBuildException
     */
    private function setSpecification(string $context)
    {
        $this->specification = $this->specificationFactory->buildSpecification($context);
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return bool|mixed
     */
    public function transform($data, array $options = [])
    {
        $transformedData = $this->transformStructure($data);
        if (! $this->specification->isSatisfy($transformedData)) {
            return false;
        }

        return $this->callNext($transformedData, $options);
    }

    /**
     * @param mixed $data
     * @param array $options
     * @return bool|mixed
     */
    private function callNext($data, array $options)
    {
        try {
            return $this->modelTransformer->transform($data, $options);
        } catch (Throwable $exception) {
            // TODO: Report about exception, now just skipping
            return false;
        }
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    private function transformStructure($data)
    {
        return $this->structureTransformer->transform($data, $this->map);
    }
}
