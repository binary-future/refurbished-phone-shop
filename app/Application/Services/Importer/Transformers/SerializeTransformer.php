<?php

namespace App\Application\Services\Importer\Transformers;

use App\Application\Services\Importer\Contextable;
use App\Application\Services\Importer\Transformers\Contracts\Transformer;
use App\Utils\Adapters\Config\Contracts\Config;
use App\Utils\Serializer\Contracts\Serializer;
use App\Utils\Transformers\Factory;

final class SerializeTransformer implements Transformer, Contextable
{
    private const CONFIG_PATH = 'importers.import-commands';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Factory
     */
    private $transformerFactory;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Transformer
     */
    private $transformer;

    /**
     * @var string
     */
    private $response;

    /**
     * SerializeTransformer constructor.
     * @param Config $config
     * @param Factory $transformerFactory
     * @param Serializer $serializer
     */
    public function __construct(Config $config, Factory $transformerFactory, Serializer $serializer)
    {
        $this->config = $config;
        $this->transformerFactory = $transformerFactory;
        $this->serializer = $serializer;
    }

    public function setContext($context)
    {
        $this->transformer = $this->transformerFactory->buildTransformer($context);
        $this->response = $this->getResponseType($context);
    }

    private function getResponseType(string $context)
    {
        return $this->config->get(sprintf('%s.%s', self::CONFIG_PATH, $context));
    }

    public function transform($data, array $options = [])
    {
        $result = $this->transformer->transform($data, $options);

        return $this->serializer->fromArray($this->response, $result);
    }
}
