<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\Paginable;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

final class RevglueDealsImport implements Scenario, Paginable
{
    public const CONTEXT = 'revglue-deals-import';

    /**
     * @var DatafeedInfo
     */
    private $datafeed;

    /**
     * @var int
     */
    private $currentPage = self::DEFAULT_CURRENT_PAGE;

    /**
     * RevglueDealsImport constructor.
     * @param DatafeedInfo $datafeed
     */
    public function __construct(DatafeedInfo $datafeed)
    {
        $this->datafeed = $datafeed;
    }

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return DatafeedInfo
     */
    public function getContent()
    {
        return $this->datafeed;
    }

    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    public function setCurrentPage(int $currentPage)
    {
        $this->currentPage = $currentPage;
    }

    public function getDescription(): string
    {
        return $this->datafeed->getStore()->getName();
    }

    public function getFields(): array
    {
        // TODO: Implement getFields() method.
    }
}
