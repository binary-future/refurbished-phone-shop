<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\AwinScenario;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\AwinPhonesImportFields;

/**
 * Class AwinPhonesImport
 * @package App\Application\Services\Importer\Scenarios
 */
final class AwinPhonesImport implements AwinScenario, AwinPhonesImportFields
{
    public const CONTEXT = 'awin-phones-import';

    /**
     * @var DatafeedInfo
     */
    private $datafeed;

    /**
     * RevglueDealsImport constructor.
     * @param DatafeedInfo $datafeed
     */
    public function __construct(DatafeedInfo $datafeed)
    {
        $this->datafeed = $datafeed;
    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return DatafeedInfo
     */
    public function getContent()
    {
        return $this->datafeed;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->datafeed->getStore()->getName();
    }

    public function getFields(): array
    {
        return [
            self::BRAND_NAME,
            self::PRODUCT_MODEL,
            self::PRODUCT_NAME,
            self::IN_STOCK,
            self::MERCHANT_IMAGE_URL,
            self::CUSTOM_2,
        ];
    }
}
