<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

final class WebgainsDealsImport implements Scenario
{
    public const CONTEXT = 'webgains-deals-import';

    /**
     * @var DatafeedInfo
     */
    private $datafeed;

    /**
     * RevglueDealsImport constructor.
     * @param DatafeedInfo $datafeed
     */
    public function __construct(DatafeedInfo $datafeed)
    {
        $this->datafeed = $datafeed;
    }

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return DatafeedInfo
     */
    public function getContent()
    {
        return $this->datafeed;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->datafeed->getStore()->getName();
    }

    public function getFields(): array
    {
        // TODO: Implement getFields() method.
    }
}
