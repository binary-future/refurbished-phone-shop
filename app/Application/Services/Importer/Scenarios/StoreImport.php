<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Scenarios\Contracts\Fields\StoreImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Class StoreImport
 * @package App\Application\Services\Importer\Scenarios
 */
final class StoreImport implements Scenario, StoreImportFields
{
    public const CONTEXT = 'stores-data-import';

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return 'stores-data';
    }

    public function getDescription(): string
    {
        return $this->getContent();
    }

    public function getFields(): array
    {
        return [
            self::RG_STORE_ID,
            self::API_ID,
            self::STORE_TITLE,
            self::URL_KEY,
            self::TERM_IN_MONTHS,
            self::GUARANTEE_IN_MONTHS,
            self::STORE_DESCRIPTION,
            self::IMAGE_URL,
            self::URL,
        ];
    }
}
