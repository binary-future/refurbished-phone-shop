<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Scenarios\Contracts\Fields\SpecsSeedFields;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Class SpecsSeed
 * @package App\Application\Services\Importer\Scenarios
 */
final class SpecsSeed implements Scenario, SpecsSeedFields
{
    public const CONTEXT = 'specs-dump-import';

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return 'Specs Item';
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getContent();
    }

    public function getFields(): array
    {
        return [
            self::BRAND,
            self::MODEL,
            self::MEMORY_CARD_SLOT,
            self::CHIPSET,
            self::BLUETOOTH,
            self::WIFI,
            self::GPS,
            self::BATTERY_TYPE,
            self::DISPLAY_SIZE,
            self::DISPLAY_TYPE,
        ];
    }
}
