<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Scenarios\Contracts\Fields\SpecsDumpImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

final class SpecsDumpImport implements Scenario, SpecsDumpImportFields
{
    public const CONTEXT = 'specs-revglue-dump';

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    public function getContent()
    {
        return $this->getContext();
    }

    public function getDescription(): string
    {
        return 'Specs';
    }

    public function getFields(): array
    {
        return [
            self::BRAND,
            self::MODEL,
            self::BLUETOOTH,
            self::WIFI,
            self::GPS,
            self::SIMS_CAPACITY,
            self::MEMORY_CARD,
            self::USB,
            self::THREE_OR_4G,
            self::WIFI_HOTSPOT,
            self::RAM,
            self::PROCESSOR,
            self::TOUCHSCREEN,
            self::RESOLUTION,
            self::FRONT_CAMERA,
            self::BACK_CAMERA,
            self::BATTERY_CAPACITY,
            self::STANDBY_TIME,
            self::TALK_TIME,
            self::WEIGHT,
            self::THICKNESS,
            self::HEIGHT,
            self::WIDTH,
            self::OPERATING_SYSTEM,
        ];
    }
}
