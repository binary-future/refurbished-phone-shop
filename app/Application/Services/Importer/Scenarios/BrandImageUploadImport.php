<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Scenarios\Contracts\Fields\BrandImageUploadImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use Illuminate\Http\UploadedFile;

final class BrandImageUploadImport implements Scenario, BrandImageUploadImportFields
{
    public const CONTEXT = 'brand-images-upload';

    /**
     * @var UploadedFile
     */
    private $content;

    /**
     * PhoneImageUploadImport constructor.
     * @param UploadedFile $content
     */
    public function __construct(UploadedFile $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return UploadedFile
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return self::CONTEXT;
    }

    public function getFields(): array
    {
        return [
            self::NAME,
            self::PATH,
        ];
    }
}
