<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\EcrawlerScenario;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\EnvirofoneDealsImportFields;

final class EcrawlerDealsImport implements EcrawlerScenario, EnvirofoneDealsImportFields
{
    public const CONTEXT = 'ecrawler-deals-import';

    /**
     * @var DatafeedInfo
     */
    private $datafeed;

    public function __construct(DatafeedInfo $datafeed)
    {
        $this->datafeed = $datafeed;
    }

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return DatafeedInfo
     */
    public function getContent()
    {
        return $this->datafeed;
    }

    public function getDescription(): string
    {
        return $this->datafeed->getStore()->getName();
    }

    public function getFields(): array
    {
        return [];
    }
}
