<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Scenarios\Contracts\Fields\StoreRatingSeedFields;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Class StoreRatingSeed
 * @package App\Application\Services\Importer\Scenarios
 */
final class StoreRatingSeed implements Scenario, StoreRatingSeedFields
{
    public const CONTEXT = 'stores-rating-import';

    /**
     * @return string
     */
    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return 'Rating Item';
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getContent();
    }

    public function getFields(): array
    {
        return [
            self::STORE_SLUG,
            self::RATING_VALUE,
            self::URL,
        ];
    }
}
