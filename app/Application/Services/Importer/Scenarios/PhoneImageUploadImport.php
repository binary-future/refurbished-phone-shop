<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Scenarios\Contracts\Fields\PhoneImageUploadImportFields;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;
use Illuminate\Http\UploadedFile;

/**
 * Class PhoneImageUploadImport
 * @package App\Application\Services\Importer\Scenarios
 */
final class PhoneImageUploadImport implements Scenario, PhoneImageUploadImportFields
{
    public const CONTEXT = 'phone-images-upload';

    /**
     * @var UploadedFile
     */
    private $content;

    /**
     * PhoneImageUploadImport constructor.
     * @param UploadedFile $content
     */
    public function __construct(UploadedFile $content)
    {
        $this->content = $content;
    }

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return UploadedFile
     */
    public function getContent()
    {
        return $this->content;
    }

    public function getDescription(): string
    {
        return $this->getContext();
    }

    public function getFields(): array
    {
        return [
            self::NAME,
            self::PATH,
        ];
    }
}
