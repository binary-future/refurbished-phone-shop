<?php

namespace App\Application\Services\Importer\Scenarios\Contracts;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;

interface EcrawlerScenario extends Scenario
{
    /**
     * AwinScenario constructor.
     * @param DatafeedInfo $datafeedInfo
     */
    public function __construct(DatafeedInfo $datafeedInfo);
}
