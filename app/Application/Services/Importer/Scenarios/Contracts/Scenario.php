<?php

namespace App\Application\Services\Importer\Scenarios\Contracts;

interface Scenario
{
    public function getContext(): string;

    public function getContent();

    public function getDescription(): string;

    /** @return string[] */
    public function getFields(): array;
}
