<?php

namespace App\Application\Services\Importer\Scenarios\Contracts;

interface Paginable extends Scenario
{
    public const DEFAULT_CURRENT_PAGE = 1;

    public function getCurrentPage(): int;

    public function setCurrentPage(int $currentPage);
}