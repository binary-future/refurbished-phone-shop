<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface BrandImageUploadImportFields
{
    public const NAME = 'name';
    public const PATH = 'path';
}
