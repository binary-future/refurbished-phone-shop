<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface StoreImportFields
{
    public const RG_STORE_ID = 'rg_store_id';
    public const API_ID = 'api_id';
    public const STORE_TITLE = 'store_title';
    public const URL_KEY = 'url_key';
    public const TERM_IN_MONTHS = 'term_in_months';
    public const GUARANTEE_IN_MONTHS = 'guarantee_in_months';
    public const STORE_DESCRIPTION = 'store_description';
    public const IMAGE_URL = 'image_url';
    public const URL = 'url';
}
