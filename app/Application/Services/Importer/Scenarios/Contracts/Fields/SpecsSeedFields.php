<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface SpecsSeedFields
{
    public const BRAND = 'brand';
    public const MODEL = 'model';
    public const MEMORY_CARD_SLOT = 'memory_card_slot';
    public const CHIPSET = 'chipset';
    public const BLUETOOTH = 'bluetooth';
    public const WIFI = 'wifi';
    public const GPS = 'gps';
    public const BATTERY_TYPE = 'battery_type';
    public const DISPLAY_SIZE = 'display_size';
    public const DISPLAY_TYPE = 'display_type';
}
