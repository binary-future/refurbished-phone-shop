<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface SpecsDumpImportFields
{
    public const BRAND = 'brand';
    public const MODEL = 'model';
    public const BLUETOOTH = 'bluetooth';
    public const WIFI = 'wifi';
    public const GPS = 'gps';
    public const SIMS_CAPACITY = 'sims_capacity';
    public const MEMORY_CARD = 'memory_card';
    public const USB = 'usb';
    public const THREE_OR_4G = 'three_or_4g';
    public const WIFI_HOTSPOT = 'wifi_hotspot';
    public const RAM = 'ram';
    public const PROCESSOR = 'processor';
    public const TOUCHSCREEN = 'touchscreen';
    public const RESOLUTION = 'resolution';
    public const FRONT_CAMERA = 'front_camera';
    public const BACK_CAMERA = 'back_camera';
    public const BATTERY_CAPACITY = 'battery_capacity';
    public const STANDBY_TIME = 'standby_time';
    public const TALK_TIME = 'talk_time';
    public const WEIGHT = 'weight';
    public const THICKNESS = 'thickness';
    public const HEIGHT = 'height';
    public const WIDTH = 'width';
    public const OPERATING_SYSTEM = 'operating_system';
}
