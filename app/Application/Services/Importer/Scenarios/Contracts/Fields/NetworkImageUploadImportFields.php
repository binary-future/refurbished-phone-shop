<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface NetworkImageUploadImportFields
{
    public const NAME = 'name';
    public const PATH = 'path';
}
