<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface StoreRatingSeedFields
{
    public const STORE_SLUG = 'storeSlug';
    public const RATING_VALUE = 'ratingValue';
    public const URL = 'url';
}
