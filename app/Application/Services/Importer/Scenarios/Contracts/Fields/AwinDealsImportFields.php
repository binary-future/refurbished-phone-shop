<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface AwinDealsImportFields
{
    public const BRAND_NAME = 'brand_name';
    public const PRODUCT_MODEL = 'product_model';
    public const PRODUCT_NAME = 'product_name';
    public const AW_PRODUCT_ID = 'aw_product_id';
    public const IN_STOCK = 'in_stock';
    public const MERCHANT_IMAGE_URL = 'merchant_image_url';
    public const DESCRIPTION = 'description';
    public const BASE_PRICE = 'base_price';
    public const AW_DEEP_LINK = 'aw_deep_link';
    public const CUSTOM_1 = 'custom_1';
    public const CUSTOM_2 = 'custom_2';
}
