<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface PhoneImageUploadImportFields
{
    public const NAME = 'name';
    public const PATH = 'path';
}
