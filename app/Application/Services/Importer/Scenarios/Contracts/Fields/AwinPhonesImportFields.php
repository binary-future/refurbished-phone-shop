<?php

namespace App\Application\Services\Importer\Scenarios\Contracts\Fields;

interface AwinPhonesImportFields
{
    public const BRAND_NAME = 'brand_name';
    public const PRODUCT_MODEL = 'product_model';
    public const PRODUCT_NAME = 'product_name';
    public const IN_STOCK = 'in_stock';
    public const MERCHANT_IMAGE_URL = 'merchant_image_url';
    public const CUSTOM_2 = 'custom_2';
}
