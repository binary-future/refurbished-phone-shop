<?php


namespace App\Application\Services\Importer\Scenarios\Contracts;


use App\Application\Services\Importer\Datafeeds\DatafeedInfo;

/**
 * Interface AwinScenario
 * @package App\Application\Services\Importer\Scenarios\Contracts
 */
interface AwinScenario extends Scenario
{
    /**
     * AwinScenario constructor.
     * @param DatafeedInfo $datafeedInfo
     */
    public function __construct(DatafeedInfo $datafeedInfo);
}