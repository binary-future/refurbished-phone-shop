<?php

namespace App\Application\Services\Importer\Scenarios;

use App\Application\Services\Importer\Datafeeds\DatafeedInfo;
use App\Application\Services\Importer\Scenarios\Contracts\AwinScenario;
use App\Application\Services\Importer\Scenarios\Contracts\Fields\AwinDealsImportFields;

final class AwinDealsImport implements AwinScenario, AwinDealsImportFields
{
    public const CONTEXT = 'awin-deals-import';

    /**
     * @var DatafeedInfo
     */
    private $datafeed;

    public function __construct(DatafeedInfo $datafeed)
    {
        $this->datafeed = $datafeed;
    }

    public function getContext(): string
    {
        return self::CONTEXT;
    }

    /**
     * @return DatafeedInfo
     */
    public function getContent()
    {
        return $this->datafeed;
    }

    public function getDescription(): string
    {
        return $this->datafeed->getStore()->getName();
    }

    public function getFields(): array
    {
        return [
            self::BRAND_NAME,
            self::PRODUCT_MODEL,
            self::PRODUCT_NAME,
            self::AW_PRODUCT_ID,
            self::IN_STOCK,
            self::MERCHANT_IMAGE_URL,
            self::DESCRIPTION,
            self::BASE_PRICE,
            self::AW_DEEP_LINK,
            self::CUSTOM_1,
            self::CUSTOM_2,
        ];
    }
}
