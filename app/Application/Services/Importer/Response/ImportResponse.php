<?php

namespace App\Application\Services\Importer\Response;

use App\Application\Services\Importer\Response\Contracts\ImportResponse as Contract;

/**
 * Class ImportResponse
 * @package App\Application\Services\Importer\Response
 */
final class ImportResponse implements Contract
{
    private const START_COUNT = 1;

    /**
     * @var array
     */
    private $content = [];

    /**
     * @var bool
     */
    private $success = true;

    /**
     * @param bool $success
     * @return mixed|void
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
    }


    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param string $alias
     * @param mixed|null $content
     * @return int|mixed|null
     */
    public function add(string $alias, $content = null)
    {
        if ($content) {
            return $this->content[$alias] = $content;
        }

        if (isset($this->content[$alias]) && is_numeric($this->content[$alias])) {
            return $this->content[$alias]++;
        }

        return $this->content[$alias] = self::START_COUNT;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param string $param
     * @return mixed|null
     */
    public function get(string $param)
    {
        return $this->content[$param] ?? null;
    }
}
