<?php

namespace App\Application\Services\Importer\Response\Contracts;

/**
 * Interface ImportResponse
 * @package App\Application\Services\Importer\Response\Contracts
 */
interface ImportResponse
{
    /**
     * @param bool $success
     * @return mixed
     */
    public function setSuccess(bool $success);

    /**
     * @return bool
     */
    public function isSuccess(): bool;

    /**
     * @param string $alias
     * @param $content
     * @return mixed
     */
    public function add(string $alias, $content = null);

    /**
     * @return array
     */
    public function getContent(): array;

    /**
     * @param string $param
     * @return mixed
     */
    public function get(string $param);
}
