<?php

namespace App\Application\Services\Importer\Specifications;

use App\Utils\Specification\Contracts\Specification;

final class WebgainsDealsImportSpecification implements Specification
{
    /**
     * @var Specification
     */
    private $specification;

    /**
     * RevglueDealsImportSpecification constructor.
     * @param Specification $specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    public function isSatisfy($value, $params = null): bool
    {
        return $this->specification->isSatisfy($value, $this->getRequiredFields())
            && $this->isNotException($value);
    }


    /**
     * @return array
     */
    private function getRequiredFields(): array
    {
        return [
            'brand' => [
                'name'
            ],
            'product' => [
                'name'
            ],
            'color' => [
                'name'
            ],
            'link' => [
                'link'
            ],
            'deal-payment' => [
                "payment_term", "initial_cost", "monthly_cost"
            ],
            'operator' => [
                'name'
            ],
            'deal' => [
                'external_id',
                'in_stock',
            ]
        ];
    }

    private function isNotException(array $data): bool
    {
        return $this->checkOperator($data['operator']['name'] ?? null)
            && $this->checkDealType($data['deal']['type'] ?? null);
    }

    private function checkOperator(?string $operator): bool
    {
        return $operator && stripos($operator, 'no network') === false;
    }

    private function checkDealType(?string $type)
    {
        return $type && stripos($type, 'free') === false;
    }
}