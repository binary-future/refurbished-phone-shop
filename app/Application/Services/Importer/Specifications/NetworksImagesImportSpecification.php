<?php

namespace App\Application\Services\Importer\Specifications;

class NetworksImagesImportSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'image' => ['path'],
            'network' => ['slug']
        ];
    }
}
