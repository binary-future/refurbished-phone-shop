<?php

namespace App\Application\Services\Importer\Specifications;

final class PhonesImportSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'phone' => [
                'external_id', 'capacity'
            ],
            'brand' => [
                'name'
            ],
            'color' => [
                'name'
            ],
            'phone_model' => [
                'name'
            ]
        ];
    }
}
