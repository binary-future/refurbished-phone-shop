<?php

namespace App\Application\Services\Importer\Specifications;

/**
 * Class StoreImportSpecification
 * @package App\Application\Services\Importer\Specifications
 */
final class StoreImportSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'store' => [
                'name', 'slug'
            ],
            'datafeed' => [
                'external_id', 'api_id'
            ],
        ];
    }
}
