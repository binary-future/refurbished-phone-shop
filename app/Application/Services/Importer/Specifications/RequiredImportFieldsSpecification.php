<?php

namespace App\Application\Services\Importer\Specifications;

use App\Utils\Specification\Contracts\Specification;

abstract class RequiredImportFieldsSpecification implements Specification
{
    /**
     * @var Specification
     */
    private $specification;

    /**
     * StoreImportSpecification constructor.
     * @param Specification $specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        return $this->specification->isSatisfy($value, $this->getRequiredFields());
    }

    /**
     * @return array
     */
    abstract protected function getRequiredFields(): array;
}
