<?php


namespace App\Application\Services\Importer\Specifications;

/**
 * Class PhonesImagesImportSpecification
 * @package App\Application\Services\Importer\Specifications
 */
final class BrandsImagesImportSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'image' => ['path'],
            'brand' => ['slug']
        ];
    }
}
