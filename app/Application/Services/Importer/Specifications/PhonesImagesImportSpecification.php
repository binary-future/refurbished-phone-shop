<?php


namespace App\Application\Services\Importer\Specifications;

/**
 * Class PhonesImagesImportSpecification
 * @package App\Application\Services\Importer\Specifications
 */
final class PhonesImagesImportSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'image' => ['path'],
            'phone' => ['check_hash']
        ];
    }
}
