<?php

namespace App\Application\Services\Importer\Specifications\Awin;

use App\Domain\Deals\Contract\Network;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Utils\Specification\Contracts\Specification;

final class AwinPhonesImportSpecification implements Specification
{
    /**
     * @var Specification
     */
    private $specification;

    /**
     * RevglueDealsImportSpecification constructor.
     * @param Specification $specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    /**
     * @param mixed $value
     * @param null $params
     * @return bool
     */
    public function isSatisfy($value, $params = null): bool
    {
        return is_array($value)
            && $this->specification->isSatisfy($value, $this->getRequiredFields())
            && $this->checkStock($value['deal']['in_stock'] ?? null)
            && $this->checkNetwork($value['network'][Network::FIELD_NAME] ?? null);
    }

    /**
     * @return array
     */
    private function getRequiredFields(): array
    {
        return [
            'brand' => [
                Brand::FIELD_NAME
            ],
            'phone_model' => [
                PhoneModel::FIELD_NAME
            ],
            'product' => [
                'name',
            ]
        ];
    }

    private function checkNetwork(?string $network): bool
    {
        return (bool)$network;
    }

    private function checkStock($stock): bool
    {
        return $stock !== null && $stock !== '0';
    }
}
