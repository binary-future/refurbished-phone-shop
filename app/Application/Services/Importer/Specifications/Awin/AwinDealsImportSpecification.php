<?php

namespace App\Application\Services\Importer\Specifications\Awin;

use App\Domain\Deals\Contract\Contract;
use App\Domain\Deals\Contract\Network;
use App\Domain\Deals\Deal\Deal;
use App\Domain\Phone\Brand\Brand;
use App\Domain\Phone\Model\PhoneModel;
use App\Domain\Shared\Image\Image;
use App\Domain\Shared\Link\Link;
use App\Utils\Specification\Contracts\Specification;

class AwinDealsImportSpecification implements Specification
{
    /**
     * @var Specification
     */
    private $specification;

    /**
     * AwinDealsImportSpecification constructor.
     * @param Specification $specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    public function isSatisfy($value, $params = null): bool
    {
        return $this->specification->isSatisfy($value, $this->getRequiredFields())
            && $this->isNotException($value);
    }


    /**
     * @return array
     */
    private function getRequiredFields(): array
    {
        return [
            'brand' => [
                Brand::FIELD_NAME
            ],
            'phone_model' => [
                PhoneModel::FIELD_NAME
            ],
            'link' => [
                Link::FIELD_LINK
            ],
            'product' => [
                'name',
            ],
            'image' => [
                Image::FIELD_PATH
            ],
            'contract' => [
                Contract::FIELD_CONDITION,
            ],
            'deal-payment' => [
                Deal::FIELD_TOTAL_COST
            ],
            'deal' => [
                'in_stock',
                Deal::FIELD_EXTERNAL_ID
            ],
            'network' => [
                Network::FIELD_NAME
            ],
        ];
    }

    private function isNotException(array $data): bool
    {
        return $this->checkStock($data['deal']['in_stock'])
            && $this->checkCapacity($data['product']['name']);
    }

    private function checkStock(string $stock): bool
    {
        return $stock !== '0';
    }

    /**
     * @param string $productName
     * @return bool
     */
    private function checkCapacity(string $productName): bool
    {
        return stripos($productName, 'GB') !== false;
    }
}
