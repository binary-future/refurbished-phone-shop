<?php

namespace App\Application\Services\Importer\Specifications;

use App\Utils\Specification\Contracts\Specification;

final class RevglueDealsImportSpecification implements Specification
{
    /**
     * @var Specification
     */
    private $specification;

    /**
     * RevglueDealsImportSpecification constructor.
     * @param Specification $specification
     */
    public function __construct(Specification $specification)
    {
        $this->specification = $specification;
    }

    public function isSatisfy($value, $params = null): bool
    {
        return $this->specification->isSatisfy($value, $this->getRequiredFields())
            && $this->isNotException($value);
    }


    /**
     * @return array
     */
    private function getRequiredFields(): array
    {
        return [
            'product' => [
                'external_id'
            ],
            'link' => [
                'link'
            ],
            'deal-payment' => [
                "payment_term", "initial_cost", "monthly_cost"
            ],
            'network' => [
                'name'
            ],
        ];
    }

    private function isNotException(array $data): bool
    {
        return $this->checkDealTitle($data['deal']['title'] ?? null)
            && $this->checkNetwork($data['network']['name'] ?? null)
            && $this->checkMobileId((int) $data['product']['external_id'] ?? 0)
            && $this->checkDealType($data['deal']['type'] ?? null);
    }

    private function checkMobileId(int $mobileId): bool
    {
        return $mobileId > 0;
    }

    private function checkNetwork(?string $network): bool
    {
        return $network && stripos($network, 'no network') === false;
    }

    private function checkDealTitle(?string $title)
    {
        return $title
            && stripos($title, 'sim card') === false
            && stripos($title, 'Classic Pay As You Go') === false;
    }

    private function checkDealType(?string $type)
    {
        return $type && stripos($type, 'sim free') === false;
    }
}
