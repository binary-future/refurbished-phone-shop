<?php


namespace App\Application\Services\Importer\Specifications;


final class SpecsSeedSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'brand' => [
                'name'
            ],
            'phone_model' => [
                'name'
            ]
        ];
    }
}