<?php

namespace App\Application\Services\Importer\Specifications;

use App\Domain\Shared\Link\Link;
use App\Domain\Shared\Rating\Rating;
use App\Domain\Store\Store;

final class StoresRatingSeedSpecification extends RequiredImportFieldsSpecification
{
    /**
     * @return array
     */
    protected function getRequiredFields(): array
    {
        return [
            'store' => [Store::FIELD_SLUG],
            'rating' => [Rating::FIELD_RATING],
            'link' => [Link::FIELD_LINK],
        ];
    }
}
