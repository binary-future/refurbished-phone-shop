<?php

namespace App\Application\Services\Importer\Source;

use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Interface Source
 * @package App\Application\Services\Importer\Source
 */
interface Source
{
    /**
     * @param Scenario $scenario
     * @return SourceResponse
     */
    public function getContent(Scenario $scenario): SourceResponse;
}
