<?php

namespace App\Application\Services\Importer\Source;


interface SourceResponse
{
    /**
     * @return array
     */
    public function getContent(): iterable;

    /**
     * @return bool
     */
    public function isSucceed(): bool;

    /**
     * @param bool $succeed
     */
    public function setSucceed(bool $succeed): void;

    /**
     * @return bool
     */
    public function isPaginated(): bool;

    /**
     * @param bool $paginated
     */
    public function setPaginated(bool $paginated): void;

    /**
     * @return int
     */
    public function getPages(): int;

    /**
     * @param int $pages
     */
    public function setPages(int $pages): void;

    /**
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * @param int $currentPage
     */
    public function setCurrentPage(int $currentPage): void;

    /**
     * @return string
     */
    public function getMessage(): string;

    /**
     * @param string $message
     * @return mixed
     */
    public function setMessage(string $message);
}
