<?php

namespace App\Application\Services\Importer;

use App\Application\Services\Importer\Response\Contracts\ImportResponse;
use App\Application\Services\Importer\Scenarios\Contracts\Scenario;

/**
 * Interface Handler
 * @package App\Application\Services\Importer
 */
interface Handler
{
    /**
     * @param Scenario $scenario
     * @return ImportResponse
     */
    public function handleImport(Scenario $scenario): ImportResponse;
}
