# refshop

Application for comparing refurbished mobiles

## Deploy

### Environment
You can start from deploying docker from `.server/docker` folder.
We have easy to use `yank` scripts for doing any small commands in docker containers.

- Open project root
- Run `./yank dc up -d` for running docker containers

##### Yank 
Open `scripts.json` in project root and there you will see quick scripts for easy working 
with docker containers. 

 - `yank dc` - use for work with docker-compose in `.server/docker` directory
 - `yank artisan` - runs artisan in 'app' container
 - `yank composer` - runs composer in 'app' container
 - `yank phpunit` - runs phpunit in 'app' container
 
 ### Project
 Do next steps for deploying project for starting work as expected:
 
 1. Copy `.env` file and set necessary configurations or execute next command 
 `./yank composer post-root-package-install`
 2. Generate key if you need `composer composer post-create-project-cmd`
 3. `./yank artisan storage:link`
 4. Seed data
    - `./yank artisan db:seed -- --class=DatafeedApiSeeder`
    - `./yank artisan db:seed -- --class=RoleSeeder`
    - `./yank artisan db:seed -- --class=UserSeeder`
 5. Install and configure Redis (it needs for working filters and deals importing) 
 6. Install and configure Supervisord (see `.server/supervisord/supervisord.conf`)
 7. Import and activate data 
    - `./yank artisan store:import`. Go to `stores` table and 
      set `1` value for `is_active` flag 
    - `./yank artisan phones:import`. Go to `phone_models` table and 
      set `1` value for `is_active` flag 
    - `./yank artisan specs:seed`
    - `./yank artisan deals:import`
 8. Download Wordpress database and import it to your local database
 9. Download image archive
